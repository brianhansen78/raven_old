//
//  NSCIncludes.h
//  V-Control
//
//  Created by Neyrinck on 7/4/12.
//  Copyright (c) 2012 Neyrinck. All rights reserved.
//

#ifndef NSCSDKIncludes_h
#define NSCSDKIncludes_h

#include <string>

typedef enum   // highest level events.  they often use an index to indicate a track or a subtype 
{
    eNSCMainLCD=1,  //used by Mackie Control, Command 8
    eNSCPlugInLCD=2,  //used by HUI
    eNSCTimeDisplay=4,
    
    eNSCTrackEvent=9,
	eNSCSystem,
    
    eNSCKeyboard=142,
    
    eNSCMemoryLocation=164,
    
    eNSCGlobalControl=194,	
    eNSCGlobalName=195,		

	eNSCPlugInInsert=196,	
	eNSCPlugInInsertName,
	eNSCPlugInState,
	eNSCPlugInControl,
	eNSCPlugInControlDisplayValue,
	eNSCPlugInControlDisplayName,
    
    eNSCInstrumentControl=212,
	eNSCInstrumentControlDisplayValue,
	eNSCInstrumentControlDisplayName,
    
	eNSCRequestRefresh=219,
    
	eNSCControlRoom = 1002,

    eNSCVWindowMessage=211,	

	eNSCVWindow2Message = 3000,

	eNSCTest = 4000,

} ENSCEventType;

typedef enum
{
	eNSCTest_ping_TCP = 0,
	eNSCTest_ping_UDP,
	eNSCTest_layer_1
} ENSCTest;

typedef enum
{
	eDAWMode_Disabled=-1000,
	eDAWMode_Samplitude,
	eDAWMode_Acid,
    eDAWMode_MediaComposer,
	eDAWMode_Soundscape,
	eDAWMode_SAWStudio,
	eDAWMode_Audition,   
	eDAWMode_None = -1,
	eDAWMode_Cubase,
	eDAWMode_DigitalPerformer,
	eDAWMode_FinalCut,
	eDAWMode_FLStudio,
	eDAWMode_Live,
	eDAWMode_Logic,
	eDAWMode_MIOConsole,
	eDAWMode_ProTools,
    eDAWMode_Reaper,
	eDAWMode_Reason,
	eDAWMode_Sonar,
	eDAWMode_StudioOne,
	eDAWMode_Tracktion,
	
	eDAWMode_TotalNumber
} EDAWMode;

typedef enum 
{
	eControllerBankAll		= -5,
	eControllerBankCurrent	= -4,
	eControllerBankPanner	= -3,
	eControllerBankVpc		= -2,
	eControllerBankUnused	= -1,
	eControllerBankVControl	=  0,
	eControllerBankXT2,
	eControllerBankXT3,
	eControllerBankXT4,
	eControllerBankXT5,
	eControllerBankXT6,
	eControllerBankXT7,
	eControllerBankXT8,
	eMaxControllerBank
} EControllerBank;




typedef enum 
{
	eNSCTrackName=3,
	eNSCTrackFaderValue=9,
	eNSCTrackVPotValue,
	eNSCTrackSendValue,
	eNSCTrackFaderSwitch,
	eNSCTrackMuteSwitch,
	eNSCTrackSoloSwitch,
	eNSCTrackRecordSwitch,
	eNSCTrackInMonitorSwitch,
	eNSCTrackSelectSwitch,
	eNSCTrackAutoSwitch,
	eNSCTrackVSelectSwitch,
	eNSCTrackInsertSwitch,
	eNSCTrackVPotSwitch,
    
	eNSCTrackMeter1=96,
	eNSCTrackMeter2,
	eNSCTrackMeter3,
	eNSCTrackMeter4,
	eNSCTrackMeter5,
	eNSCTrackMeter6,
	eNSCTrackMeter7,
	eNSCTrackMeter8,
    
	eNSCTrackMeterPeak1=169,
	eNSCTrackMeterPeak2,
	eNSCTrackMeterPeak3,
	eNSCTrackMeterPeak4,
	eNSCTrackMeterPeak5,
	eNSCTrackMeterPeak6,
	eNSCTrackMeterPeak7,
	eNSCTrackMeterPeak8,
	
	eNSCTrackMeterClip1=177,
	eNSCTrackMeterClip2,
	eNSCTrackMeterClip3,
	eNSCTrackMeterClip4,
	eNSCTrackMeterClip5,
	eNSCTrackMeterClip6,
	eNSCTrackMeterClip7,
	eNSCTrackMeterClip8,
    
	eNSCTrackFaderDisplayValue=188,
	eNSCTrackKnobValue,  //Logic Protocol
	eNSCTrackName2,		// deprecated

	eNSCTrackMode=204,   //what is this?
	eNSCMixerMode=205,   //what is this?
    
	eNSCTrackKnobDisplayValue=206,
	eNSCTrackKnobDisplayName,
	eNSCTrackKnobTouch,

} ENSCTrackEventType;



typedef enum
{
	eNSCGSTransportFirst=0,
	eNSCGSTransportRewind=0,
	eNSCGSTransportFastFwd,
	eNSCGSTransportStop,
	eNSCGSTransportPlay,
	eNSCGSTransportRecord,
	eNSCGSTransportPause,
	eNSCGSTransportRTZ,
	eNSCGSTransportGTE,
	eNSCGSTransportLoopMode,
	eNSCGSTransportPunchMode,
	eNSCGSTransportLast=eNSCGSTransportPunchMode,
	
	eNSCGSGotoLeft, //10
	eNSCGSGotoRight,
	eNSCGSMarkerSwitch,	
	eNSCGSNudgeSwitch,
	eNSCGSCycleSwitch,
	eNSCGSDropSwitch,
	eNSCGSReplaceSwitch,
	eNSCGSClickSwitch,
	eNSCGSSoloSwitch,
	eNSCGSSetLeft,	//19
	
	eNSCGSSetRight,//20
	eNSCGSSetLeftDrop,
	eNSCGSSetRightDrop,
	eNSCGSGroupSwitch,
	eNSCGSUtilitiesSaveSwitch,
	eNSCGSUtilitiesUndoSwitch,
	eNSCGSUtilitiesCancelSwitch,
	eNSCGSUtilitiesEnterSwitch,
	eNSCGSUtilitiesEditModeSwitch,
	eNSCGSUtilitiesEditToolSwitch,
	
	eNSCGSCursorUp,//30
	eNSCGSCursorDown,
	eNSCGSCursorLeft,
	eNSCGSCursorRight,
	eNSCGSZoom,
	eNSCGSScrub,
	eNSCGSShuttle,
	eNSCGSWindow0,//37
	eNSCGSWindow1,//38  //PT Edit
	eNSCGSWindow2,      //PT Mix
	
	eNSCGSWindow3,//40
	eNSCGSWindow4,
	eNSCGSWindow5,//42
	eNSCGSWindow6,
	eNSCGSWindow7,//44
	eNSCGSWindow8,
	eNSCGSWindow9,//46
	eNSCGSWindow10,
	eNSCGSWindow11,//48
	eNSCGSWindow12,
	
	eNSCGSWindow13,//50
	eNSCGSWindow14,
	eNSCGSWindow15,
	eNSCGSWindow16,
	eNSCGSWindow17,
	eNSCGSWindow18,
	eNSCGSWindow19,
	eNSCGSScreenSet0,
	eNSCGSScreenSet1,//58
	eNSCGSScreenSet2,
	
	eNSCGSScreenSet3,//60
	eNSCGSScreenSet4,
	eNSCGSScreenSet5,//62
	eNSCGSScreenSet6,
	eNSCGSScreenSet7,//64
	eNSCGSScreenSet8,
	eNSCGSScreenSet9,//66
	eNSCGSReserved1,    //was cut command (and is used for cut in logic)
	eNSCGSReserved2,//68  //was copy command  (and is used for copy in logic)
	eNSCGSReserved3, //was paste command  (and is used for paste in logic)
	
	eNSCGSCommandDelete,//70
	eNSCGSCommandSelectAll,
	eNSCGSAutoVolume,//72
	eNSCGSAutoPan,
	eNSCGSAutoMute,//74
	eNSCGSAutoSend,
	eNSCGSAutoSendMute,//76
	eNSCGSAutoPlugIn,
	eNSCGSAutoSolo,//78
	eNSCGSInsert1,
	
	eNSCGSInsert2,//80
	eNSCGSInsert3,
	eNSCGSInsert4,
	eNSCGSInsert5,
	eNSCGSInsert6,
	eNSCGSInsert7,
	eNSCGSInsert8,
	eNSCGSInsert9,
	eNSCGSInsert10,
	eNSCGSInsert11,
	
	eNSCGSInsert12,//90
	eNSCGSInsert13,
	eNSCGSInsert14,
	eNSCGSInsert15,
	eNSCGSInsert16,
	eNSCGSInsertBypass1,
	eNSCGSInsertBypass2,
	eNSCGSInsertBypass3,
	eNSCGSInsertBypass4,
	eNSCGSInsertBypass5,
	
	eNSCGSInsertBypass6,//100
	eNSCGSInsertBypass7,
	eNSCGSInsertBypass8,
	eNSCGSInsertBypass9,
	eNSCGSInsertBypass10,
	eNSCGSInsertBypass11,
	eNSCGSInsertBypass12,
	eNSCGSInsertBypass13,
	eNSCGSInsertBypass14,
	eNSCGSInsertBypass15,
	
	eNSCGSInsertBypass16,//110
	eNSCGSUtilitiesRedoSwitch,
	eNSCGSTransportOnlineMode,
	eNSCGSMarkerPrevious,
	eNSCGSMarkerNext=114,
	/* deprecated, use keyboard type instead
	eNSCGSKeyboard1,//115
	eNSCGSKeyboard2,
	eNSCGSKeyboard3,
	eNSCGSKeyboard4,
	eNSCGSKeyboard5,
	eNSCGSKeyboard6,//120
	eNSCGSKeyboard7,
	eNSCGSKeyboard8,
	eNSCGSKeyboard9,
	eNSCGSKeyboard0,
	eNSCGSKeyboardDel,//125
	eNSCGSKeyboardEsc,
	eNSCGSKeyboardEnter,
	eNSCGSKeyboardPeriod,
	eNSCGSKeyboardLeft,
	eNSCGSKeyboardRight,//130
	eNSCGSKeyboardUp,
	eNSCGSKeyboardDown, */
	
	eNSCGSGotoNumber=133,
	eNSCGSMuteSwitch,
	eNSCGSBarsBeatsCounterSwitch,//135
	eNSCGSSMPTECounterSwitch,
	eNSCGSFeetFramesCounterSwitch,
	eNSCGSMinSecsCounterSwitch,
	
	eNSCGSAutomationOffSwitch,
	eNSCGSAutomationReadSwitch,//140
	eNSCGSAutomationTouchSwitch,
	eNSCGSAutomationLatchSwitch,
	eNSCGSAutomationWriteSwitch,
	eNSCGSAutomationTrimSwitch,
    
    eNSCGSPlugInInsert,    //these are for HUI and other plugin editors //145
    eNSCGSPlugInAssign,
	eNSCGSPlugInSwitchSelect1,
	eNSCGSPlugInSwitchSelect2,
	eNSCGSPlugInSwitchSelect3,
	eNSCGSPlugInSwitchSelect4,//150
    eNSCGSPlugInBypass,
    eNSCGSPlugInCompare,
    
    eNSCGSPlugInVPot1,
    eNSCGSPlugInVPot2,
    eNSCGSPlugInVPot3,    //155
    eNSCGSPlugInVPot4,
    
    eNSCGSPlugInBankLeft,
    eNSCGSPlugInBankRight,

    eNSCGSInstrumentBankLeftSwitch,
    eNSCGSInstrumentBankRightSwitch,    //160
    
    eNSCGSBankMode1,    //Send A for PT, Send 1 for Cubase, Logic
    eNSCGSBankMode2,    //Send B for PT, Send 2 for Cubase, Logic
    eNSCGSBankMode3,    //Send C for PT, Send 3 for Cubase, Logic
    eNSCGSBankMode4,    //Send D for PT, Send 4 for Cubase, Logic
    eNSCGSBankMode5,  //165  //Send E for PT, Send 5 for Cubase, Logic
    eNSCGSBankMode6,    //Send 6 for Logic
    eNSCGSBankMode7,    //Send 7 for Logic
    eNSCGSBankMode8,    //Send 8 for Logic
    eNSCGSBankMode9,
    eNSCGSBankMode10,//170
    eNSCGSBankMode11,
    eNSCGSBankMode12,
    eNSCGSBankMode13,
    eNSCGSBankMode14,   //Input Mode for PT
    eNSCGSBankMode15,//175       //Output Mode for PT
    eNSCGSBankMode16,      //Pan Mode for PT
    
    eNSCGSModifierShift,
    eNSCGSModifierAlt,
    eNSCGSModifierOption,
    eNSCGSModifierControl,//180
    
    eNSCGSEditCommand1,
    eNSCGSEditCommand2,
    eNSCGSEditCommand3,
    eNSCGSEditCommand4,
    eNSCGSEditCommand5,
    eNSCGSEditCommand6,
    eNSCGSEditCommand7,
    eNSCGSEditCommand8,
    eNSCGSEditCommand9,
    eNSCGSEditCommand10,//190
    eNSCGSEditCommand11,
    eNSCGSEditCommand12,
    eNSCGSEditCommand13,
    eNSCGSEditCommand14,
    eNSCGSEditCommand15,
    eNSCGSEditCommand16,
    
    eNSCGSMemLocation1,
    eNSCGSMemLocation2,
    eNSCGSMemLocation3,
    eNSCGSMemLocation4,//200
    eNSCGSMemLocation5,
    eNSCGSMemLocation6,
    eNSCGSMemLocation7,
    eNSCGSMemLocation8,
    eNSCGSMemLocation9,
    eNSCGSMemLocation10,
    eNSCGSMemLocation11,
    eNSCGSMemLocation12,
    eNSCGSMemLocation13,
    eNSCGSMemLocation14,//210
    eNSCGSMemLocation15,
    eNSCGSMemLocation16,

    eNSCGSClearMeters,

    eNSCGSSelectAssign,
    eNSCGSFlip,
    eNSCGSAutoSuspend,
    eNSCGSRecReadyAll,
    eNSCGSInsertBypass,
    eNSCGSDefault,
    eNSCGSSelectMute,//220
    eNSCGSAudition,
    eNSCGSAuditionIn,
    eNSCGSAuditionOut,
    eNSCGSPreRoll,
    eNSCGSPostRoll,
    
    eNSCGSGroupSuspend,
    eNSCGSGroupCreate,
    eNSCGSGroupStatus,
    eNSCGSAutoStatus,
    eNSCGSMonitor,//230
    
    eNSCGSTimeCounterDisplayDeprecated,	// which = 0xSSSSMMMM where SSSS = start char, MMMM=max display length for this time format
    eNSCGSJogShuttleWheel,
	
	eNSCGSAssignmentBankLeftSwitch,//233
	eNSCGSAssignmentBankRightSwitch,
	eNSCGSAssignmentChannelLeftSwitch,
	eNSCGSAssignmentChannelRightSwitch,
	eNSCGSFlipSwitch,

	eNSCGSEditTool1,
	eNSCGSEditTool2,
	eNSCGSEditTool3,
	eNSCGSEditTool4,
	eNSCGSEditTool5,
	eNSCGSEditTool6,
	eNSCGSEditTool7,
	eNSCGSEditTool8,
	eNSCGSEditTool9,
	eNSCGSEditTool10,

    eNSCGSEditMode1,
    eNSCGSEditMode2,
    eNSCGSEditMode3,
    eNSCGSEditMode4,
    eNSCGSEditMode5,
    eNSCGSEditMode6,
    eNSCGSEditMode7,
    eNSCGSEditMode8,
    eNSCGSEditMode9,
    eNSCGSEditMode10,

	eNSCGSIntrumentControl1=8600,
	eNSCGSIntrumentControlLast=8699,

    eNSCGSInvalid=0x0ffe,
    
    eNSCGSPTCommand_Cut=eNSCGSEditCommand1,
    eNSCGSPTCommand_Copy=eNSCGSEditCommand2,
    eNSCGSPTCommand_Paste=eNSCGSEditCommand3,
    eNSCGSPTCommand_Delete=eNSCGSEditCommand4,
    eNSCGSPTCommand_Capture=eNSCGSEditCommand5,
    eNSCGSPTCommand_Separate=eNSCGSEditCommand6,

    eNSCGSPTBankMode_Pan=eNSCGSBankMode16,
    eNSCGSPTBankMode_SendA=eNSCGSBankMode1,
    eNSCGSPTBankMode_SendB=eNSCGSBankMode2,
    eNSCGSPTBankMode_SendC=eNSCGSBankMode3,
    eNSCGSPTBankMode_SendD=eNSCGSBankMode4,
    eNSCGSPTBankMode_SendE=eNSCGSBankMode5,
    eNSCGSPTBankMode_Input=eNSCGSBankMode14,
    eNSCGSPTBankMode_Output=eNSCGSBankMode15,
    
    eNSCGSPTWindow_Transport=eNSCGSWindow0,
	eNSCGSPTWindow_Edit=eNSCGSWindow1,
	eNSCGSPTWindow_Mix=eNSCGSWindow2,
	eNSCGSPTWindow_PlugIn=eNSCGSWindow3,
	eNSCGSPTWindow_Setup=eNSCGSWindow4,
	eNSCGSPTWindow_MemoryLocations=eNSCGSWindow5,
    
    eNSCGSPTEditTool_Zoom=eNSCGSEditTool1,
    eNSCGSPTEditTool_Scrub=eNSCGSEditTool2,
    eNSCGSPTEditTool_Trim=eNSCGSEditTool3,
    eNSCGSPTEditTool_Select=eNSCGSEditTool4,
    eNSCGSPTEditTool_Grab=eNSCGSEditTool5,
    eNSCGSPTEditTool_Pencil=eNSCGSEditTool6,

    eNSCGSPTEditMode_Shuffle=eNSCGSEditMode1,
    eNSCGSPTEditMode_Slip=eNSCGSEditMode2,
    eNSCGSPTEditMode_Spot=eNSCGSEditMode3,
    eNSCGSPTEditMode_Grid=eNSCGSEditMode4,

} ENSCGlobalControlIndex;



typedef enum 
{
	eNSCControlRoomInputStem=0,
	eNSCControlRoomInputAux,
	eNSCControlRoomInsert,
	eNSCControlRoomSpeaker,
	eNSCControlRoomTalkback,
	eNSCControlRoomVolume,
	eNSCControlRoomRef,
	eNSCControlRoomLock,
	eNSCControlRoomDim,
	eNSCControlRoomMute,
	eNSCControlRoomSum,
	eNSCControlRoomDelayMode,
	eNSCControlRoomDownmix,
	eNSCControlRoomChannel,
	eNSCControlRoomSoloMute,

} ENSCControlRoom;

enum ENSCAutomationMode
{
	eNSCAutoModeOff=0,
	eNSCAutoModeRead,
	eNSCAutoModeTouch,
	eNSCAutoModeLatch,
	eNSCAutoModeWrite
};

typedef enum
{
	eNSCTrackModeStrip=0,
	eNSCTrackModePlugIn,
	eNSCTrackModeInstrument,
	eNSCTrackModeNone
} ENSCTrackMode; 

typedef enum
{
    eNSCKeyboard_Keypad0=0,
    eNSCKeyboard_Keypad1,
    eNSCKeyboard_Keypad2,
    eNSCKeyboard_Keypad3,
    eNSCKeyboard_Keypad4,
    eNSCKeyboard_Keypad5,
    eNSCKeyboard_Keypad6,
    eNSCKeyboard_Keypad7,
    eNSCKeyboard_Keypad8,
    eNSCKeyboard_Keypad9,
    eNSCKeyboard_KeypadAdd=10,
    eNSCKeyboard_KeypadSubtract,
    eNSCKeyboard_KeypadMultiply,
    eNSCKeyboard_KeypadDivide,
    eNSCKeyboard_KeypadSeparator,   //not implemented
    eNSCKeyboard_KeypadDecimalPoint,
    eNSCKeyboard_KeypadEquals,
    eNSCKeyboard_KeypadClear,

    eNSCKeyboard_Space, //not implemented
    eNSCKeyboard_Escape,
    eNSCKeyboard_Enter,
    eNSCKeyboard_Last
} ENSCKeyboard;


//==============================================================================
class NscLink
{
public:
	virtual ~NscLink() {}

	// These are for sending messages to the DAW.
	// bank indicates which bank (of 8 faders) this pertains to.  This is 0-indexed, relative to your starting bank.
	// return value is true if message was sent, false if unrecognized or otherwise can't be sent.
	// Note that subType is not always used, depending on the type of message.
	// There are two overloaded methods.  This is because some NSC messages require a float variable,
	// while some require a string.  A very large majority of messages going to Ney-Fi use floats.

	virtual bool set(ENSCEventType type, int subType, int bank, int which, float val) =0;
	virtual bool set(ENSCEventType type, int subType, int bank, int which, const std::string & data)=0;

	// You may use this to change daw mode, change the bank that your client is representing, or change the number of 
	// banks that are being represented by the client.
	virtual void setDawMode(EDAWMode dawMode, EControllerBank bank, int numBanks)=0;	// bank is ABSOLUTE here

	// This is for connecting to the Ney Fi server.  The arguments are identical to what is passed to 
	// your NscAppDelegate in nscServerFound.
	virtual void connectToServer(const char* serverName, const char * domain)=0;		// call with value from nscServerFound.  Set to serverName and domain to "" disconnect


};

enum { eMatchesAllBanks = -1};

class NscAppDelegate
{
public:
	virtual ~NscAppDelegate() {}
	
	// Here are the callbacks your app will need to handle to receive messages from the DAW
	// There are two overloaded methods because some NSC messages use a float variable,
	// while some use a string.  The majority of messages use floats.  Messages like
	// type = eNSCTrackEvent, subType = eNSCTrackName use a string.
	// Note that subType is not always used, depending on type.
	// return value should be 0, though currently unused.

	virtual int callback(ENSCEventType type, int subType, int bank, int which, float val)=0;
	virtual int callback(ENSCEventType type, int subType, int bank, int which, const std::string & data)=0;

	// The DAW mode may be set remotely.  This will happen if other clients connect to the Ney-Fi server.
	// If two clients set different DAW modes, the most recent message which the Ney-Fi server receives
	// will determine the daw mode, and clients will be updated accordingly.
	// Most messages coming from NeyFi are DAW independent.  However, note that the exact set of NeyFi messages
	// which will be supported are entirely dependent on the the DAW.
	virtual void setDawMode(EDAWMode mode)=0;

	// The following methods are for dealing with and connecting to Ney-Fi.
	// Note that multiple nsc servers may be found, i.e., on different machines on a network.
	// They also may come and go at any time, i.e., if a machine is rebooted or Ney-Fi is closed.
	// Therefore, it is important to build a list of all detected services and present these to the user.
	// return value should be 0, though currently unused.
	
	virtual int nscServerFound(const char* serverName, const char * domain) = 0;	// Called when a NeyFi server has been detected.  Pass arguments to connectToServer to establish connection.
	virtual int nscConnected(const char* serverName, const char * domain) = 0;		// Called once link to server has been established.  NeyFi link can be used for communication only after this callback.
	virtual int nscDisconnected(const char* serverName, const char * domain) = 0;	// Called when NeyFi has stopped (regardless of whether or not a link to it has been formed via connectToServer).  link is no longer valid.
};

//==============================================================================
//
// Library interface functions
//
//==============================================================================

#ifdef __cplusplus
extern "C" {
#endif
	
NscLink *		NSC_CreateLink(unsigned int libraryVersion, class NscAppDelegate * delegate_, EDAWMode dawMode, EControllerBank bank, int numBanks);
void			NSC_DestroyLink(NscLink * link);
unsigned int	NSC_GetLibraryVersion();
const char *    NSC_NameForDAWMode(int mode);

#ifdef __cplusplus
}
#endif


#endif

/*
 Automation
 
 V-Control provides these things to control and display automation:
 
 1.  Track Automation Display - one on every track.  the value displayed depends on the DAW
 2.  Track Automation Select button - one on every track.  this tells the system on which track to edit the automation mode
 3.  Global Automation Mode buttons - this is a set of global buttons. off, read, write, touch, latch, trim.  they are used to set the mode for the track selected for automation editing
 4.  Global Automation Status button.  This tells the system to display extra information in the scribble strips.
 5.  Track Scribble Strips
 
 
 Pro Tools Specific Info
 
 Status Display
 
 Pro Tools and HUI protocol ambiguously displays the automation state of each track with a light that has three states:
 OFF, GREEN, RED
 OFF=off
 GREEN=read
 RED (not blinking) = write
 RED (blinking) = touch or latch
 
 TheGlobal Automation Status button, when pushed and held, displays more information the scribble strips
 
 Button Controls
 
 Each track has a Track  Automation Select button.  This signals Pro Tools to edit the automation for that track.  Pro Tools lights the button to indicate which track is selected for automation editing.
 
 The Global Automation Mode buttons are used to set the mode for the auto selected track.
 */

/*
 HUI DSP/Assign Section
 
 HUI controllers provide a set of buttons, rotary encoders, and 2 X 40 LCD display
 for assigning and editing things.  Pro Tools uses it to assign and edit plugins.  Cubase uses it for even more things.  The DAW responds to control changes to light buttons and change the LCD text.  Thus, it is modal.  Its modes depend on how the DAW chooses to use it in response to the user.  
 
 Look at the Pro Tools MIDI Controllers Guide for more info.
 
 The V-Control SDK provides the HUI DSP/Assign Section controls for the client controller to use as it sees fit.
  
 Pro Tools Specific Info
 Insert/Param Button - This switches the mode between editing inserts or parameters
    Insert Mode - The LCD displays the name of the inserts, four at a time.  When you select it, Pro Tools displays the names of inserts 1-4.  Turning Scroll will cause PT to dispay insert 5.  If you push a Select switch, Pro Tools will switch to Params mode.
    Params Mode - The LCD displays the name and values of plug-in controls, four at a time.  When you select it, Pro Tools displays the names of the plugin controls and their values.  It will also briefly display how many pages there are.  Scroll swicthes through the pages.
 
 Scroll - this rotary encoder is used to see different sets of info on the LCD.
    Insert Mode - Move to next/previous set of inserts
    Params Mode - Move to next/previous plugin controls page
 
 Assign Button - This puts the controller into a mode where you can change the insert.  It starts by displaying inserts 1-4.  Scroll will switch it to display insert 5.  If you turn a Rotary control, the insert name will change to display teh name of a different plugin.  If you have changed it you can press select to change to that plugin.
 
 Rotary 1,  Rotary 2,  Rotary 3,  Rotary 4 - These adjust a value depending on the mode.
 Insert Mode + Assign Mode - These scroll through various plugins avaialable for an insert
 Params Mode - These adjust the plugin control value associated with rotary control
 
 Select 1 Button, Select 2 Button, Select 3 Button, Select 4 Button - These depend on the mode
 Insert Mode + Assign Mode - These confirm a change to an insert made by using a rotary control to select a plugin
 Params Mode - These adjust the plugin control value associated with select switch

 Compare Button - Same as clicking Compare button in a plug-in window.
 Bypass Button - Same as clicking Bypass button in a plug-in window.
 
*/

/* Rotary Encoder Display Value
 A rotary encoder display value has various pieces of information encoded into the value.
 Controller hardware has a set of LEDs that form an arc around a rotary encoder.  The LEDs
 can display a value as a pan position, level value, filter width, and EQ boost/cut.  For more info
 look at Logic_Control_Surfaces_Info.pdf.  HUI controllers have an additional LED that indicates the control is exactly at the center position.
 
 V-Control encodes these pieces of information:
 numdots - this is the number of LEDs
 centerLightOn
 mode - 
 value - this is the actual fractional value to display
 
 - (void)setEncoderValue:(float)to {
 float rawposition = to;
 numdots = (int) (rawposition / 1000.0);
 rawposition -= numdots*1000.0;
 if (!numdots) numdots=11;
 centerLightOn = (rawposition >= 100.0);
 if (rawposition >= 100.0)
 rawposition -= 100.0;
 mode = (int) (rawposition / 10.0);
 rawposition -= mode*10.0;
 value=rawposition;
 [self setNeedsDisplay];
 }
 
 Messages are received for the track encoders and the DSP editor encoders.
 eNSCTrackEvent, eNSCTrackVPotValue
 
 eNSCGlobalControl, eNSCGSPlugInVPot1,
 eNSCGlobalControl, eNSCGSPlugInVPot2,
 eNSCGlobalControl, eNSCGSPlugInVPot3,
 eNSCGlobalControl, eNSCGSPlugInVPot4,

 */

/* Sending Rotary Encoder (V-Pot) Control Changes
 Rotary encoders send control changes as relative changes.  The change is either positive or negative.
 A value of +1 means one click to the right and -1 means one click to the left.  You can send multiple clicks in one message but I can not guarantee how the DAW will respond.
 
 this refers to a track rotary encoder control
 eNSCTrackEvent, eNSCTrackVPotValue
 
 These refer to the four v-pots for HUI plug-in editing
 eNSCGlobalControl, eNSCGSPlugInVPot1,
 eNSCGlobalControl, eNSCGSPlugInVPot2,
 eNSCGlobalControl, eNSCGSPlugInVPot3,
 eNSCGlobalControl, eNSCGSPlugInVPot4,
 
 eNSCGlobalControl, eNSCGSJogShuttleWheel,
 */

/* LCD messages are  received as text messages */

