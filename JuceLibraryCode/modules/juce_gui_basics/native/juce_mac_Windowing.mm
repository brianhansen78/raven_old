/*
  ==============================================================================

   This file is part of the JUCE library - "Jules' Utility Class Extensions"
   Copyright 2004-11 by Raw Material Software Ltd.

  ------------------------------------------------------------------------------

   JUCE can be redistributed and/or modified under the terms of the GNU General
   Public License (Version 2), as published by the Free Software Foundation.
   A copy of the license is included in the JUCE distribution, or can be found
   online at www.gnu.org/licenses.

   JUCE is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

  ------------------------------------------------------------------------------

   To release a closed-source product which uses JUCE, commercial licenses are
   available: visit www.rawmaterialsoftware.com/juce for more information.

  ==============================================================================
*/

void LookAndFeel::playAlertSound()
{
    NSBeep();
}

//==============================================================================
class OSXMessageBox  : private AsyncUpdater
{
public:
    OSXMessageBox (AlertWindow::AlertIconType type,
                   const String& title_, const String& message_,
                   const char* b1, const char* b2, const char* b3,
                   ModalComponentManager::Callback* callback_,
                   const bool runAsync)
        : iconType (type), title (title_),
          message (message_), callback (callback_),
          button1 (b1), button2 (b2), button3 (b3)
    {
        if (runAsync)
            triggerAsyncUpdate();
    }

    int getResult() const
    {
        JUCE_AUTORELEASEPOOL
        NSInteger r = getRawResult();
        return r == NSAlertDefaultReturn ? 1 : (r == NSAlertOtherReturn ? 2 : 0);
    }

    static int show (AlertWindow::AlertIconType iconType, const String& title, const String& message,
                     ModalComponentManager::Callback* callback, const char* b1, const char* b2, const char* b3)
    {
        ScopedPointer<OSXMessageBox> mb (new OSXMessageBox (iconType, title, message, b1, b2, b3,
                                                            callback, callback != nullptr));
        if (callback == nullptr)
            return mb->getResult();

        mb.release();
        return 0;
    }

private:
    AlertWindow::AlertIconType iconType;
    String title, message;
    ModalComponentManager::Callback* callback;
    const char* button1;
    const char* button2;
    const char* button3;

    void handleAsyncUpdate()
    {
        const int result = getResult();

        if (callback != nullptr)
            callback->modalStateFinished (result);

        delete this;
    }

    static NSString* translateIfNotNull (const char* s)
    {
        return s != nullptr ? juceStringToNS (TRANS (s)) : nil;
    }

    NSInteger getRawResult() const
    {
        NSString* msg = juceStringToNS (message);
        NSString* ttl = juceStringToNS (title);
        NSString* b1  = translateIfNotNull (button1);
        NSString* b2  = translateIfNotNull (button2);
        NSString* b3  = translateIfNotNull (button3);

        switch (iconType)
        {
            case AlertWindow::InfoIcon:     return NSRunInformationalAlertPanel (ttl, msg, b1, b2, b3);
            case AlertWindow::WarningIcon:  return NSRunCriticalAlertPanel      (ttl, msg, b1, b2, b3);
            default:                        return NSRunAlertPanel              (ttl, msg, b1, b2, b3);
        }
    }
};


void JUCE_CALLTYPE NativeMessageBox::showMessageBox (AlertWindow::AlertIconType iconType,
                                                     const String& title, const String& message,
                                                     Component* /*associatedComponent*/)
{
    OSXMessageBox box (iconType, title, message, "OK", nullptr, nullptr, nullptr, false);
    (void) box.getResult();
}

void JUCE_CALLTYPE NativeMessageBox::showMessageBoxAsync (AlertWindow::AlertIconType iconType,
                                                          const String& title, const String& message,
                                                          Component* /*associatedComponent*/)
{
    new OSXMessageBox (iconType, title, message, "OK", nullptr, nullptr, nullptr, true);
}

bool JUCE_CALLTYPE NativeMessageBox::showOkCancelBox (AlertWindow::AlertIconType iconType,
                                                      const String& title, const String& message,
                                                      Component* /*associatedComponent*/,
                                                      ModalComponentManager::Callback* callback)
{
    return OSXMessageBox::show (iconType, title, message, callback, "OK", "Cancel", nullptr) == 1;
}

int JUCE_CALLTYPE NativeMessageBox::showYesNoCancelBox (AlertWindow::AlertIconType iconType,
                                                        const String& title, const String& message,
                                                        Component* /*associatedComponent*/,
                                                        ModalComponentManager::Callback* callback)
{
    return OSXMessageBox::show (iconType, title, message, callback, "Yes", "Cancel", "No");
}


//==============================================================================
bool DragAndDropContainer::performExternalDragDropOfFiles (const StringArray& files, const bool /*canMoveFiles*/)
{
    if (files.size() == 0)
        return false;

    MouseInputSource* draggingSource = Desktop::getInstance().getDraggingMouseSource(0);

    if (draggingSource == nullptr)
    {
        jassertfalse;  // This method must be called in response to a component's mouseDown or mouseDrag event!
        return false;
    }

    Component* sourceComp = draggingSource->getComponentUnderMouse();

    if (sourceComp == nullptr)
    {
        jassertfalse;  // This method must be called in response to a component's mouseDown or mouseDrag event!
        return false;
    }

    JUCE_AUTORELEASEPOOL

    NSView* view = (NSView*) sourceComp->getWindowHandle();

    if (view == nil)
        return false;

    NSPasteboard* pboard = [NSPasteboard pasteboardWithName: NSDragPboard];
    [pboard declareTypes: [NSArray arrayWithObject: NSFilenamesPboardType]
                   owner: nil];

    NSMutableArray* filesArray = [NSMutableArray arrayWithCapacity: 4];
    for (int i = 0; i < files.size(); ++i)
        [filesArray addObject: juceStringToNS (files[i])];

    [pboard setPropertyList: filesArray
                    forType: NSFilenamesPboardType];

    NSPoint dragPosition = [view convertPoint: [[[view window] currentEvent] locationInWindow]
                                     fromView: nil];
    dragPosition.x -= 16;
    dragPosition.y -= 16;

    [view dragImage: [[NSWorkspace sharedWorkspace] iconForFile: juceStringToNS (files[0])]
                 at: dragPosition
             offset: NSMakeSize (0, 0)
              event: [[view window] currentEvent]
         pasteboard: pboard
             source: view
          slideBack: YES];

    return true;
}

bool DragAndDropContainer::performExternalDragDropOfText (const String& /*text*/)
{
    jassertfalse;    // not implemented!
    return false;
}

//==============================================================================
bool Desktop::canUseSemiTransparentWindows() noexcept
{
    return true;
}

Point<int> MouseInputSource::getCurrentMousePosition()
{
    JUCE_AUTORELEASEPOOL
    const NSPoint p ([NSEvent mouseLocation]);
    return Point<int> (roundToInt (p.x), roundToInt ([[[NSScreen screens] objectAtIndex: 0] frame].size.height - p.y));
}

void Desktop::setMousePosition (const Point<int>& newPosition)
{
    // this rubbish needs to be done around the warp call, to avoid causing a
    // bizarre glitch..
    CGAssociateMouseAndMouseCursorPosition (false);
    CGWarpMouseCursorPosition (convertToCGPoint (newPosition));
    CGAssociateMouseAndMouseCursorPosition (true);
}

Desktop::DisplayOrientation Desktop::getCurrentOrientation() const
{
    return upright;
}

//==============================================================================
#ifndef __POWER__  // Some versions of the SDK omit this function..
 extern "C"  { extern OSErr UpdateSystemActivity (UInt8); }
#endif

class ScreenSaverDefeater   : public Timer
{
public:
    ScreenSaverDefeater()
    {
        startTimer (10000);
        timerCallback();
    }

    void timerCallback()
    {
        if (Process::isForegroundProcess())
            UpdateSystemActivity (1 /*UsrActivity*/);
    }
};

static ScopedPointer<ScreenSaverDefeater> screenSaverDefeater;

void Desktop::setScreenSaverEnabled (const bool isEnabled)
{
    if (isEnabled)
        screenSaverDefeater = nullptr;
    else if (screenSaverDefeater == nullptr)
        screenSaverDefeater = new ScreenSaverDefeater();
}

bool Desktop::isScreenSaverEnabled()
{
    return screenSaverDefeater == nullptr;
}

//==============================================================================
class DisplaySettingsChangeCallback  : private DeletedAtShutdown
{
public:
    DisplaySettingsChangeCallback()
    {
        CGDisplayRegisterReconfigurationCallback (displayReconfigurationCallBack, 0);
    }

    ~DisplaySettingsChangeCallback()
    {
        CGDisplayRemoveReconfigurationCallback (displayReconfigurationCallBack, 0);
        clearSingletonInstance();
    }

    static void displayReconfigurationCallBack (CGDirectDisplayID, CGDisplayChangeSummaryFlags, void*)
    {
        const_cast <Desktop::Displays&> (Desktop::getInstance().getDisplays()).refresh();
    }

    juce_DeclareSingleton_SingleThreaded_Minimal (DisplaySettingsChangeCallback);

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (DisplaySettingsChangeCallback)
};

juce_ImplementSingleton_SingleThreaded (DisplaySettingsChangeCallback);

static Rectangle<int> convertDisplayRect (NSRect r, CGFloat mainScreenBottom)
{
    r.origin.y = mainScreenBottom - (r.origin.y + r.size.height);
    return convertToRectInt (r);
}

void Desktop::Displays::findDisplays()
{
    JUCE_AUTORELEASEPOOL

    DisplaySettingsChangeCallback::getInstance();

    NSArray* screens = [NSScreen screens];
    const CGFloat mainScreenBottom = [[screens objectAtIndex: 0] frame].size.height;

    for (unsigned int i = 0; i < [screens count]; ++i)
    {
        NSScreen* s = (NSScreen*) [screens objectAtIndex: i];

        Display d;
        d.userArea  = convertDisplayRect ([s visibleFrame], mainScreenBottom);
        d.totalArea = convertDisplayRect ([s frame], mainScreenBottom);
        d.isMain = (i == 0);

       #if defined (MAC_OS_X_VERSION_10_7) && (MAC_OS_X_VERSION_MAX_ALLOWED >= MAC_OS_X_VERSION_10_7)
        if ([s respondsToSelector: @selector (backingScaleFactor)])
            d.scale = s.backingScaleFactor;
        else
       #endif
            d.scale = 1.0;

        displays.add (d);
    }
}

//==============================================================================
bool juce_areThereAnyAlwaysOnTopWindows()
{
    NSArray* windows = [NSApp windows];

    for (unsigned int i = 0; i < [windows count]; ++i)
    {
        const NSInteger level = [((NSWindow*) [windows objectAtIndex: i]) level];

        if (level == NSFloatingWindowLevel
             || level == NSStatusWindowLevel
             || level == NSModalPanelWindowLevel)
            return true;
    }

    return false;
}

//==============================================================================
Image juce_createIconForFile (const File& file)
{
    JUCE_AUTORELEASEPOOL

    NSImage* image = [[NSWorkspace sharedWorkspace] iconForFile: juceStringToNS (file.getFullPathName())];

    Image result (Image::ARGB, (int) [image size].width, (int) [image size].height, true);

    [NSGraphicsContext saveGraphicsState];
    [NSGraphicsContext setCurrentContext: [NSGraphicsContext graphicsContextWithGraphicsPort: juce_getImageContext (result) flipped: false]];

    [image drawAtPoint: NSMakePoint (0, 0)
              fromRect: NSMakeRect (0, 0, [image size].width, [image size].height)
             operation: NSCompositeSourceOver fraction: 1.0f];

    [[NSGraphicsContext currentContext] flushGraphics];
    [NSGraphicsContext restoreGraphicsState];

    return result;
}

//==============================================================================
void SystemClipboard::copyTextToClipboard (const String& text)
{
    NSPasteboard* pb = [NSPasteboard generalPasteboard];

    [pb declareTypes: [NSArray arrayWithObject: NSStringPboardType]
               owner: nil];

    [pb setString: juceStringToNS (text)
          forType: NSStringPboardType];
}

String SystemClipboard::getTextFromClipboard()
{
    NSString* text = [[NSPasteboard generalPasteboard] stringForType: NSStringPboardType];

    return text == nil ? String::empty
                       : nsStringToJuce (text);
}

void Process::setDockIconVisible (bool isVisible)
{
   #if defined (MAC_OS_X_VERSION_10_6) && (MAC_OS_X_VERSION_MIN_REQUIRED >= MAC_OS_X_VERSION_10_6)
    [NSApp setActivationPolicy: isVisible ? NSApplicationActivationPolicyRegular
                                          : NSApplicationActivationPolicyProhibited];
   #else
    jassertfalse; // sorry, not available in 10.5!
   #endif
}

//added Ryan McGee
void Process::makeProToolsActiveAndShow()
{
    @autoreleasepool
    {
        NSWorkspace *workspace = [NSWorkspace sharedWorkspace];
        NSArray *appArray = [workspace runningApplications];
        
        int numApps = [appArray count];
        for(int i = 0; i < numApps; i++)
        {
            NSRunningApplication *app = [appArray objectAtIndex: i];
            //if([app isActive])
            {
                NSString *appName = [app localizedName];
                
                //NSLog(@"running app name = %@", appName);
                
                if([appName isEqualToString: @"Pro Tools"])
                {
                    //NSLog(@"found pro tools");
                    if(![app isActive])
                    {
                        //NSLog(@"make pro tool active");
                        [app activateWithOptions: NSApplicationActivateIgnoringOtherApps];
                        
                    }
                    if([app isHidden])
                    {
                        //NSLog(@"pro tools hidden, so unhide");
                        [app unhide];
                    }
                    return;
                }
                
                /*//get list of all windows to determine Raven Window ID
                CFArrayRef allWindowList = CGWindowListCopyWindowInfo(kCGWindowListOptionOnScreenOnly, kCGNullWindowID);
                int numWins = CFArrayGetCount(allWindowList);
                for(int j = 0; j < numWins; j++)
                {
                    CFDictionaryRef win = (CFDictionaryRef)CFArrayGetValueAtIndex(allWindowList, j);
                    
                    NSLog(@"%@", win);
                    
                    NSString *winOwnerName = (NSString*)CFDictionaryGetValue(win, kCGWindowOwnerName);
                    CFNumberRef winLayerRef = (CFNumberRef)CFDictionaryGetValue(win, kCGWindowLayer);
                    int winLayer;
                    CFNumberGetValue(winLayerRef, kCFNumberIntType, &winLayer);
                    if( [winOwnerName isEqualToString: appName] && winLayer >= 3) // 3 is PT plug-in level (Raven on 2)
                    {
                        //NSLog(@"active app is %@", winOwnerName);
                        //NSLog(@"%@", win);
                        
                        //[app activateWithOptions: NSApplicationActivateAllWindows];
                    }
                }*/
                
            }
        }
        
    } //end autorelease pool
}

//added Ryan McGee
bool Process::checkOtherAppTouch (int touchX, int touchY)
{
    @autoreleasepool
    {
        
        NSWorkspace *workspace = [NSWorkspace sharedWorkspace];
        NSArray *appArray = [workspace runningApplications];
        
        int numApps = [appArray count];
        for(int i = 0; i < numApps; i++)
        {
            NSRunningApplication *app = [appArray objectAtIndex: i];
            if([app isActive])
            {
                
                NSString *appName = [app localizedName];
                
                //CGWindowID ravenWinID;
                
                //get list of all windows to determine Raven Window ID
                CFArrayRef allWindowList = CGWindowListCopyWindowInfo(kCGWindowListOptionOnScreenOnly, kCGNullWindowID);
                int numWins = CFArrayGetCount(allWindowList);
                for(int j = 0; j < numWins; j++)
                {
                    CFDictionaryRef win = (CFDictionaryRef)CFArrayGetValueAtIndex(allWindowList, j);
                    
                    NSString *winOwnerName = (NSString*)CFDictionaryGetValue(win, kCGWindowOwnerName);
                    CFNumberRef winLayerRef = (CFNumberRef)CFDictionaryGetValue(win, kCGWindowLayer);
                    int winLayer;
                    CFNumberGetValue(winLayerRef, kCFNumberIntType, &winLayer);
                    if( [winOwnerName isEqualToString: appName] && winLayer >= 3) // 3 is PT plug-in level (Raven on 2)
                    {
                        
                        CFDictionaryRef boundsDictionary = (CFDictionaryRef)CFDictionaryGetValue(win, kCGWindowBounds);
                        
                        CGRect boundsRect;
                        CGRectMakeWithDictionaryRepresentation(boundsDictionary, &boundsRect);
                        
                        int x1 = boundsRect.origin.x;
                        int y1 = boundsRect.origin.y;
                        int x2 = x1 + boundsRect.size.width;
                        int y2 = y1 + boundsRect.size.height;
                        
                        //NSLog(@"%@", win);
                        
                        if(touchX >= x1 && touchX <= x2 && touchY >= y1 && touchY <= y2)
                        {                            
                            //[app unhide]; // do we need this??
                            return true;
                        }
                        else return false;
                        
                        //ravenWinID = (CGWindowID)CFDictionaryGetValue(win, kCGWindowNumber);
                        //NSLog(@"active app is %@", winOwnerName);
                        
                        
                        //[app activateWithOptions: NSApplicationActivateAllWindows];
                    }
                }
                
                /*
                 CFArrayRef aboveRavenWindowList = CGWindowListCopyWindowInfo(kCGWindowListOptionOnScreenBelowWindow, ravenWinID);
                 int numWinsAbove = CFArrayGetCount(aboveRavenWindowList);
                 if (numWinsAbove == 0) NSLog(@"Nothing Above Raven");
                 for(int j = 0; j < numWinsAbove; j++)
                 {
                 CFDictionaryRef win = (CFDictionaryRef)CFArrayGetValueAtIndex(aboveRavenWindowList, j);
                 NSString *winOwnerName = (NSString*)CFDictionaryGetValue(win, kCGWindowOwnerName);
                 NSLog(@"%@ is above Raven", winOwnerName);
                 }
                 */
                
            }
        }
        
        return false;
    } //end autorelease pool
}

//added Ryan McGee
bool Process::checkPlugInMoveTouch (int touchX, int touchY)
{
    @autoreleasepool
    {
        
        NSWorkspace *workspace = [NSWorkspace sharedWorkspace];
        NSArray *appArray = [workspace runningApplications];
        
        int numApps = [appArray count];
        for(int i = 0; i < numApps; i++)
        {
            NSRunningApplication *app = [appArray objectAtIndex: i];
            if([app isActive])
            {
                
                NSString *appName = [app localizedName];
                CFArrayRef allWindowList = CGWindowListCopyWindowInfo(kCGWindowListOptionOnScreenOnly, kCGNullWindowID);
                int numWins = CFArrayGetCount(allWindowList);
                for(int j = 0; j < numWins; j++)
                {
                    CFDictionaryRef win = (CFDictionaryRef)CFArrayGetValueAtIndex(allWindowList, j);
                    
                    NSString *winOwnerName = (NSString*)CFDictionaryGetValue(win, kCGWindowOwnerName);
                    CFNumberRef winLayerRef = (CFNumberRef)CFDictionaryGetValue(win, kCGWindowLayer);
                    int winLayer;
                    CFNumberGetValue(winLayerRef, kCFNumberIntType, &winLayer);
                    if( [winOwnerName isEqualToString: appName] && winLayer >= 3) // 3 is PT plug-in level (Raven on 2)
                    {
                        CFDictionaryRef boundsDictionary = (CFDictionaryRef)CFDictionaryGetValue(win, kCGWindowBounds);
                        CGRect boundsRect;
                        CGRectMakeWithDictionaryRepresentation(boundsDictionary, &boundsRect);
                        
                        int x1 = boundsRect.origin.x;
                        int y1 = boundsRect.origin.y;
                        int x2 = x1 + boundsRect.size.width;
                        
                        int yGrabMin = y1;
                        int yGrabMax = y1 + 32;
                        int cursorGrabY = y1 + 5;
                        
                        if(touchX >= x1 && touchX <= x2 && touchY >= yGrabMin && touchY <= yGrabMax)
                        {                            
                            CGDisplayMoveCursorToPoint(1, CGPointMake(touchX, cursorGrabY)); // 2 pixels down from top bar to ensure move grab
                            //Mouse Down at that position
                            CGEventSourceRef src = CGEventSourceCreate(kCGEventSourceStateHIDSystemState);
                            CGEventRef mouseDown = CGEventCreateMouseEvent(src, kCGEventLeftMouseDown, CGPointMake(touchX,cursorGrabY), kCGMouseButtonLeft);
                            
                            CGEventPost(kCGHIDEventTap, mouseDown);
                            return true;
                        }
                        else return false;
                    }
                }
            }
        }
        return false;
    } //end autorelease pool
    
}

//added Ryan McGee
void Process::releasePlugInMoveTouch (int touchX, int touchY)
{
    CGEventSourceRef src = CGEventSourceCreate(kCGEventSourceStateHIDSystemState);
    CGEventRef mouseUp = CGEventCreateMouseEvent(src, kCGEventLeftMouseUp, CGPointMake(touchX, touchY), kCGMouseButtonLeft);
    CGEventPost(kCGHIDEventTap, mouseUp);
}
