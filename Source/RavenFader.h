
#ifndef Raven_RavenFader_h
#define Raven_RavenFader_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "NscGuiElement.h"
#include "RavenLabel.h"
#include "RavenGUI.h"
#include "RavenMessage.h"

const float fader0dbVal = 0.754098;
const int numFaderDBVals = 10;
const float faderDBvals[numFaderDBVals] = { 1.000000,       //12dB
                                            0.881967,       //6dB
                                            fader0dbVal,    //0dB
                                            0.632787,       //-5dB
                                            0.511475,       //-10dB
                                            0.390164,       //-15dB
                                            0.281967,       //-20dB
                                            0.190164,       //-30dB
                                            0.085246,       //-60dB
                                            0.036066 };     //-90dB

const float faderDBs[numFaderDBVals]   =  { 12.00,      //12dB
                                            6.00,       //6dB
                                            0.00,       //0dB
                                            -5.00,      //-5dB
                                            -10.00,     //-10dB
                                            -15.00,     //-15dB
                                            -20.00,     //-20dB
                                            -30.00,     //-30dB
                                            -60.00,     //-60dB
                                            -90.00 };   //-90dB

class RavenFader : public Slider, public NscGuiElement
{
public:
    RavenFader(NscGuiContainer *owner, int trackLength);
    ~RavenFader();
    
    void paint (Graphics& g);
    void repaint (Graphics& g);
    
    // NscGuiElement overrides
	virtual void updateNscValue(float value);			// callback when DAW changes a value; generally
    virtual void updateNscValue(const String & value);			// callback when DAW changes a value; generally
    
    void updateTouchValue(float value);
    
    void updateNSCThread();
    void updateTouchThread();
    
    int getCapCenterScreenX();
    int getCapCenterScreenY();
    int getTrackTopScreenY();
    int getTrackBottomScreenY();
    int getTrackLength(){return mTrackLength;}
    
    bool isBeingTouched;
    void setDBLabel(RavenLabel *label){dBLabel = label;}
    float getDBVal();
    
    virtual void setHybridMode(bool hybridMode){}
    
protected:
	Image faderImage;
    Image faderCapImg;    
    int mTrackLength;
    RavenLabel *dBLabel;
    bool isSend;
    
private:
    long int updateCount;
    float newNSCValue, newTouchValue;
    bool needsNSCUpdating, needsTouchUpdating;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenFader)
    
};

class RavenMixerFader : public RavenFader
{
public:
    RavenMixerFader(NscGuiContainer *owner, int trackLength, int bank, int chan);
    void setHybridMode(bool hybridMode);
    
private:
    int nonHybridModeTrackLength;
};

class RavenSendFader : public RavenFader
{
public:
    RavenSendFader(NscGuiContainer *owner, int trackLength, int bank, int chan);
};

#endif
