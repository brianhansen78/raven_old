/*
 
 Calling hideMouse() will force a background application to hide its cursor.  Its operation is not understood completely,
 but it seems to work. The solution was found on Stack Overflow:
 http://stackoverflow.com/questions/7004804/hide-mouse-pointer-or-block-movement-in-os-x
 
 */
 
#ifndef Raven_MacHideBackgroundMouse_h
#define Raven_MacHideBackgroundMouse_h

class MacHideBackgroundMouse
{
public:
    static void hideMouse();
};

#endif
