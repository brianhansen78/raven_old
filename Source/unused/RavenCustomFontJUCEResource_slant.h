/* (Auto-generated binary data file). */

#ifndef BINARY_RAVENCUSTOMFONTJUCERESOURCE_H
#define BINARY_RAVENCUSTOMFONTJUCERESOURCE_H

namespace RavenCustomFontJUCEResource
{
    extern const char*  ravenlcdfont;
    const int           ravenlcdfontSize = 8086;

}

#endif
