

#ifndef Raven_NewSDK_RavenInsertsBankComponent_h
#define Raven_NewSDK_RavenInsertsBankComponent_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenInsertsChannelComponent.h"
#include "NscGuiElement.h"

class RavenInsertsBankComponent : public Component, public NscGuiContainer, public Button::Listener
{
public:
    RavenInsertsBankComponent(int numChans, int bankNum) :
    NscGuiContainer(bankNum),
    mBankNum(bankNum)
    {
        setSize (0, 0);
        int xOffset = 0;
        
        for(int i = 0; i < numChans; i++)
        {
            RavenInsertsChannelComponent *insertChannel = new RavenInsertsChannelComponent(this, bankNum, i);
            insertChannel->setTopLeftPosition(xOffset, 0);
            insertChannel->setChannelAndBankIndex(i, bankNum);
            insertChannel->addButtonListener(this);
            addAndMakeVisible(insertChannel);
            inserts.add(insertChannel);

            xOffset += insertChannel->getWidth();
            //setSize(xOffset, insertChannel->getHeight());
            setSize(xOffset, 20);
        }
            
    }
    
    // Button::Listener interface
	void	buttonStateChanged (Button*){}
	void	buttonClicked (Button* b)
    {
        NscGuiElement * element = dynamic_cast<NscGuiElement *>(b);
        if (!element)
        {
            // try our temporary workaround hack.
            int ptr = b->getProperties()[kNscGuiIdentifier];
            element = (NscGuiElement *) ptr;
            if (!element)
            {
                DBG("Switches must inherit and set up NscGuiElement");
                jassertfalse;
            }
        }
        if (element)
        {
            switchClicked(element, b->getToggleState());
        }
    }
        
    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, float val)
    {
        if (bank == eMatchesAllBanks || bank == mBankNum) this->callback(type, subType, bank, which, val);
    }
    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, const char * text)
    {
        if (bank == eMatchesAllBanks || bank == mBankNum) this->callback(type, subType, bank, which, text);
    }
    
    RavenInsertsChannelComponent* getChannel(int num){return inserts[num];}

    void setMixerButtonListener(Button::Listener *listener)
    {
        for (int i=0; i< inserts.size(); i++) inserts[i]->setMixerButtonListener(listener);
    }
    
private:
    OwnedArray<RavenInsertsChannelComponent> inserts;
    int mBankNum;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenInsertsBankComponent)
};


#endif
