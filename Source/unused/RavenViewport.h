
#ifndef Raven_RavenViewport_h
#define Raven_RavenViewport_h

#include "../JuceLibraryCode/JuceHeader.h"

class RavenViewport : public Viewport
{
public:
    RavenViewport(Component *viewedComponent)
    {
        setViewedComponent(viewedComponent);
        setViewPosition(0, 0);
        setScrollBarsShown(false, false);
    }
    ~RavenViewport(){deleteAllChildren();}
    
};

#endif
