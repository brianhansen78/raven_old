
#include "MouseCatchWindow.h"
//==============================================================================
MouseCatchWindow::MouseCatchWindow()
: DocumentWindow (JUCEApplication::getInstance()->getApplicationName(),
                  Colours::black,
                  DocumentWindow::allButtons)
{
    MouseHideComponent *mouseHider = new MouseHideComponent(1920, 1080);
    setContentOwned(mouseHider, true);
    
    setUsingNativeTitleBar(false);
    
    setVisible (true);
    
    setTitleBarHeight(0);
    setTopLeftPosition(0,0);
    
    setAlwaysOnTop(true);
    
}

MouseCatchWindow::~MouseCatchWindow()
{

}

void MouseCatchWindow::closeButtonPressed()
{
    JUCEApplication::getInstance()->systemRequestedQuit();
}