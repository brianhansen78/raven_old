

#ifndef __JUCER_HEADER_RAVENINSERTSCOMPONENT_RAVENSENDSCOMPONENT_2F05906E__
#define __JUCER_HEADER_RAVENINSERTSCOMPONENT_RAVENSENDSCOMPONENT_2F05906E__

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenInsertsBankComponent.h"
#include "RavenRackComponent.h"

class RavenInsertsComponent : public RavenRackComponent
{
public:
    RavenInsertsComponent (int numBanks, int chansPerBank, int rackPos) : RavenRackComponent(rackPos), mNumBanks(0), mChansPerBank(0)
    {
        
        setSize(1920, 20);
        
        int xOffset = INSERTS_X_OFFSET;
        for(int i = 0; i < numBanks; i++)
        {
            RavenInsertsBankComponent *bank = new RavenInsertsBankComponent(chansPerBank, i);
            addAndMakeVisible(bank);
            bank->setTopLeftPosition(xOffset, 0);
            xOffset += bank->getWidth();
            banks.add(bank);
        }
        
        mNumBanks = numBanks;
        mChansPerBank = chansPerBank;
        
        insertsPanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "MIXERGRAPHICS/INSERTSV4/INSERTPANEL/INSERT-PANEL-1to5.png"));
        ////draw 4 pix before mixer channels
        Rectangle<int>clipRectL(0, 0, INSERTS_X_OFFSET, insertsPanelImg.getHeight());
        insertsPanelImgL = insertsPanelImg.getClippedImage(clipRectL);
        Rectangle<int>clipRectR(1900, 0, 20, insertsPanelImg.getHeight());
        insertsPanelImgR = insertsPanelImg.getClippedImage(clipRectR);
        ////
        
        //setPaintingIsUnclipped(true);
        //setBufferedToImage(false);
        
    }
    
    void setNscLink	(class NscLink * link_)
    {
        mLink = link_;
        for(int i = 0; i < banks.size(); i++) banks[i]->setNscLink(link_);
    }
    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, float val)
    {        
        for (int i=0; i<banks.size();++i)
        {
            if (bank == eMatchesAllBanks || bank == i)
                banks[i]->nscCallback(type, subType, bank, which, val);
        }
    }
    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, const char * text)
    {               
        //printf("callback received!, text = %s, bank = %d, which = %d\n", text, bank, which);
        for (int i=0; i<banks.size();++i)
        {
            if (bank == eMatchesAllBanks || bank == i)
                banks[i]->nscCallback(type, subType, bank, which, text);
        }
    }
    
    int getNumBanks(){return mNumBanks;}
    int getChansPerBank(){return mChansPerBank;}
    
    RavenInsertsBankComponent* getBank(int num){return banks[num];}
    
    void setMixerButtonListener(Button::Listener *listener)
    {
        for (int i=0; i<banks.size();++i) banks[i]->setMixerButtonListener(listener);
    }
    
    //*
    void paint(Graphics &g)
    {
        //printf("painting inserts panel\n");
        //g.drawImageAt(insertsPanelImgL, 0, 0);
        //g.drawImageAt(insertsPanelImgR, 1900, 0);
    } 
    //*/
    
    
private:    
    
    int mNumBanks, mChansPerBank;
    OwnedArray<RavenInsertsBankComponent> banks;
    
    Image insertsPanelImg, insertsPanelImgL, insertsPanelImgR;
    
    NscLink *mLink;

};


#endif   
