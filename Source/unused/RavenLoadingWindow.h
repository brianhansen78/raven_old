
#ifndef Raven_RavenLoadingWindow_h
#define Raven_RavenLoadingWindow_h

#include "../JuceLibraryCode/JuceHeader.h"


class RavenLoadingComponent : public Component
{
public:
    RavenLoadingComponent()
    {
        setSize(400, 300);
        
        Label *titleLabel = new Label (String("title label"), String("Raven"));
        titleLabel->setFont (Font (22.3000f, Font::plain));
        titleLabel->setJustificationType (Justification::centred);
        titleLabel->setEditable (false, false, false);
        titleLabel->setColour (Label::textColourId, Colours::white);
        titleLabel->setColour (TextEditor::textColourId, Colours::black);
        titleLabel->setColour (TextEditor::backgroundColourId, Colours::black);
        titleLabel->setBounds(150, 125, 100, 50);
        addAndMakeVisible(titleLabel);
        
        statusLabel = new Label (String("status label"), String("connecting to Ney-Fi..."));
        statusLabel->setFont (Font (14.000f, Font::plain));
        statusLabel->setJustificationType (Justification::left);
        statusLabel->setEditable (false, false, false);
        statusLabel->setColour (Label::textColourId, Colours::white);
        statusLabel->setColour (TextEditor::textColourId, Colours::black);
        statusLabel->setColour (TextEditor::backgroundColourId, Colours::black);
        statusLabel->setBounds(0, 250, 400, 50);
        addAndMakeVisible(statusLabel);
        
        //setAlwaysOnTop(true);
    }
    ~RavenLoadingComponent()
    {
        deleteAllChildren();
    }
    void paint(Graphics &g)
    {
        g.fillAll(Colours::black);
    }
    
    void updateStatusLabel(const String &text)
    {
        const MessageManagerLock lock;
        if(lock.lockWasGained()) statusLabel->setText(text, false);
    }
    
private:
    Label *statusLabel;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(RavenLoadingComponent)
};

class RavenLoadingWindow   : public DocumentWindow
{
public:
    //==============================================================================
    RavenLoadingWindow() : DocumentWindow (JUCEApplication::getInstance()->getApplicationName(),
                                           Colours::black,
                                           DocumentWindow::allButtons)
    {
        
            centreWithSize (400, 300);
        
            loadCmp = new RavenLoadingComponent();
            setContentOwned(loadCmp, false);
            
            setUsingNativeTitleBar(false);
        
            setTitleBarHeight(0);
            
            setAlwaysOnTop(true);
        
            setVisible (true);
            
            //this prevents the OS Alert Sounds with multi-touch
            enterModalState(false); //take keyboard focus = false
   
    }
    ~RavenLoadingWindow()
    {
        delete loadCmp;
    }
    
    void updateStatusLabel(const String &text)
    {
        loadCmp->updateStatusLabel(text);
    }
    
private:
    
    RavenLoadingComponent *loadCmp;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenLoadingWindow)
};

#endif
