

#ifndef Raven_NewSDK_RavenInsertsChannelComponent_h
#define Raven_NewSDK_RavenInsertsChannelComponent_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenButton.h"
#include "RavenTouchComponent.h"
#include "RavenLabel.h"
#include "NscGuiElement.h"

class RavenInsertsChannelComponent : public RavenTouchComponent, public NscGuiElement, Button::Listener
{
public:
    RavenInsertsChannelComponent (NscGuiContainer *owner, int bank, int chan) : NscGuiElement(owner)
    {
        
        int yOffset = 0;

        RavenButton *insertBypassButton = new RavenButton();
        insertBypassButton->setClickingTogglesState(true);
        insertBypassButton->setImagePaths(RavenGUI::getInsertButtonPath("bypass", true, bank, chan, 0), RavenGUI::getInsertButtonPath("bypass", false, bank, chan, 0));
        insertBypassButton->setTopLeftPosition(0, yOffset);
        //addAndMakeVisible(insertBypassButton);
        //ravenTouchButtons.add(insertBypassButton);
        
                    
        insertButton = new RavenButton();
        insertButton->setName("insertButton" + String(bank) + "_" + String(chan));
        insertButton->setImagePaths(RavenGUI::getInsertButtonPath("display", true, bank, chan, 0), RavenGUI::getInsertButtonPath("display", false, bank, chan, 0));
        insertButton->setTopLeftPosition(0, yOffset);
        addAndMakeVisible(insertButton);
        ravenTouchButtons.add(insertButton);
        
        //int chanNum = (bank*8)+(chan+1); //1 to 24
        RavenLabel *insLabel = new RavenLabel();
        insLabel->setBounds(2, yOffset+2, 45, 20);
        insLabel->setText("INSERT", false);
        addAndMakeVisible(insLabel);
        
        yOffset += insertButton->getHeight();
        
        setSize (insertBypassButton->getWidth() + insertButton->getWidth(), getHeight() + yOffset);
        
        
        static int panelOffset = INSERTS_X_OFFSET;
        insertsPanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "MIXERGRAPHICS/INSERTSV4/INSERTPANEL/INSERT-PANEL-1to5.png"));
        Rectangle<int>clipRect;
        clipRect = Rectangle<int>(panelOffset, 0, getWidth(), insertsPanelImg.getHeight());
        insertsPanelImg = insertsPanelImg.getClippedImage(clipRect);
        panelOffset += insertsPanelImg.getWidth();
                
    }
    
    ~RavenInsertsChannelComponent()
    {
        deleteAllChildren();
    }
    
    void	buttonStateChanged (Button* b)
    {
        //lbl->setTopLeftPosition(lbl->getX(), lbl->getY()+2);
    }
	void	buttonClicked (Button* b)
    {
        //lbl->setTopLeftPosition(lbl->getX(), lbl->getY()-2);
    }
    
    void setChannelAndBankIndex(int chan, int bank)
    {
        //insertButton->setChannelIndex(chan);
        //insertButton->setBankIndex(bank);
    }
    
    void addButtonListener(Button::Listener * listener)
    {
        for(int i = 0; i < ravenNSCTouchButtons.size(); i++) ravenNSCTouchButtons[i]->addListener(listener);
    }
    
    void setMixerButtonListener(Button::Listener * listener)
    {
        insertButton->addListener(listener);
    }
    
private:
    
    Image insertsPanelImg;

    RavenButton *insertButton;

};
 


#endif
