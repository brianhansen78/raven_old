//
// This window is invisible and exists behind our main app window.  For the Raven, the main app window is set to ignore mouse events in setKioskModeComponent,
// so this window prevents the mouse events from being passed all the way through to ProTools.  This window should contain the MouseHide Component with cursor
// set to no cursor.
//

#ifndef Raven_MouseCatchWindow_h
#define Raven_MouseCatchWindow_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "MouseHideComponent.h"

class MouseCatchWindow  : public DocumentWindow
{
public:
    //==============================================================================
    MouseCatchWindow();
    ~MouseCatchWindow();
    
    void closeButtonPressed();
    
    /* Note: Be careful when overriding DocumentWindow methods - the base class
     uses a lot of them, so by overriding you might break its functionality.
     It's best to do all your work in you content component instead, but if
     you really have to override any DocumentWindow methods, make sure your
     implementation calls the superclass's method.
     */
    
private:
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MouseCatchWindow)
};


#endif
