
#include "RavenMixerBankComponent.h"


//==============================================================================
RavenMixerBankComponent::RavenMixerBankComponent (int numChans, int bankNumber) : NscGuiContainer(bankNumber), isInPanMode(false)
{

    setSize (0, 0);
    int xOffset = 0;
    for(int i = 0; i < numChans; i++)
    {        
        RavenMixerChannelComponent *channel = new RavenMixerChannelComponent(this, bankNumber, i);
        addAndMakeVisible(channel);
        channel->setTopLeftPosition(xOffset, 0);
        channel->setChannelAndBankIndex(i, bankNum);
        channel->addSliderListener(this);
        channel->addButtonListener(this);
        channels.add(channel);
                
        xOffset += channel->getWidth();
        setSize(xOffset, channel->getHeight());
        
        lastSelectTimes[i] = 0;
    }
}

RavenMixerBankComponent::~RavenMixerBankComponent()
{

}

void RavenMixerBankComponent::sliderValueChanged (Slider *slider)
{

	NscGuiElement * element = dynamic_cast<NscGuiElement *>(slider);
	if (!element)
	{
		DBG("Faders and vpots must inherit and set up NscGuiElement");
		jassertfalse;
	}
	else
		sliderChanged(element, (float) slider->getValue());
}

void RavenMixerBankComponent::sliderDragStarted (Slider *slider)
{
	NscGuiElement * element = dynamic_cast<NscGuiElement *>(slider);
	if (!element)
	{
		DBG("Faders and vpots must inherit and set up NscGuiElement");
		jassertfalse;
	}
	else
		sliderBegan(element);
}
void RavenMixerBankComponent::sliderDragEnded (Slider *slider)
{
	NscGuiElement * element = dynamic_cast<NscGuiElement *>(slider);
	if (!element)
	{
		DBG("Faders and vpots must inherit and set up NscGuiElement");
		jassertfalse;
	}
	else
		this->sliderEnded(element);
}

void RavenMixerBankComponent::buttonStateChanged (Button* b)
{
    
}

void RavenMixerBankComponent::buttonClicked (Button* b)
{
    
	NscGuiElement * element = dynamic_cast<NscGuiElement *>(b);
	if (!element)
	{
		// try our temporary workaround hack.
		int ptr = b->getProperties()[kNscGuiIdentifier];
		element = (NscGuiElement *) ptr;
		if (!element)
		{
			DBG("Switches must inherit and set up NscGuiElement");
			jassertfalse;
		}
	}
	if (element)
	{
        int tag = element->getTag();
        //int channel = element->getChannelIndex();
        if(tag == eNSCTrackSelectSwitch)
        {
            //This should all be handled in canTriggerTrackSelect() now: 
//            if(Time::getCurrentTime().toMilliseconds() - lastSelectTimes[channel] < /*1000*/huiWaitMs)
//            {
//                printf("too fast\n");
//                return; //dont allow double click on track select
//            }
//            lastSelectTimes[channel] = Time::getCurrentTime().toMilliseconds();
        }
        
		switchClicked(element, b->getToggleState());
        
	}
}