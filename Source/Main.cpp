
#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenContainer.h"
#include "RavenLoadingWindow.h"

//==============================================================================
class RavenApplication  : public JUCEApplication
{
public:
    //==============================================================================
    RavenApplication()
    {
    }

    ~RavenApplication()
    {
    }

    //==============================================================================
    void initialise (const String& commandLine)
    {
        RavenLoadingComponent *loadCmp = new RavenLoadingComponent();
        container = new RavenContainer(loadCmp);
    }

    void shutdown()
    {
        container = 0;
    }

    //==============================================================================
    void systemRequestedQuit()
    {
        quit();
    }

    //==============================================================================
    const String getApplicationName()
    {
        return "Raven";
    }

    const String getApplicationVersion()
    {
        return ProjectInfo::versionString;
    }

    bool moreThanOneInstanceAllowed()
    {
        return false;
    }

    void anotherInstanceStarted (const String& commandLine)
    {
        
    }

private:
    ScopedPointer<RavenContainer> container;
};



//==============================================================================
// This macro generates the main() routine that starts the app.
START_JUCE_APPLICATION(RavenApplication)
