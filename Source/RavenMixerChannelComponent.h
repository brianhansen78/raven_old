#ifndef __JUCER_HEADER_RAVENMIXERCHANNELCOMPONENT_RAVENMIXERCHANNELCOMPONENT_E957F1__
#define __JUCER_HEADER_RAVENMIXERCHANNELCOMPONENT_RAVENMIXERCHANNELCOMPONENT_E957F1__

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenTextDisplay.h"
#include "RavenTouchComponent.h"
#include "RavenMeter.h"
#include "RavenAutomationComponent.h"
#include "time.h"

class RavenMixerChannelComponent  : public RavenTouchComponent, public NscGuiElement, public Slider::Listener, public Button::Listener, public ActionBroadcaster
{
public:
    RavenMixerChannelComponent (NscGuiContainer *owner, int bank, int chan);
    ~RavenMixerChannelComponent();
    
    //used for updating non NSC based pan labels
    void 	sliderValueChanged (Slider *slider);
    void 	sliderDragStarted (Slider *slider);
    void 	sliderDragEnded (Slider *slider);

    void paint (Graphics &g);
    
    void setChannelAndBankIndex(int chan, int bank)
    {
        
        mBank = bank;
        mChan = chan;
        
        fader->setChannelIndex(chan);
        fader->setBankIndex(bank);
        
        knob->setChannelIndex(chan);
        knob->setBankIndex(bank);
        
        meterL->setChannelIndex(chan);
        meterL->setBankIndex(bank);
        meterL->setClipPeakChannelBankIndex(chan, bank);
        
        meterR->setChannelIndex(chan);
        meterR->setBankIndex(bank);
        meterR->setClipPeakChannelBankIndex(chan, bank);
    
        setChannelIndex(chan);
        setBankIndex(bank);
        
        for(int i = 0; i < ravenNSCTouchButtons.size(); i++)
        {
            ravenNSCTouchButtons[i]->setChannelIndex(chan);
            ravenNSCTouchButtons[i]->setBankIndex(bank);
        }
        
        autoSelectButton->setChannelIndex(chan);
        autoSelectButton->setBankIndex(bank);
        
        insButtonNSC->setChannelIndex(chan);
        insButtonNSC->setBankIndex(bank);
        
        selectButton->setChannelIndex(chan);
        selectButton->setBankIndex(bank);
        
        //panModeButton->setChannelIndex(kNscGuiBankGlobalIdentifier);

    }
    
    void addSliderListener(Slider::Listener * listener)
    {   
        fader->addListener(listener);
        knob->addListener(listener);
    }
    void addButtonListener(Button::Listener * listener)
    {
        for(int i = 0; i < ravenNSCTouchButtons.size(); i++) ravenNSCTouchButtons[i]->addListener(listener);
        
        autoSelectButton->addListener(listener);
        
        //panModeButton->addListener(listener);
        
        insButtonNSC->addListener(listener);
        
        selectButton->addListener(listener);
    }
    
    void setRackContainerButtonListener(Button::Listener * listener)
    {
        insButton->addListener(listener);
        insButtonNSC->addListener(listener);
    }
    
    void toggleInsertButtonOff()
    {
        insButton->setToggleState(false, false); // toggle off with no notificaition
    }
    
    void beginFader();
    void endFader();
    
    void adjustKnobXOffset(int bgScreenX)
    {
        int diff = bgScreenX - getScreenX();
        int newX = 2*diff+1 + 2*(knob->getWidth()-44);
        knob->setTopLeftPosition(newX, knob->getY());
    }
    
    void buttonStateChanged (Button* b);
    void buttonClicked (Button* b);
    
    void setNscLink	(class NscLink * link_) {link = link_; knob->setNscLink(link_);}
    
    void zeroFader()
    {
        link->set(eNSCTrackEvent, eNSCTrackFaderSwitch, mBank, mChan, 1);
        link->set(eNSCTrackEvent, eNSCTrackFaderValue , mBank, mChan, fader0dbVal);
        link->set(eNSCTrackEvent, eNSCTrackFaderSwitch, mBank, mChan, 0.0);
        
//        RavenTouchMessage *m = new RavenTouchMessage(eUpdateFader, fader0dbVal);
//        postMessage(m);
    }
    void zeroKnob()
    {
        knob->setValue(0.5, sendNotification);
    }
    
    void enterFlipMode();
    void exitFlipMode();
    void flipAlpha(bool on);
    
    bool getAutoCompVisible(){return autoComp->isVisible();}
    void setAutomationImage(const String &message, bool needToHideAutomationComponent);
    
    RavenNSCButton *getTrackSelectButton() { return selectButton; }
    
    bool canTriggerTrackSelect()
    {
        //printf("track select diff, %lld\n", Time::getCurrentTime().toMilliseconds() - lastSelectTime.toMilliseconds());
        if(Time::getCurrentTime().toMilliseconds() - lastSelectTime.toMilliseconds() > huiTrackSelectWaitMs)
            return true;
        return false;
    }
    
    void triggerClickTrackSelectAndResetTime()
    {
        selectButton->triggerClick();
        lastSelectTime = Time::getCurrentTime();
    }
    
    void triggerClickTrackSelectAndResetTime(bool _selectTrack)
    {
        selectButton->triggerClick();
        lastSelectTime = Time::getCurrentTime();
        selectTrack(_selectTrack);
    }
    
    void selectTrack(bool select)
    {     
        trackSelected = select;
        LOCK_JUCE_THREAD repaint();
    }
    
    bool checkTrackSelectTouch(int touchX, int touchY);
    
    bool isSelected() { return trackSelected; }
    
    void setBlankMaster(bool _blank);
    void setBlank(bool _blank);
    bool isBlank()
    {
        return blank;
    }
    
    RavenFuturaTextDisplay *getTrackNameLabel(){ return trackName;}
    Label *getDBLabel() { return dBLabel; }
    Button *getAutoModeButton() { return autoModeButton; }
    Button *getRecButton() { return recButton; }
    Button *getSoloButton() { return soloButton; }
    Button *getMuteButton() { return muteButton; }
    
    bool isAutoModeSelected() { return autoComp->isVisible();}
    
    void setTouchRecordEnabled(bool enable) {touchRecordEnabled = enable;}
    bool isTouchRecordEnabled() {return touchRecordEnabled; }
    
    void setTouchSoloEnabled(bool enable) {touchSoloEnabled = enable;}
    bool isTouchSoloEnabled() {return touchSoloEnabled; }
    
    void setTouchMuteEnabled(bool enable) {touchMuteEnabled = enable;}
    bool isTouchMuteEnabled() {return touchMuteEnabled; }

    void resetTouchEnablers()
    {
        touchRecordEnabled = false;
        touchSoloEnabled = false;
        touchMuteEnabled = false;
        
        // add mute
        // add solo
        // automation
    }
    
    void setBlankFrames(int _blankFrames)
    {
        blankFrames = _blankFrames;
    }
    
    void checkBlankFrames()
    {
        blankFrames++;
        if(blankFrames >= RAVEN_FRAMES_UNTIL_BLANK)
            setBlank(true);
    }
    
    void setPanMode(bool _panMode)
    {
        panMode = _panMode;
    }
    
    bool isKnobBlank(){return knob->isBlank();}
    void setKnobBlank(bool _blank);
    
    void setFineFaderImage(bool ff);
    
    void setFineFader(bool ff)
    {
        fineFader = ff;
        setFineFaderImage(ff);
        LOCK_JUCE_THREAD repaint();
    }
    
    void visibilityChanged()
    {
        for(int i = 0;i< getNumChildComponents();i++)
        {
            //getChildComponent(i)->setVisible(isVisible());
        }
    }
    
    bool autoCompEmptyTouch(TUIO::TuioCursor *tcur)
    {
        autoComp->checkButtonTouch(tcur);
        return (autoComp->isVisible() && componentTouched(autoComp, tcur, false));
    }
    
    bool checkMeterClipTouch(int touchX, int touchY)
    {
        return (meterL->checkMeterClipTouch(touchX, touchY) || meterR->checkMeterClipTouch(touchX, touchY));
    }
    
    bool checkSelectButtonTouch(TUIO::TuioCursor *tcur);
    
    RavenMeter *getMeterL() { return meterL; }
    RavenMeter *getMeterR() { return meterR; }
    
    String getAutomationString()
    {
        return lastAutomationString;
    }
    
    void enterHybridMode(int _numberOfHybridModeBanks);
    
    void exitHybridMode();

    void setHybridPanMode(bool _hybridPanMode)
    {
        knob->setHidden(!_hybridPanMode);
        setBlank(blank);
    }
    
    void unClipMeter()
    {
        meterL->setClipped(false);
        meterR->setClipped(false);
    }
    
    bool isMeterLClipped(){ return meterL->isClipped();}
    bool isMeterRClipped(){ return meterR->isClipped();}
    
    void setMeterLClipped(bool _clipped){ meterL->setClipped(_clipped);}
    void setMeterRClipped(bool _clipped){ meterR->setClipped(_clipped);}
    
    void syncInternalTrackSelectWithHUI()
    {
        if(trackSelected != selectButton->getToggleState())
        {
            printf("correcting track select\n");
            trackSelected = selectButton->getToggleState();
            repaint();
        }
        
    }
    
private:
        
    NscGuiContainer *mOwner;

    ScopedPointer<RavenMeter> meterL;
    ScopedPointer<RavenMeter> meterR;
    ScopedPointer<RavenLabel> panLLabel;
    ScopedPointer<RavenLabel> panRLabel;
    ScopedPointer<RavenLabel> dBLabel;
    
    RavenButton *autoModeButton;
    ScopedPointer<RavenNSCButton> autoSelectButton; //invisible and not added to owned array of touch buttons, so use scoped
    ScopedPointer<RavenAutomationComponent> autoComp;
    
    RavenButton *insButton;
    ScopedPointer<RavenNSCButton> insButtonNSC;  //invisible
    RavenNSCButton *muteButton;
    RavenNSCButton *recButton;
    ScopedPointer<RavenNSCButton> selectButton;
    RavenNSCButton *soloButton;
    
    //ScopedPointer<RavenNSCButton> panModeButton; // be sure not to add this to touch button array if it's scoped
    //RavenNSCButton *panModeButton; // be sure not to add this to touch button array if it's scoped
    
    Image mixerPanelImg, faderTrackImg, panLevelDisplayImg, levelDisplayImg, nameDisplayImg;
    Image hybridFaderTrackImg, hybridFineFaderTrackImg, hybridMixerPanelImg;
    
    NscLink *link;
    
    int mBank, mChan;
    
    bool trackSelected, blank, touchRecordEnabled, touchSoloEnabled, touchMuteEnabled;
    
    Time lastSelectTime;
    int blankFrames;
    bool panMode;
    bool ignoreNextUnBlank;
    String lastAutomationString;
    
    ScopedPointer<RavenFuturaTextDisplay> trackName;
    int timerNeededCount;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenMixerChannelComponent)
};


#endif   // __JUCER_HEADER_RAVENMIXERCHANNELCOMPONENT_RAVENMIXERCHANNELCOMPONENT_E957F1__
