
#ifndef Raven_RavenMessage_h
#define Raven_RavenMessage_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "NscGuiElement.h"

//RAVEN TOUCH MESSAGES ////////////////////////////////
enum RavenUpdateType
{
    eUpdateFader,
    eUpdateKnob,
    eUpdateFaderBankFromNSC,
    eUpdateFaderBankFromTouch,
    eUpdateMeterLevels,
    eUpdateLargeMeterLevels
};

class RavenMessage : public Message
{
public:
    RavenMessage(RavenUpdateType _type, float _val = 0) : type(_type), val(_val){}
    
    RavenUpdateType type;
    float val;
};

//RAVEN NSC MESSAGES ////////////////////////////////
class RavenNSCMessage : public Message
{
public:
    RavenNSCMessage(NscGuiElement *element, float value)        : el(element), val(value), text(""), isText(false) {}
    RavenNSCMessage(NscGuiElement *element, const char *txt)    : el(element), val(0), text(txt), isText(true) {}
    
    void updateNSC()
    {
        if(isText)  el->updateNscValue(text);
        else        el->updateNscValue(val);
    }
    
private:
    NscGuiElement *el;
    float val;
    String text;
    bool isText;
};

//RAVEN AUTO MESSAGES ////////////////////////////////
class RavenAutoMessage : public Message
{
public:
    RavenAutoMessage(int bnk, int chn, String autoType) : bank(bnk), chan(chn), automationType(autoType){}

    int bank, chan;
    String automationType;

};

//RAVEN TRACK BLANK MESSAGES ////////////////////////////////
class RavenTrackBlankMessage : public Message
{
public:
    RavenTrackBlankMessage(int bnk, int chn, bool _isBlank) : bank(bnk), chan(chn), isBlank(_isBlank){}
    
    int bank, chan;
    bool isBlank;
};

//RAVEN CLEAR METER MESSAGES ////////////////////////////////
class RavenClearMetersMessage : public Message
{
public:
    RavenClearMetersMessage(){}
};

//RAVEN COMPONENT MOVE MESSAGES ////////////////////////////////
class RavenComponentMoveMessage : public Message
{
public:
    RavenComponentMoveMessage(Component *cmp, int x, int y, int animateMS = 0) : component(cmp), xPos(x), yPos(y), animationTime(animateMS) {}
    
    void moveComponent()
    {
        if(animationTime > 0) //antimate
        {
            Rectangle<int> moveToRect(xPos,  yPos, component->getWidth(), component->getHeight());
            Desktop::getInstance().getAnimator().animateComponent (component, moveToRect, 1.0, animationTime, true, 0.0, 0.0);
        }
        else
        {
            component->setTopLeftPosition(xPos, yPos);
        }
    }
    
private:
    Component *component;
    int xPos, yPos;
    int animationTime;
};

#endif
