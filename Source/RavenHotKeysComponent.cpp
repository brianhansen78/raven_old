

#include "RavenHotKeysComponent.h"
#include "ASCIICGKeyCode.h"
#include "RavenRackContainer.h"

RavenHotKeysComponent::RavenHotKeysComponent() : currentButton(0), programMode(false)
{
    
    this->addKeyListener(this);
    this->setWantsKeyboardFocus(true);
    
    int xPos = 0;
    int yPos = 0;
    for(int i = 0; i < NUM_HOT_KEYS; i++)
    {

        String onFile = "TOOLBAR/FUNCTIONMODULE/HotKey/images/hotkey-on_0" + String(i+1) + ".png";
        String offFile = "TOOLBAR/FUNCTIONMODULE/HotKey/images/hotkey-off_0" + String(i+1) + ".png";
        
        RavenButton *hotKey = new RavenButton();
        hotKey->setImagePaths(onFile, offFile);
        hotKey->setTopLeftPosition(xPos, yPos);
        hotKey->addListener(this);
        hotKey->setWantsKeyboardFocus(true);
        //hotKey->setClickingTogglesState(true);
        hotKey->getProperties().set("isHotKey", true);
        addAndMakeVisible(hotKey);
        ravenTouchHotKeys.add(hotKey);
        
        Label *keyLabel = new Label();
        keyLabel->setWantsKeyboardFocus(true);
        keyLabel->setText(String(i+1), false);
        
        int labelX, labelY;
        if(i == 0)
        {
            labelX = hotKey->getX()+17;
            labelY = hotKey->getY()+16;
        }
        else if(i < 4)
        {
            labelX = hotKey->getX()+12;
            labelY = hotKey->getY()+16;
        }
        else if(i == 4)
        {
            labelX = hotKey->getX()+17;
            labelY = hotKey->getY()+11;
        }
        else
        {
            labelX = hotKey->getX()+13;
            labelY = hotKey->getY()+10;
        }
        
        if(i > 4) labelX -= 2;
        
        keyLabel->setBounds(labelX, labelY, 30, 30);
        keyLabel->setJustificationType(Justification::centred);
        keyLabel->setFont(Font(9));
        keyLabel->setColour(Label::textColourId, Colours::white);
        //keyLabel->setColour(Label::backgroundColourId, Colours::red);
        keyLabel->setInterceptsMouseClicks(false, false);
        addAndMakeVisible(keyLabel);
        keyLabels.add(keyLabel);
        
        xPos += hotKey->getWidth();
        if(i == 3)
        {
            yPos += hotKey->getHeight();
            xPos = 0;
        }
        
        if(i == NUM_HOT_KEYS-1) setSize(4*hotKey->getWidth(), 2*hotKey->getHeight());
    }

    
}
RavenHotKeysComponent::~RavenHotKeysComponent()
{
    
}

void RavenHotKeysComponent::paint (Graphics& g)
{
    //g.fillAll(Colours::black);
}

void RavenHotKeysComponent::buttonStateChanged (Button *b)
{
    //printf("button state change\n");
    
    //if(ravenTouchHotKeys[currentButton]->getProperties().getWithDefault("touchDown", false))
        //ravenTouchHotKeys[currentButton]->getProperties().set("touchDown", false);
    
    if(b->getToggleState() && !programMode)
    {        
        for(int i = 0; i <NUM_HOT_KEYS; i++)
        {
            //if(ravenTouchHotKeys[i]->getProperties().getWithDefault("touchDown", false)) return;
            
            if(b == ravenTouchHotKeys[i])
            {
                currentButton = i;
                downTimes[currentButton] = Time::getCurrentTime();
                startTimer(50);
                Process::makeForegroundProcess();
                RAVEN_TOUCH_LOCK
                return;
            }
        }
    }
    else if(b->getToggleState() && programMode)
    {
        b->setToggleState(false, false);
    }
    else if(!b->getToggleState())
    {
        stopTimer();
        RAVEN_TOUCH_UNLOCK
    }
}

void RavenHotKeysComponent::timerCallback()
{
    Time currentTime = Time::getCurrentTime();
        
    if(!programMode && (currentTime.toMilliseconds() - downTimes[currentButton].toMilliseconds() > 1000))
    {
        stopTimer();
        
        printf("enter program mode!\n");
        
        //bool isFE = getProperties().getWithDefault("isInFloatingEssentials", false);
        //if(isFE) return;
        
        programMode = true;
        keyCodes[currentButton].clear();
        //printf("program mode on!\n");
        
        //Set Hot Key Panel Position
        Button *b = ravenTouchHotKeys[currentButton];
        std::cout << "CurrentHotKeyButton: " << currentButton << std::endl;
        
        b->getProperties().set("isBeingProgrammed", true);
        
        std::cout << "PROGRAMMED?: " << (bool)ravenTouchHotKeys[currentButton]->getProperties().getWithDefault("isBeingProgrammed", false) << std::endl;
        
        int xPos = b->getScreenX() + b->getWidth()/2 - hotKeyPanel->getWidth()/2;
        int yPos = b->getScreenY() - hotKeyPanel->getHeight() + 30;
        if(yPos < osxMenuBarHeight) yPos += (osxMenuBarHeight-yPos);
        if(yPos > screenHeight - hotKeyPanel->getHeight()) yPos -= (yPos - (screenHeight - hotKeyPanel->getHeight()));
        if(xPos < hotKeyPanel->getWidth()/2) xPos += (hotKeyPanel->getWidth()/2 - xPos);
        if(xPos > screenWidth - hotKeyPanel->getWidth()/2) xPos -= (xPos - (screenWidth - hotKeyPanel->getWidth()/2));

        hotKeyPanel->setVisible(true);
        hotKeyPanel->getWindow()->addToDesktopWithOptions(RAVEN_HOTKEY_LEVEL);
        hotKeyPanel->getWindow()->setTopLeftPosition(xPos, yPos);
        hotKeyPanel->toFront(false);
        /////////////////////////////
        
        
        ravenTouchHotKeys[currentButton]->setToggleState(true, false);
        keyLabels[currentButton]->setText("", false);
        
        hotKeyPanel->setCommandText("enter key combination...");
        customNameText = "";
        hotKeyPanel->setNameText("name...");
        
        Process::setDockIconVisible(true); //needed to gain keyboard focus
        Process::makeForegroundProcess();
        
        Desktop::getInstance().ravenMakeKeyWindow();
        
        enterModalState(); //prevent other components in this app from taking keyboard focus
        
        this->grabKeyboardFocus();
        
        std::cout << "PROGRAMMED?: " << (bool)ravenTouchHotKeys[currentButton]->getProperties().getWithDefault("isBeingProgrammed", false) << std::endl;

        RAVEN_TOUCH_UNLOCK
        //if(hasKeyboardFocus(true)) printf("has focus!!\n");
    }
}

void RavenHotKeysComponent::buttonClicked (Button* b)
{
    
//this happens via action listener callback below
//    if(b->getName() == "customNameToggle")
//    {
//        if(programMode)
//        {
//            if(b->getToggleState()) keyLabels[currentButton]->setText(customNameText, false);
//            else                    keyLabels[currentButton]->setText(defaultNameText, false);
//        }
//        return;
//    }
    
    int programedKeyIndex = 0;
    for(int i = 0; i <NUM_HOT_KEYS; i++)
    {
        if(b == ravenTouchHotKeys[i]) currentButton = i;
        
        //std::cout << "currentButton: " << currentButton << std::endl;
        //std::cout << "Index: " << i << ", PROGRAMMED?: " << (bool)ravenTouchHotKeys[i]->getProperties().getWithDefault("isBeingProgrammed", false) << std::endl;

        if((bool)ravenTouchHotKeys[i]->getProperties().getWithDefault("isBeingProgrammed", false))
            programedKeyIndex = i;
    }
    
    /*
    Time currentTime = Time::getCurrentTime();
    if(!programMode && (currentTime.toMilliseconds() - downTimes[currentButton].toMilliseconds() > 1000))
    {

        programMode = true;
        keyCodes[currentButton].clear();
        //printf("program mode on!\n");
        
        //Set Hot Key Panel Position
        int xPos = b->getScreenX() + b->getWidth()/2 - hotKeyPanel->getWidth()/2;
        int yPos = b->getScreenY() - (screenHeight - hotKeyPanel->getParentComponent()->getHeight()) - hotKeyPanel->getHeight() + 35;
        if(yPos < osxMenuBarHeight) yPos += (osxMenuBarHeight-yPos);
        if(yPos > screenHeight - hotKeyPanel->getHeight()) yPos -= (yPos - (screenHeight - hotKeyPanel->getHeight()));
        if(xPos < hotKeyPanel->getWidth()/2) xPos += (hotKeyPanel->getWidth()/2 - xPos);
        if(xPos > screenWidth - hotKeyPanel->getWidth()/2) xPos -= (xPos - (screenWidth - hotKeyPanel->getWidth()/2));
        hotKeyPanel->setTopLeftPosition(xPos, yPos);
        hotKeyPanel->setVisible(true);
        /////////////////////////////

        
        ravenTouchHotKeys[currentButton]->setToggleState(true, false);
        
        hotKeyPanel->setCommandText("enter key combination...");
        customNameText = "";
        hotKeyPanel->setNameText("name...");

        Process::setDockIconVisible(true); //needed to gain keyboard focus
        Process::makeForegroundProcess();
        
        Desktop::getInstance().ravenMakeKeyWindow();
        
        enterModalState(); //prevent other components in this app from taking keyboard focus
 
        this->grabKeyboardFocus();
        
        //if(hasKeyboardFocus(true)) printf("has focus!!\n");
        
        return;
    }
     */
    
    bool isBeingProgrammed = ravenTouchHotKeys[currentButton]->getProperties().getWithDefault("isBeingProgrammed", false);
    
    
    if(programMode && isBeingProgrammed)
    {
        //printf("exit program mode...\n");
        ravenTouchHotKeys[currentButton]->getProperties().set("isBeingProgrammed", false);
        ravenTouchHotKeys[currentButton]->setToggleState(false, false);
        programMode = false;
        hotKeyPanel->resetCustomName();
        hotKeyPanel->getWindow()->removeFromDesktop();
        hotKeyPanel->saveProgram();
        exitModalState(0);
        if(RAVEN_IS_BACKGROUND_APP)
            Process::setDockIconVisible(false); //revert so keys will pass through to protools
    }
    else if (programMode && !isBeingProgrammed)
    {
        ravenTouchHotKeys[currentButton]->setToggleState(false, false);
        return;
    }
    else
    {
        //printf("hot key clicked!\n");
        MacKeyPress::pressCombo(keyCodes[currentButton]);
    }
}

void RavenHotKeysComponent::actionListenerCallback (const String &message)
{
    if(!programMode) return;
    if(message == "usecustomname")          keyLabels[currentButton]->setText(customNameText, false);
    else if(message == "usedefaultname")    keyLabels[currentButton]->setText(defaultNameText, false);
}

bool RavenHotKeysComponent::keyStateChanged (bool isKeyDown, Component *originatingComponent)
{
    return true;
}

bool RavenHotKeysComponent::keyPressed (const KeyPress &key, Component *originatingComponent)
{
    if(programMode && key.getKeyCode() == key.returnKey)
    {
        //ravenTouchHotKeys[currentButton]->getProperties().set("isBeingProgrammed", false);
        ravenTouchHotKeys[currentButton]->triggerClick();
        //ravenTouchHotKeys[currentButton]->setToggleState(false, false);
    }
    //took this out so that tab key could be used in shortcuts: 
//    else if(programMode && key.getKeyCode() == key.tabKey)
//    {
//        hotKeyPanel->selectCustomName();
//    }
    else if(programMode && !hotKeyPanel->useCustomName())
    {
        keyCodes[currentButton].clear();
        
        String labelText = "";
        defaultNameText = "";
        
        ModifierKeys mods = key.getModifiers();
        
        if(mods.isCommandDown())
        {
            keyCodes[currentButton].push_back(55);
            labelText = labelText + "cmd ";
            defaultNameText = defaultNameText + "cmd ";
        }
        if(mods.isShiftDown())
        {
            keyCodes[currentButton].push_back(56);
            labelText = labelText + "sft ";
            defaultNameText = defaultNameText + "sft ";
        }
        if(mods.isAltDown())
        {
            keyCodes[currentButton].push_back(58);
            labelText = labelText + "alt ";
            defaultNameText = defaultNameText + "alt ";
        }
        if(mods.isCtrlDown())
        {
            keyCodes[currentButton].push_back(59);
            labelText = labelText + "ctl ";
            defaultNameText = defaultNameText + "ctl ";
        }
        
        //key.getTextDescription() doesn't seem to give us anything for tab so do it manually:
        if(key.getKeyCode() == key.tabKey)
        {
            labelText = labelText + "tab ";
            defaultNameText = defaultNameText + "tab ";
        }
        if(key.getKeyCode() == key.backspaceKey)//check for delete key by checking for backspace
        {
            labelText = labelText + "del ";
            defaultNameText = defaultNameText + "del ";
        }
        if(key.getKeyCode() == key.escapeKey)
        {
            labelText = labelText + "escp ";
            defaultNameText = defaultNameText + "esc ";
        }
        if(key.getKeyCode() == key.upKey)
        {
            labelText = labelText + "up ";
            defaultNameText = defaultNameText + "up ";
        }
        if(key.getKeyCode() == key.downKey)
        {
            labelText = labelText + "down ";
            defaultNameText = defaultNameText + "down ";
        }
        if(key.getKeyCode() == key.leftKey)
        {
            labelText = labelText + "left ";
            defaultNameText = defaultNameText + "left ";
        }
        if(key.getKeyCode() == key.rightKey)
        {
            labelText = labelText + "rght ";
            defaultNameText = defaultNameText + "rght ";
        }
        if(key.getKeyCode() == key.spaceKey)
        {
            labelText = labelText + "spc ";
            defaultNameText = defaultNameText + "spc ";
        }
        
        //String newText = currentText + key.getTextDescription();
        
        hotKeyPanel->setCommandText(key.getTextDescription());

        char ascii = (char)key.getKeyCode();
        
        //these keys aren't right when converted from ascii to char using the method above, so we have to check/set them manually... 
        if(key.getKeyCode() == key.numberPad0)
        {
            ascii = ' ';
            labelText += "num 0";
            defaultNameText += "num 0";
        }
        else if(key.getKeyCode() == key.numberPad1)
        {
            ascii = ' ';
            labelText += "num 1";
            defaultNameText += "num 1";
        }
        else if(key.getKeyCode() == key.numberPad2)
        {
            ascii = ' ';
            labelText += "num 2";
            defaultNameText += "num 2";
        }
        else if(key.getKeyCode() == key.numberPad3)
        {
            ascii = ' ';
            labelText += "num 3";
            defaultNameText += "num 3";
        }
        else if(key.getKeyCode() == key.numberPad4)
        {
            ascii = ' ';
            labelText += "num 4";
            defaultNameText += "num 4";
        }
        else if(key.getKeyCode() == key.numberPad5)
        {
            ascii = ' ';
            labelText += "num 5";
            defaultNameText += "num 5";
        }
        else if(key.getKeyCode() == key.numberPad6)
        {
            ascii = ' ';
            labelText += "num 6";
            defaultNameText += "num 6";
        }
        else if(key.getKeyCode() == key.numberPad7)
        {
            ascii = ' ';
            labelText += "num 7";
            defaultNameText += "num 7";
        }
        else if(key.getKeyCode() == key.numberPad8)
        {
            ascii = ' ';
            labelText += "num 8";
            defaultNameText += "num 8";
        }
        else if(key.getKeyCode() == key.numberPad9)
        {
            ascii = ' ';
            labelText += "num 9";
            defaultNameText += "num 9";
        }
        else if(key.getKeyCode() == key.numberPadAdd)
        {
            ascii = ' ';
            labelText += "num +";
            defaultNameText += "num +";
        }
        else if(key.getKeyCode() == key.numberPadSubtract)
        {
            ascii = ' ';
            labelText += "num -";
            defaultNameText += "num -";
        }
        else if(key.getKeyCode() == key.numberPadMultiply)
        {
            ascii = ' ';
            labelText += "num *";
            defaultNameText += "num *";
        }
        else if(key.getKeyCode() == key.numberPadDivide)
        {
            ascii = ' ';
            labelText += "num /";
            defaultNameText += "num /";
        }
        else if(key.getKeyCode() == key.numberPadDecimalPoint)
        {
            ascii = ' ';
            labelText += "num .";
            defaultNameText += "num .";
        }
        else if(key.getKeyCode() == key.numberPadEquals)
        {
            ascii = ' ';
            labelText += "num =";
            defaultNameText += "num =";
        }
        else if(key.getKeyCode() == key.numberPadDelete)
        {
            ascii = ' ';
            labelText += "clear";
            defaultNameText += "clear";
        }
        
        //printf("int code = %d, char code = %c\n", key.getKeyCode(), ascii);
        
        labelText = labelText + ascii;
        defaultNameText = defaultNameText + ascii;
        
        //keyLabels[currentButton]->setText(labelText, false);
        keyLabels[currentButton]->setText(defaultNameText, false);
        
        //int cgCode = ASCIICGKeyCode::getCGKeyCode(ascii);
        int cgCode = ASCIICGKeyCode::getCGKeyCode(key.getKeyCode());
        
        keyCodes[currentButton].push_back(cgCode);
        
        printf("hot key programmed: ascii code = %d, cg code = %d, char = %c\n", key.getKeyCode(), cgCode, (char)key.getKeyCode());
    }
    else if(programMode && hotKeyPanel->useCustomName())
    {
        if(key.getKeyCode() == key.backspaceKey && customNameText.length() > 0)
        {
            customNameText = customNameText.dropLastCharacters(1);
        }
        else
        {
            customNameText = customNameText + key.getTextCharacter();
        }
        
        hotKeyPanel->setNameText(customNameText);
        keyLabels[currentButton]->setText(customNameText, false);
        
    }
    
    return true;
}