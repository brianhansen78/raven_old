#include "RavenKnob.h"
#include "RavenGUI.h"
#include "RavenConfig.h"
//#include "GUI_Resources/DAWKnobResource.h"

RavenKnob::RavenKnob(NscGuiContainer *owner, int bank, int chan)
:
NscGuiElement(owner),
prevVal(0.5),
isBeingTouched(false),
panIncrements(0),
knobNumFrames(128),
mBank(bank),
mChan(chan),
lastPanIndex(5),
blank(false),
hidden(false)
{    
    setSliderStyle(Rotary);
    setTextBoxStyle(NoTextBox, true, 0, 0);
    setRange(0.000, 1.000, 0.001);
}
            
RavenKnob::~RavenKnob()
{
    
}

void RavenKnob::setKnobType(EKnobType type)
{
    knobType = type;
    if(type == eKnobMixerStereo)
    {
        knobFrameWidth = 72;
        knobFrameHeight = 82;
        File imgFile(String(GUI_PATH) + "KNOBS/MIXER/Pan2Mix.png");
        knobImage = ImageCache::getFromFile(imgFile);
        setSize(knobImage.getWidth(), knobImage.getHeight()/knobNumFrames);
    }
    else if(type == eKnobMixerMono)
    {
        File imgFile;
        
        if(RAVEN_24)
        {
            imgFile = File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixKnob/Knob-Pan-Mix.png");
            panLevelDisplayImage = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixDisplay/Display-Pan-Level-Mix.png"));
        }
        else
        {
            imgFile = File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixKnob/Knob-Pan-Mix.png");
            panLevelDisplayImage = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixDisplay/Display-Pan-Level-Mix.png"));
        }
        
        knobImage = ImageCache::getFromFile(imgFile);
        knobFrameWidth = knobImage.getWidth();
        knobFrameHeight = knobImage.getHeight()/knobNumFrames;
        setSize(knobFrameWidth, knobFrameHeight);
    
    }
    else if(type == eKnobSendMono)
    {
        File imgFile;

        if(RAVEN_24)
            imgFile = File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendKnob/Knob-Pan-Send.png");
        else
            imgFile = File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendKnob/Knob-Pan-Send.png");
        
        knobImage = ImageCache::getFromFile(imgFile);
        knobFrameWidth = knobImage.getWidth();
        knobFrameHeight = knobImage.getHeight()/knobNumFrames;
        setSize(knobFrameWidth, knobFrameHeight);
        
    }
    else if(type == eKnobSendStereo)
    {
        knobFrameWidth = 71;
        knobFrameHeight = 86;
        File imgFile(String(GUI_PATH) + "KNOBS/SEND/Pan2Send.png");
        knobImage = ImageCache::getFromFile(imgFile);
        setSize(knobImage.getWidth(), knobImage.getHeight()/knobNumFrames);
    }
}

void RavenKnob::paint (Graphics& g)
{    
    if (!knobImage.isNull())
    {
        double normalizedValue = (getValue() - getMinimum()) / (getMaximum() - getMinimum());
        int frameNum = normalizedValue*(knobNumFrames-1);
        Rectangle<int> clipRect(0, frameNum*knobFrameHeight, knobFrameWidth, knobFrameHeight);
        const Image & clippedIm = knobImage.getClippedImage(clipRect);
        g.drawImageAt(clippedIm, 0, 0);
    }
}

void RavenKnob::setBlank(bool _blank, bool forceRepainting/*DEFAULT = false*/)
{
    if(_blank == true)
    {
        setVisible(false);
        setEnabled(false);
        panLLabel->setVisible(false);
        panRLabel->setVisible(false);
    }
    else
    {
        if(!hidden)
        {
            setVisible(true);
            setEnabled(true);
            panLLabel->setVisible(true);
            panRLabel->setVisible(true);
        }
        else
        {
            setVisible(false);
            setEnabled(false);
            panLLabel->setVisible(false);
            panRLabel->setVisible(false);
        }
    }
    bool needsRepainting = false;
    if(blank != _blank || forceRepainting)
    {
        repaint();
        needsRepainting = true;
    }
    blank = _blank;
    if(needsRepainting == true){getParentComponent()->repaint();}
}

void RavenKnob::updateNscValue(float value)
{
//    printf("raw NSC value: %f\n", value);
    //set blank/not based on value
    if(value == 0)
    {
        if(knobType != eKnobSendMono)
        {
            setBlank(true);
        }
    }
    else
    {
        setBlank(false);
    }
    
    if(fabs(value - 11100.545898) < 0.0001)//if HUI sends a 11100.545898 then it's at direct center
    {
        if(!isBeingTouched)
        {
            value = 0.5;
            prevVal = getValue();
            setValue(value, dontSendNotification);
            lastPanIndex = 5;
            panLLabel->setText(String(0), false);
            panRLabel->setText(String(0), false);
            return;
        }
    }
    
    //printf("bank %d, chan %d, knob val = %f\n", mBank, mChan, value);
    float val = getValue();
    if(!isBeingTouched)
    {
        value -= 11000.090820;
        value /= (11001.0 - 11000.090820);
        //printf("NSC value: %f, zero: %d\n", value, atZero);
        /*
                 incoming messages are only sent quantized into 11 stages so the values have to be approximated between a range
                 0.0     -100		-100
                 0.099893	-88--98		-92
                 0.199785	-64--86		-74
                 0.299678	-38--62		-50
                 0.399571	-14--36		-24
                 0.500537	-12-12		0
                 0.600430	14-36		24
                 0.700322	38-62		50
                 0.800215	64-86		74
                 0.900107	88-98		92
                 1.000000 	100     100
                 */
        
        int approximateInt = 0;
//        if(fabs(value-0.000000) < 0.0001){approximateInt = -100;}//index 0
//        if(fabs(value-0.099893) < 0.0001){approximateInt = -92;}//index 1
//        if(fabs(value-0.199785) < 0.0001){approximateInt = -74;}//index 2
//        if(fabs(value-0.299678) < 0.0001){approximateInt = -50;}//index 3
//        if(fabs(value-0.399571) < 0.0001){approximateInt = -24;}//index 4
//        if(fabs(value-0.500537) < 0.0001){approximateInt = 0;}//index 5
//        if(fabs(value-0.600430) < 0.0001){approximateInt = 24;}//index 6
//        if(fabs(value-0.700322) < 0.0001){approximateInt = 50;}//index 7
//        if(fabs(value-0.800215) < 0.0001){approximateInt = 74;}//index 8
//        if(fabs(value-0.900107) < 0.0001){approximateInt = 92;}//index 9
//        if(fabs(value-1.000000) < 0.0001){approximateInt = 100;}//index 10
        
        if(fabs(value-0.000000) < 0.0001)
        {
            lastPanIndex = 0;
            approximateInt = -100;
        }
        else if(fabs(value-0.099893) < 0.0001)
        {
            if(lastPanIndex < 1)
                approximateInt = -98;
            else
                approximateInt = -88;
            
            lastPanIndex = 1;
        }
        else if(fabs(value-0.199785) < 0.0001)
        {
            if(lastPanIndex < 2)
                approximateInt = -86;
            else
                approximateInt = -64;
            
            lastPanIndex = 2;
        }
        else if(fabs(value-0.299678) < 0.0001)
        {
            if(lastPanIndex < 3)
                approximateInt = -62;
            else
                approximateInt = -38;
            
            lastPanIndex = 3;
        }
        else if(fabs(value-0.399571) < 0.0001)
        {
            if(lastPanIndex < 4)
                approximateInt = -36;
            else
                approximateInt = -14;
            
            lastPanIndex = 4;
        }
        else if(fabs(value-0.500537) < 0.0001)
        {
            if(lastPanIndex < 5)
                approximateInt = -12;
            else if(lastPanIndex > 5)
                approximateInt = 12;
            else
                approximateInt = 0;
            
            lastPanIndex = 5;
        }
        else if(fabs(value-0.600430) < 0.0001)
        {
            if(lastPanIndex < 6)
                approximateInt = 14;
            else
                approximateInt = 36;
            
            lastPanIndex = 6;
        }
        else if(fabs(value-0.700322) < 0.0001)
        {
            if(lastPanIndex < 7)
                approximateInt = 38;
            else
                approximateInt = 62;
            
            lastPanIndex = 7;
        }
        else if(fabs(value-0.800215) < 0.0001)
        {
            if(lastPanIndex < 8)
                approximateInt = 64;
            else
                approximateInt = 86;
            
            lastPanIndex = 8;
        }
        else if(fabs(value-0.900107) < 0.0001)
        {
            if(lastPanIndex < 9)
                approximateInt = 88;
            else
                approximateInt = 98;
            
            lastPanIndex = 9;
        }
        else if(fabs(value-1.000000) < 0.0001)
        {
            lastPanIndex = 10;
            approximateInt = 100;
        }
        
        //update label
        if(approximateInt < 0)
        {
            panLLabel->setText(String(abs(approximateInt)), false);
            panRLabel->setText(String(0), false);
        }
        else
        {
            panRLabel->setText(String(abs(approximateInt)), false);
            panLLabel->setText(String(0), false);
        }

        if (value != val && !isBeingTouched)
        {  
            setValue(value, dontSendNotification);
            prevVal = val;
            panIncrements = approximateInt;
        }
    }
}

void RavenKnob::syncPanning()
{
    const float val = getValue();
    
    isBeingTouched = true;
    //printf("knob drag started\n");
    panIncrements = (int)((val-0.5)*100);
    
    //HACK clears it out to make sure we're at zero before setting to current Raven position
//    link->set(eNSCTrackEvent, eNSCTrackVPotValue , getBankIndex(), getChannelIndex(), 63);
//    link->set(eNSCTrackEvent, eNSCTrackVPotValue , getBankIndex(), getChannelIndex(), 63);
//    link->set(eNSCTrackEvent, eNSCTrackVPotValue , getBankIndex(), getChannelIndex(), 63);
//    link->set(eNSCTrackEvent, eNSCTrackVPotValue , getBankIndex(), getChannelIndex(), 63);
//    link->set(eNSCTrackEvent, eNSCTrackVPotValue , getBankIndex(), getChannelIndex(), -63);
//    link->set(eNSCTrackEvent, eNSCTrackVPotValue , getBankIndex(), getChannelIndex(), -63);
    
    int difference = panIncrements;
    //set to whatever the current Raven position is
    if(difference > 0)
    {
        while(difference > 0)
        {
            link->set(eNSCTrackEvent, eNSCTrackVPotValue , getBankIndex(), getChannelIndex(), 1);
            difference--;
        }
    }
    else if(difference < 0)
    {
        while(difference < 0)
        {
            link->set(eNSCTrackEvent, eNSCTrackVPotValue , getBankIndex(), getChannelIndex(), -1);
            difference++;
        }
    }
}


void RavenKnob::updatePanning()
{
    const float val = getValue();
    
    int tempIncrements = (int)((val-0.5)*100);
    
    //printf("value: %f\n", val);
    //printf("tempIncrements %d\n", tempIncrements);
    int panL, panR;
    if (tempIncrements < 0)
    {
        panR = 0;
        panL = abs(tempIncrements*2);
    }
    else
    {
        panL = 0;
        panR = tempIncrements*2;
    }
    
    //Send NSC to update ProTools
    if(tempIncrements != panIncrements)
    {
        int difference = tempIncrements-panIncrements;
        if(difference > 0)
        {
            while(difference > 0)
            {
                link->set(eNSCTrackEvent, eNSCTrackVPotValue , getBankIndex(), getChannelIndex(), 1);
                difference--;
                panIncrements++;
            }
        }
        else if(difference < 0)
        {
            while(difference < 0)
            {
                link->set(eNSCTrackEvent, eNSCTrackVPotValue , getBankIndex(), getChannelIndex(), -1);
                difference++;
                panIncrements--;
            }
        }
        
        panIncrements = tempIncrements;
        //printf("increments: %d\n", knob->panIncrements);
    }
    
    panLLabel->setText(String(panL), false);
    panRLabel->setText(String(panR), false);
    
    prevVal = getValue();
}