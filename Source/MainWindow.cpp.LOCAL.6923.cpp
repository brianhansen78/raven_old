
#include "MainWindow.h"
#include "RavenLoadingWindow.h"
//==============================================================================
MainAppWindow::MainAppWindow()
:
DocumentWindow (JUCEApplication::getInstance()->getApplicationName(), Colours::black, DocumentWindow::allButtons)
{
    /*
    RavenLoadingComponent *loadCmp = new RavenLoadingComponent();
    
    RavenContainer *container = new RavenContainer(this, loadCmp);
    
    setContentOwned(container, true); //true=> will resize with container
    
    setUsingNativeTitleBar(false); //if this isn't false then you can't set title bar height
    
    if(RAVEN_DEBUG)
    {
        setTitleBarHeight(22);
        setTopLeftPosition(0,0);
    }
    else
    {        
        setTitleBarHeight(0);
        //setTopLeftPosition(0, screenHeight - container->getHeight());
        setTopLeftPosition(0, osxMenuBarHeight);
    }
    
    //this was preventing  OS Alert Sounds with multi-touch, but no longer needed for that
    // gaining keyboard focus for hot keys is possible, but seems unrelieable without this
    //enterModalState(false);
    
    setOpaque(true);	
     */
}

MainAppWindow::~MainAppWindow()
{
    
}

void MainAppWindow::resized()
{
    //printf("main app window resized\n");
}

void MainAppWindow::closeButtonPressed()
{
    JUCEApplication::getInstance()->systemRequestedQuit();
}