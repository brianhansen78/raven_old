//
//  RavenWindow.h
//  Raven
//
//  Created by Ryan McGee on 5/7/13.
//
//

#ifndef Raven_RavenWindow_h
#define Raven_RavenWindow_h

#include "../JuceLibraryCode/JuceHeader.h"

class RavenWindow : public DocumentWindow
{
public:
    RavenWindow(Component *contentComponent, int initialRackPos = 0)  : DocumentWindow ("Raven", Colours::transparentBlack, 0, false), hidden(false), needsToHide(false), mRackPos(initialRackPos)
    {
        setDropShadowEnabled(false);
        setTitleBarHeight(0);
        setContentOwned (contentComponent, true);
        setVisible (true);
        setResizable(false, false);
        setBroughtToFrontOnMouseClick(false);
    }
    
    virtual BorderSize<int> getBorderThickness()
    {
        BorderSize<int>size(0);
        return size;
    }
    
    void addChildWindowBehind(Component *backWindow)
    {
        Desktop::getInstance().ravenChildWindow(this, backWindow);
    }
    
    void addToDesktopWithOptions(int level = RAVEN_RACK_LEVEL, bool toBack = false)
    {
        addToDesktop();
        Desktop::getInstance().ravenSetWindowOptions(this, level, toBack);
    }
    
    void setToMoverLevel()
    {
        Desktop::getInstance().ravenSetWindowOptions(this, RAVEN_MOVER_LEVEL);
    }
    
    void setToPaletteLevel()
    {
        Desktop::getInstance().ravenSetWindowOptions(this, RAVEN_PALETTE_LEVEL);
    }
    
//    hideButton = new RavenButton();
//    hideButton->setClickingTogglesState(true);
//    hideButton->setImagePaths("PowerButtons/POWER-ON.png", "PowerButtons/POWER-OFF.png");
//    hideButton->setToggleState(true, false);

    
    int getRackPos(){return mRackPos;}
    void incRackPos(){mRackPos += 1;}
    void decRackPos(){mRackPos -= 1;}
    void setRackPos(int pos) {mRackPos = pos;}

    
    void setOrigHeight(int height){origHeight = height;}
    int getOrigHeight(){return origHeight;}

    
    bool isHidden() {return hidden;}
    bool needsHiding() {return needsToHide;}  //upon exiting move modules
    void setHidden(bool hide)
    {
        hidden = hide;
//        if(hide)  hideButton->setToggleState(false, false);
//        else      hideButton->setToggleState(true, false);
    }
    void setNeedsHiding(bool hide)
    {
        needsToHide = hide;
        
//        if(hide)  hideButton->setToggleState(false, false);
//        else      hideButton->setToggleState(true, false);
//        
//        Rectangle<int> moveToRect(getX(), getY(), getWidth(), getHeight());
        
//        if(hide) Desktop::getInstance().getAnimator().animateComponent (this, moveToRect, 0.4, 500, true, 0.0, 0.0);
//        else     Desktop::getInstance().getAnimator().animateComponent (this, moveToRect, 1.0, 500, true, 0.0, 0.0);
    }

//    RavenButton *hideButton;
    
//    int compareElements(RavenWindow *first, RavenWindow *second)
//    {
//        if(first->getRackPos() < second->getRackPos()) return - 1;
//        if(first->getRackPos() == second->getRackPos()) return 0;
//        if(first->getRackPos() > second->getRackPos()) return 1;
//        return 0;
//    }

    
private:
    
    bool hidden, needsToHide;
    int mRackPos;
    int origHeight;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenWindow)
};


#endif
