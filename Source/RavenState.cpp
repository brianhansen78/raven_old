//
//  RavenState.cpp
//  Raven
//
//  Created by Joshua Dickinson on 4/3/13.
//
//

#include "RavenState.h"

//Global static pointer used to ensure a single instance of the class.
RavenState* RavenState::instance = 0;