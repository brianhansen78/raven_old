#include "RavenFader.h"
//#include "GUI_Resources/DAWFaderResource.h"
#include "RavenConfig.h"

RavenFader::RavenFader(NscGuiContainer *owner, int trackLength) : NscGuiElement(owner), isBeingTouched(false), mTrackLength(trackLength), isSend(false), updateCount(0), newNSCValue(0), needsNSCUpdating(false)
{
    setSliderStyle(LinearVertical);
    setTextBoxStyle(NoTextBox, true, 0, 0);
    setRange(0.000, 1.000);
    //setValue(0);
}

RavenMixerFader::RavenMixerFader(NscGuiContainer *owner, int trackLength, int bank, int chan) : RavenFader(owner, trackLength), nonHybridModeTrackLength(trackLength)
{
    File capImgFile;
    if(RAVEN_24)
    {
        faderCapImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixFader/Fader-Mix.png"));
    }
    else
    {
        faderCapImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixFader/Fader-Mix.png"));
    }
    
    int totalHeight = faderCapImg.getHeight() + mTrackLength;
    setSize(faderCapImg.getWidth(), totalHeight);
}

void RavenMixerFader::setHybridMode(bool hybridMode)
{
    if(hybridMode)
    {
        mTrackLength = RAVEN_HYBRID_MODE_FADER_LENGTH;
        faderCapImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FloatingMixer/FloatingMixerGraphics/MixFader/Fader-Mix.png"));
    }
    else
    {
        mTrackLength = nonHybridModeTrackLength;
        if(RAVEN_24)
        {
            faderCapImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixFader/Fader-Mix.png"));
        }
        else
        {
            faderCapImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixFader/Fader-Mix.png"));
        }
    }
    setSize(faderCapImg.getWidth(), faderCapImg.getHeight() + mTrackLength);
}

RavenSendFader::RavenSendFader(NscGuiContainer *owner, int trackLength, int bank, int chan) : RavenFader(owner, trackLength)
{
    isSend = true;
    
    File capImgFile;
    
    if(RAVEN_24)
    {
        faderCapImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendFader/Fader-Send.png"));
    }
    else
    {
        faderCapImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendFader/Fader-Send.png"));
    }
    
    int totalHeight = faderCapImg.getHeight() + mTrackLength;
    setSize(faderCapImg.getWidth(), totalHeight);
}

RavenFader::~RavenFader()
{
    
}

int RavenFader::getCapCenterScreenX()
{
    return getScreenX() + faderCapImg.getWidth()/2;
}
int RavenFader::getCapCenterScreenY()
{
    double normalizedValue = (getValue() - getMinimum()) / (getMaximum() - getMinimum());        
    int yPos = (1.0-normalizedValue)*mTrackLength;
    return getScreenY() + yPos + faderCapImg.getHeight()/2;
}

int RavenFader::getTrackTopScreenY()
{
    return getScreenY() + faderCapImg.getHeight()/2;
}

/*
void RavenFader::paint (Graphics& g)
{    
    if (!faderCapImg.isNull())
    {
        double normalizedValue = (getValue() - getMinimum()) / (getMaximum() - getMinimum());        
        int yPos = (1.0-normalizedValue)*mTrackLength;
        g.drawImageAt(faderCapImg, 0, yPos);
    }
}
*/

//fast paint
void RavenFader::paint (Graphics& g)
{
    //printf("reduced = %d\n", clip);
    int yPos = (1.0-getValue())*mTrackLength;
    g.drawImageAt(faderCapImg, 0, yPos);
    g.reduceClipRegion(faderCapImg.getBounds());
}

void RavenFader::repaint (Graphics& g)
{

}

void RavenFader::updateNscValue(float value)
{
    //printf("update nsc value: %f\n", value);
    //printf("is being touched %d, value: %f, getValue: %f\n", isBeingTouched, value, getValue());
    if (value != getValue() && !isBeingTouched)
    {
        //printf("going to update\n");
        newNSCValue = value;
        needsNSCUpdating = true;
    }
}

void RavenFader::updateTouchValue(float value)
{
    if (value != getValue() && isBeingTouched)
    {
        //printf("new touch value: %f\n", value);
        newTouchValue = value;
        needsTouchUpdating = true;
    }
}

void RavenFader::updateNSCThread()
{
    if (needsNSCUpdating)
    {
        //printf("NSC set val %f\n", newNSCValue);
        setValue(newNSCValue, dontSendNotification);
        
        float dBVal = getDBVal();
        
        if(isSend)  dBLabel->setText(String(dBVal).substring(0, 4), false);
        else
        {
            int strlen = 4;
            if(dBVal > 0 && dBVal < 10) strlen = 3;
            if(dBVal < -10) strlen = 5;
            dBLabel->setText(String(dBVal).substring(0, strlen) + " dB", false);
        }
        needsNSCUpdating = false;
    }
}

void RavenFader::updateTouchThread()
{
    if (needsTouchUpdating)
    {
        setValue(newTouchValue);
        needsTouchUpdating = false;
    }
}

void RavenFader::updateNscValue(const String & value)
{
    
}

float RavenFader::getDBVal()
{
    float val = getValue();
    
    /*
    if(val > fader0dbVal)
    {
        val -= fader0dbVal;
        val *= (1.0f/(1.0-fader0dbVal))*12;
    }
    else
    {
        val /= fader0dbVal;
        val = powf(val, 3.4);
        val = 20*log10f(val);
    }
     */
    
    float dbVal = 0;
    if(val <= faderDBvals[numFaderDBVals-1])
    {
        //float f = (faderDBvals[numFaderDBVals-1] - val)/(faderDBvals[numFaderDBVals-1]);
        //dbVal = (1.0-f)*faderDBvals[numFaderDBVals-1];
        
        val /= fader0dbVal;
        val = powf(val, 3.6);
        dbVal = 20*log10f(val);
        
    }
    else
    {
        for(int i = 0; i < numFaderDBVals-1; i++)
        {
            if(val <= faderDBvals[i] && val > faderDBvals[i+1])
            {
                float f = (faderDBvals[i] - val)/(faderDBvals[i] - faderDBvals[i+1]);
                dbVal = (1.0-f)*faderDBs[i] + f*faderDBs[i+1];
                break;
            }
        }
    }
    
    if(dbVal >= -0.05 && dbVal <= 0.05) dbVal = 0.0;
    
    return dbVal;
}