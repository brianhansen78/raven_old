#ifndef __RAVENCONTAINER_H_B6CC5B6__
#define __RAVENCONTAINER_H_B6CC5B6__

//JUCE
#include "../JuceLibraryCode/JuceHeader.h"

//Raven
#include "RavenLoadingWindow.h"
#include "RavenRackContainer.h"
#include "RavenConfig.h"
#include "MouseHideComponent.h"
#include "RavenMessage.h"
#include "RavenMacUtilities.h"
#include "RavenRackContainer.h"
#include "RavenSettingsComponent.h"
#include "RavenIconPaletteComponent.h"

//NSC
#include "NscSDKIncludes.h"

//Multi-Touch
#include "MTDefines.h"

class RavenContainer : public TUIO::TuioListener, public NscAppDelegate, public MultiTimer, public MessageListener, public ActionListener
{
public:
    RavenContainer(RavenLoadingComponent *loadCmp);
    ~RavenContainer();
    
    //MessageListener
    void handleMessage(const Message &message);
    //ActionListener
    void actionListenerCallback (const String &message);
    //MultiTimer
    void timerCallback(int timerID);
    
    void autoEnterFlipMode();
    void enterFlipMode();
    void exitFlipMode();
    
    void enterHybridMode();
    void exitHybridMode();
    
    void startInitialization();
    void finishedInitialization();

    //TUIO
    void addTuioCursor(TUIO::TuioCursor *tcur);
	void updateTuioCursor(TUIO::TuioCursor *tcur);
	void removeTuioCursor(TUIO::TuioCursor *tcur);
    //unused:
    void addTuioObject(TUIO::TuioObject *tobj){}
	void updateTuioObject(TUIO::TuioObject *tobj){}
	void removeTuioObject(TUIO::TuioObject *tobj){}
    void refresh(TUIO::TuioTime frameTime){}
    
    //PQ
    static void onReceivePQGesture(const PQ_SDK_MultiTouch::TouchGesture & ges, void *call_back_object);
    bool activePQGesture;
    bool activeNavPadGesture; //used for PQ gesture callback to indicate the start of a nav pad gesture.  also prevents TUIO callback from updating navpad
    bool isPQSplitGesture;
    
    //Touches
    struct touch
    {
        EMTGestureType gesture;
        bool isRavenRack, isRavenPalette, isMixer, isSend, isCounter, isInsertsPalette, isNavPad, isFunction, isDrumPad, isTrackSelect, isTrackDeselect, isAutoMode, isRecEnable, isEssentials, isFloatingNavPad, isFloatingNavPadMove, isFloatingMixerMove, isIconPaletteMove,
                isSoloEnable, isMuteEnable, isSettingsPalette, isIconPalette;
        bool isFader, isKnob, isButton, isModuleMove, isPlugInMove, isMainGestureFinger, isOtherAppTouch, isSurroundPannerTouch;
        int bank, chan;
        int initTouchX;
        int initTouchY;
        Button* holdButton;
        int holdButtonTimerID;
    };
    touch touches[MAX_NUM_TOUCHES];
        
    // NscAppDelegate interface
	virtual int nscServerFound(const char* serverName, const char * domain);	// Called when a NeyFi server has been detected.  Pass arguments to connectToServer to establish connection.
	virtual int nscConnected(const char* serverName, const char * domain);		// Called once link to server has been established.  NeyFi link is valid only after this callback.
	virtual int nscDisconnected(const char* serverName, const char * domain);	// Called when NeyFi has stopped (regardless of whether or not a link to it has been formed via connectToServer).  link is no longer valid.
	virtual int callback(ENSCEventType type, int subType, int bank, int which, float val);
	virtual int callback(ENSCEventType type, int subType, int bank, int which, const std::string & data);
	virtual void setDawMode(EDAWMode mode);
    
    void releaseFaderByTouchID(int _id);
    
    bool isCommandDown(){ return (MacKeyPress::isCommandDown() || functionsManager->getCommandButtonState());}
    bool isShiftDown(){ return (MacKeyPress::isShiftDown() || functionsManager->getShiftButtonState());}
    
private:
    
    TUIO::TuioClient *tuioClient;
    
    NscLink *link;
    
    //we will delete this after initialization, so doesn't need to be a scoped pointer
    RavenLoadingComponent *loadingWindow;
    
    //these are created and managed within rackContainer, so they don't need to be scoped pointers
    RavenMixerComponent *mixer;
    RavenSendsComponent *sends;
    RavenTrackNameComponent *names;
    RavenFunctionsManager *functionsManager;
    RavenFloatingEssentialsComponent *floatingEssentials;
    
    RavenWindow *mixerWindow;
    RavenWindow *sendsWindow;
    RavenWindow *namesWindow;    
    
    RavenSettingsComponent *settings;
    RavenIconPaletteComponent *iconPalette;

    bool touchesOn;
    TUIO::TuioPoint lastTapPos;
    TUIO::TuioTime lastTapTime;
    Time prevTime;
    
    bool isMovingRackModules;
    
    Array<RavenMixerChannelComponent*> touchedChannels;
    
    int prevSelectedBank;
    int prevSelectedChannel;
    int initTouchBank;
    int initTouchChannel;
    bool initCommandState;
    bool initShiftState;
    
    bool inFlipMode, blockFlipSwitch; //try to get this from HUI button instead...
    
    int panModeCounters[numBanks];
    std::vector<int> panModeBanksNeedingChange;
    
    bool waitingForAutomationLabels;
    bool temporarilyBlockAllNameSwitching;
    bool temporarilyBlockCertainNames;
    
    int trackSwipeStartX;
    int tracksSinceSwipeStart;
    bool ignoreIncomingTrackInfomation;
    int lastPQXposition;
    
    Time lastExitFlipModeTime;
    
    ThreadPool meterPool, largeMeterPool, faderNSCPool, faderTouchPool, sendsFaderTouchPool, sendsFaderNSCPool;
    
    std::vector<int> onlineModeKeyCode;
    Time stopButtonHoldStartTime;
    
    
#if RAVEN_ADD_TRANSPARENT_MIXER
    RavenTransparentMixerComponent *transparentMixer;
    ThreadPool transparentFaderNSCPool, transparentFaderTouchPool;
#endif

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenContainer)
};



#endif 
