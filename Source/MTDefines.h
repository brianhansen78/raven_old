
#ifndef _MTGesture_h
#define _MTGesture_h

//TUIO
#include "TUIO/TuioListener.h"
#include "TUIO/TuioClient.h"
#include "TUIO/TuioObject.h"
#include "TUIO/TuioCursor.h"
#include "TUIO/TuioPoint.h"

//PQ SDK
#include "PQMTClient.h"
#include "RavenConfig.h"

#define FADER_SMOOTHING             0.005   //0.005 is a good minimum
#define FADER_X_SLACK               5      //# pixels "slack" on each side of fader to allow for easier grabbing
#define FINE_FADER_AMT              0.001

#define FADER_HALF_HEIGHT_24        52
#define FADER_HALF_HEIGHT_32        40
#define FADER_HALF_HEIGHT_32_HYBRID 20

#define SCREEN_WIDTH 1920
#define SCREEN_HEIGHT 1080
#define MT_2FINGER_WAIT_USEC 100000//27000
#define MT_3FINGER_WAIT_USEC 100000//50000
#define MT_TAP_WAIT_USEC 300000
#define MT_MOD_HOLD_WAIT_USEC 1000000
#define MT_2FINGER_TOUCH_DISTANCE 0.15
#define MT_3FINGER_TOUCH_DISTANCE 0.4
#define MT_TAP_TOUCH_DISTANCE 0.02
#define MT_BANK_SWIPE_DISTANCE  100//distance in pixels for bank swipe on floating mixer

#define BUTTON_TOUCH_HEIGHT_RATIO   0.65

//NAVPAD
#if RAVEN_24
    #define RAVEN_NAVPAD_NUM_AVG_DEFAULT          10      //number of frames to average
    #define RAVEN_NAVPAD_NUM_AVG_PINCH            2       //number of frames to average for pinch
    #define RAVEN_SHUTTLE_SPEED_MULT              50.0f   //controls the speed of shuttle. (100.0f)
    #define RAVEN_WAVEFORM_WIDTH_SPEED_THRESH     0.1f    //only triggers if average speed is above this threshold (0.1f)
    #define RAVEN_WAVEFORM_HEIGHT_SPEED_THRESH    0.1f    //only triggers if average speed is above this threshold (0.1f)
    #define RAVEN_TRACK_ZOOM_SPEED_THRESH         0.1f    //only triggers if average speed is above this threshold (0.1f)
    #define RAVEN_SCROLL_SPEED_THRESH             0.1f    //only triggers if average speed is above this threshold (0.1f)
#else
    #define RAVEN_NAVPAD_NUM_AVG_DEFAULT          20      //number of frames to average
    #define RAVEN_NAVPAD_NUM_AVG_PINCH            2       //number of frames to average for pinch
    #define RAVEN_SHUTTLE_SPEED_MULT              50.0f   //controls the speed of shuttle. (100.0f)
    #define RAVEN_WAVEFORM_WIDTH_SPEED_THRESH     0.4f    //only triggers if average speed is above this threshold (0.1f)
    #define RAVEN_WAVEFORM_HEIGHT_SPEED_THRESH    0.4f    //only triggers if average speed is above this threshold (0.1f)
    #define RAVEN_TRACK_ZOOM_SPEED_THRESH         0.4f    //only triggers if average speed is above this threshold (0.1f)
    #define RAVEN_SCROLL_SPEED_THRESH             0.4f    //only triggers if average speed is above this threshold (0.1f)
#endif


#define RAVEN_NAVPAD_NUM_AVG_SCRUB            1       //number of frames to average for scrub
#define RAVEN_SCRUB_SPEED_THRESH              0.0f    //only triggers if average speed is above this threshold (0.0001f)
#define RAVEN_SCRUB_SPEED_MULT                0.5f    //0.5f while using distance method. 100.0f when using old method

#define RAD2DEG 57.2957795

#define KNOB_ROT_RAD_FACTOR 10 //multiplied by knob radius to determine outer bounds of control
#define KNOB_TOUCH_RAD_FACTOR 1.2 //factor of radius that will allow touch to register as knob touch 1 = knob, > 1 = outside knob

typedef enum
{
    eMTOneFinger,
    eMTTwoFinger,
    eMTThreeFinger
} EMTGestureType;


#define RAVEN_BUTTON_HEIGHT_0 42
#define RAVEN_BUTTON_HEIGHT_1 52
#define RAVEN_BUTTON_HEIGHT_2 54
#define RAVEN_BUTTON_HEIGHT_3 65
#define RAVEN_BUTTON_HEIGHT_4 34
#define RAVEN_BUTTON_HEIGHT_5 48 //unique height for edit mix button

#define RAVEN_BUTTON_0(component) component->getProperties().set("CustomHeight", RAVEN_BUTTON_HEIGHT_0);
#define RAVEN_BUTTON_1(component) component->getProperties().set("CustomHeight", RAVEN_BUTTON_HEIGHT_1);
#define RAVEN_BUTTON_2(component) component->getProperties().set("CustomHeight", RAVEN_BUTTON_HEIGHT_2);
#define RAVEN_BUTTON_3(component) component->getProperties().set("CustomHeight", RAVEN_BUTTON_HEIGHT_3);
#define RAVEN_BUTTON_4(component) component->getProperties().set("CustomHeight", RAVEN_BUTTON_HEIGHT_4);
#define RAVEN_BUTTON_5(component) component->getProperties().set("CustomHeight", RAVEN_BUTTON_HEIGHT_5);


#endif
