
#ifndef Raven_RavenLoadingWindow_h
#define Raven_RavenLoadingWindow_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenConfig.h" 

class RavenLoadingComponent : public Component
{
public:
    RavenLoadingComponent()
    {
        startupImg = ImageCache::getFromFile(File(String(GUI_PATH) + "startup1.png"));
        
        setAlwaysOnTop(true);
        
        setVisible (true);
        
        centreWithSize(startupImg.getWidth(), startupImg.getHeight());
        
        statusLabel = new Label (String("status label"), String("connecting to Ney-Fi..."));
        statusLabel->setFont (Font (14.000f, Font::plain));
        statusLabel->setJustificationType (Justification::left);
        statusLabel->setEditable (false, false, false);
        statusLabel->setColour (Label::textColourId, Colours::white);
        statusLabel->setColour (TextEditor::textColourId, Colours::black);
        statusLabel->setColour (TextEditor::backgroundColourId, Colours::black);
        statusLabel->setBounds(400, getHeight()-120, 400, 30);
        addAndMakeVisible(statusLabel);
        
        setVisible(true);
        
        addToDesktop(0);
    }

    void paint(Graphics &g)
    {
        g.drawImageAt(startupImg, 0, 0);
    }
    
    void updateStatusLabel(const String &text)
    {
        LOCK_JUCE_THREAD statusLabel->setText(text, false);
    }
    
private:
    Image startupImg;
    ScopedPointer<Label> statusLabel;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(RavenLoadingComponent)
};

//unused: we just add the loading component to the desktop instead
//class RavenLoadingWindow   : public DocumentWindow
//{
//public:
//    //==============================================================================
//    RavenLoadingWindow() : DocumentWindow (JUCEApplication::getInstance()->getApplicationName(),
//                                           Colours::black,
//                                           DocumentWindow::allButtons)
//    {
//            loadCmp = new RavenLoadingComponent();
//        
//            centreWithSize (loadCmp->getWidth(), loadCmp->getHeight());
//        
//            setContentOwned(loadCmp, false);
//            
//            setUsingNativeTitleBar(false);
//        
//            setTitleBarHeight(0);
//            
//            setAlwaysOnTop(true);
//        
//            setVisible (true);
//    }
//    
//    void updateStatusLabel(const String &text)
//    {
//        loadCmp->updateStatusLabel(text);
//    }
//    
//private:
//    
//    ScopedPointer<RavenLoadingComponent> loadCmp;
//    
//    //==============================================================================
//    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenLoadingWindow)
//};

#endif
