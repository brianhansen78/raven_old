
#ifndef __JUCER_HEADER_RAVENMIXERCOMPONENT_RAVENMIXERCOMPONENT_931F6D3B__
#define __JUCER_HEADER_RAVENMIXERCOMPONENT_RAVENMIXERCOMPONENT_931F6D3B__

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenWindowComponent.h"
#include "RavenMixerBankComponent.h"
#include "RavenWindow.h"
#include "MouseHideComponent.h"
#include "RavenImageComponent.h"

class RavenMixerComponent : public RavenWindowComponent, public ActionListener, public Button::Listener
{
public:
    //==============================================================================
    RavenMixerComponent (int numBanks, int chansPerBank) : numberOfHybridModeBanks(2), hybridX(0), hybridY(0), nonHybridX(0), nonHybridY(0)//, meterThreadPool(numBanks)
    {
        hybridLeftSideImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FloatingMixer/Frame/Left.png"));
        hybridRightSideImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FloatingMixer/Frame/Right.png"));
        hybridHeaderLabelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FloatingMixer/Frame/Win_Title.png"));
        
        setSize (0, 0);
        int xOffset = 0;
        //if(!RAVEN_32_TEST) xOffset = MIXER_X_OFFSET;
        if(RAVEN_24) xOffset = 0;
        for(int i = 0; i < numBanks; i++)
        {
            RavenMixerBankComponent *bank = new RavenMixerBankComponent(chansPerBank, i);
            setSize(getWidth() + bank->getWidth(), bank->getHeight());
            addAndMakeVisible(bank);
            bank->setTopLeftPosition(xOffset, 0);
            xOffset += bank->getWidth();
            banks.add(bank);
        }
        
        //hideButton->setTopLeftPosition(30, 200);
        //hideButton->setName("hideMixer");
//        hideButton->setVisible(false);
//        ravenTouchButtons.add(hideButton);
        //addChildComponent(hideButton);
        
        for(int b = 0; b < banks.size(); b++)
        {
            for(int c = 0; c < chansPerBank; c++)
            {
                banks[b]->getChannel(c)->addActionListener(this);
            }
        }
        
        /*Thread Pool for Meters
        for(int b = 0; b < numBanks; b++)
        {
            meterThreadPool.addJob(getBank(b), false);
        }
        //*/
        
        hybridModeMoveComponent = new RavenImageComponent();
        hybridModeMoveComponent->setImage(String(GUI_PATH) + "FloatingMixer/Frame/Top.png");
        addAndMakeVisible(hybridModeMoveComponent);
        hybridModeMoveComponent->setVisible(false);
        
        hybridBottomComponent = new RavenImageComponent();
        hybridBottomComponent->setImage(String(GUI_PATH) + "FloatingMixer/Frame/Bottom_full_Lenght.png");
        addAndMakeVisible(hybridBottomComponent);
        hybridBottomComponent->setVisible(false);
        
        hybridTopRightComponent = new RavenImageComponent();
        hybridTopRightComponent->setImage(String(GUI_PATH) + "FloatingMixer/Frame/Top_Right.png");
        addAndMakeVisible(hybridTopRightComponent);
        hybridTopRightComponent->setVisible(false);
        
        
        hybridX = (0.5*screenWidth)-((2*RAVEN_BANK_WIDTH+hybridRightSideImg.getWidth()+hybridLeftSideImg.getWidth())*0.5);
        hybridY = screenHeight-(2*RAVEN_TOOLBAR_HEIGHT+hybridModeMoveComponent->getHeight());
        
        closeButton = new RavenButton();
        closeButton->setTopLeftPosition(435, 14);
        closeButton->setImagePaths("FloatingMixer/HeaderButtons/Fm_Close_On.png", "FloatingMixer/HeaderButtons/Fm_Close_Off.png");
        ravenTouchButtons.add(closeButton);
        closeButton->addListener(this);
        addAndMakeVisible(closeButton);
        closeButton->setVisible(false);
        
        addBankButton = new RavenButton();
        addBankButton->setTopLeftPosition(132, 13);
        addBankButton->setImagePaths("FloatingMixer/HeaderButtons/Fm_Plus_On.png", "FloatingMixer/HeaderButtons/Fm_Plus_Off.png");
        ravenTouchButtons.add(addBankButton);
        addBankButton->addListener(this);
        addAndMakeVisible(addBankButton);
        addBankButton->setVisible(false);
        
        removeBankButton = new RavenButton();
        removeBankButton->setTopLeftPosition(12, 13);
        removeBankButton->setImagePaths("FloatingMixer/HeaderButtons/Fm_Minus_On.png", "FloatingMixer/HeaderButtons/Fm_Minus_Off.png");
        ravenTouchButtons.add(removeBankButton);
        removeBankButton->addListener(this);
        addAndMakeVisible(removeBankButton);
        removeBankButton->setVisible(false);
        
        controlModeButton = new RavenButton();
        controlModeButton->setTopLeftPosition(72, 13);
        controlModeButton->setImagePaths("FloatingMixer/HeaderButtons/Fm_Pan_On.png", "FloatingMixer/HeaderButtons/Fm_Pan_Off.png");
        ravenTouchButtons.add(controlModeButton);
        controlModeButton->addListener(this);
        addAndMakeVisible(controlModeButton);
        controlModeButton->setVisible(false);
        
        #if !RAVEN_DEBUG
        //Invisible Mouse Hiding Component (hides mouse and intercepts clicks)
        //MacHideBackGroundMouse enables this to work
        mouseHider = new MouseHideComponent(getWidth(), getHeight());
        addAndMakeVisible(mouseHider);
        #endif
    }
    
    ~RavenMixerComponent()
    {
        //meterThreadPool.removeAllJobs (true, 2000);
    }
    
    void actionListenerCallback (const String &message)
    {
        if(message == "Off " || message == "    " || message == "Read" || message == "Tch " || message == "Ltch" || message == "Wrt " || message == "Trim")
        {
            for(int b = 0; b < banks.size(); b++)
            {
                for(int c = 0; c < chansPerBank; c++)
                {
                    if(banks[b]->getChannel(c)->getAutoCompVisible())
                    {
                        banks[b]->getChannel(c)->setAutomationImage(message, true);
                    }
                }
            }
        }
        
        if(message == "left pan mode")
        {
            for(int i = 0;i<banks.size();i++)
                for(int j = 0;j<chansPerBank;j++)
                {
                    banks[i]->getChannel(j)->setPanMode(false);
                }
        }
        else if(message == "right pan mode")
        {
            for(int i = 0;i<banks.size();i++)
                for(int j = 0;j<chansPerBank;j++)
                {
                    banks[i]->getChannel(j)->setPanMode(true);
                }
        }
    }
    
    void enterFlipMode()
    {
        for(int i = 0; i < banks.size(); i++) banks[i]->enterFlipMode();
    }
    
    void exitFlipMode()
    {
        for(int i = 0; i < banks.size(); i++) banks[i]->exitFlipMode();
    }
    
    void flipAlpha(bool on)
    {
        for(int i = 0; i < banks.size(); i++) banks[i]->flipAlpha(on);
    }
    
    void setRackContainerButtonListener(Button::Listener * listener)
    {
        for(int i = 0; i < banks.size(); i++) banks[i]->setRackContainerButtonListener(listener);
//        hideButton->addListener(listener);
    }
    
    void toggleInsertButtonsOff()
    {
        for(int i = 0; i < banks.size(); i++) banks[i]->toggleInsertButtonsOff();
    }
    
    void setNscLink	(class NscLink * link_)
    {
        for(int i = 0; i < banks.size(); i++)
        {
            banks[i]->setNscLink(link_);
            for(int c = 0; c < chansPerBank; c++) banks[i]->getChannel(c)->setNscLink(link_);
        }
    }
    
    void setFinePan(bool fp)
    {
        for(int i = 0; i < banks.size(); i++) banks[i]->setFinePan(fp);
    }
    
    void setFineFader(bool ff)
    {
        for(int i = 0; i < banks.size(); i++) banks[i]->setFineFader(ff);
    }
    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, float val)
    {
        for (int i=0; i<banks.size();++i)
        {
            if (bank == eMatchesAllBanks || bank == i)
                banks[i]->callback(type, subType, bank, which, val);
        }
    }
    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, const char * text)
    {
        for (int i=0; i<banks.size();++i)
        {
            if (bank == eMatchesAllBanks || bank == i)
                banks[i]->callback(type, subType, bank, which, text);
        }
    }
    
    void setAutomationImage(int bank, int which, const String& automationType)
    {
        banks[bank]->setAutomationImage(which, automationType);
    }
    
    void setAllAutomationImagesOff()
    {
        for (int i=0; i<banks.size();++i)
        {
            for(int j = 0;j<chansPerBank;j++)
            {
                if(banks[i]->getChannel(j)->isBlank() == false)
                {
                    banks[i]->getChannel(j)->setBlank(false);
                }
                banks[i]->getChannel(j)->setBlankFrames(0);
                //banks[i]->getChannel(j)->setAutomationImage(String("Off "), false);
            }
        }
    }
    
    void resetAllBlankFrames()
    {
        for (int i=0; i<banks.size();++i)
            for(int j = 0;j<chansPerBank;j++)
                banks[i]->getChannel(j)->setBlankFrames(0);
    }
    
    void checkBlankFrames()
    {
    }
    
    void trackLeft()
    {
    }
    
    void trackRight()
    {
    }
    
    bool canTrackRight();
    
    bool canTrackLeft();
    
    int numberCanBankRight();
    int numberCanBankLeft();
    
    int numberCanBankRightThreeFinger();
    int numberCanBankLeftThreeFinger();

    
    RavenMixerBankComponent* getBank(int num){return banks[num];}
    
    void resized();
    
    void visibilityChanged()
    {
        for (int i=0; i<banks.size();i++)
            banks[i]->setVisible(isVisible());
    }
    
    void setUseImageProxy(bool useImageProxy)
    {
        if(useImageProxy)
        {
            
        }
    }
    
    void hybridModeSizing()
    {
        hybridModeMoveComponent->setVisible(true);
        hybridBottomComponent->setVisible(true);
        hybridTopRightComponent->setVisible(true);
        closeButton->setVisible(true);
        addBankButton->setVisible(true);
        removeBankButton->setVisible(true);
        controlModeButton->setVisible(true);
        
        static int hybridPanelImageHeight = 275;
        getWindow()->setSize(numberOfHybridModeBanks*RAVEN_BANK_WIDTH+hybridRightSideImg.getWidth()+hybridLeftSideImg.getWidth(), hybridPanelImageHeight+RAVEN_HYBRID_MODE_TOP_BAR_HEIGHT+RAVEN_HYBRID_MODE_BOTTOM_BAR_HEIGHT);
        
        hybridModeMoveComponent->setTopLeftPosition(hybridLeftSideImg.getWidth(), 0);
        hybridModeMoveComponent->setSize(getWidth()-(hybridRightSideImg.getWidth()+hybridLeftSideImg.getWidth()), hybridModeMoveComponent->getHeight());
        hybridBottomComponent->setTopLeftPosition(hybridLeftSideImg.getWidth(), getHeight()-hybridBottomComponent->getHeight());
        hybridBottomComponent->setSize(getWidth()-(hybridRightSideImg.getWidth()+hybridLeftSideImg.getWidth()), hybridBottomComponent->getHeight());
        hybridTopRightComponent->setTopLeftPosition(getWidth()-hybridTopRightComponent->getWidth()-hybridRightSideImg.getWidth(), 0);
        closeButton->setTopLeftPosition(getWidth()-69, 14);
        
        if(screenWidth-getScreenX() < getWidth())
        {
            hybridX = screenWidth-getWidth()+RAVEN_HYBRID_MODE_SIDE_BAR_WIDTH+1;//plus one is because the right and left side images are different widths by one pixel. French graphics >_<. 
        }
        
        getWindow()->setTopLeftPosition(hybridX, hybridY);
        getWindow()->addToDesktopWithOptions(RAVEN_PALETTE_LEVEL);
        
        for (int i=0; i<banks.size();++i)
        {
            for(int j = 0;j<chansPerBank;j++)
            {
                banks[i]->getChannel(j)->enterHybridMode(numberOfHybridModeBanks);
            }
        }
    }
 
    void enterHybridMode()
    {
        //setVisible(true);
        nonHybridX = getScreenX();
        nonHybridY = getScreenY();
        int xOffset = 0;
        for(int i = 0; i < numBanks; i++)
        {
            banks[i]->setTopLeftPosition(xOffset+hybridLeftSideImg.getWidth(), RAVEN_HYBRID_MODE_TOP_BAR_HEIGHT);
            xOffset += banks[i]->getWidth();
        }
        hybridModeSizing();
    }
    
    void exitHybridMode()
    {
        hybridModeMoveComponent->setVisible(false);
        hybridBottomComponent->setVisible(false);
        hybridTopRightComponent->setVisible(false);
        closeButton->setVisible(false);
        addBankButton->setVisible(false);
        removeBankButton->setVisible(false);
        controlModeButton->setVisible(false);
        int xOffset = 0;
        for(int i = 0; i < numBanks; i++)
        {
            banks[i]->setTopLeftPosition(xOffset, 0);
            xOffset += banks[i]->getWidth();
        }
        for (int i=0; i<banks.size();++i)
        {
            for(int j = 0;j<chansPerBank;j++)
            {
                banks[i]->getChannel(j)->exitHybridMode();
            }
        }
        
        hybridX = getWindow()->getScreenX();
        hybridY = getWindow()->getScreenY();
        //printf("setTopLeftPosition: %d, %d\n", nonHybridX, nonHybridY);
        getWindow()->removeFromDesktop();
        getWindow()->setTopLeftPosition(nonHybridX, nonHybridY);
        getWindow()->setSize(screenWidth, RAVEN_MIXER_HEIGHT);//TODO: add 24 channel dimensions
    }
    
    void addHybridModeBank()
    {
        if(numberOfHybridModeBanks < 4 /*&& RAVEN->isHybridMode()*/)
        {
            hybridX = getScreenX();
            hybridY = getScreenY();
            numberOfHybridModeBanks++;
            hybridModeSizing();
        }
    }
    
    void removeHybridModeBank()
    {
        if(numberOfHybridModeBanks > 1 /*&& RAVEN->isHybridMode()*/)
        {
            hybridX = getScreenX();
            hybridY = getScreenY();
            numberOfHybridModeBanks--;
            hybridModeSizing();
        }
    }
    
    void setControlMode(bool _controlMode)
    {
        printf("controlMode: %d\n", _controlMode);
    }
    
    Component* getHybridModeMoveComponent(){return hybridModeMoveComponent;}
    
    int getNumberOfHybridModeBanks(){ return numberOfHybridModeBanks;}
    
    void unClipMeters()
    {
        for (int i=0; i<banks.size();++i)
        {
            for(int j = 0;j<chansPerBank;j++)
            {
                banks[i]->getChannel(j)->unClipMeter();
            }
        }
    }
    
    void setHybridPanMode(bool _hybridPanMode)
    {
        for (int i=0; i<banks.size();++i)
        {
            for(int j = 0;j<chansPerBank;j++)
            {
                banks[i]->getChannel(j)->setHybridPanMode(_hybridPanMode);
            }
        }
    }
    
    void paintOverChildren(Graphics &g);
    
    void buttonStateChanged (Button* b);
    
    void buttonClicked (Button* b);
    
    void syncInternalTrackSelectWithHUI()
    {
        for(int i = 0;i<numBanks;i++)
        {
            for(int j = 0;j<chansPerBank;j++)
            {
                banks[i]->getChannel(j)->syncInternalTrackSelectWithHUI();
            }
        }
    }
    
    void setHybridPositionParameters(int topLeftX, int topLeftY, int numBanks)
    {
        hybridX = topLeftX;
        hybridY = topLeftY;
        numberOfHybridModeBanks = numBanks;
    }
    int getHybridX() {return hybridX;}
    int getHybridY() {return hybridY;}
    int getNumHybridBanks() {return numberOfHybridModeBanks;}
    
private:
    OwnedArray<RavenMixerBankComponent> banks;
    //RavenMixerProxyComponent proxyComponent;
    
    ScopedPointer<MouseHideComponent> mouseHider;
    
    int numberOfHybridModeBanks;
    int hybridX, hybridY, nonHybridX, nonHybridY;
    
    RavenButton* closeButton;
    
    ScopedPointer<RavenImageComponent> hybridModeMoveComponent;
    ScopedPointer<RavenImageComponent> hybridBottomComponent;
    ScopedPointer<RavenImageComponent> hybridTopRightComponent;
    Image hybridLeftSideImg, hybridRightSideImg, hybridHeaderLabelImg, hybridTopRightImg;
    
    RavenButton *addBankButton;
    RavenButton *removeBankButton;
    RavenButton *controlModeButton;
    
    //ThreadPool meterThreadPool;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenMixerComponent)
};

#endif   // __JUCER_HEADER_RAVENMIXERCOMPONENT_RAVENMIXERCOMPONENT_931F6D3B__