

#ifndef Raven_ASCIICGKeyCode_h
#define Raven_ASCIICGKeyCode_h

class ASCIICGKeyCode
{
    //http://boredzo.org/blog/wp-content/uploads/2007/05/imtx-virtual-keycodes-thumb.png
public:
    //static int getCGKeyCode(const char &key)
    static int getCGKeyCode(const int keyInt)
    {
        printf("getCGKeyCode: %d\n", keyInt);
        if(keyInt == 63234) return 123;//left
        if(keyInt == 63235) return 124; //right
        if(keyInt == 63233) return 125;//down
        if(keyInt == 63232) return 126; //up
        if(keyInt == 63276) return 116;//page up
        if(keyInt == 63277) return 121;//page down
        if(keyInt == 9)     return 48;//tab
        if(keyInt == 127)   return 51;//delete
        if(keyInt == 196640)      return 82;//numpad 0
        if(keyInt == 196641)      return 83;//numpad 1
        if(keyInt == 196642)      return 84;//numpad 2
        if(keyInt == 196643)      return 85;//numpad 3
        if(keyInt == 196644)      return 86;//numpad 4
        if(keyInt == 196645)      return 87;//numpad 5
        if(keyInt == 196646)      return 88;//numpad 6
        if(keyInt == 196647)      return 89;//numpad 7
        //skips one randomly for return. Not a mistake:
        if(keyInt == 196648)      return 91;//numpad 8
        if(keyInt == 196649)      return 92;//numpad 9
        if(keyInt == 196655)      return 65;//numpad .
        if(keyInt == 196650)      return 69;//numpad +
        if(keyInt == 196651)      return 78;//numpad -
        if(keyInt == 196652)      return 67;//numpad *
        if(keyInt == 196653)      return 75;//numpad /
        if(keyInt == 196656)      return 81;//numpad =
        if(keyInt == 63289)      return 71;//numpad clear
        
        
        /////////////////////////////////////////
        const char key = (char)keyInt;
        
        //*
        if(key == 'a' || key == 'A')  return 0;
        if(key == 's' || key == 'S')  return 1;
        if(key == 'd' || key == 'D')  return 2;
        if(key == 'f' || key == 'F')  return 3;
        if(key == 'h' || key == 'H')  return 4;
        if(key == 'g' || key == 'G')  return 5;
        if(key == 'z' || key == 'Z')  return 6;
        if(key == 'x' || key == 'X')  return 7;
        if(key == 'c' || key == 'C')  return 8;
        if(key == 'v' || key == 'V')  return 9;
        if(key == 'b' || key == 'B')  return 11;
        if(key == 'q' || key == 'Q')  return 12;
        if(key == 'w' || key == 'W')  return 13;
        if(key == 'e' || key == 'E')  return 14;
        if(key == 'r' || key == 'R')  return 15;
        if(key == 'y' || key == 'Y')  return 16;
        if(key == 't' || key == 'T')  return 17;
        if(key == '1')                return 18;
        if(key == '2')                return 19;
        if(key == '3')                return 20;
        if(key == '4')                return 21;
        if(key == '6')                return 22;
        if(key == '5')                return 23;
        if(key == '=')                return 24;
        if(key == '9')                return 25;
        if(key == '7')                return 26;
        if(key == '-')                return 27;
        if(key == '+')                return 24;
        if(key == '8')                return 28;
        if(key == '0')                return 29;
        if(key == ']')                return 30;
        if(key == 'o' || key == 'O')  return 31;
        if(key == 'u' || key == 'U')  return 32;
        if(key == '[')                return 33;
        if(key == 'i' || key == 'I')  return 34;
        if(key == 'p' || key == 'P')  return 35;
        if(key == 'l' || key == 'L')  return 37;
        if(key == 'j' || key == 'J')  return 38;
        //if(key == ''')              return 39;
        if(key == 'k' || key == 'K')  return 40;
        if(key == ';')                return 41;
        if(key == '\\')               return 42;
        if(key == ',')                return 43;
        if(key == '/')                return 44;
        if(key == 'n' || key == 'N')  return 45;
        if(key == 'm' || key == 'M')  return 46;
        if(key == '.')                return 47;
        if(key == ' ')                return 49;
        if(key == '`')                return 50;
        
        
        //*/
        
        /* num pad on full keyboard
         if (strcmp(keyString, "TAB") == 0) return 48;
         if (strcmp(keyString, "SPACE") == 0) return 49;
         if (strcmp(keyString, "`") == 0) return 50;
         if (strcmp(keyString, "DELETE") == 0) return 51;
         if (strcmp(keyString, "ENTER") == 0) return 52;
         if (strcmp(keyString, "ESCAPE") == 0) return 53;
         */
        
        /*
         if (strcmp(keyString, ".") == 0) return 65;
         
         if (strcmp(keyString, "*") == 0) return 67;
         
         if (strcmp(keyString, "+") == 0) return 69;
         
         if (strcmp(keyString, "CLEAR") == 0) return 71;
         
         if (strcmp(keyString, "/") == 0) return 75;
         if (strcmp(keyString, "ENTER") == 0) return 76;  // numberpad on full kbd
         
         if (strcmp(keyString, "=") == 0) return 78;
         
         if (strcmp(keyString, "=") == 0) return 81;
         if (strcmp(keyString, "0") == 0) return 82;
         if (strcmp(keyString, "1") == 0) return 83;
         if (strcmp(keyString, "2") == 0) return 84;
         if (strcmp(keyString, "3") == 0) return 85;
         if (strcmp(keyString, "4") == 0) return 86;
         if (strcmp(keyString, "5") == 0) return 87;
         if (strcmp(keyString, "6") == 0) return 88;
         if (strcmp(keyString, "7") == 0) return 89;
         
         if (strcmp(keyString, "8") == 0) return 91;
         if (strcmp(keyString, "9") == 0) return 92;
         */
        
        
        
        /*
         if (strcmp(keyString, "F5") == 0) return 96;
         if (strcmp(keyString, "F6") == 0) return 97;
         if (strcmp(keyString, "F7") == 0) return 98;
         if (strcmp(keyString, "F3") == 0) return 99;
         if (strcmp(keyString, "F8") == 0) return 100;
         if (strcmp(keyString, "F9") == 0) return 101;
         
         if (strcmp(keyString, "F11") == 0) return 103;
         
         if (strcmp(keyString, "F13") == 0) return 105;
         
         if (strcmp(keyString, "F14") == 0) return 107;
         
         if (strcmp(keyString, "F10") == 0) return 109;
         
         if (strcmp(keyString, "F12") == 0) return 111;
         
         if (strcmp(keyString, "F15") == 0) return 113;
         if (strcmp(keyString, "HELP") == 0) return 114;
         if (strcmp(keyString, "HOME") == 0) return 115;
         if (strcmp(keyString, "PGUP") == 0) return 116;
         if (strcmp(keyString, "DELETE") == 0) return 117;
         if (strcmp(keyString, "F4") == 0) return 118;
         if (strcmp(keyString, "END") == 0) return 119;
         if (strcmp(keyString, "F2") == 0) return 120;
         if (strcmp(keyString, "PGDN") == 0) return 121;
         if (strcmp(keyString, "F1") == 0) return 122;
         if (strcmp(keyString, "LEFT") == 0) return 123;
         if (strcmp(keyString, "RIGHT") == 0) return 124;
         if (strcmp(keyString, "DOWN") == 0) return 125;
         if (strcmp(keyString, "UP") == 0) return 126;
         */
        
        return 9999;
    }
};

#endif
