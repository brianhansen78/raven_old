
#include "RavenMixerChannelComponent.h"
#include "RavenGUI.h"
#include "RavenRackContainer.h"

RavenMixerChannelComponent::RavenMixerChannelComponent (NscGuiContainer *owner, int bank, int chan)
: NscGuiElement(owner), mOwner(owner), mBank(0), mChan(0), trackSelected(false), blank(true), touchRecordEnabled(false), lastSelectTime(0), blankFrames(0), panMode(false), ignoreNextUnBlank(false), lastAutomationString("Off ")
{
    
    hybridFaderTrackImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FloatingMixer/FloatingMixerGraphics/MixFader/Guide-Fader-Mix.png"));
    hybridFineFaderTrackImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FloatingMixer/FloatingMixerGraphics/MixFader/Guide-Fader-Mix-HiPower.png"));
    hybridMixerPanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FloatingMixer/Mix-channel-background.png"));
    
    if(RAVEN_24)
    {
        mixerPanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixBackground/Background-Mix_noName24.png"));
        faderTrackImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixFader/Guide-Fader-Mix.png"));
        levelDisplayImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixDisplay/Display-Level-Mix.png"));
        nameDisplayImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixDisplay/Display-Level-Mix.png"));
        panLevelDisplayImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixDisplay/Display-Pan-Level-Mix.png"));
    }
    else
    {
        mixerPanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixBackground/Background-Mix.png"));
        faderTrackImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixFader/Guide-Fader-Mix.png"));
        levelDisplayImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixDisplay/Display-Level-Mix.png"));
        nameDisplayImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixDisplay/Display-Level-Mix.png"));
        panLevelDisplayImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixDisplay/Display-Pan-Level-Mix.png"));
    }
        
    setSize(mixerPanelImg.getWidth(),mixerPanelImg.getHeight());

    //invisible
    autoSelectButton = new RavenNSCButton(owner);
    autoSelectButton->setTag(eNSCTrackAutoSwitch);
    ////////////
    
    autoModeButton = new RavenButton();
    autoModeButton->setImagePaths(RavenGUI::getMixerAutoOffButtonPath(true, bank, chan), RavenGUI::getMixerAutoOffButtonPath(false, bank, chan));
    
    if(RAVEN_24)
    {
        autoModeButton->setTopLeftPosition(4, 130);
        autoModeButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/Bt-Auto-On-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/Bt-Auto-Off-Mix.png");
    }
    else
    {
        autoModeButton->setTopLeftPosition(2, 92);
        autoModeButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/Bt-Auto-On-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/Bt-Auto-Off-Mix.png");
    }
    
    ravenTouchButtons.add(autoModeButton);
    autoModeButton->addListener(this);
    
    recButton = new RavenNSCButton(owner);
    //recButton->setClickingTogglesState(true); //doesn't need to be true because HUI will set toggle state
    addAndMakeVisible(recButton);
    recButton->setTag(eNSCTrackRecordSwitch);
    
    if(RAVEN_24)
    {
        recButton->setTopLeftPosition(4, 173);
        //recButton->setTopLeftPosition(4, autoModeButton->getBottom());
        recButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/Bt-Rec-On-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/Bt-Rec-Off-Mix.png");
    }
    else
    {
        //recButton->setTopLeftPosition(2, autoModeButton->getBottom()-1);
        recButton->setTopLeftPosition(2, 117);
        recButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/Bt-Rec-On-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/Bt-Rec-Off-Mix.png");
    }
    ravenNSCTouchButtons.add(recButton);

    insButtonNSC = new RavenNSCButton(owner);
    insButtonNSC->setName("insertButtonNSC");
    insButtonNSC->setTag(eNSCTrackInsertSwitch);
    
    insButton = new RavenButton();
    insButton->setName("insertButton");
    insButton->setClickingTogglesState(true);
    insButton->setTopLeftPosition(recButton->getRight(), autoModeButton->getBottom());
    
    if(RAVEN_24)
    {
        insButton->setTopLeftPosition(43, 173);
        //insButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/Bt-Ins-On-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/Bt-Ins-Off-Mix.png");
        insButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/Bt-Ins-On-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/Bt-Ins-Disable-Mix.png");
    }
    else
    {
        //insButton->setTopLeftPosition(recButton->getRight(), autoModeButton->getBottom());
        insButton->setTopLeftPosition(30,117);
        //insButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/Bt-Ins-On-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/Bt-Ins-Off-Mix.png");
        insButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/Bt-Ins-On-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/Bt-Ins-Disable-Mix.png");
    }
    
    addAndMakeVisible(insButton);
    ravenTouchButtons.add(insButton);
    insButton->addListener(this);
    
    soloButton = new RavenNSCButton(owner);
    soloButton->setClickingTogglesState(true);
    soloButton->setTag(eNSCTrackSoloSwitch);
    if(RAVEN_24)
    {
        soloButton->setTopLeftPosition(4, recButton->getBottom());
        soloButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/Bt-Solo-On-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/Bt-Solo-Off-Mix.png");
    }
    else
    {
        //soloButton->setTopLeftPosition(2, recButton->getBottom());
        soloButton->setTopLeftPosition(2, 149);
        soloButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/Bt-Solo-On-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/Bt-Solo-Off-Mix.png");
    }
    addAndMakeVisible(soloButton);
    
    ravenNSCTouchButtons.add(soloButton);
    
    muteButton = new RavenNSCButton(owner);
    muteButton->setClickingTogglesState(true);
    muteButton->setTopLeftPosition(soloButton->getRight(), insButton->getBottom());
    addAndMakeVisible(muteButton);
    muteButton->setTag(eNSCTrackMuteSwitch);

    if(RAVEN_24)
    {
        muteButton->setTopLeftPosition(43, insButton->getBottom());
        muteButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/Bt-Mute-On-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/Bt-Mute-Off-Mix.png");
    }
    else
    {
        muteButton->setTopLeftPosition(soloButton->getRight(), insButton->getBottom());
        muteButton->setTopLeftPosition(30, 149);
        muteButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/Bt-Mute-On-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/Bt-Mute-Off-Mix.png");
    }

    ravenNSCTouchButtons.add(muteButton);
    
    selectButton = new RavenNSCButton(owner);
    selectButton->setName("trackSelectButton");
    selectButton->addListener(this);
    selectButton->setClickingTogglesState(true);
    addAndMakeVisible(selectButton);
    selectButton->setTag(eNSCTrackSelectSwitch);
    selectButton->setImagePaths("TRACKFOCUSLOGO/images/TrackFocus-Logo_ON.png", "TRACKFOCUSLOGO/images/TrackFocus-Logo_ON.png");
    //ravenNSCTouchButtons.add(selectButton);//we're checking this in checkSelectButtonTouch() now

    if(RAVEN_24)
        selectButton->setTopLeftPosition(4, 300);
    else
        selectButton->setTopLeftPosition(1, soloButton->getBottom()+24
                                         );
    
    meterL = new RavenMeter(owner, bank, chan, false);
    addAndMakeVisible(meterL);
    meterL->setTag(eNSCTrackMeter1);
    
    meterR = new RavenMeter(owner, bank, chan, true);
    addAndMakeVisible(meterR);
    meterR->setTag(eNSCTrackMeter2);
    
    if(RAVEN_24)
    {
        meterL->setTopLeftPosition(0, 425);
        meterR->setTopLeftPosition(meterL->getRight(), 425);
    }
    else
    {
        meterL->setTopLeftPosition(0, 297);
        meterR->setTopLeftPosition(meterL->getRight(), 297);
    }
    
    knob = new RavenKnob(owner, bank, chan);
    knob->setKnobType(eKnobMixerMono);
    if(RAVEN_24)
        knob->setTopLeftPosition(4, 7);
    else
        knob->setTopLeftPosition(1, 6);
    
    knob->setValue(0.5);
    addAndMakeVisible(knob);
    knob->setTag(eNSCTrackVPotValue);
    knob->addListener(this); // to udpdate pan labels
    
    if(RAVEN_24)
    {
        fader = new RavenMixerFader(owner, 297, bank, chan);
        //faderThread = new RavenFaderThread(fader);
        fader->setTopLeftPosition(22, 260);
    }
    else
    {
        fader = new RavenMixerFader(owner, 223, bank, chan);
        //faderThread = new RavenFaderThread(fader);
        fader->setTopLeftPosition(16, 174);
    }
    
    fader->addListener(this);
    fader->setTag(eNSCTrackFaderValue);
    fader->setValue(fader0dbVal, sendNotification);
    
    panLLabel = new RavenLabel();
    if(RAVEN_24)
        panLLabel->setBounds(8, 88, 28, 23);
    else
        panLLabel->setBounds(2, 64, 28, 23);
    
    Font font = panLLabel->getFont();
    font.setHeight(14.0f);
    panLLabel->setFont(font);
    panLLabel->setJustificationType(Justification::centredBottom);
    panLLabel->setText("0", false);
    panLLabel->setEditable(false);
    addAndMakeVisible(panLLabel);
    
    panRLabel = new RavenLabel();
    
    if(RAVEN_24)
        panRLabel->setBounds(44, 88, 28, 23);
    else
        panRLabel->setBounds(getWidth()/2, 64, 28, 23);
    
    panRLabel->setFont(font);
    panRLabel->setJustificationType(Justification::centredBottom);
    panRLabel->setText("0", false);
    panRLabel->setEditable(false);
    addAndMakeVisible(panRLabel);
    
    knob->setPanLLabel(panLLabel);
    knob->setPanRLabel(panRLabel);
    
    dBLabel = new RavenLabel();
    if(RAVEN_24)
        dBLabel->setBounds(2, 272, getWidth()-4, 27);
    else
        dBLabel->setBounds(1, 178, getWidth()-2, 27);

    dBLabel->setFont(font);
    dBLabel->setJustificationType(Justification::centredBottom);
    dBLabel->setText("-inf dB", false);
    addAndMakeVisible(dBLabel);
    
    fader->setDBLabel(dBLabel);

    trackName = new RavenFuturaTextDisplay(owner);
    trackName->setColour(Label::textColourId, Colours::white);
    trackName->setFontSize(22);
    //trackName->setFontSize(10);
    //trackName->setFont(font);
    trackName->setBounds(2, 272, getWidth()-4, 27);
    addAndMakeVisible(trackName);
    trackName->setTag(eNSCTrackName);
    trackName->setChannelIndex(chan);
    trackName->setBankIndex(bank);
    trackName->setVisible(false);
    
    addAndMakeVisible(fader);
    
    autoComp = new RavenAutomationComponent(bank, chan);
    
    if(RAVEN_24)
        autoComp->setTopLeftPosition(0, autoModeButton->getBottom()-10);
    else
        autoComp->setTopLeftPosition(0, autoModeButton->getBottom()-10);
    
    addChildComponent(autoComp);
    //childTouchComponents.add(autoComp);
    autoComp->setAutoButtonListener(this);
    
    //add fader after everything else so that it's on top of the labels
    
    addAndMakeVisible(autoModeButton);
    
    //pan mode
    /*
    panModeButton = new RavenNSCButton(owner);
    panModeButton->setBankIndex(bank);
    panModeButton->setChannelIndex(kNscGuiBankGlobalIdentifier);
    panModeButton->setTag(eNSCGSPTBankMode_Pan);
    panModeButton->setTopLeftPosition(50, 110);
    ravenNSCTouchButtons.add(panModeButton); // don't add to touch buttons because channel index will be over written
    addAndMakeVisible(panModeButton); // hide for now
     */
    
    setOpaque(true);
    
    dBLabel->setBufferedToImage(true);
    
    lastSelectTime = Time::getCurrentTime();
    
    setBlank(true);//initialize everything to blank until it receives a name
}

RavenMixerChannelComponent::~RavenMixerChannelComponent()
{
    
}

bool RavenMixerChannelComponent::checkSelectButtonTouch(TUIO::TuioCursor *tcur)
{
    //check the select button touch making sure that the fader isn't near it in the Y dimension.
    int threshold = 60;//pixels
    if(abs((selectButton->getScreenY()+selectButton->getHeight()/2) - fader->getCapCenterScreenY()) >= threshold)
    {
        if(componentTouched(selectButton, tcur, false))
        {
            //printf("select center: %d, fader center: %d, fader screen: %d\n", (selectButton->getScreenY()+selectButton->getHeight()/2), fader->getCapCenterScreenY());
            //selectButton->triggerClick();
            return true;
        }
    }
    return false;
}

bool RavenMixerChannelComponent::checkTrackSelectTouch(int touchX, int touchY)
{
    if(isVisible() == false){return false;}
    int minX = getScreenX();
    int maxX = minX + getWidth();
    int minY = dBLabel->getScreenY() +  dBLabel->getHeight();
    int maxY = getScreenY() + getHeight();
    //printf("screen y, %d height %d\n", getScreenY(), getHeight());
    
    if(touchX >= minX && touchX <= maxX && touchY >= minY && touchY <= maxY)
    {
        //check that its not within # pixels of fader cap
        int faderCenterY = fader->getCapCenterScreenY();
        int halfCapHeight = FADER_HALF_HEIGHT_32;
        if(RAVEN_24) halfCapHeight= FADER_HALF_HEIGHT_24;
        if(RAVEN->isHybridMode()) halfCapHeight = FADER_HALF_HEIGHT_32_HYBRID;
        int thumbY1 = faderCenterY - halfCapHeight;
        int thumbY2 = faderCenterY + halfCapHeight;
        int pixelsAboveBelow = halfCapHeight/2;
        if(touchY >= thumbY1 - pixelsAboveBelow && touchY <= thumbY2 + pixelsAboveBelow) return false;
        //else
        return true;
    }
    //else
    return false;
}

void RavenMixerChannelComponent::setBlankMaster(bool _blank)
{
    ignoreNextUnBlank = false;
    setBlank(_blank);
    if(_blank == true)
        ignoreNextUnBlank = true;
}

void RavenMixerChannelComponent::setBlank(bool _blank)
{
    if(RAVEN->isHybridMode())
    {
        if(_blank == true)
        {
            autoComp->setVisible(false);
            autoSelectButton->setVisible(false);
            autoModeButton->setVisible(false);
            recButton->setVisible(false);
            insButton->setVisible(false);
            soloButton->setVisible(false);
            muteButton->setVisible(false);
            selectButton->setVisible(false);
            meterL->setVisible(false);
            meterR->setVisible(false);
            knob->setVisible(false);
            fader->setVisible(false);
            panLLabel->setVisible(false);
            panRLabel->setVisible(false);
            dBLabel->setVisible(false);
            trackName->setVisible(false);
        }
        else
        {
            if(ignoreNextUnBlank == false)
            {
                if(RAVEN->isHybridPanMode())
                {
                    autoComp->setVisible(false);
                    recButton->setVisible(false);
                    insButton->setVisible(false);
                    soloButton->setVisible(false);
                    muteButton->setVisible(false);
                    autoSelectButton->setVisible(false);
                    autoModeButton->setVisible(false);
                    if(!knob->isBlank())
                    {
                        knob->setVisible(true);
                        panLLabel->setVisible(true);
                        panRLabel->setVisible(true);
                    }
                }
                else
                {
                    recButton->setVisible(true);
                    insButton->setVisible(true);
                    soloButton->setVisible(true);
                    muteButton->setVisible(true);
                    autoSelectButton->setVisible(true);
                    autoModeButton->setVisible(true);
                }

                fader->setVisible(true);
                dBLabel->setVisible(true);
                trackName->setVisible(true);
                
                meterL->setVisible(true);
                meterR->setVisible(true);

                selectButton->setVisible(false);
            }
        }
    }
    else//RAVEN is not hybrid mode
    {
        if(_blank == true)
        {
            autoComp->setVisible(false);
            autoSelectButton->setVisible(false);
            autoModeButton->setVisible(false);
            recButton->setVisible(false);
            insButton->setVisible(false);
            soloButton->setVisible(false);
            muteButton->setVisible(false);
            selectButton->setVisible(false);
            meterL->setVisible(false);
            meterR->setVisible(false);
            knob->setVisible(false);
            fader->setVisible(false);
            panLLabel->setVisible(false);
            panRLabel->setVisible(false);
            dBLabel->setVisible(false);
            trackName->setVisible(false);
        }
        else
        {
            if(ignoreNextUnBlank == false)
            {
                autoSelectButton->setVisible(true);
                autoModeButton->setVisible(true);
                recButton->setVisible(true);
                insButton->setVisible(true);
                soloButton->setVisible(true);
                muteButton->setVisible(true);
                selectButton->setVisible(true);
                meterL->setVisible(true);
                meterR->setVisible(true);
                fader->setVisible(true);
                dBLabel->setVisible(true);
                trackName->setVisible(false);
                if(!knob->isBlank())
                {
                    knob->setVisible(true);
                    panLLabel->setVisible(true);
                    panRLabel->setVisible(true);
                }
            }
        }
    }
    
    

    if(blank != _blank)
    {
        blank = _blank;
        repaint();
    }
    
    ignoreNextUnBlank = false;
}

void RavenMixerChannelComponent::paint (Graphics &g)
{
    //g.fillAll(Colour(0, 0, 0));
    if(RAVEN->isHybridMode())
    {
        g.drawImageAt(hybridMixerPanelImg, 0, 0);
        if(!blank)
        {
            g.setOpacity(1.0);
            g.drawImageAt(levelDisplayImg, 2, 86);
            if(RAVEN->isHybridPanMode())
            {
                if(!knob->isBlank())
                {
                    g.drawImageAt(panLevelDisplayImg, 2, 65);
                    if(panMode)
                    {
                        g.setColour(Colours::yellow);
                        g.drawRect(64, 53, 10, 13, 1);
                    }
                }
            }
            if(fineFader)
            {
                g.drawImageAt(hybridFineFaderTrackImg, 32, 105);
            }
            else
            {
                g.drawImageAt(hybridFaderTrackImg, 32, 105);
            }
        }
    }
    else if(RAVEN_24)
    {
        g.drawImageAt(mixerPanelImg, 0, 0);
        if(!blank)
        {
            g.drawImageAt(levelDisplayImg, 4, 275);
            if(!knob->isBlank() && !RAVEN->isHybridMode())
            {
                g.drawImageAt(panLevelDisplayImg, 4, 88);
                if(panMode)
                {
                    g.setColour(Colours::yellow);
                    g.drawRect(66, 76, 10, 13, 1);
                }
            }
            if(fineFader)
            {
                g.drawImageAt(faderTrackImg, 28, 306);
                g.setColour(Colours::yellow);
                g.drawRect(5, 277, 70, 23);
            }
            else
            {
                g.drawImageAt(faderTrackImg, 34, 317);
            }
        }
    }
    else
    {
        g.drawImageAt(mixerPanelImg, 0, 0);
        //        g.setOpacity(0.63);
        if(!blank)
        {
            g.setOpacity(1.0);
            g.drawImageAt(levelDisplayImg, 2, 186);
            if(!knob->isBlank())
            {
                g.drawImageAt(panLevelDisplayImg, 2, 68);
                if(panMode)
                {
                    g.setColour(Colours::yellow);
                    g.drawRect(49, 58, 10, 12, 1);
                }
            }
            if(fineFader)
            {
                g.drawImageAt(faderTrackImg, 21, 211);
                g.setColour(Colours::yellow);
                //g.drawRect(dBLabel->getX()+1, dBLabel->getY()+10, dBLabel->getWidth()-1, dBLabel->getHeight()-10, 1);
                g.drawRect(2, 182, 56, 24);
            }
            else
            {
                g.drawImageAt(faderTrackImg, 26, 217);
            }
        }
    }
    
    if(trackSelected)
    {
        g.setColour(Colours::cyan);
        if(RAVEN->isHybridMode()){ g.drawRect(1, 3, getWidth()-1, getHeight()-RAVEN_HYBRID_MODE_BOTTOM_BAR_HEIGHT+2, 1);}
        else{ g.drawRect(1, 4, getWidth()-1, getHeight()-4, 1);}
        trackName->setColour(Label::textColourId, Colours::cyan);
    }
    else
    {
        trackName->setColour(Label::textColourId, Colours::white);
    }
    

}

void RavenMixerChannelComponent::enterFlipMode()
{
    fader->setEnabled(false);
    fader->setAlpha(0.4);
    knob->setEnabled(false);
    knob->setAlpha(0.4);
    muteButton->enabled = false;
    muteButton->setAlpha(0.4);
    
    fader->setTag(-1);
    knob->setTag(-1);
    muteButton->setTag(-1);
    //prevent touches
}
void RavenMixerChannelComponent::exitFlipMode()
{
    fader->setTag(eNSCTrackFaderValue);
    knob->setTag(eNSCTrackVPotValue);
    muteButton->setTag(eNSCTrackMuteSwitch);
    //enable touches
    
    fader->setEnabled(true);
    fader->setAlpha(1.0);
    knob->setEnabled(true);
    knob->setAlpha(1.0);
    muteButton->enabled = true;
    muteButton->setAlpha(1.0);
}

void RavenMixerChannelComponent::flipAlpha(bool on)
{
    if(on)
    {
        fader->setAlpha(1.0);
        knob->setAlpha(1.0);
        muteButton->setAlpha(1.0);
    }
    else
    {
        fader->setAlpha(0.4);
        knob->setAlpha(0.4);
        muteButton->setAlpha(0.4);
    }
}

void RavenMixerChannelComponent::setAutomationImage(const String &message, bool needToHideAutomationComponent)
{
    if(needToHideAutomationComponent)
    {
        link->set(eNSCTrackEvent, eNSCTrackAutoSwitch, mBank, mChan, 0);
        autoComp->setVisible(false);
    }

    if(message == "Off " || message == "Read" || message == "Tch " || message == "Ltch" || message == "Wrt " || message == "Trim")
    {
            //blankFrames = 0;
            //setBlank(false);
    }
    
    if(message != lastAutomationString)
    {
        if(RAVEN_24)
        {
            if(message == "Off " || message == "    ")
            {
                autoModeButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/Bt-Auto-On-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/Bt-Auto-Off-Mix.png");
            }
            else if(message == "Read")
            {
                autoModeButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Read-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Read-Mix.png");
            }
            else if(message == "Tch ")
            {
                autoModeButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Touch-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Touch-Mix.png");
            }
            else if(message == "Ltch")
            {
                autoModeButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Latch-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Latch-Mix.png");
            }
            else if(message == "Wrt ")
            {
                autoModeButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Write-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Write-Mix.png");
            }
            else if(message == "Trim")
            {
                autoModeButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Trim-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Trim-Mix.png");
            }
        }
        else //!RAVEN_24
        {
            if(message == "Off " || message == "    ")
            {
                autoModeButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/Bt-Auto-On-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/Bt-Auto-Off-Mix.png");
            }
            else if(message == "Read")
            {
                autoModeButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Read-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Read-Mix.png");
            }
            else if(message == "Tch ")
            {
                autoModeButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Touch-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Touch-Mix.png");
            }
            else if(message == "Ltch")
            {
                autoModeButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Latch-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Latch-Mix.png");
            }
            else if(message == "Wrt ")
            {
                autoModeButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Write-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Write-Mix.png");
            }
            else if(message == "Trim")
            {
                autoModeButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Trim-Mix.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixButtons/AUTO_VAriation/Bt-Auto-Trim-Mix.png");
            }
        }
        repaint();
    }
    lastAutomationString = message;
}

void RavenMixerChannelComponent::setFineFaderImage(bool ff)
{
    if(RAVEN_24)
    {
        if(ff)
        {
            faderTrackImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/GlowingFaders/GlowingFaders24/Guide-Fader-Mix-HiPower.png"));
//            lowPowerFaderTrackImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/GlowingFaders/GlowingFaders24/Guide-Fader-Mix-LoPower.png"));
        }
        else
        {
            faderTrackImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixFader/Guide-Fader-Mix.png"));
        }
    }
    else
    {
        if(ff)
        {
            faderTrackImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/GlowingFaders/GlowingFaders32/Guide-Fader-Mix-HiPower.png"));
//            lowPowerFaderTrackImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/GlowingFaders/GlowingFaders32/Guide-Fader-Mix-LoPower.png"));
        }
        else
        {
            faderTrackImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixFader/Guide-Fader-Mix.png"));
        }
    }
    //repaint();
}

void RavenMixerChannelComponent::buttonStateChanged (Button* b)
{    
    if (b == selectButton)
    {
        
        //Time currentTime = Time::getCurrentTime();
        //if(currentTime.toMilliseconds() - lastSelectTime.toMilliseconds() > 1000)
        {
            //printf("BUTTON STATE is setting track %d select to %d\n", mChan, selectButton->getToggleState());
            selectTrack(selectButton->getToggleState());
            //lastSelectTime = Time::getCurrentTime();
        }
        
    }
}

void RavenMixerChannelComponent::buttonClicked (Button* b)
{
    if(b == insButton)
    {
        if(insButton->getToggleState())
        {
            insButtonNSC->triggerClick();
        }
    }
    else if(b == autoModeButton)
    {
        if(autoComp->isVisible())
        {
            link->set(eNSCTrackEvent, eNSCTrackAutoSwitch, mBank, mChan, 0);
        }
        else
        {
            link->set(eNSCTrackEvent, eNSCTrackAutoSwitch, mBank, mChan, 1);
        }
        autoComp->setVisible(!autoComp->isVisible());
    }
    else if(b->getName() == "auto_off")
    {
        link->set(eNSCGlobalControl, eNSCGSAutomationOffSwitch, 0, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSAutomationOffSwitch, 0, 0, 0);
        link->set(eNSCGlobalControl, eNSCGSAutomationOffSwitch, 1, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSAutomationOffSwitch, 1, 0, 0);
        link->set(eNSCGlobalControl, eNSCGSAutomationOffSwitch, 2, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSAutomationOffSwitch, 2, 0, 0);
        if(!RAVEN_24)
        {
            link->set(eNSCGlobalControl, eNSCGSAutomationOffSwitch, 3, 0, 1);
            link->set(eNSCGlobalControl, eNSCGSAutomationOffSwitch, 3, 0, 0);
        }
        sendActionMessage("Off ");
    }
    else if(b->getName() == "auto_read")
    {
        link->set(eNSCGlobalControl, eNSCGSAutomationReadSwitch, 0, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSAutomationReadSwitch, 0, 0, 0);
        link->set(eNSCGlobalControl, eNSCGSAutomationReadSwitch, 1, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSAutomationReadSwitch, 1, 0, 0);
        link->set(eNSCGlobalControl, eNSCGSAutomationReadSwitch, 2, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSAutomationReadSwitch, 2, 0, 0);
        if(!RAVEN_24)
        {
            link->set(eNSCGlobalControl, eNSCGSAutomationReadSwitch, 3, 0, 1);
            link->set(eNSCGlobalControl, eNSCGSAutomationReadSwitch, 3, 0, 0);
        }
        sendActionMessage("Read");
    }
    else if(b->getName() == "auto_touch")
    {
        link->set(eNSCGlobalControl, eNSCGSAutomationTouchSwitch, 0, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSAutomationTouchSwitch, 0, 0, 0);
        link->set(eNSCGlobalControl, eNSCGSAutomationTouchSwitch, 1, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSAutomationTouchSwitch, 1, 0, 0);
        link->set(eNSCGlobalControl, eNSCGSAutomationTouchSwitch, 2, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSAutomationTouchSwitch, 2, 0, 0);
        if(!RAVEN_24)
        {
            link->set(eNSCGlobalControl, eNSCGSAutomationTouchSwitch, 3, 0, 1);
            link->set(eNSCGlobalControl, eNSCGSAutomationTouchSwitch, 3, 0, 0);
        }
        sendActionMessage("Tch ");
    }
    else if(b->getName() == "auto_latch")
    {
        link->set(eNSCGlobalControl, eNSCGSAutomationLatchSwitch, 0, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSAutomationLatchSwitch, 0, 0, 0);
        link->set(eNSCGlobalControl, eNSCGSAutomationLatchSwitch, 1, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSAutomationLatchSwitch, 1, 0, 0);
        link->set(eNSCGlobalControl, eNSCGSAutomationLatchSwitch, 2, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSAutomationLatchSwitch, 2, 0, 0);
        if(!RAVEN_24)
        {
            link->set(eNSCGlobalControl, eNSCGSAutomationLatchSwitch, 3, 0, 1);
            link->set(eNSCGlobalControl, eNSCGSAutomationLatchSwitch, 3, 0, 0);
        }
        sendActionMessage("Ltch");
    }
    else if(b->getName() == "auto_write")
    {
        link->set(eNSCGlobalControl, eNSCGSAutomationWriteSwitch, 0, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSAutomationWriteSwitch, 0, 0, 0);
        link->set(eNSCGlobalControl, eNSCGSAutomationWriteSwitch, 1, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSAutomationWriteSwitch, 1, 0, 0);
        link->set(eNSCGlobalControl, eNSCGSAutomationWriteSwitch, 2, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSAutomationWriteSwitch, 2, 0, 0);
        if(!RAVEN_24)
        {
            link->set(eNSCGlobalControl, eNSCGSAutomationWriteSwitch, 3, 0, 1);
            link->set(eNSCGlobalControl, eNSCGSAutomationWriteSwitch, 3, 0, 0);
        }
        sendActionMessage("Wrt ");
    }
    else if(b->getName() == "auto_trim")
    {
//        link->set(eNSCGlobalControl, eNSCGSAutomationTrimSwitch, 0, 0, 1);
//        link->set(eNSCGlobalControl, eNSCGSAutomationTrimSwitch, 0, 0, 0);
//        link->set(eNSCGlobalControl, eNSCGSAutomationTrimSwitch, 1, 0, 1);
//        link->set(eNSCGlobalControl, eNSCGSAutomationTrimSwitch, 1, 0, 0);
//        link->set(eNSCGlobalControl, eNSCGSAutomationTrimSwitch, 2, 0, 1);
//        link->set(eNSCGlobalControl, eNSCGSAutomationTrimSwitch, 2, 0, 0);
//        if(!RAVEN_24)
//        {
//            link->set(eNSCGlobalControl, eNSCGSAutomationTrimSwitch, 3, 0, 1);
//            link->set(eNSCGlobalControl, eNSCGSAutomationTrimSwitch, 3, 0, 0);
//        }
        sendActionMessage("Trim");
    }
    else if(b->getName() == "auto_status")
    {
        link->set(eNSCGlobalControl, eNSCGSAutoStatus, 0, 0, 1);
        //link->set(eNSCGlobalControl, eNSCGSAutoStatus, 0, 0, 0);
        link->set(eNSCGlobalControl, eNSCGSAutoStatus, 1, 0, 1);
        //link->set(eNSCGlobalControl, eNSCGSAutoStatus, 1, 0, 0);
        link->set(eNSCGlobalControl, eNSCGSAutoStatus, 2, 0, 1);
        //link->set(eNSCGlobalControl, eNSCGSAutoStatus, 2, 0, 0);
        if(!RAVEN_24)
        {
            link->set(eNSCGlobalControl, eNSCGSAutoStatus, 3, 0, 1);
            //link->set(eNSCGlobalControl, eNSCGSAutoStatus, 3, 0, 0);
        }
        sendActionMessage("STATUS Status Selected");
    }
}

void RavenMixerChannelComponent::sliderValueChanged (Slider *slider)
{
    if(slider == knob)
    {
        knob->updatePanning();
    }
    
    else if(slider == fader)
    {
        //printf("slider val = %f\n", fader->getValue());
        float val = fader->getDBVal();
        int strlen = 4;
        if(val > 0 && val < 10) strlen = 3;
        if(val < -10) strlen = 5;
        dBLabel->setText(String(val).substring(0, strlen) + " dB", false);
    }
    
}
void RavenMixerChannelComponent::sliderDragStarted (Slider *slider)
{
    //printf("slider drag started\n");
      
    if(slider == knob)
    {   
        knob->syncPanning();
    }
}
void RavenMixerChannelComponent::sliderDragEnded (Slider *slider)
{
    if(slider == knob) knob->isBeingTouched = false;
}

void RavenMixerChannelComponent::beginFader()
{
//    printf("slider began\n");
    fader->isBeingTouched = true;
    mOwner->sliderBegan(fader);
    
//    if(RAVEN->isHybridMode())
//    {
//        LOCK_JUCE_THREAD
//        {
//            dBLabel->setVisible(true);
//            trackName->setVisible(false);
//        }
//    }
}

void RavenMixerChannelComponent::endFader()
{
    mOwner->sliderEnded(fader);
    fader->isBeingTouched = false;
    
//    if(RAVEN->isHybridMode())
//    {
//        LOCK_JUCE_THREAD
//        {
//            dBLabel->setVisible(false);
//            trackName->setVisible(true);
//        }
//    }
}

void RavenMixerChannelComponent::setKnobBlank(bool _blank)
{
    knob->setBlank(_blank);
//    if(!RAVEN->isHybridPanMode())
//    {
//        //knob->setVisible(false);
//    }
}

void RavenMixerChannelComponent::enterHybridMode(int _numberOfHybridModeBanks)
{
    if(mBank >= _numberOfHybridModeBanks){ setVisible(false);}
    else{ setVisible(true);}
    
    autoModeButton->setTopLeftPosition(2, 2);
    autoComp->setTopLeftPosition(0, autoModeButton->getBottom()-10);
    soloButton->setTopLeftPosition(2, 59);
    recButton->setTopLeftPosition(2, 27);
    insButton->setTopLeftPosition(30,27);
    muteButton->setTopLeftPosition(30, 59);
    fader->setTopLeftPosition(29, 70);
    dBLabel->setBounds(1, soloButton->getBottom()-10, getWidth()-2, 24);
    trackName->setBounds(1, getHeight()-29, getWidth()-2, 24);
    meterL->setHybridMode(true);
    meterR->setHybridMode(true);
    meterL->setTopLeftPosition(0, 107);
    meterR->setTopLeftPosition(meterL->getRight(), 107);
    
    panLLabel->setBounds(2, 62, 28, 23);
    panRLabel->setBounds(getWidth()/2, 62, 28, 23);
    
    //knob->setTopLeftPosition(1, 2);
    
    if(!RAVEN->isHybridPanMode())
    {
        knob->setHidden(true);
    }
      
    fader->setHybridMode(true);
    
    setBlank(blank);
    repaint();
}

void RavenMixerChannelComponent::exitHybridMode()
{
    setVisible(true);
    autoModeButton->setTopLeftPosition(2, 92);
    autoComp->setTopLeftPosition(0, autoModeButton->getBottom()-10);
    soloButton->setTopLeftPosition(2, 149);
    recButton->setTopLeftPosition(2, 117);
    insButton->setTopLeftPosition(30,117);
    muteButton->setTopLeftPosition(soloButton->getRight(), insButton->getBottom());
    fader->setTopLeftPosition(16, 174);
    dBLabel->setBounds(1, 178, getWidth()-2, 27);
    meterL->setHybridMode(false);
    meterR->setHybridMode(false);
    meterL->setTopLeftPosition(0, 297);
    meterR->setTopLeftPosition(meterL->getRight(), 297);
    
//    if(RAVEN_24)
//        knob->setTopLeftPosition(4, 7);
//    else
//        knob->setTopLeftPosition(1, 6);
    
    if(RAVEN_24)
    {
        panLLabel->setBounds(8, 88, 28, 23);
        panRLabel->setBounds(44, 88, 28, 23);
    }
    else
    {
        panLLabel->setBounds(2, 64, 28, 23);
        panRLabel->setBounds(getWidth()/2, 64, 28, 23);

    }
        
    fader->setHybridMode(false);
    knob->setHidden(false);
    
    setBlank(blank);
    repaint();
}

