
#include "RavenMacUtilities.h"

#import <AppKit/NSApplication.h>
#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <Carbon/Carbon.h>

//#import "RavenConfig.h"

int currentWinNum = 0;
int RavenMacUtilities::lastX = 0;
int RavenMacUtilities::lastY = 0;
int RavenMacUtilities::initialX = 0;
int RavenMacUtilities::initialY = 0;

void RavenMacUtilities::turnOnEventMonitor()
{
    //turn this on to print all system mouse events
    static bool once = false;
    if(once == false)
    {
        //http://www.cocoabuilder.com/archive/cocoa/63996-best-way-to-refuse-events.html
        [NSEvent addLocalMonitorForEventsMatchingMask:NSAnyEventMask handler:^(NSEvent*  event) {
            NSLog(@"NS event: %@", event/*, NSStringFromPoint([mouseEvent locationInWindow])*/);
            return (NSEvent *)nil;
        }];
        once = true;
    }
}

bool RavenMacUtilities::checkHostName(const char* serverName)
{
    NSString *serverStr = [NSString stringWithUTF8String:serverName];
    NSString *localName = [[NSHost currentHost] localizedName];
    return [localName isEqualToString:serverStr];
    

    
    //test to auto-hide dock, not working...
    //SetSystemUIMode(kUIModeContentSuppressed, 0);// (makes the Dock auto-hide)
    //SetSystemUIMode(kUIModeContentHidden, 0); //(makes the Dock go away)
    
}

bool RavenMacUtilities::checkSurroundPannerTouch (int touchX, int touchY)
{
    @autoreleasepool
    {
        NSWorkspace *workspace = [NSWorkspace sharedWorkspace];
        NSArray *appArray = [workspace runningApplications];
        
        int numApps = [appArray count];
        for(int i = 0; i < numApps; i++)
        {
            NSRunningApplication *app = [appArray objectAtIndex: i];
            if([app isActive])
            {
                
                NSString *appName = [app localizedName];
                
                //CGWindowID ravenWinID;
                
                //get list of all windows to determine Raven Window ID
                CFArrayRef allWindowList = CGWindowListCopyWindowInfo(kCGWindowListOptionOnScreenOnly, kCGNullWindowID);
                int numWins = CFArrayGetCount(allWindowList);
                for(int j = 0; j < numWins; j++)
                {
                    CFDictionaryRef win = (CFDictionaryRef)CFArrayGetValueAtIndex(allWindowList, j);
                    
                    NSString *winOwnerName = (NSString*)CFDictionaryGetValue(win, kCGWindowOwnerName);
                    CFNumberRef winLayerRef = (CFNumberRef)CFDictionaryGetValue(win, kCGWindowLayer);
                    int winLayer;
                    CFNumberGetValue(winLayerRef, kCFNumberIntType, &winLayer);
                    if( [winOwnerName isEqualToString: appName] && winLayer >= 3) // 3 is PT plug-in level (Raven on 2)
                    {
                        
                        CFDictionaryRef boundsDictionary = (CFDictionaryRef)CFDictionaryGetValue(win, kCGWindowBounds);
                        
                        CGRect boundsRect;
                        CGRectMakeWithDictionaryRepresentation(boundsDictionary, &boundsRect);
                        
                        int x1 = boundsRect.origin.x;
                        int y1 = boundsRect.origin.y;
                        //int x2 = x1 + boundsRect.size.width;
                        //int y2 = y1 + boundsRect.size.height;
                        
                        //the relative location of the touch on the window
                        int relativeX = touchX-x1;
                        int relativeY = touchY-y1;
                        //printf("x1: %d, y1: %d, touchX: %d, touchY: %d, relative X %d, relative y %d\n", x1, y1, touchX, touchY, relativeX, relativeY);
                        
                        //55-250 y bounds of mono panner
                        //13-210 x bounds of mono panner
                        
                        if((boundsRect.size.width == 308 && boundsRect.size.height == 508) || (boundsRect.size.width == 378 && boundsRect.size.height == 508))
                        {
                            //is mono surround panner
                            if(relativeX >= 13 && relativeX <= 210 && relativeY >= 55 && relativeY <= 250)
                            {
                                return true;
                            }
                        }
                        else if((boundsRect.size.width == 527 && boundsRect.size.height == 508)
                                || (boundsRect.size.width == 597 && boundsRect.size.height == 508))
                        {
                            printf("isStereoPanner\n");
                            //is stereo panner
                            if((relativeX >= 13 && relativeX <= 210 && relativeY >= 55 && relativeY <= 250)
                               || (relativeX >= 315 && relativeX <= 520 && relativeY >= 55 && relativeY <= 250))
                            {
                                printf("surround panning true\n");
                                return true;
                            }
                        }
                        else
                        {
                            return false;
                        }
                        //NSLog(@"%@", win);
                    }
                }
            }
        }
        return false;
    } //end autorelease pool
}

void RavenMacUtilities::touchMouseDown(int touchX, int touchY)
{
    
    initialX = touchX;
    initialY = touchY;
    
    @autoreleasepool
    {
        NSWorkspace *workspace = [NSWorkspace sharedWorkspace];
        NSArray *appArray = [workspace runningApplications];
        int numApps = [appArray count];
        for(int i = 0; i < numApps; i++)
        {
            NSRunningApplication *app = [appArray objectAtIndex: i];
            if([app isActive])
            {
                NSString *appName = [app localizedName];
                CFArrayRef allWindowList = CGWindowListCopyWindowInfo(kCGWindowListOptionOnScreenOnly, kCGNullWindowID);
                int numWins = CFArrayGetCount(allWindowList);
                for(int j = 0; j < numWins; j++)
                {
                    CFDictionaryRef win = (CFDictionaryRef)CFArrayGetValueAtIndex(allWindowList, j);
                    NSString *winOwnerName = (NSString*)CFDictionaryGetValue(win, kCGWindowOwnerName);
                    CFNumberRef winLayerRef = (CFNumberRef)CFDictionaryGetValue(win, kCGWindowLayer);
                    CFNumberRef winNumRef = (CFNumberRef)CFDictionaryGetValue(win, kCGWindowNumber);
                    int winLayer;
                    CFNumberGetValue(winLayerRef, kCFNumberIntType, &winLayer);
                    CFNumberGetValue(winNumRef, kCFNumberIntType, &currentWinNum);
                    if( [winOwnerName isEqualToString: appName] && winLayer >= 3) // 3 is PT plug-in level (Raven on 2)
                    {
                        CFDictionaryRef boundsDictionary = (CFDictionaryRef)CFDictionaryGetValue(win, kCGWindowBounds);
                        CGRect boundsRect;
                        CGRectMakeWithDictionaryRepresentation(boundsDictionary, &boundsRect);
                        
                        int x1 = boundsRect.origin.x;
                        int y1 = boundsRect.origin.y;
                        int x2 = x1 + boundsRect.size.width;
                        int y2 = boundsRect.origin.y + boundsRect.size.height;
                        
                        if(touchX >= x1 && touchX <= x2 && touchY >= y1 && touchY <= y2)
                        {
                            //NSLog(@"%@", win);
                            //CGDisplayMoveCursorToPoint(1, CGPointMake(touchX, touchY));
                            CGEventSourceRef src = CGEventSourceCreate(kCGEventSourceStateCombinedSessionState);
                            CGEventRef mouseDown = CGEventCreateMouseEvent(src, kCGEventLeftMouseDown, CGPointMake(touchX,touchY), kCGMouseButtonLeft);
                            
                            CGEventPost(kCGSessionEventTap, mouseDown);
                            NSEvent            *theEvent;
                            theEvent = [NSEvent mouseEventWithType:NSLeftMouseDown
                                        location:NSMakePoint(touchX,touchY)
                                        modifierFlags:0
                                        timestamp:1
                                        windowNumber: currentWinNum
                                        context: [NSGraphicsContext currentContext]
                                        eventNumber:1
                                        clickCount:1
                                        pressure:0.0];
                            
                            CGEventRef eventRef = [theEvent CGEvent];
                            CGEventPost(kCGHIDEventTap, eventRef);
                            
                        }
                    }
                }
            }
        }
        //return false;
    } //end autorelease pool
    setLastXY(touchX, touchY);
}

void RavenMacUtilities::touchMouseDrag (int touchX, int touchY)
{
    @autoreleasepool
    {
        
        /*
        CGEventSourceRef src = CGEventSourceCreate(kCGEventSourceStateCombinedSessionState); // kCGEventSourceStateHIDSystemState
        CGEventRef mouseDrag = CGEventCreateMouseEvent(src, kCGEventLeftMouseDragged, CGPointMake(touchX, touchY), kCGMouseButtonLeft);
        CGEventPost(kCGSessionEventTap, mouseDrag); // kCGHIDEventTap
         */ 
        
        NSEvent            *theEvent;
        
        theEvent =  [NSEvent mouseEventWithType:NSLeftMouseDragged //masked?
                    location:NSMakePoint(touchX,touchY)
                    modifierFlags:0
                    timestamp:1
                    windowNumber: currentWinNum
                    context:[NSGraphicsContext currentContext]
                    eventNumber:1
                    clickCount:1
                    pressure:0.0];
        
        CGEventRef eventRef = [theEvent CGEvent];
        
        int deltaX = touchX-lastX;
        int deltaY = touchY-lastY;
        
        CGEventSetDoubleValueField(eventRef, kCGMouseEventDeltaX, deltaX);
        CGEventSetDoubleValueField(eventRef, kCGMouseEventDeltaY, deltaY);
        
        CGEventPost(kCGHIDEventTap, eventRef);
    }
    setLastXY(touchX, touchY);
    //printf("mouse draggggg\n");
}

void RavenMacUtilities::surroundPannerDrag (int touchX, int touchY)
{
    @autoreleasepool
    {
        
        /*
         CGEventSourceRef src = CGEventSourceCreate(kCGEventSourceStateCombinedSessionState); // kCGEventSourceStateHIDSystemState
         CGEventRef mouseDrag = CGEventCreateMouseEvent(src, kCGEventLeftMouseDragged, CGPointMake(touchX, touchY), kCGMouseButtonLeft);
         CGEventPost(kCGSessionEventTap, mouseDrag); // kCGHIDEventTap
         */
        
        NSEvent            *theEvent;
        
        theEvent =  [NSEvent mouseEventWithType:NSLeftMouseDragged //masked?
                                       location:NSMakePoint(touchX,touchY)
                                  modifierFlags:0
                                      timestamp:1
                                   windowNumber: currentWinNum
                                        context:[NSGraphicsContext currentContext]
                                    eventNumber:1
                                     clickCount:1
                                       pressure:0.0];
        
        CGEventRef eventRef = [theEvent CGEvent];
        
        int deltaX = touchX-lastX;
        int deltaY = touchY-lastY;
        
        CGEventSetDoubleValueField(eventRef, kCGMouseEventDeltaX, deltaX);
        CGEventSetDoubleValueField(eventRef, kCGMouseEventDeltaY, deltaY);
        
        CGEventPost(kCGHIDEventTap, eventRef);
    }
    setLastXY(touchX, touchY);
    //printf("mouse draggggg\n");
}

void RavenMacUtilities::touchMouseUp (int touchX, int touchY)
{
    @autoreleasepool
    {
        /*
        CGEventSourceRef src = CGEventSourceCreate(kCGEventSourceStateCombinedSessionState);
        CGEventRef mouseUp = CGEventCreateMouseEvent(src, kCGEventLeftMouseUp, CGPointMake(touchX, touchY), kCGMouseButtonLeft);
        CGEventPost(kCGSessionEventTap, mouseUp);
         */
        
        NSEvent            *theEvent;
        
        theEvent = [NSEvent mouseEventWithType:NSLeftMouseUp //mask???
                    location:NSMakePoint(touchX,touchY)
                    modifierFlags:0
                    timestamp:1
                    windowNumber: currentWinNum
                    context:[NSGraphicsContext currentContext]
                    eventNumber:1
                    clickCount:1
                    pressure:0.0];
        
        CGEventRef eventRef = [theEvent CGEvent];
        CGEventPost(kCGHIDEventTap, eventRef);
    }
    setLastXY(touchX, touchY);
    //printf("mouse touch UP\n");
}

void RavenMacUtilities::setLastXY (int touchX, int touchY)
{
    lastX = touchX;
    lastY = touchY;
}


void RavenMacUtilities::touchMouseClick(int touchX, int touchY)
{
    //http://stackoverflow.com/questions/2734117/simulating-mouse-input-programmatically-in-os-x
    CGEventRef clickDown = CGEventCreateMouseEvent(NULL, kCGEventLeftMouseDown, CGPointMake(touchX, touchY), kCGMouseButtonLeft);
    CGEventRef clickUp = CGEventCreateMouseEvent(NULL, kCGEventLeftMouseUp, CGPointMake(touchX, touchY), kCGMouseButtonLeft);
    CGEventPost(kCGHIDEventTap, clickDown);
    CGEventPost(kCGHIDEventTap, clickUp);
    CFRelease(clickDown);
    CFRelease(clickUp);
}

///////////////////////////////////////MacKeyPress
bool MacKeyPress::isCommandDown()
{
    return CGEventSourceKeyState(kCGEventSourceStateCombinedSessionState, 55);  //kCGEventSourceStateCombinedSessionState
}

bool MacKeyPress::isShiftDown()
{
    return CGEventSourceKeyState(kCGEventSourceStateCombinedSessionState, 56);
}

bool MacKeyPress::isOptionDown()
{
    return CGEventSourceKeyState(kCGEventSourceStateCombinedSessionState, 58);
}

bool MacKeyPress::isControlDown()
{
    return CGEventSourceKeyState(kCGEventSourceStateCombinedSessionState, 59);
}

void MacKeyPress::toggleModifier(ERavenModiferKey key, bool down)
{
    CGEventSourceRef src = CGEventSourceCreate(kCGEventSourceStateHIDSystemState/*kCGEventSourceStateCombinedSessionState*/); ////kCGEventSourceStateCombinedSessionState
    CGEventRef keyToggle;
    
    if(key == eRavenModifer_Command)
    {
        keyToggle = CGEventCreateKeyboardEvent(src, 55, down);
        //CGEventSetFlags(keyToggle, kCGEventFlagMaskCommand | CGEventGetFlags(keyToggle));
    }
    else if(key == eRavenModifer_Shift)
    {
        keyToggle = CGEventCreateKeyboardEvent(src, 56, down);
        //CGEventSetFlags(keyToggle, kCGEventFlagMaskShift | CGEventGetFlags(keyToggle));
    }
    else if(key == eRavenModifer_Option)
    {
        keyToggle = CGEventCreateKeyboardEvent(src, 58, down);
        //CGEventSetFlags(keyToggle, kCGEventFlagMaskAlternate | CGEventGetFlags(keyToggle));
    }
    else if(key == eRavenModifer_Control)
    {
        keyToggle = CGEventCreateKeyboardEvent(src, 59, down);
        //CGEventSetFlags(keyToggle, kCGEventFlagMaskControl | CGEventGetFlags(keyToggle));
    }
    
    CGEventTapLocation loc = kCGSessionEventTap;//kCGHIDEventTap; // kCGSessionEventTap also works //kCGHIDEventTap
    CGEventPost(loc, keyToggle);
    
    CFRelease(keyToggle);
    CFRelease(src);
}

void MacKeyPress::pressCombo(const std::vector<int> &keyCodes)
{
    int numKeys = keyCodes.size();
    
    //    printf("key codes: ");
    //    for(int i = 0;i<keyCodes.size();i++)
    //    {
    //        printf("%d ", keyCodes[i]);
    //    }
    //    printf("\n");
    
    CGEventSourceRef src = CGEventSourceCreate(kCGEventSourceStateHIDSystemState);
    //CGEventSourceRef src = CGEventSourceCreate(kCGEventSourceStateCombinedSessionState);
    CGEventRef keyDowns[numKeys];
    CGEventRef keyUps[numKeys];
    
    for(int i = 0; i < numKeys; i++)
    {
        keyDowns[i] = CGEventCreateKeyboardEvent(src, (CGKeyCode)keyCodes[i], true);
        CGEventSetFlags(keyDowns[i], 0);
        keyUps[i] = CGEventCreateKeyboardEvent(src, (CGKeyCode)keyCodes[i], false);
        CGEventSetFlags(keyUps[i], 0);
    }
    
    for(int i = 0; i < numKeys; i++)
    {
        if(keyCodes[i] == 55) //command
        {
            CGEventSetFlags(keyDowns[numKeys-1], kCGEventFlagMaskCommand | CGEventGetFlags(keyDowns[numKeys-1]));
            CGEventSetFlags(keyUps[numKeys-1], kCGEventFlagMaskCommand | CGEventGetFlags(keyUps[numKeys-1]));
        }
        else if(keyCodes[i] == 56) //shift
        {
            CGEventSetFlags(keyDowns[numKeys-1], kCGEventFlagMaskShift | CGEventGetFlags(keyDowns[numKeys-1]));
            CGEventSetFlags(keyUps[numKeys-1], kCGEventFlagMaskShift | CGEventGetFlags(keyUps[numKeys-1]));
        }
        else if(keyCodes[i] == 58) //alternate
        {
            CGEventSetFlags(keyDowns[numKeys-1], kCGEventFlagMaskAlternate | CGEventGetFlags(keyDowns[numKeys-1]));
            CGEventSetFlags(keyUps[numKeys-1], kCGEventFlagMaskAlternate | CGEventGetFlags(keyUps[numKeys-1]));
        }
        else if(keyCodes[i] == 59) //control
        {
            CGEventSetFlags(keyDowns[numKeys-1], kCGEventFlagMaskControl | CGEventGetFlags(keyDowns[numKeys-1]));
            CGEventSetFlags(keyUps[numKeys-1], kCGEventFlagMaskControl | CGEventGetFlags(keyUps[numKeys-1]));
        }
    }
    
    NSWorkspace *workspace = [NSWorkspace sharedWorkspace];
    NSArray *appArray = [workspace runningApplications];
    int numApps = [appArray count];
    int process_id = -999;
    ProcessSerialNumber psn;
    for(int i = 0; i < numApps; i++)
    {
        NSRunningApplication *app = [appArray objectAtIndex: i];
        NSString *appName = [app localizedName];
        //NSLog(@"running app name = %@", appName);
        if([appName isEqualToString: @"Pro Tools"])
            process_id = [app processIdentifier];
    }
    GetProcessForPID(process_id, &psn);
    
    if(process_id == -999)  return;//don't send anything if protools isn't open
    
    //    CGEventTapLocation loc = kCGHIDEventTap; // kCGSessionEventTap also works
    
    for(int i = 0; i < numKeys; i++)
    {
        if(keyCodes[i] != 55 && keyCodes[i] != 56 && keyCodes[i] != 58 && keyCodes[i] != 59)
            CGEventPostToPSN(&psn, keyDowns[i]);
    }
    
    for(int i = numKeys-1; i >= 0; i--)
        CGEventPostToPSN(&psn, keyUps[i]);
    
    for(int i = 0; i < numKeys; i++)
    {
        CFRelease(keyDowns[i]);
        CFRelease(keyUps[i]);
    }
    
    CFRelease(src);
}
