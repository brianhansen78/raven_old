#ifndef Raven_RavenConfig_h
#define Raven_RavenConfig_h

#define	kLocalNscLibraryVersion			0x01070000			// This example is built with Nsc version 1.7.0.0

#define RAVEN_IS_BACKGROUND_APP     1

#define RAVEN_SKIP_INITIALIZATION   0

#define RAVEN_DEBUG                 0

#define RAVEN_NOHUI                 0

#define RAVEN_24                    0

#define RAVEN_ADD_TRANSPARENT_MIXER 0

#define INSERTS_NO_BYPASS           1

#define RAVEN_ESSENTIALS_PALETTE    1

#define RAVEN_HYBRID_MODE           1

#define RAVEN_FRAMES_UNTIL_BLANK    3

#define RAVEN_METER_SMOOTHING       0.0

#define RAVEN_NAVPAD_WAIT_MILLISECONDS 10 

#define RAVEN_BANK_TIMER_MILLISECONDS 60

#define RAVEN_METER_THREAD_MS       15
#define RAVEN_FADER_THREAD_MS       5

#define RAVEN_HYBRID_MODE_FADER_LENGTH  135

#define RAVEN_TOOLBAR_HEIGHT        141

#define RAVEN_HYBRID_MODE_TOP_BAR_HEIGHT    36
#define RAVEN_HYBRID_MODE_SIDE_BAR_WIDTH    11  //The right side image is actually 12 pixels so if this causes problems we should split them out. 
#define RAVEN_HYBRID_MODE_BOTTOM_BAR_HEIGHT 14
#define MOVE_MODULE_DOWNSAMPLE_AMT  5
#define FLOATING_DOWNSAMPLE_AMT     5

#define RAVEN_RACK_LEVEL            2
#define RAVEN_PALETTE_LEVEL         4
#define RAVEN_HOTKEY_LEVEL          5
#define RAVEN_MOVER_LEVEL           6
#define RAVEN_ALERT_LEVEL           7

#define RETURN_MIX_MODE             1
#define RETURN_DAW_MODE             2

#if RAVEN_24
const int numBanks = 3;
#define RAVEN_NUMBER_OF_ICONS       16
#define KNOB_CENTER_OFFSET          15

#define MAX_NUM_TOUCHES             6
#define RAVEN_BANK_WIDTH            (640)
#define RAVEN_MIXER_HEIGHT          484//<--NOT correct
#else
const int numBanks = 4;
#define RAVEN_NUMBER_OF_ICONS       16
#define KNOB_CENTER_OFFSET          12
#define MAX_NUM_TOUCHES             12
#define RAVEN_BANK_WIDTH            (480)
#define RAVEN_MIXER_HEIGHT          484
#endif

#define RAVEN_HIDE_MOUSE            Desktop::getInstance().getMainMouseSource().ravenShowMouseCursor(MouseCursor::NoCursor);

//hold button timers
#define RAVEN_STOP_BUTTON_HOLD_MS   1000

const int chansPerBank = 8;

const int screenWidth = 1920;
const int screenHeight = 1080;
const int osxMenuBarHeight = 22;

const int huiWaitMs = 200; // any lower than 200ms lead to unreliable HUI sending for inserts palette
const int huiWaitMsLong = 400; // any lower than 400ms lead to unreliable HUI sending for Flip Mode Initialization
const int huiWaitMsExtraLong = 600;//some things in initialization seem to need extra time (to prevent sends channels from being initialized to non-zero random numbers, etc.)
const int huiTrackSelectWaitMs = 400;// any lower than 400ms will cause a rename track command to be sent.

const int largeMeterStageBottoms[] = {271, 265, 260, 254, 248, 242, 236, 230, 224, 219, 213, 207, 201, 196, 190, 184, 178, 173, 167, 161, 155, 150, 144, 138, 132, 126, 121, 115, 109, 104, 98, 92, 86, 80, 75, 69, 63, 57, 51, 46, 40, 34, 29, 20};

//startup sequence stuff
const int numberOfBlinksToDeterminePanMode = 4;
const int numberOfSecondsToCheckForPanMode = 5;

#define LOCK_JUCE_THREAD const MessageManagerLock lock;if(lock.lockWasGained())

#define GUI_PATH File::getSpecialLocation(File::currentApplicationFile).getFullPathName() + "/Contents/Resources/Raven_GUI_Resources/"

#endif
