
#include "RavenContainer.h"
#include "MacHideBackgroundMouse.h"
#include "RavenMacUtilities.h"
#include "RavenThreadPoolJobs.h"

//Startup Timer
const int timerInterval = 50;

enum
{
    eUpdateTUIOTimerID,
    eStartupTimerID,
    eFlipModeTimerID,
    eInitSendsTimerID,
    eCheckForPanModeTimerID,
    eExitPanModeTimerID,
    eAutomationStatusTimerID,
    eEverySecondTimerID,
    eStopButtonTimerID
};

#define DESKTOP Desktop::getInstance()

//==============================================================================
RavenContainer::RavenContainer(RavenLoadingComponent *loadCmp) :
    activePQGesture(false),
    activeNavPadGesture(false),
    isPQSplitGesture(false),
    loadingWindow(loadCmp),
    touchesOn(true),
    lastTapPos(0, 0),
    prevTime(0),
    isMovingRackModules(false),
    prevSelectedBank(-1),
    prevSelectedChannel(-1),
    initTouchBank(-1),
    initTouchChannel(-1),
    initCommandState(false),
    initShiftState(false),
    inFlipMode(false),
    blockFlipSwitch(false),
    waitingForAutomationLabels(false),
    temporarilyBlockAllNameSwitching(false),
    temporarilyBlockCertainNames(false),
    meterPool(numBanks),
    largeMeterPool(numBanks),
    faderNSCPool(numBanks),
    faderTouchPool(numBanks),
    sendsFaderTouchPool(numBanks),
    sendsFaderNSCPool(numBanks)
#if RAVEN_ADD_TRANSPARENT_MIXER
    ,transparentFaderTouchPool(numBanks)
    ,transparentFaderNSCPool(numBanks)
#endif
{
    
    #if !RAVEN_DEBUG
    // This is essentially a hack to hide the mouse cursor in an OSX background only application
    MacHideBackgroundMouse::hideMouse();
    #endif
    
    lastExitFlipModeTime = Time::getCurrentTime();
    
    //RavenMacUtilities::turnOnEventMonitor();
    
    //construct the static rack container
    RAVEN->init();
    
    mixer = RAVEN->getMixer();
    sends = RAVEN->getSends();
    names = RAVEN->getTrackNames();
    floatingEssentials = RAVEN->getEssentialsPalette();
    mixerWindow = RAVEN->getMixer()->getWindow();
    sendsWindow = RAVEN->getSends()->getWindow();
    namesWindow = RAVEN->getTrackNames()->getWindow();
    functionsManager = RAVEN->getFunctionsManager();
    
    for(int b = 0; b < numBanks; b++)
    {
//      LargeMeterThreadPoolJob *largeMeterJob = new LargeMeterThreadPoolJob(sends, b);
//      largeMeterPool.addJob(largeMeterJob, true);
        
        MeterThreadPoolJob *meterJob = new MeterThreadPoolJob(mixer, b);
        meterPool.addJob(meterJob, true);
        
        FaderNSCThreadPoolJob *faderNSCJob = new FaderNSCThreadPoolJob(mixer, b); //incoming NSC for fader automation
        faderNSCPool.addJob(faderNSCJob, true);
        
        FaderTouchThreadPoolJob *faderTouchJob = new FaderTouchThreadPoolJob(mixer, b);
        faderTouchPool.addJob(faderTouchJob, true);
        
        SendsFaderNSCThreadPoolJob *sendsFaderNSCJob = new SendsFaderNSCThreadPoolJob(sends, b);
        sendsFaderNSCPool.addJob(sendsFaderNSCJob, true);
        
        SendsFaderTouchThreadPoolJob *sendsFaderTouchJob = new SendsFaderTouchThreadPoolJob(sends, b);
        sendsFaderTouchPool.addJob(sendsFaderTouchJob, true);
        
#if RAVEN_ADD_TRANSPARENT_MIXER
        transparentMixer = RAVEN->getTransparentMixer();
        TransparentFaderNSCThreadPoolJob *transparentFaderNSCJob = new TransparentFaderNSCThreadPoolJob(transparentMixer, b);
        transparentFaderNSCPool.addJob(transparentFaderNSCJob, true);
        
        TransparentFaderTouchThreadPoolJob *transparentFaderTouchJob = new TransparentFaderTouchThreadPoolJob(transparentMixer, b);
        transparentFaderTouchPool.addJob(transparentFaderTouchJob, true);
#endif
        onlineModeKeyCode.push_back(55);//control
        onlineModeKeyCode.push_back(38);//J
    }
    
    //initialize touch fields
    for(int i = 0; i < MAX_NUM_TOUCHES; i++)
    {
        touches[i].isRavenRack = false;
        touches[i].isRavenPalette = false;
        touches[i].gesture = eMTOneFinger;
        touches[i].isMainGestureFinger = false;
        touches[i].isPlugInMove = false;
        touches[i].isOtherAppTouch = false;
        touches[i].isSurroundPannerTouch = false;
        touches[i].isFader = false;
        touches[i].isKnob = false;
        touches[i].isButton = false;
        touches[i].isTrackSelect = false;
        touches[i].isTrackDeselect = false;
        touches[i].isModuleMove = false;
        touches[i].isMixer = false;
        touches[i].isSend = false;
        touches[i].isCounter = false;
        touches[i].isInsertsPalette = false;
        touches[i].isEssentials = false;
        touches[i].isNavPad = false;
        touches[i].isFloatingNavPadMove = false;
        touches[i].isFloatingNavPad = false;
        touches[i].isFunction = false;
        touches[i].isAutoMode = false;
        touches[i].isRecEnable = false;
        touches[i].isSoloEnable = false;
        touches[i].isMuteEnable = false;
        touches[i].isFloatingMixerMove = false;
        touches[i].isIconPaletteMove = false;
        touches[i].bank = 999;
        touches[i].chan = 999;
        touches[i].holdButton = 0;
    }
    
    //*//TUIO
    tuioClient = new TUIO::TuioClient(3333);
	tuioClient->addTuioListener(this);
	tuioClient->connect();
    //*/
    
    //PQ Gesture SDK
    //Set Gesture received callback
    PQ_SDK_MultiTouch::SetOnReceiveGesture(&onReceivePQGesture, this);
    // connect server
    int err_code = PQMTE_SUCCESS;
    printf("connect to MT server...\n");
	if((err_code = PQ_SDK_MultiTouch::ConnectServer()) != PQMTE_SUCCESS)
    {
		printf("connect to MT server fail\n");
	}
	// send request to server
	printf("MT server connect success...\n");
    PQ_SDK_MultiTouch::TouchClientRequest tcq = {0}; // check ref for this..
	tcq.type = RQST_GESTURE_ALL; //tcq.type = RQST_RAWDATA_ALL | RQST_GESTURE_ALL;
	if((err_code = PQ_SDK_MultiTouch::SendRequest(tcq)) != PQMTE_SUCCESS)
    {
		printf("send MT server request fail\n");// << err_code << endl;
	}
    
    //NSC
	unsigned int nscLibVersion = NSC_GetLibraryVersion();
	if ( nscLibVersion >= kLocalNscLibraryVersion)		// we require version 1.7.0.X or higher
	{
		link = NSC_CreateLink(kLocalNscLibraryVersion, this, eDAWMode_ProTools, eControllerBankVControl, numBanks);
        RAVEN->setNscLink(link);
	}
    
    for(int i = 0; i < numBanks; i++)
        panModeCounters[i] = 0;
    
    functionsManager->addActionListener(this);
    functionsManager->addActionListener(mixer);
    functionsManager->addActionListener(sends);
    functionsManager->getTrackBankComponent()->addActionListener(this);
    
    //if not using HUI then delete the loading window and make the main visible
    if(RAVEN_NOHUI) finishedInitialization();
}

RavenContainer::~RavenContainer()
{
    //stop threads
    meterPool.removeAllJobs (true, 2000);
    largeMeterPool.removeAllJobs (true, 2000);
    faderNSCPool.removeAllJobs (true, 2000);
    faderTouchPool.removeAllJobs(true, 2000);
    sendsFaderTouchPool.removeAllJobs(true, 2000);
    sendsFaderNSCPool.removeAllJobs (true, 2000);
    
    //stop tuio
    tuioClient->disconnect();
    delete tuioClient;
    
    //stop NSC
    NSC_DestroyLink(link);
    //delete link; ?does NSC_DestroyLink() do this?
    
    //delete the Rack Container
    RAVEN->close();
}

void RavenContainer::actionListenerCallback (const String &message)
{
    if(message == "enter meter mode")
    {
        exitFlipMode();
    }
    else if(message == "set mixer automation off")
    {
        RAVEN->setAutomationCheckBlockCounter(0);
        waitingForAutomationLabels = false;
        temporarilyBlockAllNameSwitching = false;
        stopTimer(eAutomationStatusTimerID);
    }
    else if(message == "track left" || message == "track right")
    {
        RAVEN->setAutomationCheckBlockCounter(0);
        waitingForAutomationLabels = false;
        temporarilyBlockAllNameSwitching = false;
        stopTimer(eAutomationStatusTimerID);
    }
    else if(message == "bank left" || message == "bank right")
    {
        RAVEN->setAutomationCheckBlockCounter(-2);
        waitingForAutomationLabels = false;
        temporarilyBlockAllNameSwitching = false;
        stopTimer(eAutomationStatusTimerID);
    }
}

void RavenContainer::autoEnterFlipMode() //prepare mixer/sends/names to handle flip mode state that was set by PT
{
    if(!inFlipMode)
    {
        LOCK_JUCE_THREAD
        {
            names->enterFlipMode();
            mixer->enterFlipMode();
            sends->enterFlipMode();
            functionsManager->enterFlipMode();
            inFlipMode = true;
        }
    }
}

void RavenContainer::enterFlipMode() //Simulate NSC button press to enter flip mode manually by touching sends component
{
    if(!inFlipMode && !blockFlipSwitch)
    {
        LOCK_JUCE_THREAD
        {
            names->enterFlipMode(); // prevents track names from switching to send names (maybe use this to detect flip mode?)
            sends->enterFlipMode();
            mixer->enterFlipMode();
            functionsManager->enterFlipMode();
            functionsManager->getMasterFlipModeButton()->triggerClick();
            mixer->flipAlpha(false);
            
            inFlipMode = true;
            blockFlipSwitch = true;
        }
    }
}

void RavenContainer::exitFlipMode()
{
    if(inFlipMode && !blockFlipSwitch)
    {
        LOCK_JUCE_THREAD
        {
            lastExitFlipModeTime = Time::getCurrentTime();
            names->exitFlipMode();
            sends->exitFlipMode();
            mixer->exitFlipMode();
            functionsManager->exitFlipMode();
            functionsManager->getMasterFlipModeButton()->triggerClick();
            
            inFlipMode = false;
            blockFlipSwitch = true;
        }
    }
}

void RavenContainer::enterHybridMode()
{
    if(!RAVEN->isHybridMode())
    {
        LOCK_JUCE_THREAD
        {
            names->exitFlipMode();
            sends->exitFlipMode();
            mixer->exitFlipMode();
            functionsManager->exitFlipMode();
            functionsManager->getMasterFlipModeButton()->triggerClick();
            
            RAVEN->setHybridMode(true);
        }
    }
}

//checks to make sure components utilizing gestures are on top before we recognize the gesture
//for cases in which there are dual components (specifically the navpad and floating navpad) we have an optional window and secondary windowComponentTouched check
// to only check a component don't pass in a window, to only check a window pass in false for componentTouched
//pass in a boolean for isInFloating essentials if the component receiving the gester is currently in the floating essentials palette (ex. nav pad but not fader)
bool checkPQGesture(int touchX, int touchY, bool componentTouched, RavenWindow *window = 0, bool windowComponentTouched = false, bool isInFloatingEssentials = false)
{    
    //optional check window
    if(window && windowComponentTouched) //if we are checking for a gesture in a window and that window is on top and we are touching the correct area within that window (windowComponentTouched)
    {
        for(int i = DESKTOP.getNumComponents()-1; i >= 0; i--) //check desktop front-to-back order
        {
            Component *win = DESKTOP.getComponent(i);
            if(RavenTouchComponent::componentTouched(win, touchX, touchY) && win->isOnDesktop()) //stop at the first window touched
            {
                if(win == window)  return true;
                else               break;
            }
        }
    }
    
    if(isInFloatingEssentials && componentTouched) //are FE showing without anything covering them?
    {
        for(int i = DESKTOP.getNumComponents()-1; i >= 0; i--) //check desktop front-to-back order
        {
            Component *win = DESKTOP.getComponent(i);
            if(RavenTouchComponent::componentTouched(win, touchX, touchY) && win->isOnDesktop())
            {
                if(win == RAVEN->essentialsWindow)  return true;
                else                                break;
            }
        }
    }
    
    //after this point if componentTouched is not true, then the other checks don't matter, so return false
    if(!componentTouched) return false;
    
    //if we are touching any other palettes, then they are on top and we should block the gesture
    if(RAVEN->paletteTouched(touchX, touchY)) return false;
    
    //check floating mixer touched in hybrid mode
    if(RAVEN->hybridMixerTouched(touchX, touchY)) return false;
    
    //if we are touching any other applications, then they are on top and we should block the gesture
    if(Process::checkOtherAppTouch(touchX, touchY)) return false;
    
    return true;
}

void RavenContainer::onReceivePQGesture(const PQ_SDK_MultiTouch::TouchGesture & ges, void * call_back_object)
{
    
    if(RAVEN->getTouchLockState())  return;
    //NOTE: TUIO addCursor is always called first before this callback...
    
//    std::cout <<"ges,name:"<< GetGestureName(ges) << " type:" << ges.type << ",param size:" << ges.param_size << " ";
//	for(int i = 0; i < ges.param_size; ++ i)
//		std::cout << ges.params[i] << " ";
//    std::cout << std::endl;
    
    //need to check against down/up so the movement can continue outside the navpad bounds
    static int gesPrevX = 0;
    static int gesPrevY = 0;
    
    RavenContainer *container = (RavenContainer*)call_back_object;
    RavenFunctionsManager *funcManager = RAVEN->getFunctionsManager();
    RavenMixerComponent *mixer = RAVEN->getMixer();
    RavenSendsComponent *sends = RAVEN->getSends();
    //PQ add/////////////////////////////////////////////
    //don't want to enable any gestures if in customize mode.
    if(funcManager->isCustomMode() || funcManager->getMoveToggleState())
    {
        container->activePQGesture = false;
        container->activeNavPadGesture = false;
        container->isPQSplitGesture = false;
        return;
    }
    
    int touchX = ges.params[0];
    int touchY = ges.params[1];
    
    if(ges.type == TG_NEAR_PARALLEL_DOWN)
    {
        bool componentTouched = funcManager->checkNavPadTouch(touchX, touchY);
        RavenWindow *window = RAVEN->navpadWindow;
        bool windowComponentTouched = RAVEN->checkFloatingNavPadTap(touchX, touchY);
        bool isInFloatingEssentials = (bool)funcManager->getNavPadComponent()->getProperties().getWithDefault("isInFloatingEssentials", false);
        printf("nav pad in FE = %d\n", isInFloatingEssentials);
        if(checkPQGesture(touchX, touchY, componentTouched, window, windowComponentTouched, isInFloatingEssentials))
        //if(funcManager->checkNavPadTouch(touchX, touchY) || RAVEN->checkFloatingNavPadTap(touchX, touchY))
        {
            LOCK_JUCE_THREAD
            {
                funcManager->exitScrubShuttle();
                container->activePQGesture = true;
                container->activeNavPadGesture = true;
                gesPrevX = ges.params[0];
                gesPrevY = ges.params[1];
            }
        }
    }
    
    else if(ges.type == TG_NEAR_PARALLEL_UP)
    {
        bool componentTouched = funcManager->checkEditToolTouch(touchX, touchY);
        bool isInFloatingEssentials = (bool)funcManager->getEditToolComponent()->getProperties().getWithDefault("isInFloatingEssentials", false);
        if(checkPQGesture(touchX, touchY, componentTouched, 0, false, isInFloatingEssentials))
        //if(funcManager->checkEditToolTouch(touchX, touchY))
        {
            funcManager->getSmartToolButton()->triggerClick();
        }
    }
    
    else if(ges.type == TG_SPLIT_START)
    {
        int touchX2 = ges.params[2];
        int touchY2 = ges.params[3];
        //only process split/pinch on floating(large) navpad
        RavenWindow *window = RAVEN->navpadWindow;
        bool windowComponentTouched = (RAVEN->checkFloatingNavPadTap(touchX, touchY) || RAVEN->checkFloatingNavPadTap(touchX2, touchY2));
        if(checkPQGesture(touchX, touchY, false, window, windowComponentTouched))
        //if(RAVEN->checkFloatingNavPadTap(touchX, touchY) || RAVEN->checkFloatingNavPadTap(touchX2, touchY2))
        {
            container->activePQGesture = true;
            container->activeNavPadGesture = true;
            container->isPQSplitGesture = true;
        }
    }
    else if(ges.type == TG_DB_CLICK)
    {

        //check for stuff on top
        
        for(int b = 0; b < numBanks; b++)
        {
            for(int c = 0; c < chansPerBank; c++)
            {
                bool meterClipTouch = mixer->getBank(b)->getChannel(c)->checkMeterClipTouch(touchX, touchY);
                bool mixerFaderTouch = mixer->getBank(b)->getChannel(c)->checkFaderTouch(touchX, touchY);
                bool mixerKnobTouch = mixer->getBank(b)->getChannel(c)->checkKnobTouch(touchX, touchY);
                bool sendsFaderTouch = sends->getBank(b)->getChannel(c)->checkFaderTouch(touchX, touchY);
                bool sendsKnobTouch = sends->getBank(b)->getChannel(c)->checkKnobTouch(touchX, touchY);
                
                if(checkPQGesture(touchX, touchY, meterClipTouch, RAVEN->getMixer()->getWindow(), meterClipTouch, false))
                //if(mixer->getBank(b)->getChannel(c)->checkMeterClipTouch(touchX, touchY))
                {
                    printf("un-clip meter\n");
                    RavenClearMetersMessage *m = new RavenClearMetersMessage();
                    container->postMessage(m);
                    return;
                }
//                else if(sends->getBank(b)->getChannel(c)->checkMeterClipTouch(touchX, touchY))
//                {
//                    LOCK_JUCE_THREAD
//                    printf("un-clip LARGE meter\n");
//                    return;
//                }
                
                else if(checkPQGesture(touchX, touchY, mixerFaderTouch, RAVEN->getMixer()->getWindow(), mixerFaderTouch))
                //else if(mixer->getBank(b)->getChannel(c)->checkFaderTouch(touchX, touchY))
                {
                    //Thread-safe update
                    LOCK_JUCE_THREAD mixer->getBank(b)->getChannel(c)->zeroFader();
                    return;
                }
                else if(checkPQGesture(touchX, touchY, sendsFaderTouch, RAVEN->getSends()->getWindow(), sendsFaderTouch))
                //else if(sends->getBank(b)->getChannel(c)->checkFaderTouch(touchX, touchY))
                {
                    LOCK_JUCE_THREAD sends->getBank(b)->getChannel(c)->zeroFader();
                    return;
                }
                else if(checkPQGesture(touchX, touchY, mixerKnobTouch, RAVEN->getMixer()->getWindow(), mixerKnobTouch))
                //else if(mixer->getBank(b)->getChannel(c)->checkKnobTouch(touchX, touchY))
                {
                    LOCK_JUCE_THREAD mixer->getBank(b)->getChannel(c)->zeroKnob();
                    printf("zero knob\n");
                    return;
                }
                else if(checkPQGesture(touchX, touchY, sendsKnobTouch, RAVEN->getSends()->getWindow(), sendsKnobTouch))
                //else if(sends->getBank(b)->getChannel(c)->checkKnobTouch(touchX, touchY))
                {
                    LOCK_JUCE_THREAD sends->getBank(b)->getChannel(c)->zeroKnob();
                    return;
                }
            }
        }

        //printf("PQ double click on nothing specific\n");
    }
    //PG_DB_CLICK check and zero peak meters
    //LOCK_JUCE_THREAD mixer->getBank(touches[id].bank)->getChannel(touches[id].chan)->zeroFader();
    
    else if(ges.type == TG_CLICK)
    {
        bool componentTouched = (funcManager->checkNavPadTouch(touchX, touchY) && funcManager->getNavPadComponent()->isScrubOrShuttleMode());
        RavenWindow *window = RAVEN->navpadWindow;
        bool windowComponentTouched = (RAVEN->checkFloatingNavPadTap(touchX, touchY) && funcManager->getNavPadComponent()->isScrubOrShuttleMode());
        bool isInFloatingEssentials = (bool)funcManager->getNavPadComponent()->getProperties().getWithDefault("isInFloatingEssentials", false);
        if(checkPQGesture(touchX, touchY, componentTouched, window, windowComponentTouched, isInFloatingEssentials))
        //if(funcManager->checkNavPadTouch(touchX, touchY) || RAVEN->checkFloatingNavPadTap(touchX, touchY) )
        {
            //if(funcManager->getNavPadComponent()->isScrubOrShuttleMode())
            //{
                LOCK_JUCE_THREAD funcManager->restartScrubOrShuttleMode();
            //}
        }
    }
    
    //PQ update//////////////////////////////////////////
    
    else if(ges.type == TG_SPLIT_APART || ges.type == TG_SPLIT_CLOSE)
    {
        static int prDistX = 0;
        static int prDistY = 0;
        int curDistX = ges.params[4] - ges.params[2];
        int curDistY = ges.params[5] - ges.params[3];
        int deltaX = abs(curDistX - prDistX);
        int deltaY = abs(curDistY - prDistY);
        prDistX = curDistX;
        prDistY = curDistY;
        
        if(container->activeNavPadGesture && container->isPQSplitGesture)
        {
            float velX;
            float velY;
            
            if(deltaY > deltaX){
                velX = 0;
                velY = (float)(ges.params[0]) / 4.f; // / (float)SCREEN_HEIGHT;
                if(ges.type == TG_SPLIT_APART)
                {
                    velY = -velY;
                    if(velY > 0) velY = -velY;
                }
                else
                {
                    if(velY < 0) velY = -velY;
                }
            }else
            {
                velX = (float)(ges.params[0]) / 4.f; // / (float)SCREEN_HEIGHT;
                velY = 0;
                if(ges.type == TG_SPLIT_APART)
                {
                    if(velX < 0) velX = -velX;
                }
                else
                { 
                    velX = -velX;
                    if(velX > 0) velX = -velX;
                }
            }

            funcManager->navPadTouch(0, 0, velX, velY, false, RAVEN_NAVPAD_NUM_AVG_PINCH);
        }
    }

    else if(ges.type == TG_NEAR_PARALLEL_MOVE || ges.type == TG_MOVE) // whats the diff b/t this and TG_NEAR_PARALLEL_MOVE_UP/DOWN/LEFT/RIGHT (see ref)
    {
        if(container->activeNavPadGesture && !container->isPQSplitGesture)
        {
            float velX = (float)(touchX - gesPrevX);// / (float)SCREEN_WIDTH;
            float velY = (float)(touchY - gesPrevY);// / (float)SCREEN_HEIGHT;
            
            //printf("!!!nav pad two finger gesture!!! vel X = %f, vel Y = %f\n", velX, velY);
            funcManager->navPadTouch(0, 0, velX, velY, true);
            
            gesPrevX = touchX;
            gesPrevY = touchY;
        }
    }
    
    //PQ END////////////////////////////////////////////
    
    else if(ges.type == TG_TOUCH_END)
    {        
        container->activePQGesture = false;
        container->activeNavPadGesture = false;
        container->isPQSplitGesture = false;
    }
}

void RavenContainer::releaseFaderByTouchID(int _id)
{
    if(touches[_id].isFader)
    {
        touches[_id].isFader = false;
        int b = touches[_id].bank;
        int c = touches[_id].chan;
        if(touches[_id].isMixer)
        {
            mixer->getBank(b)->getChannel(c)->endFader();
            mixer->getBank(b)->getChannel(c)->releaseFaderTouch();
        }
        if(touches[_id].isSend)
        {
            sends->getBank(b)->getChannel(c)->endFader();
            sends->getBank(b)->getChannel(c)->releaseFaderTouch();
        }
        
        touches[_id].bank = 0;//999;
        touches[_id].chan = 0;//999;
    }
}

void RavenContainer::addTuioCursor(TUIO::TuioCursor *tcur)
{
    if(!touchesOn || activePQGesture) return;
    if(isMovingRackModules) return;
    if(RAVEN->getTouchLockState()) return;
    /*
    //this works for preventing pass-though touches as long as we are not running as "background only"
    //issue: have to touch twice before registering as active again.... a problem for instant button pushed back on raven
    // might still need to collect windows to fix this...
    bool isActive = Process::isForegroundProcess();
    printf("is active = %d\n", (int)isActive);
    if(!isActive) return;0
     */
    
    //test demonstrating that namesWindow resizes when moved and re-drawn.
//    LOCK_JUCE_THREAD
//        namesWindow->setTopLeftPosition(0, namesWindow->getY()+(rand()%100));
    
    int id = tcur->getCursorID();
    if(id > MAX_NUM_TOUCHES-1) return;
    int touchX = tcur->getScreenX(SCREEN_WIDTH);
    int touchY = tcur->getScreenY(SCREEN_HEIGHT);
    touches[id].initTouchX = touchX;
    touches[id].initTouchY = touchY;
    
    //printf("ADD CURSOR\n");
    
    for(int i = 0; i < MAX_NUM_TOUCHES; i++)
    {
        if(touches[i].isFloatingMixerMove) return;
        //might want to add other palette move checks here?
    }
    
    ////////////////////////////////////////////////////////////////////////////// should move this down
    // Move Modules Check ////////////////////////////////////////////////////////
#if !RAVEN_24
    if (functionsManager->getMoveToggleState())
    {
        //start moving a module
        if(!functionsManager->checkMoveToggleTouch(tcur))
        {
            //check for show/hide button
            if(mixer->checkButtonTouch(tcur) || sends->checkButtonTouch(tcur))
            {
                touches[id].isButton = true;
                touches[id].isFunction = true;
                return;
            }
            
            RAVEN->startMoveWindow(touchX, touchY);
            touches[id].isModuleMove = true;
            isMovingRackModules = true;
            
        }
        //unless touched the move modules toggle again to exit move modules...
        else if(functionsManager->checkFunctionsButtonTouched(tcur))
        {
            touches[id].isButton = true;
            touches[id].isFunction = true;
            
            //NOTE: the two booleans set above don't do anything special. Also, they only allow hot keys to be
            //disabled in removeTUIOCursor.
            //
            //touches[id].moveModulesButton = true;
            //
        }
        
        return;
    }
#endif
    
    ///////////////////////////////////////////////////////////////////////////////////
    // Other App and Hoy Key Checks ///////////////////////////////////////////////////
    
    //We check in order of high to low window level, see RAVEN_PALETTE_LEVEL, RAVEN_RACK_LEVEL, etc.
    
    //Hot Key window is modal and must be checked like it is another app
    //printf("checkHotKeyWindow!\n");
    if(RAVEN->checkHotKeyPanelTouch(tcur))    return;
    
    ///////////////////////////////////////////////////////////////////////////////////
    // Floating Palettes Check ////////////////////////////////////////////////////////
    // Touching Raven Palette Check, then make sure ProTools gets activated, then hide mouse
    
    bool floatingMixerTouch = false; //temporary fix to prevent pass through floating mixer to functions
    
    if(RAVEN->paletteTouched(touchX, touchY))
    {
        touches[id].isRavenPalette = true;
        Process::makeProToolsActiveAndShow();
        RAVEN_HIDE_MOUSE
    }
    
    for(int i = DESKTOP.getNumComponents()-1; i >= 0; i--) //check desktop front-to-back order
    {
        Component *cmp = DESKTOP.getComponent(i);
                        
        if(cmp == mixer->getWindow() && RAVEN->isHybridMode())
        {
            if(RavenTouchComponent::componentTouched(cmp, tcur, false, RAVEN_HYBRID_MODE_SIDE_BAR_WIDTH, 0, RAVEN_HYBRID_MODE_SIDE_BAR_WIDTH, RAVEN_HYBRID_MODE_BOTTOM_BAR_HEIGHT))
            {
                floatingMixerTouch = true;
                break; //break, not return because we need to continue to check the mixer touches later in this method
            }
        }
        
        else if (cmp == RAVEN->counterWindow)
        {
            if(RAVEN->checkCounterTouch(tcur))
            {
                touches[id].isCounter = true;
                return;
            }
            
        }
        else if (cmp == RAVEN->essentialsWindow)
        {
            if(RAVEN->checkEssentialsMoveTouch(tcur))
            {
                touches[id].isEssentials = true;
                return;
            }
            // Customize Toolbar Check
            else if (functionsManager->isCustomMode()) //custom mode
            {
                if(functionsManager->checkModuleTouch(tcur, id))
                {
                    touches[id].isModuleMove = true;
                }
                return;
            }
            if(RavenTouchComponent::componentTouched(cmp, tcur))
            {
                if(functionsManager->checkFunctionsButtonTouched(tcur, true))
                {
                    touches[id].isButton = true;
                    touches[id].isFunction = true;
                    return;
                }
            }
            if(functionsManager->checkNavPadTouch(touchX, touchY))
            {
                bool isInFloatingEssentials = (bool)functionsManager->getNavPadComponent()->getProperties().getWithDefault("isInFloatingEssentials", false);
                if(isInFloatingEssentials)
                {
                    touches[id].isNavPad = true;
                    return;
                }
            }
        }
        else if(cmp == RAVEN->navpadWindow)
        {
            if(RAVEN->checkFloatingNavPadMoveTouch(tcur))
            {
                touches[id].isFloatingNavPadMove = true;
                return;
            }
            if(/*RAVEN->checkFloatingNavPadTouch(tcur)*/RAVEN->checkFloatingNavPadComponentTouch(tcur))
            {
                touches[id].isFloatingNavPad = true;
                return;
            }
        }
        else if (cmp == RAVEN->iconPaletteWindow)
        {
            if(RAVEN->checkIconPaletteButtonTouch(tcur))
            {
                touches[id].isIconPalette = true;
                return;
            }
            else if(RAVEN->checkIconPaletteMoveTouch(tcur))
            {
                touches[id].isIconPalette = true;
                touches[id].isIconPaletteMove = true;
                return;
            }
            
        }
        else if (cmp == RAVEN->insertsWindow)
        {
            if(RAVEN->checkInsertsPaletteMoveTouch(tcur))
            {
                touches[id].isInsertsPalette = true;
                return;
            }
            if(RAVEN->checkInsertsPaletteTouch(tcur)) return;
        }
        else if (cmp == RAVEN->settingsWindow)
        {
            if(RAVEN->checkSettingsMoveTouch(tcur))
            {
                touches[id].isSettingsPalette = true;
                return;
            }
            if(RAVEN->checkSettingsTouch(tcur)) return;
        }
    }
    
    //check is trying to move window in PT (usually a plug-in)
    if(Process::checkPlugInMoveTouch(touchX, touchY))
    {
        touches[id].isPlugInMove = true;
        return;
    }
    
    //return if touching the surround panner
    if(RavenMacUtilities::checkSurroundPannerTouch(touchX, touchY) && !floatingMixerTouch)
    {
        //printf("try mouse touch down...\n");
        RavenMacUtilities::touchMouseDown(touchX, touchY);
        touches[id].isSurroundPannerTouch = true;
        return;
    }
    
    //return if touching other running app window
    if(Process::checkOtherAppTouch(touchX, touchY) && !floatingMixerTouch)
    {
        //printf("try mouse touch down...\n");
        //RavenMacUtilities::touchMouseDown(touchX, touchY); //used to try and move surround panner- not working
        touches[id].isOtherAppTouch = true;
        return;
    }
    
    ///////////////////////////////////////////////////////////////////////////////////
    //Touching Raven Rack Check, then make sure ProTools gets activated, then hide mouse
    if(RAVEN->rackTouched(touchX, touchY))
    {
        touches[id].isRavenRack = true;
        Process::makeProToolsActiveAndShow();
        RAVEN_HIDE_MOUSE
    }
    
    TUIO::TuioTime startTime = tcur->getStartTime();  
    long startSec = startTime.getSeconds(); 
    long startUSec = startTime.getMicroseconds();
    
    std::list<TUIO::TuioCursor*> cursorList = tuioClient->getTuioCursors();
    
    ///////////////////////////////////////////////////////////////////////////////////
    // Functions Check ////////////////////////////////////////////////////////
    if(functionsManager->checkFunctionsTouched(touchX, touchY) && !floatingMixerTouch)
    {
        //check hold buttons
        touches[id].holdButton = functionsManager->checkFunctionsHoldButtonTouched(tcur);
        if(touches[id].holdButton)
        {
            //printf("hold button touched: %ls\n", touches[id].holdButton->getName().toWideCharPointer());
            if(touches[id].holdButton && touches[id].holdButton->getName() == "stopButton")
            {
                touches[id].holdButtonTimerID = eStopButtonTimerID;
                stopButtonHoldStartTime =   Time::getCurrentTime();
                startTimer(eStopButtonTimerID, 100);
            }
        }
        //printf("functions component or essentials touched\n");
        if(functionsManager->getTrackOrBankButtonTouched(tcur))
        {
            //exit flip mode when tracking/banking left/right when not 24
            if(!RAVEN_24)
            {
                if(inFlipMode)
                {
                    exitFlipMode();
                }
            }
        }
        // Customize Toolbar Check
        if (functionsManager->isCustomMode()) //custom mode
        {
            printf("Custom mode\n");
            if(functionsManager->checkModuleTouch(tcur, id))
            {
                touches[id].isModuleMove = true;
            }
            return;
        }
        // Nav Pad
        else if(functionsManager->checkNavPadTouch(touchX, touchY))//if the pad part of the navpad is touched
        {
            //printf("nav pad touch!\n");
            touches[id].isNavPad = true;
            return;
        }
        // Function Button
        else if(functionsManager->checkFunctionsButtonTouched(tcur, true))
        {
            //printf("functions button touched\n");
            touches[id].isButton = true;
            touches[id].isFunction = true;
           
//            touches[id].holdButton = functionsManager->checkFunctionsHoldButtonTouched(tcur);
//            if(touches[id].holdButton && touches[id].holdButton->getName() == "stopButton")
//            {
//                touches[id].holdButtonTimerID = eStopButtonTimerID;
//                stopButtonHoldStartTime =   Time::getCurrentTime();
//                startTimer(eStopButtonTimerID, 100);
//            }
            return;
        }
    }
    
    //placed after functions button check because of buttons on the floating essentials 
    if(mixer->checkButtonTouch(tcur))
    {
        return;
    }
    if(RAVEN->checkFloatingMixerMoveTouch(tcur))
    {
        touches[id].isFloatingMixerMove = true;
        return;
    }
    ///////////////////////////////////////////////////////////////////////////////////
    // Mixer/Sends Check ////////////////////////////////////////////////////////
    else 
    {
        #if !RAVEN_24
        // Flip Mode Handling ////////////////////////////////////
        if(RavenTouchComponent::componentTouched(mixer, touchX, touchY))
        {
            //make this happen when toggling out of flip mode in 24
            //Exit flip mode if needed
            exitFlipMode();
            blockFlipSwitch = true;
        }
        else if(RavenTouchComponent::componentTouched(sends, touchX, touchY)/* && sends->getMeterMode() == false*/)
        {
            //make thishappen when toggling into flip mode on 24
            //Enter Flip Mode Upon Sends Touch if needed
            enterFlipMode();
            blockFlipSwitch = true;
        }
        #endif
        // Check Each Bank/Channel///////////////////////////////////////////
        for(int b = 0; b < numBanks; b++)
        {
            for(int c = 0; c < chansPerBank; c++)
            {
                //check names first because icon panel may be on top of everything
                if(names->getBank(b)->getChannel(c)->checkButtonTouch(tcur))
                {
                    touches[id].isButton = true;
                    touches[id].bank = b;
                    touches[id].chan = c;
                }
                else if(mixerWindow->isOnDesktop() && !inFlipMode)
                {
                    // Mixer /////////////////////////////////////////////
                    RavenMixerChannelComponent *channel = mixer->getBank(b)->getChannel(c);
                    Button *autoModeButton = channel->getAutoModeButton();
                    Button *recButton = channel->getRecButton();
                    Button *soloButton = channel->getSoloButton();
                    Button *muteButton = channel->getMuteButton();

                    if(channel->autoCompEmptyTouch(tcur))
                    {
                        return;
                    }
                    else if(RavenTouchComponent::componentTouched(autoModeButton, touchX, touchY))
                    {
                        touches[id].isButton = true;
                        touches[id].isMixer = true;
                        touches[id].isAutoMode = true;
                        autoModeButton->triggerClick();
                        touches[id].bank = b;
                        touches[id].chan = c;
                        temporarilyBlockAllNameSwitching = true;
                        waitingForAutomationLabels = false;
                        stopTimer(eAutomationStatusTimerID);
                    }
                    else if(RavenTouchComponent::componentTouched(recButton, touchX, touchY))
                    {
                        touches[id].isButton = true;
                        touches[id].isMixer = true;
                        touches[id].isRecEnable = true;
                        recButton->triggerClick();
                        channel->setTouchRecordEnabled(true);
                        touches[id].bank = b;
                        touches[id].chan = c;
                    }
                    else if(RavenTouchComponent::componentTouched(soloButton, touchX, touchY))
                    {
                        touches[id].isButton = true;
                        touches[id].isMixer = true;
                        touches[id].isSoloEnable = true;
                        soloButton->triggerClick();
                        channel->setTouchSoloEnabled(true);
                        touches[id].bank = b;
                        touches[id].chan = c;
                    }
                    else if(RavenTouchComponent::componentTouched(muteButton, touchX, touchY))
                    {
                        touches[id].isButton = true;
                        touches[id].isMixer = true;
                        touches[id].isMuteEnable = true;
                        muteButton->triggerClick();
                        channel->setTouchMuteEnabled(true);
                        touches[id].bank = b;
                        touches[id].chan = c;
                    }
                    else if(mixer->getBank(b)->getChannel(c)->checkFaderTouch(tcur))
                    {
                        touches[id].isFader = true;
                        touches[id].isMixer = true;
                        touches[id].bank = b;
                        touches[id].chan = c;
                        if(Time::getCurrentTime().toMilliseconds() - lastExitFlipModeTime.toMilliseconds() < huiWaitMs){
                            usleep(300000);
                        }
                        //add a delay when returning from flipped mode so that you don't have to touch twice. Tested by Josh and 200ms still doesn't work every once in a while. 300ms has worked every time so far but it's possible that there are still exceptions. If so, try 400ms.
                        mixer->getBank(b)->getChannel(c)->beginFader();
                    }
                    else if(mixer->getBank(b)->getChannel(c)->checkButtonTouch(tcur))
                    {
                        touches[id].isButton = true;
                        touches[id].isMixer = true;
                        touches[id].bank = b;
                        touches[id].chan = c;
                    }
//                    else if(mixer->getBank(b)->getChannel(c)->checkSelectButtonTouch(tcur))
//                    {
//                        touches[id].isButton = true;
//                        touches[id].isMixer = true;
//                        touches[id].bank = b;
//                        touches[id].chan = c;
//                    }
                    else if(mixer->getBank(b)->getChannel(c)->checkKnobTouch(tcur))
                    {
                        touches[id].isKnob = true;
                        touches[id].isMixer = true;
                        touches[id].bank = b;
                        touches[id].chan = c;
                    }
                    //Fine Fader Touch
                    else if(RavenTouchComponent::componentTouched(channel->getDBLabel(), touchX, touchY) /*|| RavenTouchComponent::componentTouched(channel->getTrackNameLabel(), touchX, touchY)*/)
                    {
                        touches[id].isMixer = true;
                        touches[id].bank = b;
                        touches[id].chan = c;
                        bool temp = channel->isFineFader();
                        channel->setFineFader(!temp);
                        LOCK_JUCE_THREAD
                            channel->setFineFaderImage(!temp);
                    }
                    else if(RAVEN->isHybridMode()
                            && cursorList.size() > 1
                            && !isCommandDown()
                            && (channel->checkTrackSelectTouch(touchX, touchY) || channel->checkSelectButtonTouch(tcur)))
                    {
                        std::list<TUIO::TuioCursor*>::iterator it = cursorList.end();
                        std::vector<TUIO::TuioPoint> touchPoints;
                        //it--; it--;//previous touch
                        for(int i = 0; i < cursorList.size(); i++){
                            it--;
                            touchPoints.push_back((*it)->getPosition());
                        }
                        TUIO::TuioTime lastTime = (*it)->getStartTime();
                        long lastSec = lastTime.getSeconds();
                        long lastUSec = lastTime.getMicroseconds();
                        
                        if(cursorList.size() == 2)
                        {
                            if(RavenTouchComponent::touchTimeLessThan(startSec, startUSec, lastSec, lastUSec, MT_2FINGER_WAIT_USEC))
                            {
                                TUIO::TuioPoint lastPos = (*it)->getPosition();
                                if(tcur->getDistance(&lastPos) < MT_2FINGER_TOUCH_DISTANCE)
                                {
                                    touches[id].gesture = eMTTwoFinger;
                                    touches[id-1].gesture = eMTTwoFinger;
                                    touches[id].isTrackSelect = false;
                                    touches[id-1].isTrackSelect = false;
                                    touches[id].isTrackDeselect = false;
                                    touches[id-1].isTrackDeselect = false;
                                    
                                    releaseFaderByTouchID(id);
                                    releaseFaderByTouchID(id-1);
                                    
                                    touches[id-1].isMainGestureFinger = false;
                                    
                                    touches[id].isMainGestureFinger = true;
                                    //std::cout << "TWO FINGERS DOWN!!!" << std::endl;
                                }
                            }
                        }
                        else if(cursorList.size() == 3)
                        {
                            if(RavenTouchComponent::touchTimeLessThan(startSec, startUSec, lastSec, lastUSec, MT_3FINGER_WAIT_USEC))
                            {
                                //Calculating aggregate distance among all points
                                float aggTouchDist = touchPoints[0].getDistance(touchPoints[1].getX(), touchPoints[1].getY());
                                aggTouchDist += touchPoints[0].getDistance(touchPoints[2].getX(), touchPoints[2].getY());
                                aggTouchDist += touchPoints[1].getDistance(touchPoints[2].getX(), touchPoints[2].getY());
                                
                                if(aggTouchDist < MT_3FINGER_TOUCH_DISTANCE)
                                {
                                    touches[id].gesture = eMTThreeFinger;
                                    touches[id-1].gesture = eMTThreeFinger;
                                    touches[id-2].gesture = eMTThreeFinger;

                                    touches[id].isTrackSelect = false;
                                    touches[id-1].isTrackSelect = false;
                                    touches[id-2].isTrackSelect = false;
                                    touches[id].isTrackDeselect = false;
                                    touches[id-1].isTrackDeselect = false;
                                    touches[id-2].isTrackDeselect = false;
                                    
                                    releaseFaderByTouchID(id);
                                    releaseFaderByTouchID(id-1);
                                    releaseFaderByTouchID(id-2);
                                    
                                    touches[id-1].isMainGestureFinger = false;
                                    touches[id-2].isMainGestureFinger = false;
                                    
                                    touches[id].isMainGestureFinger = true;
                                    
                                    //std::cout << "threee fingers down" << std::endl;
                                }
                            }
                        }
                    }
                    //Track Select (on mixer or names)
                    else if(channel->checkTrackSelectTouch(touchX, touchY)
                            || names->getBank(b)->getChannel(c)->trackNameTouch(tcur)
                            || channel->checkSelectButtonTouch(tcur))
                    {

                        int numberOfActiveTrackSelects = 0;
                        for(int i = 0;i<MAX_NUM_TOUCHES;i++)
                        {
                            if(touches[i].isTrackSelect)
                                numberOfActiveTrackSelects++;
                        }
                        
                        if(!channel->isBlank())
                        {
                            initCommandState = isCommandDown();
                            initShiftState = isShiftDown();
                            
                            //printf("command down = %d\n", initCommandState);
                            if(prevSelectedBank >= 0 && prevSelectedChannel >= 0)
                            {
                                initTouchBank = prevSelectedBank;
                                initTouchChannel = prevSelectedChannel;
                            }
                            
                            bool alreadySelected = channel->isSelected();
                            if(!initCommandState && RAVEN->isHybridMode())
                            {
                                touches[id].gesture = eMTOneFinger;
                                touches[id].isMainGestureFinger = true;
                                touches[id].isFader = false;
                            }
                            if(!alreadySelected && !initCommandState)
                            {
                                if(prevSelectedBank == b && prevSelectedChannel == c)
                                {
                                    if(channel->canTriggerTrackSelect() && numberOfActiveTrackSelects == 0)
                                    {
                                        touches[id].isTrackSelect = true;
                                        //printf("track select 1\n");
                                        if(!RAVEN->isHybridMode())
                                            channel->selectTrack(true);
                                    }
                                }
                                else if(prevSelectedBank != b || prevSelectedChannel != c)
                                {
                                    if(channel->canTriggerTrackSelect() && numberOfActiveTrackSelects == 0)
                                    {
                                        touches[id].isTrackSelect = true;
                                        //printf("track select 2\n");
                                        if(!RAVEN->isHybridMode())
                                            channel->selectTrack(true);
                                        
                                        //deselect last track on touch down unless in hybrid mode or shift key is down
                                        if(prevSelectedBank >= 0
                                           && prevSelectedChannel >= 0
                                           && !RAVEN->isHybridMode()
                                           && !isShiftDown())
                                        {
                                            mixer->getBank(prevSelectedBank)->getChannel(prevSelectedChannel)->selectTrack(false);
                                        }
                                        
                                        prevSelectedBank = b;
                                        prevSelectedChannel = c;
                                    }
                                }
                                
                            }
                            else if(!alreadySelected && initCommandState)
                            {
                                if(channel->canTriggerTrackSelect())
                                {
                                    touches[id].isTrackSelect = true;//old
                                    channel->triggerClickTrackSelectAndResetTime(true);
                                }
                            }
                            else if(alreadySelected && (!initCommandState || isShiftDown()))
                            {
                                //do nothing in this case! (unless shift is down)
                                if(isShiftDown() && channel->canTriggerTrackSelect())
                                {
                                    channel->triggerClickTrackSelectAndResetTime();
                                    LOCK_JUCE_THREAD
                                        channel->getTrackSelectButton()->setToggleState(true, false);
                                    prevSelectedBank = b;
                                    prevSelectedChannel = c;
                                }
                                
//                                if(RAVEN->isHybridMode())
//                                    mixer->getBank(prevSelectedBank)->getChannel(prevSelectedChannel)->selectTrack(false);
                            }
                            else if(alreadySelected && initCommandState) //de-select requires command to be down (disable for now, revert to previous)
                            {
                                if(channel->canTriggerTrackSelect())
                                {
                                    touches[id].isTrackDeselect = true;//old
                                    channel->triggerClickTrackSelectAndResetTime(false);
                                }
                            }
                            
                            touches[id].bank = b;
                            touches[id].chan = c;
                        }
                    }
                    
                } //end if mixerWindow->isOnDesktop() && !inFlipMode
                else if(sendsWindow->isOnDesktop())
                {
                    // Sends //////////////////////////////////////////////
                    if(sends->getBank(b)->getChannel(c)->checkButtonTouch(tcur))
                    {
                        touches[id].isButton = true;
                        touches[id].isSend = true;
                        touches[id].bank = b;
                        touches[id].chan = c;
                    }
                    else if (sends->getBank(b)->getChannel(c)->checkFaderTouch(tcur))
                    {
                        //printf("sends fader touch\n");
                        touches[id].isFader = true;
                        touches[id].isSend = true;
                        touches[id].bank = b;
                        touches[id].chan = c;
                        sends->getBank(b)->getChannel(c)->beginFader();
                    }
                    else if(sends->getBank(b)->getChannel(c)->checkKnobTouch(tcur))
                    {
                        touches[id].isKnob = true;
                        touches[id].isSend = true;
                        touches[id].bank = b;
                        touches[id].chan = c;
                    }

                }
            }
        }
    }
}

void RavenContainer::updateTuioCursor(TUIO::TuioCursor *tcur)
{
    if(activePQGesture) return;
    
    //printf("UPDATE CURSOR\n");
    
    int id = tcur->getCursorID();
    int touchX = tcur->getScreenX(SCREEN_WIDTH);
    int touchY = tcur->getScreenY(SCREEN_HEIGHT);
    
    if(touches[id].isRavenPalette || touches[id].isRavenRack) RAVEN_HIDE_MOUSE
    
    if(touches[id].isSurroundPannerTouch)
    {
        //printf("other app touch %d, %d\n", touchX, touchY);
        RavenMacUtilities::surroundPannerDrag(touchX, touchY);
        return;
    }
    else if(touches[id].isOtherAppTouch)
    {
        //printf("other app touch %d, %d\n", touchX, touchY);
        //RavenMacUtilities::touchMouseDrag(touchX, touchY);
        return;
    }
    
    
    TUIO::TuioTime startTime = tcur->getStartTime();
    //long startSec = startTime.getSeconds();
    //long startUSec = startTime.getMicroseconds();
    
    TUIO::TuioTime thisTime = tcur->getTuioTime();  
    //long thisSec = thisTime.getSeconds();
    //long thisUSec = thisTime.getMicroseconds();
    
    //tuioClient->lockCursorList();
    std::list<TUIO::TuioCursor*> cursorList = tuioClient->getTuioCursors();
    
    //if(true)//if(!touchTimeLessThan(thisSec, thisUSec, startSec, startUSec, MT_WAIT_USEC))
    if(!activePQGesture)
    {
        //if(true)//if(cursorList.size() == 1)
        {
            //std::cout << "one finger move" << std::endl;
            if(touches[id].isFader && touches[id].isMixer)
            {
                int b = touches[id].bank;
                int c = touches[id].chan;
                mixer->getBank(b)->getChannel(c)->updateFaderTouch(tcur);
            }
            else if(touches[id].isFader && touches[id].isSend)
            {
                int b = touches[id].bank;
                int c = touches[id].chan;
                sends->getBank(b)->getChannel(c)->updateFaderTouch(tcur);
            }
            else if(touches[id].isKnob && touches[id].isMixer)
            {
                int b = touches[id].bank;
                int c = touches[id].chan;
                mixer->getBank(b)->getChannel(c)->updateKnobTouch(tcur);
            }
            else if(touches[id].isKnob && touches[id].isSend)
            {
                int b = touches[id].bank;
                int c = touches[id].chan;
                sends->getBank(b)->getChannel(c)->updateKnobTouch(tcur);
            }
            else if(touches[id].isAutoMode || touches[id].isRecEnable || touches[id].isSoloEnable || touches[id].isMuteEnable)
            {
                for(int b = 0; b < numBanks; b++)
                {
                    for(int c = 0; c < chansPerBank; c++)
                    {
                        if(!(b == touches[id].bank && c == touches[id].chan)) //dont reselect or deselect itself
                        {
                            RavenMixerChannelComponent *channel = mixer->getBank(b)->getChannel(c);
                            Button *button;
                            bool alreadySelected;
                            if(touches[id].isAutoMode)
                            {
                                button = channel->getAutoModeButton();
                                alreadySelected = channel->isAutoModeSelected();
                                if(!alreadySelected)
                                {
                                    temporarilyBlockAllNameSwitching = true;
                                    waitingForAutomationLabels = false;
                                    stopTimer(eAutomationStatusTimerID);
                                }
                            }
                            else if(touches[id].isRecEnable)
                            {
                                button = channel->getRecButton();
                                alreadySelected = channel->isTouchRecordEnabled();
                            }
                            else if(touches[id].isSoloEnable)
                            {
                                button = channel->getSoloButton();
                                alreadySelected = channel->isTouchSoloEnabled();
                            }
                            else if(touches[id].isMuteEnable)
                            {
                                button = channel->getMuteButton();
                                alreadySelected = channel->isTouchMuteEnabled();
                            }
                            
                            if(RavenTouchComponent::componentTouched(button, touchX, touchY))
                            {
                                if(!alreadySelected) button->triggerClick();
                                if(touches[id].isRecEnable) channel->setTouchRecordEnabled(true);
                                if(touches[id].isSoloEnable) channel->setTouchSoloEnabled(true);
                                if(touches[id].isMuteEnable) channel->setTouchMuteEnabled(true);
                            }
                        }
                    }
                }
            }
            //Track Select Swipe         
            else if((touches[id].isTrackSelect || touches[id].isTrackDeselect) && !touches[id].isMainGestureFinger)
            {
                bool currentCommandState = isCommandDown();
                if(initCommandState != currentCommandState) return;
                
                for(int b = 0; b < numBanks; b++)
                {
                    for(int c = 0; c < chansPerBank; c++)
                    {
                        RavenMixerChannelComponent *channel = mixer->getBank(b)->getChannel(c);
                        
                        if(channel->isBlank()) continue; // ignore  blanks
                        
                        //if(!(b == touches[id].bank && c == touches[id].chan)) //dont reselect or deselect itself
                        {                            
                            bool alreadySelected = channel->isSelected();
                            bool touchMixerPlate = RavenTouchComponent::componentTouched(channel, touchX, touchY);
                            bool touchNamePlate = names->getBank(b)->getChannel(c)->trackNameTouch(tcur);
                            
                            if(touches[id].isTrackSelect)
                            {
                                //don't do this on already selected track unless in hybrid mode, which you need
                                if((!alreadySelected || RAVEN->isHybridMode()) && (touchMixerPlate || touchNamePlate) && !initCommandState)
                                {
                                    if(!RAVEN->isHybridMode())
                                        channel->selectTrack(true);

                                    if(prevSelectedBank >= 0
                                       && prevSelectedChannel >= 0
                                       && !RAVEN->isHybridMode())
                                    {
                                        mixer->getBank(prevSelectedBank)->getChannel(prevSelectedChannel)->selectTrack(false);
                                    }
                                    prevSelectedBank = b;
                                    prevSelectedChannel = c;
                                }
                                else if(!alreadySelected && (touchMixerPlate || touchNamePlate) && initCommandState)
                                {
                                    printf("got here2\n");
                                    if(!(b == touches[id].bank && c == touches[id].chan)) //dont reselect or deselect itself
                                    {
                                        if(channel->canTriggerTrackSelect())
                                        {
                                            channel->triggerClickTrackSelectAndResetTime(true);
                                        }
                                    }
                                }
                            }
                            else if(touches[id].isTrackDeselect)
                            {
                                if(alreadySelected && (touchMixerPlate || touchNamePlate))
                                {
                                    if(channel->canTriggerTrackSelect())
                                    {
                                        if(!isShiftDown())
                                        {
                                            channel->triggerClickTrackSelectAndResetTime(false);
                                        }
                                        else//if shift is down, actually the last touched one will stay selected.
                                        {
                                            channel->triggerClickTrackSelectAndResetTime();
                                            LOCK_JUCE_THREAD
                                                channel->getTrackSelectButton()->setToggleState(true, false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if(functionsManager->getMoveToggleState() && touches[id].isModuleMove)
            {
                RAVEN->moveWindow(touchY);
            }
            else if(functionsManager->isCustomMode() && touches[id].isModuleMove)
            {
                functionsManager->moveModule(touchX, touchY, id);
            }
            else if(touches[id].isCounter && id == (int)RAVEN->counterWindow->getProperties().getWithDefault("orgTouchID", 999))
            {
                RAVEN->moveCounter(touchX, touchY);
            }
            /*
            else if(touches[id].isDrumPad)
            {
                RAVEN->moveDrumPad(touchX, touchY);
            }
            */
            else if(touches[id].isFloatingNavPadMove && id == (int)RAVEN->navpadWindow->getProperties().getWithDefault("orgTouchID", 999))
            {
                RAVEN->moveFloatingNavPad(touchX, touchY);
            }
            else if(touches[id].isFloatingMixerMove && id == (int)mixerWindow->getProperties().getWithDefault("orgTouchID", 999))
            {
                RAVEN->moveFloatingMixer(touchX, touchY);
            }
            else if(touches[id].isIconPaletteMove && id == (int)RAVEN->iconPaletteWindow->getProperties().getWithDefault("orgTouchID", 999))
            {
                RAVEN->moveIconPalette(touchX, touchY);
            }
            else if(touches[id].isEssentials && id == (int)RAVEN->essentialsWindow->getProperties().getWithDefault("orgTouchID", 999))
            {
                RAVEN->moveEssentialsPalette(touchX, touchY);
            }
            else if(touches[id].isInsertsPalette && id == (int)RAVEN->insertsWindow->getProperties().getWithDefault("orgTouchID", 999))
            {
                RAVEN->moveInsertsPalette(touchX, touchY);
            }
            else if(touches[id].isSettingsPalette && id == (int)RAVEN->settingsWindow->getProperties().getWithDefault("orgTouchID", 999))
            {
                RAVEN->moveSettingsPalette(touchX, touchY);
            }

            else if(touches[id].isNavPad || touches[id].isFloatingNavPad)
            {
                LOCK_JUCE_THREAD
                {
                    if(touches[id].gesture == eMTTwoFinger && touches[id].isMainGestureFinger)
                        //if(twoFingerRunningCount >= 15)
                {
                    //printf("two finger nav pad touch\n");
                    //functions->navPadTouch(tcur, true); //true
                    //std::cout << "NAV two finger swipe" << std::endl;
                }
                else if(!(touches[id].gesture == eMTTwoFinger) && !activePQGesture)
                {
                    //std::cout << "CROSSING THREADS!!!!" << std::endl;
                    functionsManager->navPadTouch(tcur, false);
                    //std::cout << "NAV one finger " << std::endl;
                }
                }
            }
            //*
            else if(touches[id].gesture == eMTOneFinger && touches[id].isMainGestureFinger)
            {
                int distanceFromStart = touchX - touches[id].initTouchX;
                if(abs(distanceFromStart) > MT_BANK_SWIPE_DISTANCE)
                {
                    if(distanceFromStart < 0)
                    {
                        LOCK_JUCE_THREAD
                            functionsManager->trackRight();
                        printf("one finger track right");
                    }
                    else
                    {
                        LOCK_JUCE_THREAD
                            functionsManager->trackLeft();
                        printf("one finger track left");
                    }
                    touches[id].isTrackSelect = false;//important
                    touches[id].isMainGestureFinger = false;
                }
            }
            else if(touches[id].gesture == eMTTwoFinger && touches[id].isMainGestureFinger)
            {
                int distanceFromStart = touchX - touches[id].initTouchX;
                if(abs(distanceFromStart) > MT_BANK_SWIPE_DISTANCE)
                {
                    if(distanceFromStart < 0)
                    {
                        LOCK_JUCE_THREAD
                            functionsManager->swipeBankRightTwoFingers();
                    }
                    else
                    {
                        LOCK_JUCE_THREAD
                            functionsManager->swipeBankLeftTwoFingers();
                    }
                    touches[id].isMainGestureFinger = false;
                }
            }
            else if(touches[id].gesture == eMTThreeFinger && touches[id].isMainGestureFinger)
            {
//                static int threeFingerSwipeCount = 0;
//                static float priorThreeFingerPosition = 0;
//                if(threeFingerSwipeCount%50 == 0)
//                {
//                    //(tcur->getX() - priorThreeFingerPosition > 0) ?  functions->bankRight() : functions->bankLeft();
//                }
//                
//                priorThreeFingerPosition = tcur->getX();
//                
//                threeFingerSwipeCount++;
//                if(threeFingerSwipeCount >= 100000) threeFingerSwipeCount = 0;
//                std::cout << "Three fingers!!!!! ....that's what she said" << std::endl;
                
                int distanceFromStart = touchX - touches[id].initTouchX;
                if(abs(distanceFromStart) > MT_BANK_SWIPE_DISTANCE)
                {
                    if(distanceFromStart < 0)
                    {
                        LOCK_JUCE_THREAD
                            functionsManager->swipeBankRightThreeFingers();
                        printf("three finger bank right");
                    }
                    else
                    {
                        LOCK_JUCE_THREAD
                            functionsManager->swipeBankLeftThreeFingers();
                        printf("three finger bank left");
                    }
                    touches[id].isMainGestureFinger = false;
                }
            }
            //*/
        }
        
        /*
        else if(cursorList.size() == 2)   //more detail needed (fingers close, etc)
        {
            float speedX = tcur->getXSpeed();
            std::cout << "two finger move! " << speedX << std::endl;
            scrollView(mixerView, speedX);
        }
         */
    }    
}

void RavenContainer::removeTuioCursor(TUIO::TuioCursor *tcur)
{
    if(activePQGesture)
    {
        return;
    }
    
    //printf("REMOVE CURSOR\n");
    
    int id = tcur->getCursorID();
    int touchX = tcur->getScreenX(SCREEN_WIDTH);
    int touchY = tcur->getScreenY(SCREEN_HEIGHT);

    TUIO::TuioTime startTime = tcur->getStartTime();  
    //long startSec = startTime.getSeconds();
    //long startUSec = startTime.getMicroseconds();

    TUIO::TuioTime thisTime = tcur->getTuioTime();  
    //long thisSec = thisTime.getSeconds();
    //long thisUSec = thisTime.getMicroseconds();
    
    //tuioClient->lockCursorList();
    std::list<TUIO::TuioCursor*> cursorList = tuioClient->getTuioCursors();
    if(cursorList.size() == 1 && blockFlipSwitch) // if last touch
    {
        blockFlipSwitch = false;
        if(inFlipMode)
        {
            LOCK_JUCE_THREAD mixer->flipAlpha(true);
        }
    }
    
    if(touches[id].isRavenRack || touches[id].isRavenPalette) RAVEN_HIDE_MOUSE
    
    if(touches[id].gesture == eMTOneFinger) // we use PQ SDK for single finger taps now
    {
//        if(RavenTouchComponent::touchTimeLessThan(thisSec, thisUSec, startSec, startUSec, MT_TAP_WAIT_USEC))
//        {
//            long lastTapSec = lastTapTime.getSeconds();
//            long lastTapUS = lastTapTime.getMicroseconds();
//            if(RavenTouchComponent::touchTimeLessThan(thisSec, thisUSec, lastTapSec, lastTapUS, MT_TAP_WAIT_USEC))
//            {                
//                if(tcur->getDistance(&lastTapPos) < MT_TAP_TOUCH_DISTANCE)
//                {
//                    //std::cout<< "double tap" << "id " << id << std::endl;
//                    /*/
//                    if(touches[id].isFader && touches[id].isMixer)
//                    {
//                        LOCK_JUCE_THREAD mixer->getBank(touches[id].bank)->getChannel(touches[id].chan)->zeroFader();
//                    }
//                    else if(touches[id].isKnob && touches[id].isMixer)
//                    {
//                        LOCK_JUCE_THREAD mixer->getBank(touches[id].bank)->getChannel(touches[id].chan)->zeroKnob();
//                    }
//                    else if(touches[id].isFader && touches[id].isSend)
//                    {
//                        LOCK_JUCE_THREAD sends->getBank(touches[id].bank)->getChannel(touches[id].chan)->zeroFader();
//                    }
//                    else if(touches[id].isKnob && touches[id].isSend)
//                    {
//                        LOCK_JUCE_THREAD sends->getBank(touches[id].bank)->getChannel(touches[id].chan)->zeroKnob();
//                    }
//                     //*/
//                }
//            }
//            else
//            {
//                //std::cout<< "single tap" << "id " << id << std::endl;
//            }
//            
//            lastTapTime = thisTime;
//            lastTapPos = tcur->getPosition();
//        }
    }
    else if(touches[id].gesture == eMTTwoFinger)
    {
        /*
        if(RavenTouchComponent::touchTimeLessThan(thisSec, thisUSec, startSec, startUSec, MT_TAP_WAIT_USEC))
        {
            //std::cout<< "two finger tap" << "id " << id << std::endl;
        }
         */
    }
    
    if(touches[id].isFader)
    {
        touches[id].isFader = false;
        int b = touches[id].bank;
        int c = touches[id].chan;
        if(touches[id].isMixer)
        {
            mixer->getBank(b)->getChannel(c)->endFader();
            mixer->getBank(b)->getChannel(c)->releaseFaderTouch();
        }
        if(touches[id].isSend)
        {
            sends->getBank(b)->getChannel(c)->endFader();
            sends->getBank(b)->getChannel(c)->releaseFaderTouch();
        }
        
        touches[id].bank = 0;//999;
        touches[id].chan = 0;//999;
    }
    if(touches[id].isKnob)
    {
        touches[id].isKnob = false;
        int b = touches[id].bank;
        int c = touches[id].chan;
        if(touches[id].isMixer) mixer->getBank(b)->getChannel(c)->getKnob()->isBeingTouched = false;
        if(touches[id].isSend)  sends->getBank(b)->getChannel(c)->getKnob()->isBeingTouched = false;

        touches[id].bank = 999;
        touches[id].chan = 999;
    }
    
    if(touches[id].isMixer)
    {
        touches[id].isMixer = false; 
    }
    
    if(touches[id].isSend)
    {
        touches[id].isSend = false; 
    }
    
    //resest touch fields
    touches[id].gesture = eMTOneFinger;
    touches[id].isMainGestureFinger = false;
    
    if(touches[id].isModuleMove && functionsManager->getMoveToggleState())
    {
        RAVEN->endMoveWindow();
        touches[id].isModuleMove = false;
        isMovingRackModules = false;
    }
    if(touches[id].isModuleMove && functionsManager->isCustomMode())
    {
        functionsManager->endModuleMove(id);
        touches[id].isModuleMove = false;
    }
    
    if(touches[id].isFunction && touches[id].isButton)
    {
        functionsManager->releaseFunctionsHotKeysTouch(tcur);
        floatingEssentials->releaseHotKeyTouch(tcur);
    }
    
    if(touches[id].isTrackSelect)
    {
        bool currentCommandState = isCommandDown();
        bool currentShiftState = isShiftDown();
        //if(initCommandState == currentCommandState && initShiftState == currentShiftState)
        {
            if(!initCommandState)
            {
                printf("not init command state %d, %d\n", prevSelectedChannel, initTouchChannel);
                if(prevSelectedBank >= 0
                   && prevSelectedChannel >= 0)
                {
                    if(prevSelectedBank != initTouchBank
                       || prevSelectedChannel != initTouchChannel
                       || !mixer->getBank(prevSelectedBank)->getChannel(prevSelectedChannel)->getTrackSelectButton()->getToggleState())
                    {
                        mixer->getBank(prevSelectedBank)->getChannel(prevSelectedChannel)->getTrackSelectButton()->triggerClick();
                    }
                }
            }
        }
        if(initCommandState != currentCommandState || initShiftState != currentShiftState)//else
        {
            LOCK_JUCE_THREAD
                mixer->syncInternalTrackSelectWithHUI();
        }
    }
    
    //if this touch has a hold button associated with it, stop that timer
    if(touches[id].holdButton)
        stopTimer(touches[id].holdButtonTimerID);
    
    touches[id].isButton = false;
    touches[id].isCounter = false;
    touches[id].isDrumPad = false;
    touches[id].isEssentials = false;
    touches[id].isInsertsPalette = false;
    touches[id].isSettingsPalette = false;
    touches[id].isNavPad = false;
    touches[id].isFloatingNavPad = false;
    touches[id].isFloatingNavPadMove = false;
    touches[id].isTrackSelect = false;
    touches[id].isTrackDeselect = false;
    touches[id].isAutoMode = false;
    touches[id].isRecEnable = false;
    touches[id].isSoloEnable = false;
    touches[id].isMuteEnable = false;
    touches[id].isFloatingMixerMove = false;
    touches[id].isIconPaletteMove = false;
    touches[id].isIconPalette = false;
    touches[id].isRavenRack = false;
    touches[id].holdButton = 0;
    
    if(touches[id].isRavenPalette)
    {
        //RAVEN->resetPaletteLevels();
        touches[id].isRavenPalette = false;
    }
    
    for(int b = 0; b < numBanks; b++)
    {
        for(int c = 0; c < chansPerBank; c++)
        {
            mixer->getBank(b)->getChannel(c)->resetTouchEnablers();
        }
    }


    //tuioClient->unlockCursorList();
    
    if(touches[id].isPlugInMove)
    {
        Process::releasePlugInMoveTouch(touchX, touchY);
        touches[id].isPlugInMove = false;
    }

    if(touches[id].isOtherAppTouch)
    {
        //RavenMacUtilities::touchMouseUp(touchX, touchY);
        touches[id].isOtherAppTouch = false;
    }
    
    if(touches[id].isSurroundPannerTouch)
    {
        RavenMacUtilities::touchMouseUp(touchX, touchY);
        touches[id].isSurroundPannerTouch = false;
    }

    //auto unlatch modifiers
    if(!functionsManager->checkModifierTouch(touchX, touchY))
    {
        LOCK_JUCE_THREAD functionsManager->unlatchModifiers();
    }
    
//    //Touching Raven Check, then make sure ProTools gets activated, then hide mouse
//    if(RAVEN->ravenTouched(touchX, touchY))
//    {
//        //        if(RAVEN_IS_BACKGROUND_APP)
//        Process::makeProToolsActiveAndShow();
//        RAVEN_HIDE_MOUSE
//    }
    
}

//Thread Safe JUCE Messaging
void RavenContainer::handleMessage(const Message &message)
{
    if(dynamic_cast<const RavenAutoMessage*>(&message) != NULL)
    {
        //printf("raven automation message\n");
        RavenAutoMessage *m = (RavenAutoMessage*)&message;
        int bank = m->bank;
        int which = m->chan;
        String automationType = m->automationType;
        if(activePQGesture == false)
        {
            mixer->setAutomationImage(bank, which, automationType);
        }
    }
    else if(dynamic_cast<const RavenTrackBlankMessage*>(&message) != NULL)
    {
        RavenTrackBlankMessage *m = (RavenTrackBlankMessage*)&message;
        if(m->bank < numBanks)
        {
            mixer->getBank(m->bank)->getChannel(m->chan)->setBlankMaster(m->isBlank);
            sends->getBank(m->bank)->getChannel(m->chan)->setBlankMaster(m->isBlank);
            //names->setAllIcons();//figure out why just setting one doesn't work.
            names->getBank(m->bank)->getChannel(m->chan)->setIcon();
        }
    }
    else if(dynamic_cast<const RavenClearMetersMessage*>(&message) != NULL)
    {
        mixer->unClipMeters();
        link->set(eNSCGlobalControl, eNSCGSClearMeters, 0, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSClearMeters, 0, 0, 0);     
    }
}

//Startup Timer
void RavenContainer::timerCallback(int timerID)
{
    if(timerID == eStopButtonTimerID)
    {
        if(Time::getCurrentTime().toMilliseconds() - stopButtonHoldStartTime.toMilliseconds() > RAVEN_STOP_BUTTON_HOLD_MS)
        {
            printf("entering online mode\n");
            MacKeyPress::pressCombo(onlineModeKeyCode);
            stopTimer(eStopButtonTimerID);
        }
    }
    else if (timerID == eCheckForPanModeTimerID)
    {
        loadingWindow->updateStatusLabel("Synchronizing Interface...");//pan initialization
        static int panModeTimerCount = 0;
        panModeTimerCount++;
        //we might need a longer pan check timer. I don't know if there's a better system than just timing it...
        if( panModeTimerCount >= numberOfSecondsToCheckForPanMode*1000.0/timerInterval)
        {
            stopTimer(eCheckForPanModeTimerID);
            for(int bank = 0;bank < numBanks; bank++)
            {
                if(panModeCounters[bank] > numberOfBlinksToDeterminePanMode)
                {
                    printf("bank %d in pan mode with panModeCount = %d\n", bank, panModeCounters[bank]);
                    //set to not pan mode
                    panModeBanksNeedingChange.insert(panModeBanksNeedingChange.begin(), bank);
                }
                else
                    printf("bank %d NOT in pan mode with panModeCount = %d\n", bank, panModeCounters[bank]);
            }
            
            loadingWindow->updateStatusLabel("Calibrating Trackpad...");            
            startTimer(eExitPanModeTimerID, huiWaitMs);
        }
    }
    else if (timerID == eExitPanModeTimerID)
    {
        loadingWindow->updateStatusLabel("Synchronizing Pro Tools...");//exit pan mode
        if((int)panModeBanksNeedingChange.size() > 0)
        {
            int tempIndex = panModeBanksNeedingChange.size()-1;
            link->set(eNSCGlobalControl, eNSCGSPTBankMode_Pan, panModeBanksNeedingChange.at(tempIndex), 0, 1);
            link->set(eNSCGlobalControl, eNSCGSPTBankMode_Pan, panModeBanksNeedingChange.at(tempIndex), 0, 0);
            printf("CHANGING OUT OF PAN MODE ON BANK %d\n", panModeBanksNeedingChange.at(tempIndex));
            panModeBanksNeedingChange.pop_back();
        }
        else
        {
            stopTimer(eExitPanModeTimerID);
            startTimer(eStartupTimerID, timerInterval);
        }
    }
    else if(timerID == eStartupTimerID)
    {
        //checks for flip mode
        loadingWindow->updateStatusLabel("Detecting Mixer Status...");//Initializing HUI Flip Mode
        if(functionsManager->getMasterFlipModeButton()->getToggleState())
        {
            loadingWindow->updateStatusLabel("Flip Mode Detected");
            printf("flip Mode is on!\n");
            autoEnterFlipMode();
            blockFlipSwitch = false;
            stopTimer(eStartupTimerID);
            exitFlipMode();
            blockFlipSwitch = false;
            startTimer(eFlipModeTimerID, huiWaitMsExtraLong);
        }
        
        static int timerCount = 0;
        timerCount++;
        if(timerCount >= (2.0)*1000./timerInterval)
        {
            loadingWindow->updateStatusLabel("Flip Mode Not Detected");
            printf("not in flip mode\n");
            stopTimer(eStartupTimerID);
            enterFlipMode();
            blockFlipSwitch = false;
            startTimer(eInitSendsTimerID, huiWaitMsExtraLong);
        }
    }
    else if (timerID == eFlipModeTimerID)
    {
        loadingWindow->updateStatusLabel("Initializing Send Channels...");
        enterFlipMode();
        blockFlipSwitch = false;
        stopTimer(eFlipModeTimerID);
        startTimer(eInitSendsTimerID, huiWaitMsExtraLong); //call this so we always unflip ourself on startup
    }
    else if (timerID == eInitSendsTimerID)
    {
        loadingWindow->updateStatusLabel("Initializing Mixer Channels...");
        exitFlipMode();
        blockFlipSwitch = false;
        stopTimer(eInitSendsTimerID);
        
        //start getting all automation status labels
        startTimer(eAutomationStatusTimerID, huiWaitMsLong);
    }
    else if(timerID == eAutomationStatusTimerID)
    {
        if(waitingForAutomationLabels == false)
        {
            //printf("waiting for automation\n");
            waitingForAutomationLabels = true;
            link->set(eNSCGlobalControl, eNSCGSAutoStatus, 0, 0, 1);
            link->set(eNSCGlobalControl, eNSCGSAutoStatus, 1, 0, 1);
            link->set(eNSCGlobalControl, eNSCGSAutoStatus, 2, 0, 1);
            if(!RAVEN_24)
                link->set(eNSCGlobalControl, eNSCGSAutoStatus, 3, 0, 1);
        }
        else
        {
            waitingForAutomationLabels = false;
            link->set(eNSCGlobalControl, eNSCGSAutoStatus, 0, 0, 0);
            link->set(eNSCGlobalControl, eNSCGSAutoStatus, 1, 0, 0);
            link->set(eNSCGlobalControl, eNSCGSAutoStatus, 2, 0, 0);
            if(!RAVEN_24)
                link->set(eNSCGlobalControl, eNSCGSAutoStatus, 3, 0, 0);
            
            //mixer->checkBlankFrames();
            //sends->checkBlankFrames();
            
            stopTimer(eAutomationStatusTimerID);
            names->setAllIcons();
            finishedInitialization();
        }
    }
    else if(timerID == eEverySecondTimerID)
    {
        if(RAVEN->getAutomationCheckBlockCounter() > -1)
        {
            startTimer(eAutomationStatusTimerID, huiWaitMsLong);
        }
        RAVEN->setAutomationCheckBlockCounter(RAVEN->getAutomationCheckBlockCounter()+1);
    }
}

//NSC callbacks//////////////
int RavenContainer::nscServerFound(const char* serverName, const char * domain)
{
    
	// It is possible that multiple servers will be found, running on different domains and different machines.
	// These chould be managed in a list and presented to the user for selection.
	// In this simple example, we assume just a single instance of NeyFi will be running on the network, and
	// connect to it immediately:
	// It is possible that you will get mulitple notifications of the same serverName and domain.  This will happen
	// if there are multiple network paths to the server (e.g. wired and wireless).  Even locally, it is possible to get multiple connections, 
	// for instance through the internal loopback adaptor and through a connected router.
	
    if (link && RavenMacUtilities::checkHostName(serverName))
    {
		link->connectToServer(serverName, domain);
    }
    
	return 0;			// return code not currently used, but should return 0.
    
}

int RavenContainer::nscConnected(const char* serverName, const char * domain)
{
	DBG("New connection from " + String(CharPointer_UTF8(serverName)) + "." + String(CharPointer_UTF8(domain)));
	// link is now connected for communications with Ney-Fi, and can be used for bi-directional communication.
	// Note: this callback can occur on a different thread than nscServerFound, but will be the same thread as the callback functions below.
    
    if(!RAVEN_NOHUI)
    {
        startInitialization();
    }
    
	return 0;			// return code not currently used, but should return 0.
}

int RavenContainer::nscDisconnected(const char* serverName, const char * domain)
{
	// if you are managing a list of services and presenting these to the user, note that this service might not be the one
	// that you are currently connected to.  In other words, this callback happens might happen due to NeyFi stopping on 
	// a different machine than the one that you are currently connected to.
	//DBG("NSC disconnected -- " +  String(serverName) + "." + String(domain) );
    DBG("NSC disconnected -- " );
	return 0;		// return code not currently used, but should return 0.
}


int RavenContainer::callback(ENSCEventType type, int subType, int bank, int which, float val)
{
//    if(subType == eNSCTrackMeterClip1)
//        printf("Normal HUI type: %d, sub type: %d, bank: %d, which: %d, val: %f\n", type, subType, bank, which, val);
    
    if(subType == eNSCGSPTBankMode_Pan && val == 1.0f)
    {
        printf("pan mode. bank = %d, chan = %d\n", bank, which);
        panModeCounters[bank]++;
    }

//    if(type == eNSCGlobalControl && subType == eNSCGSAutomationReadSwitch)
//    {
//        printf("auto READ = %d, chan = %d, val = %f\n", bank, which, val);
//    }
    
    RAVEN->nscCallback(type, subType, bank, which, val);
	return 0;
}
int RavenContainer::callback(ENSCEventType type, int subType, int bank, int which, const std::string &data)
{
    //if(type != 9 && subType != 3)
        //printf("incoming HUI string: type = %d, subtype = %d, bank = %d, which = %d, text = %s\n\n", type, subType, bank, which, data.c_str());
    
    if(type == 9 && subType == 3)
    {
        if(/*temporarilyBlockAllNameSwitching*/0)
        {
            return 0;
        }
        else if(waitingForAutomationLabels == true && bank < numBanks) //make sure it doesn't go over numBanks so it doesn't crash
        {
//            printf("automationLabel bank = %d, which = %d, name = %s\n", bank, which, data.c_str());
            String automationType(data.c_str());
            RavenAutoMessage *m = new RavenAutoMessage(bank, which, automationType);
            postMessage(m);
            return 0;//don't set the track name to any of the automation labels
        }
        else
        {
            String trackName(data.c_str());
            if(trackName == "Auto" || trackName == "Inpt" || trackName == "Off " || trackName == "Read" || trackName == "Tch " || trackName == "Ltch" || trackName == "Wrt " || trackName == "Trim")
            {
                return 0;//just ignore that shit because it's (almost certainly) not the real track name 
            }
            
            Image tempImage;
            if(RAVEN_24)
                tempImage = ImageCache::getFromFile(File(String(GUI_PATH) + "icons/24/frame.png"));
            else
                tempImage = ImageCache::getFromFile(File(String(GUI_PATH) + "icons/32/frame.png"));
            names->setIconMap(bank, which, &trackName, &tempImage);
            
            if(trackName == "    ")
            {
                RavenTrackBlankMessage *m = new RavenTrackBlankMessage(bank, which, true);
                RAVEN->nscCallback(type, subType, bank, which, data);
                postMessage(m);
                return 0;
            }
            else
            {
                if(trackName != " -  ")//sends sends " -  "
                {
                    RavenTrackBlankMessage *m = new RavenTrackBlankMessage(bank, which, false);
                    RAVEN->nscCallback(type, subType, bank, which, data);
                    postMessage(m);
                    return 0;
                }
            }
        }
    }

//    if(String(data.c_str()).compare(String("Hit SHUTTL")) == 0){printf("PROTOOLS IS IN SHUTTLE MODE\n");}
//    if(String(data.c_str()).compare(String("Hit SCRUB ")) == 0){printf("PROTOOLS IS IN SCRUB MODE\n");}
    
    RAVEN->nscCallback(type, subType, bank, which, data);
	return 0;
}

void RavenContainer::setDawMode(EDAWMode mode)		// this gets called when the mode changes remotely (all devices must have the same mode).
{
    //unused for now...
}

void RavenContainer::startInitialization()
{
    //Now start our HUI initialization timer...
    static bool initOnce = true;
    if(initOnce)
    {
        loadingWindow->updateStatusLabel("Initializing HUI");
        printf("STARTING HUI INITIALIZATION\n");
        if(RAVEN_SKIP_INITIALIZATION)
        {
            finishedInitialization();
        }
        else
        {
            //figure out pan mode, flip mode, and zoom mode 
            startTimer(eCheckForPanModeTimerID, timerInterval);
        }
        initOnce = false;
    }
}

void RavenContainer::finishedInitialization()
{
    static bool finishedOnce = true;
    if(finishedOnce)
    {
        loadingWindow->updateStatusLabel("Finished HUI Initialization");
        printf("FINISHED INITIALIZATION\n");
        
        //Needed to pass through keyboard commands properly
        // this is what makes the Raven a background app (NSActivationPolicy Prohibited)
        if(RAVEN_IS_BACKGROUND_APP)
            Process::setDockIconVisible(false);
        
        LOCK_JUCE_THREAD //you have to lock thread before deleting window if you're skipping initialization 
            delete loadingWindow;

        startTimer(eEverySecondTimerID, 1000);
        //start in track zoom mode
        functionsManager->getNavPadComponent()->enterTrackZoomMode();
        
        //allow flipMode listener in Rack container
        functionsManager->getMasterFlipModeButton()->getProperties().set("doneInitializing", true);
        mixer->setAllAutomationImagesOff();
        sends->setAllBlank();
        
        //unclip meters
        RavenClearMetersMessage *m = new RavenClearMetersMessage();
        postMessage(m);
        
        RAVEN->finishedInitialization();
        finishedOnce = false;
    }
}
