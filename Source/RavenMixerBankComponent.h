#ifndef __JUCER_HEADER_RAVENMIXERBANKCOMPONENT_RAVENMIXERBANKCOMPONENT_D9B2B83__
#define __JUCER_HEADER_RAVENMIXERBANKCOMPONENT_RAVENMIXERBANKCOMPONENT_D9B2B83__

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenMixerChannelComponent.h"
#include "NscGuiElement.h"

class RavenMixerBankComponent  : public Component, public NscGuiContainer, public Slider::Listener,  public Button::Listener
{
public:
    RavenMixerBankComponent (int numChans, int bankNum);
    ~RavenMixerBankComponent();
    
    // Slider::Listener interface:
	void 	sliderValueChanged (Slider *slider);
	void 	sliderDragStarted (Slider *slider);
	void 	sliderDragEnded (Slider *slider);
    
    // Button::Listener interface
	void	buttonStateChanged (Button*);
	void	buttonClicked (Button* buttonThatWasClicked);

    void enterFlipMode()
    {
        for(int i = 0; i < channels.size(); i++) channels[i]->enterFlipMode();
    }
    void exitFlipMode()
    {
        for(int i = 0; i < channels.size(); i++) channels[i]->exitFlipMode();
    }
    void flipAlpha(bool on)
    {
        for(int i = 0; i < channels.size(); i++) channels[i]->flipAlpha(on);
    }
    
    void setFinePan(bool fp)
    {
        for(int i = 0; i < channels.size(); i++) channels[i]->setFinePan(fp);
    }
    
    void setFineFader(bool ff)
    {
        for(int i = 0; i < channels.size(); i++)
        {
            channels[i]->setFineFader(ff);
        }
    }
    
    void setRackContainerButtonListener(Button::Listener * listener)
    {
        for(int i = 0; i < channels.size(); i++) channels[i]->setRackContainerButtonListener(listener);
    }
    
    void toggleInsertButtonsOff()
    {
        for(int i = 0; i < channels.size(); i++) channels[i]->toggleInsertButtonOff();
    }
    
    RavenMixerChannelComponent* getChannel(int num){return channels[num];}

    void toggleIsInPanMode ()
    {
        isInPanMode = !isInPanMode;
    }

    void setIsInPanMode(bool _isInPanMode)
    {
        isInPanMode = _isInPanMode;
    }

    void setAutomationImage(int which, const String &automationType)
    {
        channels[which]->setAutomationImage(automationType, false);
    }
    
    void resized()
    {
        for (int i=0; i<channels.size();i++)
            channels[i]->setSize(channels[i]->getWidth(), getHeight());
    }
    
    void visibilityChanged()
    {
        for (int i=0; i<channels.size();i++)
            channels[i]->setVisible(isVisible());
    }
    
    
private:

    OwnedArray<RavenMixerChannelComponent> channels;
    bool isInPanMode;
    int64 lastSelectTimes[chansPerBank];
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenMixerBankComponent)
    
    
};


#endif 
