

#ifndef __JUCER_HEADER_RAVENSENDSCOMPONENT_RAVENSENDSCOMPONENT_2F05906E__
#define __JUCER_HEADER_RAVENSENDSCOMPONENT_RAVENSENDSCOMPONENT_2F05906E__

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenSendsBankComponent.h"
#include "RavenWindowComponent.h"
#include "RavenGUI.h"

class RavenSendsComponent : public RavenWindowComponent, public Button::Listener, public ActionListener
{
public:
    RavenSendsComponent (int numBanks, int chansPerBank) : mNumBanks(0), mChansPerBank(0)/*, meterMode(false), largeMeterThreadPool(numBanks)*/
    {
        setSize (0, 0);
        int xOffset = 0;
        if(RAVEN_24) xOffset = 0;
        for(int i = 0; i < numBanks; i++)
        {
            RavenSendsBankComponent *bank = new RavenSendsBankComponent(chansPerBank, i);
            setSize(getWidth() + bank->getWidth(), bank->getHeight());
            addAndMakeVisible(bank);
            bank->addButtonListener(this);
            bank->setTopLeftPosition(xOffset, 0);
            xOffset += bank->getWidth();
            banks.add(bank);
        }
        
        for(int i = 0;i<numBanks;i++)
        {
            for(int j = 0;j<chansPerBank;j++)
            {
                banks[i]->getChannel(j)->addSendsChannelButtonListener(this);
            }
        }
        
        mNumBanks = numBanks;
        mChansPerBank = chansPerBank;
        
        ////draw 4 pix before mixer channels
        Rectangle<int>clipRect(0, 0, SENDS_X_OFFSET, sendPanelImg.getHeight());
        sendPanelImg = sendPanelImg.getClippedImage(clipRect);
        ////
        
        /*
        for(int i = 0; i < 24; i++)
        {
            File imgFile(RavenGUI::getSendKnobBGPath(i+1));
            sendPanBGImgs[i] = ImageCache::getFromFile(imgFile);
        }
         */
        
        // ???
        setPaintingIsUnclipped(true);
        // ???
        
        
//        hideButton->setTopLeftPosition(30, 120);
//        hideButton->setName("hideSends");
//        ravenTouchButtons.add(hideButton);
//        addChildComponent(hideButton);
        
        #if !RAVEN_DEBUG
        //Invisible Mouse Hiding Component (hides mouse and intercepts clicks)
        //MacHideBackGroundMouse enables this to work
        mouseHider = new MouseHideComponent(getWidth(), getHeight());
        addAndMakeVisible(mouseHider);
        #endif
    }
    
    ~RavenSendsComponent()
    {
        //largeMeterThreadPool.removeAllJobs (true, 2000);
    }
    
    void actionListenerCallback (const String &message)
    {
//        if(message == "enter meter mode")
//        {
//            setMeterMode(true);
//        }
//        if(message == "exit meter mode")
//        {
//            setMeterMode(false);
//        }
    }
    
    void setNscLink	(class NscLink * link_)
    {
        for(int i = 0; i < banks.size(); i++)
        {
            banks[i]->setNscLink(link_);
            for(int c = 0; c < 8; c++) banks[i]->getChannel(c)->setNscLink(link_);
        }
    }
    
    void enterFlipMode()
    {
        for(int i = 0; i < banks.size(); i++) banks[i]->enterFlipMode();
    }
    void exitFlipMode()
    {
        for(int i = 0; i < banks.size(); i++) banks[i]->exitFlipMode();
    }
    
    void setFinePan(bool fp)
    {
        for(int i = 0; i < banks.size(); i++) banks[i]->setFinePan(fp);
    }
    
    void setFineFader(bool ff)
    {
        for(int i = 0; i < banks.size(); i++) banks[i]->setFineFader(ff);
    }
    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, float val)
    {        
        for (int i=0; i<banks.size();++i)
        {
            if (bank == eMatchesAllBanks || bank == i)
                banks[i]->callback(type, subType, bank, which, val);
        }
    }
    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, const char * text)
    {
        for (int i=0; i<banks.size();++i)
        {
            if (bank == eMatchesAllBanks || bank == i)
                banks[i]->callback(type, subType, bank, which, text);
        }
    }
        
    int getNumBanks(){return mNumBanks;}
    int getChansPerBank(){return mChansPerBank;}
  
    RavenSendsBankComponent* getBank(int num){return banks[num];}
    
    void	buttonStateChanged (Button*){}
    
	void	buttonClicked (Button* b)
    {
        String bn = b->getName();
        if(bn == "SendA" || bn == "SendB" || bn == "SendC" || bn == "SendD" || bn == "SendE")
        {
            for(int i = 0; i < banks.size(); i++)
            {
                banks[i]->setGlobalSendChannel(b);
                for(int j = 0; j < 8; j++)
                {
                    banks[i]->getChannel(j)->setGlobalSendChannel(b);
                }
            }
            
        }
    }
    
    void setNotBlank(int bank, int which)
    {
        banks[bank]->getChannel(which)->setBlank(false);
    }
    
    void setAllBlank()
    {
        for (int i=0; i<banks.size();++i)
            for(int j = 0;j<chansPerBank;j++)
                banks[i]->getChannel(j)->setBlank(true);
    }
    
    void resetAllBlankFrames()
    {
        for (int i=0; i<banks.size();++i)
            for(int j = 0;j<chansPerBank;j++)
                banks[i]->getChannel(j)->setBlankFrames(0);
    }
    
    void checkBlankFrames()
    {
        for (int i=0; i<banks.size();++i)
            for(int j = 0;j<chansPerBank;j++)
                banks[i]->getChannel(j)->checkBlankFrames();
    }
    
    void resized()
    {
        for (int i=0; i<banks.size();i++)
            banks[i]->setSize(banks[i]->getWidth(), getHeight());
        
    }
    
    void visibilityChanged()
    {
        for (int i=0; i<banks.size();i++)
            banks[i]->setVisible(isVisible());
    }
    
//    void setMeterMode(bool _meterMode)
//    {
//        meterMode = _meterMode;
//        for (int i=0; i<banks.size();i++)
//            banks[i]->setMeterMode(_meterMode);
//    }
    
//    bool getMeterMode()
//    {
//        return meterMode;
//    }
    
private:

    int mNumBanks, mChansPerBank;
    OwnedArray<RavenSendsBankComponent> banks;
    
    Image sendPanelImg;
    
//    bool meterMode;
    //Image sendPanBGImgs[24];
    //ThreadPool largeMeterThreadPool;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenSendsComponent)
};


#endif  
