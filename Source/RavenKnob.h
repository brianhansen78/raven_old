

#ifndef Raven_RavenKnob_h
#define Raven_RavenKnob_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenLabel.h"
#include "NscGuiElement.h"

typedef enum
{
    eKnobMixerMono,
    eKnobMixerStereo,
    eKnobSendMono,
    eKnobSendStereo
} EKnobType;

class RavenKnob : public Slider, public NscGuiElement
{
public:
    RavenKnob(NscGuiContainer *owner, int bank, int chan);
    ~RavenKnob();
    
    void paint (Graphics& g);
    
    void setKnobType(EKnobType type);
    
    // NscGuiElement overrides
	virtual void updateNscValue(float value);			// callback when DAW changes a value; generally
    
    void setPanLLabel(RavenLabel *lbl) { panLLabel = lbl; }
    void setPanRLabel(RavenLabel *lbl) { panRLabel = lbl; }
    
    float prevVal;
    
    bool isBeingTouched;
    
    void setNscLink	(class NscLink * link_) {link = link_;}
    
    int panIncrements;
    
    void syncPanning();
    void updatePanning();
    
    bool isBlank(){return blank;}
    void setBlank(bool _blank, bool forceRepainting = false);
    void setBlankMaster(bool _blank);
    
    void zeroKnob()
    {
        printf("zero knob\n");
        prevVal = getValue();
        setValue(0.5, sendNotification);
        lastPanIndex = 5;
        panIncrements = 50;
        panLLabel->setText(String(0), false);
        panRLabel->setText(String(0), false);
    }
    
    void setHidden(bool _hidden)
    {
        hidden = _hidden;
        setBlank(blank, true);
    }
    
    bool isHidden(){ return hidden;}
    
    
private:

	Image knobImage;
    int knobFrameWidth;
    int knobFrameHeight;
    int knobNumFrames;
    int mBank, mChan;
    
    RavenLabel *panLLabel;
    RavenLabel *panRLabel;
    
    NscLink *link;
    
    int lastPanIndex;
    
    bool blank;
    bool hidden;//used for hybrid mode non-pan control set >_<
    
    Image panLevelDisplayImage;
    EKnobType knobType;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenKnob)
};

#endif
