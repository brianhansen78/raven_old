//
//  RavenState.h
//  Raven
//
//  Created by Joshua Dickinson on 4/3/13.
//
//

#ifndef Raven_RavenState_h
#define Raven_RavenState_h

#include "RavenFunctionsComponent.h"
#include "RavenFunctionsManager.h"
#include "RavenTrackNameComponent.h"
#include "RavenMixerComponent.h"
#include "RavenSendsComponent.h"
#include "RavenFloatingMixerControlComponent.h"
#include "RavenSettingsComponent.h"
#include "RavenIconPaletteComponent.h"
#include "RavenConfig.h"
#include "RavenWindow.h"

#define RAVEN RavenState::getInstance()

class RavenState
{
public:
    static RavenState* getInstance()
    {
        if (!instance)   // Only allow one instance of class to be generated.
            instance = new RavenState;
        return instance;
    }

    void setHybridMode(bool _hybridMode)
    {
        if(hybridMode == false && _hybridMode == true)
        {
            //enter hybrid mode
            hybridMode = true;
            funcManager->enterHybridMode(); //this just shows FE
            mixerComponent->enterHybridMode();
            sendsComponent->getWindow()->removeFromDesktop();
            namesComponent->getWindow()->removeFromDesktop();
            floatingMixerControlComponent->getWindow()->addToDesktopWithOptions();
            if(iconPaletteComponent->getWindow()->isOnDesktop())
            {
                iconPaletteComponent->getWindow()->removeFromDesktop();
                iconPaletteComponent->deActivateIcon();
            }
        }
        else if(hybridMode == true && _hybridMode == false)
        {
            //exit hybrid mode
            hybridMode = false;
            if(!dawMode)
            {
                sendsComponent->getWindow()->addToDesktopWithOptions();
                namesComponent->getWindow()->addToDesktopWithOptions();
            }
            else
            {
                mixerComponent->getWindow()->removeFromDesktop();
            }
            
            mixerComponent->exitHybridMode();
            floatingMixerControlComponent->getWindow()->removeFromDesktop();
            funcManager->exitHybridMode();
        }
    }
    
    bool isHybridMode(){ return hybridMode;}
    
    void setHybridPanMode(bool _hybridPanMode)
    {
        hybridPanMode = _hybridPanMode;
        mixerComponent->setHybridPanMode(hybridPanMode);
    }
    bool isHybridPanMode(){ return hybridPanMode;}

    void setDawMode(bool _dawMode){ dawMode = _dawMode;}
    bool isDawMode(){ return dawMode;}
    
    void setSaveToSession(bool _session){ saveToSession = _session; }
    bool isSaveToSession(){ return saveToSession;}
    
    void setComponentPointers(RavenFunctionsManager* _funcManager, RavenTrackNameComponent* _names, RavenMixerComponent* _mixer, RavenSendsComponent* _sends, RavenFloatingMixerControlComponent* _floatingMixerControlComponent, RavenSettingsComponent *_settingsComponent, RavenIconPaletteComponent *_iconPaletteComponent)
    {
        funcManager = _funcManager;
        namesComponent = _names;
        mixerComponent = _mixer;
        sendsComponent = _sends;
        floatingMixerControlComponent = _floatingMixerControlComponent;
        settingsComponent = _settingsComponent;
        iconPaletteComponent = _iconPaletteComponent;
    }
    
    RavenFunctionsManager* functionsManager(){ return funcManager;}
    RavenTrackNameComponent* names(){ return namesComponent;}
    RavenMixerComponent* mixer(){ return mixerComponent;}
    RavenSendsComponent* sends(){ return sendsComponent;}
    RavenFloatingMixerControlComponent* floatingMixerControl(){ return floatingMixerControlComponent;}
    RavenSettingsComponent* settins(){ return settingsComponent;}
    RavenIconPaletteComponent* iconPalette(){ return iconPaletteComponent;}
    
private:
    //Constructor is private so that it can  not be called
    RavenState() : hybridMode(false), hybridPanMode(false), dawMode(false) {}
    
    RavenState(RavenState const&){}             // copy constructor is private
    RavenState& operator=(RavenState const&){}  // assignment operator is private
    static RavenState* instance;
    
    bool saveToSession;
    bool hybridMode, hybridPanMode;
    bool dawMode;
    int savedNumberOfDawModeBars;
    int savedNumberOfMixModeBars;
    
    RavenFunctionsManager *funcManager;
    RavenTrackNameComponent *namesComponent;
    RavenMixerComponent *mixerComponent;
    RavenSendsComponent *sendsComponent;
    RavenFloatingMixerControlComponent *floatingMixerControlComponent;
    RavenSettingsComponent *settingsComponent;
    RavenIconPaletteComponent *iconPaletteComponent;

};

#endif
