
#ifndef Raven_RavenHotKeysComponent_h
#define Raven_RavenHotKeysComponent_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenTouchComponent.h"
#include "RavenButton.h"
#include "RavenTextDisplay.h"
#include "RavenMacUtilities.h"
#include "RavenHotKeyProgrammingComponent.h"

#define NUM_HOT_KEYS 8

class RavenHotKeysComponent : public RavenTouchComponent, public Button::Listener, public KeyListener, public Timer, public ActionListener
{
public:
    RavenHotKeysComponent();
    ~RavenHotKeysComponent();
    
    void timerCallback();
    
    void actionListenerCallback (const String &message);
    
    void setHotKeyPanel(RavenHotKeyProgrammingComponent *cmp)
    {
        hotKeyPanel = cmp;
        //hotKeyPanel->getNameButton()->addListener(this);
        hotKeyPanel->addActionListener(this);
    }
    
    //Getter/Setter Functions for custom hot keys (useful for loading/saving presets)
    String getKeyLabelText(int keyIndex) { return keyLabels[keyIndex]->getText(); } // keyIndex is 0 to 7 (8 hot keys per hot key comp)
    void setKeyLabelText(int keyIndex, String text) { keyLabels[keyIndex]->setText(text, false); }
    std::vector<int> getKeyCode(int keyIndex) { return keyCodes[keyIndex]; }
    void setKeyCode(int keyIndex, std::vector<int> code) { keyCodes[keyIndex] = code; }
    bool isProgramMode() {return programMode; }
    
private:
    void paint (Graphics& g);
    void buttonStateChanged (Button *b);
    void buttonClicked (Button *b);
    
    bool keyPressed (const KeyPress &key, Component *originatingComponent);
    bool keyStateChanged (bool isKeyDown, Component *originatingComponent);
    
    OwnedArray<Label> keyLabels;
    std::vector<int> keyCodes[NUM_HOT_KEYS];
    int currentButton;
    bool programMode;
    Time downTimes[NUM_HOT_KEYS];
    RavenHotKeyProgrammingComponent *hotKeyPanel;
    String defaultNameText, customNameText;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenHotKeysComponent)
    
};

#endif
