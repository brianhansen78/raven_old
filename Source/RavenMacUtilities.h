
#ifndef Raven_RavenMacUtilities_h
#define Raven_RavenMacUtilities_h

#include <vector>

class RavenMacUtilities
{
public:
    static bool checkHostName(const char* serverName);
    static bool checkSurroundPannerTouch(int touchX, int touchY);
    static void touchMouseDown(int touchX, int touchY);
    static void touchMouseDrag(int touchX, int touchY);
    static void surroundPannerDrag(int touchX, int touchY);
    static void touchMouseUp(int touchX, int touchY);
    static void setLastXY(int touchX, int touchY);
    static int lastX, lastY, initialX, initialY;
    static void turnOnEventMonitor();
    static void touchMouseClick(int touchX, int touchY);
};

typedef enum {
    eRavenModifer_Command,
    eRavenModifer_Option,
    eRavenModifer_Control,
    eRavenModifer_Shift
} ERavenModiferKey;

class MacKeyPress
{
public:
    static void pressCombo(const std::vector<int> &keyCodes);
    static void toggleModifier(ERavenModiferKey key, bool down);
    static bool isCommandDown();
    static bool isShiftDown();
    static bool isOptionDown();
    static bool isControlDown();
    static void holdKey(int key);
    static void pressKey(int key);
    static void releaseKey(int key);
};


#endif
