

#include "RavenFunctionsComponent.h"
#include "RavenState.h"

const int topOffset = 10;
int bottomOffset = 10;
const int horizontalSpacing = 10;
const int sideSpacing = 50;

RavenFunctionsComponent::RavenFunctionsComponent (int rackPos) : RavenRackComponent(rackPos), toolbarMode(true), customMode(false), numMixModeBars(1), numDAWModeBars(2), inFlipMode(false)
{
    
    bottomPanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "TOOLBAR/TOOLBAR/Solo/ToolBar/ToolBarPanel_03.png"));
    singlePanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "TOOLBAR/TOOLBAR/Solo/ToolBarExpand/ToolBarPanel_02.png"));
    customPanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "TOOLBAR/TOOLBAR/Solo/ToolbarCustomize/ToolBarPanel_01.png"));
    
    railImg = ImageCache::getFromFile(File(String(GUI_PATH) + "RailRack-Images/Rail-Rack/full+background.png"));
    
    singlePanelHeight = singlePanelImg.getHeight();
    //customPanelHeight = customPanelImg.getHeight();
    
    setSize (bottomPanelImg.getWidth(), numDAWModeBars*singlePanelHeight);
    
    hideButton->setVisible(false);
    ravenTouchButtons.add(hideButton);
        
    editToolCmp = new EditToolComponent();
    editToolCmp->setTopLeftPosition(1037, 20);
    //editToolCmp->setTopLeftPosition(sideSpacing, topOffset);
    addAndMakeVisible(editToolCmp);
    
    CopyPasteComponent *editCopyPasteCmp = new CopyPasteComponent(this);
    editCopyPasteCmp->setTopLeftPosition(1037, 80);
    //edit2Cmp->setTopLeftPosition(sideSpacing, topOffset + 60);
    addAndMakeVisible(editCopyPasteCmp);
    
    QuantizationComponent *editQuantCmp = new QuantizationComponent();
    editQuantCmp->setTopLeftPosition(376, 25);
    //edit3Cmp->setTopLeftPosition(editToolCmp->getRight()+horizontalSpacing, topOffset);
    addAndMakeVisible(editQuantCmp);
    
    NudgeCycleComponent *editNudgeCmp = new NudgeCycleComponent(this);
    editNudgeCmp->setTopLeftPosition(486, 25);
    //edit4Cmp->setTopLeftPosition(edit3Cmp->getRight()+horizontalSpacing, topOffset);
    addAndMakeVisible(editNudgeCmp);
    
    DropReplaceComponent *editDropReplaceCmp = new DropReplaceComponent(this);
    editDropReplaceCmp->setTopLeftPosition(486, 70);
    //edit4Cmp->setTopLeftPosition(edit3Cmp->getRight()+horizontalSpacing, topOffset);
    addAndMakeVisible(editDropReplaceCmp);
    
    trackBankCmp = new RavenTrackBankComponent(this);
    trackBankCmp->setTopLeftPosition(80, singlePanelHeight+18);
    //trackBankCmp->setTopLeftPosition(80, singlePanelHeight+topOffset);
    addAndMakeVisible(trackBankCmp);
    
    RavenUtilitiesComponent *utilCmp = new RavenUtilitiesComponent(this);
    utilCmp->setTopLeftPosition(245, singlePanelHeight+10);
    //utilCmp->setTopLeftPosition(330, singlePanelHeight+topOffset);
    addAndMakeVisible(utilCmp);

    modComp = new RavenModifiersComponent(this);
    modComp->setTopLeftPosition(245, singlePanelHeight+72);
    addAndMakeVisible(modComp);
    
    RavenPunchComponent *punchCmp = new RavenPunchComponent(this);
    punchCmp->setTopLeftPosition(607, -75);
    //punchCmp->setTopLeftPosition(edit4Cmp->getRight()+horizontalSpacing, edit4Cmp->getY());
    addAndMakeVisible(punchCmp);
    
    RavenInputGroupSuspendComponent *igsCmp = new RavenInputGroupSuspendComponent(this);
    igsCmp->setTopLeftPosition(638, singlePanelHeight+20);
    //igsCmp->setTopLeftPosition(630, singlePanelHeight+topOffset);
    addAndMakeVisible(igsCmp);
    
    saveLayoutCmp = new RavenSaveLayoutComponent();
    saveLayoutCmp->setTopLeftPosition(78, 18);
//    saveLayoutCmp->setTopLeftPosition(punchCmp->getRight()+horizontalSpacing, topOffset);
    addAndMakeVisible(saveLayoutCmp);

    RavenRollComponent *rollCmp = new RavenRollComponent(this);
    rollCmp->setTopLeftPosition(975, singlePanelHeight+15);
    //rollCmp->setTopLeftPosition(970, singlePanelHeight+topOffset);
    addAndMakeVisible(rollCmp);
    
    RavenTransportComponent *transCmp = new RavenTransportComponent(this);
    transCmp->setTopLeftPosition(975, singlePanelHeight+60);
    //transCmp->setTopLeftPosition(970, rollCmp->getBottom());
    addAndMakeVisible(transCmp);
    
    editMixCmp = new RavenEditMixComponent(this);
    editMixCmp->setTopLeftPosition(1306, singlePanelHeight + 15);
    //editMixCmp->setTopLeftPosition(transCmp->getX()+transCmp->getWidth() + topOffset, rollCmp->getY()+2);
    addAndMakeVisible(editMixCmp);
    
    clickButton = new RavenNSCButton(this);
    clickButton->setTopLeftPosition(583, singlePanelHeight + 74);
    //clickButton->setTopLeftPosition(editMixCmp->getX(), editMixCmp->getBottom()-20);
    clickButton->addListener(this);
    clickButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/EditMixClick/images/edit-click-on_02.png", "TOOLBAR/FUNCTIONMODULE/EditMixClick/images/edit-click-off_02.png");
    clickButton->setTag(eNSCGSClickSwitch);
    clickButton->setClickingTogglesState(true);
    ravenNSCTouchButtons.add(clickButton);
    addAndMakeVisible(clickButton);
    
    moveButton = new RavenButton();
    moveButton->setClickingTogglesState(true);
    moveButton->setTopLeftPosition(1416, singlePanelHeight + 16);
    //moveButton->setTopLeftPosition(editMixCmp->getRight(), rollCmp->getY());
    moveButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/MoveFine/images/Movemodule-Finepan-on_01.png", "TOOLBAR/FUNCTIONMODULE/MoveFine/images/Movemodule-Finepan-off_01.png");
    ravenTouchButtons.add(moveButton);
    addAndMakeVisible(moveButton);
    
    fineFaderButton = new RavenButton();
    fineFaderButton->setClickingTogglesState(true);
    fineFaderButton->setTopLeftPosition(1416, singlePanelHeight + 75);
    //fineFaderButton->setTopLeftPosition(clickButton->getRight(), clickButton->getY());
    fineFaderButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/FineFader/FineFaders-on.png", "TOOLBAR/FUNCTIONMODULE/FineFader/FineFaders-off.png");
    ravenTouchButtons.add(fineFaderButton);
    addAndMakeVisible(fineFaderButton);
    
    fineButton = new RavenButton();
    fineButton->setClickingTogglesState(true);
    //fineButton->setTopLeftPosition(moveButton->getX(), clickButton->getY());
    fineButton->setTopLeftPosition(583, singlePanelHeight + 11);
    fineButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanModes/SwitchTO_CircularPanMode.png", "TOOLBAR/FUNCTIONMODULE/PanModes/CircularPanMode.png");
    ravenTouchButtons.add(fineButton);
    addAndMakeVisible(fineButton);
    
    //**** NOTE: Not using the drum pad for now... ****
    drumPadButton = new RavenButton();
    drumPadButton->setName("drumPad");
    drumPadButton->setClickingTogglesState(true);
    drumPadButton->setTopLeftPosition(moveButton->getX()+300, clickButton->getY()+30);
    drumPadButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/DrumPadIcon/test_button_on.png", "TOOLBAR/FUNCTIONMODULE/DrumPadIcon/test_button_off.png");
    drumPadButton->getProperties().set("defaultX", 1830);
    drumPadButton->getProperties().set("defaultY", 75);
    ravenTouchButtons.add(drumPadButton);
    //addAndMakeVisible(drumPadButton);
    //*************************************************
    
#if RAVEN_ESSENTIALS_PALETTE
    essentialsButton = new RavenButton();
    essentialsButton->setName("essentialsButton");
    essentialsButton->setClickingTogglesState(true);
    essentialsButton->setTopLeftPosition(772, singlePanelHeight + 65);
    essentialsButton->setImagePaths("FloatingEssentials/bt-Essential-On.png", "FloatingEssentials/bt-Essential-Off.png");
    essentialsButton->getProperties().set("defaultX", 1830);
    essentialsButton->getProperties().set("defaultY", 75);
    essentialsButton->getProperties().set("CustomHeight", 65);
    ravenTouchButtons.add(essentialsButton);
    addAndMakeVisible(essentialsButton);
#endif
    
//    floatingNavPadButton = new RavenButton();
//    floatingNavPadButton->setName("floatingNavPadButton");
//    floatingNavPadButton->setClickingTogglesState(true);
//    floatingNavPadButton->setTopLeftPosition(moveButton->getX()+300, clickButton->getY()+30);
//    floatingNavPadButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/DrumPadIcon/test_button_on.png", "TOOLBAR/FUNCTIONMODULE/DrumPadIcon/test_button_off.png");
//    floatingNavPadButton->getProperties().set("defaultX", 1830);
//    floatingNavPadButton->getProperties().set("defaultY", 75);
//    ravenTouchButtons.add(floatingNavPadButton);
//    addAndMakeVisible(floatingNavPadButton);

    hotComp = new RavenHotKeysComponent();
    hotComp->setTopLeftPosition(1490, singlePanelHeight+18);
    //hotComp->setTopLeftPosition(fineButton->getX() + fineButton->getWidth() + topOffset, singlePanelHeight+topOffset);
    addAndMakeVisible(hotComp);
    
    hotComp2 = new RavenHotKeysComponent();
    hotComp2->setTopLeftPosition(1344, -125);
    //hotComp2->setTopLeftPosition(1050, -125);
    addAndMakeVisible(hotComp2);
    
    hotComp3 = new RavenHotKeysComponent();
    hotComp3->setTopLeftPosition(hotComp2->getRight()+horizontalSpacing, -125);
    //hotComp3->setTopLeftPosition(hotComp2->getRight()+horizontalSpacing, -125);
    addAndMakeVisible(hotComp3);
    
    navPadCmp = new RavenNavPadComponent(this);
    navPadCmp->setTopLeftPosition(1508, 18);
    //navPadCmp->setTopLeftPosition(saveLayoutCmp->getRight()+horizontalSpacing, topOffset);
    addAndMakeVisible(navPadCmp);
    
    RavenMemoryComponent *memComp = new RavenMemoryComponent(this);
    memComp->setTopLeftPosition(615, 27);
    //memComp->setTopLeftPosition(sideSpacing, -singlePanelHeight+topOffset);
    memComp->setEnterButton(utilCmp->getEnterButton()); //link enter buttons => memComp must be initialized after utilComp
    addAndMakeVisible(memComp);

    exitButton = new RavenButton();
    exitButton->setClickingTogglesState(true);
    exitButton->setName("exitButton");
    exitButton->setImagePaths("PowerButtons/NEW_POWER-OFF.png", "PowerButtons/NEW_POWER-ON.png");
    exitButton->setTopLeftPosition(0, singlePanelHeight + 63);
    ravenTouchButtons.add(exitButton);
    addAndMakeVisible(exitButton);
    
    lastWinButton = new RavenButton();
    lastWinButton->setName("lastWinButton");
    lastWinButton->setClickingTogglesState(true);
    lastWinButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/SHOWHIDE_PLUG/bt-Showhide-Plugin-On.png", "TOOLBAR/FUNCTIONMODULE/SHOWHIDE_PLUG/bt-Showhide-Plugin-Off.png");
    lastWinButton->setTopLeftPosition(521, singlePanelHeight + 75);
    //lastWinButton->setTopLeftPosition(trackBankCmp->getRight()+horizontalSpacing, trackBankCmp->getY()+3);
    lastWinButton->getProperties().set("defaultX", 1675);
    lastWinButton->getProperties().set("defaultY", 75);
    RAVEN_BUTTON_1(lastWinButton);
    addAndMakeVisible(lastWinButton);
    lastWinButton->addListener(this);
    ravenTouchButtons.add(lastWinButton);
    
    counterFuncCmp = new RavenCounterFunctionsComponent(this);
    //counterFuncCmp->setTopLeftPosition(hotComp->getRight() + 0, singlePanelHeight + 13);
    counterFuncCmp->setTopLeftPosition(860, singlePanelHeight + 65);
    
    addAndMakeVisible(counterFuncCmp);
    
    toolBarModeButton = new RavenButton();
    toolBarModeButton->setClickingTogglesState(true);
    toolBarModeButton->setName("toolbarMode");
//    toolBarModeButton->setTopLeftPosition(1810, singlePanelHeight+topOffset);
    toolBarModeButton->setTopLeftPosition(1734, 158);
    toolBarModeButton->addListener(this); //need different listener since its not an NSC button
    toolBarModeButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/RavenDAW/bt-RavenDaw-DawOn.png", "TOOLBAR/FUNCTIONMODULE/RavenDAW/bt-RavenDaw-DawOff.png");
    toolBarModeButton->getProperties().set("isDAW", false);
    RAVEN_BUTTON_1(toolBarModeButton);
    
    //NEW RAVEN DAW AND RAVEN TOOLBAR IMAGES 1.10.2013
    
    ravenTouchButtons.add(toolBarModeButton);
    addAndMakeVisible(toolBarModeButton);
    
    toolBarCustomButton = new RavenButton();
    toolBarCustomButton->setClickingTogglesState(true);
    toolBarCustomButton->setName("toolbarCustom");
    RAVEN_BUTTON_1(toolBarCustomButton);
//    toolBarCustomButton->setTopLeftPosition(1810, singlePanelHeight+70);
    toolBarCustomButton->setTopLeftPosition(1734, 212);
    //toolBarCustomButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/ToolBarSettings/images/toolbarsettings-on_02.png", "TOOLBAR/FUNCTIONMODULE/ToolBarSettings/images/toolbarsettings-off_02.png");
    toolBarCustomButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/RavenSettings/ravenSettings-on.png", "TOOLBAR/FUNCTIONMODULE/RavenSettings/ravenSettings-off.png");
    ravenTouchButtons.add(toolBarCustomButton);
    addAndMakeVisible(toolBarCustomButton);
    
    targetDAWButton = new RavenButton();
    targetDAWButton->setClickingTogglesState(true);
    targetDAWButton->setName("targetDAW");
    targetDAWButton->setTopLeftPosition(trackBankCmp->getRight()+ 15, singlePanelHeight + 50);
    targetDAWButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/TargetDaw/targetdaw-on.png", "TOOLBAR/FUNCTIONMODULE/TargetDaw/targetdaw-off.png");
    ravenTouchButtons.add(targetDAWButton);
    //addAndMakeVisible(targetDAWButton);
        
    //add child touch components in desired touch order
    
    //childTouchComponents.add(hotKeyPanel);
    
    moveButton->getProperties().set("defaultX", 1000);
    moveButton->getProperties().set("defaultY", 140);
    RAVEN_BUTTON_2(moveButton);
    moveButton->setName("moveModulesButton");
    
    clickButton->getProperties().set("defaultX", 1225);
    clickButton->getProperties().set("defaultY", 190);
    RAVEN_BUTTON_2(clickButton);
    clickButton->setName("clickButton");
    
    fineFaderButton->getProperties().set("defaultX", 515);
    fineFaderButton->getProperties().set("defaultY", 65);
    RAVEN_BUTTON_1(fineFaderButton);
    fineFaderButton->setName("fineFaderButton");
    
    fineButton->getProperties().set("defaultX", 1300);
    fineButton->getProperties().set("defaultY", 190);
    fineButton->getProperties().set("defaultToolbarX", 0);
    fineButton->getProperties().set("defaultToolbarY", 0);
    RAVEN_BUTTON_1(fineButton);
    fineButton->setName("finePanButton");
    
    editMixCmp->getProperties().set("defaultX", 1225);
    editMixCmp->getProperties().set("defaultY", 130);
    RAVEN_BUTTON_5(editMixCmp);
    editMixCmp->setName("editMixButton");
    childTouchComponents.add(editMixCmp);
    
    editToolCmp->getProperties().set("defaultX", 60);
    editToolCmp->getProperties().set("defaultY", topOffset);
    editToolCmp->getProperties().set("CustomHeight", 45);
    editToolCmp->setName("EditToolComponent");
    childTouchComponents.add(editToolCmp);

    editCopyPasteCmp->getProperties().set("defaultX", 60);
    editCopyPasteCmp->getProperties().set("defaultY", topOffset + 60);
    editCopyPasteCmp->getProperties().set("CustomHeight", 45);
    editCopyPasteCmp->setName("CopyPasteComponent");
    childTouchComponents.add(editCopyPasteCmp);

    editQuantCmp->getProperties().set("defaultX", 313);
    editQuantCmp->getProperties().set("defaultY", topOffset);
    editQuantCmp->getProperties().set("CustomHeight", 90);
    editQuantCmp->setName("QuantizationComponent");
    childTouchComponents.add(editQuantCmp);

    editNudgeCmp->getProperties().set("defaultX", 313);
    editNudgeCmp->getProperties().set("defaultY", topOffset + 60);
    editNudgeCmp->getProperties().set("CustomHeight", 45);
    editNudgeCmp->setName("NudgeCycleComponent");
    childTouchComponents.add(editNudgeCmp);
    
    editDropReplaceCmp->getProperties().set("defaultX", 313);
    editDropReplaceCmp->getProperties().set("defaultY", topOffset + 60);
    editDropReplaceCmp->getProperties().set("CustomHeight", 45);
    editDropReplaceCmp->setName("DropReplaceComponent");
    childTouchComponents.add(editDropReplaceCmp);

    counterFuncCmp->getProperties().set("defaultX", 1750);
    counterFuncCmp->getProperties().set("defaultY", 40);
    counterFuncCmp->getProperties().set("CustomHeight", 65);
    counterFuncCmp->setName("CounterFunctionsComponent");
    childTouchComponents.add(counterFuncCmp);
    
    saveLayoutCmp->getProperties().set("defaultX", 640);
    saveLayoutCmp->getProperties().set("defaultY", topOffset);
    saveLayoutCmp->getProperties().set("CustomHeight", 110);
    saveLayoutCmp->setName("SaveLayoutComponent");
    childTouchComponents.add(saveLayoutCmp);
//    saveLayoutCmp->isOpaque();
//    saveLayoutCmp->addToDesktop(0, 0);
//    Desktop::getInstance().ravenSetWindowOptions(saveLayoutCmp);
//    saveLayoutCmp->setAlwaysOnTop(true);
//    saveLayoutCmp->setVisible(true);

    modComp->getProperties().set("CustomHeight", 55);
    modComp->setName("ModifiersComponent");
    childTouchComponents.add(modComp);
    
    trackBankCmp->getProperties().set("defaultX", 130);
    trackBankCmp->getProperties().set("defaultY", 140);
    trackBankCmp->getProperties().set("CustomHeight", 110);
    trackBankCmp->setName("TrackBankComponent");
    childTouchComponents.add(trackBankCmp);
    
    utilCmp->getProperties().set("defaultX", 290);
    utilCmp->getProperties().set("defaultY", 130);
    utilCmp->getProperties().set("CustomHeight", 55);
    utilCmp->setName("UtilitiesComponent");
    childTouchComponents.add(utilCmp);
    
    /*
    modCmp->getProperties().set("defaultX", 290);
    modCmp->getProperties().set("defaultY", 190);
    modCmp->setName("ModifiersComponent");
    childTouchComponents.add(modCmp);
     */
    
    igsCmp->getProperties().set("defaultX", 530);
    igsCmp->getProperties().set("defaultY", 140);
    igsCmp->getProperties().set("CustomHeight", 40);
    igsCmp->setName("InputGroupSuspendComponent");
    childTouchComponents.add(igsCmp);
    
    punchCmp->getProperties().set("defaultX", 530);
    punchCmp->getProperties().set("defaultY", 190);
    punchCmp->getProperties().set("CustomHeight", 55);
    punchCmp->setName("PunchComponent");
    childTouchComponents.add(punchCmp);
    
    rollCmp->getProperties().set("defaultX", 870);
    rollCmp->getProperties().set("defaultY", 140);
    rollCmp->getProperties().set("CustomHeight", 35);
    rollCmp->setName("RollComponent");
    childTouchComponents.add(rollCmp);
    
    transCmp->getProperties().set("defaultX", 870);
    transCmp->getProperties().set("defaultY", 140);
    transCmp->getProperties().set("CustomHeight", 65);
    transCmp->setName("TransportComponent");
    childTouchComponents.add(transCmp);
    
    hotComp->getProperties().set("defaultX", 1360);
    hotComp->getProperties().set("defaultY", 135);
    hotComp->getProperties().set("CustomHeight", 110);
    hotComp->setName("HotKeysComponent_0");
    childTouchComponents.add(hotComp);
    hotKeyComponents.add(hotComp);
    
    hotComp2->getProperties().set("defaultX", 1600);
    hotComp2->getProperties().set("defaultY", 135);
    hotComp2->getProperties().set("CustomHeight", 110);
    hotComp2->setName("HotKeysComponent_1");
    childTouchComponents.add(hotComp2);
    hotKeyComponents.add(hotComp2);
    
    hotComp3->getProperties().set("defaultX", 1600);
    hotComp3->getProperties().set("defaultY", 135);
    hotComp3->getProperties().set("CustomHeight", 110);
    hotComp3->setName("HotKeysComponent_2");
    childTouchComponents.add(hotComp3);
    hotKeyComponents.add(hotComp3);
    
    navPadCmp->getProperties().set("defaultX", 920);
    navPadCmp->getProperties().set("defaultY", topOffset);
    navPadCmp->getProperties().set("CustomHeight", 110);
    navPadCmp->setName("NavPadComponent");
    childTouchComponents.add(navPadCmp);
    
    memComp->getProperties().set("defaultX", 1270);
    memComp->getProperties().set("defaultY", 25);
    memComp->getProperties().set("CustomHeight", 90);
    memComp->setName("MemoryComponent");
    childTouchComponents.add(memComp);
    
    //exitToolbarMode();
    
    //flip mode buttons//////// (invisible, triggered from touch in raven container)
    flipMode = new RavenNSCButton(this);
    flipMode->setName("flipModeButton");
    flipMode->setChannelIndex(kNscGuiBankGlobalIdentifier);
    flipMode->setBankIndex(0);
    flipMode->setTag(eNSCGSFlip);
    flipMode->addListener(this);
    flipMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/SendChannel/channelOn.png", "TOOLBAR/FUNCTIONMODULE/SendChannel/channelOff.png");
    flipMode->getProperties().set("isFlipMode", false);
    flipMode->getProperties().set("defaultX", 500);
    flipMode->getProperties().set("defaultY", topOffset+10);
    //flipMode->getProperties().set("CustomHeight", ); only on 24 version!!!
    
    flipMode->setTopLeftPosition(lastWinButton->getRight(), lastWinButton->getY());
    if(RAVEN_24)
    {
        addAndMakeVisible(flipMode);
        ravenNSCTouchButtons.add(flipMode);
    }
    
    flipMode2 = new RavenNSCButton(this);
    flipMode2->setChannelIndex(kNscGuiBankGlobalIdentifier);
    flipMode2->setBankIndex(1);
    flipMode2->setTag(eNSCGSFlip);
    flipMode2->setName("flipmode2");
    flipMode2->addListener(this);
    
    flipMode3 = new RavenNSCButton(this);
    flipMode3->setChannelIndex(kNscGuiBankGlobalIdentifier);
    flipMode3->setBankIndex(2);
    flipMode3->setTag(eNSCGSFlip);
    flipMode3->setName("flipmode3");
    flipMode3->addListener(this);
    
    flipMode4 = new RavenNSCButton(this);
    flipMode4->setChannelIndex(kNscGuiBankGlobalIdentifier);
    flipMode4->setBankIndex(3);
    flipMode4->setTag(eNSCGSFlip);
    flipMode4->setName("flipmode4");
    flipMode4->addListener(this);
    
    //pan mode buttons////////
//    fakePanMode = new RavenButton();
//    fakePanMode->getProperties().set("isLeftfakePanMode", true);
//    fakePanMode->addListener(this);
//    fakePanMode->setTopLeftPosition(lastWinButton->getX(), lastWinButton->getBottom());
//    fakePanMode->setClickingTogglesState(false);
//    fakePanMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Lon.png", "TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Loff.png");
//    addAndMakeVisible(fakePanMode);
    
    panMode = new RavenNSCButton(this);
    panMode->setName("panModeButton");
    panMode->getProperties().set("isLeftPanMode", true);
    panMode->setChannelIndex(kNscGuiBankGlobalIdentifier);
    panMode->setBankIndex(0);
    panMode->setTag(eNSCGSPTBankMode_Pan);
    panMode->addListener(this);
    panMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Lon.png", "TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Loff.png");
    panMode->setTopLeftPosition(521, singlePanelHeight + 11);
    //panMode->setTopLeftPosition(lastWinButton->getX(), lastWinButton->getBottom()-25);
    addAndMakeVisible(panMode);
    panMode->getProperties().set("defaultX", 1675);
    panMode->getProperties().set("defaultY", 15);
    RAVEN_BUTTON_1(panMode);
    ravenNSCTouchButtons.add(panMode);
    
    panMode2 = new RavenNSCButton(this);
    panMode2->setChannelIndex(kNscGuiBankGlobalIdentifier);
    panMode2->setBankIndex(1);
    panMode2->setTag(eNSCGSPTBankMode_Pan);
    panMode2->setName("panmode2");
    panMode2->addListener(this);
    
    panMode3 = new RavenNSCButton(this);
    panMode3->setChannelIndex(kNscGuiBankGlobalIdentifier);
    panMode3->setBankIndex(2);
    panMode3->setTag(eNSCGSPTBankMode_Pan);
    panMode3->setName("panmode3");
    panMode3->addListener(this);
    
    panMode4 = new RavenNSCButton(this);
    panMode4->setChannelIndex(kNscGuiBankGlobalIdentifier);
    panMode4->setBankIndex(3);
    panMode4->setTag(eNSCGSPTBankMode_Pan);
    panMode4->setName("panmode4");
    panMode4->addListener(this);
    
    floatingNavpadButton = new RavenButton();
    floatingNavpadButton->setClickingTogglesState(true);
    floatingNavpadButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/FloatingNavpadBtn/FloatingNavpad-On.png", "TOOLBAR/FUNCTIONMODULE/FloatingNavpadBtn/FloatingNavpad-Off.png");
    //floatingNavpadButton->setTopLeftPosition(hotComp->getRight() -10, panMode->getBottom()-floatingNavpadButton->getHeight()- 5);
    floatingNavpadButton->setTopLeftPosition(660, singlePanelHeight + 80);
    //floatingNavpadButton->setTopLeftPosition(760, igsCmp->getBottom());
    ravenTouchButtons.add(floatingNavpadButton);
    addAndMakeVisible(floatingNavpadButton);
    
    floatingNavpadButton->getProperties().set("defaultX", 500);
    floatingNavpadButton->getProperties().set("defaultY", topOffset);
    floatingNavpadButton->getProperties().set("CustomHeight", 40);
    floatingNavpadButton->setName("floatingNavPadButton");

    /////////////////////////////////
    
    addBarButton = new RavenButton();
    addBarButton->setName("addBarButton");
    addBarButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/RackAdd/bt-AddRack-on.png", "TOOLBAR/FUNCTIONMODULE/RackAdd/bt-AddRack-off.png");
//    addBarButton->setTopLeftPosition(hotComp->getRight() + 20, hotComp->getY()+2);
    addBarButton->setTopLeftPosition(1810, 156);
    addBarButton->getProperties().set("defaultX", 1830);
    addBarButton->getProperties().set("defaultY", 140);
    RAVEN_BUTTON_2(addBarButton);
    addAndMakeVisible(addBarButton);
    addBarButton->addListener(this);
    ravenTouchButtons.add(addBarButton);
    
    removeBarButton = new RavenButton();
    removeBarButton->setName("removeBarButton");
    removeBarButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/RackRemove/bt-RemoveRack-on.png", "TOOLBAR/FUNCTIONMODULE/RackRemove/bt-RemoveRack-off.png");
//    removeBarButton->setTopLeftPosition(addBarButton->getX(), addBarButton->getBottom() - 22);
    removeBarButton->setTopLeftPosition(1810, 211);
    removeBarButton->getProperties().set("defaultX", 1830);
    removeBarButton->getProperties().set("defaultY", 200);
    RAVEN_BUTTON_2(removeBarButton);
    addAndMakeVisible(removeBarButton);
    removeBarButton->addListener(this);
    ravenTouchButtons.add(removeBarButton);
    
//    meterModeButton = new RavenButton();
//    meterModeButton->setTopLeftPosition(1333, singlePanelHeight + 75);
//    //meterModeButton->setTopLeftPosition(680, igsCmp->getBottom()-10);
//    meterModeButton->addListener(this);
//    meterModeButton->setClickingTogglesState(true);
//    meterModeButton->setToggleState(true, true);
//    meterModeButton->setName("meterModeButton");
//    RAVEN_BUTTON_1(meterModeButton);
//    meterModeButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/LargeMeterToggle/Switch-Sends2Vu-Off.png", "TOOLBAR/FUNCTIONMODULE/LargeMeterToggle/Switch-Sends2Vu-On.png");
//    
//    ravenTouchButtons.add(meterModeButton);
//    addAndMakeVisible(meterModeButton);
    
    hybridModeButton = new RavenButton();
    hybridModeButton->setTopLeftPosition(1333, singlePanelHeight + 75);
    hybridModeButton->addListener(this);
    hybridModeButton->setClickingTogglesState(true);
    hybridModeButton->setName("hybridModeButton");
    //RAVEN_BUTTON_1(hybridModeButton);
    //hybridModeButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/LargeMeterToggle/Switch-Sends2Vu-Off.png", "TOOLBAR/FUNCTIONMODULE/LargeMeterToggle/Switch-Sends2Vu-On.png");
    
    ravenTouchButtons.add(hybridModeButton);
    addAndMakeVisible(hybridModeButton);
    
    zoomInButton = new RavenButton();
    zoomInButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Zoombuttons/angledShadow/ZoomIn-on.png", "TOOLBAR/FUNCTIONMODULE/Zoombuttons/angledShadow/ZoomIn-off.png");
    zoomInButton->setTopLeftPosition(1389, 18);
    //zoomInButton->setTopRightPosition(screenWidth-sideSpacing, navPadCmp->getY());//setting top right not top left
    zoomInButton->addListener(this);
    zoomInButton->setName("zoomInButton");
    RAVEN_BUTTON_0(zoomInButton);
    ravenTouchButtons.add(zoomInButton);
    addAndMakeVisible(zoomInButton);
    
    zoomInKeyCode.push_back(55);//command
    zoomInKeyCode.push_back(30);//]
    
    zoomOutButton = new RavenButton();
    zoomOutButton->setTopLeftPosition(1347, 18);
    //zoomOutButton->setTopLeftPosition(zoomInButton->getX()-zoomInButton->getWidth(), zoomInButton->getY());
    zoomOutButton->addListener(this);
    zoomOutButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Zoombuttons/angledShadow/ZoomOut-on.png", "TOOLBAR/FUNCTIONMODULE/Zoombuttons/angledShadow/ZoomOut-off.png");
    zoomOutButton->setName("zoomOutButton");
    RAVEN_BUTTON_0(zoomOutButton);
    ravenTouchButtons.add(zoomOutButton);
    addAndMakeVisible(zoomOutButton);
    
    zoomOutKeyCode.push_back(55);//command
    zoomOutKeyCode.push_back(33);//[

    clipLoopButton = new RavenButton();
    clipLoopButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/clipLoopButton/Bt-cliploop-On.png", "TOOLBAR/FUNCTIONMODULE/clipLoopButton/Bt-cliploop-Off.png");
    clipLoopButton->setTopLeftPosition(1389, 81);
    //clipLoopButton->setTopLeftPosition(zoomOutButton->getX(), zoomOutButton->getBottom());
    clipLoopButton->addListener(this);
    clipLoopButton->setName("clipLoopButton");
    RAVEN_BUTTON_0(clipLoopButton);
    ravenTouchButtons.add(clipLoopButton);
    addAndMakeVisible(clipLoopButton);
    
    clipLoopKeyCode.push_back(55);//command
    clipLoopKeyCode.push_back(58);//option
    clipLoopKeyCode.push_back(37);//L
    
    duplicateButton = new RavenButton();
    duplicateButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/duplicateButton/bt-edit-duplicate-On.png", "TOOLBAR/FUNCTIONMODULE/duplicateButton/bt-edit-duplicate-Off.png");
    duplicateButton->setTopLeftPosition(1347, 81);
    //duplicateButton->setTopLeftPosition(clipLoopButton->getRight(), clipLoopButton->getY());
    duplicateButton->addListener(this);
    duplicateButton->setName("duplicateButton");
    RAVEN_BUTTON_0(duplicateButton);
    ravenTouchButtons.add(duplicateButton);
    addAndMakeVisible(duplicateButton);
    
    duplicateKeyCode.push_back(55);//command
    duplicateKeyCode.push_back(2);//D
    
    nudgePlusKeyCode.push_back(69);//+
    
    nudgeMinusKeyCode.push_back(78);//-
    
    onlineModeKeyCode.push_back(55);//command + j OR option + spacebar
    onlineModeKeyCode.push_back(38);
    
    #if !RAVEN_DEBUG
    //Invisible Mouse Hiding Component (hides mouse and intercepts clicks)
    //MacHideBackGroundMouse enables this to work
    mouseHider = new MouseHideComponent(getWidth(), getHeight());
    addAndMakeVisible(mouseHider);
    #endif
    
}

void RavenFunctionsComponent::resizeFunctionsMouseHider()
{
    mouseHider->setTopLeftPosition(0, 0);
    mouseHider->setSize(getWidth(), getHeight());
    mouseHider->setAlwaysOnTop(true);
    mouseHider->toFront(false);
}

void RavenFunctionsComponent::enterHybridMode()
{
    floatingEssentials->setVisible(true);
}

void RavenFunctionsComponent::exitHybridMode()
{
    hybridModeButton->setToggleState(false, false);
}

void RavenFunctionsComponent::addBar()
{
    if(toolbarMode)
    {
        if(numDAWModeBars == MAX_NUM_BARS) return;
        setTopLeftPosition(getX(), getY()-singlePanelHeight);
        numDAWModeBars++;
    }
    else
    {
        if(numMixModeBars == MAX_NUM_BARS) return;
        setTopLeftPosition(getX(), getY()-singlePanelHeight);
        numMixModeBars++;
        sendActionMessage("addBarMixMode");
    }
    
    setSize(getWidth(), getHeight() + singlePanelHeight);
    setOrigHeight(getHeight());

    for(int i = 0; i < getNumChildComponents(); i++)
    {
        int curX = getChildComponent(i)->getX();
        int curY = getChildComponent(i)->getY();
        getChildComponent(i)->setTopLeftPosition(curX, curY + singlePanelHeight);
    }
    
    //sendActionMessage("savecurrent");
    resizeFunctionsMouseHider();
    repaint();
}

void RavenFunctionsComponent::removeBar()
{
    if(toolbarMode)
    {
        if(numDAWModeBars == 0)
        {
            return;   
        }
        else if(numDAWModeBars == 1)
        {
            floatingEssentials->setVisible(true);
            essentialsButton->setToggleState(true, false);
            floatingEssentials->toFront(false);
        }
        setTopLeftPosition(getX(), getY()+singlePanelHeight);
        numDAWModeBars--;
    }
    else
    {
        if(numMixModeBars == 0)
        {
            return;
        }
        else if(numMixModeBars == 1)
        {
            //show the floating essentials palette
            floatingEssentials->setVisible(true);
            essentialsButton->setToggleState(true, false);
            floatingEssentials->toFront(false);
        }
        setTopLeftPosition(getX(), getY()+singlePanelHeight);
        numMixModeBars--;
        sendActionMessage("removeBarMixMode");
    }
    
    int newHeight = (numDAWModeBars == 0) ? 1 : getHeight() - singlePanelHeight;
    setSize(getWidth(), newHeight);
    setOrigHeight(getHeight());
    
    for(int i = 0; i < getNumChildComponents(); i++)
    {
        int curX = getChildComponent(i)->getX();
        int curY = getChildComponent(i)->getY();
        getChildComponent(i)->setTopLeftPosition(curX, curY - singlePanelHeight);
    }
    
    //sendActionMessage("savecurrent");
    resizeFunctionsMouseHider();

    repaint();
}

void RavenFunctionsComponent::enableMoveModules(bool enable)
{
    if(enable)
    {
        moveButton->enabled = true;
        moveButton->setAlpha(1.0);
    }
    else
    {
        moveButton->enabled = false;
        moveButton->setAlpha(0.4);
    }
}

void RavenFunctionsComponent::enterToolbarMode()
{
    if(toolbarMode) return;
    
    toolbarMode = true;
    
    for(int i = 0; i < getNumChildComponents(); i++)
    {
        int curX = getChildComponent(i)->getX();
        int curY = getChildComponent(i)->getY();
        if (curY < singlePanelHeight)   getChildComponent(i)->setVisible(true);
        if (curY < singlePanelHeight*numMixModeBars)   getChildComponent(i)->setVisible(true);
        getChildComponent(i)->setTopLeftPosition(curX, curY + (numDAWModeBars-numMixModeBars)*singlePanelHeight);
    }
    
    getProperties().set("preToolbarY", getY());
    setSize(getWidth(), numDAWModeBars*singlePanelHeight);
    setOrigHeight(getHeight());
    setTopLeftPosition(0, SCREEN_HEIGHT-getHeight());

    resizeFunctionsMouseHider();
    repaint();
}

void RavenFunctionsComponent::exitToolbarMode()
{
    if(!toolbarMode) return;
    
    for(int i = 0; i < getNumChildComponents(); i++)
    {
        int curX = getChildComponent(i)->getX();
        int curY = getChildComponent(i)->getY();
        if (curY < singlePanelHeight)
        if (curY < (numDAWModeBars-numMixModeBars)*singlePanelHeight) getChildComponent(i)->setVisible(false); ////??????
        getChildComponent(i)->setTopLeftPosition(curX, curY - (numDAWModeBars-numMixModeBars)*singlePanelHeight);
    }
    
    toolbarMode = false;
    
    //mouseHider->setTopLeftPosition(0, 0);
    mouseHider->setSize(getWidth(), numMixModeBars*singlePanelHeight);
    mouseHider->setAlwaysOnTop(true);
    mouseHider->toFront(false);
    
    int preY = getProperties().getWithDefault("preToolbarY", 0);
    setTopLeftPosition(0, preY);
    setSize(getWidth(), numMixModeBars*singlePanelHeight);
    setOrigHeight(getHeight());

    resizeFunctionsMouseHider();
    
//    for(int i = 0; i < getNumChildComponents(); i++)
//    {
//        int curX = getChildComponent(i)->getX();
//        int curY = getChildComponent(i)->getY();
//        //if (curY < singlePanelHeight)
//        //if (curY < (numDAWModeBars-numMixModeBars)*singlePanelHeight) getChildComponent(i)->setVisible(false); ////??????
//        if (curY < screenHeight-getHeight()) getChildComponent(i)->setVisible(false);
//        getChildComponent(i)->setTopLeftPosition(curX, curY);
//    }
    

    repaint();
}

void RavenFunctionsComponent::enterCustomMode()
{
    if(customMode) return;
    
    if(!toolbarMode) //Mixer Mode
    {
        setSize(getWidth(), MAX_NUM_BARS*singlePanelHeight);
        setOrigHeight(getHeight());
        int addY = (MAX_NUM_BARS-getNumMixModeBars())*singlePanelHeight;
    
        for(int i = 0; i < getNumChildComponents(); i++)
        {
            int curX = getChildComponent(i)->getX();
            int curY = getChildComponent(i)->getY();
            if (curY < singlePanelHeight)   getChildComponent(i)->setVisible(true);
            getChildComponent(i)->setTopLeftPosition(curX, curY + addY);
            getChildComponent(i)->setVisible(true);
        }
        
    }
    else //DAW Mode
    {
        setSize(getWidth(), MAX_NUM_BARS*singlePanelHeight);
        setOrigHeight(getHeight());
        int addY = (MAX_NUM_BARS-getNumDAWModeBars())*singlePanelHeight;
        
        for(int i = 0; i < getNumChildComponents(); i++)
        {
            int curX = getChildComponent(i)->getX();
            int curY = getChildComponent(i)->getY();
            getChildComponent(i)->setTopLeftPosition(curX, curY + addY);
        }
    }
    
    customMode = true;
    
    resizeFunctionsMouseHider();
    repaint();
}

void RavenFunctionsComponent::exitCustomMode()
{
    if(!customMode) return;
    
    int subY;
    if(!toolbarMode) // Mixer Mode
    {
        setSize(getWidth(), numMixModeBars*singlePanelHeight);
        setOrigHeight(getHeight());
        subY = (MAX_NUM_BARS-getNumMixModeBars())*singlePanelHeight;
    }
    else  //DAW Mode
    {
        setSize(getWidth(), numDAWModeBars*singlePanelHeight);
        setOrigHeight(getHeight());
        subY = (MAX_NUM_BARS-getNumDAWModeBars())*singlePanelHeight;
    }
    
    for(int i = 0; i < getNumChildComponents(); i++)
    {
        int curX = getChildComponent(i)->getX();
        int curY = getChildComponent(i)->getY();
        getChildComponent(i)->setTopLeftPosition(curX, curY - subY);
    }
    
    customMode= false;
    
    resizeFunctionsMouseHider();
    repaint();
}

bool RavenFunctionsComponent::checkModuleTouch(TUIO::TuioCursor *tcur, int touchid)
{
    
    if(componentTouched(toolBarCustomButton, tcur, true) || componentTouched(floatingEssentials->getEssentialsCustomButton(), tcur, true))
    {
        toolBarCustomButton->triggerClick();
        return false;
    }
    else if(componentTouched(addBarButton, tcur, false))
    {
        return false;
    }
    else if(componentTouched(removeBarButton, tcur, false))
    {
        return false;
    }
    else if(componentTouched(exitButton, tcur, false))
    {
        return false;
    }
    else if(componentTouched(floatingEssentials, tcur, false))
    {
        for(int i = 0; i < floatingEssentials->getNumChildComponents(); i++)
        {
            Component *cmp = floatingEssentials->getChildComponent(i);
            if(cmp->getName() == "essentialsAddBarButton" || cmp->getName() == "essentialsRemoveBarButton") continue;
            int yTrim = cmp->getHeight() - (int)cmp->getProperties().getWithDefault("CustomHeight", cmp->getHeight());
            if(componentTouched(cmp, tcur, false, 0, 0, 0, yTrim))
            {
                //set touchid....
                Component* curMovingModule = floatingEssentials->getChildComponent(i);
                curMovingModule->getProperties().set("lastX", curMovingModule->getX());
                curMovingModule->getProperties().set("lastY", curMovingModule->getY());
                curMovingModule->getProperties().set("initTouchX", tcur->getScreenX(SCREEN_WIDTH) - curMovingModule->getScreenX());
                curMovingModule->getProperties().set("initTouchY", tcur->getScreenY(SCREEN_HEIGHT) - curMovingModule->getScreenY());
                curMovingModule->getProperties().set("lastLocation", "essentialsPalette");
                curMovingModule->getProperties().set("touchid", touchid);
                curMovingModule->getProperties().set("moveModuleCount", 0);
                movingModules.add(curMovingModule);
                
                return true;
            }
        }
        
    }
    
    for(int i = 0; i < getNumChildComponents(); i++)
    {
        Component *cmp = getChildComponent(i);
        int yTrim = cmp->getHeight() - (int)cmp->getProperties().getWithDefault("CustomHeight", cmp->getHeight());
        if(componentTouched(cmp, tcur, false, 0, 0, 0, yTrim))
        {
            //set touchid....
            Component* curMovingModule = getChildComponent(i);
            curMovingModule->getProperties().set("lastX", curMovingModule->getX());
            curMovingModule->getProperties().set("lastY", curMovingModule->getY());            
            curMovingModule->getProperties().set("initTouchX", tcur->getScreenX(SCREEN_WIDTH) - curMovingModule->getScreenX());
            curMovingModule->getProperties().set("initTouchY", tcur->getScreenY(SCREEN_HEIGHT) - curMovingModule->getScreenY());
            curMovingModule->getProperties().set("lastLocation", "toolbar");
            curMovingModule->getProperties().set("touchid", touchid);
            movingModules.add(curMovingModule);
            
            return true;
        }
    }
    
    return false;
}

void RavenFunctionsComponent::moveModule(int screenX, int screenY, int touchid)
{
    //LOCK_JUCE_THREAD
    {
        int tempMoveModuleCount = 0;
        Component* curMovingModule;
        for(int i = 0; i < movingModules.size(); i++)
        {
            int curTouchid = movingModules[i]->getProperties().getWithDefault("touchid", 0);
            if(touchid == curTouchid)
            {
                curMovingModule = movingModules[i];
                break;
            }
            else
            {
                curMovingModule = movingModules[0];
            }
        }
    
        tempMoveModuleCount = curMovingModule->getProperties().getWithDefault("moveModuleCount", 0);
        //printf("tempMoveModuleCount %d\n", tempMoveModuleCount);
        //static int moveModuleCount = 0;
        if(tempMoveModuleCount % (FLOATING_DOWNSAMPLE_AMT*movingModules.size()) == 0)
        {
            Point<int> screenPos;
            screenPos.x = screenX;
            screenPos.y = screenY;
            
            int yAdj = 0;
            
            Point<int> localPos = getLocalPoint(0, screenPos); //NULL for source component assumes screen coordinate            
            
            int topLeftX = localPos.x - curMovingModule->getWidth()/2;
            int topLeftY = localPos.y - curMovingModule->getHeight()/2 - yAdj;
            
            int initTouchX = curMovingModule->getProperties().getWithDefault("initTouchX", curMovingModule->getWidth()/2);
            int initTouchY = curMovingModule->getProperties().getWithDefault("initTouchY", curMovingModule->getHeight()/2);

            int localY = localPos.y - initTouchY - yAdj;

            bool touchingEssentialsPalette = false;
            if(floatingEssentials->isVisible())
            {
                if(screenX >= floatingEssentials->getScreenX() && screenX <= floatingEssentials->getScreenX() + floatingEssentials->getWidth())
                {
                    if(screenY >= floatingEssentials->getScreenY() && screenY <= floatingEssentials->getScreenY() + floatingEssentials->getHeight())
                    {
                        touchingEssentialsPalette = true;
                    }
                }
            }
            
#if RAVEN_ESSENTIALS_PALETTE
            if(localY < 0 && !touchingEssentialsPalette)
            {
                LOCK_JUCE_THREAD curMovingModule->addToDesktop(ComponentPeer::windowIsTemporary);
                
                Desktop::getInstance().ravenSetWindowOptions(curMovingModule);
//                curMovingModule->setAlwaysOnTop(true);
//                curMovingModule->setVisible(true);
                
                topLeftX = screenX - initTouchX;
                topLeftY = screenY - initTouchY;
            }
            else if(localY >= 0 && !touchingEssentialsPalette)
            {
                
                LOCK_JUCE_THREAD
                {
                    addChildComponent(curMovingModule);
                    curMovingModule->getProperties().set("isInFloatingEssentials", false);
                }
                
                topLeftX = localPos.x - initTouchX;
                topLeftY = localPos.y - initTouchY - yAdj;
            }
            else if(touchingEssentialsPalette)
            {
                Point<int> essCheck;
                essCheck.x = screenX;
                essCheck.y = screenY;

                LOCK_JUCE_THREAD
                {
                    floatingEssentials->addAndMakeVisible(curMovingModule);
                    curMovingModule->getProperties().set("isInFloatingEssentials", true);
                    //floatingEssentials->dynamicallyResizePalette(curMovingModule);
                }
                    
                Point<int> flEssentialsPos = floatingEssentials->getLocalPoint(0, screenPos);
                topLeftX = flEssentialsPos.x - initTouchX;
                topLeftY = flEssentialsPos.y - initTouchY - yAdj;
            }
#else 
           if(topLeftY < 0) topLeftY = 0;    //NOTE: this is only used without floating Essent active. It keeps all modules on the toolbar.
#endif
            
            //prevent move module button from leaving first rack or floatingEssentials button from leaving toolbar.
            if(curMovingModule == moveButton && topLeftY < getHeight()-singlePanelHeight) topLeftY = getHeight()-singlePanelHeight;
            if(curMovingModule == essentialsButton && topLeftY < 10) topLeftY = 10;
            
            LOCK_JUCE_THREAD curMovingModule->setTopLeftPosition(topLeftX, topLeftY);
            
            //RavenComponentMoveMessage *m = new RavenComponentMoveMessage(curMovingModule, topLeftX, topLeftY);
            //postMessage(m);
        }
        
        
        tempMoveModuleCount++;
        if(tempMoveModuleCount >= 50000) tempMoveModuleCount = 0;
        curMovingModule->getProperties().set("moveModuleCount", tempMoveModuleCount);//josh
    }
}


void RavenFunctionsComponent::endModuleMove(int touchid)
{
    LOCK_JUCE_THREAD
    {
        float componentToPanelThreshold = 1.3;
        
        Component* curMovingModule;
        int curModuleIndex;
        for(int i = 0; i < movingModules.size(); i++)
        {
            int curTouchid = movingModules[i]->getProperties().getWithDefault("touchid", 0);
            if(touchid == curTouchid)
            {
                curMovingModule = movingModules[i];
                curModuleIndex = i;
                break;
            }
            else
            {
                curMovingModule = movingModules[0];
                curModuleIndex = 0;
            }
        }
        
        if(curMovingModule->isOnDesktop())
        {
            int lastX = curMovingModule->getProperties().getWithDefault("lastX", 0);
            int lastY = curMovingModule->getProperties().getWithDefault("lastY", 0);
            String lastLoc = curMovingModule->getProperties().getWithDefault("lastLocation", "");
            curMovingModule->setTopLeftPosition(lastX, lastY);
            if(lastLoc == "toolbar")
            {
                addAndMakeVisible(curMovingModule);
                curMovingModule->getProperties().set("isInFloatingEssentials", false);
            }
            else
            {
                floatingEssentials->addAndMakeVisible(curMovingModule);
                curMovingModule->getProperties().set("isInFloatingEssentials", true);
            }
            
            movingModules.remove(curModuleIndex);
            return;
        }
        
        if(floatingEssentials->containsModule(curMovingModule))
        {
            //std::cout << "MovingModuleScreenBottom: " << curMovingModule->getScreenBounds().getBottom() << std::endl;
            bool positionedModuleOnPalette = floatingEssentials->positionComponent(curMovingModule);
            if(!positionedModuleOnPalette)
            {
                addAndMakeVisible(curMovingModule);
                curMovingModule->getProperties().set("isInFloatingEssentials", false);
            }
            movingModules.remove(curModuleIndex);
            return;
        }
        
        String lastLoc = curMovingModule->getProperties().getWithDefault("lastLocation", "");
        if(lastLoc == "essentialsPalette") floatingEssentials->resizeFloatingPalette();
        std::cout << "Last Location: " << lastLoc << std::endl;
        
        int yPos = curMovingModule->getY() + curMovingModule->getHeight()/2;
        int xPos = curMovingModule->getX();
        int regionTop;
        int regionBottom;
        int customHeight = (int)curMovingModule->getProperties().getWithDefault("CustomHeight", curMovingModule->getHeight());
        int ySetting = 0;
        
        //**********************************************************
        //******** Hack for Shitty Looking Transport Comp **********
        if(curMovingModule->getName() == "TransportComponent")
        {
            bottomOffset = 23;
        }
        //**********************************************************
        //**********************************************************
        
        
        //Y snapping
        for(int i = 0; i < MAX_NUM_BARS; i++)
        {
            int y1 = i*singlePanelHeight;
            int y2 = y1 + singlePanelHeight/2;
            int y3 = y1 + singlePanelHeight;
            
            if(yPos >= y1 && yPos < y2)
            {
                
                if(customHeight <= singlePanelHeight/2)
                {
                    ySetting = y1 + (singlePanelHeight/2 - topOffset)/2 - customHeight/2 + topOffset;
                }else
                {
                    ySetting = y1 + singlePanelHeight/2 - customHeight/2;
                }
                
                
                curMovingModule->setTopLeftPosition(xPos, ySetting); //was y1+topOffset
                regionTop = y1;
                regionBottom = y3;
            }
            else if(yPos >= y2 && yPos <= y3)
            {
               // float compToPanel = (float)curMovingModule->getHeight()/((float)singlePanelHeight/2);
                //int customHeight = (int)curMovingModule->getProperties().getWithDefault("CustomHeight", curMovingModule->getHeight());
                
                //if(compToPanel < componentToPanelThreshold)
                if(customHeight <= singlePanelHeight/2)
                {
                    ySetting = y3 - ((singlePanelHeight/2 - bottomOffset)/2 + bottomOffset + customHeight/2);
                    curMovingModule->setTopLeftPosition(xPos, ySetting);  //was: y3 - customHeight - bottomOffset
                }
                else
                {
                    ySetting = y1 + singlePanelHeight/2 - customHeight/2;
                    curMovingModule->setTopLeftPosition(xPos, ySetting);  //was: y1 + topOffset
                }
                regionTop = y1;
                regionBottom = y3;
            }
            else if(yPos > y3){
                int orgX = curMovingModule->getProperties().getWithDefault("lastX", 0);
                int orgY = curMovingModule->getProperties().getWithDefault("lastY", 0);
                curMovingModule->setTopLeftPosition(orgX, orgY);

            }
        }
        
        // X snapping
        const int snapX = 30;
        int plusX = curMovingModule->getX() % snapX;
        if(plusX < snapX/2) plusX *= -1;
        else plusX = snapX - plusX;
        //curMovingModule->setTopLeftPosition(curMovingModule->getX() + plusX, curMovingModule->getY());
        
        //Ensure no overlapping modules
        preventModuleOverlap(curMovingModule, regionTop, regionBottom);
        
        //Remove the current moving module from movingModules array
        movingModules.remove(curModuleIndex);
    }
}

void RavenFunctionsComponent::preventModuleOverlap(Component* curMovingModule, int topY, int bottomY)
{
    //To do:
    //3. TESTING, TESTING, TESTING!!!!
    
    Array<Component*> regionChildTouchComponents;
    
    //1. 
    const int maxComponents = (childTouchComponents.size() > ravenTouchButtons.size()) ?
    ((childTouchComponents.size() > ravenNSCTouchButtons.size()) ?  childTouchComponents.size() : ravenNSCTouchButtons.size()) :
    ((ravenTouchButtons.size() > ravenNSCTouchButtons.size()) ?  ravenTouchButtons.size() : ravenNSCTouchButtons.size());
        
    for(int i = 0; i < maxComponents; i++)
    {
        if(i < childTouchComponents.size()){
            int childY = childTouchComponents[i]->getY() + childTouchComponents[i]->getHeight()/2;
            
            if(childY >= topY && childY < bottomY && childTouchComponents[i]->getName() != curMovingModule->getName())
                regionChildTouchComponents.add(childTouchComponents[i]);
        }
        if(i < ravenTouchButtons.size()){
            int childY = ravenTouchButtons[i]->getY() + ravenTouchButtons[i]->getHeight()/2;
            
            if(childY >= topY && childY < bottomY && ravenTouchButtons[i]->getName() != curMovingModule->getName())
                regionChildTouchComponents.add(ravenTouchButtons[i]);
        }
        if(i < ravenNSCTouchButtons.size()){
            int childY = ravenNSCTouchButtons[i]->getY() + ravenNSCTouchButtons[i]->getHeight()/2;
            
            if(childY >= topY && childY < bottomY && ravenNSCTouchButtons[i]->getName() != curMovingModule->getName())
                regionChildTouchComponents.add(ravenNSCTouchButtons[i]);
        }
    }
    
    //2. look for overlap. Adjust the coordinates of the component to fix it.
    int trial = 0;
    bool foundOverlap = true;
    while(foundOverlap && trial < 10)
    {
        foundOverlap = false;
        
        for(int i = 0; i < regionChildTouchComponents.size(); i++)
        {
            Rectangle<int> curCompRect;
            curCompRect = regionChildTouchComponents[i]->getScreenBounds();
            curCompRect.setHeight(regionChildTouchComponents[i]->getProperties().getWithDefault("CustomHeight", regionChildTouchComponents[i]->getHeight()));
            
            Rectangle<int> curMovingModuleRect(curMovingModule->getScreenBounds());
            curMovingModuleRect.setHeight(curMovingModule->getProperties().getWithDefault("CustomHeight", curMovingModule->getHeight()));
            
            if(curMovingModuleRect.intersects(curCompRect))
            {
                Rectangle<int> intersectionArea = curMovingModuleRect.getIntersection(curCompRect);
                            
                int newX = (curMovingModule->getScreenBounds().getCentreX() <= curCompRect.getCentreX()) ? curMovingModule->getX() - intersectionArea.getWidth() : curMovingModule->getX() + intersectionArea.getWidth();
                
                curMovingModule->setTopLeftPosition(newX, curMovingModule->getY());
                                
                foundOverlap = true;
            }
        }
        trial++;
    }
    
    if((foundOverlap && trial > 8) || curMovingModule->getX() < 50 || curMovingModule->getRight() > 1870)
    {
        int orgX = curMovingModule->getProperties().getWithDefault("lastX", 0);
        int orgY = curMovingModule->getProperties().getWithDefault("lastY", 0);
        String lastLoc = curMovingModule->getProperties().getWithDefault("lastLocation", "");
        if(lastLoc == "essentialsPalette")
        {
            floatingEssentials->addAndMakeVisible(curMovingModule);
            curMovingModule->getProperties().set("isInFloatingEssentials", true);
        }
        curMovingModule->setTopLeftPosition(orgX, orgY);
    }
    else
    {
        curMovingModule->setTopLeftPosition(curMovingModule->getX(), curMovingModule->getY());
    }
    
}


void RavenFunctionsComponent::paint (Graphics& g)
{
    if(toolbarMode && !customMode)
    {        
        for(int i = 0; i < getNumBars()-1; i++) g.drawImageAt(singlePanelImg, 0, i*singlePanelHeight);
        g.drawImageAt(bottomPanelImg, 0, (getNumBars()-1)*singlePanelHeight);
    }
    else if(toolbarMode && customMode)
    {
        //g.drawImageAt(customPanelImg, 0, 0);
        //for(int i = 0; i < getNumDAWModeBars()-1; i++) g.drawImageAt(singlePanelImg, 0, customPanelHeight+i*singlePanelHeight);
        for(int i = 0; i < MAX_NUM_BARS-1; i++) g.drawImageAt(singlePanelImg, 0, i*singlePanelHeight);
        //g.drawImageAt(bottomPanelImg, 0, customPanelHeight + (getNumDAWModeBars()-1)*singlePanelHeight);
        g.drawImageAt(bottomPanelImg, 0,  (MAX_NUM_BARS-1)*singlePanelHeight);
        
    }
    else if(!toolbarMode && !customMode)
    {
        for(int i = 0; i < getNumMixModeBars()-1; i++) g.drawImageAt(singlePanelImg, 0, i*singlePanelHeight);
        g.drawImageAt(bottomPanelImg, 0, (getNumMixModeBars()-1)*singlePanelHeight);
    }

    /*
    else if(customMode && !toolbarMode)
    {
        g.fillAll(Colours::maroon);
        g.drawImageAt(customPanelImg, 0, 0);
        for(int i = 0; i < getNumDAWModeBars()-1; i++)
        {
            if(i < getNumDAWModeBars() - getNumMixModeBars())
            {
                g.setOpacity(0.4); //grey out
            }
            else
            {
                g.setOpacity(1.0);
            }
                g.drawImageAt(singlePanelImg, 0, customPanelHeight+i*singlePanelHeight); //normal
        }
        g.setOpacity(1.0);
        g.drawImageAt(bottomPanelImg, 0, customPanelHeight + (getNumDAWModeBars()-1)*singlePanelHeight);
    }
    //*/
    else if(!toolbarMode && customMode)
    {
        //g.drawImageAt(customPanelImg, 0, 0);
        //for(int i = 0; i < getNumMixModeBars()-1; i++)
        for(int i = 0; i < MAX_NUM_BARS-1; i++)
        {
            g.drawImageAt(singlePanelImg, 0, /*customPanelHeight+*/i*singlePanelHeight);
        }
        //g.drawImageAt(bottomPanelImg, 0, customPanelHeight + (getNumMixModeBars()-1)*singlePanelHeight);
        g.drawImageAt(bottomPanelImg, 0, (MAX_NUM_BARS-1)*singlePanelHeight);
    }
}

void RavenFunctionsComponent::buttonClicked (Button* b)
{
//    printf("button clicked \n");
    if(b == toolBarModeButton)
    {
        //printf("got something\n");
        
        bool isDAW = toolBarModeButton->getProperties().getWithDefault("isDAW", false);
        if(isDAW)
        {
            toolBarModeButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/RavenDAW/bt-RavenDaw-DawOn.png", "TOOLBAR/FUNCTIONMODULE/RavenDAW/bt-RavenDaw-DawOff.png");
        }
        else
        {
            toolBarModeButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/RavenDAW/bt-RavenDaw-RavenOff.png", "TOOLBAR/FUNCTIONMODULE/RavenDAW/bt-RavenDaw-RavenOn.png");
        }
        toolBarModeButton->getProperties().set("isDAW", !isDAW);
    }
    else if(b == lastWinButton)
    {
        insWinButton->triggerClick();
        return;
    }
    else if((b == addBarButton || b->getName() == "essentialsAddBarButton") && !RAVEN_STATE->isHybridMode())
    {
        addBar();
        return;
    }
    else if((b == removeBarButton || b->getName() == "essentialsRemoveBarButton") && !RAVEN_STATE->isHybridMode())
    {
        removeBar();
        return;
    }
//    else if(b == meterModeButton)
//    {
//        if(meterModeButton->getToggleState() == true)
//        {
//            meterModeButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/LargeMeterToggle/Switch-Sends2Vu-Off.png", "TOOLBAR/FUNCTIONMODULE/LargeMeterToggle/Switch-Sends2Vu-On.png");
//            sendActionMessage("enter meter mode");
//        }
//        else
//        {
//            meterModeButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/LargeMeterToggle/Switch-Vu2Sends-On.png", "TOOLBAR/FUNCTIONMODULE/LargeMeterToggle/Switch-Vu2Sends-Off.png");
//            sendActionMessage("exit meter mode");
//        }
//    }
    else if(b == hybridModeButton)
    {
        if(b->getToggleState()){ RAVEN_STATE->setHybridMode(true);}
        else{ RAVEN_STATE->setHybridMode(false);}
    }
    else if(b == zoomInButton)
    {
        MacKeyPress::pressCombo(zoomInKeyCode);
    }
    else if(b == zoomOutButton)
    {
        MacKeyPress::pressCombo(zoomOutKeyCode);
    }
    //trimKeyCode, selKeyCode, grabKeyCode, zoomKeyCode, scrubKeyCode, pencilKeyCode
    else if(b->getName() == "nudgePlusButton")
    {
        MacKeyPress::pressCombo(nudgePlusKeyCode);
    }
    else if(b->getName() == "nudgeMinusButton")
    {
        MacKeyPress::pressCombo(nudgeMinusKeyCode);
    }
    else if(b == clipLoopButton)
    {
        MacKeyPress::pressCombo(clipLoopKeyCode);
    }
    else if(b == duplicateButton)
    {
        MacKeyPress::pressCombo(duplicateKeyCode);
    }
    else if(b->getName() == "redoButton")
    {
        MacKeyPress::pressCombo(*((RavenButton*)b)->getKeyCode());
    }
    /*
    if(b == fakePanMode)
    {
        printf("FAKE PAN\n");
        link->set(eNSCGlobalControl, eNSCGSPTBankMode_Pan, 0, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSPTBankMode_Pan, 0, 0, 0);
        
        link->set(eNSCGlobalControl, eNSCGSPTBankMode_Pan, 1, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSPTBankMode_Pan, 1, 0, 0);
        
        link->set(eNSCGlobalControl, eNSCGSPTBankMode_Pan, 2, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSPTBankMode_Pan, 2, 0, 0);
//
//        if(!RAVEN_24)
//        {
//            link->set(eNSCGlobalControl, eNSCGSPTBankMode_Pan, 3, 0, 1);
//            link->set(eNSCGlobalControl, eNSCGSPTBankMode_Pan, 3, 0, 0);
//        }
        
        bool isLeftPanMode = fakePanMode->getProperties().getWithDefault("isLeftFakePanMode", false);
        printf("is left fake pan mode: %d\n", isLeftPanMode);
        
        if(isLeftPanMode == true)
        {
            fakePanMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Lon.png", "TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Loff.png");
            fakePanMode->getProperties().set("isLeftFakePanMode", false);
        }
        else
        {
            fakePanMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Ron.png", "TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Roff.png");
            fakePanMode->getProperties().set("isLeftFakePanMode", true);
        }
    }
     */

    NscGuiElement * element = dynamic_cast<NscGuiElement *>(b);
	if (!element)
	{
		// try our temporary workaround hack.
		int ptr = b->getProperties()[kNscGuiIdentifier];
		element = (NscGuiElement *) ptr;
		if (!element)
		{
            return;
			DBG("Switches must inherit and set up NscGuiElement");
			jassertfalse;
		}
	}
	if (element)
	{
        switchClicked(element, true);
	}
    
    if(b == flipMode)
    {
        bool isFlipMode = flipMode->getProperties().getWithDefault("isFlipMode", false);
        if(isFlipMode)
        {
            flipMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/SendChannel/channelOn.png", "TOOLBAR/FUNCTIONMODULE/SendChannel/channelOff.png");
            flipMode->getProperties().set("isFlipMode", false);
        }
        else
        {
            flipMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/SendChannel/sendOn.png", "TOOLBAR/FUNCTIONMODULE/SendChannel/sendOff.png");
            flipMode->getProperties().set("isFlipMode", true);
        }
        
        flipMode2->triggerClick();
        flipMode3->triggerClick();
        if(!RAVEN_24) flipMode4->triggerClick();
    }
    
    if(b == panMode)
    {
        printf("firing pan mode BUTTON\n");
        panMode2->triggerClick();
        panMode3->triggerClick();
        if(!RAVEN_24) panMode4->triggerClick();
        
        if(!inFlipMode)
        {
            bool isLeftPanMode = panMode->getProperties().getWithDefault("isLeftPanMode", false);
            if(isLeftPanMode == true)
            {
                panMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Ron.png", "TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Roff.png");
                panMode->getProperties().set("isLeftPanMode", false);
                sendActionMessage("right pan mode");
            }
            else
            {
                panMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Lon.png", "TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Loff.png");
                panMode->getProperties().set("isLeftPanMode", true);
                sendActionMessage("left pan mode");
            }
        }
    }
}

void RavenFunctionsComponent::buttonStateChanged (Button* b)
{
        

}

bool RavenFunctionsComponent::checkTrackButtonsTouch(TUIO::TuioCursor* tcur)
{
    printf("doing this\n");
    if(!isVisible()|| disableTouch)return false;
    
    int touchX = tcur->getScreenX(SCREEN_WIDTH);
    int touchY = tcur->getScreenY(SCREEN_HEIGHT);
    RavenNSCButton *trackleftNSCButton = trackBankCmp->getTrackLeftButton();
    RavenNSCButton *trackrightNSCButton = trackBankCmp->getTrackRightButton();
    RavenButton *bankLeftButton = trackBankCmp->getBankLeftButton();
    RavenButton *bankRightButton = trackBankCmp->getBankRightButton();
    
    if(componentTouched(trackleftNSCButton, touchX, touchY, true) && trackleftNSCButton->enabled && trackleftNSCButton->isShowing())
    {
        if(RAVEN_STATE->mixer()->canTrackLeft())
        {
            LOCK_JUCE_THREAD trackleftNSCButton->triggerClick();
            return true;
        }
    }
    else if(componentTouched(trackrightNSCButton, touchX, touchY, true) && trackrightNSCButton->enabled && trackrightNSCButton->isShowing())
    {
        if(RAVEN_STATE->mixer()->canTrackRight())
        {
            LOCK_JUCE_THREAD trackrightNSCButton->triggerClick();
            return true;
        }
    }
    else if(componentTouched(bankLeftButton, touchX, touchY, true) && bankLeftButton->enabled && bankLeftButton->isShowing())
    {
        if(trackBankCmp->bankBy32())
        {
            trackBankCmp-> getBankLeftNSCButton()->triggerClick();
        }
        else
        {
            //LOCK_JUCE_THREAD
            {
                int temp = RAVEN_STATE->mixer()->numberCanBankLeft();
                printf("temp: %d\n", temp);
                for(int i = 0;i<temp;i++)
                {
                    if(RAVEN_STATE->mixer()->canTrackLeft())
                    {
                        trackleftNSCButton->triggerClick();
                    }
                }
            }
            return true;
        }
    }
    else if(componentTouched(bankRightButton, touchX, touchY, true) && bankRightButton->enabled && bankRightButton->isShowing())
    {
        if(trackBankCmp->bankBy32())
        {
            trackBankCmp->getBankRightNSCButton()->triggerClick();
        }
        else
        {
            //LOCK_JUCE_THREAD
            {
                int temp = RAVEN_STATE->mixer()->numberCanBankRight();
                printf("temp: %d\n", temp);
                for(int i = 0;i<temp;i++)
                {
                    if(RAVEN_STATE->mixer()->canTrackRight())
                    {
                        trackrightNSCButton->triggerClick();
                    }
                }
            }
            return true;
        }
    }
    return false;
}