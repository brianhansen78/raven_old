
#ifndef Raven_NewSDK_RavenTextDisplay_h
#define Raven_NewSDK_RavenTextDisplay_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "NscGuiElement.h"
#include "RavenLabel.h"

class RavenTextDisplay : public RavenLabel, public NscGuiElement
{
public:
    RavenTextDisplay(NscGuiContainer *owner) : NscGuiElement(owner)
    {
        font.setHeight(14.0f);
        setFont(font);
        setJustificationType(Justification::centredBottom);
        
    }
    ~RavenTextDisplay()
    {
    
    }
        
    virtual void  updateNscValue(float value)			// callback when DAW changes a value; generally
    {
        setText(String(value), false);
    }
	virtual void  updateNscValue(const String & value)	// intended to be overriden
    {
        //prevent plug-in lcd blinking:
        //*/
        if(getTag() == eNSCPlugInLCD)
        {
            if(value.compare("          ")) //if not blank 10 character string
            {
                setText(value, false);
            }
            return;
        }//*/
        
        setText(value, false);
    }
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenTextDisplay)
    
};

class RavenFuturaTextDisplay : public RavenFuturaLabel, public NscGuiElement
{
public:
    RavenFuturaTextDisplay(NscGuiContainer *owner) : NscGuiElement(owner)
    {
        font.setHeight(14.0f);
        setFont(font);
        setJustificationType(Justification::centredBottom);
        
    }
    ~RavenFuturaTextDisplay()
    {
        
    }
    
    virtual void  updateNscValue(float value)			// callback when DAW changes a value; generally
    {
        setText(String(value), false);
    }
	virtual void  updateNscValue(const String & value)	// intended to be overriden
    {
        //prevent plug-in lcd blinking:
        //*/
        if(getTag() == eNSCPlugInLCD)
        {
            if(value.compare("          ")) //if not blank 10 character string
            {
                //printf("LCD TEXT %s\n", (const char*)value.toUTF8());
                setText(value, false);
            }
            return;
        }//*/
        
        setText(value, false);
    }
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenFuturaTextDisplay)
    
};

#endif
