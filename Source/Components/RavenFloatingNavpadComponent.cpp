//
//  RavenFloatingNavpadComponent.cpp
//  Raven
//
//  Created by Joshua Dickinson on 6/28/13.
//
//

#include "RavenFloatingNavpadComponent.h"
#include "RavenRackContainer.h"

void RavenFloatingNavPadComponent::buttonClicked(Button* b)
{
    printf("floating toggle state: %d", b->getToggleState());
    //printf("floating: floating pallet button clicked: %d %s\n", b->getToggleState(), (const char*)b->getName().toUTF8());
    if(b == waveformButton && b->getToggleState() == true)
    {
        navPadImgCmp->setImageIndex(eNavPadWaveform);
        masterNavPad->enterWaveformMode();
    }
    else if(b == trackzoomButton && b->getToggleState() == true)
    {
        if(navPadImgCmp->getImageIndex() == eNavPadTrackZoomAll || navPadImgCmp->getImageIndex() == eNavPadTrackZoom)
        {
            masterNavPad->flipZoomMode();
        }
        masterNavPad->enterTrackZoomMode();
    }
    else if(b == scrubButton && b->getToggleState() == true)
    {
        navPadImgCmp->setImageIndex(eNavPadScrub);
        masterNavPad->enterScrubMode();
    }
    else if(b == shuttleButton && b->getToggleState() == true)
    {
        navPadImgCmp->setImageIndex(eNavPadShuttle);
        masterNavPad->enterShuttleMode();
    }
    else if(b == hideLeftButton)
    {
        getWindow()->removeFromDesktop();
        RAVEN->getFunctionsManager()->getFloatingNavpadButton()->setToggleState(false, false);
    }
    else if(b == hideRightButton)
    {
        getWindow()->removeFromDesktop();
        RAVEN->getFunctionsManager()->getFloatingNavpadButton()->setToggleState(false, false);
    }
    repaint();
}