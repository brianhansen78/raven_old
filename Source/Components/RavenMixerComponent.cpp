//
//  RavenMixerComponent.cpp
//  Raven
//
//  Created by Joshua Dickinson on 6/4/13.
//
//

#include "RavenMixerComponent.h"
#include "RavenRackContainer.h"

void RavenMixerComponent::buttonStateChanged(Button* b)
{}

void RavenMixerComponent::buttonClicked(Button* b)
{
    printf("button clicked\n");
    if(b == addBankButton)
    {
        if(!RAVEN->isHybridMode())
        {
            RAVEN->setHybridMode(true);
        }
        addHybridModeBank();
    }
    else if(b == removeBankButton)
    {
        if(!RAVEN->isHybridMode())
        {
            RAVEN->setHybridMode(true);
        }
        removeHybridModeBank();
    }
    else if(b == controlModeButton)
    {
        if(RAVEN->isHybridPanMode())
        {
            controlModeButton->setImagePaths("FloatingMixer/HeaderButtons/Fm_Pan_On.png", "FloatingMixer/HeaderButtons/Fm_Pan_Off.png");
            RAVEN->setHybridPanMode(false);
        }
        else
        {
            controlModeButton->setImagePaths("FloatingMixer/HeaderButtons/Fm_Auto_On.png", "FloatingMixer/HeaderButtons/Fm_Auto_Off.png");
            RAVEN->setHybridPanMode(true);
        }
    }
    if(b == closeButton)
    {
        RAVEN->setHybridMode(false);
    }
}

void RavenMixerComponent::paintOverChildren(Graphics &g)
{
    if(RAVEN->isHybridMode())
    {
        g.drawImageAt(hybridLeftSideImg, 0, 0);
        g.drawImageAt(hybridRightSideImg, getWidth()-hybridRightSideImg.getWidth(), 0);
        g.drawImageAt(hybridHeaderLabelImg, (getWidth()*0.5)-(hybridHeaderLabelImg.getWidth()*0.5)+61, 16);
        //MADE into image component g.drawImageAt(hybridTopRightImg, getWidth()-hybridTopRightImg.getWidth()-hybridRightSideImg.getWidth(), 0);
        //MADE into image component: g.drawImageAt(hybridBottomImg, hybridLeftSideImg.getWidth(), getHeight()-hybridBottomImg.getHeight());
    }
}

void RavenMixerComponent::resized()
{
    if(!RAVEN->isHybridMode())
    {
        for (int i=0; i<banks.size();i++)
            banks[i]->setSize(banks[i]->getWidth(), getHeight());
    }
    else
    {
        for (int i=0; i<banks.size();i++)
            banks[i]->setSize(banks[i]->getWidth(), RAVEN_TOOLBAR_HEIGHT*2);
    }
}

bool RavenMixerComponent::canTrackRight()
{
    if(!banks[0]->getChannel(0)->isBlank() && banks[1]->getChannel(0)->isBlank())
    {
        return false;
    }
    return true;
}

bool RavenMixerComponent::canTrackLeft()
{
    int lastBank = RAVEN_24 ? 2 : 3;
    if(!banks[lastBank]->getChannel(7)->isBlank()
       && banks[lastBank-1]->getChannel(7)->isBlank())
    {
        return false;
    }
    return true;
}

int RavenMixerComponent::numberCanBankRight()
{
    int check1 = 0;
    int check2 = 0;
    for(int i = 0;i<8;i++)
    {
        if(!banks[0]->getChannel(i)->isBlank())
            break;
        check1++;
    }
    for(int i = 0;i<8;i++)//start on the second bank and count how many non-blanks we have to the right. 
    {
        if(banks[1]->getChannel(i)->isBlank())
            break;
        check2++;
    }
    return (check1 > check2 ? check1 : check2);
}

int RavenMixerComponent::numberCanBankLeft()
{
    printf("computing number can bank left\n");
    int check1 = 0;
    int check2 = 0;
    int lastBank = RAVEN_24 ? 2 : 3;
    if(RAVEN->isHybridMode())
        lastBank = numberOfHybridModeBanks-1;
    for(int i = 7;i>=0;i--)
    {
        if(!banks[lastBank]->getChannel(i)->isBlank())
            break;
        check1++;
    }
    if(lastBank > 0)
    {
        for(int i = 7;i>=0;i--)
        {
            if(banks[lastBank-1]->getChannel(i)->isBlank())
                break;
            check2++;
        }
    }
    else
    {
        //if we only have 1 bank, then just check if the first track is blank and if so, then you can't bank over anymore
        if(banks[0]->getChannel(0)->isBlank())
            return 0;
        else
            return 8;//might want to change this to 7 to prevent the possibility of ever not seeing at least one track. 
    }
    return (check1 > check2 ? check1 : check2);
}

int RavenMixerComponent::numberCanBankRightThreeFinger()
{
    int check1 = 0;
    int check2 = 0;
    bool loop = true;
    for(int b = 0;b<numberOfHybridModeBanks && loop;b++)
    {
        for(int i = 0;i<8 && loop;i++)
        {
            if(!banks[b]->getChannel(i)->isBlank())
            {
                loop = false;
                break;
            }
            check1++;
        }
    }

    loop = true;
    int lastBank = RAVEN_24 ? 2 : 3;
    for(int b = 1;b < lastBank && loop;b++)
    {
        for(int i = 0;i<8;i++)
        {
            if(banks[b]->getChannel(i)->isBlank())
            {
                loop = false;
                break;
            }
            check2++;
        }
    }
    
    //
    if(check2 == (numberOfHybridModeBanks-1)*chansPerBank)
        check2 = numberOfHybridModeBanks*chansPerBank;
    
    printf("bank right check 1: %d, check2: %d\n", check1, check2);

    if(check1 > numberOfHybridModeBanks*chansPerBank || check2 > numberOfHybridModeBanks*chansPerBank)
        return numberOfHybridModeBanks*chansPerBank;
    return (check1 > check2 ? check1 : check2);
}

int RavenMixerComponent::numberCanBankLeftThreeFinger()
{
    printf("computing number can bank left by swipe\n");
    int check1 = 0;
    int check2 = 0;
    int lastBank = RAVEN_24 ? 2 : 3;
    lastBank = numberOfHybridModeBanks-1;//last bank we can see...
    
    //bank left is trying to move the tracks to the right. Check1 checks from the very end of the last bank to see how many blanks we have.
    bool loop = true;
    for(int b = lastBank;b >= 0 && loop;b--)
    {
        for(int i = 7;i>=0;i--)
        {
            if(!banks[b]->getChannel(i)->isBlank())
            {
                loop = false;
                break;
            }
            check1++;//number of blank tracks starting from right. In hybrid mode this is the number blank starting from the right of the last bank you can see. 
        }
    }
    
    loop = true;
    for(int b = lastBank;b >= 0 && loop; b--)
    {
        for(int i = 7;i>=0;i--)
        {
            if(banks[lastBank]->getChannel(i)->isBlank())
            {
                loop = false;
                break;
            }
            check2++;//number of non-blank tracks starting from right.  
        }
    }
    
    //if every single track to the left of the last visible bank is non-blank, then we can only assume that you can bank by the full amount... 
    if(check2 != numberOfHybridModeBanks*chansPerBank)
        check2 -= 8;//otherwise, always keep at least 8 tracks within view
    
    printf("bank left check 1: %d, check2: %d\n", check1, check2);
    if(check1 > numberOfHybridModeBanks*chansPerBank || check2 > numberOfHybridModeBanks*chansPerBank)
        return numberOfHybridModeBanks*chansPerBank;
    return (check1 > check2 ? check1 : check2);
}
