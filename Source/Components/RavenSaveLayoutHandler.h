
#ifndef Raven_XMLHandler_h
#define Raven_XMLHandler_h

#include "RavenFunctionsManager.h"
#include "RavenWindowManager.h"

#define XML_PATH_GLOBAL File::getSpecialLocation(File::currentApplicationFile).getFullPathName() + "/Contents/Resources/Raven_Layouts/Global"
#define XML_PATH_DEFAULT File::getSpecialLocation(File::currentApplicationFile).getFullPathName() + "/Contents/Resources/Raven_Layouts/Default"

#define XML_PATH_TEMPLATE_MTX "/Users/brianhansen/Documents/Brian/Work/1_Raven/raven/Raven_Default_Layouts/MTX/Templates"
#define XML_PATH_NEWDEFAULTS_MTX "/Users/brianhansen/Documents/Brian/Work/1_Raven/raven/Raven_Default_Layouts/MTX/NewDefaults"
#define XML_PATH_TEMPLATE_MTI "/Users/brianhansen/Documents/Brian/Work/1_Raven/raven/Raven_Default_Layouts/MTI/Templates"
#define XML_PATH_NEWDEFAULTS_MTI "/Users/brianhansen/Documents/Brian/Work/1_Raven/raven/Raven_Default_Layouts/MTI/NewDefaults"


#define DEFAULT_PRESET_INDEX 10
#define REVERT_LAYOUT_INDEX 20
#define MIX_DAW_INDEX 30
#define PRIOR_CUSTOM_VIEW_INDEX 40
#define PRIOR_SAVE_VIEW_INDEX 50

class RavenSaveLayoutHandler
{
public:
    RavenSaveLayoutHandler(RavenFunctionsManager* _functionsManager, RavenWindowManager* _windowManager)
    : sessionPath(""), functionsManager(_functionsManager), windowManager(_windowManager), currentMixIndex(DEFAULT_PRESET_INDEX), currentDAWIndex(DEFAULT_PRESET_INDEX)
    {}
    ~RavenSaveLayoutHandler(){}
    
    void generateDefaultLayoutsFromTemplates();
    
    String getSaveLayoutXMLPath(bool isDAWMode, int preset);
    void setLastView(String version, int mode, int index);
    
    void saveIcons();
    void loadIcons();
    
    bool loadSession();
    
    void saveSession();
   
    bool restoreDefaultLayouts();
    
    void loadUserLayout(bool isDAWMode, int preset);
    void saveUserLayout(bool isDAWMode, int preset);
        
    void setMixIndex(int idx) { currentMixIndex = idx; }
    int mixIndex() {return currentMixIndex;}
    void setDAWIndex(int idx) { currentDAWIndex = idx; }
    int dawIndex() {return currentDAWIndex;}
    int getDefaultPresetIndx() { return DEFAULT_PRESET_INDEX; }
    
    void setFloatingEssentialsForHandler(RavenFloatingEssentialsComponent* fE) { floatingEssentials = fE; };
    
//    int getLastSessionMode() {return lastSessionMode;}
//    int getLastSessionPreset() {return lastSessionPreset;}
    
private:
    
    String sessionPath;
    
    Array<windowInfo> rackWindowLayout;
    Array<windowInfo> floatingWindowLayout;
    Array<savedFunctionsModuleInfo> functionsLayout;
    RavenFunctionsManager* functionsManager;
    RavenFloatingEssentialsComponent* floatingEssentials;
    RavenWindowManager* windowManager;
    int currentMixIndex, currentDAWIndex;
    
};




#endif
