//
//  RavenFunctionsManager.cpp
//  Raven
//
//  Created by Brian Hansen on 5/22/13.
//
//
#include "RavenFunctionsManager.h"
#include "RavenRackContainer.h"

const int topOffset = 10;
int bottomOffset = 10;
const int horizontalSpacing = 10;
const int sideSpacing = 50;

const int animationMS = 450;
const int proxyTransitionMS = 50;

RavenFunctionsManager::RavenFunctionsManager() : toolbarMode(false), customMode(false), inFlipMode(false), isLeftPanModeWhenFlipped(false), isLeftPanMode(false), animateModuleCount(0)
{
    Desktop::getInstance().getAnimator().addChangeListener(this);
    
    moverComponent = new RavenMoverComponent();
    
//    hideButton->setVisible(false);
//    ravenTouchButtons.add(hideButton);
    
    //**********************************************************
    //******************** Functions Panel 1 *******************
        
    trackBankCmp = new RavenTrackBankComponent(this);
    trackBankCmp->setTopLeftPosition(80, 18);
    trackBankCmp->getProperties().set("defaultX", 130);
    trackBankCmp->getProperties().set("defaultY", 140);
    trackBankCmp->getProperties().set("CustomHeight", 110);
    trackBankCmp->setName("TrackBankComponent");
    
    
    utilCmp = new RavenUtilitiesComponent(this);
    utilCmp->setTopLeftPosition(245, 10);
    utilCmp->getProperties().set("defaultX", 290);
    utilCmp->getProperties().set("defaultY", 130);
    utilCmp->getProperties().set("CustomHeight", 55);
    utilCmp->setName("UtilitiesComponent");
    
    modComp = new RavenModifiersComponent(this);
    modComp->setTopLeftPosition(245, 72);
    modComp->getProperties().set("CustomHeight", 55);
    modComp->setName("ModifiersComponent");
    
    igsCmp = new RavenInputGroupSuspendComponent(this);
    igsCmp->setTopLeftPosition(638, 20);
    igsCmp->getProperties().set("defaultX", 530);
    igsCmp->getProperties().set("defaultY", 140);
    igsCmp->getProperties().set("CustomHeight", 40);
    igsCmp->setName("InputGroupSuspendComponent");

    rollCmp = new RavenRollComponent(this);
    rollCmp->setTopLeftPosition(975, 15);
    rollCmp->getProperties().set("defaultX", 870);
    rollCmp->getProperties().set("defaultY", 140);
    rollCmp->getProperties().set("CustomHeight", 35);
    rollCmp->setName("RollComponent");

    transCmp = new RavenTransportComponent(this);
    transCmp->setTopLeftPosition(975, 60);
    transCmp->getProperties().set("defaultX", 870);
    transCmp->getProperties().set("defaultY", 140);
    transCmp->getProperties().set("CustomHeight", 65);
    transCmp->setName("TransportComponent");
    
    editMixCmp = new RavenEditMixComponent(this);
    editMixCmp->setTopLeftPosition(1306, 15);
    editMixCmp->getProperties().set("defaultX", 1225);
    editMixCmp->getProperties().set("defaultY", 130);
    RAVEN_BUTTON_5(editMixCmp);
    editMixCmp->setName("editMixButton");

    clickButton = new RavenNSCButton(this);
    clickButton->setTopLeftPosition(583, 74);
    clickButton->addListener(this);
    clickButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/EditMixClick/images/edit-click-on_02.png", "TOOLBAR/FUNCTIONMODULE/EditMixClick/images/edit-click-off_02.png");
    clickButton->setTag(eNSCGSClickSwitch);
    clickButton->setClickingTogglesState(true);
    clickButton->getProperties().set("defaultX", 1225);
    clickButton->getProperties().set("defaultY", 190);
    RAVEN_BUTTON_2(clickButton);
    clickButton->setName("clickButton");
    
    moveButton = new RavenButton();
    moveButton->setClickingTogglesState(true);
    moveButton->setTopLeftPosition(1416, 16);
    moveButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/MoveFine/images/Movemodule-Finepan-on_01.png", "TOOLBAR/FUNCTIONMODULE/MoveFine/images/Movemodule-Finepan-off_01.png");
    moveButton->getProperties().set("defaultX", 1000);
    moveButton->getProperties().set("defaultY", 140);
    RAVEN_BUTTON_2(moveButton);
    moveButton->setName("moveModulesButton");
    
    fineFaderButton = new RavenButton();
    fineFaderButton->setClickingTogglesState(true);
    fineFaderButton->setTopLeftPosition(1416, 75);
    fineFaderButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/FineFader/FineFaders-on.png", "TOOLBAR/FUNCTIONMODULE/FineFader/FineFaders-off.png");
    fineFaderButton->getProperties().set("defaultX", 515);
    fineFaderButton->getProperties().set("defaultY", 65);
    RAVEN_BUTTON_1(fineFaderButton);
    fineFaderButton->setName("fineFaderButton");
    
    fineButton = new RavenButton();
    fineButton->setClickingTogglesState(true);
    fineButton->setTopLeftPosition(583, 11);
    fineButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanModes/SwitchTO_CircularPanMode.png", "TOOLBAR/FUNCTIONMODULE/PanModes/CircularPanMode.png");
    fineButton->getProperties().set("defaultX", 1300);
    fineButton->getProperties().set("defaultY", 190);
    fineButton->getProperties().set("defaultToolbarX", 0);
    fineButton->getProperties().set("defaultToolbarY", 0);
    RAVEN_BUTTON_1(fineButton);
    fineButton->setName("finePanButton");

#if RAVEN_ESSENTIALS_PALETTE
    essentialsButton = new RavenButton();
    essentialsButton->setName("essentialsButton");
    essentialsButton->setClickingTogglesState(true);
    essentialsButton->setTopLeftPosition(772, 65);
    essentialsButton->setImagePaths("FloatingEssentials/bt-Essential-On.png", "FloatingEssentials/bt-Essential-Off.png");
    essentialsButton->getProperties().set("defaultX", 1830);
    essentialsButton->getProperties().set("defaultY", 75);
    essentialsButton->getProperties().set("CustomHeight", 65);
#endif
    
    hotComp = new RavenHotKeysComponent();
    hotComp->setTopLeftPosition(1490, 18);
    hotComp->getProperties().set("defaultX", 1360);
    hotComp->getProperties().set("defaultY", 135);
    hotComp->getProperties().set("CustomHeight", 110);
    hotComp->setName("HotKeysComponent_0");
    hotKeyComponents.add(hotComp);
    
    exitButton = new RavenButton();
    exitButton->setClickingTogglesState(true);
    exitButton->setName("exitButton");
    exitButton->setImagePaths("PowerButtons/NEW_POWER-OFF.png", "PowerButtons/NEW_POWER-ON.png");
    exitButton->setTopLeftPosition(0, 63);
    
    lastWinButton = new RavenButton();
    lastWinButton->setName("lastWinButton");
    lastWinButton->setClickingTogglesState(true);
    lastWinButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/SHOWHIDE_PLUG/bt-Showhide-Plugin-On.png", "TOOLBAR/FUNCTIONMODULE/SHOWHIDE_PLUG/bt-Showhide-Plugin-Off.png");
    lastWinButton->setTopLeftPosition(521, 75);
    lastWinButton->getProperties().set("defaultX", 1675);
    lastWinButton->getProperties().set("defaultY", 75);
    RAVEN_BUTTON_1(lastWinButton);
    lastWinButton->addListener(this);
    
    counterFuncCmp = new RavenCounterFunctionsComponent(this);
    counterFuncCmp->setTopLeftPosition(860, 65);
    counterFuncCmp->getProperties().set("defaultX", 1750);
    counterFuncCmp->getProperties().set("defaultY", 40);
    counterFuncCmp->getProperties().set("CustomHeight", 65);
    counterFuncCmp->setName("CounterFunctionsComponent");
    
    toolBarModeButton = new RavenButton();
    toolBarModeButton->setClickingTogglesState(true);
    toolBarModeButton->setName("toolbarMode");
    toolBarModeButton->setTopLeftPosition(1734, 17);
    toolBarModeButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/RavenDAW/bt-RavenDaw-DawOn.png", "TOOLBAR/FUNCTIONMODULE/RavenDAW/bt-RavenDaw-DawOff.png");
    toolBarModeButton->getProperties().set("isDAW", false);
    RAVEN_BUTTON_1(toolBarModeButton);
    
    //NEW RAVEN DAW AND RAVEN TOOLBAR IMAGES 1.10.2013
    
    toolBarCustomButton = new RavenButton();
    toolBarCustomButton->setClickingTogglesState(true);
    toolBarCustomButton->setName("toolbarCustom");
    RAVEN_BUTTON_1(toolBarCustomButton);
    toolBarCustomButton->setTopLeftPosition(1734, 71);
    toolBarCustomButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/RavenSettings/ravenSettings-on.png", "TOOLBAR/FUNCTIONMODULE/RavenSettings/ravenSettings-off.png");
    
    targetDAWButton = new RavenButton();
    targetDAWButton->setClickingTogglesState(true);
    targetDAWButton->setName("targetDAW");
    targetDAWButton->setTopLeftPosition(trackBankCmp->getRight()+ 15, 50);
    targetDAWButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/TargetDaw/targetdaw-on.png", "TOOLBAR/FUNCTIONMODULE/TargetDaw/targetdaw-off.png");
    
    //add child touch components in desired touch order
    //exitToolbarMode();
    
    //flip mode buttons//////// (invisible, triggered from touch in raven container)
    flipMode = new RavenNSCButton(this);
    flipMode->setName("flipModeButton");
    flipMode->setChannelIndex(kNscGuiBankGlobalIdentifier);
    flipMode->setBankIndex(0);
    flipMode->setTag(eNSCGSFlip);
    flipMode->addListener(this);
    flipMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/SendChannel/channelOn.png", "TOOLBAR/FUNCTIONMODULE/SendChannel/channelOff.png");
    flipMode->getProperties().set("isFlipMode", false);
    flipMode->getProperties().set("defaultX", 500);
    flipMode->getProperties().set("defaultY", topOffset+10);
    //flipMode->getProperties().set("CustomHeight", ); only on 24 version!!!
    
    flipMode->setTopLeftPosition(lastWinButton->getRight(), lastWinButton->getY());
    
    flipMode2 = new RavenNSCButton(this);
    flipMode2->setChannelIndex(kNscGuiBankGlobalIdentifier);
    flipMode2->setBankIndex(1);
    flipMode2->setTag(eNSCGSFlip);
    flipMode2->setName("flipmode2");
    flipMode2->addListener(this);
    
    flipMode3 = new RavenNSCButton(this);
    flipMode3->setChannelIndex(kNscGuiBankGlobalIdentifier);
    flipMode3->setBankIndex(2);
    flipMode3->setTag(eNSCGSFlip);
    flipMode3->setName("flipmode3");
    flipMode3->addListener(this);
    
    flipMode4 = new RavenNSCButton(this);
    flipMode4->setChannelIndex(kNscGuiBankGlobalIdentifier);
    flipMode4->setBankIndex(3);
    flipMode4->setTag(eNSCGSFlip);
    flipMode4->setName("flipmode4");
    flipMode4->addListener(this);
    
    panMode = new RavenNSCButton(this);
    panMode->setName("panModeButton");
    panMode->getProperties().set("isLeftPanMode", true);
    panMode->setChannelIndex(kNscGuiBankGlobalIdentifier);
    panMode->setBankIndex(0);
    panMode->setTag(eNSCGSPTBankMode_Pan);
    panMode->addListener(this);
    panMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Lon.png", "TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Loff.png");
    panMode->setTopLeftPosition(521, 11);
    panMode->getProperties().set("defaultX", 1675);
    panMode->getProperties().set("defaultY", 15);
    RAVEN_BUTTON_1(panMode);
    
    panMode2 = new RavenNSCButton(this);
    panMode2->setChannelIndex(kNscGuiBankGlobalIdentifier);
    panMode2->setBankIndex(1);
    panMode2->setTag(eNSCGSPTBankMode_Pan);
    panMode2->setName("panmode2");
    panMode2->addListener(this);
    
    panMode3 = new RavenNSCButton(this);
    panMode3->setChannelIndex(kNscGuiBankGlobalIdentifier);
    panMode3->setBankIndex(2);
    panMode3->setTag(eNSCGSPTBankMode_Pan);
    panMode3->setName("panmode3");
    panMode3->addListener(this);
    
    panMode4 = new RavenNSCButton(this);
    panMode4->setChannelIndex(kNscGuiBankGlobalIdentifier);
    panMode4->setBankIndex(3);
    panMode4->setTag(eNSCGSPTBankMode_Pan);
    panMode4->setName("panmode4");
    panMode4->addListener(this);
    
    floatingNavpadButton = new RavenButton();
    floatingNavpadButton->setClickingTogglesState(true);
    floatingNavpadButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/FloatingNavpadBtn/FloatingNavpad-On.png", "TOOLBAR/FUNCTIONMODULE/FloatingNavpadBtn/FloatingNavpad-Off.png");
    floatingNavpadButton->setTopLeftPosition(660, 80);
    floatingNavpadButton->getProperties().set("defaultX", 500);
    floatingNavpadButton->getProperties().set("defaultY", topOffset);
    floatingNavpadButton->getProperties().set("CustomHeight", 40);
    floatingNavpadButton->setName("floatingNavPadButton");
    
    
    /////////////////////////////////
    addBarButton = new RavenButton();
    addBarButton->setName("addBarButton");
    addBarButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/RackAdd/bt-AddRack-on.png", "TOOLBAR/FUNCTIONMODULE/RackAdd/bt-AddRack-off.png");
    addBarButton->setTopLeftPosition(1810, 15);
    addBarButton->getProperties().set("defaultX", 1830);
    addBarButton->getProperties().set("defaultY", 140);
    RAVEN_BUTTON_2(addBarButton);
    
    removeBarButton = new RavenButton();
    removeBarButton->setName("removeBarButton");
    removeBarButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/RackRemove/bt-RemoveRack-on.png", "TOOLBAR/FUNCTIONMODULE/RackRemove/bt-RemoveRack-off.png");
    removeBarButton->setTopLeftPosition(1810, 70);
    removeBarButton->getProperties().set("defaultX", 1830);
    removeBarButton->getProperties().set("defaultY", 200);
    RAVEN_BUTTON_2(removeBarButton);
    
    hybridModeButton = new RavenButton();
    hybridModeButton->setTopLeftPosition(1333, 75);
    hybridModeButton->setImagePaths("FloatingMixer/FloatingMixer_On_btn.png", "FloatingMixer/FloatingMixer_Off_btn.png");
    hybridModeButton->addListener(this);
    hybridModeButton->setClickingTogglesState(true);
    hybridModeButton->setName("hybridModeButton");
    RAVEN_BUTTON_1(hybridModeButton);
    
    
    //**********************************************************
    //******************** Functions Panel 2 *******************
    
    saveLayoutCmp = new RavenSaveLayoutComponent();
    saveLayoutCmp->setTopLeftPosition(78, 18);
    saveLayoutCmp->getProperties().set("defaultX", 640);
    saveLayoutCmp->getProperties().set("defaultY", topOffset);
    saveLayoutCmp->getProperties().set("CustomHeight", 110);
    saveLayoutCmp->setName("SaveLayoutComponent");
    
    editNudgeCmp = new NudgeCycleComponent(this);
    editNudgeCmp->setTopLeftPosition(486, 25);
    editNudgeCmp->getProperties().set("defaultX", 313);
    editNudgeCmp->getProperties().set("defaultY", topOffset + 60);
    editNudgeCmp->getProperties().set("CustomHeight", 45);
    editNudgeCmp->setName("NudgeCycleComponent");
    
    editDropReplaceCmp = new DropReplaceComponent(this);
    editDropReplaceCmp->setTopLeftPosition(486, 70);
    editDropReplaceCmp->getProperties().set("defaultX", 313);
    editDropReplaceCmp->getProperties().set("defaultY", topOffset + 60);
    editDropReplaceCmp->getProperties().set("CustomHeight", 45);
    editDropReplaceCmp->setName("DropReplaceComponent");
    
    editQuantCmp = new QuantizationComponent();
    editQuantCmp->setTopLeftPosition(376, 25);
    editQuantCmp->getProperties().set("defaultX", 313);
    editQuantCmp->getProperties().set("defaultY", topOffset);
    editQuantCmp->getProperties().set("CustomHeight", 90);
    editQuantCmp->setName("QuantizationComponent");

    memComp = new RavenMemoryComponent(this);
    memComp->setTopLeftPosition(615, 27);
    memComp->setEnterButton(utilCmp->getEnterButton()); //link enter buttons => memComp must be initialized after utilComp
    memComp->getProperties().set("defaultX", 1270);
    memComp->getProperties().set("defaultY", 25);
    memComp->getProperties().set("CustomHeight", 90);
    memComp->setName("MemoryComponent");
    
    editToolCmp = new EditToolComponent();
    editToolCmp->setTopLeftPosition(1037, 20);
    editToolCmp->getProperties().set("defaultX", 60);
    editToolCmp->getProperties().set("defaultY", topOffset);
    editToolCmp->getProperties().set("CustomHeight", 45);
    editToolCmp->setName("EditToolComponent");

    editCopyPasteCmp = new CopyPasteComponent(this);
    editCopyPasteCmp->setTopLeftPosition(1037, 80);
    editCopyPasteCmp->getProperties().set("defaultX", 60);
    editCopyPasteCmp->getProperties().set("defaultY", topOffset + 60);
    editCopyPasteCmp->getProperties().set("CustomHeight", 45);
    editCopyPasteCmp->setName("CopyPasteComponent");

    zoomInButton = new RavenButton();
    zoomInButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Zoombuttons/angledShadow/ZoomIn-on.png", "TOOLBAR/FUNCTIONMODULE/Zoombuttons/angledShadow/ZoomIn-off.png");
    zoomInButton->setTopLeftPosition(1389, 18);
    zoomInButton->addListener(this);
    zoomInButton->setName("zoomInButton");
    RAVEN_BUTTON_0(zoomInButton);
        
    zoomOutButton = new RavenButton();
    zoomOutButton->setTopLeftPosition(1347, 18);
    zoomOutButton->addListener(this);
    zoomOutButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Zoombuttons/angledShadow/ZoomOut-on.png", "TOOLBAR/FUNCTIONMODULE/Zoombuttons/angledShadow/ZoomOut-off.png");
    zoomOutButton->setName("zoomOutButton");
    RAVEN_BUTTON_0(zoomOutButton);

    duplicateButton = new RavenButton();
    duplicateButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/duplicateButton/bt-edit-duplicate-On.png", "TOOLBAR/FUNCTIONMODULE/duplicateButton/bt-edit-duplicate-Off.png");
    duplicateButton->setTopLeftPosition(1347, 81);
    duplicateButton->addListener(this);
    duplicateButton->setName("duplicateButton");
    RAVEN_BUTTON_0(duplicateButton);
    
    clipLoopButton = new RavenButton();
    clipLoopButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/clipLoopButton/Bt-cliploop-On.png", "TOOLBAR/FUNCTIONMODULE/clipLoopButton/Bt-cliploop-Off.png");
    clipLoopButton->setTopLeftPosition(1389, 81);
    clipLoopButton->addListener(this);
    clipLoopButton->setName("clipLoopButton");
    RAVEN_BUTTON_0(clipLoopButton);
    
    navPadCmp = new RavenNavPadComponent(this);
    navPadCmp->setTopLeftPosition(1508, 18);
    navPadCmp->getProperties().set("defaultX", 920);
    navPadCmp->getProperties().set("defaultY", topOffset);
    navPadCmp->getProperties().set("CustomHeight", 110);
    navPadCmp->setName("NavPadComponent");

    zoomInKeyCode.push_back(55);//command
    zoomInKeyCode.push_back(30);//]
    zoomOutKeyCode.push_back(55);//command
    zoomOutKeyCode.push_back(33);//[
    duplicateKeyCode.push_back(55);//command
    duplicateKeyCode.push_back(2);//D
    nudgePlusKeyCode.push_back(69);//+
    nudgeMinusKeyCode.push_back(78);//-
    clipLoopKeyCode.push_back(55);//command
    clipLoopKeyCode.push_back(58);//option
    clipLoopKeyCode.push_back(37);//L


    
    //**********************************************************
    //******************** Functions Panel 3 *******************
    
    punchCmp = new RavenPunchComponent(this);
    punchCmp->setTopLeftPosition(607, 66);
    punchCmp->getProperties().set("defaultX", 530);
    punchCmp->getProperties().set("defaultY", 190);
    punchCmp->getProperties().set("CustomHeight", 55);
    punchCmp->setName("PunchComponent");
    
    //transport component needs pointer to quick punch button in order to know whether or not to dissable record while in play mode 
    transCmp->setPunchButton(punchCmp->getPunchButton());
    
    hotComp2 = new RavenHotKeysComponent();
    hotComp2->setTopLeftPosition(1344, 16);
    hotComp2->getProperties().set("defaultX", 1600);
    hotComp2->getProperties().set("defaultY", 135);
    hotComp2->getProperties().set("CustomHeight", 110);
    hotComp2->setName("HotKeysComponent_1");
    hotKeyComponents.add(hotComp2);


    //**********************************************************
    //*********** Other Functions Components *******************
    
//**** NOTE: Not using the drum pad for now...
//    drumPadButton = new RavenButton();
//    drumPadButton->setName("drumPad");
//    drumPadButton->setClickingTogglesState(true);
//    drumPadButton->setTopLeftPosition(moveButton->getX()+300, clickButton->getY()+30);
//    drumPadButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/DrumPadIcon/test_button_on.png", "TOOLBAR/FUNCTIONMODULE/DrumPadIcon/test_button_off.png");
//    drumPadButton->getProperties().set("defaultX", 1830);
//    drumPadButton->getProperties().set("defaultY", 75);
//    ravenTouchButtons.add(drumPadButton);
//    addAndMakeVisible(drumPadButton);

}


void RavenFunctionsManager::setDefaultLayout()
{
    if(functionsComponents.size() == 0) return;
    
    if(functionsComponents.size() >= 1)
    {
        functionsComponents[0]->addAndMakeVisible(moveButton);
        functionsComponents[0]->addAndMakeVisible(lastWinButton);
        functionsComponents[0]->addAndMakeVisible(trackBankCmp);
        functionsComponents[0]->addAndMakeVisible(modComp);
        functionsComponents[0]->addAndMakeVisible(igsCmp);
        functionsComponents[0]->addAndMakeVisible(rollCmp);
        functionsComponents[0]->addAndMakeVisible(transCmp);
        functionsComponents[0]->addAndMakeVisible(editMixCmp);
        functionsComponents[0]->addAndMakeVisible(clickButton);
        functionsComponents[0]->addAndMakeVisible(fineFaderButton);
        functionsComponents[0]->addAndMakeVisible(fineButton);
        functionsComponents[0]->addAndMakeVisible(essentialsButton);
        functionsComponents[0]->addAndMakeVisible(exitButton);
        functionsComponents[0]->addAndMakeVisible(lastWinButton);
        functionsComponents[0]->addAndMakeVisible(counterFuncCmp);
        functionsComponents[0]->addAndMakeVisible(toolBarModeButton);
        functionsComponents[0]->addAndMakeVisible(toolBarCustomButton);
        functionsComponents[0]->addAndMakeVisible(targetDAWButton);
        if(RAVEN_24) functionsComponents[0]->addAndMakeVisible(flipMode);
        functionsComponents[0]->addAndMakeVisible(panMode);
        functionsComponents[0]->addAndMakeVisible(floatingNavpadButton);
        functionsComponents[0]->addAndMakeVisible(addBarButton);
        functionsComponents[0]->addAndMakeVisible(removeBarButton);
        functionsComponents[0]->addAndMakeVisible(hybridModeButton);
        functionsComponents[0]->addAndMakeVisible(trackBankCmp);
        functionsComponents[0]->addAndMakeVisible(utilCmp);
        functionsComponents[0]->addAndMakeVisible(hotComp);
        
//        functionsComponents[0]->addFunctionsChildTouchComponent(trackBankCmp);
//        functionsComponents[0]->addFunctionsChildTouchComponent(utilCmp);
//        functionsComponents[0]->addFunctionsChildTouchComponent(modComp);
//        functionsComponents[0]->addFunctionsChildTouchComponent(igsCmp);
//        functionsComponents[0]->addFunctionsChildTouchComponent(counterFuncCmp);
//        functionsComponents[0]->addFunctionsChildTouchComponent(rollCmp);
//        functionsComponents[0]->addFunctionsChildTouchComponent(transCmp);
//        functionsComponents[0]->addFunctionsChildTouchComponent(editMixCmp);
//        functionsComponents[0]->addFunctionsChildTouchComponent(hotComp);
        
        functionsComponents[0]->addFunctionsRavenTouchButton(lastWinButton);
        functionsComponents[0]->addFunctionsRavenTouchButton(fineButton);
        functionsComponents[0]->addFunctionsRavenTouchButton(floatingNavpadButton);
        functionsComponents[0]->addFunctionsRavenTouchButton(hybridModeButton);
        functionsComponents[0]->addFunctionsRavenTouchButton(moveButton);
        functionsComponents[0]->addFunctionsRavenTouchButton(fineFaderButton);
        functionsComponents[0]->addFunctionsRavenTouchButton(toolBarModeButton);
        functionsComponents[0]->addFunctionsRavenTouchButton(toolBarCustomButton);
        functionsComponents[0]->addFunctionsRavenTouchButton(targetDAWButton);//-josh to stop memory leak
        functionsComponents[0]->addFunctionsRavenTouchButton(addBarButton);
        functionsComponents[0]->addFunctionsRavenTouchButton(removeBarButton);
        functionsComponents[0]->addFunctionsRavenTouchButton(essentialsButton);
        functionsComponents[0]->addFunctionsRavenTouchButton(exitButton);
        
        functionsComponents[0]->addFunctionsRavenNSCTouchButton(panMode);
        if(RAVEN_24) functionsComponents[0]->addFunctionsRavenNSCTouchButton(flipMode);
        functionsComponents[0]->addFunctionsRavenNSCTouchButton(clickButton);
    }
    if(functionsComponents.size() >= 2)
    {
        functionsComponents[1]->addAndMakeVisible(saveLayoutCmp);
        functionsComponents[1]->addAndMakeVisible(editQuantCmp);
        functionsComponents[1]->addAndMakeVisible(editNudgeCmp);
        functionsComponents[1]->addAndMakeVisible(editDropReplaceCmp);
        functionsComponents[1]->addAndMakeVisible(memComp);
        functionsComponents[1]->addAndMakeVisible(editToolCmp);
        functionsComponents[1]->addAndMakeVisible(editCopyPasteCmp);
        functionsComponents[1]->addAndMakeVisible(zoomInButton);
        functionsComponents[1]->addAndMakeVisible(zoomOutButton);
        functionsComponents[1]->addAndMakeVisible(duplicateButton);
        functionsComponents[1]->addAndMakeVisible(clipLoopButton);
        functionsComponents[1]->addAndMakeVisible(navPadCmp);
        
//        functionsComponents[1]->addFunctionsChildTouchComponent(saveLayoutCmp);
//        functionsComponents[1]->addFunctionsChildTouchComponent(editQuantCmp);
//        functionsComponents[1]->addFunctionsChildTouchComponent(editNudgeCmp);
//        functionsComponents[1]->addFunctionsChildTouchComponent(editDropReplaceCmp);
//        functionsComponents[1]->addFunctionsChildTouchComponent(memComp);
//        functionsComponents[1]->addFunctionsChildTouchComponent(editToolCmp);
//        functionsComponents[1]->addFunctionsChildTouchComponent(editCopyPasteCmp);
//        functionsComponents[1]->addFunctionsChildTouchComponent(navPadCmp);
        
        functionsComponents[1]->addFunctionsRavenTouchButton(zoomInButton);
        functionsComponents[1]->addFunctionsRavenTouchButton(zoomOutButton);
        functionsComponents[1]->addFunctionsRavenTouchButton(duplicateButton);
        functionsComponents[1]->addFunctionsRavenTouchButton(clipLoopButton);
    }
    if(functionsComponents.size() >= 3)
    {
        functionsComponents[2]->addAndMakeVisible(punchCmp);
        functionsComponents[2]->addAndMakeVisible(hotComp2);
        //functionsComponents[2]->addAndMakeVisible(hotComp3);
        
//        functionsComponents[2]->addFunctionsChildTouchComponent(punchCmp);
//        functionsComponents[2]->addFunctionsChildTouchComponent(hotComp2);
    }
    
    for(int i = 0; i < functionsComponents.size(); i++)
    {
        for(int j = 0; j < functionsComponents[i]->getNumChildComponents(); j++)
        {
            proxyComponents.add(new RavenProxyComponent());
        }
    }
}


bool RavenFunctionsManager::repositionFunctionsModule(savedFunctionsModuleInfo saveInfo, Component* module, RavenWindowComponent* palette)
{
        //To animate component:    
        //1. setup proxy component and window at global location
        int width = module->getWidth();
        int height = module->getHeight();
        proxyComponents[animateModuleCount]->setVisible(false);
        proxyComponents[animateModuleCount]->setImage(module->createComponentSnapshot(Rectangle<int>(width, height)));
        proxyComponents[animateModuleCount]->setSize(width, height);
        proxyComponents[animateModuleCount]->setTopLeftPosition(module->getScreenX(), module->getScreenY());
        proxyComponents[animateModuleCount]->setAlpha(1.0); 
        proxyComponents[animateModuleCount]->finalAlpha = 1.0; //reset alpha for animation
        
    
        //bool originOnDesktop = module->getParentComponent()->isShowing(); // should use isOnDesktop
        RavenWindowComponent *origin = dynamic_cast<RavenWindowComponent*>(module->getParentComponent());
        bool originOnDesktop = origin->getWindow()->isOnDesktop();
        bool destinationOnDesktop  = palette->getWindow()->isOnDesktop();
    
        Point<int> moveToPointLocal = Point<int>(saveInfo.topLeftX, saveInfo.topLeftY);
        Point<int> moveToPointGlobal  = palette->localPointToGlobal(moveToPointLocal);
    
        //Rectangle<int> moveToRect;
        
        bool animate = true;
    
        //2. animate proxy window to the desired screen location
        if(originOnDesktop && destinationOnDesktop) // move
        {    
            proxyComponents[animateModuleCount]->moveToRect = Rectangle<int>(moveToPointGlobal.getX(),  moveToPointGlobal.getY(), module->getWidth(), module->getHeight());
        }
        else if(originOnDesktop && !destinationOnDesktop) //fade out
        {
            proxyComponents[animateModuleCount]->moveToRect = Rectangle<int>(proxyComponents[animateModuleCount]->getX(),  proxyComponents[animateModuleCount]->getY(), module->getWidth(), module->getHeight());
            proxyComponents[animateModuleCount]->finalAlpha = 0.0;
        }
        else if(destinationOnDesktop && !originOnDesktop) //fade in
        {
            proxyComponents[animateModuleCount]->setAlpha(0.0);
            proxyComponents[animateModuleCount]->setTopLeftPosition(moveToPointGlobal.getX(), moveToPointGlobal.getY());
            proxyComponents[animateModuleCount]->moveToRect = Rectangle<int>(moveToPointGlobal.getX(),  moveToPointGlobal.getY(), module->getWidth(), module->getHeight());
            
        }
        else    //no animation, just swap
        {
            animate = false;
            module->setTopLeftPosition(saveInfo.topLeftX, saveInfo.topLeftY);
            palette->addAndMakeVisible(module);
        }
    
        if(animate)
        {
            moverComponent->addAndMakeVisible(proxyComponents[animateModuleCount]);            
            proxyComponents[animateModuleCount]->needsAnimating = true;
            proxyComponents[animateModuleCount]->realComponent = module;
            animateModuleCount++;
        }
    
        //3. change listener callback from animator will place modules in appropriate palettes
    
        //restore hot key label and key code.
        if(module->getName().upToFirstOccurrenceOf("_", false, false) != "HotKeysComponent") return animate;
    
        RavenHotKeysComponent* curHotComp = (RavenHotKeysComponent*)module;
        
        for(int k = 0; k < NUM_HOTKEYS; k++)
        {
            curHotComp->setKeyLabelText(k, saveInfo.hotKeyLabel[k]);
            
            String keyCodeString = saveInfo.hotkeyCode[k];
            StringArray tokens;
            tokens.addTokens (keyCodeString, "_", "");
            std::vector<int> storedKeyCode;
            for (int n=0; n<tokens.size(); n++)
            {
                storedKeyCode.push_back(tokens[n].getIntValue());
            }
            curHotComp->setKeyCode(k, storedKeyCode);
        }
    
    return animate;
}

void RavenFunctionsManager::loadSavedFunctionsLayout()
{
    RAVEN_TOUCH_LOCK
    animateModuleCount = 0;
    movePaletteMap.clear();
    movePosMap.clear();
    
    /* will at least one module animate?  If so, then we wait for animation to finish before removing the mover component
     otherwise, we will remove it immediately at the end of this function */
    bool willAnimate = false;
    
    for(int i = 0; i < functionsModulePositions.size(); i++)
    {
        Component* curComp;
        bool curCompOnEssentials = false;
        
        //1. find the functionsModule and point to it. Check all functions palettes and the floating Essentials palette.
        for(int j = 0; j < floatingEssentials->getNumChildComponents(); j++)
        {
            if(functionsModulePositions[i].moduleName == floatingEssentials->getChildComponent(j)->getName())
            {
                curComp = floatingEssentials->getChildComponent(j);
                curCompOnEssentials = true;
                break;
            }
        }
        if(!curCompOnEssentials)
        {
            for(int j = 0; j < functionsComponents.size(); j++)
            {
                for(int k = 0; k < functionsComponents[j]->getNumChildComponents(); k++)
                {
                    if(functionsModulePositions[i].moduleName == functionsComponents[j]->getChildComponent(k)->getName())
                    {
                        curComp = functionsComponents[j]->getChildComponent(k);
                        break;
                    }
                }
            }
        }

        //2. check to see if the module has changed position. Continue to the next module if it hasn't.
        bool parentChanged = (curComp->getParentComponent()->getName() != functionsModulePositions[i].paletteName);
        bool positionChanged = (functionsModulePositions[i].topLeftX != curComp->getX() || functionsModulePositions[i].topLeftY != curComp->getY());
        if(!parentChanged && !positionChanged) continue;
        
        bool animate = false;
        
        //3. reposition the module to the appropriate palette.
        if(functionsModulePositions[i].paletteName == "FloatingEssentials")
        {
            animate = repositionFunctionsModule(functionsModulePositions[i], curComp, floatingEssentials);
            movePaletteMap.set(curComp, floatingEssentials);
            movePosMap.set(curComp, functionsModulePositions[i]);
        }
        else
        {
            for(int j = 0; j < functionsComponents.size(); j++)
            {
                if(functionsModulePositions[i].paletteName == functionsComponents[j]->getName())
                {
                    animate = repositionFunctionsModule(functionsModulePositions[i], curComp, functionsComponents[j]);
                    movePaletteMap.set(curComp, functionsComponents[j]);
                    movePosMap.set(curComp, functionsModulePositions[i]);
                }
            }
        }
        
        willAnimate = (willAnimate || animate);
    }
    
    synchronizeFloatingPaletteButtons();
    floatingEssentials->resizeFloatingPalette();
    
    if(willAnimate)
    {
        moverComponent->addMoverToDesktop();
        startTimer(START_ANIMATION_TIMER, proxyTransitionMS);
    }
    else RAVEN_TOUCH_UNLOCK
}

void RavenFunctionsManager::synchronizeFloatingPaletteButtons()
{
    for(int i = 0; i < RAVEN->getFloatingWindows().size(); i++)
    {
        RavenWindow *curFloatingWindow = RAVEN->getFloatingWindows()[i];
        
        if(curFloatingWindow->getName() == "navpadWindow")
            (curFloatingWindow->isOnDesktop()) ? floatingNavpadButton->setToggleState(true, false) : floatingNavpadButton->setToggleState(false, false);
        if(curFloatingWindow->getName() == "essentialsWindow")
            (curFloatingWindow->isOnDesktop()) ? essentialsButton->setToggleState(true, false) : essentialsButton->setToggleState(false, false);
        if(curFloatingWindow->getName() == "counterWindow")
            (curFloatingWindow->isOnDesktop()) ? counterFuncCmp->getCounterButton()->setToggleState(true, false) : counterFuncCmp->getCounterButton()->setToggleState(false, false);
        if(curFloatingWindow->getName() == "floatingMixerControlWindow")
            (curFloatingWindow->isOnDesktop()) ? hybridModeButton->setToggleState(true, false) : hybridModeButton->setToggleState(false, false);
    }
}


void RavenFunctionsManager::enterHybridMode()
{
    hybridModeButton->setToggleState(true, false);
    enableMoveModules(false);
}

void RavenFunctionsManager::exitHybridMode()
{
    hybridModeButton->setToggleState(false, false);
    enableMoveModules(true);
}

void RavenFunctionsManager::enableMoveModules(bool enable)
{
    if(enable)
    {
        moveButton->enabled = true;
        moveButton->setAlpha(1.0);
    }
    else
    {
        moveButton->enabled = false;
        moveButton->setAlpha(0.4);
    }
}

void RavenFunctionsManager::enableHybridMode(bool enable)
{
    if(enable)
    {
        hybridModeButton->enabled = true;
        hybridModeButton->setAlpha(1.0);
    }
    else
    {
        hybridModeButton->enabled = false;
        hybridModeButton->setAlpha(0.4);
    }
}


void RavenFunctionsManager::enterToolbarMode()
{
    //if(toolbarMode) return;
    RAVEN->setDawMode(true);
    toolbarMode = true;
    toolBarModeButton->setToggleState(true, false);
//
//    for(int i = 0; i < getNumChildComponents(); i++)
//    {
//        int curX = getChildComponent(i)->getX();
//        int curY = getChildComponent(i)->getY();
//        if (curY < singlePanelHeight)   getChildComponent(i)->setVisible(true);
//        if (curY < singlePanelHeight*numMixModeBars)   getChildComponent(i)->setVisible(true);
//        getChildComponent(i)->setTopLeftPosition(curX, curY + (numDAWModeBars-numMixModeBars)*singlePanelHeight);
//    }
//    
//    getProperties().set("preToolbarY", getY());
//    setSize(getWidth(), numDAWModeBars*singlePanelHeight);
//    //    setOrigHeight(getHeight());
//    setTopLeftPosition(0, SCREEN_HEIGHT-getHeight());
//    
//    resizeFunctionsMouseHider();
//    repaint();
}

void RavenFunctionsManager::exitToolbarMode()
{
    //if(!toolbarMode) return;
    RAVEN->setDawMode(false);
    toolbarMode = false;
    toolBarModeButton->setToggleState(false, false);
//
//    for(int i = 0; i < getNumChildComponents(); i++)
//    {
//        int curX = getChildComponent(i)->getX();
//        int curY = getChildComponent(i)->getY();
//        if (curY < singlePanelHeight)
//            if (curY < (numDAWModeBars-numMixModeBars)*singlePanelHeight) getChildComponent(i)->setVisible(false); ////??????
//        getChildComponent(i)->setTopLeftPosition(curX, curY - (numDAWModeBars-numMixModeBars)*singlePanelHeight);
//    }
//    
//    
//    //mouseHider->setTopLeftPosition(0, 0);
//    mouseHider->setSize(getWidth(), numMixModeBars*singlePanelHeight);
//    mouseHider->setAlwaysOnTop(true);
//    mouseHider->toFront(false);
//    
//    int preY = getProperties().getWithDefault("preToolbarY", 0);
//    setTopLeftPosition(0, preY);
//    setSize(getWidth(), numMixModeBars*singlePanelHeight);
//    //    setOrigHeight(getHeight());
//    
//    resizeFunctionsMouseHider();
//    
//    //    for(int i = 0; i < getNumChildComponents(); i++)
//    //    {
//    //        int curX = getChildComponent(i)->getX();
//    //        int curY = getChildComponent(i)->getY();
//    //        //if (curY < singlePanelHeight)
//    //        //if (curY < (numDAWModeBars-numMixModeBars)*singlePanelHeight) getChildComponent(i)->setVisible(false); ////??????
//    //        if (curY < screenHeight-getHeight()) getChildComponent(i)->setVisible(false);
//    //        getChildComponent(i)->setTopLeftPosition(curX, curY);
//    //    }
//    
//    
//    repaint();
}

void RavenFunctionsManager::enterCustomMode()
{
    if(customMode) return;
    toolBarCustomButton->setToggleState(true, false);
    customMode = true;
}

void RavenFunctionsManager::exitCustomMode()
{
    if(!customMode) return;
    toolBarCustomButton->setToggleState(false, false);
    customMode = false;
}


bool RavenFunctionsManager::checkModuleTouch(TUIO::TuioCursor *tcur, int touchid)
{
    if(RavenTouchComponent::componentTouched(toolBarCustomButton, tcur, true) || RavenTouchComponent::componentTouched(floatingEssentials->getEssentialsCustomButton(), tcur, true))
    {
        if(movingModules.size() == 0)
            toolBarCustomButton->triggerClick();
        return false;
    }
    else if(RavenTouchComponent::componentTouched(addBarButton, tcur, false))
    {
        return false;
    }
    else if(RavenTouchComponent::componentTouched(removeBarButton, tcur, false))
    {
        return false;
    }
    else if(RavenTouchComponent::componentTouched(exitButton, tcur, false))
    {
        return false;
    }
    
    Component *parent = 0;
    
    if(RavenTouchComponent::componentTouched(floatingEssentials, tcur, false))
    {
        parent = floatingEssentials;
    }
    else
    {
        for(int i = 0; i < functionsComponents.size(); i++)
        {
            if(RavenTouchComponent::componentTouched(functionsComponents[i], tcur, false))
            {
                parent = functionsComponents[i];
                break;
            }
        }
    }
    
    if(!parent) return false;
    
    for(int i = 0; i < parent->getNumChildComponents(); i++)
    {
        Component *cmp = parent->getChildComponent(i);
        if(cmp->getName() == "mouseHider" || cmp->getName() == "MouseHider") continue;
        if(cmp->getName() == "essentialsAddBarButton" || cmp->getName() == "essentialsRemoveBarButton") continue;
        int yTrim = cmp->getHeight() - (int)cmp->getProperties().getWithDefault("CustomHeight", cmp->getHeight());
        if(RavenTouchComponent::componentTouched(cmp, tcur, false, 0, 0, 0, yTrim))
        {
            RAVEN_TOUCH_LOCK
            
            //set touchid....
            cmp->getProperties().set("lastX", cmp->getX());
            cmp->getProperties().set("lastY", cmp->getY());
            cmp->getProperties().set("initTouchX", tcur->getScreenX(SCREEN_WIDTH) - cmp->getScreenX());
            cmp->getProperties().set("initTouchY", tcur->getScreenY(SCREEN_HEIGHT) - cmp->getScreenY());
            cmp->getProperties().set("lastLocation", parent->getName());
            cmp->getProperties().set("touchid", touchid);
            cmp->getProperties().set("moveModuleCount", 0);
            
            movingModules.add(cmp);
            
            LOCK_JUCE_THREAD
            {
                int width = cmp->getWidth();
                int height = cmp->getHeight();
                proxyComponents[touchid]->setImage(cmp->createComponentSnapshot(Rectangle<int>(width, height)));
                proxyComponents[touchid]->setSize(width, height);
                
                moverComponent->addChildComponent(proxyComponents[touchid]);
                proxyComponents[touchid]->setTopLeftPosition(cmp->getScreenX(), cmp->getScreenY());
                
                proxyComponents[touchid]->setVisible(true);
                proxyComponents[touchid]->setAlpha(1.0); //in case modified from animation
                
                modulesToBeHiddenQueue.add(cmp);
                startTimer(ADD_MOVER_TIMER, proxyTransitionMS);
            }
            
            return true;
        }
    }
    
    return false;
}

void RavenFunctionsManager::moveModule(int screenX, int screenY, int touchid)
{
    int tempMoveModuleCount = 0;
    Component* curMovingModule;
    for(int i = 0; i < movingModules.size(); i++)
    {
        int curTouchid = movingModules[i]->getProperties().getWithDefault("touchid", 0);
        if(touchid == curTouchid)
        {
            curMovingModule = movingModules[i];
            break;
        }
        else
        {
            curMovingModule = movingModules[0];
        }
    }
    
    tempMoveModuleCount = curMovingModule->getProperties().getWithDefault("moveModuleCount", 0);

    if(tempMoveModuleCount % (FLOATING_DOWNSAMPLE_AMT*movingModules.size()) == 0)
    {
        int initTouchX = curMovingModule->getProperties().getWithDefault("initTouchX", curMovingModule->getWidth()/2);
        int initTouchY = curMovingModule->getProperties().getWithDefault("initTouchY", curMovingModule->getHeight()/2);

        int topLeftX = screenX - initTouchX;
        int topLeftY = screenY - initTouchY;
        
        LOCK_JUCE_THREAD proxyComponents[touchid]->setTopLeftPosition(topLeftX, topLeftY);//curMovingModule->setTopLeftPosition(topLeftX, topLeftY);

        //todo - improve messaging system so this works..
        //RavenComponentMoveMessage *m = new RavenComponentMoveMessage(curMovingModule, topLeftX, topLeftY);
        //postMessage(m);
    }
    
    tempMoveModuleCount++;
    if(tempMoveModuleCount >= 50000) tempMoveModuleCount = 0;
    curMovingModule->getProperties().set("moveModuleCount", tempMoveModuleCount);
    
}

void RavenFunctionsManager::endModuleMove(int touchid)
{
    
    LOCK_JUCE_THREAD
    {
        Component* curMovingModule;
        int curModuleIndex;
        for(int i = 0; i < movingModules.size(); i++)
        {
            int curTouchid = movingModules[i]->getProperties().getWithDefault("touchid", 0);
            if(touchid == curTouchid)
            {
                curMovingModule = movingModules[i];
                curModuleIndex = i;
                break;
            }
            else
            {
                curMovingModule = movingModules[0];
                curModuleIndex = 0;
            }
        }
    
        int yPos = proxyComponents[touchid]->getY() + proxyComponents[touchid]->getHeight()/2;
        int xPos = proxyComponents[touchid]->getX();
        
        Point<int> screenPos;
        screenPos.x = xPos;
        screenPos.y = yPos;
        
        //find the parent component of the module. then try to position it at end of routine.
        Component* moduleParent = nullptr;
        if(RavenTouchComponent::componentTouched(floatingEssentials, xPos, yPos))
        {
            moduleParent = floatingEssentials;
        }
        else
        {
            for(int i = 0; i < functionsComponents.size(); i++)
            {
                if(RavenTouchComponent::componentTouched(functionsComponents[i], xPos, yPos))
                {
                    moduleParent = functionsComponents[i];
                    break;
                }
            }
        }
        
        //check if module should be restricted
        bool restrictModule = restrictModulePosition(curMovingModule, moduleParent);
                
        //check to see if module can be positioned in parent component
        bool relocateModule = false;
        if(!moduleParent)
        {
            relocateModule = false;
        }
        else if(moduleParent == floatingEssentials && !restrictModule)
        {
            Point<int> localPosition = floatingEssentials->getLocalPoint(0, screenPos);
            floatingEssentials->addAndMakeVisible(curMovingModule);
            curMovingModule->setTopLeftPosition(localPosition.x, localPosition.y - curMovingModule->getHeight()/2);
            curMovingModule->getProperties().set("isInFloatingEssentials", true);
            relocateModule = floatingEssentials->positionModule(curMovingModule);
        }
        else if(!restrictModule)
        {
            Point<int> localPosition = moduleParent->getLocalPoint(0, screenPos);
            moduleParent->addAndMakeVisible(curMovingModule);
            curMovingModule->setTopLeftPosition(localPosition.x, localPosition.y - curMovingModule->getHeight()/2);
            curMovingModule->getProperties().set("isInFloatingEssentials", false);
            relocateModule = positionModule(curMovingModule, (RavenFunctionsComponent*)moduleParent);
        }
        
        if(!relocateModule || restrictModule) resetToOriginalLocation(curMovingModule);

        
        //resize the FE palette in the event a module has been removed from it.
        String lastLoc = curMovingModule->getProperties().getWithDefault("lastLocation", "");
        if(lastLoc == "FloatingEssentials") floatingEssentials->resizeFloatingPalette();
        
        //Remove the current moving module from movingModules array
        movingModules.remove(curModuleIndex);
        curMovingModule->setVisible(true);
        RAVEN_TOUCH_LOCK
        proxiesToBeRemovedQueue.add(proxyComponents[touchid]);
        startTimer(REMOVE_MODULE_TIMER, proxyTransitionMS);
    }
}

bool RavenFunctionsManager::restrictModulePosition(Component* curMovingModule, Component* palette)
{
    bool resetPosition = false;
    
    if (curMovingModule == moveButton)
    {
        if(palette->getName() != "Functions")
        {
            resetPosition = true;
        }
    }
    else if (curMovingModule == essentialsButton)
    {
        if(palette->getName() == "FloatingEssentials")
        {
            resetPosition = true;
        }
    }
    
    if(!resetPosition)  return false; //don't restrict this module's position
    else                return true; //restrict this module's position
}


void RavenFunctionsManager::resetToOriginalLocation(Component* curMovingModule)
{
    int orgX = curMovingModule->getProperties().getWithDefault("lastX", 0);
    int orgY = curMovingModule->getProperties().getWithDefault("lastY", 0);
    String lastLoc = curMovingModule->getProperties().getWithDefault("lastLocation", "");
    curMovingModule->setTopLeftPosition(orgX, orgY);
    if(lastLoc == "Functions")
    {
        functionsComponents[0]->addAndMakeVisible(curMovingModule);
        curMovingModule->getProperties().set("isInFloatingEssentials", false);
    }
    else if(lastLoc == "Functions2")
    {
        functionsComponents[1]->addAndMakeVisible(curMovingModule);
        curMovingModule->getProperties().set("isInFloatingEssentials", false);
    }
    else if(lastLoc == "Functions3")
    {
        functionsComponents[2]->addAndMakeVisible(curMovingModule);
        curMovingModule->getProperties().set("isInFloatingEssentials", false);
    }
    else if(lastLoc == "FloatingEssentials")
    {
        floatingEssentials->addAndMakeVisible(curMovingModule);
        curMovingModule->getProperties().set("isInFloatingEssentials", true);
    }
    
}

bool RavenFunctionsManager::positionModule(Component* curMovingModule, RavenFunctionsComponent* parentComponent)
{
    
    int ySetting = 0;
    int customHeight = (int)curMovingModule->getProperties().getWithDefault("CustomHeight", curMovingModule->getHeight());
    
    //**********************************************************
    //******** Hack for Shitty Looking Transport Comp **********
    if(curMovingModule->getName() == "TransportComponent")
    {
        bottomOffset = 23;
    }
    //**********************************************************
    //**********************************************************
    
    singlePanelHeight = 141;
    int y1 = 0;
    int y2 = y1 + singlePanelHeight/2;
    int y3 = y1 + singlePanelHeight;
    
    //NOTE: the yPos needs to be the midpoint of the module.
    int localPositionY = curMovingModule->getY() + curMovingModule->getHeight()/2;    
    if(localPositionY >= y1 && localPositionY < y2)
    {
        if(customHeight <= singlePanelHeight/2)
        {
            ySetting = y1 + (singlePanelHeight/2 - topOffset)/2 - customHeight/2 + topOffset;
        }else
        {
            ySetting = y1 + singlePanelHeight/2 - customHeight/2;
        }
        
        curMovingModule->setTopLeftPosition(curMovingModule->getScreenX(), ySetting); //was y1+topOffset
    }
    else if(localPositionY >= y2 && localPositionY <= y3)
    {
        // float compToPanel = (float)curMovingModule->getHeight()/((float)singlePanelHeight/2);
        //int customHeight = (int)curMovingModule->getProperties().getWithDefault("CustomHeight", curMovingModule->getHeight());
        
        //if(compToPanel < componentToPanelThreshold)
        if(customHeight <= singlePanelHeight/2)
        {
            ySetting = y3 - ((singlePanelHeight/2 - bottomOffset)/2 + bottomOffset + customHeight/2);
            curMovingModule->setTopLeftPosition(curMovingModule->getScreenX(), ySetting);  //was: y3 - customHeight - bottomOffset
        }
        else
        {
            ySetting = y1 + singlePanelHeight/2 - customHeight/2;
            curMovingModule->setTopLeftPosition(curMovingModule->getScreenX(), ySetting);  //was: y1 + topOffset
        }
    }
    else if(localPositionY > y3 || localPositionY < y1)
    {
        return false;
    }
    
    // X snapping
    /*
    const int snapX = 30;
    int plusX = curMovingModule->getX() % snapX;
    if(plusX < snapX/2) plusX *= -1;
    else plusX = snapX - plusX;
    //curMovingModule->setTopLeftPosition(curMovingModule->getX() + plusX, curMovingModule->getY());
     */
    
    
    RavenFunctionsComponent* curFunctionsComponentTouched = parentComponent;
    
    //2. look for overlap. Adjust the coordinates of the component to fix it.
    int trial = 0;
    bool foundOverlap = true;
    while(foundOverlap && trial < 10)
    {
        foundOverlap = false;
        
        for(int i = 0; i < curFunctionsComponentTouched->getNumChildComponents(); i++)
        {
            if(curMovingModule->getName() == curFunctionsComponentTouched->getChildComponent(i)->getName()) continue;
            if(curFunctionsComponentTouched->getChildComponent(i)->getName() == "mouseHider") continue;
            
            Rectangle<int> curCompRect;
            curCompRect = curFunctionsComponentTouched->getChildComponent(i)->getScreenBounds();
            curCompRect.setHeight(curFunctionsComponentTouched->getChildComponent(i)->getProperties().getWithDefault("CustomHeight", curFunctionsComponentTouched->getChildComponent(i)->getHeight()));
            
            Rectangle<int> curMovingModuleRect(curMovingModule->getScreenBounds());
            curMovingModuleRect.setHeight(curMovingModule->getProperties().getWithDefault("CustomHeight", curMovingModule->getHeight()));
            
            if(curMovingModuleRect.intersects(curCompRect))
            {
                Rectangle<int> intersectionArea = curMovingModuleRect.getIntersection(curCompRect);
                
                int newX = (curMovingModule->getScreenBounds().getCentreX() <= curCompRect.getCentreX()) ? curMovingModule->getX() - intersectionArea.getWidth() : curMovingModule->getX() + intersectionArea.getWidth();
                
                curMovingModule->setTopLeftPosition(newX, curMovingModule->getY());
                
                foundOverlap = true;
            }
        }
        trial++;
    }
    
    if((foundOverlap && trial > 8) || curMovingModule->getX() < 50 || curMovingModule->getRight() > 1870)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void RavenFunctionsManager::releaseFunctionsHotKeysTouch(TUIO::TuioCursor *tcur)
{
    for(int i = 0; i < functionsComponents.size(); i++)
    {
        functionsComponents[i]->releaseHotKeyTouch(tcur);
    }
}

bool RavenFunctionsManager::checkFunctionsButtonTouched(TUIO::TuioCursor *tcur, bool ignoreIsVisible)
{
    
    if(floatingEssentials->checkButtonTouch(tcur, ignoreIsVisible)) return true;
    
    for(int i = 0 ; i < functionsComponents.size(); i++)
    {
        if(functionsComponents[i]->checkButtonTouch(tcur, ignoreIsVisible))
            return true;
    }
    
    return false;
}

Button* RavenFunctionsManager::checkFunctionsHoldButtonTouched(TUIO::TuioCursor *tcur)
{
    
    if(floatingEssentials->checkHoldButtonTouch(tcur) != 0) return floatingEssentials->checkHoldButtonTouch(tcur);
    
    for(int i = 0 ; i < functionsComponents.size(); i++)
    {
        if(functionsComponents[i]->checkHoldButtonTouch(tcur) != 0)
            return functionsComponents[i]->checkHoldButtonTouch(tcur);
    }
    
    return 0;
}


bool RavenFunctionsManager::checkFunctionsTouched(int touchX, int touchY)
{
    if(RavenTouchComponent::componentTouched(floatingEssentials, touchX, touchY)) return true;
    
    for(int i = 0 ; i < functionsComponents.size(); i++)
    {
        if(RavenTouchComponent::componentTouched(functionsComponents[i], touchX, touchY))
            return true;
    }
    return false;
}


void RavenFunctionsManager::buttonClicked (Button* b)
{
    //    printf("button clicked \n");
    if(b == toolBarModeButton)
    {
        //printf("got something\n");
        
        bool isDAW = toolBarModeButton->getProperties().getWithDefault("isDAW", false);
        if(isDAW)
        {
            toolBarModeButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/RavenDAW/bt-RavenDaw-DawOn.png", "TOOLBAR/FUNCTIONMODULE/RavenDAW/bt-RavenDaw-DawOff.png");
        }
        else
        {
            toolBarModeButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/RavenDAW/bt-RavenDaw-RavenOff.png", "TOOLBAR/FUNCTIONMODULE/RavenDAW/bt-RavenDaw-RavenOn.png");
        }
        toolBarModeButton->getProperties().set("isDAW", !isDAW);
    }
    else if(b == lastWinButton)
    {
        insWinButton->triggerClick();
        return;
    }
    else if((b == addBarButton || b->getName() == "essentialsAddBarButton") && !RAVEN->isHybridMode())
    {
        //addBar();
        return;
    }
    else if((b == removeBarButton || b->getName() == "essentialsRemoveBarButton") && !RAVEN->isHybridMode())
    {
        //removeBar();
        return;
    }
    else if(b == hybridModeButton)
    {
        if(b->getToggleState()) RAVEN->setHybridMode(true);
        else                    RAVEN->setHybridMode(false);
    }
    else if(b == zoomInButton)
    {
        MacKeyPress::pressCombo(zoomInKeyCode);
    }
    else if(b == zoomOutButton)
    {
        MacKeyPress::pressCombo(zoomOutKeyCode);
    }
    //trimKeyCode, selKeyCode, grabKeyCode, zoomKeyCode, scrubKeyCode, pencilKeyCode
    else if(b->getName() == "nudgePlusButton")
    {
        MacKeyPress::pressCombo(nudgePlusKeyCode);
    }
    else if(b->getName() == "nudgeMinusButton")
    {
        MacKeyPress::pressCombo(nudgeMinusKeyCode);
    }
    else if(b == clipLoopButton)
    {
        MacKeyPress::pressCombo(clipLoopKeyCode);
    }
    else if(b == duplicateButton)
    {
        MacKeyPress::pressCombo(duplicateKeyCode);
    }
    else if(b->getName() == "redoButton")
    {
        MacKeyPress::pressCombo(*((RavenButton*)b)->getKeyCode());
    }
    
    NscGuiElement * element = dynamic_cast<NscGuiElement *>(b);
	if (!element)
	{
		// try our temporary workaround hack.
		int ptr = b->getProperties()[kNscGuiIdentifier];
		element = (NscGuiElement *) ptr;
		if (!element)
		{
            return;
			DBG("Switches must inherit and set up NscGuiElement");
			jassertfalse;
		}
	}
	if (element)
	{
        switchClicked(element, true);
	}
    
    if(b == flipMode)
    {
        bool isFlipMode = flipMode->getProperties().getWithDefault("isFlipMode", false);
        if(isFlipMode)
        {
            flipMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/SendChannel/channelOn.png", "TOOLBAR/FUNCTIONMODULE/SendChannel/channelOff.png");
            flipMode->getProperties().set("isFlipMode", false);
        }
        else
        {
            flipMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/SendChannel/sendOn.png", "TOOLBAR/FUNCTIONMODULE/SendChannel/sendOff.png");
            flipMode->getProperties().set("isFlipMode", true);
        }
        
        flipMode2->triggerClick();
        flipMode3->triggerClick();
        if(!RAVEN_24) flipMode4->triggerClick();
    }
    
    if(b == panMode)
    {
        printf("firing pan mode BUTTON\n");
        panMode2->triggerClick();
        panMode3->triggerClick();
        if(!RAVEN_24) panMode4->triggerClick();
        
        if(inFlipMode)
        {
            bool tempIsLeftPanMode = !isLeftPanModeWhenFlipped;
            if(tempIsLeftPanMode == true)
                panMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Ron.png", "TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Roff.png");
            else
                panMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Lon.png", "TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Loff.png");
            isLeftPanModeWhenFlipped = tempIsLeftPanMode;
        }
        else
        {
            bool tempIsLeftPanMode = !isLeftPanMode;
            if(tempIsLeftPanMode == true)
            {
                panMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Ron.png", "TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Roff.png");
                sendActionMessage("right pan mode");//highlight's the "r" on the mixer's pan knob
            }
            else
            {
                panMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Lon.png", "TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Loff.png");
                sendActionMessage("left pan mode");//de-highlight's the "r" on the mixer's pan knob
            }
            isLeftPanMode = tempIsLeftPanMode;
        }
    }
}

void RavenFunctionsManager::swipeBankLeftThreeFingers()
{
    if(RAVEN->getMixer()->getNumberOfHybridModeBanks() == numBanks)
    {
        trackBankCmp->getBankLeftButton()->triggerClick();
        trackBankCmp->getBankLeftNSCButton()->triggerClick();
    }
    else
    {
        trackBankCmp->getBankLeftButton()->triggerClick();
        int numberCanBank = RAVEN->getMixer()->numberCanBankLeftThreeFinger();
        printf("number can bank left THREE FINGER: %d\n", numberCanBank);
        for(int i = 0;i<numberCanBank;i++)
        {
            if(RAVEN->getMixer()->canTrackLeft())
                trackBankCmp->getTrackLeftButton()->triggerClick();
        }
    }
}

void RavenFunctionsManager::swipeBankRightThreeFingers()
{
    if(RAVEN->getMixer()->getNumberOfHybridModeBanks() == numBanks)
    {
        trackBankCmp->getBankRightButton()->triggerClick();
        trackBankCmp->getBankRightNSCButton()->triggerClick();
    }
    else
    {
        trackBankCmp->getBankRightButton()->triggerClick();
        int numberCanBank = RAVEN->getMixer()->numberCanBankRightThreeFinger();
        printf("number can bank right THREE FINGER: %d\n", numberCanBank);
        for(int i = 0;i<numberCanBank;i++)
        {
            if(RAVEN->getMixer()->canTrackRight())
                trackBankCmp->getTrackRightButton()->triggerClick();
        }
    }
}

void RavenFunctionsManager::swipeBankLeftTwoFingers()
{
    trackBankCmp->getBankLeftButton()->triggerClick();
    int numberCanBank = RAVEN->getMixer()->numberCanBankLeft();
    printf("number can bank left: %d\n", numberCanBank);
    for(int i = 0;i<numberCanBank;i++)
    {
        if(RAVEN->getMixer()->canTrackLeft())
            trackBankCmp->getTrackLeftButton()->triggerClick();
    }
}
void RavenFunctionsManager::swipeBankRightTwoFingers()
{
    trackBankCmp->getBankRightButton()->triggerClick();
    int numberCanBank = RAVEN->getMixer()->numberCanBankRight();
    printf("number can bank right: %d\n", numberCanBank);
    for(int i = 0;i<numberCanBank;i++)
    {
        if(RAVEN->getMixer()->canTrackRight())
            trackBankCmp->getTrackRightButton()->triggerClick();
    }
}

void RavenFunctionsManager::timerCallback(int timerID)
{
    //keeps our insert button matching the HUI insert win button state
    if(timerID == FUNCTIONS_INSWIN_BUTTON_TIMER)
    {
        
        lastWinButton->setToggleState(insWinButton->getToggleState(), false);
    }
    // used when moving modules in customize mode:
    else if(timerID == ADD_MOVER_TIMER)
    {
        stopTimer(ADD_MOVER_TIMER);
        if((modulesToBeHiddenQueue.size() == 0) || (proxiesToBeRemovedQueue.size() != 0) || (movingModules.size() == 0))
        {
            modulesToBeHiddenQueue.clear();
            proxiesToBeRemovedQueue.clear();
            RAVEN_TOUCH_UNLOCK
            return;
        }
        if(!moverComponent->isOnDesktop()) moverComponent->addMoverToDesktop();
        if(modulesToBeHiddenQueue[0]->isVisible()) startTimer(HIDE_MOVING_MODULE_TIMER, proxyTransitionMS);
    }
    else if(timerID == HIDE_MOVING_MODULE_TIMER)
    {
        stopTimer(HIDE_MOVING_MODULE_TIMER);
        if((modulesToBeHiddenQueue.size() == 0) || (proxiesToBeRemovedQueue.size() != 0) || (movingModules.size() == 0))
        {
            modulesToBeHiddenQueue.clear();
            proxiesToBeRemovedQueue.clear();
            RAVEN_TOUCH_UNLOCK
            return;
        }
        modulesToBeHiddenQueue[0]->setVisible(false);
        modulesToBeHiddenQueue.remove(0);
        RAVEN_TOUCH_UNLOCK
    }
    else if(timerID == REMOVE_MODULE_TIMER)
    {
        stopTimer(REMOVE_MODULE_TIMER);
        if( (modulesToBeHiddenQueue.size() != 0) || (proxiesToBeRemovedQueue.size() == 0))
        {
            modulesToBeHiddenQueue.clear();
            proxiesToBeRemovedQueue.clear();
            moverComponent->removeFromDesktop();
            RAVEN_TOUCH_UNLOCK
            return;
        }
        moverComponent->removeChildComponent(proxiesToBeRemovedQueue[0]);
        proxiesToBeRemovedQueue.remove(0);
        if(proxiesToBeRemovedQueue.size() == 0 && movingModules.size() == 0) moverComponent->removeFromDesktop();
        RAVEN_TOUCH_UNLOCK
    }
    // used when loading layouts and animating:
    else if(timerID == START_ANIMATION_TIMER)
    {
        for(int i = 0; i < animateModuleCount; i++)
        {
            RavenProxyComponent *proxy = proxyComponents[i];
            if(proxy->needsAnimating)
            {
                Rectangle<int> moveToRect = proxy->moveToRect;
                float finalAlpha = proxy->finalAlpha;
                proxy->realComponent->setVisible(false);
                Desktop::getInstance().getAnimator().animateComponent (proxy, moveToRect, finalAlpha, animationMS, false, 1.0, 1.0);
                proxy->needsAnimating = false;
            }
        }
        stopTimer(START_ANIMATION_TIMER);
        RAVEN_TOUCH_UNLOCK
    }
    else if(timerID == REMOVE_MOVER_TIMER)
    {
        stopTimer(REMOVE_MOVER_TIMER);
        moverComponent->removeAllChildren();
        moverComponent->removeFromDesktop();
        RAVEN_TOUCH_UNLOCK
    }
}

void RavenFunctionsManager::changeListenerCallback (ChangeBroadcaster *source)
{
    if(source == &(Desktop::getInstance().getAnimator())) //if the animator sent a change message (start or stop animation)
    {
        if(!Desktop::getInstance().getAnimator().isAnimating()) //if animation is complete
        {
            //add modules to new parent (functions bar or floating essentials)
            HashMap<Component*, Component*>::Iterator i(movePaletteMap);
            while (i.next())
            {
                i.getValue()->addChildComponent(i.getKey());
            }
            //set their positions relative to parent
            HashMap<Component*, savedFunctionsModuleInfo>::Iterator j(movePosMap);
            //int p = 0;
            while (j.next())
            {
                savedFunctionsModuleInfo info = j.getValue();
                j.getKey()->setTopLeftPosition(info.topLeftX, info.topLeftY);
                j.getKey()->setVisible(true);
            }
            floatingEssentials->resizeFloatingPalette();
            startTimer(REMOVE_MOVER_TIMER, proxyTransitionMS);
        }
        else //is starting animation, lock touches
        {
            RAVEN_TOUCH_LOCK
        }
    }
}

