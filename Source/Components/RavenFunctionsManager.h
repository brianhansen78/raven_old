//
//  RavenFunctionsManager.h
//  Raven
//
//  Created by Brian Hansen on 5/22/13.
//
//

#ifndef Raven_RavenFunctionsManager_h
#define Raven_RavenFunctionsManager_h

#include "RavenConfig.h"
#include "RavenFunctionsContainer.h"
#include "RavenFunctionsComponent.h"
#include "RavenTouchComponent.h"
#include "RavenWindowComponent.h"
#include "NscGuiElement.h"
#include "RavenButton.h"
#include "RavenTextDisplay.h"
#include "RavenHotKeysComponent.h"
#include "RavenRollComponent.h"
#include "RavenTransportComponent.h"
#include "RavenMemoryComponent.h"
#include "RavenEditMixComponent.h"
#include "RavenPunchComponent.h"
#include "RavenInputGroupSuspendComponent.h"
#include "RavenUtilitiesComponent.h"
#include "RavenModifiersComponent.h"
#include "RavenTrackBankComponent.h"
#include "RavenEditComponent.h"
#include "RavenCounterFunctionsComponent.h"
#include "RavenSaveLayoutComponent.h"
#include "RavenNavPadComponent.h"
#include "RavenFloatingNavPadComponent.h"
#include "RavenHotKeyProgrammingComponent.h"
#include "RavenFloatingEssentialsComponent.h"
#include "RavenWindow.h"


#define NUM_HOTKEYS 8

#define FUNCTIONS_INSWIN_BUTTON_TIMER   1
#define REMOVE_MOVER_TIMER              2
#define ADD_MOVER_TIMER                 3
#define HIDE_MOVING_MODULE_TIMER        4
#define START_ANIMATION_TIMER           5
#define REMOVE_MODULE_TIMER             6

struct savedFunctionsModuleInfo
{
    String paletteName;
    String moduleName;
    int topLeftX;
    int topLeftY;
    bool isOnEssentialsPalette;
    bool isHotKey;
    String hotKeyLabel[NUM_HOTKEYS];
    String hotkeyCode[NUM_HOTKEYS];
};

class RavenMoverComponent : public Component
{
public:
    RavenMoverComponent()
    {
        setBounds(0, 0, screenWidth, screenHeight);
    }
    void paint(Graphics &g)
    {
        g.fillAll(Colours::blue.withAlpha(0.0f));
    }
    void addMoverToDesktop()
    {
        setBounds(0, 0, screenWidth, screenHeight);
        setVisible(true);
        addToDesktop(0);
        Desktop::getInstance().ravenSetWindowOptions(this, RAVEN_MOVER_LEVEL);
    }
};

class RavenProxyComponent : public RavenWindowComponent
{
public:
    RavenProxyComponent() : finalAlpha(1.0), needsAnimating(false), proxyImage(0)
    {
        setSize(1, 1); //can't be 0 size, proxy window won't initialize
        setVisible(true);
    }
    void paint(Graphics &g)
    {
        if(proxyImage.isValid() && isVisible())
        {
            g.drawImageAt(proxyImage, 0, 0);
            //g.fillAll(Colours::blue.withAlpha(0.5f));
        }
        else g.fillAll(Colours::white.withAlpha(0.0f));
    }
    
    void setImage(Image _proxyImage){proxyImage = _proxyImage;}
    
    Rectangle<int> moveToRect;
    float finalAlpha;
    bool needsAnimating;
    Component *realComponent;
    
private:
    Image proxyImage;
};


class RavenFunctionsManager : public RavenFunctionsContainer, public MultiTimer, public ActionBroadcaster, public ChangeListener

{
    
public:
    
    RavenFunctionsManager();
    

    void setRackContainerButtonListener(Button::Listener *listener)
    {
        toolBarModeButton->addListener(listener);
        toolBarCustomButton->addListener(listener);
        targetDAWButton->addListener(listener);
        exitButton->addListener(listener);
        counterFuncCmp->setCounterButtonListener(listener);
        //drumPadButton->addListener(listener);
        floatingNavpadButton->addListener(listener);
#if RAVEN_ESSENTIALS_PALETTE
        essentialsButton->addListener(listener);
#endif
        fineButton->addListener(listener);
        fineFaderButton->addListener(listener);
        moveButton->addListener(listener);
        saveLayoutCmp->setPresetButtonListener(listener);
        flipMode->addListener(listener);
        
        addBarButton->addListener(listener);
        removeBarButton->addListener(listener);
    }

    void setRackContainerActionListener(ActionListener *listener)
    {
        addActionListener(listener);
        saveLayoutCmp->addActionListener(listener);
        trackBankCmp->addActionListener(listener);
    }
    
    RavenTrackBankComponent *getTrackBankComponent()
    {
        return trackBankCmp;
    }

    void setHotKeyPanel(RavenHotKeyProgrammingComponent *hotPanel)
    {
        hotComp->setHotKeyPanel(hotPanel);
        hotComp2->setHotKeyPanel(hotPanel);
    }
    bool isHotKeyProgramminMode()
    {
        return (hotComp->isProgramMode() || hotComp2->isProgramMode());
    }
    
    bool getMoveToggleState(){return moveButton->getToggleState();}
    
    bool checkMoveToggleTouch(TUIO::TuioCursor *tcur){return RavenTouchComponent::componentTouched(moveButton, tcur, false);}
    
    bool checkEditToolTouch(int touchX, int touchY) { return RavenTouchComponent::componentTouched(editToolCmp, touchX, touchY); }
    RavenButton* getSmartToolButton() {return editToolCmp->getSmartToolButton(); }
    EditToolComponent* getEditToolComponent() {return editToolCmp; }

    bool checkNavPadTouch(int touchX, int touchY){ return navPadCmp->checkPadTouch(touchX, touchY); }
    void navPadTouch(TUIO::TuioCursor *tcur, bool isTwoFinger) { navPadCmp->updatePadTouch(tcur, isTwoFinger); }
    void navPadTouch(int xPos, int yPos, float xSpeed, float ySpeed, bool isTwoFinger, int numAvg = RAVEN_NAVPAD_NUM_AVG_DEFAULT)
    {
        navPadCmp->updatePadTouch(xPos, yPos, xSpeed, ySpeed, isTwoFinger, numAvg);
    }
    
    void enableMoveModules(bool enable);
    void enableHybridMode(bool enable);
    
    void setNavPadLink(NscLink *_link){navPadCmp->setNscLink(_link);}
    void setLink(NscLink *_link)
    {
        setNscLink(_link);
        navPadCmp->setNscLink(_link);
    }
    
    RavenNavPadComponent *getNavPadComponent() {return navPadCmp; }
    void setFloatingEssentials(RavenFloatingEssentialsComponent *cmp)
    {
        floatingEssentials = cmp;
    }
        
    //Show Last Win button also used in Inserts Palette, so just point to it for use here
    void setInsertWindowButton(RavenNSCButton *b) { insWinButton = b; }
    
    void timerCallback(int timerID);
    void changeListenerCallback (ChangeBroadcaster *source);
        
    void enterToolbarMode();
    void exitToolbarMode();
    
    void enterCustomMode();
    void exitCustomMode();

    //    bool isToolbarMode() { return toolbarMode; }
    bool isCustomMode() { return customMode; }
    //    int getSinglePanelHeight(){ return singlePanelHeight; }
    //int getCustomPanelHeight(){ return customPanelHeight; }
    
    bool checkModuleTouch(TUIO::TuioCursor *tcur, int touchid);
    void moveModule(int screenX, int screenY, int touchid);
    void endModuleMove(int touchid);

    void buttonClicked (Button* b);
    void buttonStateChanged (Button*){}
    
    Array<RavenHotKeysComponent*>& getHotKeyComponents(){return hotKeyComponents;}
    
    RavenNSCButton* getMasterFlipModeButton()   { return flipMode; }
    RavenNSCButton* getMasterPanModeButton()    { return panMode;  }
    
    void trackLeft(){trackBankCmp->getTrackLeftButton()->triggerClick();}
    void trackRight(){trackBankCmp->getTrackRightButton()->triggerClick();}
    void bankLeft(){trackBankCmp->getBankLeftButton()->triggerClick();}
    void bankRight(){trackBankCmp->getBankRightButton()->triggerClick();}
    
    RavenSaveLayoutComponent* getSaveLayoutComponent() { return saveLayoutCmp; }
    
    RavenButton* getToolbarCustomButton() { return toolBarCustomButton; }
    
    bool getTrackOrBankButtonTouched(TUIO::TuioCursor *tcur)
    {
        return trackBankCmp->getTrackOrBankButtonTouched(tcur);
    }
    
    void enterFlipMode()
    {
        inFlipMode = true;
        isLeftPanModeWhenFlipped = isLeftPanMode;
        //in HUI, whenever flipping back and forth, flipped mode takes on whatever pan mode the mixer is in.
    }
    
    void exitFlipMode()
    {
        inFlipMode = false;
        //reset the pan mode button to whatever state the mixer's last pan mode was in (even if it has been changed during the flip).
        if(isLeftPanMode == true)
            panMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Ron.png", "TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Roff.png");
        else
            panMode->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Lon.png", "TOOLBAR/FUNCTIONMODULE/PanLR/PanLR-Loff.png");
    }
    
    bool isFlipMode(){ return inFlipMode;}
    
    bool positionModule(Component* curMovingModule, RavenFunctionsComponent* parentComponent);
    
    void exitScrubShuttle()
    {
        navPadCmp->exitScrubShuttle();
    }
    
    void restartScrubOrShuttleMode()
    {
        navPadCmp->restartScrubOrShuttleMode();
    }
    
    void unlatchModifiers(){ modComp->unlatchModifiers(); }
    
    bool getCommandButtonState(){ return modComp->getCommandButtonState(); }
    bool getShiftButtonState(){ return modComp->getShiftButtonState(); }
    bool getOptionButtonState(){ return modComp->getOptionButtonState(); }
    bool getControlButtonState(){ return modComp->getControlButtonState(); }
    
    bool checkModifierTouch(int touchX, int touchY) {return RavenTouchComponent::componentTouched(modComp, touchX, touchY);}
    
    void enterHybridMode();
    void exitHybridMode();

    void setSavedFunctionsModuleLayout(Array<savedFunctionsModuleInfo> funcPos) {functionsModulePositions = funcPos;}
    bool repositionFunctionsModule(savedFunctionsModuleInfo saveInfo, Component* module, RavenWindowComponent* palette);
    void loadSavedFunctionsLayout();
    
    void addFunctionsComponent(RavenFunctionsComponent* fc) {functionsComponents.add(fc);}
    Array<RavenFunctionsComponent*> getFunctionsComponents() {return functionsComponents; }
    void setDefaultLayout();
    bool checkFunctionsTouched(int touchX, int touchY);    
    bool checkFunctionsButtonTouched(TUIO::TuioCursor *tcur, bool ignoreIsVisible = false);
    Button* checkFunctionsHoldButtonTouched(TUIO::TuioCursor *tcur);
    
    void releaseFunctionsHotKeysTouch(TUIO::TuioCursor *tcur);
    
    int callback(ENSCEventType type, int subType, int bank, int which, float val)
    {
        for(int i = 0; i < functionsComponents.size(); i++)
        {
            functionsComponents[i]->callback(type, subType, bank, which, val);
        }
        this->NscGuiContainer::callback(type, subType, bank, which, val);//added by josh
        return 0;
    }
    int callback(ENSCEventType type, int subType, int bank, int which, const std::string & data)
    {
        for(int i = 0; i < functionsComponents.size(); i++)
        {
            functionsComponents[i]->callback(type, subType, bank, which, data.c_str());
        }
        this->NscGuiContainer::callback(type, subType, bank, which, data.c_str());//added by josh to fix inserts palette not working
        return 0;
    }
    
    RavenButton* getEssentialsButton(){ return essentialsButton;}
    
    void swipeBankLeftThreeFingers();
    void swipeBankRightThreeFingers();
    void swipeBankLeftTwoFingers();
    void swipeBankRightTwoFingers();
    
    void synchronizeFloatingPaletteButtons();
    
    bool restrictModulePosition(Component* curMovingModule, Component* palette);
    
    RavenButton *getFloatingNavpadButton(){ return floatingNavpadButton;}
    
    void resetToOriginalLocation(Component* curMovingModule);
    
private:
    
    Array<RavenFunctionsComponent*> functionsComponents;
    
    Array<savedFunctionsModuleInfo> functionsModulePositions;
    
    Image bottomPanelImg, singlePanelImg, customPanelImg, railImg;
    
    int singlePanelHeight;//, customPanelHeight;
    
    bool toolbarMode, customMode, inFlipMode, isLeftPanModeWhenFlipped, isLeftPanMode;
    
    //int numBars; //number of toolbar bars including the bottom one visible when not in toolbar mode
    int numMixModeBars, numDAWModeBars; //number of visible toolbar panels
    
    RavenFloatingEssentialsComponent *floatingEssentials; //a pointer to the one owned by rack container
    
    Array<Component*> movingModules;
    Array<Component*> modulesToBeHiddenQueue;
    Array<Component*> proxiesToBeRemovedQueue;
    
    std::vector<int> zoomInKeyCode, zoomOutKeyCode, clipLoopKeyCode, duplicateKeyCode;
    std::vector<int> nudgePlusKeyCode, nudgeMinusKeyCode;
    
    HashMap<Component*, Component*> movePaletteMap;
    HashMap<Component*, savedFunctionsModuleInfo> movePosMap;
    
    OwnedArray<RavenProxyComponent> proxyComponents;
    
    ScopedPointer<RavenMoverComponent> moverComponent;
    int animateModuleCount;
    
    //**************************************************************
    //*** below are all functions modules appearing on toolbars ****
    
    //flip mode
    //the first one is sometimes visible/touchable for debugging so the owned touch button array will take care of it's memory
    //use #ifdef Raven_24 to handle this
    //RavenNSCButton *flipMode; //bank 1, but controls all banks with additional button listener
#if RAVEN_24
    RavenNSCButton* flipMode;
#else
    ScopedPointer<RavenNSCButton> flipMode;
#endif
    ScopedPointer<RavenNSCButton> flipMode2;
    ScopedPointer<RavenNSCButton> flipMode3;
    ScopedPointer<RavenNSCButton> flipMode4;
    
    //pan mode
    //the first one is sometimes visible/touchable for debugging so the owned touch button array will take care of it's memory
    RavenNSCButton *panMode; //bank 1, but controls all banks with additional button listener
    ScopedPointer<RavenNSCButton> panMode2;
    ScopedPointer<RavenNSCButton> panMode3;
    ScopedPointer<RavenNSCButton> panMode4;
    
    RavenButton *toolBarModeButton;
    RavenButton *toolBarCustomButton;
    RavenButton *targetDAWButton;
    RavenButton *exitButton;
    RavenButton *lastWinButton;
    RavenNSCButton *insWinButton;
    RavenButton* addBarButton;
    RavenButton* removeBarButton;
    
    ScopedPointer<RavenModifiersComponent> modComp;
    
    ScopedPointer<RavenHotKeysComponent> hotComp;
    ScopedPointer<RavenHotKeysComponent> hotComp2;
    //Array to hold all hot key components.... hotComp and hotComp2 will be deprecated
    //Note this is not an owned array since hot key components are scoped pointers
    Array<RavenHotKeysComponent*> hotKeyComponents;
    
    ScopedPointer<RavenCounterFunctionsComponent> counterFuncCmp;
    ScopedPointer<RavenEditMixComponent> editMixCmp;
    ScopedPointer<RavenNavPadComponent> navPadCmp;
    
    ScopedPointer<EditToolComponent> editToolCmp;
    ScopedPointer<CopyPasteComponent> editCopyPasteCmp;
    ScopedPointer<QuantizationComponent> editQuantCmp;
    ScopedPointer<NudgeCycleComponent> editNudgeCmp;
    ScopedPointer<DropReplaceComponent> editDropReplaceCmp;
    
    ScopedPointer<RavenUtilitiesComponent> utilCmp;
    ScopedPointer<RavenPunchComponent> punchCmp;
    ScopedPointer<RavenInputGroupSuspendComponent> igsCmp;
    ScopedPointer<RavenRollComponent> rollCmp;
    ScopedPointer<RavenTransportComponent> transCmp;
    ScopedPointer<RavenMemoryComponent> memComp;
    
    ScopedPointer<RavenSaveLayoutComponent> saveLayoutCmp;
    ScopedPointer<RavenTrackBankComponent> trackBankCmp;
    
    //RavenButton *drumPadButton;
    RavenButton *essentialsButton;
    
    RavenNSCButton *clickButton;
    RavenButton *moveButton;
    RavenButton *fineButton;
    RavenButton *fineFaderButton;
    RavenButton *floatingNavpadButton;
    RavenButton *hybridModeButton;
    
    RavenButton *zoomInButton;
    RavenButton *zoomOutButton;
    RavenButton *clipLoopButton;
    RavenButton *duplicateButton;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenFunctionsManager)
};


#endif
