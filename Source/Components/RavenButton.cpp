

#include "RavenButton.h"
#include "RavenGUI.h"

RavenNSCButton::RavenNSCButton(NscGuiContainer *owner) : NscGuiElement(owner), nscValue(0.0)
{
    //File offImgFile(File::getCurrentWorkingDirectory().getChildFile("Raven_GUI_Images/test_button_on.png"));
    //File onImgFile(File::getCurrentWorkingDirectory().getChildFile("Raven_GUI_Images/test_button_off.png"));
    
    File offImgFile(String(GUI_PATH) + "test_button_on.png");
    File onImgFile(String(GUI_PATH) + "test_button_off.png");

    
    //std::cout << "off file = " << offImgFile.getFullPathName() << std::endl;

    offImage = ImageCache::getFromFile(offImgFile);
    onImage = ImageCache::getFromFile(onImgFile);
    
    setImages(true, false, true, onImage, 1.0f, Colour(0x0), Image(), 1.0f, Colour(0x0), offImage, 1.0f, Colour(0x0));
    
}

RavenNSCButton::~RavenNSCButton()
{
    
}

void RavenNSCButton::updateNscValue(float value)
{
    //don't allow keyboard to update modifiers because they get stuck in the on position
    int tag = getTag();
    
    if(tag == eNSCGSUtilitiesSaveSwitch || tag == eNSCGSUtilitiesUndoSwitch || tag == eNSCGSAutoStatus) return;
    
    //if(tag == eNSCGSAssignmentChannelLeftSwitch || tag == eNSCGSAssignmentChannelRightSwitch) return;
    
    if (value!=0)
		setToggleState(true, false);
	else
		setToggleState(false,false);
    
    nscValue = value;
}

/////////////////////
//RavenButton////////
//////////////////////
RavenButton::RavenButton() : ImageButton(String::empty), enabled(true)
{
    //File offImgFile(File::getCurrentWorkingDirectory().getChildFile("Raven_GUI_Images/test_button_on.png"));
    //File onImgFile(File::getCurrentWorkingDirectory().getChildFile("Raven_GUI_Images/test_button_off.png"));
    
    File offImgFile(String(GUI_PATH) + "testButton-on.png");
    File onImgFile(String(GUI_PATH) + "testButton-off.png");
    
    
    //std::cout << "off file = " << offImgFile.getFullPathName() << std::endl;
    
    offImage = ImageCache::getFromFile(offImgFile);
    onImage = ImageCache::getFromFile(onImgFile);
    
    setImages(true, false, true, onImage, 1.0f, Colour(0x0), Image(), 1.0f, Colour(0x0), offImage, 1.0f, Colour(0x0));
    
}

void RavenButton::setImagePaths(String onImgPath, String offImgPath)
{
    File offImgFile(String(GUI_PATH) +onImgPath);
    File onImgFile(String(GUI_PATH) + offImgPath);
    
    onImage = ImageCache::getFromFile(onImgFile);
    offImage = ImageCache::getFromFile(offImgFile);
    
    
    if(offImgPath.contains("TrackFocus-Logo_ON"))   setImages(true, false, true, onImage, 0.0f, Colour(0x0), Image(), 1.0f, Colour(0x0), offImage, 1.0f, Colour(0x0));
    else                                            setImages(true, false, true, onImage, 1.0f, Colour(0x0), Image(), 1.0f, Colour(0x0), offImage, 1.0f, Colour(0x0));
    
    
    
}

void RavenButton::trim(int top, int bottom, int left, int right)
{
        Rectangle<int> clipRect(left, top, onImage.getWidth()-left-right, onImage.getHeight()-top-bottom);
        onImage = onImage.getClippedImage(clipRect);
        offImage = offImage.getClippedImage(clipRect);
        setImages(true, false, true, onImage, 1.0f, Colour(0x0), Image(), 1.0f, Colour(0x0), offImage, 1.0f, Colour(0x0));
}

void RavenButton::rescale(float ratio)
{
    int newWidth = ratio*getWidth();
    int newHeight = ratio*getHeight();
    onImage = onImage.rescaled(newWidth, newHeight, Graphics::highResamplingQuality);
    offImage = offImage.rescaled(newWidth, newHeight, Graphics::highResamplingQuality);
    setImages(true, false, true, onImage, 1.0f, Colour(0x0), Image(), 1.0f, Colour(0x0), offImage, 1.0f, Colour(0x0));
}

/*
void RavenButton::setImagePaths(String onImgPath, String offImgPath)
{
    File offImgFile(String(GUI_PATH) +onImgPath);
    File onImgFile(String(GUI_PATH) + offImgPath);
    
    offImage = ImageCache::getFromFile(offImgFile);
    onImage = ImageCache::getFromFile(onImgFile);
    
    setImages(true, false, true, onImage, 1.0f, Colour(0x0), Image(), 1.0f, Colour(0x0), offImage, 1.0f, Colour(0x0));
}
*/