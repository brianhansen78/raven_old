
#ifndef Raven_RavenMemoryComponent_h
#define Raven_RavenMemoryComponent_h

#include "RavenButton.h"
#include "RavenFunctionsComponent.h"
#include "RavenTouchComponent.h"

class RavenMemoryComponent : public RavenTouchComponent, public Button::Listener
{
public:
    RavenMemoryComponent(RavenFunctionsContainer *owner)
    {
        
        RavenNSCButton *memButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_0(memButton);
        memButton->setTopLeftPosition(0, 0);
        memButton->setClickingTogglesState(true);
        memButton->addListener(owner); 
        memButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Memory/images/memory-on_01.png", "TOOLBAR/FUNCTIONMODULE/Memory/images/memory-off_01.png");
        //memButton->setKeyboard(true);
        memButton->trim(0, 65, 0, 0);
        memButton->setTag(eNSCGSPTWindow_MemoryLocations); //memButton->setTag(eNSCKeyboard_Enter);
        ravenNSCTouchButtons.add(memButton);
        addAndMakeVisible(memButton);
        
        addButton = new RavenButton();
        RAVEN_BUTTON_0(addButton);
        addButton->setTopLeftPosition(6, 42);
        addButton->addListener(this);
        addButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Memory/images/memory_add_On.png", "TOOLBAR/FUNCTIONMODULE/Memory/images/memory_add_Off.png");
        ravenTouchButtons.add(addButton);
        addAndMakeVisible(addButton);
        
        int xPos = memButton->getWidth();
        
        int yPos = 0;
        
        for(int i = 0; i < 16; i++)
        {
            if(i == 8)
            {
                xPos = memButton->getWidth();
                yPos = 45;
            }
            
            String onFile, offFile;
            if(i < 8)
            {
                onFile = "TOOLBAR/FUNCTIONMODULE/Memory/images/memory-on_0" + String(i+2) + ".png";
                offFile = "TOOLBAR/FUNCTIONMODULE/Memory/images/memory-off_0" + String(i+2) + ".png";
            }
            else
            {
                onFile = "TOOLBAR/FUNCTIONMODULE/Memory/images/memory-on_" + String(i+2) + ".png";
                offFile = "TOOLBAR/FUNCTIONMODULE/Memory/images/memory-off_" + String(i+2) + ".png";
            }
            
            RavenButton *memNumButton = new RavenButton();
            RAVEN_BUTTON_0(memNumButton);
            memNumButton->setTopLeftPosition(xPos, yPos);
            memNumButton->addListener(this);
            memNumButton->setImagePaths(onFile, offFile);
            ravenTouchButtons.add(memNumButton);
            addAndMakeVisible(memNumButton);
            xPos += memNumButton->getWidth();
            
        }
        
        decimalButton = new RavenNSCButton(owner);
        decimalButton->addListener(owner);
        decimalButton->setChannelIndex(kNscGuiKeyboardIdentifier);
        decimalButton->setTag(eNSCKeyboard_KeypadDecimalPoint);

        for(int i = 0; i < 10; i++)
        {
            RavenNSCButton *invisMemButton = new RavenNSCButton(owner);
            invisMemButton->addListener(owner);
            invisMemButton->setChannelIndex(kNscGuiKeyboardIdentifier);
            invisMemButton->setTag(eNSCKeyboard_Keypad0+i);
            memButtons.add(invisMemButton);
        }
        
        setSize(400, 110);
        
    }
    
    void buttonStateChanged (Button*)
    {
        
    }
    void buttonClicked (Button* b)
    {
        
        if(b == addButton)
        {
            enterButton->triggerClick();
            return;
        }
        
        int memNum = 1;
        for(int i = 1; i < ravenTouchButtons.size(); i++) if(b == ravenTouchButtons[i]) memNum = i;
        
        decimalButton->triggerClick();
        
        if(memNum < 10)
        {
            memButtons[memNum]->triggerClick();
        }
        else if(memNum == 10)
        {
            memButtons[1]->triggerClick();
            memButtons[0]->triggerClick();
        }
        else if(memNum == 11)
        {
            memButtons[1]->triggerClick();
            memButtons[1]->triggerClick();
        }
        else if(memNum == 12)
        {
            memButtons[1]->triggerClick();
            memButtons[2]->triggerClick();
        }
        else if(memNum == 13)
        {
            memButtons[1]->triggerClick();
            memButtons[3]->triggerClick();
        }
        else if(memNum == 14)
        {
            memButtons[1]->triggerClick();
            memButtons[4]->triggerClick();
        }
        else if(memNum == 15)
        {
            memButtons[1]->triggerClick();
            memButtons[5]->triggerClick();
        }
        else if(memNum == 16)
        {
            memButtons[1]->triggerClick();
            memButtons[6]->triggerClick();
        }
        
        decimalButton->triggerClick();
        
    }
    
    void setEnterButton(RavenNSCButton *b){enterButton = b;}
    
    
private:
    
    OwnedArray<RavenNSCButton> memButtons;
    ScopedPointer<RavenNSCButton> decimalButton;
    RavenNSCButton *enterButton; //must = enter button from utilities component..use set enter (enter = add mem location)
    RavenButton *addButton;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenMemoryComponent)
    
};


#endif
