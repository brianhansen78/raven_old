
#ifndef Raven_RavenTrackBankComponent_h
#define Raven_RavenTrackBankComponent_h

#include "RavenButton.h"
#include "RavenFunctionsComponent.h"
#include "RavenTouchComponent.h"
#include "RavenConfig.h"

class RavenTrackBankComponent : public RavenTouchComponent, public Button::Listener, public ActionBroadcaster, public Timer
{
public:
    RavenTrackBankComponent(RavenFunctionsContainer *owner) : triggerQueu(0)
    {
        
        //invisible
        /*
        nscSwitchButton = new RavenNSCButton(owner);
        nscSwitchButton->setClickingTogglesState(true);
        nscSwitchButton->addListener(owner);
        nscSwitchButton->setTag(eNSCGSFlipSwitch);
         */
        
        switchButton = new RavenButton();
        RAVEN_BUTTON_0(switchButton);
        switchButton->setClickingTogglesState(true);
        switchButton->setTopLeftPosition(0, 0);
        switchButton->addListener(this);
        if(RAVEN_24)
        {
            switchButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Navigation/navigation24/images/navigation24-on_01.png", "TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-off_01.png");
        }
        else
        {
            switchButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Navigation/navigation32/navigation32-off_01.png", "TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-off_01.png");
        }
        ravenTouchButtons.add(switchButton);
        addAndMakeVisible(switchButton);
        
        bankleftNSCButton = new RavenNSCButton(owner);
        bankleftNSCButton->addListener(owner);
        bankleftNSCButton->setTag(eNSCGSAssignmentBankLeftSwitch);
        
        bankleftButton = new RavenButton();
        RAVEN_BUTTON_1(bankleftButton);
        bankleftButton->setTopLeftPosition(switchButton->getWidth(), 0);
        bankleftButton->addListener(this);
        if(RAVEN_24)
        {
            bankleftButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-on_02.png", "TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-off_02.png");
        }
        else
        {
            bankleftButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-on_02.png", "TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-off_02.png");
        }
        addAndMakeVisible(bankleftButton);
        
        bankrightNSCButton = new RavenNSCButton(owner);
        bankrightNSCButton->addListener(owner);
        bankrightNSCButton->setTag(eNSCGSAssignmentBankRightSwitch);
        
        bankrightButton = new RavenButton();
        bankrightButton->setTopLeftPosition(bankleftButton->getX()+bankleftButton->getWidth(), 0);
        bankrightButton->addListener(this);
        bankrightButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-on_03.png", "TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-off_03.png");
        addAndMakeVisible(bankrightButton);
        
        trackleftNSCButton = new RavenNSCButton(owner);
        trackleftNSCButton->addListener(owner);
        trackleftNSCButton->setTag(eNSCGSAssignmentChannelLeftSwitch);
        
        trackleftButton = new RavenButton();
        RAVEN_BUTTON_1(trackleftButton);
        trackleftButton->setTopLeftPosition(switchButton->getWidth(), bankleftButton->getHeight());
        trackleftButton->addListener(owner);
        trackleftButton->addListener(this);//-josh to allow for automation clearing
        trackleftButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-on_04.png", "TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-off_04.png");
        addAndMakeVisible(trackleftButton);
        
        trackrightNSCButton = new RavenNSCButton(owner);
        trackrightNSCButton->addListener(owner);
        trackrightNSCButton->setTag(eNSCGSAssignmentChannelRightSwitch);
        
        trackrightButton = new RavenButton();
        RAVEN_BUTTON_1(trackrightButton);
        trackrightButton->setTopLeftPosition(trackleftButton->getX()+trackleftButton->getWidth(), bankleftButton->getHeight());
        trackrightButton->addListener(owner);
        trackrightButton->addListener(this);//-josh to allow for automation clearing
        trackrightButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-on_05.png", "TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-off_05.png");
        addAndMakeVisible(trackrightButton);
        
        int totalWidth = switchButton->getWidth() + trackleftButton->getWidth() + trackrightButton->getWidth();
        int totalHeight = trackleftButton->getHeight() + bankleftButton->getHeight();
        setSize(totalWidth, totalHeight);
        
    }
    
    void buttonStateChanged (Button*)
    {}
    
    void buttonClicked (Button* b);
    
    bool getTrackOrBankButtonTouched(TUIO::TuioCursor *tcur)
    {
        if(componentTouched(trackleftNSCButton, tcur, false) || componentTouched(trackrightNSCButton, tcur, false) || componentTouched(bankleftButton, tcur, false) || componentTouched(bankrightButton, tcur, false))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    void timerCallback()
    {
        if(triggerQueu < 0)
        {
            triggerQueu++;
            trackleftNSCButton->triggerClick();
        }
        else
        {
            triggerQueu--;
            trackrightNSCButton->triggerClick();
        }
        if(triggerQueu == 0)
        {
            stopTimer();
        }
    }
    
    RavenButton *getTrackLeftButton(){return trackleftButton;}
    RavenButton *getTrackRightButton(){return trackrightButton;}
    RavenNSCButton *getBankLeftNSCButton(){return bankleftNSCButton;}
    RavenNSCButton *getBankRightNSCButton(){return bankrightNSCButton;}
    RavenButton *getBankLeftButton(){return bankleftButton;}
    RavenButton *getBankRightButton(){return bankrightButton;}
    bool bankBy32(){ return switchButton->getToggleState();}
    
private:
    RavenButton *switchButton;
    
    ScopedPointer<RavenButton> bankleftButton;
    ScopedPointer<RavenButton> bankrightButton;
    ScopedPointer<RavenButton> trackleftButton;
    ScopedPointer<RavenButton> trackrightButton;
    
    ScopedPointer<RavenNSCButton> trackleftNSCButton;
    ScopedPointer<RavenNSCButton> trackrightNSCButton;
    ScopedPointer<RavenNSCButton> bankleftNSCButton;
    ScopedPointer<RavenNSCButton> bankrightNSCButton;
    
    int triggerQueu;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenTrackBankComponent)
};



#endif
