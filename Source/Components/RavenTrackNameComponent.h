

#ifndef Raven_NewSDK_RavenTrackNameComponent_h
#define Raven_NewSDK_RavenTrackNameComponent_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenTrackNameBankComponent.h"
#include "RavenWindowComponent.h"
#include "RavenMixerBackgroundComponent.h"
#include "RavenMixerComponent.h"
#include "RavenMixerChannelComponent.h"
#include "RavenSendsComponent.h"

class RavenTrackNameComponent : public RavenWindowComponent
{
public:
    RavenTrackNameComponent (int numBanks, int chansPerBank)
    {
        setSize (0, 0);
        int xOffset = 0;
        for(int i = 0; i < numBanks; i++)
        {
            RavenTrackNameBankComponent *bank = new RavenTrackNameBankComponent(chansPerBank, i, &iconMap);
            setSize(screenWidth, bank->getHeight());
            addAndMakeVisible(bank);
            bank->setTopLeftPosition(xOffset, 0);
            xOffset += bank->getWidth();
            banks.add(bank);
        }
        
//        hideButton->setVisible(false);
//        ravenTouchButtons.add(hideButton);
        
        iconMap.set("    ", Image());
        
        #if !RAVEN_DEBUG
        //Invisible Mouse Hiding Component (hides mouse and intercepts clicks)
        //MacHideBackGroundMouse enables this to work
        mouseHider = new MouseHideComponent(getWidth(), getHeight());
        addAndMakeVisible(mouseHider);
        #endif
    }
    
    void paint(Graphics &g)
    {
        g.fillAll(Colours::hotpink);
        //g.fillAll(Colours::black);
    }
    
    void resized()
    {
//        printf("name size = %d", getHeight());
//        //Hack until we figure out this bug where it gets set to a large height (pink strip bug)
//        if(getHeight() != 96)
//        {
//            setSize(getWidth(), 96);
//            //setTopLeftPosition(getX(), getY() + 32);
//        }
    }
    
    void setMixerComponent(RavenMixerComponent *_mixer)
    {
        mixer = _mixer;
    }
    
    void setSendsComponent(RavenSendsComponent *_sends)
    {
        sends = _sends;
    }
    
    void enterFlipMode()
    {
        for(int i = 0; i < banks.size(); i++) banks[i]->enterFlipMode();
    }
    void exitFlipMode()
    {
        for(int i = 0; i < banks.size(); i++) banks[i]->exitFlipMode();
    }
    
    void setNscLink	(class NscLink * link_)
    {
        for(int i = 0; i < banks.size(); i++) banks[i]->setNscLink(link_);
    }
    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, float val)
    {        
        for (int i=0; i<banks.size();++i)
        {
            if (bank == eMatchesAllBanks || bank == i)
                banks[i]->callback(type, subType, bank, which, val);
        }
    }
    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, const char * text)
    {
        for (int i=0; i<banks.size();++i)
        {
            if (bank == eMatchesAllBanks || bank == i)
                banks[i]->callback(type, subType, bank, which, text);
        }
    }
    
    RavenTrackNameBankComponent *getBank(int b) { return banks[b]; }
    
    String getTrackName(int bank, int chan) {return banks[bank]->getTrackName(chan);}
    
    void setAllTrackNamesBlank()
    {
        for(int i = 0;i<banks.size();i++)
            banks[i]->setAllTrackNamesBlank();
    }
    
    void setAllIcons()
    {
        for(int i = 0;i<banks.size();i++)
            banks[i]->setAllIcons();
    }
    
    void setIconMap(int bank, int which, String *key, Image *icon)
    {
        if(!iconMap.contains(*key))
        {
            iconMap.set(*key, *icon);
        }
    }
    
    //TODO: make these functions more efficient: 
    void trackLeft()
    {
        for (int i = banks.size()-1;i>=0;i--)
            for(int j = chansPerBank-1;j>=0;j--)
            {
                //set blank if neighbor to the left is blank
                String tempString("    ");
                bool tempBool = false;
                bool meterLClipped = false;
                bool meterRClipped = false;
                if(j > 0)
                {
                    tempString = banks[i]->getChannel(j-1)->getTrackName();
                    tempBool = mixer->getBank(i)->getChannel(j-1)->isKnobBlank();
                    meterLClipped = mixer->getBank(i)->getChannel(j-1)->isMeterLClipped();
                    meterRClipped = mixer->getBank(i)->getChannel(j-1)->isMeterRClipped();
                    banks[i]->getChannel(j)->setTrackName(tempString);
                    if(tempString == "    ")
                    {
                        mixer->getBank(i)->getChannel(j)->setBlank(true);
                        sends->getBank(i)->getChannel(j)->setBlank(true);
                    }
                    else
                    {
                        mixer->getBank(i)->getChannel(j)->setBlank(false);
                        sends->getBank(i)->getChannel(j)->setBlank(false);
                        mixer->getBank(i)->getChannel(j)->setKnobBlank(tempBool);
                        mixer->getBank(i)->getChannel(j)->setMeterLClipped(meterLClipped);
                        mixer->getBank(i)->getChannel(j)->setMeterRClipped(meterRClipped);
                    }
                }
                else
                {
                    if(i > 0)
                    {
                        tempString = banks[i-1]->getChannel(chansPerBank-1)->getTrackName();
                        tempBool = mixer->getBank(i-1)->getChannel(chansPerBank-1)->isKnobBlank();
                        meterLClipped = mixer->getBank(i-1)->getChannel(chansPerBank-1)->isMeterLClipped();
                        meterRClipped = mixer->getBank(i-1)->getChannel(chansPerBank-1)->isMeterRClipped();
                        banks[i]->getChannel(j)->setTrackName(tempString);
                        if(tempString == "    ")
                        {
                            mixer->getBank(i)->getChannel(j)->setBlank(true);
                            sends->getBank(i)->getChannel(j)->setBlank(true);
                        }
                        else
                        {
                            mixer->getBank(i)->getChannel(j)->setBlank(false);
                            sends->getBank(i)->getChannel(j)->setBlank(false);
                            mixer->getBank(i)->getChannel(j)->setKnobBlank(tempBool);
                            mixer->getBank(i)->getChannel(j)->setMeterLClipped(meterLClipped);
                            mixer->getBank(i)->getChannel(j)->setMeterRClipped(meterRClipped);
                        }
                    }
                }
            }
    }
    
    void trackRight()
    {
        for (int i=0; i<banks.size();i++)
            for(int j = 0;j<chansPerBank;j++)
            {
                //set blank if neighbor to the right is blank
                String tempString("    ");
                bool tempBool = false;
                bool meterLClipped = false;
                bool meterRClipped = false;
                if(j < chansPerBank-1)
                {
                    tempString = banks[i]->getChannel(j+1)->getTrackName();
                    tempBool = mixer->getBank(i)->getChannel(j+1)->isKnobBlank();
                    meterLClipped = mixer->getBank(i)->getChannel(j+1)->isMeterLClipped();
                    meterRClipped = mixer->getBank(i)->getChannel(j+1)->isMeterRClipped();
                    banks[i]->getChannel(j)->setTrackName(tempString);
                    if(tempString == "    ")
                    {
                        mixer->getBank(i)->getChannel(j)->setBlank(true);
                        sends->getBank(i)->getChannel(j)->setBlank(true);
                    }
                    else
                    {
                        mixer->getBank(i)->getChannel(j)->setBlank(false);
                        sends->getBank(i)->getChannel(j)->setBlank(false);
                        mixer->getBank(i)->getChannel(j)->setKnobBlank(tempBool);
                        mixer->getBank(i)->getChannel(j)->setMeterLClipped(meterLClipped);
                        mixer->getBank(i)->getChannel(j)->setMeterRClipped(meterRClipped);
                    }
                }
                else
                {
                    if(i < banks.size()-1)
                    {
                        tempString = banks[i+1]->getChannel(0)->getTrackName();
                        bool tempBool = mixer->getBank(i+1)->getChannel(0)->isKnobBlank();
                        meterLClipped = mixer->getBank(i+1)->getChannel(0)->isMeterLClipped();
                        meterRClipped = mixer->getBank(i+1)->getChannel(0)->isMeterRClipped();
                        banks[i]->getChannel(j)->setTrackName(tempString);
                        if(tempString == "    ")
                        {
                            mixer->getBank(i)->getChannel(j)->setBlank(true);
                            sends->getBank(i)->getChannel(j)->setBlank(true);
                        }
                        else
                        {
                            mixer->getBank(i)->getChannel(j)->setBlank(false);
                            sends->getBank(i)->getChannel(j)->setBlank(false);
                            mixer->getBank(i)->getChannel(j)->setKnobBlank(tempBool);
                            mixer->getBank(i)->getChannel(j)->setMeterLClipped(meterLClipped);
                            mixer->getBank(i)->getChannel(j)->setMeterRClipped(meterRClipped);
                        }
                    }
                }
            }
    }
    
    void loadIconMap(HashMap <String, Image> *_iconMap)
    {
        printf("loading icon map\n");
        iconMap.swapWith(*_iconMap);
        setAllIcons();
    }
    
    HashMap<String, int> *getIconIndexMap()
    {
        indexMap.clear();
        printf("saving icon index map\n");
        HashMap<String, Image>::Iterator i(iconMap);
        while (i.next())
        {
            if(i.getValue().getWidth() > 0)//hack to make sure image exits to prevent bad access. TODO: find out how hashmap is ever getting populated with blank images
            {
                int tempIndex = (int)i.getValue().getProperties()->getWithDefault("imageIndex", "0");
                if(tempIndex > 0)
                    indexMap.set(i.getKey(), (int)tempIndex);
            }
        }
        return &indexMap;
    }
    
    void resetIconMap()
    {
        iconMap.clear();
        iconMap.set("    ", Image());
        setAllIcons();
    }
    
private:
    HashMap<String, int> indexMap;
    OwnedArray<RavenTrackNameBankComponent> banks;
    HashMap <String, Image> iconMap;
    RavenMixerComponent *mixer;
    RavenSendsComponent *sends;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenTrackNameComponent)
};

//            if(i.getKey() != "Ltch" && i.getKey() != "Tch " && i.getKey() != "Off " && i.getKey() != "Wrt " && i.getKey() != "Trim")
//            {
//              }



#endif
