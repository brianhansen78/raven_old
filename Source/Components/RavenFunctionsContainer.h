

#ifndef Raven_RavenFunctionsContainer_h
#define Raven_RavenFunctionsContainer_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "NscGuiElement.h"


class RavenFunctionsContainer  : public NscGuiContainer, public Button::Listener
{
public:

    RavenFunctionsContainer () : NscGuiContainer(eControllerBankAll){}
    ~RavenFunctionsContainer(){}
    
    void buttonStateChanged (Button*){}
    void buttonClicked (Button* b) {}
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenFunctionsContainer)
    
};


#endif 
