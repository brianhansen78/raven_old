
#ifndef Raven_RavenInsertsPalette_h
#define Raven_RavenInsertsPalette_h

#include "RavenTextDisplay.h"
#include "MouseHideComponent.h"
#include "RavenWindowComponent.h"
#include "RavenFunctionsComponent.h"
#include "RavenFunctionsManager.h"

#define INSERTS_PALETTE_DEBUG 0

enum
{
    eTimerID_UpdateNames,
    eTimerID_WinToggleOff,
    eTimerID_Insert1to4Name,
    eTimerID_Insert5Name,
    eTimerID_Insert1Win,
    eTimerID_Insert2Win,
    eTimerID_Insert3Win,
    eTimerID_Insert4Win,
    eTimerID_Insert5Win,
    eTimerID_Insert5WinSel,
    eTimerID_Insert1Byp,
    eTimerID_Insert2Byp,
    eTimerID_Insert3Byp,
    eTimerID_Insert4Byp,
    eTimerID_Insert5Byp,
    eTimerID_Insert5BypSel,
    eTimerID_InsertParamOff,
    eTimerID_InsertParamOffThenWin,
    eTimerID_InsertWindow,
    eTimerID_InsertBypass,
    eTimerID_BypassAllOn,
    eTimerID_BypassAllOff,
    eTimerID_InsertTrackSelect,
    eTimerID_InsertEndQueueEvent,
    eTimerID_InsertQueueFinalPauseEvent
};

class RavenInsertsPalette : public RavenWindowComponent, public Button::Listener, public MultiTimer, public ActionBroadcaster
{
public:
    
    RavenInsertsPalette()
    :
    processingQueue(false),
    currentBank(0),
    currentChan(0),
    wasVisible(false)
    {

        if(INSERTS_NO_BYPASS)
        {
            panelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/INSERTS_PANEL_NOBYPASS/background/MenuInsert_Panel.png"));
        }
        else
        {
            //background panel image
            panelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "InsertPalette/background/MenuInsert_Panel.png"));
        }
        setSize(panelImg.getWidth(), panelImg.getHeight());
        
        if(INSERTS_PALETTE_DEBUG)
        {
            setSize(500, 500);
            setVisible(true);
        }
        
        closeButton = new RavenButton();
        closeButton->setImagePaths("InsertPalette/bt_On/images/bt_close.png", "InsertPalette/bt_Off/images/bt_close.png");
        ravenTouchButtons.add(closeButton);
        closeButton->setTopLeftPosition(42, 6);
        closeButton->addListener(this);
        addAndMakeVisible(closeButton);
        
        trackLabel = new RavenFuturaLabel();
        trackLabel->setFontSize(30.0f);
        trackLabel->setColour(Label::textColourId, Colour(251, 251, 251));
        trackLabel->setBounds(closeButton->getRight(), closeButton->getY(), 100, closeButton->getHeight());
        trackLabel->setText("", false);
        trackLabel->setEnabled(true);
        addAndMakeVisible(trackLabel);
        
        bypassAllButton = new RavenButton();
        bypassAllButton->setClickingTogglesState(true);
        bypassAllButton->setImagePaths("InsertPalette/bt_On/images/bypass_all.png", "InsertPalette/bt_Off/images/bypass_all.png");
        ravenTouchButtons.add(bypassAllButton);
        bypassAllButton->setTopLeftPosition(closeButton->getRight() + 120, closeButton->getY());
        bypassAllButton->addListener(this);
        if(!INSERTS_NO_BYPASS) addAndMakeVisible(bypassAllButton);
                
        ////////////////////////////////////////
        
        win1 = new RavenButton();
        win1->setClickingTogglesState(true);
        win1->setImagePaths("InsertPalette/bt_On/images/bt_01.png", "InsertPalette/bt_Off/images/bt_01.png");
        winToggles.add(win1);
        ravenTouchButtons.add(win1);
        win1->setTopLeftPosition(53, 39);
        win1->addListener(this);
        addAndMakeVisible(win1);
        
        ins1 = new RavenFuturaLabel();
        ins1->setBounds(win1->getX(), win1->getY(), win1->getWidth(), win1->getHeight());
        ins1->setText("", false);
        addAndMakeVisible(ins1);
        
        byp1 = new RavenButton();
        byp1->setClickingTogglesState(true);
        byp1->setImagePaths("InsertPalette/bt_On/images/bt_byp_01.png", "InsertPalette/bt_Off/images/bt_byp_01.png");
        ravenTouchButtons.add(byp1);
        byp1->setTopLeftPosition(win1->getRight() + 6, win1->getY());
        byp1->addListener(this);
        if(!INSERTS_NO_BYPASS) addAndMakeVisible(byp1);
        
        ////////////////////////////////////////
    
        win2 = new RavenButton();
        win2->setClickingTogglesState(true);
        win2->setImagePaths("InsertPalette/bt_On/images/bt_02.png", "InsertPalette/bt_Off/images/bt_02.png");
        winToggles.add(win2);
        ravenTouchButtons.add(win2);
        win2->setTopLeftPosition(win1->getX(), win1->getBottom());
        win2->addListener(this);
        addAndMakeVisible(win2);
        
        ins2 = new RavenFuturaLabel();
        ins2->setBounds(win2->getX(), win2->getY(), win2->getWidth(), win2->getHeight());
        ins2->setText("", false);
        addAndMakeVisible(ins2);
        
        byp2 = new RavenButton();
        byp2->setClickingTogglesState(true);
        byp2->setImagePaths("InsertPalette/bt_On/images/bt_byp_02.png", "InsertPalette/bt_Off/images/bt_byp_02.png");
        ravenTouchButtons.add(byp2);
        byp2->setTopLeftPosition(byp1->getX(), win2->getY());
        byp2->addListener(this);
        if(!INSERTS_NO_BYPASS) addAndMakeVisible(byp2);
        
        ////////////////////////////////////////
        
        win3 = new RavenButton();
        win3->setImagePaths("InsertPalette/bt_On/images/bt_03.png", "InsertPalette/bt_Off/images/bt_03.png");
        win3->setClickingTogglesState(true);
        winToggles.add(win3);
        ravenTouchButtons.add(win3);
        win3->setTopLeftPosition(win1->getX(), win2->getBottom());
        win3->addListener(this);
        addAndMakeVisible(win3);
        
        ins3 = new RavenFuturaLabel();
        ins3->setBounds(win3->getX(), win3->getY(), win3->getWidth(), win3->getHeight());
        ins3->setText("", false);
        addAndMakeVisible(ins3);
        
        byp3 = new RavenButton();
        byp3->setClickingTogglesState(true);
        byp3->setImagePaths("InsertPalette/bt_On/images/bt_byp_03.png", "InsertPalette/bt_Off/images/bt_byp_03.png");
        ravenTouchButtons.add(byp3);
        byp3->setTopLeftPosition(byp1->getX(), win3->getY());
        byp3->addListener(this);
        if(!INSERTS_NO_BYPASS) addAndMakeVisible(byp3);
        
        ////////////////////////////////////////
        
        win4 = new RavenButton();
        win4->setImagePaths("InsertPalette/bt_On/images/bt_04.png", "InsertPalette/bt_Off/images/bt_04.png");
        win4->setClickingTogglesState(true);
        winToggles.add(win4);
        ravenTouchButtons.add(win4);
        win4->setTopLeftPosition(win1->getX(), win3->getBottom());
        win4->addListener(this);
        addAndMakeVisible(win4);
        
        ins4 = new RavenFuturaLabel();
        ins4->setBounds(win4->getX(), win4->getY(), win4->getWidth(), win4->getHeight());
        ins4->setText("", false);
        addAndMakeVisible(ins4);
        
        byp4 = new RavenButton();
        byp4->setClickingTogglesState(true);
        byp4->setImagePaths("InsertPalette/bt_On/images/bt_byp_04.png", "InsertPalette/bt_Off/images/bt_byp_04.png");
        ravenTouchButtons.add(byp4);
        byp4->setTopLeftPosition(byp1->getX(), win4->getY());
        byp4->addListener(this);
        if(!INSERTS_NO_BYPASS) addAndMakeVisible(byp4);
        
        ////////////////////////////////////////
        
        win5 = new RavenButton();
        win5->setImagePaths("InsertPalette/bt_On/images/bt_05.png", "InsertPalette/bt_Off/images/bt_05.png");
        win5->setClickingTogglesState(true);
        winToggles.add(win5);
        ravenTouchButtons.add(win5);
        win5->setTopLeftPosition(win1->getX(), win4->getBottom());
        win5->addListener(this);
        addAndMakeVisible(win5);
        
        ins5 = new RavenFuturaLabel();
        ins5->setBounds(win5->getX(), win5->getY(), win5->getWidth(), win5->getHeight());
        ins5->setText("", false);
        addAndMakeVisible(ins5);
        
        byp5 = new RavenButton();
        byp5->setClickingTogglesState(true);
        byp5->setImagePaths("InsertPalette/bt_On/images/bt_byp_05.png", "InsertPalette/bt_Off/images/bt_byp_05.png");
        ravenTouchButtons.add(byp5);
        byp5->setTopLeftPosition(byp1->getX(), win5->getY());
        byp5->addListener(this);
        if(!INSERTS_NO_BYPASS) addAndMakeVisible(byp5);

        #if !RAVEN_DEBUG
        mouseHider = new MouseHideComponent(getWidth(), getHeight());
        addAndMakeVisible(mouseHider);
        #endif
        
    }
    
    void initializeLCDs(RavenFunctionsManager *owner)
    {
        for(int i = 0; i < numBanks; i++)
        {
            for(int j = 0; j < 4; j++)
            {
                RavenTextDisplay *lcd = new RavenTextDisplay(owner);
                lcd->setBounds(10+100*j, 350+40*i, 80, 30);
                lcd->setJustificationType(Justification::centred);
                lcd->setText("", false);
                lcd->setTag(eNSCPlugInLCD);
                lcd->setBankIndex(i);
                lcd->setChannelIndex(40 + j*10);
                addAndMakeVisible(lcd);
                
                if     (j == 0)lcd1.add(lcd);
                else if(j == 1)lcd2.add(lcd);
                else if(j == 2)lcd3.add(lcd);
                else if(j == 3)lcd4.add(lcd);
            }
            
            RavenNSCButton *winBut = new RavenNSCButton(owner);
            winBut->setTopLeftPosition(300, 40+70*i);
            winBut->setTag(eNSCGSPTWindow_PlugIn);
            winBut->setBankIndex(i);
            winBut->setChannelIndex(kNscGuiBankGlobalIdentifier);
            winBut->addListener(owner);
            winButton.add(winBut);
            
            RavenNSCButton *bypBut = new RavenNSCButton(owner);
            bypBut->setTopLeftPosition(winBut->getRight()+5, 40+70*i);
            bypBut->setTag(eNSCGSPlugInBypass);
            bypBut->setBankIndex(i);
            bypBut->setChannelIndex(kNscGuiBankGlobalIdentifier);
            bypBut->addListener(owner);
            bypButton.add(bypBut);
            
            RavenNSCButton *bypAllBut = new RavenNSCButton(owner);
            bypAllBut->setTag(eNSCGSInsertBypass);
            bypAllBut->setBankIndex(i);
            bypAllBut->setChannelIndex(kNscGuiBankGlobalIdentifier);
            bypAllBut->addListener(owner);
            bypAllButton.add(bypAllBut);
            
            RavenNSCButton *bankLeftBut = new RavenNSCButton(owner);
            bankLeftBut->setTag(eNSCGSPlugInBankLeft);
            bankLeftBut->setTopLeftPosition(bypBut->getRight()+20, 40+70*i);
            bankLeftBut->setBankIndex(i);
            bankLeftBut->setChannelIndex(kNscGuiBankGlobalIdentifier);
            bankLeftBut->addListener(owner);
            bankLeftBut->triggerClick(); //keep left initially
            bankLeftButton.add(bankLeftBut);
            
            RavenNSCButton *bankRightBut = new RavenNSCButton(owner);
            bankRightBut->setTag(eNSCGSPlugInBankRight);
            bankRightBut->setTopLeftPosition(bankLeftBut->getRight(), 40+70*i);
            bankRightBut->setBankIndex(i);
            bankRightBut->setChannelIndex(kNscGuiBankGlobalIdentifier);
            bankRightBut->addListener(owner);
            bankRightButton.add(bankRightBut);
            
            RavenNSCButton *insparamBut = new RavenNSCButton(owner);
            insparamBut->setTag(eNSCGSPlugInInsert);
            insparamBut->setTopLeftPosition(bankRightBut->getRight()+20, 40+70*i);
            insparamBut->setBankIndex(i);
            insparamBut->setChannelIndex(kNscGuiBankGlobalIdentifier);
            insparamBut->addListener(owner);
            insparamButton.add(insparamBut);
            
            //ins select 1-4
            RavenNSCButton *selBut1 = new RavenNSCButton(owner);
            selBut1->setTag(eNSCGSPlugInSwitchSelect1);
            selBut1->setTopLeftPosition(300, 75+70*i);
            selBut1->setBankIndex(i);
            selBut1->setChannelIndex(kNscGuiBankGlobalIdentifier);
            selBut1->addListener(owner);
            selButton1.add(selBut1);
            
            RavenNSCButton *selBut2 = new RavenNSCButton(owner);
            selBut2->setTag(eNSCGSPlugInSwitchSelect2);
            selBut2->setTopLeftPosition(selBut1->getRight() + 5, 75+70*i);
            selBut2->setBankIndex(i);
            selBut2->setChannelIndex(kNscGuiBankGlobalIdentifier);
            selBut2->addListener(owner);
            selButton2.add(selBut2);
            
            RavenNSCButton *selBut3 = new RavenNSCButton(owner);
            selBut3->setTag(eNSCGSPlugInSwitchSelect3);
            selBut3->setTopLeftPosition(selBut2->getRight() + 5, 75+70*i);
            selBut3->setBankIndex(i);
            selBut3->setChannelIndex(kNscGuiBankGlobalIdentifier);
            selBut3->addListener(owner);
            selButton3.add(selBut3);
            
            RavenNSCButton *selBut4 = new RavenNSCButton(owner);
            selBut4->setTag(eNSCGSPlugInSwitchSelect4);
            selBut4->setTopLeftPosition(selBut3->getRight() + 5, 75+70*i);
            selBut4->setBankIndex(i);
            selBut4->setChannelIndex(kNscGuiBankGlobalIdentifier);
            selBut4->addListener(owner);
            selButton4.add(selBut4);

            if(INSERTS_PALETTE_DEBUG)
            {
                addAndMakeVisible(winBut);
                addAndMakeVisible(bypBut);
                addAndMakeVisible(bankLeftBut);
                addAndMakeVisible(bankRightBut);
                addAndMakeVisible(insparamBut);
                addAndMakeVisible(selBut1);
                addAndMakeVisible(selBut2);
                addAndMakeVisible(selBut3);
                addAndMakeVisible(selBut4);
            }
        
        }
        
    }
    
    void paint(Graphics &g)
    {
        //g.fillAll(Colour(0, 0, 0).withAlpha(0.75f));
        g.drawImageAt(panelImg, 0, 0);
    }
    
    void addToTimerQueue(int timerId)
    {
        timerQueue.add(timerId);
        nextTimerQueue();
    }
    
    void nextTimerQueue()
    {
        if(timerQueue.size() > 0 && !processingQueue)
        {
            processingQueue = true;
            startTimer(timerQueue[0], huiWaitMs);
            timerQueue.remove(0);
        }
    }
    
    void updateNames()
    {
        if(trackLabel->getText().startsWith(" "))
        {
            ins1->setText("", false);
            ins2->setText("", false);
            ins3->setText("", false);
            ins4->setText("", false);
            ins5->setText("", false);
        }
        else
        {
            if(timerQueue.size() == 0 && !processingQueue) addToTimerQueue(eTimerID_UpdateNames);
        }
    }
    
    void checkWinToggleOff()
    {
        //if there is no window showing, then none of the window toggles should be on
        if(!winButton[currentBank]->getToggleState())
        {
            win1->setToggleState(false, false);
            win2->setToggleState(false, false);
            win3->setToggleState(false, false);
            win4->setToggleState(false, false);
            win5->setToggleState(false, false);
        }
    }
    
    void visibilityChanged()
    {

    }
    
    void timerCallback(int timerId)
    {
        if(timerId == eTimerID_UpdateNames)
        {
            checkWinToggleOff();
            bankLeftButton[currentBank]->triggerClick();
            startTimer(eTimerID_Insert1to4Name, huiWaitMs);
        }
        
        else if(timerId == eTimerID_Insert1to4Name)
        {
            for(int i = 0; i < 3; i++)
            {
                ins1->setText(lcd1[currentBank]->getText(), false);
                ins2->setText(lcd2[currentBank]->getText(), false);
                ins3->setText(lcd3[currentBank]->getText(), false);
                ins4->setText(lcd4[currentBank]->getText(), false);
            }
            
            bankRightButton[currentBank]->triggerClick();
            startTimer(eTimerID_Insert5Name, huiWaitMs);
        }
        
        else if(timerId == eTimerID_Insert5Name)
        {
            //set text for 5 from lcd1
            ins5->setText(lcd1[currentBank]->getText(), false);

            startTimer(eTimerID_InsertEndQueueEvent, huiWaitMs);
        }
        
        else if(timerId == eTimerID_Insert1Win)
        {
            selButton1[currentBank]->triggerClick();
            startTimer(eTimerID_InsertParamOffThenWin, huiWaitMs);
        }
        
        else if(timerId == eTimerID_Insert2Win)
        {
            selButton2[currentBank]->triggerClick();
            startTimer(eTimerID_InsertParamOffThenWin, huiWaitMs);
        }
        
        else if(timerId == eTimerID_Insert3Win)
        {
            selButton3[currentBank]->triggerClick();
            startTimer(eTimerID_InsertParamOffThenWin, huiWaitMs);
        }
        
        else if(timerId == eTimerID_Insert4Win)
        {
            selButton4[currentBank]->triggerClick();
            startTimer(eTimerID_InsertParamOffThenWin, huiWaitMs);
        }
        
        else if(timerId == eTimerID_Insert5Win)
        {
            bankRightButton[currentBank]->triggerClick();
            startTimer(eTimerID_Insert5WinSel, huiWaitMs);
        }
        else if(timerId == eTimerID_Insert5WinSel)
        {
            selButton1[currentBank]->triggerClick();
            startTimer(eTimerID_InsertParamOffThenWin, huiWaitMs);
        }
        
        else if(timerId == eTimerID_Insert1Byp)
        {
            selButton1[currentBank]->triggerClick();
            startTimer(eTimerID_InsertBypass, huiWaitMs);
        }
        
        else if(timerId == eTimerID_Insert2Byp)
        {
            selButton2[currentBank]->triggerClick();
            startTimer(eTimerID_InsertBypass, huiWaitMs);
        }
        
        else if(timerId == eTimerID_Insert3Byp)
        {
            selButton3[currentBank]->triggerClick();
            startTimer(eTimerID_InsertBypass, huiWaitMs);
        }
        
        else if(timerId == eTimerID_Insert4Byp)
        {
            selButton4[currentBank]->triggerClick();
            startTimer(eTimerID_InsertBypass, huiWaitMs);
        }
        
        else if(timerId == eTimerID_Insert5Byp)
        {
            bankRightButton[currentBank]->triggerClick();
            startTimer(eTimerID_Insert5BypSel, huiWaitMs);
        }
        else if(timerId == eTimerID_Insert5BypSel)
        {
            selButton1[currentBank]->triggerClick();
            startTimer(eTimerID_InsertBypass, huiWaitMs);
        }
        else if(timerId == eTimerID_InsertParamOff)
        {
            insparamButton[currentBank]->triggerClick();
            
            startTimer(eTimerID_InsertEndQueueEvent, huiWaitMs);
        }
        else if(timerId == eTimerID_InsertParamOffThenWin)
        {
            //turn off param mode because this is automatically triggered after a plug-in selection
            insparamButton[currentBank]->triggerClick();
            //then show window
            startTimer(eTimerID_InsertWindow, huiWaitMs);
        }
        else if(timerId == eTimerID_InsertWindow)
        {
            //click button if not already showing window
            if(!winButton[currentBank]->getToggleState()) winButton[currentBank]->triggerClick();
            //click button if showing window and trying to close window
            else if(winButton[currentBank]->getToggleState() && !currentWin->getToggleState())
            {
                winButton[currentBank]->triggerClick();
                //setVisible(false); //also hide inserts palette
                //sendActionMessage("toggleInsertButtonsOff");
            }
            
            startTimer(eTimerID_InsertEndQueueEvent, huiWaitMs);
        }
        else if(timerId == eTimerID_InsertBypass)
        {
            bypButton[currentBank]->triggerClick();
            startTimer(eTimerID_InsertParamOff, huiWaitMs);
        }
        
        else if(timerId == eTimerID_BypassAllOn)
        {
            bypAllButton[currentBank]->triggerClick();
            startTimer(eTimerID_InsertTrackSelect, huiWaitMs);
        }
        
        else if(timerId == eTimerID_InsertTrackSelect)
        {
            currentTrackButton->triggerClick();
            startTimer(eTimerID_BypassAllOff, huiWaitMs);
        }
        
        else if(timerId == eTimerID_BypassAllOff)
        {
            bypAllButton[currentBank]->triggerClick();
            startTimer(eTimerID_InsertEndQueueEvent, huiWaitMs);
        }
        
        else if(timerId == eTimerID_InsertEndQueueEvent)
        {
            bankLeftButton[currentBank]->triggerClick();
            startTimer(eTimerID_InsertQueueFinalPauseEvent, huiWaitMs);
        }
        else if(timerId == eTimerID_InsertQueueFinalPauseEvent)
        {            
            processingQueue = false;
            nextTimerQueue();
        }
        
        else if(timerId == eTimerID_WinToggleOff)
        {
            checkWinToggleOff();
            return; // don't turn off this timer
        }
        
        //stop all timers
        stopTimer(timerId);
        
    }
    
    void buttonStateChanged (Button*){}
    void buttonClicked (Button* b)
    {
        if(b == closeButton)
        {
            setShouldBeVisible(false);
            getWindow()->removeFromDesktop();
            return;
        }
        
        if(b == win1 || b == win2 || b == win3 || b == win4 || b == win5)
        {
            for(int i = 0; i < winToggles.size(); i++)
            {
                if(b->getToggleState() && winToggles[i] != b) winToggles[i]->setToggleState(false, false);
            }
            
            currentWin = b;
        }
        
        //* Correct Raven PlugBut Implementation
        if     (b == win1) { addToTimerQueue(eTimerID_Insert1Win); }
        else if(b == win2) { addToTimerQueue(eTimerID_Insert2Win); }
        else if(b == win3) { addToTimerQueue(eTimerID_Insert3Win); }
        else if(b == win4) { addToTimerQueue(eTimerID_Insert4Win); }
        else if(b == win5) { addToTimerQueue(eTimerID_Insert5Win); }
        //*/
        
        /* Disable Bypass Buttons (no longer used)
        else if(b == byp1) { addToTimerQueue(eTimerID_Insert1Byp); }
        else if(b == byp2) { addToTimerQueue(eTimerID_Insert2Byp); }
        else if(b == byp3) { addToTimerQueue(eTimerID_Insert3Byp); }
        else if(b == byp4) { addToTimerQueue(eTimerID_Insert4Byp); }
        else if(b == byp5) { addToTimerQueue(eTimerID_Insert5Byp); }
        
        else if(b == bypassAllButton)
        {
            addToTimerQueue(eTimerID_BypassAllOn);
            bool bypAllState = b->getToggleState();
            byp1->setToggleState(bypAllState, false);
            byp2->setToggleState(bypAllState, false);
            byp3->setToggleState(bypAllState, false);
            byp4->setToggleState(bypAllState, false);
            byp5->setToggleState(bypAllState, false);
        }
        //*/
        
    }
    
    void setCurrentTrackButton(RavenNSCButton *btn)
    {
        //if current bank's window button is on, we want to show window for new bank instead
        bool showWindowOnNewBank = winButton[currentBank]->getToggleState();
        
        currentTrackButton = btn;
        currentBank = currentTrackButton->getBankIndex();
        currentChan = currentTrackButton->getChannelIndex();
        
        //if we need to show the window on the new bank and it was not already showing a window
        if(showWindowOnNewBank && !winButton[currentBank]->getToggleState()) winButton[currentBank]->triggerClick();
    }
    
    void setTrackName(String name)
    {
        if(name.startsWith("") || name.startsWith(" "))
        {
            trackLabel->setText(" ", false);
        }
        trackLabel->setText(name, false);
    }
    
    bool bypassAllOn()
    {
        
        for(int i = 0; i < 5; i++)
        {
            String insName;
            if(i == 0) insName = ins1->getText();
            if(i == 1) insName = ins2->getText();
            if(i == 2) insName = ins3->getText();
            if(i == 3) insName = ins4->getText();
            if(i == 4) insName = ins5->getText();
            
            if(!insName.contains("No Insert"))
            {
                if(insName.containsAnyOf("abcdefghijklmnopqrstuvwxyz")) return false;
            }
        }
        
        //else
        return true;
    }
    
    int  getCurrentBank()        {return currentBank;}
    int  getCurrentChan()        {return currentChan;}
    
    RavenNSCButton *getInsertWindowButton(int bank){ return winButton[bank]; }
    
    //used to check for move touch
    RavenFuturaLabel* getTrackLabel(){return trackLabel;}
    
    //separate visibility state to determine if it should be made visible after hiding when entering toolbar mode
    void setShouldBeVisible(bool vis) { wasVisible = vis; }
    bool shouldBeVisible() { return wasVisible; }
    
private:

    //invisible HUI LCDs
    OwnedArray<RavenTextDisplay> lcd1;
    OwnedArray<RavenTextDisplay> lcd2;
    OwnedArray<RavenTextDisplay> lcd3;
    OwnedArray<RavenTextDisplay> lcd4;
    
    //pointer to current insert select button on raven mixer
    RavenNSCButton *currentTrackButton;
    
    //invisible HUI buttons
    OwnedArray<RavenNSCButton> winButton;
    OwnedArray<RavenNSCButton> bypButton;
    OwnedArray<RavenNSCButton> bypAllButton;
    OwnedArray<RavenNSCButton> bankLeftButton;
    OwnedArray<RavenNSCButton> bankRightButton;
    OwnedArray<RavenNSCButton> selButton1;
    OwnedArray<RavenNSCButton> selButton2;
    OwnedArray<RavenNSCButton> selButton3;
    OwnedArray<RavenNSCButton> selButton4;
    OwnedArray<RavenNSCButton> insparamButton;
    
    //our displays for insert names
    ScopedPointer<RavenFuturaLabel> ins1;
    ScopedPointer<RavenFuturaLabel> ins2;
    ScopedPointer<RavenFuturaLabel> ins3;
    ScopedPointer<RavenFuturaLabel> ins4;
    ScopedPointer<RavenFuturaLabel> ins5;
    
    //show window touch buttons 
    RavenButton *win1;
    RavenButton *win2;
    RavenButton *win3;
    RavenButton *win4;
    RavenButton *win5;
    
    //array to store the above for correct toggle processing
    Array<Button*> winToggles;
    
    //pointer to currently selected win button
    Button *currentWin;
    
    //bypass touch buttons
    RavenButton *byp1;
    RavenButton *byp2;
    RavenButton *byp3;
    RavenButton *byp4;
    RavenButton *byp5;
    
    Array<int> timerQueue;
    
    bool processingQueue;
    
    int currentBank, currentChan;
    
    ScopedPointer<RavenFuturaLabel> trackLabel;
    
    RavenButton *closeButton;
    RavenButton *bypassAllButton;
    
    Image panelImg;
    
    bool wasVisible;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenInsertsPalette)
    
};

#endif

