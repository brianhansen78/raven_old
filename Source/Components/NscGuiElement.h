
#ifndef __NSCGUIELEMENT_H_3D9B0E26__
#define __NSCGUIELEMENT_H_3D9B0E26__

#include "../JuceLibraryCode/JuceHeader.h"
#include "NscSDKIncludes.h"

#define kNscGuiIdentifier "nscg"
#define kNscGuiGlobalIdentifier -1
#define kNscGuiKeyboardIdentifier -2
#define kNscGuiBankGlobalIdentifier -3
#define kNscTimeDisplayIdentifier -4

class NscGuiElement
{
public:
	NscGuiElement(class NscGuiContainer * owner);
	NscGuiElement(NscGuiContainer * owner, int tag);
	NscGuiElement(NscGuiContainer * owner, int bank, int channel, int tag);
	virtual ~NscGuiElement();

	virtual void	setChannelIndex(int index) {channelIndex = index;}
	virtual void	setBankIndex(int index) { bankIndex = index;}
	virtual void	setTag(int tag) { guiTag = tag;}

	int		getChannelIndex() { return channelIndex;}
	int		getBankIndex() { return bankIndex;}
	int		getTag() { return guiTag;}

	virtual void  updateNscValue(float value);			// callback when DAW changes a value; generally
	virtual void  updateNscValue(const String & value);	// intended to be overriden
        
protected:
	friend class NscGuiContainer;
	int		bankIndex;
	int		channelIndex;
	int		guiTag;
};

class NscGuiContainer : public MessageListener
{
public:
	NscGuiContainer(int bank);
	virtual ~NscGuiContainer();
    
    //Handler to safely send messages on the JUCE Thread
    void handleMessage(const Message &message);

	void addGuiElement(NscGuiElement * element);
	//bool handleUpdate(const struct SControlEvent& message); //don't know where this came from
	void setNscLink	(class NscLink * link_) { link = link_;};
    
	void 	sliderChanged (NscGuiElement *  slider, float sliderValue);
	void 	sliderBegan (NscGuiElement * slider);
	void 	sliderEnded (NscGuiElement * slider);

	// Button::Listener interface
    void	switchClicked (NscGuiElement * switchThatWasClicked, bool switchState);

	// nsc update callback (changed when change received from daw)
	virtual int callback(ENSCEventType type, int subType, int bank, int which, float val);
	virtual int callback(ENSCEventType type, int subType, int bank, int which, const char * text);

protected:
	NscGuiElement * findNscGuiElement(int tag);
	NscGuiElement * findNscGuiElement(int bank, int channel, int tag);
	int		bankNum;

	NscLink		* link;

private:
	Array < NscGuiElement *> elements;

};


#endif  // __NSCGUIELEMENT_H_3D9B0E26__
