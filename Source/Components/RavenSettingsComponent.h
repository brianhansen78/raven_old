//
//  RavenSettingsComponent.h
//  Raven
//
//  Created by Joshua Dickinson on 5/14/13.
//
//

#ifndef Raven_RavenSettingsComponent_h
#define Raven_RavenSettingsComponent_h

#include "RavenConfig.h"
#include "RavenButton.h"
#include "RavenTextDisplay.h"
#include "RavenWindowComponent.h"

class RavenSettingsComponent : public RavenWindowComponent, public Button::Listener
{
public:
    RavenSettingsComponent() : initWidth(265), initHeight(100)
    {
        panelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLoatingEssentials/floating_essential_base.png"));
        Rectangle<int> clippedPaletteImage = Rectangle<int>::Rectangle(22,22,974,552);
        panelImg = panelImg.getClippedImage(clippedPaletteImage);
        panelImg = panelImg.rescaled(initWidth, initHeight, Graphics::highResamplingQuality);
        
        setSize(panelImg.getWidth(), panelImg.getHeight());

        saveSessionButton = new RavenButton();
        saveSessionButton->setName("saveSession");
        saveSessionButton->setTopLeftPosition(10, 30);
        ravenTouchButtons.add(saveSessionButton);
        //saveSessionButton->addListener(this);
        addAndMakeVisible(saveSessionButton);
        
        loadSessionButton = new RavenButton();
        loadSessionButton->setName("loadSession");
        loadSessionButton->setTopLeftPosition(saveSessionButton->getRight() + 10, 30);
        ravenTouchButtons.add(loadSessionButton);
        //loadSessionButton->addListener(this);
        addAndMakeVisible(loadSessionButton);
        
        defaultLayoutsButton = new RavenButton();
        defaultLayoutsButton->setName("defaultLayoutsButton");
        defaultLayoutsButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/LayoutPreset/RestoreDefault-On-btn.png", "TOOLBAR/FUNCTIONMODULE/LayoutPreset/RestoreDefault-Off-btn.png");
        defaultLayoutsButton->setTopLeftPosition(loadSessionButton->getRight() + 10, 30);
        ravenTouchButtons.add(defaultLayoutsButton);
        //defaultLayoutsButton->addListener(this);
        addAndMakeVisible(defaultLayoutsButton);
        
        backgroundButton = new RavenButton();
        backgroundButton->setName("backgroundButton");
        backgroundButton->setClickingTogglesState(true);
        backgroundButton->setToggleState(true, false);
        backgroundButton->setTopLeftPosition(defaultLayoutsButton->getRight() + 10, 30);
        ravenTouchButtons.add(backgroundButton);
        //loadSessionButton->addListener(this);
        addAndMakeVisible(backgroundButton);
        
        mouseHider = new MouseHideComponent(getWidth(), getHeight());
        mouseHider->setName("MouseHider");
        addAndMakeVisible(mouseHider);
    }
    
    void setSettingsButtonListener(Button::Listener *listener)
    {
        defaultLayoutsButton->addListener(listener);
        saveSessionButton->addListener(listener);
        loadSessionButton->addListener(listener);
        backgroundButton->addListener(listener);
     }

    
    void buttonStateChanged (Button* b)
    {
    }
    
    void buttonClicked (Button* b);
    
    void paint(Graphics &g)
    {
        g.drawImageAt(panelImg, 0, 0);
    }
    
private:
    
    RavenButton *saveSessionButton;
    RavenButton *loadSessionButton;
    RavenButton *defaultLayoutsButton;
    RavenButton *backgroundButton;
    
    int initWidth;
    int initHeight;
    
    Image panelImg;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenSettingsComponent)
};

#endif
