//
//  RavenWindowManager.cpp
//  Raven
//
//  Created by Joshua Dickinson on 6/6/13.
//
//

#include "RavenConfig.h"
#include "RavenWindowManager.h"
#include "RavenRackContainer.h"
#include "RavenTouchComponent.h"

void RavenWindowManager::rebuildRack()
{
    //std::cout << "Num modules: " << windows.size() << std::endl;
    
    sortWindowsByRackPosition(0);
    
    //Reposition rack from the bottom up.
    int currentScreenPosition = screenHeight;
    for(int i = 0 ; i < windows.size(); i++)
    {
        //if in hybrid mode, set position independently from other rack windows.
        if(RAVEN->isHybridMode() && windows[i] == RAVEN->getMixer()->getWindow())
        {
            RAVEN->getMixer()->hybridModeSizing();
            continue;
        }
        
        //unanimated:
        windows[i]->setTopLeftPosition(0, currentScreenPosition - windows[i]->getHeight());
        if(!windows[i]->isOnDesktop()) windows[i]->addToDesktopWithOptions();
        
        currentScreenPosition = windows[i]->getY();
    }
//        Rectangle<int> moveToRect(0, 0, windows[0]->getWidth(), windows[0]->getHeight());
//        Desktop::getInstance().getAnimator().animateComponent (windows[0], moveToRect, 1.0, 1000, false, 0.0, 0.0);
//
//        Rectangle<int> moveToRect2(0, 500, windows[1]->getWidth(), windows[1]->getHeight());
//        Desktop::getInstance().getAnimator().animateComponent (windows[1], moveToRect2, 1.0, 1000, false, 0.0, 0.0);
}

void RavenWindowManager::loadSavedRackWindowLayout()
{
    //1. clear windows array
    windows.clear();
    
    //2. add windows from savedWindowsLayout to windows array and add to desktop.
    for(int i = 0; i < savedWindowLayout.size(); i++)
    {
        insertWindowIntoRack(savedWindowLayout[i].windowName, savedWindowLayout[i].rackPosition, true);
    }
    
    //3. remove left over windows from desktop.
    for(int i = 0; i < allRackWindows.size(); i++)
    {
        //std::cout << "All RAcks Name: " << allRackWindows[i]->getName() << std::endl;
        
        bool found = false;
        for(int j = 0; j < windows.size(); j++)
        {
            if(allRackWindows[i]->getName() == windows[j]->getName())
            {
                found = true;
                break;
            }
        }
        if(!found && allRackWindows[i]->isOnDesktop()) allRackWindows[i]->removeFromDesktop();
    }
    
    //4. reposition existing windows by rebuilding the rack.
    rebuildRack();
}

