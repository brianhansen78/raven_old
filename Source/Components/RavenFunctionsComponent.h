
#ifndef Raven_RavenMainFunctionsComponent_h
#define Raven_RavenMainFunctionsComponent_h

#include "RavenConfig.h"
#include "RavenFunctionsContainer.h"
#include "RavenTouchComponent.h"
#include "RavenWindowComponent.h"
#include "NscGuiElement.h"
#include "RavenButton.h"
#include "RavenTextDisplay.h"
#include "RavenHotKeysComponent.h"
#include "RavenRollComponent.h"
#include "RavenTransportComponent.h"
#include "RavenMemoryComponent.h"
#include "RavenEditMixComponent.h"
#include "RavenPunchComponent.h"
#include "RavenInputGroupSuspendComponent.h"
#include "RavenUtilitiesComponent.h"
#include "RavenModifiersComponent.h"
#include "RavenTrackBankComponent.h"
#include "RavenEditComponent.h"
#include "RavenCounterFunctionsComponent.h"
#include "RavenSaveLayoutComponent.h"
#include "RavenNavPadComponent.h"
#include "RavenFloatingNavPadComponent.h"
#include "RavenHotKeyProgrammingComponent.h"
#include "RavenFloatingEssentialsComponent.h"
#include "RavenWindowManager.h"


class RavenFunctionsComponent  : public RavenWindowComponent, public RavenFunctionsContainer, public Timer, public ActionBroadcaster
{
public:
    //==============================================================================
    RavenFunctionsComponent (bool bottomImage)
    {
        
        if(bottomImage)
        {
            singlePanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "TOOLBAR/TOOLBAR/Solo/ToolBar/ToolBarPanel_03.png"));
        }
        else
        {
            singlePanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "TOOLBAR/TOOLBAR/Solo/ToolBarExpand/ToolBarPanel_02.png"));
        }
        setSize (singlePanelImg.getWidth(), singlePanelImg.getHeight());
        
        
        #if !RAVEN_DEBUG
        //Invisible Mouse Hiding Component (hides mouse and intercepts clicks)
        //MacHideBackGroundMouse enables this to work
        mouseHider = new MouseHideComponent(getWidth(), getHeight());
        mouseHider->setName("mouseHider");
        addAndMakeVisible(mouseHider);
        #endif
    }

    void addFunctionsRavenTouchButton(RavenButton* button)
    {
        ravenTouchButtons.add(button);
    }
    void addFunctionsRavenNSCTouchButton(RavenNSCButton* button)
    {
        ravenNSCTouchButtons.add(button);
    }
    
    void paint(Graphics &g)
    {
        //g.fillAll(Colours::hotpink);
        g.drawImageAt(singlePanelImg, 0, 0);
    }
        
    void timerCallback() { /*lastWinButton->setToggleState(insWinButton->getToggleState(), false);*/}
    void buttonStateChanged (Button*){};
    void buttonClicked (Button* b){};

    
    
private:
    
Image bottomPanelImg, singlePanelImg, customPanelImg, railImg;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenFunctionsComponent)
    
};

#endif
