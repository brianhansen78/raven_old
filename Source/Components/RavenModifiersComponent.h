

#ifndef Raven_RavenModifiersComponent_h
#define Raven_RavenModifiersComponent_h

#include "RavenButton.h"
#include "RavenFunctionsComponent.h"
#include "RavenTouchComponent.h"
#include "RavenMacUtilities.h"

class RavenModifiersComponent : public RavenTouchComponent, public Button::Listener
{
public:
    RavenModifiersComponent(RavenFunctionsContainer *owner)
    {
        
        int xPos = 0;
        shiftButton = new RavenButton();
        RAVEN_BUTTON_1(shiftButton);
        shiftButton->setTopLeftPosition(xPos, 0);
        shiftButton->addListener(this);
        shiftButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Modifiers/images/modifier-on_01.png", "TOOLBAR/FUNCTIONMODULE/Modifiers/images/modifier-off_01.png");
        shiftButton->getProperties().set("isModifier", true);
        ravenTouchModifiers.add(shiftButton);
        addAndMakeVisible(shiftButton);
        xPos += shiftButton->getWidth();
        
        controlButton = new RavenButton();
        RAVEN_BUTTON_1(controlButton);
        controlButton->setTopLeftPosition(xPos, 0);
        controlButton->addListener(this);
        controlButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Modifiers/images/modifier-on_03.png", "TOOLBAR/FUNCTIONMODULE/Modifiers/images/modifier-off_03.png");
        controlButton->getProperties().set("isModifier", true);
        ravenTouchModifiers.add(controlButton);
        addAndMakeVisible(controlButton);
        xPos += controlButton->getWidth();
        
        altopButton = new RavenButton();
        RAVEN_BUTTON_1(altopButton);
        altopButton->setTopLeftPosition(xPos, 0);
        //altopButton->setClickingTogglesState(true);
        altopButton->addListener(this);
        altopButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Modifiers/images/modifier-on_02.png", "TOOLBAR/FUNCTIONMODULE/Modifiers/images/modifier-off_02.png");
        altopButton->getProperties().set("isModifier", true);
        ravenTouchModifiers.add(altopButton);
        addAndMakeVisible(altopButton);
        xPos += altopButton->getWidth();
        
        commandButton = new RavenButton();
        RAVEN_BUTTON_1(commandButton);
        commandButton->setTopLeftPosition(xPos, 0);
        commandButton->addListener(this);
        commandButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Modifiers/images/modifier-on_04.png", "TOOLBAR/FUNCTIONMODULE/Modifiers/images/modifier-off_04.png");
        commandButton->setName("commandButton");
        commandButton->getProperties().set("isModifier", true);
        ravenTouchModifiers.add(commandButton);
        addAndMakeVisible(commandButton);
        xPos += commandButton->getWidth();
        
        setSize(xPos, altopButton->getHeight());
    }
    
    void buttonStateChanged(Button *b)
    {
        bool down = b->getToggleState();
        
        if     (b == shiftButton)   MacKeyPress::toggleModifier(eRavenModifer_Shift, down);
        else if(b == controlButton) MacKeyPress::toggleModifier(eRavenModifer_Control, down);
        else if(b == altopButton)   MacKeyPress::toggleModifier(eRavenModifer_Option, down);
        else if(b == commandButton) MacKeyPress::toggleModifier(eRavenModifer_Command, down);
    }
    void buttonClicked(Button *b)
    {
        
    }

    void unlatchModifiers()
    {
        LOCK_JUCE_THREAD
        {
            if(shiftButton->getToggleState()) shiftButton->setToggleState(false, true);
            if(controlButton->getToggleState()) controlButton->setToggleState(false, true);
            if(altopButton->getToggleState())   altopButton->setToggleState(false, true);
            if(commandButton->getToggleState()) commandButton->setToggleState(false, true);
        }
    }
    
    bool getCommandButtonState() { return commandButton->getToggleState(); }
    bool getShiftButtonState(){ return shiftButton->getToggleState();}
    bool getOptionButtonState(){ return altopButton->getToggleState();}
    bool getControlButtonState(){ return controlButton->getToggleState();}
    
private:
    
    RavenButton *shiftButton;
    RavenButton *controlButton;
    RavenButton *altopButton;
    RavenButton *commandButton;
        
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenModifiersComponent)

};

#endif
