
#ifndef Raven_RavenPunchComponent_h
#define Raven_RavenPunchComponent_h

#include "RavenButton.h"
#include "RavenFunctionsComponent.h"
#include "RavenTouchComponent.h"

class RavenPunchComponent : public RavenTouchComponent, public Button::Listener
{
public:
    RavenPunchComponent(RavenFunctionsContainer *owner)
    {
        
        int xPos = 0;
        
        punchButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_2(punchButton);
        punchButton->setTopLeftPosition(xPos, 0);
        punchButton->addListener(owner);
        punchButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/punchInOut/images/punchInOut-on_01.png", "TOOLBAR/FUNCTIONMODULE/punchInOut/images/punchInOut-off_01.png");
        punchButton->setTag(eNSCGSTransportPunchMode);
        ravenNSCTouchButtons.add(punchButton);
        punchButton->setClickingTogglesState(true);// TOGGLE
        addAndMakeVisible(punchButton);
        xPos += punchButton->getWidth();
        
        RavenNSCButton *cycleButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_2(cycleButton);
        cycleButton->setTopLeftPosition(xPos, 0);
        cycleButton->addListener(owner);
        cycleButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/punchInOut/images/punchInOut-on_02.png", "TOOLBAR/FUNCTIONMODULE/punchInOut/images/punchInOut-off_02.png");
        cycleButton->setTag(eNSCGSTransportLoopMode);
        cycleButton->setClickingTogglesState(true);// TOGGLE
        ravenNSCTouchButtons.add(cycleButton);
        addAndMakeVisible(cycleButton);
        xPos += cycleButton->getWidth();

        RavenNSCButton *inButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_2(inButton);
        inButton->setTopLeftPosition(xPos, 0);
        inButton->addListener(owner);
        inButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/punchInOut/images/punchInOut-on_04.png", "TOOLBAR/FUNCTIONMODULE/punchInOut/images/punchInOut-off_04.png");
        inButton->setTag(eNSCGSSetLeft);
        ravenNSCTouchButtons.add(inButton);
        addAndMakeVisible(inButton);
        xPos += inButton->getWidth();
        
// Not necessary for Pro Tools build, but we may need these for Logic
//        RavenNSCButton *leftdropButton = new RavenNSCButton(owner);
//        RAVEN_BUTTON_2(leftdropButton);
//        leftdropButton->setTopLeftPosition(xPos, 0);
//        leftdropButton->addListener(owner);
//        leftdropButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/punchInOut/images/punchInOut-on_05.png", "TOOLBAR/FUNCTIONMODULE/punchInOut/images/punchInOut-off_05.png");
//        leftdropButton->setTag(eNSCGSSetLeftDrop);
//        ravenNSCTouchButtons.add(leftdropButton);
//        addAndMakeVisible(leftdropButton);
//        xPos += leftdropButton->getWidth();
//        
//        RavenNSCButton *rightdropButton = new RavenNSCButton(owner);
//        RAVEN_BUTTON_2(rightdropButton);
//        rightdropButton->setTopLeftPosition(xPos, 0);
//        rightdropButton->addListener(owner);
//        rightdropButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/punchInOut/images/punchInOut-on_06.png", "TOOLBAR/FUNCTIONMODULE/punchInOut/images/punchInOut-off_06.png");
//        rightdropButton->setTag(eNSCGSSetRightDrop);
//        ravenNSCTouchButtons.add(rightdropButton);
//        addAndMakeVisible(rightdropButton);
//        xPos += rightdropButton->getWidth();
        
        RavenNSCButton *outButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_2(outButton);
        outButton->setTopLeftPosition(xPos, 0);
        outButton->addListener(owner);
        outButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/punchInOut/images/punchInOut-on_07.png", "TOOLBAR/FUNCTIONMODULE/punchInOut/images/punchInOut-off_07.png");
        outButton->setTag(eNSCGSSetRight);
        ravenNSCTouchButtons.add(outButton);
        addAndMakeVisible(outButton);
        xPos += outButton->getWidth();
    
        setSize(xPos, outButton->getHeight());
        
    }
    
    void buttonStateChanged (Button*)
    {
        
    }
    void buttonClicked (Button* b)
    {
        
    }
    
    RavenNSCButton* getPunchButton(){   return punchButton;}
    
private:
    RavenNSCButton *punchButton;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenPunchComponent)
    
};

#endif
