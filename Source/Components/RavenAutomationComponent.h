

#ifndef Raven_RavenAutomationComponent_h
#define Raven_RavenAutomationComponent_h

#include "RavenTouchComponent.h"

class RavenAutomationComponent: public RavenTouchComponent
{
public:
    RavenAutomationComponent(int bank, int chan)
    {
        int xOff, yOff, buttonHeight, spacing;
        
        if(RAVEN_24)
        {
            yOff = 28;
            xOff = 9;
            buttonHeight = 21;
            spacing = 4;
        }
        else
        {
            yOff = 27;
            xOff = 0;
            buttonHeight = 21;
            spacing = 9;//4;
        }
        
        offButton = new RavenButton();
        offButton->setName("auto_off");
        
        if(RAVEN_24)
            offButton->setImagePaths("FLAT_GRAPHICS/AUTOMATION_STATUS/buttonOn/images/off.png", "FLAT_GRAPHICS/AUTOMATION_STATUS/buttonOff/images/off.png");
        else
            offButton->setImagePaths("FLAT_GRAPHICS/AUTOMATION_STATUS32/buttonOn/images/off.png", "FLAT_GRAPHICS/AUTOMATION_STATUS32/buttonOff/images/off.png");
        
        offButton->setTopLeftPosition(xOff, yOff);
        addAndMakeVisible(offButton);
        ravenTouchButtons.add(offButton);
        yOff += buttonHeight + spacing;
        
        latchButton = new RavenButton();
        latchButton->setName("auto_latch");
        
        if(RAVEN_24)
            latchButton->setImagePaths("FLAT_GRAPHICS/AUTOMATION_STATUS/buttonOn/images/latch.png", "FLAT_GRAPHICS/AUTOMATION_STATUS/buttonOff/images/latch.png");
        else
            latchButton->setImagePaths("FLAT_GRAPHICS/AUTOMATION_STATUS32/buttonOn/images/latch.png", "FLAT_GRAPHICS/AUTOMATION_STATUS32/buttonOff/images/latch.png");
        
        latchButton->setTopLeftPosition(xOff, yOff);
        addAndMakeVisible(latchButton);
        ravenTouchButtons.add(latchButton);
        yOff += buttonHeight + spacing;
        
        readButton = new RavenButton();
        readButton->setName("auto_read");
        
        if(RAVEN_24)
            readButton->setImagePaths("FLAT_GRAPHICS/AUTOMATION_STATUS/buttonOn/images/read.png", "FLAT_GRAPHICS/AUTOMATION_STATUS/buttonOff/images/read.png");
        else
            readButton->setImagePaths("FLAT_GRAPHICS/AUTOMATION_STATUS32/buttonOn/images/read.png", "FLAT_GRAPHICS/AUTOMATION_STATUS32/buttonOff/images/read.png");
        
        readButton->setTopLeftPosition(xOff, yOff);
        addAndMakeVisible(readButton);
        ravenTouchButtons.add(readButton);
        yOff += buttonHeight + spacing;
        
        touchButton = new RavenButton();
        touchButton->setName("auto_touch");
        
        if(RAVEN_24)
            touchButton->setImagePaths("FLAT_GRAPHICS/AUTOMATION_STATUS/buttonOn/images/touch.png", "FLAT_GRAPHICS/AUTOMATION_STATUS/buttonOff/images/touch.png");
        else
            touchButton->setImagePaths("FLAT_GRAPHICS/AUTOMATION_STATUS32/buttonOn/images/touch.png", "FLAT_GRAPHICS/AUTOMATION_STATUS32/buttonOff/images/touch.png");
        
        touchButton->setTopLeftPosition(xOff, yOff);
        addAndMakeVisible(touchButton);
        ravenTouchButtons.add(touchButton);
        yOff += buttonHeight + spacing;
        
        writeButton = new RavenButton();
        writeButton->setName("auto_write");
        
        if(RAVEN_24)
            writeButton->setImagePaths("FLAT_GRAPHICS/AUTOMATION_STATUS/buttonOn/images/write.png", "FLAT_GRAPHICS/AUTOMATION_STATUS/buttonOff/images/write.png");
        else
            writeButton->setImagePaths("FLAT_GRAPHICS/AUTOMATION_STATUS32/buttonOn/images/write.png", "FLAT_GRAPHICS/AUTOMATION_STATUS32/buttonOff/images/write.png");
        
        writeButton->setTopLeftPosition(xOff, yOff);
        addAndMakeVisible(writeButton);
        ravenTouchButtons.add(writeButton);
        yOff += buttonHeight + spacing;
        
        trimButton = new RavenButton();
        trimButton->setName("auto_trim");
        
        if(RAVEN_24)
            trimButton->setImagePaths("FLAT_GRAPHICS/AUTOMATION_STATUS/buttonOn/images/trim.png", "FLAT_GRAPHICS/AUTOMATION_STATUS/buttonOff/images/trim.png");
        else
            trimButton->setImagePaths("FLAT_GRAPHICS/AUTOMATION_STATUS32/buttonOn/images/trim.png", "FLAT_GRAPHICS/AUTOMATION_STATUS32/buttonOff/images/trim.png");
        
        trimButton->setTopLeftPosition(xOff, yOff);
        addAndMakeVisible(trimButton);
        ravenTouchButtons.add(trimButton);
        yOff += buttonHeight + spacing;
        
        if(RAVEN_24)
            autoPanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/AUTOMATION_STATUS/panel/AutmationMenu-Panel.png"));
        else
            autoPanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/AUTOMATION_STATUS32/panel/AutmationMenu-Panel.png"));
        
        Rectangle<int>clipRect(0, 0, autoPanelImg.getWidth(), autoPanelImg.getHeight()-26);
        //autoPanelImg = autoPanelImg.getClippedImage(clipRect);
        
        setSize(autoPanelImg.getWidth(), autoPanelImg.getHeight());
    }
    
    void paint(Graphics &g)
    {
        g.drawImageAt(autoPanelImg, 0, 0);
    }
    
    void setAutoButtonListener(Button::Listener *listener)
    {
        offButton->addListener(listener);
        latchButton->addListener(listener);
        readButton->addListener(listener);
        touchButton->addListener(listener);
        writeButton->addListener(listener);
        trimButton->addListener(listener);
    }
    
    
private:
    
    Image autoPanelImg;
    RavenButton *offButton;
    RavenButton *latchButton;
    RavenButton *readButton;
    RavenButton *touchButton;
    RavenButton *writeButton;
    RavenButton *trimButton;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenAutomationComponent)
};

#endif
