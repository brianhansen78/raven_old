

#ifndef Raven_RavenInputGroupSuspendComponent_h
#define Raven_RavenInputGroupSuspendComponent_h

#include "RavenButton.h"
#include "RavenFunctionsComponent.h"
#include "RavenTouchComponent.h"

class RavenInputGroupSuspendComponent : public RavenTouchComponent, public Button::Listener
{
public:
    RavenInputGroupSuspendComponent(RavenFunctionsContainer *owner)
    {
        
        int xPos = 0;
        
        RavenNSCButton *inmonButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_4(inmonButton);
        inmonButton->setClickingTogglesState(true); // TOGGLE
        inmonButton->setTopLeftPosition(xPos, 0);
        inmonButton->addListener(owner);
        inmonButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Global/images/global-on_01.png", "TOOLBAR/FUNCTIONMODULE/Global/images/global-off_01.png");
        inmonButton->setTag(eNSCGSMonitor);
        ravenNSCTouchButtons.add(inmonButton);
        addAndMakeVisible(inmonButton);
        xPos += inmonButton->getWidth();
        
        RavenNSCButton *autoinButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_4(autoinButton);
        autoinButton->setClickingTogglesState(true); // TOGGLE
        autoinButton->setTopLeftPosition(xPos, 0);
        autoinButton->addListener(owner);
        autoinButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Global/images/global-on_02.png", "TOOLBAR/FUNCTIONMODULE/Global/images/global-off_02.png");
        autoinButton->setTag(eNSCGSAutoStatus);
        ravenNSCTouchButtons.add(autoinButton);
        addAndMakeVisible(autoinButton);
        xPos += autoinButton->getWidth();
        
        RavenNSCButton *creategroupButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_4(creategroupButton);
        //creategroupButton->setClickingTogglesState(true); //TOGGLE
        creategroupButton->setTopLeftPosition(xPos, 0);
        creategroupButton->addListener(owner);
        creategroupButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Global/images/global-on_03.png", "TOOLBAR/FUNCTIONMODULE/Global/images/global-off_03.png");
        creategroupButton->setTag(eNSCGSGroupCreate);
        ravenNSCTouchButtons.add(creategroupButton);
        addAndMakeVisible(creategroupButton);
        xPos += creategroupButton->getWidth();
        
        RavenNSCButton *susgroupButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_4(susgroupButton);
        susgroupButton->setClickingTogglesState(true); //TOGGLE
        susgroupButton->setTopLeftPosition(xPos, 0);
        susgroupButton->addListener(owner);
        susgroupButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Global/images/global-on_04.png", "TOOLBAR/FUNCTIONMODULE/Global/images/global-off_04.png");
        susgroupButton->setTag(eNSCGSGroupSuspend);
        ravenNSCTouchButtons.add(susgroupButton);
        addAndMakeVisible(susgroupButton);
        xPos += susgroupButton->getWidth();
        
        RavenNSCButton *autosusButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_4(autosusButton);
        autosusButton->setClickingTogglesState(true); //TOGGLE
        autosusButton->setTopLeftPosition(xPos, 0);
        autosusButton->addListener(owner);
        autosusButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Global/images/global-on_05.png", "TOOLBAR/FUNCTIONMODULE/Global/images/global-off_05.png");
        autosusButton->setTag(eNSCGSAutoSuspend);
        ravenNSCTouchButtons.add(autosusButton);
        addAndMakeVisible(autosusButton);
        xPos += autosusButton->getWidth();
        
        setSize(xPos, inmonButton->getHeight());
        
    }
    
    void buttonStateChanged (Button*)
    {
        
    }
    void buttonClicked (Button* b)
    {
        
    }
    
private:
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenInputGroupSuspendComponent)
    
};

#endif
