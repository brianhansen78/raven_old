

#ifndef Raven_RavenCounterComponent_h
#define Raven_RavenCounterComponent_h

#include "RavenButton.h"
#include "RavenTextDisplay.h"
#include "RavenWindowComponent.h"

class RavenCounterComponent : public RavenWindowComponent
{
public:
    
    RavenCounterComponent()
    {
        
        File imgFile(String(GUI_PATH) + "TOOLBAR/FUNCTIONMODULE/Counter/counter-void-sm.png");
        counterImg = ImageCache::getFromFile(imgFile);
        setSize(counterImg.getWidth(), counterImg.getHeight());
        
        mouseHider = new MouseHideComponent(getWidth(), getHeight());
        mouseHider->setName("MouseHider");
        addAndMakeVisible(mouseHider);
    }
    
    void initializeCounter(NscGuiContainer *owner)
    {
        counterDisplay = new RavenTextDisplay(owner);
        counterDisplay->setBounds(10, 10, getWidth()-20, getHeight()-20);
        counterDisplay->setJustificationType(Justification::centred);
        
        Font font = counterDisplay->getFont();
        font.setHeight(48.0f);
        counterDisplay->setFont(font);

        counterDisplay->setColour(Label::textColourId, Colour(255, 164, 5));
        counterDisplay->setText("00:00:00", false);
        counterDisplay->setTag(eNSCTimeDisplay);
        counterDisplay->setBankIndex(0);
        counterDisplay->setChannelIndex(kNscTimeDisplayIdentifier);
        addAndMakeVisible(counterDisplay);
    }
    
    void paint(Graphics &g)
    {
        g.drawImageAt(counterImg, 0, 0);
    }
    
private:
    
    Image counterImg;
    ScopedPointer<RavenTextDisplay> counterDisplay;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenCounterComponent)
    
};

#endif
