
#ifndef Raven_NewSDK_RavenMeter_h
#define Raven_NewSDK_RavenMeter_h

#include "RavenConfig.h"
#include "RavenInterpolator.h"

#define RAVEN_CLEAR_CLIP_HEIGHT         50
#define RAVEN_CLEAR_CLIP_HEIGHT_LARGE   100

//#if !RAVEN_24
const int levelIndexHighPixel[] =       {164, 139, 126, 114, 102, 90, 78, 65, 53, 41, 17};//tops of buttons in pixels
const int hybridLevelIndexHighPixel[] = {140, 121, 110, 98,  88,  77, 66, 56, 45, 34, 13};//tops of buttons in pixels
/*bottom (SKIP -60!), -50 top, -40 top, -30 top, -20 top, -14 top, -10 top, -8 top, -6 top, -4 top (SKIP -2!), 0 top*/ 
//#else
//const int levelIndexHighPixel[] = {164, 139, 127, 114, 102, 90, 78, 66, 54, 41, 17};//tops of buttons in pixels
//const int hybridLevelIndexHighPixel[] = {164, 139, 127, 114, 102, 90, 78, 66, 54, 41, 17};//tops of buttons in pixels
//#endif

class RavenMeter : public Component, public NscGuiElement, public Button::Listener
{
public:
    RavenMeter(NscGuiContainer *owner, int bank, int chan, bool _isRightSide) : NscGuiElement(owner), level(0.0), clip(false), peak(false), isRightSide(_isRightSide), hybridMode(false), interpolator(RAVEN_METER_SMOOTHING, 0.0), clearClipTapAreaHeight(50), needsUpdating(false)
    {
        initializeImages();
        
        meterClip = new RavenNSCButton(owner);
        meterClip->setTopLeftPosition(0, 0);
        
        meterPeak = new RavenNSCButton(owner);
        meterPeak->setTopLeftPosition(0, meterClip->getBottom()+5);
        
        if(isRightSide == false)
        {
            setTag(eNSCTrackMeter1);
            meterClip->setTag(eNSCTrackMeterClip1);
            meterPeak->setTag(eNSCTrackMeterPeak1);
        }
        else
        {
            setTag(eNSCTrackMeter2);
            meterClip->setTag(eNSCTrackMeterClip2);
            meterPeak->setTag(eNSCTrackMeterPeak2);
        }
        
        meterClip->addListener(this);
        meterPeak->addListener(this);
        
//        setChannelIndex(chan);
//        setBankIndex(bank);
//        setClipPeakChannelBankIndex(chan, bank);
    }
    ~RavenMeter(){}
    
    void setHybridMode(bool _hybridMode)
    {
        hybridMode = _hybridMode;
        initializeImages();
    }
    
    void initializeImages()
    {
        File offImgFile, onImgFile;
        
        if(hybridMode)
        {
            offImgFile = File(String(GUI_PATH) + "FloatingMixer/FloatingMixerGraphics/MixLedMeterYellow/LedMeter-Off.png");
            onImgFile = File(String(GUI_PATH) + "FloatingMixer/FloatingMixerGraphics/MixLedMeterYellow/LedMeter-On.png");
        }
        else if(RAVEN_24)
        {
            offImgFile = File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixLedMeter/LedMeter-Off.png");
            onImgFile = File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixLedMeter/LedMeter-On.png");
        }
        else
        {
            offImgFile = File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixLedMeter/LedMeter-Off.png");
            onImgFile = File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/MixLedMeter/LedMeter-On.png");
        }
        
        clearClipTapAreaHeight = RAVEN_CLEAR_CLIP_HEIGHT;
        
        meterOffImage = ImageCache::getFromFile(offImgFile);
        meterOnImage = ImageCache::getFromFile(onImgFile);
        
        if(hybridMode)
        {
            if(isRightSide)
            {
                Rectangle<int> clipRectOff(9, 0, 8, meterOffImage.getHeight());//these numbers don't make sense but they look right...
                meterOffImage = meterOffImage.getClippedImage(clipRectOff);
                Rectangle<int> clipRectOn(9, 0, 8, meterOnImage.getHeight());//these numbers don't make sense but they look right...
                meterOnImage = meterOnImage.getClippedImage(clipRectOn);
                
                meterNumbersImage = ImageCache::getFromFile(offImgFile);
                Rectangle<int> clipRect(17, 0, 15, meterOffImage.getHeight());
                meterNumbersImage = meterNumbersImage.getClippedImage(clipRect);
            }
            else
            {
                Rectangle<int> clipRectOff(0, 0, 9, meterOffImage.getHeight());
                meterOffImage = meterOffImage.getClippedImage(clipRectOff);
                Rectangle<int> clipRectOn(0, 0, 9, meterOnImage.getHeight());
                meterOnImage = meterOnImage.getClippedImage(clipRectOn);
            }
        }
        else if(RAVEN_24)
        {
            if(isRightSide)
            {
                Rectangle<int> clipRectOff(13, 0, 9, meterOffImage.getHeight());//these numbers don't make sense but they look right...
                meterOffImage = meterOffImage.getClippedImage(clipRectOff);
                Rectangle<int> clipRectOn(13, 0, 9, meterOnImage.getHeight());//these numbers don't make sense but they look right...
                meterOnImage = meterOnImage.getClippedImage(clipRectOn);
                
                meterNumbersImage = ImageCache::getFromFile(offImgFile);
                Rectangle<int> clipRect(22, 0, 14, meterOffImage.getHeight());
                meterNumbersImage = meterNumbersImage.getClippedImage(clipRect);
            }
            else
            {
                Rectangle<int> clipRectOff(0, 0, 13, meterOffImage.getHeight());
                meterOffImage = meterOffImage.getClippedImage(clipRectOff);
                Rectangle<int> clipRectOn(0, 0, 13, meterOnImage.getHeight());
                meterOnImage = meterOnImage.getClippedImage(clipRectOn);
            }
        }
        else
        {
            if(isRightSide)
            {
                Rectangle<int> clipRectOff(10, 0, 7, meterOffImage.getHeight());//these numbers don't make sense but they look right...
                meterOffImage = meterOffImage.getClippedImage(clipRectOff);
                Rectangle<int> clipRectOn(10, 0, 7, meterOnImage.getHeight());//these numbers don't make sense but they look right...
                meterOnImage = meterOnImage.getClippedImage(clipRectOn);
                
                meterNumbersImage = ImageCache::getFromFile(offImgFile);//onImgFile
                Rectangle<int> clipRect(17, 0, 9, meterOffImage.getHeight());
                meterNumbersImage = meterNumbersImage.getClippedImage(clipRect);
            }
            else
            {
                Rectangle<int> clipRectOff(0, 0, 10, meterOffImage.getHeight());
                meterOffImage = meterOffImage.getClippedImage(clipRectOff);
                Rectangle<int> clipRectOn(0, 0, 10, meterOnImage.getHeight());
                meterOnImage = meterOnImage.getClippedImage(clipRectOn);
            }
        }
        setSize(meterOnImage.getWidth()+meterNumbersImage.getWidth(), meterOnImage.getHeight());
    }
    
    void paint (Graphics& g)
    {
        //clamp level between 0.0 and 1.0
        if (level < 0.0){ level = 0.0;}
        else if (level > 1.0) { level = 1.0;}
        //printf("level: %f\n", level);
        drawMeterPeak(g);
        
        g.setOpacity(0.65);
        g.drawImageAt(meterOffImage, 0, 0);
        g.setOpacity(1.0);
        //draw numbers. This is just blank if it's not the right side:
        if(isRightSide){ g.drawImageAt(meterNumbersImage, meterOffImage.getWidth(), 0);}

        int w = meterOnImage.getWidth();
        int h = meterOnImage.getHeight();
        int top = 0;
        if(!hybridMode)
            top = levelIndexHighPixel[convertHUIMeterLevelToIndex(level)];
        else
            top = hybridLevelIndexHighPixel[convertHUIMeterLevelToIndex(level)];
        
        g.drawImage(meterOnImage, 0, top, w, h, 0, top, w, h);
        
        /*
        void drawImage (const Image& imageToDraw,
                        int destX, int destY, int destWidth, int destHeight,
                        int sourceX, int sourceY, int sourceWidth, int sourceHeight,
                        bool fillAlphaChannelWithCurrentBrush = false) const; */
        
        const int singleLEDHeightInPixels = 0.085*h;//make this a value in pixels (the size of one LED dot)
        int peakLevelIndex = convertHUIMeterLevelToIndex(meterPeak->getNscValue());
        if(peakLevelIndex > 0){ peakLevelIndex -= 1;}
        int peakHeight = 0;
        if(!hybridMode)
            peakHeight = levelIndexHighPixel[peakLevelIndex];
        else
            peakHeight = hybridLevelIndexHighPixel[peakLevelIndex];
        
        if(clip)
        {
            g.drawImage(meterOnImage, 0, 0, w, singleLEDHeightInPixels, 0, 0, w, singleLEDHeightInPixels);
        }
        if(peak)
        {
            g.drawImage(meterOnImage, 0, peakHeight, w, singleLEDHeightInPixels, 0, peakHeight, w, singleLEDHeightInPixels);
        }
    }
    
    
    
    void buttonStateChanged (Button* b)
    {
        if(b == meterPeak)
        {
            if(b->getToggleState())
            {
                peak = true;
                //store current meter level and keep painted until the next one of these comes in..
            }
            else{ peak = false;}
        }
        else if(b == meterClip)
        {
            if(b->getToggleState()) clip = true;
            else                    clip = false;
        }
        repaint();
    }
    void buttonClicked (Button* b){}
    
    void setClipPeakChannelBankIndex(int chan, int bank)
    {
        meterClip->setChannelIndex(chan);
        meterClip->setBankIndex(bank);
        meterPeak->setChannelIndex(chan);
        meterPeak->setBankIndex(bank);
    }
    
    // NscGuiElement overrides
	virtual void updateNscValue(float value)			// callback when DAW changes a value; generally
    {
        if(value != level) //only update if value has changed
        {
            level = value;
            needsUpdating = true;
            //repaint(); //repainted in thread now
        }
    }
    
    bool checkMeterClipTouch(int touchX, int touchY)
    {
        if(!isVisible())
            return false;

        if(touchX >= getScreenX() && touchX <= getScreenX()+getWidth() && touchY >= getScreenY() && touchY <= getScreenY()+clearClipTapAreaHeight)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    void updateLevel()
    {
        if(needsUpdating)
        {
            repaint();
            needsUpdating = false;
        }
    }
    
    void drawMeterPeak(Graphics& g)
    {
        //float peakLevel = meterPeak->getNscValue();
        //printf("peak level: %f\n", peakLevel);
    }
    
    int convertHUIMeterLevelToIndex(float HUILevel)
    {
        //printf("hui level: %.20f\n", HUILevel);
        int index = 0;
        //Todo: faster floating point comparison, although I think this is quite efficient 
        if(fabs(HUILevel-0.083333) < 0.0001){ index = 1;}
        else if(fabs(HUILevel-0.166667f) < 0.0001f){ index = 2;}
        else if(fabs(HUILevel-0.250000f) < 0.0001f){ index = 3;}
        else if(fabs(HUILevel-0.333333f) < 0.0001f){ index = 4;}
        else if(fabs(HUILevel-0.416667f) < 0.0001f){ index = 5;}
        else if(fabs(HUILevel-0.500000f) < 0.0001f){ index = 6;}
        else if(fabs(HUILevel-0.583333f) < 0.0001f){ index = 7;}
        else if(fabs(HUILevel-0.666667f) < 0.0001f){ index = 8;}
        else if(fabs(HUILevel-0.750000f) < 0.0001f){ index = 9;}
        else if(fabs(HUILevel-0.833333f) < 0.0001f){ index = 10;}
        else if(fabs(HUILevel-0.916667f) < 0.0001f){ index = 11;}
        return index;
        
        /*
         -infdb to -50db = 0.0
         -50db to -40db = 0.0833333
         -40db to -30db = 0.166667
         -30db to -20db = .25
         -20db to -14db = .333333
         -14db to = -10db = .416667
         -10db to -8db = .5
         - 8db to -6db = .583333
         -6db to -4db = .666667
         - 4db to -2db = .75
         -2db to 0db = .833333
         Peak can go to 0.916667 but that's because everything it outputs is one tick over reality
         */
    }
    
    void setClipped(bool _clipped)
    {
        //Has to be called after initialization to make sure that all meters start with no clipping
        //HUI sends out bogus clipping information that makes some of the tracks start clipped
        meterClip->setToggleState(_clipped, false);
        clip = _clipped;
        //if(!_clipped){ peak = false;}//might want to remove clipping/peaking whenever mixer channel turns blank
        //problem with this is that HUI only sets peak to true again if it thinks it's false, so it won't turn on again until another cycle... 
    }
    
    bool isClipped(){ return clip;}

    
private:
    
	Image meterOffImage;
    Image meterOnImage;
    Image meterNumbersImage;
    float level;
    bool clip, peak;
    bool isRightSide;
    bool hybridMode;
    
    //invisible HUT buttons to determine peak and clip drawing
    ScopedPointer<RavenNSCButton> meterClip;
    ScopedPointer<RavenNSCButton> meterPeak;
    
    RavenInterpolator interpolator;
    int clearClipTapAreaHeight;
    
    bool needsUpdating;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenMeter)
    
};

#endif
