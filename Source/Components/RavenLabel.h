
#ifndef Raven_RavenLabel_h
#define Raven_RavenLabel_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "../RavenCustomFontJUCEResource.h"
#include "../RavenFuturaFont.h"

class RavenLabel : public Label
{
public:
    RavenLabel()
    {

        MemoryInputStream mis(RavenCustomFontJUCEResource::ravenlcdfont, RavenCustomFontJUCEResource::ravenlcdfontSize, false);
        
        typeface = new CustomTypeface(mis);
        font = Font(typeface);
        
        font.setHeight(10.0f);
        setJustificationType(Justification::left);
        
        setFont(font);
        setColour(textColourId, Colours::red);
        setText("test", false);
        
        setInterceptsMouseClicks(false, false);
                
    }
    
    void setFontSize(float size)
    {
        font.setHeight(size);
        setFont(font);
    }

protected:
    CustomTypeface::Ptr typeface;
    Font font;
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenLabel)
    
};

class RavenFuturaLabel : public Label
{
public:
    RavenFuturaLabel()
    {
        MemoryInputStream mis(RavenFuturaFont::ravenfuturafont, RavenFuturaFont::ravenfuturafontSize, false);
        
        typeface = new CustomTypeface(mis);
        
        font = Font(typeface);
        
        font.setHeight(24.0f);
        setJustificationType(Justification::centred);
        
        setFont(font);
        setColour(Label::textColourId, Colour(240, 224, 41));
        setText("", false);
        
        setInterceptsMouseClicks(false, false);
        
    }
    
    void setFontSize(float size)
    {
        font.setHeight(size);
        setFont(font);
    }
    
protected:
    CustomTypeface::Ptr typeface;
    Font font;
};

#endif
