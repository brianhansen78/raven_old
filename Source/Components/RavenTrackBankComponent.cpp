//
//  RavenTrackBankComponent.cpp
//  Raven
//
//  Created by Joshua Dickinson on 6/25/13.
//
//

#include "RavenTrackBankComponent.h"
#include "RavenRackContainer.h"

void RavenTrackBankComponent::buttonClicked(Button* b)
{
    if(b != switchButton)
        sendActionMessage("set mixer automation off");
    
    if(b == switchButton)
    {
        if(b->getToggleState())
        {
            if(RAVEN_24)
            {
                bankleftButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Navigation/navigation24/images/navigation24-on_02.png", "TOOLBAR/FUNCTIONMODULE/Navigation/navigation24/images/navigation24-off_02.png");
                bankrightButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Navigation/navigation24/images/navigation24-on_03.png", "TOOLBAR/FUNCTIONMODULE/Navigation/navigation24/images/navigation24-off_03.png");
            }
            else
            {
                bankleftButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Navigation/navigation32/navigation32-on_02.png", "TOOLBAR/FUNCTIONMODULE/Navigation/navigation32/navigation32-off_02.png");
                bankrightButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Navigation/navigation32/navigation32-on_03.png", "TOOLBAR/FUNCTIONMODULE/Navigation/navigation32/navigation32-off_03.png");
            }
        }
        else
        {
            bankleftButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-on_02.png", "TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-off_02.png");
            bankrightButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-on_03.png", "TOOLBAR/FUNCTIONMODULE/Navigation/navigation8/images/navigation8-off_03.png");
        }
    }
    else if(b == trackrightButton)
    {
        printf("trackrightButton\n");
        if(RAVEN->getMixer()->canTrackRight())
        {
            
            if(!(RAVEN->getFunctionsManager()->getOptionButtonState() || MacKeyPress::isOptionDown()))
                sendActionMessage("track right");//sets automation check block counter
            trackrightNSCButton->triggerClick();
        }
    }
    else if(b == trackleftButton)
    {
        printf("trackleftButton\n");
        if(RAVEN->getMixer()->canTrackLeft())
        {
            if(!(RAVEN->getFunctionsManager()->getOptionButtonState() || MacKeyPress::isOptionDown()))
                sendActionMessage("track left");//sets automation check block counter
            trackleftNSCButton->triggerClick();
        }
    }
    else if(b == bankrightButton)
    {
        sendActionMessage("bank right");//sets automation check block counter
        if(bankBy32())
        {
            bankrightNSCButton->triggerClick();
        }
        else
        {
            int numberCanBank = RAVEN->getMixer()->numberCanBankRight();
            printf("number can bank right: %d\n", numberCanBank);
            for(int i = 0;i<numberCanBank;i++)
            {
                if(RAVEN->getMixer()->canTrackRight())
                    trackrightNSCButton->triggerClick();
            }
        }
    }
    else if(b == bankleftButton)
    {
        sendActionMessage("bank left");//sets automation check block counter
        if(bankBy32())
        {
            bankleftNSCButton->triggerClick();
        }
        else
        {
            int numberCanBank = RAVEN->getMixer()->numberCanBankLeft();
            printf("number can bank left: %d\n", numberCanBank);
            for(int i = 0;i<numberCanBank;i++)
            {
                if(RAVEN->getMixer()->canTrackLeft())
                    trackleftNSCButton->triggerClick();
            }
        }
    }
}