
#ifndef Raven_NewSDK_RavenMixerContainer_h
#define Raven_NewSDK_RavenMixerContainer_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenConfig.h"
#include "MouseHideComponent.h"
#include "NscSDKIncludes.h"
#include "RavenRackComponent.h"
#include "RavenMixerComponent.h"
#include "RavenSendsComponent.h"
#include "RavenTrackNameComponent.h"
#include "RavenCounterComponent.h"
#include "RavenInsertsPalette.h"
#include "RavenHotKeyProgrammingComponent.h"
#include "RavenFunctionsComponent.h"
#include "RavenWindowManager.h"
#include "RavenSaveLayoutHandler.h"
#include "RavenDrumPadComponent.h"
#include "RavenFloatingEssentialsComponent.h"
#include "RavenNavPadComponent.h"
#include "RavenFloatingNavPadComponent.h"
#include "RavenLargeMetersComponent.h"
#include "RavenState.h"
#include "RavenWindow.h"

#include "RavenMessage.h"

class RavenRackContainer : public RavenWindowManager, public Button::Listener, public ActionListener, public ActionBroadcaster, public MessageListener
{
public:
    RavenRackContainer(int numBanks, int chansPerBank)
    {        
        //setSize(screenWidth, screenHeight-osxMenuBarHeight);
        
        //Build from the bottom-up
        int rackPos = 0;
        
        //Functions and Save Layout Handler
        
        curNumFunctionsBars = 1;
        functions = new RavenFunctionsComponent(rackPos, 0);
        //Save Layout Handler
        saveLayoutHandler = new RavenSaveLayoutHandler(functions, this);
        //Save DAW Mode Default based on set positions and reset all presets to match if necessary
        saveLayoutHandler->saveUserLayout(true, DEFAULT_PRESET_INDEX);
        #if RAVEN_RESET_PRESETS
        for(int i = 0; i < NUM_PRESETS; i++) saveLayoutHandler->saveUserLayout(true, i);
        #endif
        functions->exitToolbarMode();
        //Save Mix Mode Default based on set positions and reset all presets to match if necessary
        saveLayoutHandler->saveUserLayout(false, DEFAULT_PRESET_INDEX);
        #if RAVEN_RESET_PRESETS
        for(int i = 0; i < NUM_PRESETS; i++) saveLayoutHandler->saveUserLayout(false, i);
        #endif
//        functions->setOrigHeight(functions->getHeight());
        functions->setName("Functions");
//        functions->setTopLeftPosition(0, SCREEN_HEIGHT-functions->getHeight());
//        functions->addToDesktop(0);
//        functions->setVisible(true);
//        Desktop::getInstance().ravenSetWindowOptions(functions);
        functions->setRackContainerButtonListener(this);
        functions->setRackContainerActionListener(this);
        //modules.add(functions);
        
        //temp hack to get all functions components to show when adding a bar before enter custom mode once
        //simulate entering and exiting custom mode- something in this makes them appear
        functions->enterCustomMode();
        functions->exitCustomMode();
        
        functionsWindow = new RavenWindow(functions, rackPos);
        functionsWindow->setTopLeftPosition(0, SCREEN_HEIGHT - functionsWindow->getHeight());
        functionsWindow->setOrigHeight(functions->getHeight());
        functionsWindow->setName("functionsWindow");
        windows.add(functionsWindow);
        
        rackPos++;
        
        
//********* Adding new functions component windows ********************
        functions2 = new RavenFunctionsComponent(rackPos, 1);
        //functions2->exitToolbarMode();
        functions2->setName("functions2");
//        functions2->setTopLeftPosition(0, functionsWindow->getY()-functions->getHeight());
        
        functionsWindow2 = new RavenWindow(functions2, rackPos);
        functionsWindow2->setTopLeftPosition(0, functionsWindow->getY()-functionsWindow->getHeight());
        functionsWindow2->setOrigHeight(functions2->getHeight());  //functions2->getHeight()
        functionsWindow2->setName("functionsWindow2");
        //modules.add(functionsWindow2);
        
        //rackPos--;

        
        functions3 = new RavenFunctionsComponent(rackPos, 1);
        functions3->setName("functions3");
        
        functionsWindow3 = new RavenWindow(functions3, rackPos);
        functionsWindow3->setTopLeftPosition(0, functionsWindow->getY()-functionsWindow->getHeight());
        functionsWindow3->setOrigHeight(functions3->getHeight());  //functions2->getHeight()
        functionsWindow3->setName("functionsWindow2");
        //modules.add(functionsWindow2);
//***********************************************************************        
        

        trackNames = new RavenTrackNameComponent(numBanks, chansPerBank, rackPos);
//        trackNames->setOrigHeight(trackNames->getHeight());
        trackNames->setName("TrackNames");
//        trackNames->setTopLeftPosition(0, functions2->getY()-trackNames->getHeight());
//        trackNames->addToDesktop(0);
//        trackNames->setVisible(true);
//        Desktop::getInstance().ravenSetWindowOptions(trackNames);
//        modules.add(trackNames);
        
        namesWindow = new RavenWindow(trackNames, rackPos);
        namesWindow->setTopLeftPosition(0, functionsWindow2->getY()-namesWindow->getHeight());
        namesWindow->setOrigHeight(namesWindow->getHeight());
        namesWindow->setName("namesWindow");
        windows.add(namesWindow);
        
        rackPos++;
        
        mixer = new RavenMixerComponent(numBanks, chansPerBank, rackPos);
//        mixer->setOrigHeight(mixer->getHeight());
        mixer->setName("Mixer");
//        mixer->setTopLeftPosition(0, trackNames->getY()-mixer->getHeight());
//        mixer->addToDesktop(0);
//        mixer->setVisible(true);
//        Desktop::getInstance().ravenSetWindowOptions(mixer);
        mixer->setRackContainerButtonListener(this);
//        modules.add(mixer);
        
        trackNames->setMixerComponent(mixer);
        
        mixerWindow = new RavenWindow(mixer, rackPos);
        mixerWindow->setTopLeftPosition(0, functionsWindow2->getY()-mixerWindow->getHeight());
        //mixerWindow->setTopLeftPosition(0, namesWindow->getY()-mixerWindow->getHeight());
        mixerWindow->setOrigHeight(mixer->getHeight());
        mixerWindow->setName("mixerWindow");
        windows.add(mixerWindow);
        
        rackPos++;
        
        sends = new RavenSendsComponent(numBanks, chansPerBank, rackPos);
//        sends->setOrigHeight(sends->getHeight());
        sends->setName("Sends");
//        sends->setTopLeftPosition(0, mixer->getY()-sends->getHeight());
//        sends->addToDesktop(0);
//        sends->setVisible(true);
//        Desktop::getInstance().ravenSetWindowOptions(sends);
//        sends->hideButton->addListener(this);
//        modules.add(sends);
        
        trackNames->setSendsComponent(sends);
        
        sendsWindow = new RavenWindow(sends, rackPos);
        sendsWindow->setTopLeftPosition(0, mixerWindow->getY()-sendsWindow->getHeight());
        sendsWindow->setOrigHeight(sends->getHeight());
        sendsWindow->setName("sendsWindow");
        windows.add(sendsWindow);

        
        
//        if(!RAVEN_24)
//        {
//            largeMeters = new RavenLargeMetersComponent(numBanks, chansPerBank, rackPos);
//            largeMeters->setOrigHeight(largeMeters->getHeight());
//            largeMeters->setName("LargeMeters");
//            largeMeters->setTopLeftPosition(0, mixer->getY()-largeMeters->getHeight());
//            //addAndMakeVisible(largeMeters);
//            modules.add(largeMeters);
//        }
        
        updateRemainingHeight();
        
        //Track Names listen to track select button to change font color
        for(int b = 0; b < numBanks; b++)
        {
            for(int c = 0; c < chansPerBank; c++)
            {
                mixer->getBank(b)->getChannel(c)->getTrackSelectButton()->addListener(trackNames->getBank(b)->getChannel(c));
            }
        }
        
        counter = new RavenCounterComponent();
        counter->setName("Counter");
        counter->setTopLeftPosition(1000, 270);
        floatingComponents.add(counter);
        counter->addToDesktop(0);
        Desktop::getInstance().ravenSetWindowOptions(counter);
        counter->setAlwaysOnTop(true);
        
        drumPad = new RavenDrumPadComponent();
        drumPad->setName("DrumPad");
        drumPad->setTopLeftPosition(1050, 270);
        floatingComponents.add(drumPad);
        drumPad->addToDesktop(0);
        Desktop::getInstance().ravenSetWindowOptions(drumPad);
        drumPad->setAlwaysOnTop(true);
        
        floatingNavPad = new RavenFloatingNavPadComponent(functions, functions->getNavPadComponent());
        floatingNavPad->setName("floatingNavPad");
        floatingNavPad->setTopLeftPosition(1050, 270);
        floatingComponents.add(floatingNavPad);
        floatingNavPad->addToDesktop(0);
        Desktop::getInstance().ravenSetWindowOptions(floatingNavPad);
        floatingNavPad->setAlwaysOnTop(true);
        //floatingNavPad->setButtonListener(functions->getNavPadComponent());
        //functions->getNavPadComponent()->setButtonListener(floatingNavPad);
        functions->getNavPadComponent()->addActionListener(floatingNavPad);

        floatingEssentials = new RavenFloatingEssentialsComponent();
        floatingEssentials->setName("FloatingEssentials");
        floatingEssentials->setTopLeftPosition(1050, 270);
        floatingComponents.add(floatingEssentials);
        floatingEssentials->addToDesktop(0);
        Desktop::getInstance().ravenSetWindowOptions(floatingEssentials);
        floatingEssentials->setAlwaysOnTop(true);
        //floatingEssentials->setVisible(true);
        functions->setFloatingEssentials(floatingEssentials);
        floatingEssentials->getEssentialsCustomButton()->addListener(this);
        floatingEssentials->setButtonListenersForEssentials(functions);
        saveLayoutHandler->setFloatingEssentialsForHandler(floatingEssentials);

        insertsPalette = new RavenInsertsPalette();
        insertsPalette->setName("InsertsPalette");
        insertsPalette->setTopLeftPosition(300, 270);
        floatingComponents.add(insertsPalette);
        insertsPalette->addToDesktop(0);
        Desktop::getInstance().ravenSetWindowOptions(insertsPalette);
        insertsPalette->setAlwaysOnTop(true);
        insertsPalette->addActionListener(this);
        
        initializeCounter(functions);
        initializeInsertsPalette(functions);
        
        hotKeyPanel = new RavenHotKeyProgrammingComponent();
        hotKeyPanel->setTopLeftPosition(0, 0);
        hotKeyPanel->addToDesktop(0);
        hotKeyPanel->addActionListener(this);
        functions->setHotKeyPanel(hotKeyPanel);
        
        functions->setInsertWindowButton(insertsPalette->getInsertWindowButton(0));
        //start timer keeping the function ins win button matching the actual NSC button in the palette
        functions->startTimer(200);

        //mainAppWindow = win;
        //functions->setMainWindow(win);
                
        ///*
        if(RAVEN_HIDE_SENDS == 1 || RAVEN_24 == 1)
        {
            hideModule(3);
            sendsWindow->setNeedsHiding(true);
        }
        if(RAVEN_HIDE_MIXER)    hideModule(2);
        if(RAVEN_HIDE_NAMES)    hideModule(1);
        if(RAVEN_HIDE_FUNCTIONS)    hideModule(0);
        //*/
        
        railImg = ImageCache::getFromFile(File(String(GUI_PATH) + "RailRack-Images/Rail-Rack/full+background.png"));
        ravenBackground = ImageCache::getFromFile(File(String(GUI_PATH) + "RailRack-Images/Rail-Rack/raven-background.png"));
        
        addActionListener(functions->getSaveLayoutComponent());
        
        
        //temporary until fader speed with meters is improved- show sends by default
        functions->getMeterModeButton()->triggerClick();
        
        RAVEN_STATE->setComponentPointers(functions, trackNames, mixer, sends);
    }
    ~RavenRackContainer()
    {
        
    }
    
    /*
    void resized()
    {
        //if(getParentComponent() != nullptr) getParentComponent()->setSize(getWidth(), getHeight());
        //posMainAppWindow();
    }
     */
    
    void posMainAppWindow()
    {
        //Rectangle<int> moveToRect(0,  screenHeight - getHeight(), getWidth(), getHeight());
        //Desktop::getInstance().getAnimator().animateComponent (mainAppWindow, moveToRect, 1.0, 500, true, 0.0, 0.0);
        //if(mainAppWindow) mainAppWindow->setTopLeftPosition(0, screenHeight - getHeight());
    }
    
    void paint (Graphics &g)
    {
        g.fillAll(Colours::black);
        g.drawImageAt(ravenBackground, 0, 0);
        
        if(functions->isCustomMode())
        {
            g.drawImageAt(railImg, 0, 0);
        }
    }
    
    void buttonStateChanged (Button*)
    {
    }
    
    void buttonClicked (Button* b)
    {
        
        //std::cout << "button name: " << b->getName() << std::endl;
        
        if(b->getName().compare("toolbarMode") == 0)
        {            
            //Don't allowing entering DAW Mode from Custom Mode
            if(functions->isCustomMode())
            {
                b->setToggleState(!b->getToggleState(), false);
                return; 
            }
            
            //Entering toolbar/DAW mode
            if(b->getToggleState()) 
            {
                //1. save the current layout for Mixer mode
                saveLayoutHandler->saveUserLayout(false, 998);
                
                //2. prevent move modules in DAW mode
                functions->enableMoveModules(false);
                
                //3. fetch DAW Mode layout from saved layouts.
                saveLayoutHandler->loadUserLayout(true, 999);
                
                //4. rebuild the rack with retrieved layout.
                //loadRackLayout()
                clearRack();
                appendWindowToRack(functionsWindow);
                appendWindowToRack(functionsWindow2);
                rebuildRack();
                                
                //5. make sure insertsPalette is not visible
                insertsPalette->setVisible(false);
                
                //6. save the sate boolean
                RAVEN_STATE->setDawMode(true);
                //curNumFunctionsBars = RAVEN_STATE->getSavedNumberOfDawModeBars(); NOTE: will get this when layout is saved properly
                curNumFunctionsBars = 2; //NOTE: currently using two because we are defaulting to this until save layout works properly
                //NOTE: curNumFunctionsBars could be automatically set in rebuild rack...
                                
            }
            else //Mixer mode (Exiting toolbar/DAW mode)
            {
                //1. save the current layout for DAW mode
                saveLayoutHandler->saveUserLayout(true, 998);
                
                //2. fetch Mixer Mode layout from saved layouts.
                saveLayoutHandler->loadUserLayout(false, 999);
                
                //3. rebuild the rack with new layout.
                //loadRackLayout()
                //NOTE the order of operations: 0. clear modules 1. add window to rack, 2. set rack position, 3. rebuild rack...
                clearRack();
                appendWindowToRack(functionsWindow);
                appendWindowToRack(namesWindow);
                appendWindowToRack(mixerWindow);
                appendWindowToRack(sendsWindow);
                rebuildRack();
                
                //4. check state of inserts palette and enable move modules on functions component.
                if(insertsPalette->shouldBeVisible()) insertsPalette->setVisible(true);
                functions->enableMoveModules(true);
                curNumFunctionsBars = 1; //NOTE: this will be retrieved from save layout.
                RAVEN_STATE->setDawMode(false);
            }
        }
        else if(b->getName().compare("essentialsPaletteCustom") == 0)
        {
            //click the customize toolbar button here.
            if(!functions->isCustomMode())
                functions->getToolbarCustomButton()->triggerClick();
         }
        else if(b->getName().compare("toolbarCustom") == 0)
        {

            if(b->getToggleState()) //enter customize mode
            {
                
                //1. save prior rack layout. (To be called when exiting custom mode)
                
                //2. rearrange rack for custom mode
                clearRack();
                appendWindowToRack(functionsWindow);
                appendWindowToRack(functionsWindow2);
                appendWindowToRack(functionsWindow3);
                rebuildRack();
                
                //3. set customMode = true in functions, functions2, and functions3 (allows for moving modules).
                //NOTE: need to gut functions->enterCustomMode(). This should only set customMode = true.
                functions->enterCustomMode();
                floatingEssentials->getEssentialsCustomButton()->setToggleState(true, false);
                
                
            }
            else  //exit customize mode
            {
                //1. set customMode = false in functions, functions2, and functions3 (disables moving modules).
                //NOTE: need to gut functions->exitCustomMode(). This should only set customMode = false.
                functions->exitCustomMode();
                floatingEssentials->getEssentialsCustomButton()->setToggleState(false, false);
                
                
                //2. fetch prior custom mode saved layout
                
                //3. load pre-custom mode rack layout
                //NOTE: below is default view. This will come from save layout handler.
                clearRack();
                
                appendWindowToRack(functionsWindow);
                appendWindowToRack(namesWindow);
                appendWindowToRack(mixerWindow);
                appendWindowToRack(sendsWindow);
                                
                rebuildRack();

            }
            
        }
        else if(b->getName().compare("counterButton") == 0)
        {
            counter->setVisible(b->getToggleState());
            counter->toFront(false);
        }
        else if(b->getName().compare("drumPad") == 0)
        {
            drumPad->setVisible(b->getToggleState());
            drumPad->toFront(false);
        }
        else if(b->getName().compare("essentialsButton") == 0)
        {
            floatingEssentials->setVisible(b->getToggleState());
            floatingEssentials->toFront(false);
        }
        else if(b->getName().compare("enterHybridModeButton") == 0)
        {
//            floatingEssentials->setVisible(b->getToggleState());
//            floatingEssentials->toFront(false);
        }
        else if(b->getName().compare("floatingNavPadButton") == 0)
        {
            floatingNavPad->setVisible(b->getToggleState());
            floatingNavPad->toFront(false);
        }
        else if(b->getName().compare("insertButton") == 0)
        {
            //simulating a global radio button group here...
            if(b->getToggleState()) //if this one is selected
            {
                mixer->toggleInsertButtonsOff(); //turn off all insert buttons
                b->setToggleState(true, false); // toggle this one back on but don't send notification
                insertsPalette->setVisible(true);
                insertsPalette->setShouldBeVisible(true);
                insertsPalette->toFront(false);
            }
            else
            {
                insertsPalette->setVisible(false);
                insertsPalette->setShouldBeVisible(false);
            }
        }
        else if(b->getName().compare("insertButtonNSC") == 0)
        {
            RavenNSCButton *nscTrackButton = dynamic_cast<RavenNSCButton*>(b);
            
            int bnk = nscTrackButton->getBankIndex();
            int chn = nscTrackButton->getChannelIndex();
            insertsPalette->setTrackName(trackNames->getTrackName(bnk, chn));
            insertsPalette->setCurrentTrackButton(nscTrackButton);
            functions->setInsertWindowButton(insertsPalette->getInsertWindowButton(bnk));
            insertsPalette->updateNames();
        }
        else if(b->getName().compare("hideSends") == 0)
        {
            if(!sendsWindow->needsHiding())   sendsWindow->setNeedsHiding(true);
            else                        sendsWindow->setNeedsHiding(false);
        }
        else if(b->getName().compare("hideMixer") == 0)
        {
            if(!mixerWindow->needsHiding())   mixerWindow->setNeedsHiding(true);
            else                        mixerWindow->setNeedsHiding(false);
        }
        else if(b->getName().compare("finePanButton") == 0)
        {
            mixer->setFinePan(b->getToggleState());
            sends->setFinePan(b->getToggleState());
        }
        else if(b->getName().compare("fineFaderButton") == 0)
        {
            mixer->setFineFader(b->getToggleState());
            sends->setFineFader(b->getToggleState());
        }
        else if (b->getName().compare("moveModulesButton") == 0)
        {
            #if RAVEN_24
            if(b->getToggleState()) b->setToggleState(false, false);
            #endif
            
            bool moveMode = b->getToggleState();
            //mixer->hideButton->setVisible(moveMode);
            //if(!RAVEN_24) sends->hideButton->setVisible(moveMode);
            
            if(moveMode) //entering move modules mode
            {
                //temporarily resize functions so everything fits
                /*
                int subHeight = functions->getHeight() - functions->getSinglePanelHeight(); //height of extra function bars
                functions->setSize(functions->getWidth(), functions->getSinglePanelHeight());
                 */
                
                //Don't show sends if not enough room
                if(!RAVEN_24)
                {
                    //printf("rem height = %d\n", remainingHeight);
                    if(sendsWindow->getOrigHeight() <= remainingHeight)
                    {
                        if(sendsWindow->isHidden())
                        {
                            showModule(sendsWindow);
                            sendsWindow->setNeedsHiding(true);
                        }
                    }
                    /*// don't worry about mixer for now..because it's not allowed to hide in mix mode
                    if(mixer->isHidden())
                    {
                        showModule(mixer);
                        mixer->setNeedsHiding(true);
                    }
                     */
                }
            }
            else //exiting move modules mode
            {
                // resize functions to proper size
                //functions->setSize(functions->getWidth(), functions->getOrigHeight());
                
                if(!RAVEN_24)
                {
                    if(sendsWindow->needsHiding() || functions->getNumMixModeBars() > 1) hideModule(sendsWindow);
                    else                     showModule(sendsWindow);
                    
                    //NOTE: need this logic in case push the hide sends button when in move modules.
                    //Add condition to only show sends if numToolbars > 1.
                    
                    
                    if(mixerWindow->needsHiding()) hideModule(mixerWindow);
                    else                     showModule(mixerWindow);
                }
            }
        }
        else if(b->getName().compare("targetDAW") == 0)
        {
             if(b->getToggleState()) printf("show menu\n");
             else                    printf("hide menu\n");
        }
        #if RAVEN_24
        else if(b->getName().compare("flipModeButton") == 0)
        {
            bool doneInit = b->getProperties().getWithDefault("doneInitializing", false);
            if(!doneInit) return; //wait for intialization to finish using flip mode before we listen
            
            bool flipMode = getProperties().getWithDefault("isFlipMode", false);
            if(!flipMode)
            {
                printf("flip mode so hiding mixer\n");
                
                hideModule(mixer);
                //mixer->setNeedsHiding(true);//-josh
                showModule(sends);
                //sends->setNeedsHiding(false);//-josh
                
                mixer->enterFlipMode();
                sends->enterFlipMode();
                trackNames->enterFlipMode();
                functions->enterFlipMode();
                
                getProperties().set("isFlipMode", true);
            }
            else
            {
                printf("regular mode so hiding sends\n");
                
                hideModule(sends);
                //sends->setNeedsHiding(true);//-josh
                showModule(mixer);
                //mixer->setNeedsHiding(false);//-josh
                
                mixer->exitFlipMode();
                sends->exitFlipMode();
                trackNames->exitFlipMode();
                functions->exitFlipMode();
                
                getProperties().set("isFlipMode", false);

            }
        }
        #endif
        else if(b->getName().compare("exitButton") == 0)
        {
            JUCEApplication::quit();
        }
         
        else if(b->getName().compare("addBarButton") == 0)
        {
            std::cout << "curNumFunctionBars: " << curNumFunctionsBars << std::endl;
        
            if(curNumFunctionsBars == 0)
            {
                insertWindowIntoRack(functionsWindow, functionsWindow->getRackPos(), true);
            }
            else if(curNumFunctionsBars == 1)
            {
                insertWindowIntoRack(functionsWindow2, functionsWindow, true);
                if(!RAVEN_STATE->isDawMode()) removeWindowFromRack(sendsWindow);
            }
            else if(curNumFunctionsBars == 2)
            {
                insertWindowIntoRack(functionsWindow3, functionsWindow2, true);
            }
            else if(curNumFunctionsBars == 3)
            {
                return;
            }
            
            rebuildRack();
            
            curNumFunctionsBars++;
            if(curNumFunctionsBars > 3) curNumFunctionsBars = 3;
        }
        else if(b->getName().compare("removeBarButton") == 0)
        {
            std::cout << "curNumFunctionBars: " << curNumFunctionsBars << std::endl;
            
            if(curNumFunctionsBars == 0)
            {
                return;
            }
            else if(curNumFunctionsBars == 1)
            {
                removeWindowFromRack(functionsWindow);
            }
            else if(curNumFunctionsBars == 2)
            {
                removeWindowFromRack(functionsWindow2);
                if(!RAVEN_STATE->isDawMode()) insertWindowIntoRack(sendsWindow, sendsWindow->getRackPos(), true);
            }
            else if(curNumFunctionsBars == 3)
            {
                removeWindowFromRack(functionsWindow3);
            }

            rebuildRack();
            
            curNumFunctionsBars--;
            if(curNumFunctionsBars < 0) curNumFunctionsBars = 0;
            
        }


    }
    
    void actionListenerCallback (const String &message)
    {
        //NOTE: might need another var - needsHiding from hide/show vs hiding from space
        
        if(message.startsWith("addBarMixMode"))
        {
            if(remainingHeight < functions->getSinglePanelHeight())
            {
                //do logic to compute the furthest module from the toolbar (mixer or sends)... then hide that one
                //printf("hide sends to make room for new toolbar\n");
                hideModule(sendsWindow);
                //sends->setNeedsHiding(true);
            }
            else remainingHeight = remainingHeight - functions->getSinglePanelHeight();
            
            //Reset rack layout, building from bottom up.
            int currentScreenPosition = screenHeight;
            for(int i = windows.size(); i >= 0; i--)
            {
                for(int j = 0; j < windows.size(); j++)
                {
                    if(i == windows[j]->getRackPos())
                    {
                        windows[j]->setTopLeftPosition(0, currentScreenPosition - windows[j]->getHeight());
                        currentScreenPosition = windows[j]->getY();
                    }
                }
            }
            
        }
        else if(message.startsWith("removeBarMixMode"))
        {
            //Reset rack layout, building from bottom up.
            int currentScreenPosition = screenHeight;// - osxMenuBarHeight;
            for(int i = windows.size(); i >= 0; i--)
            {
                for(int j = 0; j < windows.size(); j++)
                {
                    if(i == windows[j]->getRackPos())
                    {
                        windows[j]->setTopLeftPosition(0, currentScreenPosition - windows[j]->getHeight());
                        currentScreenPosition = windows[j]->getY();
                    }
                }
            }

            //Should make remaining height a var of rack manager, not needed as property now that we connect fuctions and rack container via action broadcast/listen
            int tempRemainingHeight = remainingHeight + functions->getSinglePanelHeight();
            if(tempRemainingHeight >= sendsWindow->getOrigHeight())
            {
                printf("there is  room, show sends\n");
                //TODO: unhide farthest
                if(!sendsWindow->needsHiding())
                {
                    showModule(sendsWindow);
                    sendsWindow->setNeedsHiding(false);
                }
                else remainingHeight = tempRemainingHeight;
            }
            else remainingHeight = tempRemainingHeight;
            

        }
        else if(message.startsWith("savecurrent"))
        {
            if(functions->isToolbarMode())
            {
                saveLayoutHandler->saveUserLayout(true, saveLayoutHandler->getDefaultPresetIndx());
            }
            else
            {
                saveLayoutHandler->saveUserLayout(false, saveLayoutHandler->getDefaultPresetIndx());
            }
        }
        else if(message.startsWith("save"))
        {
            int presetIndex = message.substring(4, 5).getIntValue();
            saveLayoutHandler->saveUserLayout(functions->isToolbarMode(), presetIndex);
        }
        else if(message.startsWith("load"))
        {
            int presetIndex = message.substring(4, 5).getIntValue();
            saveLayoutHandler->loadUserLayout(functions->isToolbarMode(), presetIndex);
        }
        else if(message == "set mixer automation off")
        {
            //mixer->resetAllBlankFrames();
            //sends->resetAllBlankFrames();
            //trackNames->setAllTrackNamesBlank();
        }
        else if(message == "track left")
        {
            mixer->trackLeft();
            sends->trackLeft();
            trackNames->trackLeft();
        }
        else if(message == "track right")
        {
            mixer->trackRight();
            sends->trackRight();
            trackNames->trackRight();
        }
        else if(message == "toggleInsertButtonsOff")
        {
            mixer->toggleInsertButtonsOff();
        }
    }
    
    void handleMessage (const Message &message)
    {
        RavenComponentMoveMessage *m = (RavenComponentMoveMessage*)&message;
        m->moveComponent();
    }
    
    void initializeCounter(NscGuiContainer *owner)
    {
        counter->initializeCounter(owner);
    }
    
    void initializeInsertsPalette(RavenFunctionsComponent *owner)
    {
        insertsPalette->initializeLCDs(owner);
    }
    
    bool ravenTouched(int touchX, int touchY)
    {
        bool sendsTouched = RavenTouchComponent::componentTouched(sends, touchX, touchY);
        bool mixerTouched = RavenTouchComponent::componentTouched(mixer, touchX, touchY);
        bool namesTouched = RavenTouchComponent::componentTouched(trackNames, touchX, touchY);
        bool functionsTouched = RavenTouchComponent::componentTouched(functions, touchX, touchY);
        return (sendsTouched || mixerTouched || namesTouched || functionsTouched);
    }
    
    bool checkCounterTouch(TUIO::TuioCursor *tcur)
    {
        if(counter->componentTouched(counter, tcur, false))
        {
            int touchX = tcur->getScreenX(SCREEN_WIDTH);
            int touchY = tcur->getScreenY(SCREEN_HEIGHT);
            Point<int> screenLocation;
            screenLocation.setXY(touchX, touchY);
            Point<int> localPoint = counter->getPeer()->globalToLocal(screenLocation);
            counter->getProperties().set("initTouchX", localPoint.getX());
            counter->getProperties().set("initTouchY", localPoint.getY());
            return true;
        }
        return false;
    }
    
    bool checkDrumMoveTouch(TUIO::TuioCursor *tcur)
    {
        return (drumPad->componentTouched(drumPad, tcur, false) && !drumPad->checkButtonTouch(tcur));
        
    }
    bool checkDrumTouch(TUIO::TuioCursor *tcur)
    {
        return (drumPad->isShowing() && drumPad->checkButtonTouch(tcur));
    }
    
    bool checkEssentialsMoveTouch(TUIO::TuioCursor *tcur)
    {
        bool touchingEssentialsComponent = false;
        for(int i = 0; i < floatingEssentials->getNumChildComponents(); i++)
        {
            if(floatingEssentials->getChildComponent(i)->getName() == "MouseHider") continue;
            if(floatingEssentials->componentTouched(floatingEssentials->getChildComponent(i), tcur, false))
            {
                touchingEssentialsComponent = true;
                break;
            }
        }
        if(floatingEssentials->componentTouched(floatingEssentials, tcur, false) && !touchingEssentialsComponent)
        {
            int touchX = tcur->getScreenX(SCREEN_WIDTH);
            int touchY = tcur->getScreenY(SCREEN_HEIGHT);
            Point<int> screenLocation;
            screenLocation.setXY(touchX, touchY);
            Point<int> localPoint = floatingEssentials->getPeer()->globalToLocal(screenLocation);
            floatingEssentials->getProperties().set("initTouchX", localPoint.getX());
            floatingEssentials->getProperties().set("initTouchY", localPoint.getY());
            return true;
        }
        
        return false;
    }
    
    bool checkEssentialsTouch(TUIO::TuioCursor *tcur)
    {
        return floatingEssentials->componentTouched(floatingEssentials, tcur, false);
    }

    bool checkFloatingMixerMoveTouch(TUIO::TuioCursor *tcur)
    {
        if(mixer->componentTouched(mixer->getHybridModeMoveComponent(), tcur, false))
        {
            int touchX = tcur->getScreenX(SCREEN_WIDTH);
            int touchY = tcur->getScreenY(SCREEN_HEIGHT);
            Point<int> screenLocation;
            screenLocation.setXY(touchX, touchY);
            Point<int> localPoint = mixer->getPeer()->globalToLocal(screenLocation);
            mixer->getProperties().set("initTouchX", localPoint.getX());
            mixer->getProperties().set("initTouchY", localPoint.getY());
            return true;
        }
        return false;
    }
    
    bool checkFloatingNavPadTap(int touchX, int touchY)
    {
        return floatingNavPad->checkPadTouch(touchX, touchY);
    }
    
    bool checkFloatingNavPadComponentTouch(TUIO::TuioCursor *tcur)
    {
        int touchX = tcur->getScreenX(SCREEN_WIDTH);
        int touchY = tcur->getScreenY(SCREEN_HEIGHT);
        return RavenTouchComponent::componentTouched(floatingNavPad, touchX, touchY);
    }
    
    bool checkFloatingNavPadTouch(int touchX, int touchY)
    {
        return (floatingNavPad->isShowing() && (floatingNavPad->checkButtonTouch(touchX, touchY) || floatingNavPad->checkPadTouch(touchX, touchY)));
    }
    
    bool checkFloatingNavPadTouch(TUIO::TuioCursor *tcur)
    {
        int touchX = tcur->getScreenX(SCREEN_WIDTH);
        int touchY = tcur->getScreenY(SCREEN_HEIGHT);
        return (floatingNavPad->isShowing() && (floatingNavPad->checkButtonTouch(tcur) || floatingNavPad->checkPadTouch(touchX, touchY)));
    }
 
    bool checkFloatingNavPadMoveTouch(TUIO::TuioCursor *tcur)
    {
        int touchX = tcur->getScreenX(SCREEN_WIDTH);
        int touchY = tcur->getScreenY(SCREEN_HEIGHT);

        if(floatingNavPad->componentTouched(floatingNavPad, tcur, false) && !floatingNavPad->checkPadTouch(touchX, touchY)
           && !floatingNavPad->checkButtonTouch(tcur))
        {
            Point<int> screenLocation;
            screenLocation.setXY(touchX, touchY);
            Point<int> localPoint = floatingNavPad->getPeer()->globalToLocal(screenLocation);
            floatingNavPad->getProperties().set("initTouchX", localPoint.getX());
            floatingNavPad->getProperties().set("initTouchY", localPoint.getY());
            return true;
        }

        return false;
    }
    
    bool checkInsertsPaletteMoveTouch(TUIO::TuioCursor *tcur)
    {
        return insertsPalette->componentTouched(insertsPalette->getTrackLabel(), tcur, false);
    }
    
    bool checkInsertsPaletteTouch(TUIO::TuioCursor *tcur)
    {
        if(insertsPalette->isShowing())
        {
            return (insertsPalette->checkButtonTouch(tcur) || insertsPalette->componentTouched(insertsPalette, tcur, false));
        }
        else return false;
    }
    
    bool checkHotKeyPanelTouch(TUIO::TuioCursor *tcur)
    {
        return hotKeyPanel->checkButtonTouch(tcur);
    }
    void moveCounter(int screenX, int screenY)
    {
        //downsample movement for faster response
        static int counterMoveCount = 0;
        if(counterMoveCount % FLOATING_DOWNSAMPLE_AMT == 0)
        {
            int initTouchX = counter->getProperties().getWithDefault("initTouchX", 0);
            int initTouchY = counter->getProperties().getWithDefault("initTouchY", 0);
            int xPos = screenX - initTouchX; //counter->getWidth()/2;
            int yPos = screenY - initTouchY; //counter->getHeight()/2;
            RavenComponentMoveMessage *m = new RavenComponentMoveMessage(counter, xPos, yPos);
            postMessage(m);
        }
        
        counterMoveCount++;
        if(counterMoveCount >= 50000) counterMoveCount = 0;
    }
    void moveDrumPad(int screenX, int screenY)
    {
        //downsample movement for faster response
        static int drumPadMoveCount = 0;
        if(drumPadMoveCount % FLOATING_DOWNSAMPLE_AMT == 0)
        {
            int xPos = screenX - drumPad->getWidth()/2;
            int yPos = screenY;
            RavenComponentMoveMessage *m = new RavenComponentMoveMessage(drumPad, xPos, yPos);
            postMessage(m);
        }
        
        drumPadMoveCount++;
        if(drumPadMoveCount >= 50000) drumPadMoveCount = 0;
    }
                
    void moveEssentialsPalette(int screenX, int screenY)
    {
        //downsample movement for faster response
        static int essentialsMoveCount = 0;
        if(essentialsMoveCount % FLOATING_DOWNSAMPLE_AMT == 0)
        {
//            int xPos = screenX - floatingEssentials->getWidth()/2;
//            int yPos = screenY;
            int initTouchX = floatingEssentials->getProperties().getWithDefault("initTouchX", 0);
            int initTouchY = floatingEssentials->getProperties().getWithDefault("initTouchY", 0);
            int xPos = screenX - initTouchX; //counter->getWidth()/2;
            int yPos = screenY - initTouchY; //counter->getHeight()/2;
            RavenComponentMoveMessage *m = new RavenComponentMoveMessage(floatingEssentials, xPos, yPos);
            postMessage(m);
        }
        essentialsMoveCount++;
        if(essentialsMoveCount >= 50000) essentialsMoveCount = 0;
    }
    
    void moveInsertsPalette(int screenX, int screenY)
    {
        //downsample movement for faster response
        static int insertsPaletteMoveCount = 0;
        if(insertsPaletteMoveCount % FLOATING_DOWNSAMPLE_AMT == 0)
        {
            int xPos = screenX - 147;
            int yPos = screenY - 15;
            RavenComponentMoveMessage *m = new RavenComponentMoveMessage(insertsPalette, xPos, yPos);
            postMessage(m);
        }
        
        insertsPaletteMoveCount++;
        if(insertsPaletteMoveCount >= 50000) insertsPaletteMoveCount = 0;
    }
    
    void moveFloatingNavPad(int screenX, int screenY)
    {
        //downsample movement for faster response
        static int floatingNavPadMoveCount = 0;
        if(floatingNavPadMoveCount % FLOATING_DOWNSAMPLE_AMT == 0)
        {
            int initTouchX = floatingNavPad->getProperties().getWithDefault("initTouchX", 0);
            int initTouchY = floatingNavPad->getProperties().getWithDefault("initTouchY", 0);
            int xPos = screenX - initTouchX;
            int yPos = screenY - initTouchY;
            
//            int xPos = screenX;
//            int yPos = screenY;
//            if(screenX < floatingNavPad->getScreenX() + floatingNavPad->getWidth()/2) //left side
//            {
//                xPos = screenX - 0.10*floatingNavPad->getWidth();
//                yPos = screenY - 0.65*floatingNavPad->getHeight();
//            }
//            else //right side
//            {
//                xPos = screenX - 0.88*floatingNavPad->getWidth();
//                yPos = screenY - 0.22*floatingNavPad->getHeight();
//            }

            RavenComponentMoveMessage *m = new RavenComponentMoveMessage(floatingNavPad, xPos, yPos);
            postMessage(m);
        }
        
        floatingNavPadMoveCount++;
        if(floatingNavPadMoveCount >= 50000) floatingNavPadMoveCount = 0;
    }
    
    void moveFloatingMixer(int screenX, int screenY)
    {
        //downsample movement for faster response
        static int mixerMoveCount = 0;
        if(mixerMoveCount % FLOATING_DOWNSAMPLE_AMT == 0)
        {
            int initTouchX = mixer->getProperties().getWithDefault("initTouchX", 0);
            int initTouchY = mixer->getProperties().getWithDefault("initTouchY", 0);
            int xPos = screenX - initTouchX;
            int yPos = screenY - initTouchY;
            RavenComponentMoveMessage *m = new RavenComponentMoveMessage(mixer, xPos, yPos);
            postMessage(m);
        }
        mixerMoveCount++;
        if(mixerMoveCount >= 50000) mixerMoveCount = 0;
    }

    void setNscLink	(class NscLink * link_)
    {
        sends->setNscLink(link_);
        mixer->setNscLink(link_);
        trackNames->setNscLink(link_);
        functions->setNscLink(link_);
    }
    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, float val)
    {        
        sends->nscCallback(type, subType, bank, which, val);
        mixer->nscCallback(type, subType, bank, which, val);
        trackNames->nscCallback(type, subType, bank, which, val);
        functions->callback(type, subType, bank, which, val);
//        largeMeters->nscCallback(type, subType, bank, which, val);
    }
    void nscCallback(ENSCEventType type, int subType, int bank, int which, const std::string & data)
    {
        sends->nscCallback(type, subType, bank, which, data.c_str());
        mixer->nscCallback(type, subType, bank, which, data.c_str());
        trackNames->nscCallback(type, subType, bank, which, data.c_str());
        functions->callback(type, subType, bank, which, data.c_str());
        
        //update insert palette names
        if(type == eNSCPlugInLCD && bank == 0 && which == 40)
        {
            //printf("insert name update: %s \n", data.c_str());
            insertsPalette->updateNames();
        }
    }
    
    void finishedInitialization()
    {
        functionsWindow->addToDesktop();
        Desktop::getInstance().ravenSetWindowOptions(functionsWindow);
        
        namesWindow->addToDesktop();
        Desktop::getInstance().ravenSetWindowOptions(namesWindow);
        
        mixerWindow->addToDesktop();
        Desktop::getInstance().ravenSetWindowOptions(mixerWindow);
        
        sendsWindow->addToDesktop();
        Desktop::getInstance().ravenSetWindowOptions(sendsWindow);
        
        rebuildRack();
        
        

        //functions->addToDesktop(0);
        //functions->setVisible(true);
        //Desktop::getInstance().ravenSetWindowOptions(functions);
        
//        trackNames->addToDesktop(0);
//        trackNames->setVisible(true);
//        Desktop::getInstance().ravenSetWindowOptions(trackNames);
//        
//        mixer->addToDesktop(0);
//        mixer->setVisible(true);
//        Desktop::getInstance().ravenSetWindowOptions(mixer);
//        
//        sends->addToDesktop(0);
//        sends->setVisible(true);
//        Desktop::getInstance().ravenSetWindowOptions(sends);
    }
    
    RavenMixerComponent* getMixer(){return mixer;}
    RavenSendsComponent* getSends(){return sends;}
    RavenTrackNameComponent* getTrackNames(){return trackNames;}
    RavenFunctionsComponent* getFunctions() {return functions;}
    
    RavenWindow* getMixerWindow(){return mixerWindow;}
    RavenWindow* getSendsWindow(){return sendsWindow;}
    RavenWindow* getTrackNamesWindow(){return namesWindow;}
    RavenWindow* getFunctionsWindow() {return functionsWindow;}

    
private:
    ScopedPointer<RavenWindow> sendsWindow;
    ScopedPointer<RavenWindow> mixerWindow;
    ScopedPointer<RavenWindow> namesWindow;
    ScopedPointer<RavenWindow> functionsWindow;
    ScopedPointer<RavenWindow> functionsWindow2;
    ScopedPointer<RavenWindow> functionsWindow3;
    
    ScopedPointer<RavenWindow> insertsWindow;
    ScopedPointer<RavenWindow> hotkeyWindow;
    ScopedPointer<RavenWindow> navpadWindow;
    ScopedPointer<RavenWindow> essentialsWindow;
    
    ScopedPointer<RavenSendsComponent> sends;
    ScopedPointer<RavenMixerComponent> mixer;
    ScopedPointer<RavenTrackNameComponent> trackNames;
    ScopedPointer<RavenFunctionsComponent> functions;
    ScopedPointer<RavenFunctionsComponent> functions2;
    ScopedPointer<RavenFunctionsComponent> functions3;
    ScopedPointer<RavenCounterComponent> counter;
    ScopedPointer<RavenInsertsPalette> insertsPalette;
    ScopedPointer<RavenHotKeyProgrammingComponent> hotKeyPanel;
    ScopedPointer<RavenSaveLayoutHandler> saveLayoutHandler;
    ScopedPointer<RavenDrumPadComponent> drumPad;
    ScopedPointer<RavenFloatingNavPadComponent> floatingNavPad;
    ScopedPointer<RavenFloatingEssentialsComponent> floatingEssentials;
//    ScopedPointer<RavenLargeMetersComponent> largeMeters;
    
    Component* essentialsMovingModule;
    
    
//    int initTouchX;
//    int initTouchY;
    
    int curNumFunctionsBars;
    
    
    Image railImg;
    Image ravenBackground;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenRackContainer)
    
};

#endif
