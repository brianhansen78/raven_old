
#ifndef Raven_EditMoveFineClickComponent_h
#define Raven_EditMoveFineClickComponent_h

#include "RavenButton.h"
#include "RavenFunctionsComponent.h"
#include "RavenTouchComponent.h"

class RavenEditMixComponent : public RavenTouchComponent, public Button::Listener
{
public:
    RavenEditMixComponent(RavenFunctionsContainer *owner) : isEditWin(true)
    {
        //invisible
        editWinNSCButton = new RavenNSCButton(owner);
        editWinNSCButton->addListener(owner);
        editWinNSCButton->setTag(eNSCGSPTWindow_Edit);
        
        mixWinNSCButton = new RavenNSCButton(owner);
        mixWinNSCButton->addListener(owner);
        mixWinNSCButton->setTag(eNSCGSPTWindow_Mix);
        
        editMixButton = new RavenButton();
        RAVEN_BUTTON_5(editMixButton);
        editMixButton->setTopLeftPosition(0, 2);
        editMixButton->addListener(this);
        editMixButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/EditMixClick/images/bt-EditMix-On.png", "TOOLBAR/FUNCTIONMODULE/EditMixClick/images/bt-EditMix-Off.png");
        ravenTouchButtons.add(editMixButton);
        addAndMakeVisible(editMixButton);
      
        setSize(editMixButton->getWidth(), editMixButton->getHeight());        
    }
    
    void buttonStateChanged (Button*)
    {
        
    }
    void buttonClicked (Button* b)
    {
        if(b == editMixButton)
        {
            isEditWin = !isEditWin;
            if(isEditWin)   editWinNSCButton->triggerClick();
            else            mixWinNSCButton->triggerClick();
        }
    }
    
    
private:
    
    RavenButton *editMixButton;
    
    //invisible
    ScopedPointer<RavenNSCButton> editWinNSCButton;
    ScopedPointer<RavenNSCButton> mixWinNSCButton;
    
    bool isEditWin;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenEditMixComponent)
    
};

#endif
