
#ifndef Raven_NewSDK_RavenRackComponent_h
#define Raven_NewSDK_RavenRackComponent_h

#include "RavenTouchComponent.h"
#include "RavenWindow.h"
#include "MouseHideComponent.h"
//#include "MacHideBackgroundMouse.h"

//RavenWindowComponents are the large containing componets that require a mouse hider (and?) i.e. rack components and palette components

class RavenWindowComponent : public RavenTouchComponent
{
    
public:
    RavenWindowComponent() : window(0)
    {}

    void setWindow(RavenWindow *win)
    {
        window = win;        
    }
    
    RavenWindow* getWindow()
    {
        try
        {
            if(window) return window;
            else       throw 0;
        }
        catch(...)
        {
            printf("EXCEPTION: Window not initialized!\n");
            return 0;
        }
    }
    
//    void resized()
//    {        
//        std::cout << "!!!! TRIGGERING RESIZE EVENT !!!!" << std::endl;
//        mouseHider->setTopLeftPosition(0, 0);
//        mouseHider->setSize(getWidth(), getHeight());
//        printf("%s Rack Component Resized to %d %d \n", (const char*)getName().toUTF8(), getWidth(), getHeight());
//        mouseHider->setAlwaysOnTop(true);
//        mouseHider->toFront(false);
//        printf("this x = %d, y = %d\n", getScreenX(), getScreenY());
//        printf("this' mouse hider x = %d, y = %d\n", mouseHider->getScreenX(), mouseHider->getScreenY());
//    }

//    void addFunctionsChildTouchComponent(RavenTouchComponent* cmp)
//    {
//        addChildComponent(cmp);
//        childTouchComponents.add(cmp);
//    }
//    void removeFunctionsChildTouchComponent(RavenTouchComponent* cmp)
//    {
//        removeChildComponent(cmp);
//        childTouchComponents.removeAllInstancesOf(cmp);
//    }

protected:
    RavenWindow *window;
    ScopedPointer<MouseHideComponent> mouseHider;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenWindowComponent)
};


#endif
