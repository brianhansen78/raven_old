

#ifndef Raven_RavenMixerBackgroundComponent_h
#define Raven_RavenMixerBackgroundComponent_h

#include "../JuceLibraryCode/JuceHeader.h"


class RavenMixerBackgroundComponent : public Component
{
public:
    
    RavenMixerBackgroundComponent()
    {

        //Inserts Panel
        insertsPanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "MIXERGRAPHICS/INSERTSV4/INSERTPANEL/INSERT-PANEL-1to5.png"));
        
        //Sends Panel
        sendPanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "MIXERGRAPHICS/SENDS/SENDSPANEL/SendPanel.png"));
        
        for(int i = 0; i < 24; i++)
        {
            File imgFile(RavenGUI::getSendKnobBGPath(i+1));
            sendPanBGImgs[i] = ImageCache::getFromFile(imgFile);
        }
        
        //Mixer Panel
        mixerPanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "MIXERGRAPHICS/MIXER/MIXERPANEL/MixerPanel.png"));
        
        for(int i = 0; i < 24; i++)
        {
            File imgFile(RavenGUI::getMixerKnobBGPath(i+1));
            mixerPanBGImgs[i] = ImageCache::getFromFile(imgFile);
        }
        
        //Name Panel
        trackNamePanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "MIXERGRAPHICS/TRACKNAMEPANEL/TrackNamePanel.png"));
        
        //total size
        setSize(mixerPanelImg.getWidth(), insertsPanelImg.getHeight() + sendPanelImg.getHeight() + mixerPanelImg.getHeight() + trackNamePanelImg.getHeight());
        
        //drawing order
        insertsTop = 0;
        sendsTop = insertsPanelImg.getHeight();
        mixerTop = sendsTop + sendPanelImg.getHeight();
        namesTop = mixerTop + mixerPanelImg.getHeight();
        
        //setBufferedToImage(true);
        setOpaque(true);

    }
    
    void paint (Graphics& g)
    {
     
        
            //printf("painting entire background (mixerk bg comp)\n");
                    
            g.drawImageAt(insertsPanelImg, 0, insertsTop);
            
            g.drawImageAt(sendPanelImg, 0, sendsTop);
            
            int xOff = 16;
            for(int i = 0; i < 24; i++)
            {
                //panKnobXOffsets[i] = xOff;
                g.drawImageAt(sendPanBGImgs[i], xOff, sendsTop);
                xOff += sendPanBGImgs[i].getWidth();
            }
            
            g.drawImageAt(mixerPanelImg, 0, mixerTop);
            
            xOff = 11;
            for(int i = 0; i < 24; i++)
            {
                g.drawImageAt(mixerPanBGImgs[i], xOff, mixerTop);
                xOff += mixerPanBGImgs[i].getWidth();
            }
                    
            g.drawImageAt(trackNamePanelImg, 0, namesTop);
        
    }
    
    void setMixerPanelTop(int yPos)
    {
        mixerTop = yPos;
        repaint();
    }
    void setSendsPanelTop(int yPos)
    {
        sendsTop = yPos;
        repaint();
    }
    void setNamesPanelTop(int yPos)
    {
        namesTop = yPos;
        repaint();
    }
    void setInsertsPanelTop(int yPos)
    {
        insertsTop = yPos;
        repaint();
    }
    
private:
    
    Image insertsPanelImg;
    
    Image sendPanelImg;
    Image sendPanBGImgs[24];
    
    Image mixerPanelImg;
    Image mixerPanBGImgs[24];
    
    Image trackNamePanelImg;
    
    int mixerTop, sendsTop, insertsTop, namesTop;
};

#endif


