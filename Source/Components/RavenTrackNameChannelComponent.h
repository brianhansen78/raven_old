

#ifndef Raven_NewSDK_RavenTrackNameChannelComponent_h
#define Raven_NewSDK_RavenTrackNameChannelComponent_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "NscGuiElement.h"
#include "RavenTextDisplay.h"
#include "RavenTouchComponent.h"
#include "RavenIconComponent.h"

class RavenTrackNameChannelComponent : public RavenTouchComponent, public NscGuiElement, public Button::Listener
{
public:
    RavenTrackNameChannelComponent (NscGuiContainer *owner, int bank, int chan, HashMap<String, Image> *_iconMap) : NscGuiElement(owner), iconMap(_iconMap), blank(false)
    {
        if(RAVEN_24)
        {
            trackNamePanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/NameBackground/ChannelName24.png"));
            iconPanelImage = ImageCache::getFromFile(File(String(GUI_PATH) + "icons/24/frame.png"));
            setSize (trackNamePanelImg.getWidth(), trackNamePanelImg.getHeight()+iconPanelImage.getHeight());
        }
        else
        {
            trackNamePanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/NameBackground/ChannelName32.png"));
            iconPanelImage = ImageCache::getFromFile(File(String(GUI_PATH) + "icons/32/frame.png"));
            setSize (trackNamePanelImg.getWidth(), trackNamePanelImg.getHeight()+iconPanelImage.getHeight());
        }
        
        trackName = new RavenFuturaTextDisplay(owner);
        trackName->setColour(Label::textColourId, Colours::white);
        trackName->setFontSize(24);
        trackName->setBounds(0, 0, getWidth(), 32);
        addAndMakeVisible(trackName);
        trackName->setTag(eNSCTrackName);
        
        iconModeButton = new RavenButton();
        if(RAVEN_24)
            iconModeButton->setImagePaths("icons/24/frame.png", "icons/24/frame.png");
        else
            iconModeButton->setImagePaths("icons/32/frame.png", "icons/32/frame.png");
        iconModeButton->setTopLeftPosition(0, trackNamePanelImg.getHeight());
        ravenTouchButtons.add(iconModeButton);
        iconModeButton->addListener(this);
        addAndMakeVisible(iconModeButton);
        
    }
    
    void buttonStateChanged(Button* b)
    {
        if(b->getName().startsWith("trackSelectButton"))
        {
            if(b->getToggleState())
            {
                trackName->setColour(Label::textColourId, Colours::cyan);
            }
            else
            {
                trackName->setColour(Label::textColourId, Colours::white);
            }
        }
    }
    
	void buttonClicked(Button* b);
    
    void paint (Graphics &g)
    {
        g.fillAll(Colours::black);
        g.drawImageAt(trackNamePanelImg, 0, 0);
        //g.drawImageAt(iconPanelImage, 0, trackNamePanelImg.getHeight());
    }
    
    void paintOverChildren(Graphics &g)
    {
       g.drawImageAt(iconPanelImage, 0, trackNamePanelImg.getHeight()); 
    }
    
    void setChannelAndBankIndex(int chan, int bank)
    {
        trackName->setChannelIndex(chan);
        trackName->setBankIndex(bank);
    }
    
    void enterFlipMode()
    {
        trackName->setTag(-1);
    }
    
    void exitFlipMode()
    {
        trackName->setTag(eNSCTrackName);
    }
    
    String getTrackName() {return trackName->getText();}
    
    bool trackNameTouch(TUIO::TuioCursor *tcur) { return componentTouched(trackName, tcur, false); }
    
    void setTrackName(String _trackName)
    {
        if(_trackName == "    " || _trackName == "")
        {
            blank = true;
        }
        else
        {
            blank = false;
        }
        trackName->setText(_trackName, false);
        setIcon();
    }
    
    void setTrackNameBlank()
    {
        setTrackName("    ");
    }
    
    void setIconImg(Image _trackIconImg)
    {

    }
    
    void setIcon()
    {
        Image tempImage = (*iconMap)[trackName->getText()];//HashMap either returns an icon image or a default Image()
        if(iconModeButton->getNormalImage() != tempImage)
        {
            iconModeButton->setImages(true, false, true, tempImage, 1.0f, Colour(0x0), Image(), 1.0f, Colour(0x0), tempImage, 1.0f, Colour(0x0));
            repaint();
        }
    }
    
    
private:
    Image trackNamePanelImg;
    ScopedPointer<RavenFuturaTextDisplay> trackName;
    
    HashMap<String, Image> *iconMap;
    RavenButton *iconModeButton;
    Image iconPanelImage;
    bool blank;
};

#endif
