//
//  RavenLargeMeter.h
//  Raven
//
//  Created by Joshua Dickinson on 2/20/13.
//
//

#ifndef Raven_RavenLargeMeter_h
#define Raven_RavenLargeMeter_h

#include "RavenConfig.h"
#include "RavenInterpolator.h"
#include "MTDefines.h"

#define RAVEN_CLEAR_CLIP_HEIGHT         50
#define RAVEN_CLEAR_CLIP_HEIGHT_LARGE   100
#define RAVEN_METER_STAGES              43.0f
#define RAVEN_METER_SIZE_IN_PIXELS      251.0f//size of the active part of the meter

class RavenLargeMeter : public Component, public NscGuiElement, public Button::Listener
{
public:
    RavenLargeMeter(NscGuiContainer *owner, int bank, int chan, bool _isRightSide) : NscGuiElement(owner), /*ThreadPoolJob("Large Meter Thread"),*/level(0.0), needsUpdating(false), clip(false), peak(false), isRightSide(_isRightSide), lastDbIndex(0), interpolator(RAVEN_METER_SMOOTHING, 0.0), clearClipTapAreaHeight(50)
    {
        File offImgFile, onImgFile;

        offImgFile = File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/LargeMeters/Large-vu-off.png");
        onImgFile = File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/LargeMeters/Large-vu-on.png");
        clearClipTapAreaHeight = RAVEN_CLEAR_CLIP_HEIGHT_LARGE;
        meterOffImage = ImageCache::getFromFile(offImgFile);
        meterOnImage = ImageCache::getFromFile(onImgFile);
        
        if(isRightSide)
        {
            Rectangle<int> clipRectNumbers(42, 0, 18, meterOnImage.getHeight());
            meterNumbersImage = meterOffImage.getClippedImage(clipRectNumbers);
            Rectangle<int> clipRectOff(22, 0, 20, meterOffImage.getHeight());//these numbers don't make sense but they look right...
            meterOffImage = meterOffImage.getClippedImage(clipRectOff);
            Rectangle<int> clipRectOn(22, 0, 20, meterOnImage.getHeight());//these numbers don't make sense but they look right...
            meterOnImage = meterOnImage.getClippedImage(clipRectOn);
        }
        else
        {
            Rectangle<int> clipRectOff(0, 0, 22, meterOffImage.getHeight());
            meterOffImage = meterOffImage.getClippedImage(clipRectOff);
            Rectangle<int> clipRectOn(0, 0, 22, meterOnImage.getHeight());
            meterOnImage = meterOnImage.getClippedImage(clipRectOn);
        }
        
        setSize(meterOnImage.getWidth()+meterNumbersImage.getWidth(), meterOnImage.getHeight());
        
        meterClip = new RavenNSCButton(owner);
        meterClip->setTopLeftPosition(0, 0);
        
        meterPeak = new RavenNSCButton(owner);
        meterPeak->setTopLeftPosition(0, meterClip->getBottom()+5);
        
        if(isRightSide == false)
        {
            setTag(eNSCTrackMeter1);
            meterClip->setTag(eNSCTrackMeterClip1);
            meterPeak->setTag(eNSCTrackMeterPeak1);
        }
        else
        {
            setTag(eNSCTrackMeter2);
            meterClip->setTag(eNSCTrackMeterClip2);
            meterPeak->setTag(eNSCTrackMeterPeak2);
        }
        
        meterClip->addListener(this);
        meterPeak->addListener(this);
        
        setChannelIndex(chan);
        setBankIndex(bank);
        setClipPeakChannelBankIndex(chan, bank);
        
        setOpaque(true);
    }
    ~RavenLargeMeter()
    {

    }
    
    void drawMeterPeak(Graphics& g)
    {
        float peakLevel = meterPeak->getNscValue();
        int index = 0;
        //printf("peakLevel: %f\n", peakLevel);
        if(fabs(peakLevel-0.000000) < 0.0001)
        {
            index = 0;
            return;//don't draw peak if it's at 0.0
        }
        else if(fabs(peakLevel-0.083333) < 0.0001)
        {
            index = 4;
        }
        else if(fabs(peakLevel-0.166667) < 0.0001)
        {
            index = 5;
        }
        else if(fabs(peakLevel-0.250000) < 0.0001)
        { 
            index = 7;
        }
        else if(fabs(peakLevel-0.333333) < 0.0001)
        {
            index = 12;
        }
        else if(fabs(peakLevel-0.416667) < 0.0001)
        {
            index = 16;
        }
        else if(fabs(peakLevel-0.500000) < 0.0001)
        {
            index = 21;
        }
        else if(fabs(peakLevel-0.583333) < 0.0001)
        {
            index = 25;
        }
        else if(fabs(peakLevel-0.666667) < 0.0001)
        {
            index = 29;
        }
        else if(fabs(peakLevel-0.750000) < 0.0001)
        {
            index = 33;
        }
        else if(fabs(peakLevel-0.833333) < 0.0001)
        {
            index = 37;
        }
        else if(fabs(peakLevel-0.916667) < 0.0001)
        {
            index = 42;
        }
        
        int top = largeMeterStageBottoms[index];
        int bottom = largeMeterStageBottoms[index-1];
        
        int h = abs(top-bottom);//meterOnImage.getHeight()-23-top;
        int w = meterOnImage.getWidth();
        
        Rectangle<int> clipRectOff(0, top, w, h);//these numbers don't make sense but they look right...
        tempImage = meterOnImage.getClippedImage(clipRectOff);
        g.drawImageAt(tempImage, 0, top);
    }
    
    void paint (Graphics& g)
    {
        if (level < 0.0)
            level = 0.0;
        else if (level > 1.0)
            level = 1.0;
        
        g.drawImageAt(meterOffImage, 0, 0);
        if(isRightSide)
        {
            g.drawImageAt(meterNumbersImage, 20, 0);
        }

        int stage = level*RAVEN_METER_STAGES;
        int top = largeMeterStageBottoms[stage];
        
        /*
         HUI dB increments:
         0.000000: -inf to -50
         0.083333: -50 to -40
         0.166667: -40 to -30
         0.250000: -30 to -20
         0.333333: -20 to -15
         0.416667: -15 to -10
         0.500000: -10 to -8
         0.583333: -8 to -6
         0.666667: -6 to -4
         0.750000: -4 to -2
         0.833333: -2 to 0
         0.916667: CLIP*/
        //printf("clip %d\n", clip);
        if(clip == true)
        {
            //printf("clipping !\n");
            lastDbIndex = 11;
            Rectangle<int> clipRectOff(0, 20, meterOnImage.getWidth(), 8);
            g.drawImageAt(meterOnImage.getClippedImage(clipRectOff), 0, 20);
        }
        
        if(fabs(level-0.000000) < 0.0001)
        {
            lastDbIndex = 0;
            top = largeMeterStageBottoms[0];
        }
        else if(fabs(level-0.083333) < 0.0001)
        {
            if(lastDbIndex < 1)
                top = largeMeterStageBottoms[4];
            else
                top = largeMeterStageBottoms[5];
            
            lastDbIndex = 1;
        }
        else if(fabs(level-0.166667) < 0.0001)
        {
            if(lastDbIndex < 2)
                top = largeMeterStageBottoms[5];
            else
                top = largeMeterStageBottoms[6];
            
            lastDbIndex = 2;
        }
        else if(fabs(level-0.250000) < 0.0001)
        {
            if(lastDbIndex < 3)
                top = largeMeterStageBottoms[7];
            else
                top = largeMeterStageBottoms[11];
            
            lastDbIndex = 3;
        }
        else if(fabs(level-0.333333) < 0.0001)
        {
            if(lastDbIndex < 4)
                top = largeMeterStageBottoms[12];
            else
                top = largeMeterStageBottoms[15];
            
            lastDbIndex = 4;
        }
        else if(fabs(level-0.416667) < 0.0001)
        {
            if(lastDbIndex < 5)
                top = largeMeterStageBottoms[16];
            else
                top = largeMeterStageBottoms[20];
            
            lastDbIndex = 5;
        }
        else if(fabs(level-0.500000) < 0.0001)
        {
            if(lastDbIndex < 6)
                top = largeMeterStageBottoms[21];
            else
                top = largeMeterStageBottoms[24];
            
            lastDbIndex = 6;
        }
        else if(fabs(level-0.583333) < 0.0001)
        {
            if(lastDbIndex < 7)
                top = largeMeterStageBottoms[25];
            else
                top = largeMeterStageBottoms[29];
            
            lastDbIndex = 7;
        }
        else if(fabs(level-0.666667) < 0.0001)
        {
            if(lastDbIndex < 8)
                top = largeMeterStageBottoms[29];
            else
                top = largeMeterStageBottoms[32];
            
            lastDbIndex = 8;
        }
        else if(fabs(level-0.750000) < 0.0001)
        {
            if(lastDbIndex < 9)
                top = largeMeterStageBottoms[33];
            else
                top = largeMeterStageBottoms[36];
            
            lastDbIndex = 9;
        }
        else if(fabs(level-0.833333) < 0.0001)
        {
            if(lastDbIndex < 10)
                top = largeMeterStageBottoms[37];
            else
                top = largeMeterStageBottoms[42];
            
            lastDbIndex = 10;
        }
        else if(fabs(level-0.916667) < 0.0001)
        {
            top = largeMeterStageBottoms[43];
            lastDbIndex = 11;
        }

        int h = meterOnImage.getHeight()-23-top;
        int w = meterOnImage.getWidth();
        
        Rectangle<int> clipRectOff(0, top, w, h);//these numbers don't make sense but they look right...
        //use meterOnImage.getPixelData()
        tempImage = meterOnImage.getClippedImage(clipRectOff);
        //top blend (alpha multiple: 0 is completely transparent)
//        int numberOfPixelsToBlend = 5;
//        tempImage = meterOnImage.getClippedImage(clipRectOff).createCopy();
//        for(int i = 0;i<tempImage.getWidth();i++)
//        {
//            for(int j = 0;j<numberOfPixelsToBlend;j++)
//            {
//                tempImage.multiplyAlphaAt(i, j, (j+1)*1.0/(float)numberOfPixelsToBlend);
//            }
//        }
        
        g.drawImageAt(tempImage, 0, top);
        drawMeterPeak(g);
        
        //printf("meter peak: %f\n", meterPeak->getNscValue());
        
    }
    
    void buttonStateChanged (Button* b)
    {
        if(b == meterPeak)
        {
            if(b->getToggleState()) peak = true;
            else                    peak = false;
        }
        else if(b == meterClip)
        {
            if(b->getToggleState()) clip = true;
            else                    clip = false;
        }
        repaint();
    }
    void buttonClicked (Button* b){}
    
    void setClipPeakChannelBankIndex(int chan, int bank)
    {
        meterClip->setChannelIndex(chan);
        meterClip->setBankIndex(bank);
        meterPeak->setChannelIndex(chan);
        meterPeak->setBankIndex(bank);
    }
    
    // NscGuiElement overrides
	virtual void updateNscValue(float value)			// callback when DAW changes a value; generally
    {
        if(value != level) //only repaint if it has changed
        {
            needsUpdating = true;
            level = value;
            //repaint(); now gets painted in thread callback if needsUpdating
        }
    }
    
    bool checkMeterClipTouch(int touchX, int touchY)
    {
        if(!isVisible())
            return false;
        
        if(touchX >= getScreenX() && touchX <= getScreenX()+getWidth() && touchY >= getScreenY() && touchY <= getScreenY()+clearClipTapAreaHeight)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
           
    float getLevel()
   {
       return level;
   }
    
    void updateLevel()
    {
        if(needsUpdating)
        {
            repaint();
            needsUpdating = false;
        }
    }
    
private:
    
	Image meterOffImage;
    Image meterOnImage;
    Image meterNumbersImage;
    Image tempImage;
    float level;
    bool needsUpdating;
    bool clip, peak;
    bool isRightSide;
    int lastDbIndex;
    
    
    //invisible HUT buttons to determine peak and clip drawing
    ScopedPointer<RavenNSCButton> meterClip;
    ScopedPointer<RavenNSCButton> meterPeak;
    
    RavenInterpolator interpolator;
    int clearClipTapAreaHeight;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenLargeMeter)
};

class RavenLargeMeterPair : public Component
{
public:
    RavenLargeMeterPair(NscGuiContainer *owner, int bank, int chan)
    {
        meterL = new RavenLargeMeter(owner, bank, chan, false);
        meterR = new RavenLargeMeter(owner, bank, chan, true);
        meterR->setTopLeftPosition(meterL-> getWidth(), 0);
        addAndMakeVisible(meterL);
        addAndMakeVisible(meterR);
        setSize(meterL->getWidth() + meterR->getWidth(), meterL->getHeight() + meterR->getHeight());
    }
    
    ScopedPointer<RavenLargeMeter> meterL;
    ScopedPointer<RavenLargeMeter> meterR;
};

#endif
