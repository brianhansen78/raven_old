

#ifndef Raven_RavenCounterFunctionsComponent_h
#define Raven_RavenCounterFunctionsComponent_h

#include "RavenButton.h"
#include "RavenFunctionsComponent.h"
#include "RavenTouchComponent.h"

class RavenCounterFunctionsComponent : public RavenTouchComponent, public Button::Listener
{
public:
    RavenCounterFunctionsComponent(RavenFunctionsContainer *owner)
    {
        
        /*RavenNSCButton *counterModeButton = new RavenNSCButton(owner);
        counterModeButton->setTopLeftPosition(0, 20);
        counterModeButton->addListener(owner);
        counterModeButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/CounterMode/images/countermode-on_01.png", "TOOLBAR/FUNCTIONMODULE/CounterMode/images/countermode-off_01.png");
        counterModeButton->setTag(eNSCGSMinSecsCounterSwitch );
         */
        /*
         eNSCGSBarsBeatsCounterSwitch,//135
         eNSCGSSMPTECounterSwitch,
         eNSCGSFeetFramesCounterSwitch,
         eNSCGSMinSecsCounterSwitch,
         */
        
       // ravenNSCTouchButtons.add(counterModeButton);
        //addAndMakeVisible(counterModeButton);
        
        /*
        RavenButton *counterSizeButton = new RavenButton();
        counterSizeButton->setTopLeftPosition(0, counterModeButton->getHeight());
        counterSizeButton->addListener(this);
        counterSizeButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/CounterMode/images/countermode-on_02.png", "TOOLBAR/FUNCTIONMODULE/CounterMode/images/countermode-off_02.png");
        ravenTouchButtons.add(counterSizeButton);
        addAndMakeVisible(counterSizeButton);
         */
        
        counterButton = new RavenButton();
        counterButton->setClickingTogglesState(true);
        counterButton->setName("counterButton");
        counterButton->setTopLeftPosition(0, 0);
        //counterButton->addListener(owner);
        counterButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/CounterIcon/counterShow.png", "TOOLBAR/FUNCTIONMODULE/CounterIcon/counterHide.png");
        ravenTouchButtons.add(counterButton);
        addAndMakeVisible(counterButton);
        
        setSize(counterButton->getWidth(),counterButton->getHeight());        
    }
    
    RavenButton* getCounterButton(){return counterButton;}

    
    void buttonStateChanged (Button*)
    {
        
    }
    void buttonClicked (Button* b)
    {
        
    }
    
    void setCounterButtonListener(Button::Listener *listener)
    {
        counterButton->addListener(listener);
    }
    
private:
    
    RavenButton *counterButton;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenCounterFunctionsComponent)
    
};


#endif
