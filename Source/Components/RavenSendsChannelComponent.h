

#ifndef __JUCER_HEADER_RAVENSENDSCHANNELCOMPONENT_RAVENSENDSCOMPONENT_2F05906E__
#define __JUCER_HEADER_RAVENSENDSCHANNELCOMPONENT_RAVENSENDSCOMPONENT_2F05906E__

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenTouchComponent.h"
#include "NscGuiElement.h"
#include "RavenLargeMeter.h"

class RavenSendsChannelComponent : public RavenTouchComponent, public NscGuiElement, public Slider::Listener
{
public:
    RavenSendsChannelComponent (NscGuiContainer *owner, int bank, int chan) : NscGuiElement(owner), mOwner(owner), blankFrames(0), ignoreNextUnBlank(false), meterMode(false)
    {
        setSize(60, 294);
        
        if(RAVEN_24)
        {
            sendPanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendBackground/Background-Sends.png"));
            faderTrackImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendFader/FlipGuide-Fader-Send.png"));
            levelDisplayImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendDisplay/Display-Level-Send.png"));
            panLevelDisplayImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendDisplay/Display-Pan-Level-Send.png"));
        }
        else
        {
            sendPanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendBackground/Background-Sends.png"));
            faderTrackImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendFader/FlipGuide-Fader-Send.png"));
            levelDisplayImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendDisplay/Display-Level-Send.png"));
            panLevelDisplayImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendDisplay/Display-Pan-Level-Send.png"));
        }
        
        meterPanelImage = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendBackground/Background-Sends.png"));
        
        setSize(sendPanelImg.getWidth(), sendPanelImg.getHeight());
        
        const int buttonXOffset = 0;
        
        muteButton = new RavenNSCButton(owner);
        muteButton->setClickingTogglesState(true);
        addAndMakeVisible(muteButton);
            
        if(RAVEN_24)
        {
            muteButton->setTopLeftPosition(9, 125);
            muteButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendButtons/Bt-Mute-On-Send.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendButtons/Bt-Mute-Off-Send.png");
        }
        else
        {
            muteButton->setTopLeftPosition(2, 92);
            muteButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendButtons/Bt-Mute-On-Send.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendButtons/Bt-Mute-Off-Send.png");
        }
        
        ravenNSCTouchButtons.add(muteButton);
        
        aButton = new RavenButton();
        aButton->setClickingTogglesState(true);
        aButton->setRadioGroupId(99+bank+chan); //unique per chan
        aButton->setName("SendA");
        addAndMakeVisible(aButton);
        //aButton->setChannelIndex(kNscGuiBankGlobalIdentifier);
        //aButton->setTag(eNSCGSPTBankMode_SendA);

        if(RAVEN_24)
        {
            aButton->setTopLeftPosition(9, 162);
            aButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendButtons/Bt-A-On-Send.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendButtons/Bt-A-Off-Send.png");
        }
        else
        {
            //aButton->setTopLeftPosition(2, muteButton->getBottom());
            aButton->setTopLeftPosition(2, 120);
            aButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendButtons/Bt-A-On-Send.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendButtons/Bt-A-Off-Send.png");
        }
        
        ravenTouchButtons.add(aButton);
        
        bButton = new RavenButton();
        bButton->setClickingTogglesState(true);
        bButton->setRadioGroupId(99+bank+chan); //unique per chan
        bButton->setName("SendB");
        addAndMakeVisible(bButton);
        //bButton->setChannelIndex(kNscGuiBankGlobalIdentifier);
        //bButton->setTag(eNSCGSPTBankMode_SendB);
        
        if(RAVEN_24)
        {
            bButton->setTopLeftPosition(9, 199);
            bButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendButtons/Bt-B-On-Send.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendButtons/Bt-B-Off-Send.png");
        }
        else
        {
            //bButton->setTopLeftPosition(2, aButton->getBottom());
            bButton->setTopLeftPosition(2, 148);
            bButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendButtons/Bt-B-On-Send.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendButtons/Bt-B-Off-Send.png");
        }
        ravenTouchButtons.add(bButton);
        
        cButton = new RavenButton();
        cButton->setClickingTogglesState(true);
        cButton->setRadioGroupId(99+bank+chan); //unique per chan
        cButton->setName("SendC");
        addAndMakeVisible(cButton);
        //cButton->setChannelIndex(kNscGuiBankGlobalIdentifier);
        //cButton->setTag(eNSCGSPTBankMode_SendC);
        
        if(RAVEN_24)
        {
            cButton->setTopLeftPosition(9, 235);
        cButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendButtons/Bt-C-On-Send.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendButtons/Bt-C-Off-Send.png");
        }
        else
        {
            //cButton->setTopLeftPosition(2, bButton->getBottom());
            cButton->setTopLeftPosition(2, 177);
            cButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendButtons/Bt-C-On-Send.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendButtons/Bt-C-Off-Send.png");
        }
        ravenTouchButtons.add(cButton);

        dButton = new RavenButton();
        dButton->setClickingTogglesState(true);
        dButton->setRadioGroupId(99+bank+chan); //unique per chan
        dButton->setName("SendD");
        addAndMakeVisible(dButton);
        //dButton->setChannelIndex(kNscGuiBankGlobalIdentifier);
        //dButton->setTag(eNSCGSPTBankMode_SendD);
        
        if(RAVEN_24)
        {
            dButton->setTopLeftPosition(44, 161);
            dButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendButtons/Bt-D-On-Send.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendButtons/Bt-D-Off-Send.png");
        }
        else
        {
            //dButton->setTopLeftPosition(2, cButton->getBottom());
            dButton->setTopLeftPosition(2, 205);
            dButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendButtons/Bt-D-On-Send.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendButtons/Bt-D-Off-Send.png");
        }
        ravenTouchButtons.add(dButton);
        
        eButton = new RavenButton();
        eButton->setClickingTogglesState(true);
        eButton->setRadioGroupId(99+bank+chan); //unique per chan
        eButton->setName("SendE");
        addAndMakeVisible(eButton);
        //eButton->setChannelIndex(kNscGuiBankGlobalIdentifier);
        //eButton->setTag(eNSCGSPTBankMode_SendE);
        
        if(RAVEN_24)
        {
            eButton->setTopLeftPosition(44, 199);
            eButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendButtons/Bt-E-On-Send.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendButtons/Bt-E-Off-Send.png");
        }
        else
        {
            //eButton->setTopLeftPosition(1, dButton->getBottom());
            eButton->setTopLeftPosition(2, 232);
            eButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendButtons/Bt-E-On-Send.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendButtons/Bt-E-Off-Send.png");
        }
        ravenTouchButtons.add(eButton);
        
        preButton = new RavenNSCButton(owner);
        preButton->setClickingTogglesState(true);
        preButton->setTopLeftPosition(buttonXOffset, eButton->getBottom());
        addAndMakeVisible(preButton);
        preButton->setTag(eNSCTrackVSelectSwitch);
        
        if(RAVEN_24)
        {
            preButton->setTopLeftPosition(44, 235);
            preButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendButtons/Bt-Pre-On-Send.png", "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/SendButtons/Bt-Pre-Off-Send.png");
        }
        else
        {
            //preButton->setTopLeftPosition(2, eButton->getBottom());
            preButton->setTopLeftPosition(2, 259);
            preButton->setImagePaths("FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendButtons/Bt-Pre-On-Send.png", "FLAT_GRAPHICS/MIXER_AND_SEND32/Pictures/SendButtons/Bt-Pre-Off-Send.png");
        }
        ravenNSCTouchButtons.add(preButton);
        
        panLLabel = new RavenLabel();
        if(RAVEN_24)
            panLLabel->setBounds(8, 94, 28, 23);
        else
            panLLabel->setBounds(4, 66, 28, 23);
        
        Font font = panLLabel->getFont();
        font.setHeight(14.0f);
        panLLabel->setFont(font);
        panLLabel->setJustificationType(Justification::centredBottom);
        panLLabel->setText("0", false);
        panLLabel->setEditable(false);
        addAndMakeVisible(panLLabel);
        
        panRLabel = new RavenLabel();
        
        if(RAVEN_24)
            panRLabel->setBounds(44, 94, 28, 23);
        else
            panRLabel->setBounds(getWidth()/2, 66, 28, 23);
        
        panRLabel->setFont(font);
        panRLabel->setJustificationType(Justification::centredBottom);
        panRLabel->setText("0", false);
        panRLabel->setEditable(false);
        addAndMakeVisible(panRLabel);
        
        dBLabel = new RavenLabel();
        
        if(RAVEN_24)
            dBLabel->setBounds(2, 270, 76, 25);
        else
            dBLabel->setBounds(12, 90, getWidth(), 25);
        
        dBLabel->setFont(font);
        dBLabel->setJustificationType(Justification::centredBottom);
        
        if(!RAVEN_24)
            font.setHeight(10.0f);//make font smaller for smaller display area
        
        dBLabel->setText("-inf", false);
        addAndMakeVisible(dBLabel);
        
        
        
        if(RAVEN_24)
        {
            fader = new RavenSendFader(owner, 287, bank, chan);
            fader->setTopLeftPosition(22, 272);
        }
        else
        {
            fader = new RavenSendFader(owner, 122, bank, chan);
            fader->setTopLeftPosition(27, 98);
        }
        
        fader->addListener(this);
        addAndMakeVisible(fader);
        fader->setValue(0.0, dontSendNotification);
        
        fader->setDBLabel(dBLabel);
        fader->setValue(fader0dbVal, sendNotification);
        
        knob = new RavenKnob(owner, bank, chan);
        knob->setKnobType(eKnobSendMono);
        
        if(RAVEN_24)
            knob->setTopLeftPosition(4, 7);
        else
            knob->setTopLeftPosition(0, 5);
        
        knob->addListener(this);
        knob->setValue(0.5, dontSendNotification);
        addAndMakeVisible(knob);
        
        knob->setPanLLabel(panLLabel);
        knob->setPanRLabel(panRLabel);
        
        //Send A by default
        aButton->triggerClick();
        
//        largeMeters = new RavenLargeMeterPair(owner, bank, chan);
//        addAndMakeVisible(largeMeters);
    }
    
    ~RavenSendsChannelComponent()
    {
        
    }
    
    void enterFlipMode()
    {
        fader->setTag(eNSCTrackFaderValue);
        knob->setTag(eNSCTrackVPotValue);
        muteButton->setTag(eNSCTrackMuteSwitch);
        aButton->setToggleState(true, false);
    }
    void exitFlipMode()
    {
        fader->setTag(-1);
        knob->setTag(-1);
        muteButton->setTag(-1);
    }
    
    void zeroFader(){
//        fader->setTag(eNSCTrackFaderValue);
        fader->setValue(fader0dbVal, sendNotification);
    }
    void zeroKnob()
    {
        knob->zeroKnob();
        //knob->setValue(0.5, sendNotification);
    }
    
    void sliderValueChanged (Slider *slider)
    {
        if(slider == knob)
        {
            knob->updatePanning();
            
            /*
            float val = knob->getValue();
            
            int panL, panR;
            if (val <= 0.5)
            {
                panR = 0;
                panL = (0.5-val)*200;
            }
            else
            {
                panL = 0;
                panR = (val-0.5)*200;
            }
            
            panLLabel->setText(String(panL), false);
            panRLabel->setText(String(panR), false);
            */
        }
        
        if(slider == fader)
        {
            dBLabel->setText(String(fader->getDBVal()).substring(0, 4), false);
        }
        
    }
    
    void sliderDragStarted (Slider *slider) {}
    void sliderDragEnded (Slider *slider) {}
    
    void setGlobalSendChannel(Button *sendButtonPushed)
    {
        if(sendButtonPushed->getName() == "SendA")
            aButton->setToggleState(true, false);
        else if(sendButtonPushed->getName() == "SendB")
            bButton->setToggleState(true, false);
        else if(sendButtonPushed->getName() == "SendC")
            cButton->setToggleState(true, false);
        else if(sendButtonPushed->getName() == "SendD")
            dButton->setToggleState(true, false);
        else if(sendButtonPushed->getName() == "SendE")
            eButton->setToggleState(true, false);
    }
    
    void setChannelAndBankIndex(int chan, int bank)
    {
        fader->setChannelIndex(chan);
        fader->setBankIndex(bank);
        
        knob->setChannelIndex(chan);
        knob->setBankIndex(bank);
        
        for(int i = 0; i < ravenNSCTouchButtons.size(); i++) ravenNSCTouchButtons[i]->setBankIndex(bank);
        
        muteButton->setChannelIndex(chan);
        muteButton->setBankIndex(bank);
        
        preButton->setChannelIndex(chan);
        preButton->setBankIndex(bank);
    }
    
    void addSliderListener(Slider::Listener * listener)
    {   
        fader->addListener(listener);
        knob->addListener(listener);
    }
    void addButtonListener(Button::Listener * listener)
    {
        for(int i = 0; i < ravenNSCTouchButtons.size(); i++) ravenNSCTouchButtons[i]->addListener(listener);
    }
    
    void addSendsChannelButtonListener(Button::Listener * listener)
    {
        aButton->addListener(listener);
        bButton->addListener(listener);
        cButton->addListener(listener);
        dButton->addListener(listener);
        eButton->addListener(listener);
    }
    
    void setFineFader(bool ff)
    {
        fineFader = ff;
    }
    
    void beginFader()
    {
        fader->isBeingTouched = true;
        mOwner->sliderBegan(fader);
    }
    
    void endFader()
    {
        printf("end fader\n");
        mOwner->sliderEnded(fader);
        fader->isBeingTouched = false;
    }
    
    bool isBlank()
    {
        return blank;
    }
    
    void setBlankMaster(bool _blank)
    {
        ignoreNextUnBlank = false;
        setBlank(_blank);
        if(_blank == true)
            ignoreNextUnBlank = true;
    }
    
    void setBlank(bool _blank)
    {
        if(meterMode == false)
        {
//            largeMeters->meterL->setVisible(false);
//            largeMeters->meterR->setVisible(false);
            
            if(_blank == true)
            {
                muteButton->setVisible(false);
                aButton->setVisible(false);
                bButton->setVisible(false);
                cButton->setVisible(false);
                dButton->setVisible(false);
                eButton->setVisible(false);
                preButton->setVisible(false);
                panLLabel->setVisible(false);
                panRLabel->setVisible(false);
                dBLabel->setVisible(false);
                fader->setVisible(false);
                knob->setBlank(true);
            }
            else
            {
                if(ignoreNextUnBlank == false)
                {
                    setBlankFrames(0);
                    muteButton->setVisible(true);
                    aButton->setVisible(true);
                    bButton->setVisible(true);
                    cButton->setVisible(true);
                    dButton->setVisible(true);
                    eButton->setVisible(true);
                    preButton->setVisible(true);
                    panLLabel->setVisible(true);
                    panRLabel->setVisible(true);
                    dBLabel->setVisible(true);
                    fader->setVisible(true);
                    knob->setBlank(false);
                }
            }
        }
        else//meterMode == true
        {
            muteButton->setVisible(false);
            aButton->setVisible(false);
            bButton->setVisible(false);
            cButton->setVisible(false);
            dButton->setVisible(false);
            eButton->setVisible(false);
            preButton->setVisible(false);
            panLLabel->setVisible(false);
            panRLabel->setVisible(false);
            dBLabel->setVisible(false);
            fader->setVisible(false);
            knob->setBlank(true);
            
            if(_blank == true)
            {
//                largeMeters->meterL->setVisible(false);
//                largeMeters->meterR->setVisible(false);
            }
            else
            {
                if(ignoreNextUnBlank == false)
                {
                    setBlankFrames(0);
//                    largeMeters->meterL->setVisible(true);
//                    largeMeters->meterR->setVisible(true);
                }
            }
        }
        ignoreNextUnBlank = false;
        
        if(blank != _blank)
        {
            repaint();   
        }
        blank = _blank;
    }
    
    void setBlankFrames(int _blankFrames)
    {
        blankFrames = _blankFrames;
    }
    
    void checkBlankFrames()
    {
        blankFrames++;
        if(blankFrames >= RAVEN_FRAMES_UNTIL_BLANK)
            setBlank(true);
    }
    
    void setMeterMode(bool _meterMode)
    {
        meterMode = _meterMode;
        setBlank(blank);
        repaint();
    }
    
    void paint(Graphics &g)
    {
        if(meterMode == true)
        {
            g.drawImageAt(meterPanelImage, 0, 0);
        }
        else
        {
            g.drawImageAt(sendPanelImg, 0, 0);
        }
        
        if(blank == false && meterMode == false)
        {
            if(RAVEN_24)
            {
                g.drawImageAt(faderTrackImg, 11, 316);
                g.drawImageAt(levelDisplayImg, 4, 270);
                g.drawImageAt(panLevelDisplayImg, 4, 92);//88
            }
            else
            {
                g.setOpacity(0.63);
                g.drawImageAt(faderTrackImg, 25, 129);
                g.setOpacity(1.0);
                g.drawImageAt(levelDisplayImg, 25, 96);
                g.drawImageAt(panLevelDisplayImg, 2, 70);
            }
        }
    }
    
    void setNscLink	(class NscLink * link_) {link = link_; knob->setNscLink(link_);}
    
//    bool checkMeterClipTouch(int touchX, int touchY)
//    {
//        if(meterMode == false)
//        {
//            return false;
//        }
//        else
//        {
//            return (largeMeters->meterL->checkMeterClipTouch(touchX, touchY) || largeMeters->meterR->checkMeterClipTouch(touchX, touchY));
//        }
//    }
    
//    RavenLargeMeterPair *getLargeMeters() { return largeMeters; }
    
private:
    
    NscGuiContainer *mOwner;
    ScopedPointer<RavenLabel> panLLabel;
    ScopedPointer<RavenLabel> panRLabel;
    ScopedPointer<RavenLabel> dBLabel;
    
    RavenNSCButton *muteButton;
    RavenNSCButton *preButton;
    
    RavenButton *aButton;
    RavenButton *bButton;
    RavenButton *cButton;
    RavenButton *dButton;
    RavenButton *eButton;
    
    Image sendPanelImg, faderTrackImg;
    Image panLevelDisplayImg, levelDisplayImg;
    
    NscLink *link;
    
    bool blank;
    int blankFrames;
    bool ignoreNextUnBlank;
    
    bool meterMode;
    Image meterPanelImage;
//    ScopedPointer<RavenLargeMeterPair> largeMeters;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenSendsChannelComponent)
};


#endif   
