
#ifndef Raven_NewSDK_RavenMixerContainer_h
#define Raven_NewSDK_RavenMixerContainer_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenConfig.h"
#include "MouseHideComponent.h"
#include "NscSDKIncludes.h"
#include "RavenWindowComponent.h"
#include "RavenMixerComponent.h"
#include "RavenSendsComponent.h"
#include "RavenTrackNameComponent.h"
#include "RavenCounterComponent.h"
#include "RavenInsertsPalette.h"
#include "RavenHotKeyProgrammingComponent.h"
#include "RavenFunctionsComponent.h"
#include "RavenWindowManager.h"
#include "RavenSaveLayoutHandler.h"
#include "RavenDrumPadComponent.h"
#include "RavenFloatingEssentialsComponent.h"
#include "RavenNavPadComponent.h"
#include "RavenFloatingNavPadComponent.h"
#include "RavenLargeMetersComponent.h"
#include "RavenWindow.h"
#include "RavenFunctionsManager.h"
#include "RavenSettingsComponent.h"
#include "RavenSettingsComponent.h"
#include "RavenIconPaletteComponent.h"
#include "RavenMessage.h"
#include "RavenBackgroundImageComponent.h"
#include "RavenAlertComponent.h"

#define RAVEN RavenRackContainer::getInstance()
#define RAVEN_TOUCH_LOCK RAVEN->setTouchLock(true);
#define RAVEN_TOUCH_UNLOCK RAVEN->setTouchLock(false);

class RavenRackContainer : public RavenWindowManager, public Button::Listener, public ActionListener, public ActionBroadcaster, public MessageListener, public Timer
{
public:
    
    void init()
    {
        
#if RAVEN_24
        currentRavenVersion = "MTI";
#else 
        currentRavenVersion = "MTX";
#endif
        
        /////////////////////////////////////////////////////////////////////////////////
        // Build Rack////////////////////////////////////////////////////////////////////
        
        //Build from the bottom-up
        int rackPos = 0;
        
        //Functions
        curNumFunctionsBars = 1;
        
        ravenBackground = new RavenBackgroundImageComponent;
        backgroundWindow = new RavenWindow(ravenBackground);
        backgroundWindow->setName("backgroundWindow");
        backgroundWindow->setTopLeftPosition(0, osxMenuBarHeight);
        
        alertComponent = new RavenAlertComponent;
        alertWindow = new RavenWindow(alertComponent);
        alertWindow->setName("alertWindow");
        alertWindow->centreWithSize(alertComponent->getWidth(), alertComponent->getHeight());
        alertComponent->setWindow(alertWindow);
        
        functions1 = new RavenFunctionsComponent(true);
        functions1->setName("Functions");
        functionsWindow1 = new RavenWindow(functions1, rackPos);
        functionsWindow1->setTopLeftPosition(0, SCREEN_HEIGHT - functions1->getHeight());
        functionsWindow1->setOrigHeight(functions1->getHeight());
        functionsWindow1->setName("functionsWindow");
        functions1->setWindow(functionsWindow1);
        windows.add(functionsWindow1);
        allRackWindows.add(functionsWindow1);
        
        functions2 = new RavenFunctionsComponent(false);
        functions2->setName("Functions2");
        functionsWindow2 = new RavenWindow(functions2, rackPos);
        functionsWindow2->setTopLeftPosition(0, functionsWindow1->getY()-functions2->getHeight());
        functionsWindow2->setOrigHeight(functions2->getHeight());
        functionsWindow2->setName("functionsWindow2");
        functions2->setWindow(functionsWindow2);
        allRackWindows.add(functionsWindow2);
        
        functions3 = new RavenFunctionsComponent(false);
        functions3->setName("Functions3");
        functionsWindow3 = new RavenWindow(functions3, rackPos);
        functionsWindow2->setTopLeftPosition(0, functionsWindow2->getY()-functions3->getHeight());
        functionsWindow3->setOrigHeight(functions3->getHeight());  //functions2->getHeight()
        functionsWindow3->setName("functionsWindow3");
        functions3->setWindow(functionsWindow3);
        allRackWindows.add(functionsWindow3);
        
        functionsManager = new RavenFunctionsManager();
        functionsManager->setRackContainerButtonListener(this);
        functionsManager->setRackContainerActionListener(this);
        functionsManager->addFunctionsComponent(functions1);
        functionsManager->addFunctionsComponent(functions2);
        functionsManager->addFunctionsComponent(functions3);
        functionsManager->setDefaultLayout();  //NOTE: function that distributes modules to functions components.
        
        rackPos++;
        
        trackNames = new RavenTrackNameComponent(numBanks, chansPerBank);
        trackNames->setName("TrackNames");
        namesWindow = new RavenWindow(trackNames, rackPos);
        namesWindow->setTopLeftPosition(0, functionsWindow1->getY()-trackNames->getHeight());
        namesWindow->setOrigHeight(trackNames->getHeight());
        //NOTE: the line below allows for setting a component's max/min size to be less/greater than the juce min/max sizes.
        namesWindow->setResizeLimits(screenWidth, namesWindow->getHeight(), screenWidth, namesWindow->getHeight());
        
        namesWindow->setName("namesWindow");
        trackNames->setWindow(namesWindow);
        windows.add(namesWindow);
        allRackWindows.add(namesWindow);
        
        rackPos++;
        
        mixer = new RavenMixerComponent(numBanks, chansPerBank);
        mixer->setName("Mixer");
        mixer->setRackContainerButtonListener(this);
        mixerWindow = new RavenWindow(mixer, rackPos);
        mixerWindow->setTopLeftPosition(0, namesWindow->getY()-mixer->getHeight());
        mixerWindow->setOrigHeight(mixer->getHeight());
        mixerWindow->setName("mixerWindow");
        mixer->setWindow(mixerWindow);
        windows.add(mixerWindow);
        allRackWindows.add(mixerWindow);
        
        rackPos++;
        
        sends = new RavenSendsComponent(numBanks, chansPerBank);
        sends->setName("Sends");
        sendsWindow = new RavenWindow(sends, rackPos);
        sendsWindow->setTopLeftPosition(0, mixerWindow->getY()-sends->getHeight());
        sendsWindow->setOrigHeight(sends->getHeight());
        sendsWindow->setName("sendsWindow");
        sends->setWindow(sendsWindow);
        if(!RAVEN_24) windows.add(sendsWindow);
        allRackWindows.add(sendsWindow);
        
        trackNames->setMixerComponent(mixer);
        trackNames->setSendsComponent(sends);
        
        //Save Layout Handler
        saveLayoutHandler = new RavenSaveLayoutHandler(functionsManager, this);
                
        updateRemainingHeight();
        
        //Track Names listen to track select button to change font color
        for(int b = 0; b < numBanks; b++)
        {
            for(int c = 0; c < chansPerBank; c++)
            {
                mixer->getBank(b)->getChannel(c)->getTrackSelectButton()->addListener(trackNames->getBank(b)->getChannel(c));
            }
        }
        
        /////////////////////////////////////////////////////////////////////////////////
        // Palettes ////////////////////////////////////////////////////////////////////
        
        counter = new RavenCounterComponent();
        counter->setName("Counter");
        counterWindow = new RavenWindow(counter);
        counterWindow->setName("counterWindow");
        counterWindow->setTopLeftPosition(1000, 270);
        counter->setWindow(counterWindow);
        floatingWindows.add(counterWindow);
        
        floatingNavPad = new RavenFloatingNavPadComponent(functionsManager, functionsManager->getNavPadComponent());
        floatingNavPad->setName("floatingNavPad");
        floatingNavPad->setButtonListener(functionsManager->getNavPadComponent());
        functionsManager->getNavPadComponent()->setButtonListener(floatingNavPad);
        functionsManager->getNavPadComponent()->addActionListener(floatingNavPad);
        navpadWindow = new RavenWindow(floatingNavPad);
        navpadWindow->setName("navpadWindow");
        navpadWindow->setTopLeftPosition(1000, 270);
        floatingNavPad->setWindow(navpadWindow);
        floatingWindows.add(navpadWindow);
        
        floatingEssentials = new RavenFloatingEssentialsComponent();
        floatingEssentials->setName("FloatingEssentials");
        floatingEssentials->setButtonListenersForEssentials(this); //NOTE: was functionsManager before...
        functionsManager->setFloatingEssentials(floatingEssentials);
        saveLayoutHandler->setFloatingEssentialsForHandler(floatingEssentials);
        essentialsWindow = new RavenWindow(floatingEssentials);
        essentialsWindow->setName("essentialsWindow");
        essentialsWindow->setTopLeftPosition(1000, 270);
        floatingEssentials->setWindow(essentialsWindow);
        floatingWindows.add(essentialsWindow);
        
        iconPalette = new RavenIconPaletteComponent();
        iconPalette->setName("iconPalette");
        iconPaletteWindow = new RavenWindow(iconPalette, 0);
        iconPaletteWindow->setName("iconPaletteWindow");
        iconPaletteWindow->setTopLeftPosition(0, screenHeight-RAVEN_TOOLBAR_HEIGHT-300);
        iconPalette->setWindow(iconPaletteWindow);
        floatingWindows.add(iconPaletteWindow);
        
        insertsPalette = new RavenInsertsPalette();
        insertsPalette->setName("InsertsPalette");
        insertsPalette->addActionListener(this);
        insertsWindow = new RavenWindow(insertsPalette);
        insertsWindow->setName("insertsWindow");
        insertsWindow->setTopLeftPosition(1000, 270);
        insertsPalette->setWindow(insertsWindow);
        floatingWindows.add(insertsWindow);
        
        settings = new RavenSettingsComponent();
        settings->setName("settingsPalette");
        settings->setSettingsButtonListener(this);
        settingsWindow = new RavenWindow(settings);
        settingsWindow->setName("settingsWindow");
        settingsWindow->setTopLeftPosition(0, 500);//functionsWindow3->getY() - settings->getHeight() - 20); //problem here?
        settings->setWindow(settingsWindow);
        floatingWindows.add(settingsWindow);
        
        initializeCounter(functionsManager);
        initializeInsertsPalette(functionsManager);
        
        hotKeyPanel = new RavenHotKeyProgrammingComponent();
        hotKeyPanel->addActionListener(this);      
        hotkeyWindow = new RavenWindow(hotKeyPanel);
        hotkeyWindow->setTopLeftPosition(0, 0);
        hotKeyPanel->setWindow(hotkeyWindow);
        functionsManager->setHotKeyPanel(hotKeyPanel);
        
        functionsManager->setInsertWindowButton(insertsPalette->getInsertWindowButton(0));
        //start timer keeping the function ins win button matching the actual NSC button in the palette
        functionsManager->startTimer(FUNCTIONS_INSWIN_BUTTON_TIMER, 200);
        
        addActionListener(functionsManager->getSaveLayoutComponent());
    
        //printf("names window: %d, namesComponent: %d\n", namesWindow->getHeight(), trackNames->getHeight());
        
#if RAVEN_ADD_TRANSPARENT_MIXER
        transparentMixer = new RavenTransparentMixerComponent();
        transparentMixer->setName("transparentMixer");
        transparentMixerWindow = new RavenWindow(transparentMixer);
        transparentMixerWindow->setTopLeftPosition(0, 0);
        transparentMixerWindow->setOrigHeight(transparentMixer->getHeight());
        transparentMixerWindow->setName("transparentMixerWindow");
        transparentMixer->setWindow(transparentMixerWindow);
        transparentMixerWindow->addToDesktopWithOptions(RAVEN_PALETTE_LEVEL);
//        windows.add(transparentMixerWindow);
//        allRackWindows.add(transparentMixerWindow);
#endif

        
    }
    
    static RavenRackContainer* getInstance()
    {
        if (!instance)   // Only allow one instance of class to be generated.
            instance = new RavenRackContainer();
        return instance;
    }
    
    void close()
    {
        delete instance;
    }
    
    void setHybridMode(bool _hybridMode, int mode = RETURN_MIX_MODE)
    {
        if(_hybridMode)
        {
            //enter hybrid mode
            hybridMode = true;
            functionsManager->enterHybridMode(); //this just shows FE
            mixer->enterHybridMode();
            
            setHybridModeRackWindows(true, mode);
            
            if(iconPaletteWindow->isOnDesktop())
            {
                iconPaletteWindow->removeFromDesktop();
                iconPalette->deActivateIcon();
            }
        }
        else
        {
            //exit hybrid mode
            hybridMode = false;
            mixer->exitHybridMode();
            functionsManager->exitHybridMode();
            
            setHybridModeRackWindows(false, mode);
        }
    }
    
    void setHybridModeRackWindows(bool enterHybridMode, int mode)
    {
        if(enterHybridMode)
        {
            removeWindowFromRack(sendsWindow);
            removeWindowFromRack(namesWindow);
            backgroundWindow->removeFromDesktop();
            
            if(getCurrentNumFunctionBars() >= 1) functionsWindow1->setRackPos(0);
            if(getCurrentNumFunctionBars() >= 2) functionsWindow2->setRackPos(1);
            if(getCurrentNumFunctionBars() >= 3) functionsWindow3->setRackPos(2);

            mixerWindow->setRackPos(getCurrentNumFunctionBars());
        }
        else if(mode == RETURN_MIX_MODE) //return to mixer mode...
        {
            if(shouldShowBackground()) showBackground();
            insertWindowIntoRack(namesWindow, getCurrentNumFunctionBars(), false);
            //mixerWindow->setRackPos(getCurrentNumFunctionBars()+1);
            if(getCurrentNumFunctionBars() < 2) insertWindowIntoRack(sendsWindow, mixerWindow, false);
        }
        else if(mode == RETURN_DAW_MODE) //return to DAW or custom mode mode...
        {
            //no sends, names, or mixer windows.
            removeWindowFromRack(mixerWindow);
        }
        
        
        
        rebuildRack(); //NOTE: only rebuild rack here if exiting hybrid mode via hybrid button...
    }
    
    bool isHybridMode(){ return hybridMode;}
    
    void setHybridPanMode(bool _hybridPanMode)
    {
        hybridPanMode = _hybridPanMode;
        mixer->setHybridPanMode(hybridPanMode);
    }
    bool isHybridPanMode(){ return hybridPanMode;}
    bool isSavedHybridLayout(){ return savedHybridLayout;}
    
    void setDawMode(bool _dawMode){ dawMode = _dawMode;}
    bool isDawMode(){ return dawMode;}
    
    void setSaveToSession(bool _default){ saveToSession = _default; }
    bool isSaveToSession(){ return saveToSession;}
        
    void buttonStateChanged (Button*)
    {
    }
    
    void buttonClicked (Button* b)
    {
        
        //std::cout << "button name: " << b->getName() << std::endl;
        
        if(b->getName().compare("toolbarMode") == 0)
        {            
            //Don't allowing entering DAW Mode from Custom Mode
            if(functionsManager->isCustomMode())
            {
                b->setToggleState(!b->getToggleState(), false);//TODO: use layout handler for this
                return; 
            }
            
            if(b->getToggleState()) //Entering toolbar/DAW mode
            {
                //1. save the current layout for Mixer mode
                saveLayoutHandler->saveUserLayout(false, MIX_DAW_INDEX);
                
                //if in hybrid mode. need to exit to DAW mode setup. return to DAW mode.
                if(isHybridMode()) setHybridMode(false, RETURN_DAW_MODE);
                
                //3. fetch DAW Mode layout from saved layouts.
                //savedWindowLayout.clear();
                saveLayoutHandler->loadUserLayout(true, MIX_DAW_INDEX);
                
                //4. rebuild the rack with retrieved layout.
                loadSavedRackWindowLayout();
                loadSavedFloatingWindowsLayout();
                functionsManager->loadSavedFunctionsLayout();
                
                //5. make sure insertsPalette is not visible
                insertsWindow->removeFromDesktop();
                
                //6. save the sate boolean
                setDawMode(true);
                functionsManager->enableMoveModules(false);
                functionsManager->enableHybridMode(false);

                //curNumFunctionsBars = getSavedNumberOfDawModeBars(); NOTE: will get this when layout is saved properly
                //NOTE: curNumFunctionsBars could be automatically set in rebuild rack...
                
            }
            else //Mixer mode (Exiting toolbar/DAW mode)
            {
                //1. save the current layout for DAW mode
                saveLayoutHandler->saveUserLayout(true, MIX_DAW_INDEX);
                
                //2. fetch Mixer Mode layout from saved layouts.
                saveLayoutHandler->loadUserLayout(false, MIX_DAW_INDEX);
                
                //3. rebuild the rack with new layout.
                if(isSavedHybridLayout()) setHybridMode(true);
                
                loadSavedRackWindowLayout();
                loadSavedFloatingWindowsLayout();
                functionsManager->loadSavedFunctionsLayout();
                
                //4. check state of inserts palette and enable move modules on functions component.
                if(insertsPalette->shouldBeVisible()) insertsWindow->addToDesktopWithOptions(RAVEN_PALETTE_LEVEL); //insertsPalette->setVisible(true);
                functionsManager->enableMoveModules(true);
                functionsManager->enableHybridMode(true);
                setDawMode(false);
            }
        }
        else if(b->getName().compare("essentialsPaletteCustom") == 0)
        {
            //syncing with the customize toolbar button here.
            if(!functionsManager->isCustomMode())
                functionsManager->getToolbarCustomButton()->triggerClick();
        }
        else if(b->getName().compare("toolbarCustom") == 0)
        {
            if(b->getToggleState()) //enter customize mode
            {
                //1. save prior rack layout. (To be called when exiting custom mode)
                saveLayoutHandler->saveUserLayout(isDawMode(), PRIOR_CUSTOM_VIEW_INDEX);
                
                //***********
                //if in hybrid mode. need to exit to DAW mode setup. return to CUSTOM mode.
                if(isHybridMode()) setHybridMode(false, RETURN_DAW_MODE);
                //***********
                
                //2. rearrange rack for custom mode
                Array<windowInfo> rackWindowLayout;
                windowInfo wi;
                
                wi.windowName = "functionsWindow";
                wi.rackPosition = 0;
                wi.isOnDesktop = 1;
                rackWindowLayout.add(wi);
                
                wi.windowName = "functionsWindow2";
                wi.rackPosition = 1;
                wi.isOnDesktop = 1;
                rackWindowLayout.add(wi);
                
                wi.windowName = "functionsWindow3";
                wi.rackPosition = 2;
                wi.isOnDesktop = 1;
                rackWindowLayout.add(wi);
                
                setSavedRackWindowLayout(rackWindowLayout);
                loadSavedRackWindowLayout();
                
                //3. only show FE palette, settings windows, and background.
                essentialsWindow->setTopLeftPosition(1384, 360);
                essentialsWindow->addToDesktopWithOptions(RAVEN_PALETTE_LEVEL);
                settingsWindow->setTopLeftPosition(302, 492);
                settingsWindow->addToDesktopWithOptions(RAVEN_PALETTE_LEVEL);
                if(shouldShowBackground()) showBackground();
                
                for(int i = 0; i < floatingWindows.size(); i++)
                {
                    if(floatingWindows[i] == essentialsWindow || floatingWindows[i] == settingsWindow)
                        continue;
                    else
                        floatingWindows[i]->removeFromDesktop();
                }
                
                functionsManager->enterCustomMode();
                floatingEssentials->getEssentialsCustomButton()->setToggleState(true, false);
                
                
            }
            else  //exit customize mode
            {
                //1. set customMode = false in functions, functions2, and functions3 (disables moving modules).
                settingsWindow->removeFromDesktop();
                functionsManager->exitCustomMode();
                floatingEssentials->getEssentialsCustomButton()->setToggleState(false, false);
                
                //2. fetch prior custom mode saved layout
                saveLayoutHandler->loadUserLayout(isDawMode(), PRIOR_CUSTOM_VIEW_INDEX);
                
                //3. load pre-custom mode rack layout
                loadSavedRackWindowLayout();
                loadSavedFloatingWindowsLayout(); //NOTE: floating essentials may be affected by this...
                //functionsManager->loadSavedFunctionsLayout(); Don't load functions layouts because this assumes we just changed them!
                
                if(isSavedHybridLayout()) setHybridMode(true);
             }
        }
        else if(b->getName().compare("defaultLayoutsButton") == 0)
        {
            //restore factory default layouts and fetch initial layout from xml
            if(saveLayoutHandler->restoreDefaultLayouts())
            {
                saveLayoutHandler->loadUserLayout(isDawMode(), 1);
                
                //exit custom mode.
                settingsWindow->removeFromDesktop();
                functionsManager->exitCustomMode();
                floatingEssentials->getEssentialsCustomButton()->setToggleState(false, false);

                //load initial default window layout for rack, floating palettes, and functions modules.
                loadSavedRackWindowLayout();
                loadSavedFloatingWindowsLayout();
                functionsManager->loadSavedFunctionsLayout();
    //            b->setToggleState(false, false);
            }
        }
        else if(b->getName().compare("saveSession") == 0)
        {
            saveLayoutHandler->saveSession();
        }
        else if(b->getName().compare("loadSession") == 0)
        {
            if(saveLayoutHandler->loadSession())
            {
                //sync hybird mode state.
                if(isHybridMode() != isSavedHybridLayout())
                {
                    if(!isSavedHybridLayout())
                    {
                        int exitHybridStrategy = (isDawMode()) ? RETURN_DAW_MODE : RETURN_MIX_MODE;
                        setHybridMode(false, exitHybridStrategy);
                    }
                    else if(isSavedHybridLayout())
                    {
                        setHybridMode(true);
                    }
                }
                
                loadSavedRackWindowLayout();
                loadSavedFloatingWindowsLayout();
                functionsManager->loadSavedFunctionsLayout();
                             
                //exit custom mode.
                settingsWindow->removeFromDesktop();
                functionsManager->exitCustomMode();
                floatingEssentials->getEssentialsCustomButton()->setToggleState(false, false);
            }
        }
        else if(b->getName().compare("backgroundButton") == 0)
        {
            if(b->getToggleState() == true)
            {
                setShouldShowBackground(true);
                showBackground();
            }
            else
            {
                setShouldShowBackground(false);
                getBackgroundWindow()->removeFromDesktop();
            }
        }
        else if(b->getName().compare("counterButton") == 0)
        {
            (b->getToggleState()) ? counterWindow->addToDesktopWithOptions(RAVEN_PALETTE_LEVEL) : counterWindow->removeFromDesktop();
            counterWindow->toFront(false);
        }
        else if(b->getName().compare("drumPad") == 0)
        {
            //drumPad->setVisible(b->getToggleState());
            //drumPad->toFront(false);
        }
        else if(b->getName().compare("essentialsButton") == 0)
        {
            (b->getToggleState()) ? essentialsWindow->addToDesktopWithOptions(RAVEN_PALETTE_LEVEL) : essentialsWindow->removeFromDesktop();
            //essentialsWindow->toFront(false);
        }
        else if(b->getName().compare("floatingNavPadButton") == 0)
        {
            (b->getToggleState()) ? navpadWindow->addToDesktopWithOptions(RAVEN_PALETTE_LEVEL) : navpadWindow->removeFromDesktop();
            navpadWindow->toFront(false);
        }
        else if(b->getName().compare("insertButton") == 0)
        {
            //simulating a global radio button group here...
            if(b->getToggleState()) //if this one is selected
            {
                mixer->toggleInsertButtonsOff(); //turn off all insert buttons
                b->setToggleState(true, false); // toggle this one back on but don't send notification
                insertsWindow->addToDesktopWithOptions(RAVEN_PALETTE_LEVEL);
                insertsWindow->toFront(false);
                insertsPalette->setShouldBeVisible(true);
            }
            else
            {
                insertsWindow->removeFromDesktop();
                insertsPalette->setShouldBeVisible(false);
            }
        }
        else if(b->getName().compare("insertButtonNSC") == 0)
        {
            RavenNSCButton *nscTrackButton = dynamic_cast<RavenNSCButton*>(b);
            
            int bnk = nscTrackButton->getBankIndex();
            int chn = nscTrackButton->getChannelIndex();
            insertsPalette->setTrackName(trackNames->getTrackName(bnk, chn));
            insertsPalette->setCurrentTrackButton(nscTrackButton);
            functionsManager->setInsertWindowButton(insertsPalette->getInsertWindowButton(bnk));
            insertsPalette->updateNames();
        }
        else if(b->getName().compare("hideSends") == 0)
        {
            if(!sendsWindow->needsHiding())   sendsWindow->setNeedsHiding(true);
            else                        sendsWindow->setNeedsHiding(false);
        }
        else if(b->getName().compare("hideMixer") == 0)
        {
            if(!mixerWindow->needsHiding())   mixerWindow->setNeedsHiding(true);
            else                        mixerWindow->setNeedsHiding(false);
        }
        else if(b->getName().compare("finePanButton") == 0)
        {
            mixer->setFinePan(b->getToggleState());
            sends->setFinePan(b->getToggleState());
        }
        else if(b->getName().compare("fineFaderButton") == 0)
        {
            mixer->setFineFader(b->getToggleState());
            sends->setFineFader(b->getToggleState());
        }
        else if (b->getName().compare("moveModulesButton") == 0)
        {
            #if RAVEN_24
            if(b->getToggleState()) b->setToggleState(false, false);
            #endif
            
            bool moveMode = b->getToggleState();
            //mixer->hideButton->setVisible(moveMode);
            //if(!RAVEN_24) sends->hideButton->setVisible(moveMode);
            
            if(moveMode) //entering move modules mode
            {
                //temporarily resize functions so everything fits
                /*
                int subHeight = functions->getHeight() - functions->getSinglePanelHeight(); //height of extra function bars
                functions->setSize(functions->getWidth(), functions->getSinglePanelHeight());
                 */
                
                //Don't show sends if not enough room
//                if(!RAVEN_24)
//                {
//                    //printf("rem height = %d\n", remainingHeight);
//                    if(sendsWindow->getOrigHeight() <= remainingHeight)
//                    {
//                        if(sendsWindow->isHidden())
//                        {
//                            showModule(sendsWindow);
//                            sendsWindow->setNeedsHiding(true);
//                        }
//                    }
//                    /*// don't worry about mixer for now..because it's not allowed to hide in mix mode
//                    if(mixer->isHidden())
//                    {
//                        showModule(mixer);
//                        mixer->setNeedsHiding(true);
//                    }
//                     */
//                }
            }
            else //exiting move modules mode
            {
                // resize functions to proper size
                //functions->setSize(functions->getWidth(), functions->getOrigHeight());
                
//                if(!RAVEN_24)
//                {
//                    if(sendsWindow->needsHiding() /*|| functions->getNumMixModeBars() > 1*/) hideModule(sendsWindow);
//                    else                     showModule(sendsWindow);
//                    
//                    //NOTE: need this logic in case push the hide sends button when in move modules.
//                    //Add condition to only show sends if numToolbars > 1.
//                    
//                    
//                    if(mixerWindow->needsHiding()) hideModule(mixerWindow);
//                    else                     showModule(mixerWindow);
//                }
            }
        }
        else if(b->getName().compare("targetDAW") == 0)
        {
             if(b->getToggleState()) printf("show menu\n");
             else                    printf("hide menu\n");
        }
        #if RAVEN_24
        else if(b->getName().compare("flipModeButton") == 0)
        {
            bool doneInit = b->getProperties().getWithDefault("doneInitializing", false);
            if(!doneInit) return; //wait for intialization to finish using flip mode before we listen
            
            bool flipMode = functionsManager->getMasterFlipModeButton()->getProperties().getWithDefault("isFlipMode", false);
            if(flipMode)
            {
                printf("flip mode so hiding mixer\n");
                
                //swap mixer window with sends
                removeWindowFromRack(mixerWindow);
                insertWindowIntoRack(sendsWindow, mixerWindow->getRackPos(), true);
                
                mixer->enterFlipMode();
                sends->enterFlipMode();
                trackNames->enterFlipMode();
                functionsManager->enterFlipMode();
                
                functionsManager->getMasterFlipModeButton()->getProperties().set("isFlipMode", true);
            }
            else
            {
                printf("regular mode so hiding sends\n");
                
                //swap mixer window with sends
                removeWindowFromRack(sendsWindow);
                insertWindowIntoRack(mixerWindow, sendsWindow->getRackPos(), true);
                
                mixer->exitFlipMode();
                sends->exitFlipMode();
                trackNames->exitFlipMode();
                functionsManager->exitFlipMode();
                
                functionsManager->getMasterFlipModeButton()->getProperties().set("isFlipMode", false);

            }
        }
        #endif
        else if(b->getName().compare("exitButton") == 0)
        {
            RAVEN_TOUCH_LOCK //lock touches, will need to unlock if we give user the option to cancel the exit
            
            //if we're in flip mode, exit it before closing. For some reason this isn't working... 
            if(functionsManager->isFlipMode())//TODO: flip mode should only be stored in one place as a global.
            {
                printf("exiting flip mode before close\n");
                sends->exitFlipMode();//prevents sends faders from changing to mixer faders
                functionsManager->getMasterFlipModeButton()->setToggleState(false, true);//triggerClick();
            }
            
            String alertMessage = "Save your layouts before exiting the Raven?";
            int alertResponse = getAlertWindowComponent()->callAlertWindow(3, alertMessage, "Yes", "No", "Cancel");
            
            if(alertResponse == 1) //yes
            {
                if(!isSaveToSession()) saveLayoutHandler->saveSession();
                saveLayoutHandler->setLastView(RAVEN->getCurrentRavenVersion(), isDawMode(), PRIOR_SAVE_VIEW_INDEX);
                saveLayoutHandler->saveIcons();
                saveLayoutHandler->saveUserLayout(isDawMode(), PRIOR_SAVE_VIEW_INDEX);
            }
            else if(alertResponse == 2) //cancel
            {
                //set exit button toggle state to green.
                b->setToggleState(false, dontSendNotification);
                RAVEN_TOUCH_UNLOCK
                return;
            }
                       
//            JUCEApplication::quit();
            startTimer(huiWaitMsLong*2);//<-- here is where we quit the raven. We give it some time to prevent flip buttons from not having a chance to toggle. 
        }
        
        else if(b->getName().compare("addBarButton") == 0 || b->getName() == "essentialsAddBarButton")
        {
            std::cout << "curNumFunctionBars: " << curNumFunctionsBars << std::endl;
        
            if(RAVEN_24)
            {
                if(curNumFunctionsBars == 0)
                {
                    insertWindowIntoRack(functionsWindow1, functionsWindow1->getRackPos(), true);
                }
                else if(curNumFunctionsBars == 1)
                {
                    removeWindowFromRack(namesWindow);
                    insertWindowIntoRack(functionsWindow2, functionsWindow1, true);
                }
                else if(curNumFunctionsBars == 2)
                {
                    if(isDawMode() || isHybridMode()) insertWindowIntoRack(functionsWindow3, functionsWindow2, true);
                    else                              return;
                }
                else if(curNumFunctionsBars == 3)
                {
                    return;
                }
                
                if(!isHybridMode())
                {
                    printf("rebuilding rack\n");
                    rebuildRack();
                }
                
                curNumFunctionsBars++;
                if(curNumFunctionsBars > 3) curNumFunctionsBars = 3; 
            }
            else
            {
                if(curNumFunctionsBars == 0)
                {
                    insertWindowIntoRack(functionsWindow1, functionsWindow1->getRackPos(), true);
                }
                else if(curNumFunctionsBars == 1)
                {
                    insertWindowIntoRack(functionsWindow2, functionsWindow1, true);
                    if(!isDawMode() && !isHybridMode()) removeWindowFromRack(sendsWindow);
                }
                else if(curNumFunctionsBars == 2)
                {
                    insertWindowIntoRack(functionsWindow3, functionsWindow2, true);
                }
                else if(curNumFunctionsBars == 3)
                {
                    return;
                }
                
                if(!isHybridMode())
                {
                    printf("rebuilding rack\n");
                    rebuildRack();
                }
                
                curNumFunctionsBars++;
                if(curNumFunctionsBars > 3) curNumFunctionsBars = 3;
            }
        }
        else if(b->getName().compare("removeBarButton") == 0 || b->getName() == "essentialsRemoveBarButton")
        {
            std::cout << "curNumFunctionBars: " << curNumFunctionsBars << std::endl;
            
            if(RAVEN_24)
            {
                if(curNumFunctionsBars == 0)
                {
                    return;
                }
                else if(curNumFunctionsBars == 1)
                {
                    removeWindowFromRack(functionsWindow1);
                    essentialsWindow->addToDesktopWithOptions(RAVEN_PALETTE_LEVEL);
                    functionsManager->getEssentialsButton()->setToggleState(true, false);
                }
                else if(curNumFunctionsBars == 2)
                {
                    removeWindowFromRack(functionsWindow2);
                    if(!isDawMode() && !isHybridMode()) insertWindowIntoRack(namesWindow, functionsWindow1, true);
                }
                else if(curNumFunctionsBars == 3)
                {
                    removeWindowFromRack(functionsWindow3);
                }
                
                if(!isHybridMode())
                {
                    printf("rebuilding rack\n");
                    rebuildRack();
                }
                
                curNumFunctionsBars--;
                if(curNumFunctionsBars < 0) curNumFunctionsBars = 0;
            }
            else
            {
                if(curNumFunctionsBars == 0)
                {
                    return;
                }
                else if(curNumFunctionsBars == 1)
                {
                    removeWindowFromRack(functionsWindow1);
                    essentialsWindow->addToDesktopWithOptions(RAVEN_PALETTE_LEVEL);
                    functionsManager->getEssentialsButton()->setToggleState(true, false);
                }
                else if(curNumFunctionsBars == 2)
                {
                    removeWindowFromRack(functionsWindow2);
                    if(!isDawMode() && !isHybridMode()) insertWindowIntoRack(sendsWindow, sendsWindow->getRackPos(), true);
                }
                else if(curNumFunctionsBars == 3)
                {
                    removeWindowFromRack(functionsWindow3);
                }
                
                if(!isHybridMode())
                {
                    printf("rebuilding rack\n");
                    rebuildRack();
                }
                
                curNumFunctionsBars--;
                if(curNumFunctionsBars < 0) curNumFunctionsBars = 0;
            }
        }
    }
    
    void actionListenerCallback (const String &message)
    {
        //NOTE: might need another var - needsHiding from hide/show vs hiding from space
        
        if(message == "revertLayout")
        {
            saveLayoutHandler->loadUserLayout(isDawMode(), REVERT_LAYOUT_INDEX);
            
            if(isHybridMode() != isSavedHybridLayout())
            {
                if(isSavedHybridLayout())
                {
                    setHybridMode(true);
                }
                else
                {
                    int exitHybridStrategy = (isDawMode()) ? RETURN_DAW_MODE : RETURN_MIX_MODE;
                    setHybridMode(false, exitHybridStrategy);
                }
            }
            
            loadSavedRackWindowLayout();
            loadSavedFloatingWindowsLayout();
            functionsManager->loadSavedFunctionsLayout();
        }
        else if(message.startsWith("save"))
        {
            int presetIndex = message.substring(4, 5).getIntValue();
            saveLayoutHandler->saveUserLayout(isDawMode(), presetIndex);
        }
        else if(message.startsWith("load"))
        {
            int presetIndex = message.substring(4, 5).getIntValue();
            
            //save the current view in case a user wants to revert.
            saveLayoutHandler->saveUserLayout(isDawMode(), REVERT_LAYOUT_INDEX);
            
            //load the selected stored layout.
            saveLayoutHandler->loadUserLayout(isDawMode(), presetIndex);

            //sync hybird mode state.
            if(isHybridMode() != isSavedHybridLayout())
            {
                if(isSavedHybridLayout())
                {
                    setHybridMode(true);
                }
                else
                {
                    int exitHybridStrategy = (isDawMode()) ? RETURN_DAW_MODE : RETURN_MIX_MODE;
                    setHybridMode(false, exitHybridStrategy);
                }
            }
            
            loadSavedRackWindowLayout();
            loadSavedFloatingWindowsLayout();
            functionsManager->loadSavedFunctionsLayout();
        }
        else if(message == "set mixer automation off")
        {
            //mixer->resetAllBlankFrames();
            //sends->resetAllBlankFrames();
            //trackNames->setAllTrackNamesBlank();
        }
        else if(message == "track left")
        {
            mixer->trackLeft();
            trackNames->trackLeft();
        }
        else if(message == "track right")
        {
            mixer->trackRight();
            trackNames->trackRight();
        }
        else if(message == "toggleInsertButtonsOff")
        {
            mixer->toggleInsertButtonsOff();
        }
    }
    
    void handleMessage (const Message &message)
    {
        RavenComponentMoveMessage *m = (RavenComponentMoveMessage*)&message;
        m->moveComponent();
    }
    
    void initializeCounter(NscGuiContainer *owner)
    {
        counter->initializeCounter(owner);
    }
    
    void initializeInsertsPalette(RavenFunctionsManager *owner)
    {
        insertsPalette->initializeLCDs(owner);
    }
    
    bool rackTouched(int touchX, int touchY)
    {        
        for(int i = 0; i < windows.size(); i++)
        {
            if(windows[i]->isOnDesktop())
            {
                if(RavenTouchComponent::componentTouched(windows[i], touchX, touchY)) return true;
            }
        }
        return false;
    }
    
    bool paletteTouched(int touchX, int touchY)
    {
        for(int i = 0; i < floatingWindows.size(); i++)
        {
            if(floatingWindows[i]->isOnDesktop())
            {
                if(RavenTouchComponent::componentTouched(floatingWindows[i], touchX, touchY))
                {
                    //bring to front
                    //LOCK_JUCE_THREAD floatingWindows[i]->toFront(false);
                    // this is messing up the ordering of desktop windows/components in addTuioCursor
                    return true;
                }
            }
        }
        return false;
    }
    
    bool hybridMixerTouched(int touchX, int touchY)
    {
        //check mixer if in hybrid mode
        return (isHybridMode() && RavenTouchComponent::componentTouched(mixer, touchX, touchY));
    }
    
    bool resetPaletteLevels()
    {
        for(int i = 0; i < floatingWindows.size(); i++)
        {
            if(floatingWindows[i]->isOnDesktop())
            {
                //revert to normal palette level
                LOCK_JUCE_THREAD floatingWindows[i]->setToPaletteLevel();
                return true;
            }
        }
        return false;
    }
    
    bool checkCounterTouch(TUIO::TuioCursor *tcur)
    {
        if(RavenTouchComponent::componentTouched(counterWindow, tcur, false))
        {
//            std::cout << "counter window touched" << std::endl;
            int touchX = tcur->getScreenX(SCREEN_WIDTH);
            int touchY = tcur->getScreenY(SCREEN_HEIGHT);
            Point<int> screenLocation;
            screenLocation.setXY(touchX, touchY);
            Point<int> localPoint = counterWindow->getPeer()->globalToLocal(screenLocation);
            
            //std::cout << "initTouchX: " << localPoint.getX() << "initTouchY: " << localPoint.getY() << std::endl;
            
            counterWindow->getProperties().set("initTouchX", localPoint.getX());
            counterWindow->getProperties().set("initTouchY", localPoint.getY());
            counterWindow->getProperties().set("orgTouchID", tcur->getCursorID());
            return true;
        }
        return false;
    }
    
    bool checkEssentialsMoveTouch(TUIO::TuioCursor *tcur)
    {
        bool touchingEssentialsComponent = false;
        for(int i = 0; i < floatingEssentials->getNumChildComponents(); i++)
        {
            if(floatingEssentials->getChildComponent(i)->getName() == "MouseHider") continue;
            if(floatingEssentials->componentTouched(floatingEssentials->getChildComponent(i), tcur, false))
            {
                touchingEssentialsComponent = true;
                break;
            }
        }
        if(RavenTouchComponent::componentTouched(essentialsWindow, tcur, false) && !touchingEssentialsComponent)
        {
            int touchX = tcur->getScreenX(SCREEN_WIDTH);
            int touchY = tcur->getScreenY(SCREEN_HEIGHT);
            Point<int> screenLocation;
            screenLocation.setXY(touchX, touchY);
            Point<int> localPoint = essentialsWindow->getPeer()->globalToLocal(screenLocation);
            essentialsWindow->getProperties().set("initTouchX", localPoint.getX());
            essentialsWindow->getProperties().set("initTouchY", localPoint.getY());
            essentialsWindow->getProperties().set("orgTouchID", tcur->getCursorID());
            return true;
        }
        
        return false;
    }
    bool checkSettingsMoveTouch(TUIO::TuioCursor *tcur)
    {
        bool touchingSettingsComponent = false;
        for(int i = 0; i < settings->getNumChildComponents(); i++)
        {
            if(settings->getChildComponent(i)->getName() == "MouseHider") continue;
            if(settings->componentTouched(settings->getChildComponent(i), tcur, false))
            {
                touchingSettingsComponent = true;
                break;
            }
        }
        if(RavenTouchComponent::componentTouched(settingsWindow, tcur, false) && !touchingSettingsComponent)
        {
            int touchX = tcur->getScreenX(SCREEN_WIDTH);
            int touchY = tcur->getScreenY(SCREEN_HEIGHT);
            Point<int> screenLocation;
            screenLocation.setXY(touchX, touchY);
            Point<int> localPoint = settingsWindow->getPeer()->globalToLocal(screenLocation);
            settingsWindow->getProperties().set("initTouchX", localPoint.getX());
            settingsWindow->getProperties().set("initTouchY", localPoint.getY());
            settingsWindow->getProperties().set("orgTouchID", tcur->getCursorID());
            return true;
        }
        
        return false;
    }

    bool checkSettingsTouch(TUIO::TuioCursor *tcur)
    {
        return (settings->checkButtonTouch(tcur) || RavenTouchComponent::componentTouched(settingsWindow, tcur, false));
    }

    bool checkFloatingMixerMoveTouch(TUIO::TuioCursor *tcur)
    {
        if(RavenTouchComponent::componentTouched(mixer->getHybridModeMoveComponent(), tcur, false))
        {
            int touchX = tcur->getScreenX(SCREEN_WIDTH);
            int touchY = tcur->getScreenY(SCREEN_HEIGHT);
            Point<int> screenLocation;
            screenLocation.setXY(touchX, touchY);
            Point<int> localPoint = mixerWindow->getPeer()->globalToLocal(screenLocation);
            mixerWindow->getProperties().set("initTouchX", localPoint.getX());
            mixerWindow->getProperties().set("initTouchY", localPoint.getY());
            mixerWindow->getProperties().set("orgTouchID", tcur->getCursorID());
            return true;
        }
        return false;
    }
    
    bool checkSettingsButtonTouch(TUIO::TuioCursor *tcur)
    {
        return settings->checkButtonTouch(tcur);
    }
    
    bool checkIconPaletteButtonTouch(TUIO::TuioCursor *tcur)
    {
        return iconPalette->checkButtonTouch(tcur);
    }
    
    bool checkIconPaletteMoveTouch(TUIO::TuioCursor *tcur)
    {
        if(RavenTouchComponent::componentTouched(iconPalette, tcur, false))
        {
            int touchX = tcur->getScreenX(SCREEN_WIDTH);
            int touchY = tcur->getScreenY(SCREEN_HEIGHT);
            Point<int> screenLocation;
            screenLocation.setXY(touchX, touchY);
            Point<int> localPoint = iconPaletteWindow->getPeer()->globalToLocal(screenLocation);
            iconPaletteWindow->getProperties().set("initTouchX", localPoint.getX());
            iconPaletteWindow->getProperties().set("initTouchY", localPoint.getY());
            iconPaletteWindow->getProperties().set("orgTouchID", tcur->getCursorID());
            return true;
        }
        return false;
    }
    
    bool checkFloatingNavPadTap(int touchX, int touchY)
    {
        return floatingNavPad->checkPadTouch(touchX, touchY);
    }
    
    bool checkFloatingNavPadComponentTouch(TUIO::TuioCursor *tcur)
    {
        int touchX = tcur->getScreenX(SCREEN_WIDTH);
        int touchY = tcur->getScreenY(SCREEN_HEIGHT);
        return RavenTouchComponent::componentTouched(navpadWindow, touchX, touchY);
    }
    
    bool checkFloatingNavPadTouch(int touchX, int touchY)
    {
        return (navpadWindow->isOnDesktop() && (floatingNavPad->checkButtonTouch(touchX, touchY) || floatingNavPad->checkPadTouch(touchX, touchY)));
    }
    
    bool checkFloatingNavPadTouch(TUIO::TuioCursor *tcur)
    {
        int touchX = tcur->getScreenX(SCREEN_WIDTH);
        int touchY = tcur->getScreenY(SCREEN_HEIGHT);
        return (navpadWindow->isOnDesktop() && (floatingNavPad->checkButtonTouch(tcur) || floatingNavPad->checkPadTouch(touchX, touchY)));
    }
 
    bool checkFloatingNavPadMoveTouch(TUIO::TuioCursor *tcur)
    {
        int touchX = tcur->getScreenX(SCREEN_WIDTH);
        int touchY = tcur->getScreenY(SCREEN_HEIGHT);

        if(RavenTouchComponent::componentTouched(navpadWindow, tcur, false) && !floatingNavPad->checkPadTouch(touchX, touchY)
           && !floatingNavPad->checkButtonTouch(tcur))
        {
            Point<int> screenLocation;
            screenLocation.setXY(touchX, touchY);
            Point<int> localPoint = navpadWindow->getPeer()->globalToLocal(screenLocation);
            navpadWindow->getProperties().set("initTouchX", localPoint.getX());
            navpadWindow->getProperties().set("initTouchY", localPoint.getY());
            navpadWindow->getProperties().set("orgTouchID", tcur->getCursorID());
            return true;
        }

        return false;
    }
    
    bool checkInsertsPaletteMoveTouch(TUIO::TuioCursor *tcur)
    {
        if(insertsPalette->componentTouched(insertsPalette->getTrackLabel(), tcur, false))
        {
            insertsWindow->getProperties().set("orgTouchID", tcur->getCursorID());
            return true;
        }
        return false;
    }
    
    bool checkInsertsPaletteTouch(TUIO::TuioCursor *tcur)
    {
        if(insertsWindow->isOnDesktop())
        {
            return (insertsPalette->checkButtonTouch(tcur) || RavenTouchComponent::componentTouched(insertsWindow, tcur, false));
        }
        else return false;
    }
    
    bool checkHotKeyPanelTouch(TUIO::TuioCursor *tcur)
    {
        //printf("Check hot key button touch!\n");        
        if(hotKeyPanel->checkButtonTouch(tcur)) return true;
        if(RavenTouchComponent::componentTouched(hotkeyWindow, tcur)) return true;
        
        if(functionsManager->isHotKeyProgramminMode())
        {
            for(int i = 0; i < functionsManager->getHotKeyComponents().size(); i++)
            {
                if(functionsManager->getHotKeyComponents()[i]->isProgramMode())
                    functionsManager->getHotKeyComponents()[i]->checkButtonTouch(tcur);
            }
            return true;
        }
        return false;
    }
    
    
    
    void moveCounter(int screenX, int screenY)
    {
        //downsample movement for faster response
        static int counterMoveCount = 0;
        if(counterMoveCount % FLOATING_DOWNSAMPLE_AMT == 0)
        {
            int initTouchX = counterWindow->getProperties().getWithDefault("initTouchX", 0);
            int initTouchY = counterWindow->getProperties().getWithDefault("initTouchY", 0);
            int xPos = screenX - initTouchX; //counter->getWidth()/2;
            int yPos = screenY - initTouchY; //counter->getHeight()/2;
            RavenComponentMoveMessage *m = new RavenComponentMoveMessage(counterWindow, xPos, yPos);
            postMessage(m);
        }
        
        counterMoveCount++;
        if(counterMoveCount >= 50000) counterMoveCount = 0;
    }
    
    void moveEssentialsPalette(int screenX, int screenY)
    {
        //downsample movement for faster response
        static int essentialsMoveCount = 0;
        
        if(essentialsMoveCount % FLOATING_DOWNSAMPLE_AMT == 0)
        {
            int initTouchX = essentialsWindow->getProperties().getWithDefault("initTouchX", 0);
            int initTouchY = essentialsWindow->getProperties().getWithDefault("initTouchY", 0);
            int xPos = screenX - initTouchX; //counter->getWidth()/2;
            int yPos = screenY - initTouchY; //counter->getHeight()/2;
            RavenComponentMoveMessage *m = new RavenComponentMoveMessage(essentialsWindow, xPos, yPos);
            postMessage(m);
        }
        essentialsMoveCount++;
        if(essentialsMoveCount >= 50000) essentialsMoveCount = 0;
    }
    
    void moveSettingsPalette(int screenX, int screenY)
    {
        //downsample movement for faster response
        static int settingsMoveCount = 0;
        if(settingsMoveCount % FLOATING_DOWNSAMPLE_AMT == 0)
        {
            int initTouchX = settingsWindow->getProperties().getWithDefault("initTouchX", 0);
            int initTouchY = settingsWindow->getProperties().getWithDefault("initTouchY", 0);
            int xPos = screenX - initTouchX; //counter->getWidth()/2;
            int yPos = screenY - initTouchY; //counter->getHeight()/2;
            RavenComponentMoveMessage *m = new RavenComponentMoveMessage(settingsWindow, xPos, yPos);
            postMessage(m);
        }
        settingsMoveCount++;
        if(settingsMoveCount >= 50000) settingsMoveCount = 0;
    }
    void moveInsertsPalette(int screenX, int screenY)
    {
        //downsample movement for faster response
        static int insertsPaletteMoveCount = 0;
        if(insertsPaletteMoveCount % FLOATING_DOWNSAMPLE_AMT == 0)
        {
            int xPos = screenX - 147;
            int yPos = screenY - 15;
            RavenComponentMoveMessage *m = new RavenComponentMoveMessage(insertsWindow, xPos, yPos);
            postMessage(m);
        }
        
        insertsPaletteMoveCount++;
        if(insertsPaletteMoveCount >= 50000) insertsPaletteMoveCount = 0;
    }
    
    void moveFloatingNavPad(int screenX, int screenY)
    {
        //downsample movement for faster response
        static int floatingNavPadMoveCount = 0;
        if(floatingNavPadMoveCount % FLOATING_DOWNSAMPLE_AMT == 0)
        {
            int initTouchX = navpadWindow->getProperties().getWithDefault("initTouchX", 0);
            int initTouchY = navpadWindow->getProperties().getWithDefault("initTouchY", 0);
            int xPos = screenX - initTouchX;
            int yPos = screenY - initTouchY;
            
            RavenComponentMoveMessage *m = new RavenComponentMoveMessage(navpadWindow, xPos, yPos);
            postMessage(m);
        }
        
        floatingNavPadMoveCount++;
        if(floatingNavPadMoveCount >= 50000) floatingNavPadMoveCount = 0;
    }
    
    void moveFloatingMixer(int screenX, int screenY)
    {
        //downsample movement for faster response
        static int mixerMoveCount = 0;
        if(mixerMoveCount % FLOATING_DOWNSAMPLE_AMT == 0)
        {
            if(!isHybridMode()) return;
            int initTouchX = mixerWindow->getProperties().getWithDefault("initTouchX", 0);
            int initTouchY = mixerWindow->getProperties().getWithDefault("initTouchY", 0);
            int xPos = screenX - initTouchX;
            int yPos = screenY - initTouchY;
            //if maximum number of banks, only allow vertical movement: 
            if(mixer->getNumberOfHybridModeBanks() == numBanks){
                //xPos = screenWidth-mixerWindow->getWidth()+RAVEN_HYBRID_MODE_SIDE_BAR_WIDTH;
                xPos = -RAVEN_HYBRID_MODE_SIDE_BAR_WIDTH;
            }
            
            mixer->setHybridPositionParameters(xPos, yPos, mixer->getNumberOfHybridModeBanks());
            
            RavenComponentMoveMessage *m = new RavenComponentMoveMessage(mixerWindow, xPos, yPos);
            postMessage(m);
        }
        mixerMoveCount++;
        if(mixerMoveCount >= 50000) mixerMoveCount = 0;
    }

    void moveIconPalette(int screenX, int screenY)
    {
        //downsample movement for faster response
        static int iconPaletteMoveCount = 0;
        if(iconPaletteMoveCount % FLOATING_DOWNSAMPLE_AMT == 0)
        {
            int initTouchX = iconPaletteWindow->getProperties().getWithDefault("initTouchX", 0);
            int initTouchY = iconPaletteWindow->getProperties().getWithDefault("initTouchY", 0);
            int xPos = screenX - initTouchX;
            int yPos = screenY - initTouchY;
            RavenComponentMoveMessage *m = new RavenComponentMoveMessage(iconPaletteWindow, xPos, yPos);
            postMessage(m);
        }
        iconPaletteMoveCount++;
        if(iconPaletteMoveCount >= 50000) iconPaletteMoveCount = 0;
    }
    
    void setNscLink	(class NscLink * link_)
    {
        sends->setNscLink(link_);
        mixer->setNscLink(link_);
        trackNames->setNscLink(link_);
        functionsManager->setLink(link_); //sets functions Nsc link for GuiContainer and sub components (nav pad, for example)
        functions1->setNscLink(link_);
        functions2->setNscLink(link_);
        functions3->setNscLink(link_);
    }
    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, float val)
    {        
        sends->nscCallback(type, subType, bank, which, val);
        mixer->nscCallback(type, subType, bank, which, val);
        trackNames->nscCallback(type, subType, bank, which, val);
        functionsManager->callback(type, subType, bank, which, val);
//        largeMeters->nscCallback(type, subType, bank, which, val);
    }
    void nscCallback(ENSCEventType type, int subType, int bank, int which, const std::string & data)
    {
        sends->nscCallback(type, subType, bank, which, data.c_str());
        mixer->nscCallback(type, subType, bank, which, data.c_str());
        trackNames->nscCallback(type, subType, bank, which, data.c_str());
        functionsManager->callback(type, subType, bank, which, data.c_str());
        //update insert palette names
        if(type == eNSCPlugInLCD && bank == 0 && which == 40)
        {
            //printf("insert name update: %s \n", data.c_str());
            insertsPalette->updateNames();
        }
    }
    
    void finishedInitialization()
    {
        rebuildRack();
        showBackground();
        setShouldShowBackground(true);
    }
    
    void showBackground()
    {
        backgroundWindow->addToDesktopWithOptions(RAVEN_RACK_LEVEL-1);
    }
    
    bool shouldShowBackground() { return _shouldShowBackground;}
    
    void setShouldShowBackground(bool show){ _shouldShowBackground = show;}
    
    //QUIT THE RAVEN: 
    void timerCallback()
    {
        stopTimer();
        JUCEApplication::quit();
    }
    
    int getAutomationCheckBlockCounter(){  return automationCheckBlockCounter;}
    void setAutomationCheckBlockCounter(int temp){  automationCheckBlockCounter = temp;}
    void setSavedHybridModeLayout(bool hybridLayout){savedHybridLayout = hybridLayout;}
    
    void setTouchLock(bool touchState){touchLock = touchState;}
    bool getTouchLockState(){return touchLock;}
    String getCurrentRavenVersion() {return currentRavenVersion;}
    
    RavenFunctionsManager* getFunctionsManager() {return functionsManager;}
    RavenMixerComponent* getMixer(){return mixer;}
    RavenSendsComponent* getSends(){return sends;}
    RavenTrackNameComponent* getTrackNames(){return trackNames;}
    RavenSettingsComponent* getSettings(){return settings;}
    RavenIconPaletteComponent* getIconPalette(){return iconPalette;}
    RavenFloatingEssentialsComponent* getEssentialsPalette(){return floatingEssentials;}
    RavenWindow* getBackgroundWindow() {return backgroundWindow;}
    RavenAlertComponent* getAlertWindowComponent(){return alertComponent;}
#if RAVEN_ADD_TRANSPARENT_MIXER
    RavenTransparentMixerComponent* getTransparentMixer(){ return transparentMixer;}
#endif
    
    ScopedPointer<RavenWindow> counterWindow;
    ScopedPointer<RavenWindow> navpadWindow;
    ScopedPointer<RavenWindow> essentialsWindow;
    ScopedPointer<RavenWindow> insertsWindow;
    ScopedPointer<RavenWindow> hotkeyWindow;
    ScopedPointer<RavenWindow> settingsWindow;
    ScopedPointer<RavenWindow> iconPaletteWindow;
    
private:
    
    //Constructor is private so that it can  not be called
    RavenRackContainer() : saveToSession(false), hybridMode(false), hybridPanMode(false), savedHybridLayout(false), dawMode(false), touchLock(false), _shouldShowBackground(false), automationCheckBlockCounter(0){}
    RavenRackContainer(RavenRackContainer const&){}             // copy constructor is private
    void operator=(RavenRackContainer const&){}  // assignment operator is private, technically should return RavenRackContainer&, but void removes compiler warning
    static RavenRackContainer* instance;
    
    bool saveToSession;
    bool hybridMode, hybridPanMode, savedHybridLayout;
    bool dawMode;
    bool touchLock;
    bool _shouldShowBackground;
    String currentRavenVersion;
    
    int automationCheckBlockCounter;
    
    ScopedPointer<RavenWindow> sendsWindow;
    ScopedPointer<RavenWindow> mixerWindow;
    ScopedPointer<RavenWindow> namesWindow;
    ScopedPointer<RavenWindow> functionsWindow1;
    ScopedPointer<RavenWindow> functionsWindow2;
    ScopedPointer<RavenWindow> functionsWindow3;
    ScopedPointer<RavenWindow> backgroundWindow;
    ScopedPointer<RavenWindow> alertWindow;
    
    ScopedPointer<RavenFunctionsManager> functionsManager;
    
    ScopedPointer<RavenSendsComponent> sends;
    ScopedPointer<RavenMixerComponent> mixer;
    ScopedPointer<RavenTrackNameComponent> trackNames;
    ScopedPointer<RavenFunctionsComponent> functions1;
    ScopedPointer<RavenFunctionsComponent> functions2;
    ScopedPointer<RavenFunctionsComponent> functions3;
    ScopedPointer<RavenCounterComponent> counter;
    ScopedPointer<RavenInsertsPalette> insertsPalette;
    ScopedPointer<RavenHotKeyProgrammingComponent> hotKeyPanel;
    ScopedPointer<RavenFloatingNavPadComponent> floatingNavPad;
    ScopedPointer<RavenFloatingEssentialsComponent> floatingEssentials;
    ScopedPointer<RavenSettingsComponent> settings;
    ScopedPointer<RavenIconPaletteComponent> iconPalette;
    ScopedPointer<RavenBackgroundImageComponent> ravenBackground;
    ScopedPointer<RavenAlertComponent> alertComponent;
    
#if RAVEN_ADD_TRANSPARENT_MIXER
    ScopedPointer<RavenTransparentMixerComponent> transparentMixer;
    ScopedPointer<RavenWindow> transparentMixerWindow;
#endif
    
    ScopedPointer<RavenSaveLayoutHandler> saveLayoutHandler;
    
    //JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenRackContainer)

};

#endif
