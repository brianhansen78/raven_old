////
////  RavenLargeMetersComponent.h
////  Raven
////
////  Created by Joshua Dickinson on 2/6/13.
////
////
//
//#ifndef __Raven__RavenLargeMetersComponent__
//#define __Raven__RavenLargeMetersComponent__
//
//#include "../JuceLibraryCode/JuceHeader.h"
//#include "RavenTextDisplay.h"
//#include "RavenTouchComponent.h"
//#include "RavenMeter.h"
//#include "RavenAutomationComponent.h"
//#include "RavenRackComponent.h"
//
//class RavenLargeMetersChannelComponent : public RavenTouchComponent, public NscGuiElement
//{
//public:
//    RavenLargeMetersChannelComponent(NscGuiContainer *owner, int bank, int chan): NscGuiElement(owner)
//    {
//        meterL = new RavenMeter(owner, bank, chan, false, true);
//        addAndMakeVisible(meterL);
//        meterL->setTag(eNSCTrackMeter1);
//        
//        meterL->setChannelIndex(chan);
//        meterL->setBankIndex(bank);
//        meterL->setClipPeakChannelBankIndex(chan, bank);
//        
//        meterR = new RavenMeter(owner, bank, chan, true, true);
//        addAndMakeVisible(meterR);
//        meterR->setTag(eNSCTrackMeter2);
//        
//        meterR->setChannelIndex(chan);
//        meterR->setBankIndex(bank);
//        meterR->setClipPeakChannelBankIndex(chan, bank);
//        
//        meterL->setTopLeftPosition(0, 0);
//        meterR->setTopLeftPosition(meterL->getRight(), 0);
//        
//        setSize(60, 294);
//    }
//    
//    void paint(Graphics &g)
//    {
//        
//    }
//    
//private:
//    ScopedPointer<RavenMeter> meterL;
//    ScopedPointer<RavenMeter> meterR;
//};
//
//class RavenLargeMetersBankComponent : public Component, public NscGuiContainer
//{
//public:
//    RavenLargeMetersBankComponent(int numChans, int bankNumber) : NscGuiContainer(bankNumber)
//    {
//        setSize (0, 0);
//        int xOffset = 0;
//        for(int i = 0; i < numChans; i++)
//        {
//            RavenLargeMetersChannelComponent *channel = new RavenLargeMetersChannelComponent(this, bankNumber, i);
//            channel->setTopLeftPosition(xOffset, 0);
//            addAndMakeVisible(channel);
//            channels.add(channel);
//            xOffset += channel->getWidth();
//            setSize(xOffset, channel->getHeight());
//        }
//    }
//    
//private:
//    OwnedArray<RavenLargeMetersChannelComponent> channels;
//};
//
//class RavenLargeMetersComponent : public RavenRackComponent
//{
//public:
//    RavenLargeMetersComponent(int numBanks, int chansPerBank, int rackPos) : RavenRackComponent(rackPos)
//    {
//        setSize (0, 0);
//        int xOffset = 0;
//        for(int i = 0; i < numBanks; i++)
//        {
//            RavenLargeMetersBankComponent *bank = new RavenLargeMetersBankComponent(chansPerBank, i);
//            setSize(getWidth() + bank->getWidth(), bank->getHeight());
//            addAndMakeVisible(bank);
//            bank->setTopLeftPosition(xOffset, 0);
//            xOffset += bank->getWidth();
//            banks.add(bank);
//        }
//    }
//    
//    void nscCallback(ENSCEventType type, int subType, int bank, int which, float val)
//    {
//        for (int i=0; i<banks.size();++i)
//        {
//            if (bank == eMatchesAllBanks || bank == i)
//                banks[i]->callback(type, subType, bank, which, val);
//        }
//    }
//    
//private:
//    OwnedArray<RavenLargeMetersBankComponent> banks;
//};
//
//#endif /* defined(__Raven__RavenLargeMetersComponent__) */
