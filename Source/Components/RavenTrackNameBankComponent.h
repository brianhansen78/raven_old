

#ifndef Raven_NewSDK_RavenTrackNameBankComponent_h
#define Raven_NewSDK_RavenTrackNameBankComponent_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenTrackNameChannelComponent.h"
#include "NscGuiElement.h"

class RavenTrackNameBankComponent : public Component, public NscGuiContainer
{
public:
    RavenTrackNameBankComponent(int numChans, int bankNum, HashMap<String, Image> *_iconMap) : NscGuiContainer(bankNum)
    {
        setSize (0, 0);
        int xOffset = 0;
        
        for(int i = 0; i < numChans; i++)
        {
            RavenTrackNameChannelComponent *trackNameChannel = new RavenTrackNameChannelComponent(this, bankNum, i, _iconMap);
            addAndMakeVisible(trackNameChannel);
            trackNameChannel->setTopLeftPosition(xOffset, 0);
            trackNameChannel->setChannelAndBankIndex(i, bankNum);
            trackNames.add(trackNameChannel);
            
            xOffset += trackNameChannel->getWidth();
            setSize(xOffset, trackNameChannel->getHeight());
        }
    }
    
    void enterFlipMode()
    {
        for(int i = 0; i < trackNames.size(); i++) trackNames[i]->enterFlipMode();
    }
    void exitFlipMode()
    {
        for(int i = 0; i < trackNames.size(); i++) trackNames[i]->exitFlipMode();
    }
    
    RavenTrackNameChannelComponent *getChannel(int c) { return trackNames[c];}
    
    String getTrackName(int chan) { return trackNames[chan]->getTrackName();}
    
    void setAllTrackNamesBlank()
    {
        for(int i = 0;i<trackNames.size();i++)
            trackNames[i]->setTrackNameBlank();
    }
    
    void setAllIcons()
    {
        for(int i = 0;i<trackNames.size();i++)
            trackNames[i]->setIcon();
    }
    
private:
    OwnedArray<RavenTrackNameChannelComponent> trackNames;
};


#endif
