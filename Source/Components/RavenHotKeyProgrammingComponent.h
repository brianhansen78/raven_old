

#ifndef Raven_RavenHotKeyProgrammingComponent_h
#define Raven_RavenHotKeyProgrammingComponent_h

#include "RavenWindowComponent.h"

class RavenHotKeyProgrammingComponent : public RavenWindowComponent,  public Button::Listener, public ActionBroadcaster
{
public:
    
    RavenHotKeyProgrammingComponent()
    {
        
        File imgFile(String(GUI_PATH) + "HotKeyPopUp/Popup-Customkey-Panel.png");
        popupImg = ImageCache::getFromFile(imgFile);
        setSize(popupImg.getWidth(), popupImg.getHeight());
        
        File imgFile1(String(GUI_PATH) + "HotKeyPopUp/Popup-Customkey-DispNoFocus_03.png");
        commandBGNoFocus = ImageCache::getFromFile(imgFile1);
        
        File imgFile2(String(GUI_PATH) + "HotKeyPopUp/Popup-Customkey-DispFocus_03.png");
        commandBGFocus = ImageCache::getFromFile(imgFile2);
        
        File imgFile3(String(GUI_PATH) + "HotKeyPopUp/Popup-Customkey-DispNoFocus_06.png");
        nameBGNoFocus = ImageCache::getFromFile(imgFile3);
        
        File imgFile4(String(GUI_PATH) + "HotKeyPopUp/Popup-Customkey-DispFocus_06.png");
        nameBGFocus = ImageCache::getFromFile(imgFile4);
        
        File imgFile5(String(GUI_PATH) + "HotKeyPopUp/Popup-Customkey-DispDisabled_06.png");
        nameBGDisabled = ImageCache::getFromFile(imgFile5);
        
        nameButton = new RavenButton();
        nameButton->setClickingTogglesState(true);
        nameButton->setName("customNameToggle");
        nameButton->setImagePaths("HotKeyPopUp/Popup-Customkey-btOn_08.png", "HotKeyPopUp/Popup-Customkey-btOff_08.png");
        nameButton->setTopLeftPosition(25+nameBGDisabled.getWidth(), 40);
        nameButton->addListener(this);
        ravenTouchButtons.add(nameButton);
        addAndMakeVisible(nameButton);
                
        commandLabel = new Label();
        commandLabel->setText("enter key command...", false);
        commandLabel->setWantsKeyboardFocus(false);
        commandLabel->setBounds(20, 5, commandBGFocus.getWidth(), commandBGFocus.getHeight());
        commandLabel->setColour(Label::textColourId, Colours::white);
        addAndMakeVisible(commandLabel);
        
        nameLabel = new Label();
        nameLabel->setText("name...", false);
        nameLabel->setWantsKeyboardFocus(false);
        nameLabel->setBounds(20, 40, nameBGFocus.getWidth(), nameBGFocus.getHeight());
        nameLabel->setColour(Label::textColourId, Colours::white.withAlpha(0.2f));
        addAndMakeVisible(nameLabel);
        
        buttonLabel = new Label();
        buttonLabel->setText("default name", false);
        buttonLabel->setWantsKeyboardFocus(false);
        buttonLabel->setBounds(nameButton->getX(), nameButton->getY(), nameButton->getWidth(), nameButton->getHeight());
        buttonLabel->setColour(Label::textColourId, Colours::white);
        addAndMakeVisible(buttonLabel);
    
        nameImg = &nameBGDisabled;
        commandImg = & commandBGFocus;
        
        setWantsKeyboardFocus(false);
        
        mouseHider = new MouseHideComponent(getWidth(), getHeight());
        addAndMakeVisible(mouseHider);
        
    }
    
    void buttonStateChanged (Button *b)
    {
        
    }
    void buttonClicked (Button *b)
    {
        if(b == nameButton)
        {
            if(b->getToggleState())
            {
                buttonLabel->setText("custom name", false);
                nameImg = &nameBGFocus;
                nameLabel->setColour(Label::textColourId, Colours::white.withAlpha(1.0f));
                commandImg = &commandBGNoFocus;
                repaint();
                sendActionMessage("usecustomname");
            }
            else
            {
                buttonLabel->setText("default name", false);
                nameImg = &nameBGDisabled;
                commandImg = &commandBGFocus;
                nameLabel->setColour(Label::textColourId, Colours::white.withAlpha(0.2f));
                repaint();
                sendActionMessage("usedefaultname");
            }
        }
    }

    void paint(Graphics &g)
    {
        g.drawImageAt(popupImg, 0, 0);
        g.drawImageAt(*commandImg, 20, 5);
        g.drawImageAt(*nameImg, 20, 40);
    }
    
    void setCommandText(const String &text){ commandLabel->setText(text, false);}
    void setNameText(const String &text){ nameLabel->setText(text, false);}
    
    bool useCustomName() { return nameButton->getToggleState(); }
    
    void resetCustomName(){ if(nameButton->getToggleState()) nameButton->triggerClick(); }
    
    //RavenButton *getNameButton() { return nameButton; }
    
    void saveProgram()
    {
        sendActionMessage("savecurrent");
    }
    
private:
    
    Image popupImg;
    Image commandBGNoFocus, commandBGFocus, nameBGFocus, nameBGNoFocus, nameBGDisabled;
    Image *nameImg;
    Image *commandImg;
    
    RavenButton *nameButton;
    
    ScopedPointer<Label> commandLabel;
    ScopedPointer<Label> nameLabel;
    ScopedPointer<Label> buttonLabel;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenHotKeyProgrammingComponent)
    
};


#endif
