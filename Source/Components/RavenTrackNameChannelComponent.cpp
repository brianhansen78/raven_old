//
//  RavenTrackNameChannelComponent.cpp
//  Raven
//
//  Created by Joshua Dickinson on 5/15/13.
//
//

#include "RavenTrackNameChannelComponent.h"
#include "RavenRackContainer.h"

void RavenTrackNameChannelComponent::buttonClicked(Button* b)
{
    if(b == iconModeButton)
    {
        if(RAVEN->getIconPalette()->getWindow()->isOnDesktop())
        {
            if(RAVEN->getIconPalette()->hasActiveIcon() && trackName->getText() != "    " && trackName->getText() != "")
            {
                iconMap->set(trackName->getText(), RAVEN->getIconPalette()->getActiveIconImage());
                setIcon();
            }
        }
        else
        {
            int iconPaletteHeight = RAVEN->getIconPalette()->getWindow()->getHeight();
            int iconPaletteWidth = RAVEN->getIconPalette()->getWindow()->getWidth();
            int tempX = (getScreenX()+(getWidth()*0.5))-(0.5*iconPaletteWidth);
            int tempY = getScreenY() - iconPaletteHeight;
            
            if(tempX < 0){  tempX = 0;}
            if(tempX+iconPaletteWidth > screenWidth)
            {
                tempX = screenWidth - iconPaletteWidth;
            }
            if(tempY-iconPaletteHeight < 0)
            {
                tempY = getScreenY()+getHeight();
            }
            
            RAVEN->getIconPalette()->getWindow()->setTopLeftPosition(tempX, tempY);
            RAVEN->getIconPalette()->getWindow()->addToDesktopWithOptions(RAVEN_PALETTE_LEVEL);
        }
    }
}