

#ifndef Raven_TransportComponent_Header_h
#define Raven_TransportComponent_Header_h

#include "RavenButton.h"
#include "RavenFunctionsComponent.h"
#include "RavenTouchComponent.h"

class RavenTransportComponent : public RavenTouchComponent, public Button::Listener
{
public:
    RavenTransportComponent(RavenFunctionsContainer *owner)
    {
        int yPos = 0;
        int xPos = 0;
        
        playButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_3(playButton);
        playButton->setTopLeftPosition(xPos, yPos);
        playButton->addListener(owner); // or add listener from nscContainer?
        playButton->addListener(this);
        playButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Transport/images/transport-on_01.png", "TOOLBAR/FUNCTIONMODULE/Transport/images/transport-off_01.png");
        playButton->setTag(eNSCGSTransportPlay);
        ravenNSCTouchButtons.add(playButton);
        addAndMakeVisible(playButton);
        playButton->setClickingTogglesState(true);
        xPos += playButton->getWidth();
        
        stopButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_3(stopButton);
        stopButton->setTopLeftPosition(xPos, yPos);
        stopButton->addListener(owner); // or add listener from nscContainer?
        stopButton->addListener(this);
        stopButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Transport/images/transport-on_02.png", "TOOLBAR/FUNCTIONMODULE/Transport/images/transport-off_02.png");
        stopButton->setTag(eNSCGSTransportStop);
        ravenNSCTouchButtons.add(stopButton);
        addAndMakeVisible(stopButton);
        stopButton->setClickingTogglesState(true); //MAKE TOGGLE BUTTON
        xPos += stopButton->getWidth();
        stopButton->setName("stopButton");
        ravenHoldButtons.add(stopButton);
        
        recButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_3(recButton);
        recButton->setTopLeftPosition(xPos, yPos);
        recButton->addListener(owner); // or add listener from nscContainer?
        recButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Transport/images/transport-on_03.png", "TOOLBAR/FUNCTIONMODULE/Transport/images/transport-off_03.png");
        recButton->setTag(eNSCGSTransportRecord);
        ravenNSCTouchButtons.add(recButton);
        addAndMakeVisible(recButton);
        recButton->setClickingTogglesState(true); //MAKE TOGGLE BUTTON
        xPos += recButton->getWidth();
        
        RavenNSCButton *rewButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_3(rewButton);
        rewButton->setTopLeftPosition(xPos, yPos);
        rewButton->addListener(owner); // or add listener from nscContainer?
        rewButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Transport/images/transport-on_04.png", "TOOLBAR/FUNCTIONMODULE/Transport/images/transport-off_04.png");
        rewButton->setTag(eNSCGSTransportRewind);
        ravenNSCTouchButtons.add(rewButton);
        addAndMakeVisible(rewButton);
        rewButton->setClickingTogglesState(true); //MAKE TOGGLE BUTTON
        xPos += rewButton->getWidth();
        
        RavenNSCButton *ffButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_3(ffButton);
        ffButton->setTopLeftPosition(xPos, yPos);
        ffButton->addListener(owner); // or add listener from nscContainer?
        ffButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Transport/images/transport-on_05.png", "TOOLBAR/FUNCTIONMODULE/Transport/images/transport-off_05.png");
        ffButton->setTag(eNSCGSTransportFastFwd);
        ravenNSCTouchButtons.add(ffButton);
        addAndMakeVisible(ffButton);
        ffButton->setClickingTogglesState(true); //MAKE TOGGLE BUTTON
        xPos += rewButton->getWidth();
        
        int width = xPos;
        int height = playButton->getHeight();
        setSize(width, height);
    }
    
    void buttonStateChanged (Button* b)
    {
        if(b == playButton)
        {
            if(b->getToggleState())
            {
                stopButton->enabled = true;
                if(punchButton)
                {
                    punchButton->enabled = false;
                    if(!punchButton->getToggleState())
                        recButton->enabled = false;
                }
            }
            else
            {
                recButton->enabled = true;
                if(punchButton)
                    punchButton->enabled = true;
            }
        }
        else if(b == stopButton)
        {
            if(b->getToggleState())
            {
                stopButton->enabled = false;
            }
            else
            {
                stopButton->enabled = true;
            }
        }
    }
    
    void buttonClicked (Button* b)    
    {
        
    }
    
    void setPunchButton(RavenNSCButton* _punchButton)
    {
        punchButton = _punchButton;
    }
    
private:
    RavenNSCButton *stopButton;
    RavenNSCButton *recButton;
    RavenNSCButton *playButton;
    RavenNSCButton *punchButton;//needs a pointer to the punch button in order to enable/dissable it when transport is playing
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenTransportComponent)
    
};

#endif
