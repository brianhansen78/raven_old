//
//  RavenSettingsComponent.h
//  Raven
//
//  Created by Brian Hansen on 6/23/13.
//
//

#ifndef Raven_AlertComponent_h
#define Raven_AlertComponent_h

#include "RavenConfig.h"
#include "RavenButton.h"
#include "RavenTextDisplay.h"
#include "RavenWindowComponent.h"

class RavenAlertComponent : public RavenWindowComponent, public Button::Listener
{
public:
    RavenAlertComponent() : buttonResponseState(false)
    {
        panelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLoatingEssentials/floating_essential_base.png"));
        Rectangle<int> clippedPaletteImage = Rectangle<int>::Rectangle(22,22,974,552);
        panelImg = panelImg.getClippedImage(clippedPaletteImage);
        panelImg = panelImg.rescaled(500, 200, Graphics::highResamplingQuality);
        
        setSize(panelImg.getWidth(), panelImg.getHeight());
        
        //see textBox if label doesn't work.
        message = new Label("messageLabel", "Here is your alert window!!!!");
        message->setName("message");
        message->setSize(400, 200);
        Font curFont(22);
        message->setFont(curFont);
        message->setJustificationType(Justification::centred);
        //message->setMinimumHorizontalScale(0.1);
        addChildComponent(message);
        message->setCentreRelative(0.5f, 0.25f);
        message->setVisible(true);
        
        yesButton = new TextButton("Yes");
        yesButton->setName("yes");
        yesButton->setSize(100, 30);
        yesButton->addListener(this);
        addChildComponent(yesButton);
        yesButton->setCentreRelative(0.25f, 0.65f);
        yesButton->setVisible(true);
        
        noButton = new TextButton("No");
        noButton->setName("no");
        noButton->setSize(100, 30);
        noButton->addListener(this);
        addChildComponent(noButton);
        noButton->setCentreRelative(0.5f, 0.65f);
        noButton->setVisible(true);
        
        cancelButton = new TextButton("Cancel");
        cancelButton->setName("cancel");
        cancelButton->setSize(100, 30);
        cancelButton->addListener(this);
        addChildComponent(cancelButton);
        cancelButton->setCentreRelative(0.75f, 0.65f);
        cancelButton->setVisible(true);
    }
    
    TextButton* getYesButton(){return yesButton;}
    TextButton* getNoButton(){return noButton;}
    TextButton* getCancelButton(){return cancelButton;}
    
    int callAlertWindow(int numButtons, String message = "DEFAULT MESSAGE", String text1 = "yes", String text2 = "no", String text3 = "cancel")
    {
        showNumButtons(numButtons);
        setMessageText(message);
        setButtonText(text1, text2, text3);
        getWindow()->addToDesktopWithOptions(RAVEN_ALERT_LEVEL);
        getWindow()->runModalLoop();
        return getResponseState();
    }
    
    void showNumButtons(int numButtons)
    {
        if(numButtons == 1)
        {
            yesButton->setVisible(true);
            noButton->setVisible(false);
            cancelButton->setVisible(false);
        }
        else if(numButtons == 2)
        {
            yesButton->setVisible(true);
            noButton->setVisible(true);
            cancelButton->setVisible(false);
        }
        else
        {
            yesButton->setVisible(true);
            noButton->setVisible(true);
            cancelButton->setVisible(true);
        }
        repositionButtons();
    }
    
    void setButtonText(String text1 = "Yes", String text2 = "No", String text3 = "Cancel")
    {
        yesButton->setButtonText(text1);
        noButton->setButtonText(text2);
        cancelButton->setButtonText(text3);
    }
    
    void setMessageText(String text = "Default Message")
    {
       message->setText(text, 0);
    }
    
    void repositionButtons()
    {
        int numVisibleButtons = -1; //NOTE: initialize as -1 to account for message component that is always visible.
        for(int i = 0; i < getNumChildComponents(); i++)
        {
            if(getChildComponent(i)->isVisible()) numVisibleButtons++;
        }
        
        if(numVisibleButtons == 1)
        {
            yesButton->setCentreRelative(0.5, 0.65f);
        }
        else if(numVisibleButtons == 2)
        {
            yesButton->setCentreRelative(0.333, 0.65f);
            noButton->setCentreRelative(0.666, 0.65f);
        }
        else if(numVisibleButtons == 3)
        {
            yesButton->setCentreRelative(0.25, 0.65f);
            noButton->setCentreRelative(0.5, 0.65f);
            cancelButton->setCentreRelative(0.75, 0.65f);
        }
    }
    
    void makeAllButtonsVisible()
    {
        yesButton->setVisible(true);
        noButton->setVisible(true);
        cancelButton->setVisible(true);
        
        repositionButtons();
    }

    void buttonStateChanged (Button* b){};
    
    void buttonClicked (Button* b)
    {
        if(b->getName() == "yes")
        {
            setResponseState(1);
        }
        else if(b->getName() == "no")
        {
            setResponseState(0);
        }

        else if(b->getName() == "cancel")
        {
            setResponseState(2);
        }
        getWindow()->exitModalState(0);
        getWindow()->removeFromDesktop();
    };
    
    void paint(Graphics &g)
    {
        g.drawImageAt(panelImg, 0, 0);
    }
    
    void setResponseState(int state){ buttonResponseState = state;}
    
    int getResponseState(){return buttonResponseState;}
    
private:
    
    int buttonResponseState;
    ScopedPointer<TextButton> yesButton;
    ScopedPointer<TextButton> noButton;
    ScopedPointer<TextButton> cancelButton;
    
    ScopedPointer<Label> message;
    
    Image panelImg;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenAlertComponent)
};

#endif
