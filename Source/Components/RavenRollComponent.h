
#ifndef Raven_RollComponent_Header_h
#define Raven_RollComponent_Header_h

#include "RavenButton.h"
#include "RavenFunctionsComponent.h"
#include "RavenTouchComponent.h"

class RavenRollComponent : public RavenTouchComponent
{
public:
    RavenRollComponent(RavenFunctionsContainer *owner)
    {
        
        int yPos = 0;
        int xPos = 0;
        
        RavenNSCButton *preRollButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_4(preRollButton);
        preRollButton->setTopLeftPosition(xPos, yPos);
        preRollButton->addListener(owner); // or add listener from nscContainer?
        preRollButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Locate/images/locate-on_01.png", "TOOLBAR/FUNCTIONMODULE/Locate/images/locate-off_01.png");
        preRollButton->setTag(eNSCGSPreRoll);
        ravenNSCTouchButtons.add(preRollButton);
        addAndMakeVisible(preRollButton);
        preRollButton->setClickingTogglesState(true); //MAKE TOGGLE BUTTON
        xPos += preRollButton->getWidth();
        
        RavenNSCButton *postRollButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_4(postRollButton);
        postRollButton->setTopLeftPosition(xPos, yPos);
        postRollButton->addListener(owner); // or add listener from nscContainer?
        postRollButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Locate/images/locate-on_02.png", "TOOLBAR/FUNCTIONMODULE/Locate/images/locate-off_02.png");
        postRollButton->setTag(eNSCGSPostRoll);
        ravenNSCTouchButtons.add(postRollButton);
        addAndMakeVisible(postRollButton);
        postRollButton->setClickingTogglesState(true); //MAKE TOGGLE BUTTON
        xPos += postRollButton->getWidth();
        
        RavenNSCButton *auditionButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_4(auditionButton);
        auditionButton->setTopLeftPosition(xPos, yPos);
        auditionButton->addListener(owner); // or add listener from nscContainer?
        auditionButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Locate/images/locate-on_03.png", "TOOLBAR/FUNCTIONMODULE/Locate/images/locate-off_03.png");
        auditionButton->setTag(eNSCGSAudition);
        ravenNSCTouchButtons.add(auditionButton);
        addAndMakeVisible(auditionButton);
        auditionButton->setClickingTogglesState(true); //MAKE TOGGLE BUTTON
        xPos += auditionButton->getWidth();
        
        RavenNSCButton *rtzButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_4(rtzButton);
        rtzButton->setTopLeftPosition(xPos, yPos);
        rtzButton->addListener(owner); // or add listener from nscContainer?
        rtzButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Locate/images/locate-on_04.png", "TOOLBAR/FUNCTIONMODULE/Locate/images/locate-off_04.png");
        rtzButton->setTag(eNSCGSTransportRTZ);
        ravenNSCTouchButtons.add(rtzButton);
        addAndMakeVisible(rtzButton);
        //rtzButton->setClickingTogglesState(true); //MAKE TOGGLE BUTTON
        xPos += rtzButton->getWidth();
        
        RavenNSCButton *gteButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_4(gteButton);
        gteButton->setTopLeftPosition(xPos, yPos);
        gteButton->addListener(owner); // or add listener from nscContainer?
        gteButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Locate/images/locate-on_05.png", "TOOLBAR/FUNCTIONMODULE/Locate/images/locate-off_05.png");
        gteButton->setTag(eNSCGSTransportGTE);
        ravenNSCTouchButtons.add(gteButton);
        addAndMakeVisible(gteButton);
        //gteButton->setClickingTogglesState(true); //MAKE TOGGLE BUTTON
        xPos += gteButton->getWidth();
        
        //next row
        int width = xPos;
        xPos = 0;
        yPos += preRollButton->getHeight() - 20;
        int height = yPos;
        setSize(width, height);
    }

    
private:
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenRollComponent)
    
};

#endif
