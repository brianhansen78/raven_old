//
//  RavenInterpolator.h
//  Raven
//
//  Created by Joshua Dickinson on 2/6/13.
//
//

#ifndef Raven_RavenInterpolator_h
#define Raven_RavenInterpolator_h

#include <cmath>
using namespace std;

class RavenInterpolator
{
    //interpolator which uses a (recursive) one-pole IIR filter
public:
    RavenInterpolator(float _timeInSamples = 100.0, float value = 0.0) : timeInSamples(_timeInSamples)
    {
        setInstant(value);
        setTimeInSamples(_timeInSamples);
    }
    
    void set(const float& value)
    {
        targetValue = value;
    }
    
    void set(const float& value, const int& _timeInSamples)
    {
        setTimeInSamples(_timeInSamples);
        set(value);
    }
    
    void setInstant(const float& value)
    {
        //set to a number instantly without interpolation
        targetValue = value;
        currentValue = value;
    }
    
    void setTimeInSamples(const float& _timeInSamples)
    {
        //sets smoothing coefficient so that after X samples, value will be with in 1% of target
        A1 = exp(log(0.01f)/_timeInSamples);
    }
    
    void setTimeInSamplesAnalog(const float& _timeInSamples)
    {
        //sets smoothing coefficient so that after X samples, value will be with in 36.8% of target. This is the spec commonly used for analog capacitors.
        //taken from: "Designing Audio Effect Plug-ins in C++", chapter 12
        if(_timeInSamples == 0.0)
        {
            A1 = 0.0;
            return;
        }
        A1 = exp(log(0.368f)/_timeInSamples);
    }
    
    float update()
    {
        //taken from: http://music.columbia.edu/pipermail/music-dsp/2007-June/066273.html
        return currentValue = targetValue + A1*(currentValue-targetValue);
    }
    
    float operator()()
    {
        //returns the current value so you can use it just like a variable: float x = myInterpolator();
        return currentValue;
    }
    
    float operator()(const float& input)
    {
        //set target and interpolate in one step for use on continuous streams
        targetValue = input;
        return currentValue = targetValue + A1*(currentValue-targetValue);
    }
    
    float peak(const float& input)
    {
        //sets to higher values (almost) instantly
        targetValue = input;
        if(input < currentValue)
            return currentValue = targetValue + A1*(currentValue-targetValue);    // decay stage - 1 pole filter
        else
            return currentValue = input;  // attack stage - average := peak
    }
    
    float valley(const float& input)
    {
        //sets to lower values instantly
        targetValue = input;
        if(input > currentValue)
            return currentValue = targetValue + A1*(currentValue-targetValue);    // attack stage - 1 pole filter
        else
            return currentValue = input;  // attack stage - average := peak
    }
    
    float operator()(const float& input, const float& coefficient)
    {
        //set target and interpolate with explicit coefficient
        targetValue = input;
        return currentValue = input + coefficient*(currentValue-input);
    }
    
private:
    float currentValue, targetValue, timeInSamples, A1, A1PEAK;
};

#endif
