//
//  RavenIconComponent.h
//  Raven
//
//  Created by Joshua Dickinson on 1/21/13.
//
//
#ifndef Raven_RavenIconComponent_h
#define Raven_RavenIconComponent_h

#include "RavenTouchComponent.h"

class RavenIconComponent: public RavenTouchComponent
{
public:
    RavenIconComponent()
    {
        int xOff, yOff, buttonHeight, spacing;
        
        if(RAVEN_24)
        {
            yOff = 28;
            xOff = 0;
            buttonHeight = 80;
            spacing = 0;
        }
        else
        {
            yOff = 27;
            xOff = 0;
            buttonHeight = 60;
            spacing = 0;
        }
        
        int numberOfIcons = RAVEN_NUMBER_OF_ICONS;
        for(int i = 0;i<numberOfIcons;i++)
        {
            iconButtons.add(new RavenButton());
            iconButtons[i]->setName("auto_off");
            if(RAVEN_24)
            {
                iconButtons[i]->setImagePaths("icons/24/" + String(i+1) + ".png", "icons/24/" + String(i+1) + ".png");
                iconButtons[i]->getProperties().set("originalImagePath", String(GUI_PATH) + "icons/24/" + String(i+1) + ".png");
            }
            else
            {
                iconButtons[i]->setImagePaths("icons/32/" + String(i+1) + ".png", "icons/32/" + String(i+1) + ".png");
                iconButtons[i]->getProperties().set("originalImagePath", String(GUI_PATH) + "icons/32/" + String(i+1) + ".png");
            }
            iconButtons[i]->setTopLeftPosition(xOff, yOff);
            iconButtons[i]->setName("icon" + String(i+1));
            addAndMakeVisible(iconButtons[i]);
            ravenTouchButtons.add(iconButtons[i]);
            yOff += buttonHeight + spacing;
        }
                
        if(RAVEN_24)
            panelImage = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/AUTOMATION_STATUS/panel/AutmationMenu-Panel.png"));
        else
            panelImage = ImageCache::getFromFile(File(String(GUI_PATH) + "FLAT_GRAPHICS/AUTOMATION_STATUS32/panel/AutmationMenu-Panel.png"));
        
        setSize(panelImage.getWidth(), yOff);
    }
    
    void paint(Graphics &g)
    {
        //g.drawImageAt(panelImage, 0, 0);
    }
    
    void setIconButtonListener(Button::Listener *listener)
    {
        for(int i = 0;i<iconButtons.size();i++)
        {
            iconButtons[i]->addListener(listener);
        }
    }
    
    Image getButtonImage(int index)
    {
        return iconButtons[index]->getNormalImage();
    }
    
private:
    
    Image panelImage;
    Array<RavenButton*> iconButtons;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenIconComponent)
};

#endif
