
#ifndef Raven_RavenNavPadComponent_h
#define Raven_RavenNavPadComponent_h

#include "RavenFunctionsComponent.h"
#include "RavenTouchComponent.h"
#include "RavenButton.h"
#include "MTDefines.h"
#include <algorithm>

enum
{
    eNavPadScrub,
    eNavPadShuttle,
    eNavPadTrackZoom,
    eNavPadTrackZoomAll,
    eNavPadWaveform,
    eNumNavPadStates
};

enum
{
    eKeyCodeScrub,
    eKeyCodeShuttle,
    eKeyCodeTrackZoom,
    eKeyCodeTrackZoomAll,
    eKeyCodeWaveform,
    eKeyCodeNavigate
};

#define NAV_PAD_GROUP_ID 3

class RavenKeyHolder
{
public:
    RavenKeyHolder()
    {}
    
    void holdKeys(std::vector<int> &keys)
    {
        //check to see if we need to release any keys
        bool shouldBeReleased[keysBeingHeld.size()];
        for(int i = 0;i<keysBeingHeld.size();i++)
        {
            shouldBeReleased[i] = true;
            for(int j = 0;j<keys.size()-1;j++)
            {
                if(keysBeingHeld[i] == keys[j])
                {
                    shouldBeReleased[i] = false;
                }
            }
        }
        
        for(int i = 0;i<keysBeingHeld.size();i++)
        {
            if(shouldBeReleased[i] == true)
            {
                MacKeyPress::releaseKey(keysBeingHeld[i]);
            }
        }
        
        //check to see if we need to hold down any keys
        for(int i = 0;i<keys.size()-1;i++)
        {
            bool shouldBeHeld = true;
            for(int j = 0;j<keysBeingHeld.size();j++)
            {
                if(keys[i] == keysBeingHeld[j])
                {
                    shouldBeHeld = false;
                }
            }
            if(shouldBeHeld)
            {
                printf("holding key: %d\n", keys[i]);
                MacKeyPress::holdKey(keys.at(i));
            }
        }
        
        keysBeingHeld.clear();
        for(int i = 0;i<keys.size()-1;i++)
        {
            keysBeingHeld.push_back(keys[i]);
        }
        MacKeyPress::pressKey(keys[keys.size()-1]);
    }
    
    void clear()
    {
        printf("clearing keys\n");
        for(int i = 0;i<keysBeingHeld.size();i++)
        {
            printf("clearing this key: %d\n", keysBeingHeld[i]);
            MacKeyPress::releaseKey(keysBeingHeld[i]);
        }
        keysBeingHeld.clear();
    }
    
private:
    std::vector<int> keysBeingHeld;
};

class RavenNavPadImageComponent : public Component
{
public:
    RavenNavPadImageComponent(bool isFloating = false) : imgIndex(eNavPadScrub)
    {
        if(!isFloating)
        {
            //original size navpad
            navPadImg[eNavPadScrub]     =   ImageCache::getFromFile(File(String(GUI_PATH) + "TOOLBAR/FUNCTIONMODULE/NavPad/Navpad-Scrub.png"));
            navPadImg[eNavPadShuttle]   =   ImageCache::getFromFile(File(String(GUI_PATH) + "TOOLBAR/FUNCTIONMODULE/NavPad/Navpad-Shuttle.png"));
            navPadImg[eNavPadTrackZoom] =   ImageCache::getFromFile(File(String(GUI_PATH) + "TOOLBAR/FUNCTIONMODULE/NavPad/Navpad-trackzoom.png"));
            navPadImg[eNavPadTrackZoomAll] =   ImageCache::getFromFile(File(String(GUI_PATH) + "TOOLBAR/FUNCTIONMODULE/NavPad/Navpad-trackzoomAll.png"));
            navPadImg[eNavPadWaveform]  =   ImageCache::getFromFile(File(String(GUI_PATH) + "TOOLBAR/FUNCTIONMODULE/NavPad/Navpad-waveform.png"));
        }
        else
        {
            navPadImg[eNavPadScrub]     =   ImageCache::getFromFile(File(String(GUI_PATH) + "FloatingNavpad/Background/Navpad-scrub.png"));
            navPadImg[eNavPadShuttle]   =   ImageCache::getFromFile(File(String(GUI_PATH) + "FloatingNavpad/Background/Navpad-shuttle.png"));
            navPadImg[eNavPadTrackZoom] =   ImageCache::getFromFile(File(String(GUI_PATH) + "FloatingNavpad/Background/Navpad-trackzoom.png"));
            navPadImg[eNavPadTrackZoomAll] =   ImageCache::getFromFile(File(String(GUI_PATH) + "FloatingNavpad/Background/Navpad-trackzoomAll.png"));
            navPadImg[eNavPadWaveform]  =   ImageCache::getFromFile(File(String(GUI_PATH) + "FloatingNavpad/Background/Navpad-waveform.png"));
        }
        
        setSize(navPadImg[0].getWidth(), navPadImg[0].getHeight());
    }
    
    void paint(Graphics &g)
    {
        g.drawImageAt(navPadImg[imgIndex], 0, 0);
    }
    
    void setImageIndex(int idx)
    {
        imgIndex = idx;
        repaint();
    }
    
    int getImageIndex() {return imgIndex;}
    
private:
    
    Image navPadImg[eNumNavPadStates];
    int imgIndex;
};

class RavenNavPadComponent : public RavenTouchComponent, public Button::Listener, public ActionBroadcaster
{
public:
    RavenNavPadComponent(RavenFunctionsContainer *owner, bool _isFloating = false) : isFloating(_isFloating), sumXSpeed(0), sumYSpeed(0), sumCount(0), mouseStartX(0), mode(eNavPadTrackZoom), individualTrackZoomMode(true), lastKeyPressType(-1)
    {
        //Invisible NSC buttons
//        leftButton = new RavenNSCButton(owner);
//        leftButton->setTag(eNSCGSCursorLeft);
//        leftButton->addListener(owner);
//        
//        rightButton = new RavenNSCButton(owner);
//        rightButton->setTag(eNSCGSCursorRight);
//        rightButton->addListener(owner);
//        
//        upButton = new RavenNSCButton(owner);
//        upButton->setTag(eNSCGSCursorUp);
//        upButton->addListener(owner);
//        
//        downButton = new RavenNSCButton(owner);
//        downButton->setTag(eNSCGSCursorDown);
//        downButton->addListener(owner);
        
        //Raven Buttons
        waveformButton = new RavenButton();
        RAVEN_BUTTON_1(waveformButton);
        waveformButton->setName("waveformButon");
        waveformButton->setClickingTogglesState(true);
        waveformButton->setTopLeftPosition(0, 0);
        waveformButton->addListener(this);
        waveformButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/NavPadMode/images/navpadmode-on_01.png", "TOOLBAR/FUNCTIONMODULE/NavPadMode/images/navpadmodeBrown_01.png");
        waveformButton->setName("waveformButton");
        ravenTouchButtons.add(waveformButton);
        
        //the nav pad itself
        navPadImgCmp = new RavenNavPadImageComponent(isFloating);
        navPadImgCmp->setTopLeftPosition(2*waveformButton->getWidth(), 0);
        addAndMakeVisible(navPadImgCmp);
        
        addAndMakeVisible(waveformButton);//make waveform button visible AFTER the nav pad so that it goes on top
        
        nscScrubButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_1(nscScrubButton);
        nscScrubButton->setTag(eNSCGSScrub);
        nscScrubButton->addListener(this);
        nscScrubButton->addListener(owner);
        nscScrubButton->setTopLeftPosition(0, waveformButton->getHeight());
        nscScrubButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Scrub/bt-Scrub-On.png", "TOOLBAR/FUNCTIONMODULE/Scrub/bt-Scrub-Off.png");
        ravenNSCTouchButtons.add(nscScrubButton);
        nscScrubButton->setClickingTogglesState(true);
        addAndMakeVisible(nscScrubButton);
        
        nscShuttleButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_1(nscShuttleButton);
        nscShuttleButton->setTag(eNSCGSShuttle);
        nscShuttleButton->addListener(this);
        nscShuttleButton->addListener(owner);
        nscShuttleButton->setTopLeftPosition(waveformButton->getWidth(), waveformButton->getHeight());
        nscShuttleButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Shuttle/bt-Shuttle-On.png", "TOOLBAR/FUNCTIONMODULE/Shuttle/bt-Shuttle-Off.png");
        ravenNSCTouchButtons.add(nscShuttleButton);
        nscShuttleButton->setClickingTogglesState(true);
        addAndMakeVisible(nscShuttleButton);
        
        trackzoomButton = new RavenButton();
        RAVEN_BUTTON_1(trackzoomButton);
        trackzoomButton->setName("trackzoomButton");
        trackzoomButton->setClickingTogglesState(true);
        trackzoomButton->addListener(this);
        trackzoomButton->setTopLeftPosition(waveformButton->getWidth(), 0);
        trackzoomButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/NavPadMode/images/navpadmode-on_02.png", "TOOLBAR/FUNCTIONMODULE/NavPadMode/images/navpadmodeBrown_02.png");
        ravenTouchButtons.add(trackzoomButton);
        addAndMakeVisible(trackzoomButton);
        setSize(2*waveformButton->getWidth() + navPadImgCmp->getWidth(), navPadImgCmp->getHeight());
        
        //initialize key codes
        trackZoomKeyCodeUp.push_back(58);//alt
        trackZoomKeyCodeUp.push_back(59);//control
        trackZoomKeyCodeUp.push_back(126);//up
        
        trackZoomKeyCodeDown.push_back(58);//alt
        trackZoomKeyCodeDown.push_back(59);//control
        trackZoomKeyCodeDown.push_back(125);//down
        
        navigateKeyCodeUp.push_back(116);//page up
        
        navigateKeyCodeDown.push_back(121);//page down
        
        navigateKeyCodeLeft.push_back(58);//alt
        navigateKeyCodeLeft.push_back(116);//page up
        
        navigateKeyCodeRight.push_back(58);//alt
        navigateKeyCodeRight.push_back(121);//page down
        
        waveformZoomKeyCodeUp.push_back(58);//alt
        waveformZoomKeyCodeUp.push_back(55);//command
        waveformZoomKeyCodeUp.push_back(30);//]
        
        waveformZoomKeyCodeDown.push_back(58);//alt
        waveformZoomKeyCodeDown.push_back(55);//command
        waveformZoomKeyCodeDown.push_back(33);//[
        
        waveformZoomKeyCodeRight.push_back(55);//command
        waveformZoomKeyCodeRight.push_back(30);//]
        
        waveformZoomKeyCodeLeft.push_back(55);//command
        waveformZoomKeyCodeLeft.push_back(33);//[
        
        enterWaveformKeyCode.push_back(55);//command
        enterWaveformKeyCode.push_back(20);//[
        
        individualTrackZoomKeyCodeUp.push_back(59);//control
        individualTrackZoomKeyCodeUp.push_back(126);//up
        
        individualTrackZoomKeyCodeDown.push_back(59);//control
        individualTrackZoomKeyCodeDown.push_back(125);//down
        
        timeOfLastKeyPress = Time::getCurrentTime();
    }
    
    void buttonStateChanged (Button* b)
    {
    }
    
    void buttonClicked (Button* b)
    {
        //printf("normal button clicked: %d\n", b->getToggleState());

        if(isFloating == false)
        {
            if(b == waveformButton && b->getToggleState() == true)
            {
                enterWaveformMode();
            }
            else if(b == trackzoomButton && b->getToggleState() == true)
            {
                enterTrackZoomMode();
            }
            else if(b == trackzoomButton && b->getToggleState() == false)
            {
                printf("trackzoom pressed again!\n");
                //stay in track zoom but flip the individual track zoom flag
                flipZoomMode();
                enterTrackZoomMode();
            }
            else if(b == nscScrubButton && b->getToggleState() == true)
            {
                enterScrubMode();
            }
            else if(b == nscShuttleButton && b->getToggleState() == true)
            {
                enterShuttleMode();
            }
        }    
        repaint();
    }
    
    bool checkPadTouch(int touchX, int touchY)
    {
        if(componentTouched(navPadImgCmp, touchX, touchY, false) == true)
        {
            resetSums(touchX);
            return true;
        }
        else
        {
            return false;
        }
    }
    
    void updatePadTouch(TUIO::TuioCursor *tcur, bool isTwoFinger)
    {
        float xSpeed = tcur->getXSpeed();
        float ySpeed = tcur->getYSpeed();
        int xPos = tcur->getScreenX(SCREEN_WIDTH);
        int yPos = tcur->getScreenY(SCREEN_HEIGHT);
        updatePadTouch(xPos, yPos, xSpeed, ySpeed, isTwoFinger);
    }
    
    void updatePadTouch(int xPos, int yPos, float xSpeed, float ySpeed, bool isTwoFinger, int numAvg = RAVEN_NAVPAD_NUM_AVG_DEFAULT)
    {
        sumXSpeed += xSpeed;
        sumYSpeed += ySpeed;
        sumCount++;
        //printf("sumXSpeed: %f, sumYSpeed: %f\n", fabs(sumXSpeed), fabs(sumYSpeed));
        
        if(mode == eNavPadScrub)
        {
            numAvg = RAVEN_NAVPAD_NUM_AVG_SCRUB;
        }
        
        if(sumCount >= numAvg && (Time::getCurrentTime().toMilliseconds() - timeOfLastKeyPress.toMilliseconds() > RAVEN_NAVPAD_WAIT_MILLISECONDS))
        {
            //printf("   TIME difference %lld\n", Time::getCurrentTime().toMilliseconds() - timeOfLastKeyPress.toMilliseconds());
            float avgXSpeed = sumXSpeed / numAvg;
            float avgYSpeed = sumYSpeed / numAvg;
            //printf("AvgXSpeed %f", avgXSpeed);
            if(fabs(avgXSpeed) > fabs(avgYSpeed))//left/right movement
            {
                if(avgXSpeed > 0) //right
                {
                    if(isTwoFinger)
                    {
                        if(fabs(avgXSpeed) > RAVEN_SCROLL_SPEED_THRESH)
                        {
                            MacKeyPress::pressCombo(navigateKeyCodeRight);
                        }
                    }
                    else if(mode == eNavPadWaveform || mode == eNavPadTrackZoom)
                    {
                        if(fabs(avgXSpeed) > RAVEN_WAVEFORM_WIDTH_SPEED_THRESH)
                        {
                            MacKeyPress::pressCombo(waveformZoomKeyCodeRight);//Hotkey way
                            //rightButton->triggerClick();//HUI Way
                        }
                    }
                    else if (mode == eNavPadScrub || mode == eNavPadShuttle)
                    {
                        if(fabs(avgXSpeed) > RAVEN_SCRUB_SPEED_THRESH)
                        {
                            float tempSpeed;
                            if(mode == eNavPadScrub)
                            {
                                int targetClicks = (xPos-mouseStartX)*RAVEN_SCRUB_SPEED_MULT;
                                //printf("right %d, %d, %d, %d, %d\n", targetClicks, currentClicks, xPos, mouseStartX, targetClicks-currentClicks);
                                int tempClicks = std::min(targetClicks-currentClicks, 127);
                                link->set(eNSCGlobalControl, eNSCGSJogShuttleWheel, 0, 0, tempClicks);
                                currentClicks = currentClicks+tempClicks;
                            }
                            else
                            {
                                tempSpeed = std::min(avgXSpeed*RAVEN_SHUTTLE_SPEED_MULT, 127.0f);
                                link->set(eNSCGlobalControl, eNSCGSJogShuttleWheel, 0, 0, tempSpeed);
                            }
                        }
                    }
                }
                else //left
                {
                    if(isTwoFinger)
                    {
                        if(fabs(avgXSpeed) > RAVEN_SCROLL_SPEED_THRESH)
                        {
                            MacKeyPress::pressCombo(navigateKeyCodeLeft);
                        }
                    }
                    else if(mode == eNavPadWaveform || mode == eNavPadTrackZoom)
                    {
                        if(fabs(avgXSpeed) > RAVEN_WAVEFORM_WIDTH_SPEED_THRESH)
                        {
                            MacKeyPress::pressCombo(waveformZoomKeyCodeLeft);
                            //leftButton->triggerClick();//HUI Way
                        }
                    }
                    else if (mode == eNavPadScrub || mode == eNavPadShuttle)
                    {
                        if(fabs(avgXSpeed) > RAVEN_SCRUB_SPEED_THRESH)
                        {
                            float tempSpeed;
                            if(mode == eNavPadScrub)
                            {
                                int targetClicks = (xPos-mouseStartX)*RAVEN_SCRUB_SPEED_MULT;
                                //printf("left %d, %d, %d, %d, %d\n", targetClicks, currentClicks, xPos, mouseStartX, targetClicks-currentClicks);
                                int tempClicks = std::max(targetClicks-currentClicks, -63);
                                link->set(eNSCGlobalControl, eNSCGSJogShuttleWheel, 0, 0, tempClicks);
                                currentClicks = currentClicks+tempClicks;
                            }
                            else
                            {
                                tempSpeed = std::max(avgXSpeed*RAVEN_SHUTTLE_SPEED_MULT, -63.0f);
                                link->set(eNSCGlobalControl, eNSCGSJogShuttleWheel, 0, 0, tempSpeed);
                            }
                        }
                    }
                }
            }
            else //up/down movement
            {
                //std::cout << "ZOOM!!!!" << std::endl;
                
                if(avgYSpeed > 0) //down
                {
                    if(isTwoFinger)
                    {
                        if(fabs(avgYSpeed) > RAVEN_SCROLL_SPEED_THRESH)
                            MacKeyPress::pressCombo(navigateKeyCodeDown);
                    }
                    else if(mode == eNavPadWaveform)
                    {
                        if(fabs(avgYSpeed) > RAVEN_WAVEFORM_HEIGHT_SPEED_THRESH)
                        {
                            //downButton->triggerClick();//HUI way
                            MacKeyPress::pressCombo(waveformZoomKeyCodeDown);//Hotkey way
                        }
                    }
                    else if(mode == eNavPadTrackZoom)
                    {
                        if(fabs(avgYSpeed) > RAVEN_TRACK_ZOOM_SPEED_THRESH)
                        {
                            if(individualTrackZoomMode == true)
                            {
                                MacKeyPress::pressCombo(individualTrackZoomKeyCodeDown);
                            }
                            else
                            {
                                MacKeyPress::pressCombo(trackZoomKeyCodeDown);
                            }
                        }
                    }
                }
                else //up
                {
                    if(isTwoFinger)
                    {
                        if(fabs(avgYSpeed) > RAVEN_SCROLL_SPEED_THRESH)
                            MacKeyPress::pressCombo(navigateKeyCodeUp);
                    }
                    else if(mode == eNavPadWaveform)
                    {
                        if(fabs(avgYSpeed) > RAVEN_WAVEFORM_HEIGHT_SPEED_THRESH)
                        {
                            //upButton->triggerClick();//HUI way
                            MacKeyPress::pressCombo(waveformZoomKeyCodeUp);//Hotkey way
                        }
                    }
                    else if(mode == eNavPadTrackZoom)
                    {
                        if(fabs(avgYSpeed) > RAVEN_TRACK_ZOOM_SPEED_THRESH)
                        {
                            if(individualTrackZoomMode == true)
                            {
                                MacKeyPress::pressCombo(individualTrackZoomKeyCodeUp);
                            }
                            else
                            {
                                MacKeyPress::pressCombo(trackZoomKeyCodeUp);
                            }
                        }
                    }
                }
            }
            timeOfLastKeyPress = Time::getCurrentTime();
            sumCount = 0;
            sumXSpeed = 0;
            sumYSpeed = 0;
        }
    }
    
    void setNscLink(NscLink *_link) { link = _link;}
    
    void setButtonListener(Button::Listener *listener)
    {
        waveformButton->addListener(listener);
        trackzoomButton->addListener(listener);
    }
    
    void enterWaveformMode()
    {
        printf("waveform pressed!\n");
        //if triggered from the floating nav pad, the waveform button might not be pressed, so press it.
        if(waveformButton->getToggleState() == false)
        {
            waveformButton->setToggleState(true, false);
        }
        navPadImgCmp->setImageIndex(eNavPadWaveform);
        trackzoomButton->setToggleState(false, false);
        
        if(nscScrubButton->getToggleState() == true)
        {
            //nscScrubButton->triggerClick();
            nscScrubButton->setToggleState(false, true);
        }
        else if(nscShuttleButton->getToggleState() == true)
        {
            nscShuttleButton->setToggleState(false, true);
        }
        mode = eNavPadWaveform;
        sendActionMessage("waveform");
        printf("mode is waveform\n");
    }
    
    void enterTrackZoomMode()
    {
        printf("trackzoom pressed!\n");
        //if triggered from the floating nav pad, the trackzoom button might not be pressed, so press it.
        if(trackzoomButton->getToggleState() == false)
        {
            trackzoomButton->setToggleState(true, false);
        }
        waveformButton->setToggleState(false, false);
        
        if(nscScrubButton->getToggleState() == true)
        {
            //nscScrubButton->triggerClick();
            nscScrubButton->setToggleState(false, true);
        }
        else if(nscShuttleButton->getToggleState() == true)
        {
            nscShuttleButton->setToggleState(false, true);
        }
        
        mode = eNavPadTrackZoom;
        if(individualTrackZoomMode == true)
        {
            navPadImgCmp->setImageIndex(eNavPadTrackZoom);
            trackzoomButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/NavPadMode/images/navpadmode-on_02.png", "TOOLBAR/FUNCTIONMODULE/NavPadMode/images/navpadmodeBrown_02.png");
            sendActionMessage("trackzoom");
            printf("mode is trackzoom\n");
        }
        else
        {
            navPadImgCmp->setImageIndex(eNavPadTrackZoomAll);
            trackzoomButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/NavPadMode/images/navpadmode-TarckZoomAll-On.png", "TOOLBAR/FUNCTIONMODULE/NavPadMode/images/navpadmode-TarckZoomAll-Off.png");
            sendActionMessage("trackzoom all");
            printf("mode is trackzoom all\n");
        }
    }
    
    void enterScrubMode()
    {
        printf("scrub pressed!\n");
        if(nscShuttleButton->getToggleState() == true)
        {
            nscShuttleButton->setToggleState(false, true);
        }
        //if triggered from the floating nav pad, the scrub button might not be pressed, so press it. 
        if(nscScrubButton->getToggleState() == false)
        {
            nscScrubButton->setToggleState(true, true);
        }
        navPadImgCmp->setImageIndex(eNavPadScrub);
        trackzoomButton->setToggleState(false, false);
        waveformButton->setToggleState(false, false);
        
        link->set(eNSCGlobalControl, eNSCGSTransportStop, 0, 0, 1);
        link->set(eNSCGlobalControl, eNSCGSTransportStop, 0, 0, 0);
        
        mode = eNavPadScrub;
        sendActionMessage("scrub");
        printf("mode is scrub\n");
    }
    
    void enterShuttleMode()
    {
        printf("shuttle pressed!\n");
        if(nscScrubButton->getToggleState() == true)
        {
            nscScrubButton->setToggleState(false, true);
        }
        //if triggered from the floating nav pad, the shuttle button might not be pressed, so press it.
        if(nscShuttleButton->getToggleState() == false)
        {
            nscShuttleButton->setToggleState(true, true);
        }
        navPadImgCmp->setImageIndex(eNavPadShuttle);
        trackzoomButton->setToggleState(false, false);
        waveformButton->setToggleState(false, false);
        
        mode = eNavPadShuttle;
        sendActionMessage("shuttle");
        printf("mode is shuttle\n");
    }
    
    void exitScrubShuttle()
    {
        if(nscScrubButton->getToggleState() == true)
        {
            enterWaveformMode();
        }
        else if(nscShuttleButton->getToggleState() == true)
        {
            enterWaveformMode();
        }
    }
    
    void restartScrubOrShuttleMode()
    {
        if(mode == eNavPadScrub)
        {
            nscScrubButton->setToggleState(false, true);
            nscScrubButton->setToggleState(true, true);
        }
        else if(mode == eNavPadShuttle)
        {
            nscShuttleButton->setToggleState(false, true);
            nscShuttleButton->setToggleState(true, true);
        }
    }
    
    void reEnterShuttle()
    {
        nscShuttleButton->setToggleState(false, true);
        nscShuttleButton->setToggleState(true, true);
    }
    
    void resetSums(int touchX)
    {
        sumXSpeed = 0;
        sumYSpeed = 0;
        sumCount = 0;
        mouseStartX = touchX;
        currentClicks = 0;
    }
    
    void flipZoomMode()
    {
        individualTrackZoomMode = !individualTrackZoomMode;
    }
    
    bool getIndividualTrackZoomMode()
    {
        return individualTrackZoomMode;
    }
    
    bool isScrubOrShuttleMode()
    {
        return (mode == eNavPadScrub || mode == eNavPadShuttle);
    }
    
private:
    
    ScopedPointer<RavenNavPadImageComponent> navPadImgCmp;
    
    RavenButton *waveformButton;
    RavenButton *trackzoomButton;
    
    RavenNSCButton* nscScrubButton;
    RavenNSCButton* nscShuttleButton;
    
    //invisible NSC Buttons
//    ScopedPointer<RavenNSCButton> leftButton;
//    ScopedPointer<RavenNSCButton> rightButton;
//    ScopedPointer<RavenNSCButton> upButton;
//    ScopedPointer<RavenNSCButton> downButton;
    
    NscLink *link;
    bool isFloating;
    
    std::vector<int> trackZoomKeyCodeUp, trackZoomKeyCodeDown, waveformZoomKeyCodeUp, waveformZoomKeyCodeDown, waveformZoomKeyCodeLeft, waveformZoomKeyCodeRight, navigateKeyCodeUp, navigateKeyCodeDown, navigateKeyCodeLeft, navigateKeyCodeRight;
    
    std::vector<int> enterWaveformKeyCode, individualTrackZoomKeyCodeUp, individualTrackZoomKeyCodeDown;
    
    float sumXSpeed;
    float sumYSpeed;
    int sumCount;
    
    int mouseStartX;
    int currentClicks;
    int mode;
    bool individualTrackZoomMode;
    bool isBeingTouched;
    Time timeOfLastKeyPress;
    
    RavenKeyHolder keyHolder;
    int lastKeyPressType;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenNavPadComponent)
};

//int RavenNavPadComponent::mode = eNavPadTrackZoom;

#endif
