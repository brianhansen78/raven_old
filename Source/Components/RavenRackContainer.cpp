//
//  RavenRackContainer.cpp
//  Raven
//
//  Created by Ryan McGee on 6/6/13.
//
//

#include "RavenRackContainer.h"

//Global static pointer used to ensure a single instance of the class.
RavenRackContainer* RavenRackContainer::instance = 0;