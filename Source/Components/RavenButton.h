
#ifndef Raven_NewSDK_RavenButton_h
#define Raven_NewSDK_RavenButton_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "NscGuiElement.h"

typedef enum
{
    eButtonType_Default,
    eButtonType_S,
    eButtonType_M,
    eButtonType_L,
    eButtonType_XL
} RavenButtonType;


class RavenButton : public ImageButton
{
public:
    RavenButton();
    
    ~RavenButton()
    {
        
    }
    
    void setImagePaths(String onImgPath, String offImgPath);
    
    /* move to raven component...
    static void setRavenButtonType(RavenButtonType type)
    {
        switch (type) {
            case eButtonType_S:
                
                break;
            case eButtonType_M:

                break;
            case eButtonType_L:

                break;
            case eButtonType_XL:

                break;
            default:

                break;
        }
    }
     */

    void trim(int top, int bottom, int left, int right);
    
    void rescale(float ratio);
    
    std::vector<int> *getKeyCode(){return &keyCode;}
    
    void addToKeyCode(int key)
    {
        keyCode.push_back(key);
    }
    
    void clearKeyCode()
    {
        keyCode.clear();
    }

    bool enabled; // our own enablement because we are unable to keep juce from changing button alpha when disabled
    
protected:
    
	Image offImage;
    Image onImage;
    
private:
    std::vector<int> keyCode;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenButton)
};

class RavenNSCButton : public RavenButton, public NscGuiElement
{
public:
    RavenNSCButton(NscGuiContainer *owner);
    ~RavenNSCButton();
    
    virtual void  updateNscValue(float value);			// callback when DAW changes a value
    float getNscValue(){return nscValue;}//used for meter peaks

private:
    float nscValue;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenNSCButton)
};


#endif
