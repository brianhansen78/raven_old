
#ifndef Raven_EditMoveFineClickComponent_h
#define Raven_EditMoveFineClickComponent_h

#include "RavenButton.h"
#include "RavenFunctionsComponent.h"
#include "RavenTouchComponent.h"

class RavenEditMoveFineClickComponent : public RavenTouchComponent, public Button::Listener
{
public:
    RavenEditMoveFineClickComponent(RavenFunctionsContainer *owner) : isEditWin(true)
    {
        //invisible
        editWinNSCButton = new RavenNSCButton(owner);
        editWinNSCButton->addListener(owner);
        editWinNSCButton->setTag(eNSCGSPTWindow_Edit);
        
        mixWinNSCButton = new RavenNSCButton(owner);
        mixWinNSCButton->addListener(owner);
        mixWinNSCButton->setTag(eNSCGSPTWindow_Mix);
        
        RavenButton *editMixButton = new RavenButton();
        editMixButton->setTopLeftPosition(0, 2);
        editMixButton->addListener(this);
        editMixButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/EditMixClick/images/edit-click-on_01.png", "TOOLBAR/FUNCTIONMODULE/EditMixClick/images/edit-click-off_01.png");
        ravenTouchButtons.add(editMixButton);
        addAndMakeVisible(editMixButton);
        
        RavenNSCButton *clickButton = new RavenNSCButton(owner);
        clickButton->setTopLeftPosition(0, editMixButton->getBottom());
        clickButton->addListener(owner);
        clickButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/EditMixClick/images/edit-click-on_02.png", "TOOLBAR/FUNCTIONMODULE/EditMixClick/images/edit-click-off_02.png");
        clickButton->setTag(eNSCGSClickSwitch);
        clickButton->setClickingTogglesState(true);
        ravenNSCTouchButtons.add(clickButton);
        addAndMakeVisible(clickButton);
        
        moveButton = new RavenButton();
        moveButton->setName("moveModules");
        moveButton->setClickingTogglesState(true);
        moveButton->setTopLeftPosition(editMixButton->getWidth(), 0);
        moveButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/MoveFine/images/Movemodule-Finepan-on_01.png", "TOOLBAR/FUNCTIONMODULE/MoveFine/images/Movemodule-Finepan-off_01.png");
        moveButton->addListener(this);
        ravenTouchButtons.add(moveButton);
        addAndMakeVisible(moveButton);
        
        fineButton = new RavenButton();
        fineButton->setName("finePan");
        fineButton->setClickingTogglesState(true);
        fineButton->setTopLeftPosition(clickButton->getWidth(), moveButton->getHeight());
        fineButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/PanModes/MSwitchTO_CircularPanMode.png", "TOOLBAR/FUNCTIONMODULE/PanModes/CircularPanMode.png");
        ravenTouchButtons.add(fineButton);
        addAndMakeVisible(fineButton);

        setSize(editMixButton->getWidth()+moveButton->getWidth(), editMixButton->getHeight()+clickButton->getHeight());        
    }
    
    void buttonStateChanged (Button*)
    {
        
    }
    void buttonClicked (Button* b)
    {
        if(b == ravenTouchButtons[0]) //if edit/mix button
        {
            isEditWin = !isEditWin;
            if(isEditWin)   editWinNSCButton->triggerClick();
            else            mixWinNSCButton->triggerClick();
        }
    }
    
    bool getMoveToggleState(){ return moveButton->getToggleState(); }
    
    bool checkMoveToggleTouch(TUIO::TuioCursor *tcur){return componentTouched(moveButton, tcur);}
    
    void setRackButtonListener(Button::Listener *listener)
    {
        fineButton->addListener(listener);
        moveButton->addListener(listener);
    }
    
private:
    
    RavenButton *moveButton;
    RavenButton *fineButton;
    
    //invisible
    ScopedPointer<RavenNSCButton> editWinNSCButton;
    ScopedPointer<RavenNSCButton> mixWinNSCButton;
    
    bool isEditWin;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenEditMoveFineClickComponent)
    
};

#endif
