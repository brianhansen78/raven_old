//
//  RavenIconPaletteComponent.h
//  Raven
//
//  Created by Joshua Dickinson on 5/14/13.
//
//

#ifndef Raven_RavenIconPaletteComponent_h
#define Raven_RavenIconPaletteComponent_h

#include "RavenConfig.h"
#include "RavenButton.h"
#include "RavenTextDisplay.h"
#include "RavenWindowComponent.h"

#if RAVEN_24
const String iconPaths[] =
{
    "icons/24/frame.png",
    "icons/24/icons-GTR-24ch.png",
    "icons/24/icons-Bass-24ch.png",
    "icons/24/icons-LeadMale-24ch.png",
    "icons/24/icons-LeadFemale-24ch.png",
    "icons/24/icons-Delay-24ch.png",
    "icons/24/icons-Reverb-24ch.png",
    "icons/24/icons-Synth-24ch.png",
    "icons/24/icons-Organ-24ch.png",
    "icons/24/icons-Piano-24ch.png",
    "icons/24/icons-HH-24ch.png",
    "icons/24/icons-OH-24ch.png",
    "icons/24/icons-Kick In-24ch.png",
    "icons/24/icons-Kick Out-24ch.png",
    "icons/24/icons-Snare-24ch.png",
    "icons/24/icons-KitRoom-24ch.png",
    "icons/24/icons-Tom1-24ch.png"
};
#else
const String iconPaths[] =
{
    "icons/32/frame.png",
    "icons/32/icons-GTR-32ch.png",
    "icons/32/icons-Bass-32ch.png",
    "icons/32/icons-LeadMale-32ch.png",
    "icons/32/icons-LeadFemale-32ch.png",
    "icons/32/icons-Delay-32ch.png",
    "icons/32/icons-Reverb-32ch.png",
    "icons/32/icons-Synth-32ch.png",
    "icons/32/icons-Organ-32ch.png",
    "icons/32/icons-Piano-32ch.png",
    "icons/32/icons-HH-32ch.png",
    "icons/32/icons-OH-32ch.png",
    "icons/32/icons-Kick In-32ch.png",
    "icons/32/icons-Kick Out-32ch.png",
    "icons/32/icons-Snare-32ch.png",
    "icons/32/icons-KitRoom-32ch.png",
    "icons/32/icons-Tom1-32ch.png"
};
#endif

class RavenIconPaletteComponent : public RavenWindowComponent, public Button::Listener
{
public:
    RavenIconPaletteComponent() : activeIcon(false), initWidth(200), initHeight(100)
    {
        int headerHeight = 73;
        int leftPadding = 42;
        int buttonHeight = RAVEN_24 ? 80 : 60;
        int spacing = 0;
        int iconsPerRow = 5;
        int iconsPerColumn = 3;
        
        int index = 0;
        for(int i = 0;i<iconsPerColumn;i++)
        {
            for(int j = 0;j<iconsPerRow;j++)
            {
                if(index < RAVEN_NUMBER_OF_ICONS)
                {
                    iconButtons.add(new RavenButton());
                    iconButtons[index]->setImagePaths(iconPaths[index], iconPaths[index]);
                    iconButtons[index]->getNormalImage().getProperties()->set("imageIndex", index+1);
                    iconButtons[index]->setTopLeftPosition(j*(buttonHeight+spacing)+leftPadding, i*(buttonHeight+spacing)+headerHeight);
                    iconButtons[index]->setName("icon" + String(index+1));
                    iconButtons[index]->setClickingTogglesState(true);
                    addAndMakeVisible(iconButtons[index]);
                    iconButtons[index]->addListener(this);
                    ravenTouchButtons.add(iconButtons[index]);
                    index++;
                }
            }
        }
        
        initWidth = iconsPerRow*(buttonHeight+spacing);
        initHeight = iconsPerColumn*(buttonHeight+spacing)+headerHeight;
        
        panelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "iconPalette/Background/ICP_Background.png"));
        
        setSize(panelImg.getWidth(), panelImg.getHeight());
        
        testButton = new RavenButton();
        testButton->setTopLeftPosition(323, 30);
        testButton->setImagePaths("iconPalette/Buttons/ICP_Close_On_Btn.png", "iconPalette/Buttons/ICP_Close_Off_Btn.png");
        ravenTouchButtons.add(testButton);
        testButton->addListener(this);
        addAndMakeVisible(testButton);
        
        browseButton = new RavenButton();
        browseButton->setTopLeftPosition(259, 275);
        browseButton->setImagePaths("iconPalette/Buttons/ICP_Browse_On_btn.png", "iconPalette/Buttons/ICP_Browse_Off_btn.png");
        ravenTouchButtons.add(browseButton);
        browseButton->addListener(this);
        addAndMakeVisible(browseButton);
        
        mouseHider = new MouseHideComponent(getWidth(), getHeight());
        mouseHider->setName("MouseHider");
        addAndMakeVisible(mouseHider);
    }
    
    void buttonStateChanged (Button* b)
    {
    }
    
    void buttonClicked (Button* b);//in .cpp
    
    void paint(Graphics &g)
    {
        g.drawImageAt(panelImg, 0, 0);
    }
    
    void paintOverChildren(Graphics&g)
    {
        if(activeIcon)
        {
            g.setColour(Colours::yellow);
            g.drawRect(activeIcon->getX(), activeIcon->getY(), activeIcon->getWidth(), activeIcon->getHeight(), 4);
        }
    }
    
    bool hasActiveIcon()
    {
        if(activeIcon){ return true;}
        return false;
    }
    
    Image getActiveIconImage()
    {
        //TODO: if(activeIcon){ return activeIcon->getNormalImage();}
        //          else{ return Defualt image or whatever to prevent crash}
        return activeIcon->getNormalImage();
    }
    
    void deActivateIcon()
    {
        if(activeIcon){ activeIcon->setToggleState(false, false);}
        activeIcon = NULL;
    }
    
    void convertIndexMapToIconMap(HashMap<String, int> *_indexMap);
    
private:
    
    RavenButton *testButton;
    RavenButton *browseButton;
    Array<RavenButton*> iconButtons;
    RavenButton* activeIcon;
    
    int initWidth;
    int initHeight;
    Image panelImg;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenIconPaletteComponent)
};

#endif
