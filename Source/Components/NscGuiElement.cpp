
#include "NscGuiElement.h"
#include "RavenMessage.h"

NscGuiElement::NscGuiElement(NscGuiContainer * owner)
	: bankIndex(0)
	, channelIndex(kNscGuiGlobalIdentifier)
	, guiTag(-1)
{
	if (owner)		owner->addGuiElement(this);
}

NscGuiElement::NscGuiElement(NscGuiContainer * owner, int tag)
	: bankIndex(0)
	, channelIndex(kNscGuiGlobalIdentifier)
	, guiTag(tag)
{
	if (owner)		owner->addGuiElement(this);
}

NscGuiElement::NscGuiElement(NscGuiContainer * owner, int bank, int channel, int tag)
	: bankIndex(bank)
	, channelIndex(channel)
	, guiTag(tag)
{
	if (owner)		owner->addGuiElement(this);
}

NscGuiElement::~NscGuiElement()
{
}

void NscGuiElement::updateNscValue(float value)
{
}
void NscGuiElement::updateNscValue(const String& value)
{
}

//==========================================================================================

NscGuiContainer::NscGuiContainer(int bank)
	: bankNum(bank)
	, link(0)
{
	elements.clear();
}
NscGuiContainer::~NscGuiContainer()
{
	elements.clear();
}

void NscGuiContainer::handleMessage(const Message &message)
{
    RavenNSCMessage *m = (RavenNSCMessage*)&message;
    
    //if(m->isText) m->el->updateNscValue(m->text);
    //else m->el->updateNscValue(m->val);
    m->updateNSC();
    
}

void NscGuiContainer::addGuiElement( NscGuiElement * element)
{
	elements.addIfNotAlreadyThere(element);
}

NscGuiElement * NscGuiContainer::findNscGuiElement(int tag)
{
    
	NscGuiElement * retVal = 0;

	for (int i=0; i< elements.size(); ++i)
	{
		NscGuiElement * el = elements[i];
		if (el->guiTag == tag)
		{
			retVal = el;
			break;
		}
	}

	return retVal;
}

NscGuiElement * NscGuiContainer::findNscGuiElement(int bank, int channel, int tag)
{
    
	NscGuiElement * retVal = 0;

	for (int i=0; i< elements.size(); ++i)
	{
		NscGuiElement * el = elements[i];
        
        //adding conditions here accounts for some HUI elements not responding to updates otherwise
        // (fix blinking here)
        
        //time display seems to need all channels
        if(el->guiTag == tag && el->guiTag == eNSCTimeDisplay && el->bankIndex == bank && el->channelIndex == kNscTimeDisplayIdentifier)
        {            
            retVal = el;
			break;
        }
        //inserts elements apply to all channels too
        else if (el->guiTag == tag && el->guiTag == eNSCGSPTWindow_PlugIn && el->bankIndex == bank)
		{
			retVal = el;
			break;
		}
        else if(el->guiTag == tag && tag == eNSCGSFlip) // Flip Mode is channel independant
        {
            retVal = el;
			break;
        }
        else if(el->guiTag == tag && tag == eNSCGSPTBankMode_Pan) // Pan Mode is channel independant
        {
            retVal = el;
			break;
        }
		else if (el->guiTag == tag && el->channelIndex== channel && el->bankIndex == bank)
		{
			retVal = el;
			break;
		}
	}

	return retVal;
}

int NscGuiContainer::callback(ENSCEventType type, int subType, int bank, int which, float val)
{
	NscGuiElement * el = 0;

	if (type == eNSCTrackEvent)
	{
        //if(subType == eNSCTrackMeterClip1) printf("meter clip  = %d\n", val);
        
		el = findNscGuiElement(bank, which, subType);
        if(el)
        {
            RavenNSCMessage *m = new RavenNSCMessage(el, val);
            postMessage(m);
        }
	}
	else if (type == eNSCGlobalControl)
	{
		el = findNscGuiElement(bank, kNscGuiGlobalIdentifier, subType);
        if (el)
        {
            RavenNSCMessage *m = new RavenNSCMessage(el, val);
            postMessage(m);
        }
	}


	return 0;
}
int NscGuiContainer::callback(ENSCEventType type, int subType, int bank, int which, const char * text)
{
	NscGuiElement * el = 0;

	if (type == eNSCTrackEvent)
	{
		el = findNscGuiElement(bank, which, subType);
        if(el)
        {
            RavenNSCMessage *m = new RavenNSCMessage(el, text);
            postMessage(m);
        }
	}
	else if (type == eNSCGlobalControl)
	{
		el = findNscGuiElement(bank, kNscGuiGlobalIdentifier, subType);
        if (el)
        {
            RavenNSCMessage *m = new RavenNSCMessage(el, text);
            postMessage(m);
        }
	}
    else if (type == eNSCPlugInLCD)
	{
		//el = findNscGuiElement(bank, kNscGuiGlobalIdentifier, subType);  if (el) el->updateNscValue(text);
        el = findNscGuiElement(bank, which, subType);
        if (el)
        {
            RavenNSCMessage *m = new RavenNSCMessage(el, text);
            postMessage(m);
        }
	}
    else if (type == eNSCPlugInInsertName)
	{
		//el = findNscGuiElement(bank, kNscGuiGlobalIdentifier, subType);  if (el) el->updateNscValue(text);
        el = findNscGuiElement(bank, which, subType);
        if (el)
        {
            RavenNSCMessage *m = new RavenNSCMessage(el, text);
            postMessage(m);
        }
	}
    
    else if (type == eNSCTimeDisplay)
	{        
        el = findNscGuiElement(bank, which, subType);
        if (el)
        {
            RavenNSCMessage *m = new RavenNSCMessage(el, text);
            postMessage(m);
        }
	}

	return 0;

}


void NscGuiContainer::sliderChanged (NscGuiElement *  slider, float sliderValue)
{
	//DBG (String::formatted("%ld Sending bank = %d, channel = %d, tag = %d", Time::currentTimeMillis(), element->getBankIndex(), element->getChannelIndex(), element->getTag()));
    if (!link || !slider)	return;

	int tag = slider->getTag();
	int channel = slider->getChannelIndex();
    //printf("slider changed tag: %d, bank: %d, channel: %d, sliderValue: %f\n", tag, slider->getBankIndex(), slider->getChannelIndex(), sliderValue);
    
	if (channel < 0)
    {
		link->set(eNSCGlobalControl, tag , 0, 0, sliderValue);
    }
	else if (tag != eNSCTrackVPotValue)
    {
        //printf("slider changed to %f, bank: %d, channel: %d\n", sliderValue, slider->getBankIndex(), channel);
        link->set(eNSCTrackEvent, tag , slider->getBankIndex(), channel, sliderValue);
    }
    //vpots are differet and are handled in the mixer channel component

}
void NscGuiContainer::sliderBegan (NscGuiElement * slider)
{
    //printf("slider began\n");
	if (!link || !slider)	return;
	int tag = slider->getTag();

	if (tag == eNSCTrackFaderValue)
    {
		link->set(eNSCTrackEvent, eNSCTrackFaderSwitch, slider->getBankIndex(), slider->getChannelIndex(), 1);
        //doesn't fix bug by adding two: link->set(eNSCTrackEvent, eNSCTrackFaderSwitch, slider->getBankIndex(), slider->getChannelIndex(), 1);
        //printf("slider began tag: %d, bank: %d, channel: %d\n", tag, slider->getBankIndex(), slider->getChannelIndex());
    }
}

void NscGuiContainer::sliderEnded (NscGuiElement * slider)
{
	if (!link || !slider)	return;
	int tag = slider->getTag();

	if (tag == eNSCTrackFaderValue)
		link->set(eNSCTrackEvent, eNSCTrackFaderSwitch, slider->getBankIndex(), slider->getChannelIndex(), 0.0);
}

void NscGuiContainer::switchClicked (NscGuiElement * Switch, bool switchState)
{
	if (!link || !Switch)	return;

	int tag = Switch->getTag();
    int channel = Switch->getChannelIndex();
    
    //Test new Ney-Fi PlugBot:
    if(tag == eNSCPlugInInsert)
    {
        link->set(eNSCPlugInInsert, tag, Switch->getBankIndex(), channel, 1);
        //link->set(eNSCPlugInInsert, tag, Switch->getBankIndex(), channel, 0);
        return;
    }
    
    if(channel == kNscGuiKeyboardIdentifier)
    {
        link->set(eNSCKeyboard, tag, 0, 0, 1);
        link->set(eNSCKeyboard, tag, 0, 0, 0);
    }
    else if (channel == kNscGuiBankGlobalIdentifier)
	{        
        link->set(eNSCGlobalControl, tag, Switch->getBankIndex(), 0, 1);
        link->set(eNSCGlobalControl, tag, Switch->getBankIndex(), 0, 0);
	}
	
	else if (channel == kNscGuiGlobalIdentifier)
	{
        link->set(eNSCGlobalControl, tag, 0, 0, 1);
        link->set(eNSCGlobalControl, tag, 0, 0, 0);
	}
	else
	{
		link->set(eNSCTrackEvent, tag, Switch->getBankIndex(), channel , 1);
        link->set(eNSCTrackEvent, tag, Switch->getBankIndex(), channel , 0);
	}

}



