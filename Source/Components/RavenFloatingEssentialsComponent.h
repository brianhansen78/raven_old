
#ifndef Raven_RavenFloatingEssentialsComponent_h
#define Raven_RavenFloatingEssentialsComponent_h


#include "RavenButton.h"
#include "RavenTextDisplay.h"
#include "RavenWindowComponent.h"

const int paletteMargin = 15;
const int initialNumChildren = 2;
const int paletteMaxWidth = 974;
const int paletteMaxHeight = 552;
const int moduleGap = 10;


class RavenFloatingEssentialsComponent : public RavenWindowComponent, public Button::Listener
{
public:
    RavenFloatingEssentialsComponent() : customMode(false), initWidth(350), initHeight(250)
    {
        panelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "FLoatingEssentials/floating_essential_base.png"));
        Rectangle<int> clippedPaletteImage = Rectangle<int>::Rectangle(22,22,paletteMaxWidth,paletteMaxHeight);
        panelImg = panelImg.getClippedImage(clippedPaletteImage);
        panelImg = panelImg.rescaled(initWidth, initHeight, Graphics::highResamplingQuality);
        
        setSize(panelImg.getWidth(), panelImg.getHeight());
        
        essentialsCustomButton = new RavenButton();
        essentialsCustomButton->setClickingTogglesState(true);
        essentialsCustomButton->setName("essentialsPaletteCustom");
        RAVEN_BUTTON_1(essentialsCustomButton);
        //essentialsCustomButton->setTopLeftPosition(panelImg.getWidth() - essentialsCustomButton->getWidth() - 45, panelImg.getHeight() - essentialsCustomButton->getHeight() - 45);
        essentialsCustomButton->setTopLeftPosition(paletteMargin, paletteMargin);
        essentialsCustomButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/RavenSettings/ravenSettings-on.png", "TOOLBAR/FUNCTIONMODULE/RavenSettings/ravenSettings-off.png");
        addAndMakeVisible(essentialsCustomButton);
        ravenTouchButtons.add(essentialsCustomButton);
            
        essentialsAddBarButton = new RavenButton();
        essentialsAddBarButton->setName("essentialsAddBarButton");
        RAVEN_BUTTON_2(essentialsAddBarButton);
        essentialsAddBarButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/RackAdd/bt-AddRack-on.png", "TOOLBAR/FUNCTIONMODULE/RackAdd/bt-AddRack-off.png");
        //showToolbarButton->setTopLeftPosition(panelImg.getWidth() - showToolbarButton->getWidth() - 10, panelImg.getHeight() - showToolbarButton->getHeight() - 5);
        essentialsAddBarButton->setTopLeftPosition(paletteMargin, essentialsCustomButton->getBottom());
        //addChildComponent(showToolbarButton);
        addAndMakeVisible(essentialsAddBarButton);
        ravenTouchButtons.add(essentialsAddBarButton);
        
        essentialsRemoveBarButton = new RavenButton();
        essentialsRemoveBarButton->setName("essentialsRemoveBarButton");
        essentialsRemoveBarButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/RackRemove/bt-RemoveRack-on.png", "TOOLBAR/FUNCTIONMODULE/RackRemove/bt-RemoveRack-off.png");
        //    removeBarButton->setTopLeftPosition(addBarButton->getX(), addBarButton->getBottom() - 22);
        essentialsRemoveBarButton->setTopLeftPosition(paletteMargin, essentialsAddBarButton->getY() + 55);
        RAVEN_BUTTON_2(essentialsRemoveBarButton);
        addAndMakeVisible(essentialsRemoveBarButton);
        ravenTouchButtons.add(essentialsRemoveBarButton);

        mouseHider = new MouseHideComponent(getWidth(), getHeight());
        mouseHider->setName("MouseHider");
        addAndMakeVisible(mouseHider);
    }
    
    void setRackContainerButtonListener(Button::Listener *listener) //NEED THIS SO THE RACK CONTAINER LISTENS!!!!
    {
       essentialsCustomButton->addListener(listener);
    }
    
    void setCustomMode(bool mode) { customMode = mode; }
    bool isCustomMode() { return customMode; }
    
    bool containsModule(Component* curMovingModule)
    {
        for(int i = 0; i < getNumChildComponents(); i++)
        {
            if(getChildComponent(i)->getName() == curMovingModule->getName()) return true;
        }
        return false;
    }
    
    bool positionModule(Component* curMovingModule)
    {
        
        /*
         1. make sure module is completely within palette bounds. if not shift up and down.
            - here extend palette util max boundary is reached. 
            - if beyond max (right side), then snap back within bounds
            - if beyond left side, then shift right.
        */
        int shift = 0;
        if(curMovingModule->getScreenBounds().getRight() > getScreenX() + paletteMaxWidth - paletteMargin){
            shift = curMovingModule->getScreenBounds().getRight() - (getScreenX() + paletteMaxWidth - paletteMargin);
            curMovingModule->setTopLeftPosition(curMovingModule->getX() - shift, curMovingModule->getY());
        }
        if(curMovingModule->getScreenBounds().getBottom() > getScreenY() + paletteMaxHeight - paletteMargin){
            shift = curMovingModule->getScreenBounds().getBottom() - (getScreenY() + paletteMaxHeight - paletteMargin);
            curMovingModule->setTopLeftPosition(curMovingModule->getX(), curMovingModule->getY() - shift);
        }
        if(curMovingModule->getScreenBounds().getX() < getScreenX() + paletteMargin){
            shift = getScreenX() + paletteMargin - curMovingModule->getScreenBounds().getX();
            curMovingModule->setTopLeftPosition(curMovingModule->getX() + shift, curMovingModule->getY());
        }
        if(curMovingModule->getScreenBounds().getY() < getScreenY() + paletteMargin){
            shift = getScreenY() + paletteMargin - curMovingModule->getScreenBounds().getY();
            curMovingModule->setTopLeftPosition(curMovingModule->getX(), curMovingModule->getY() + shift);
        }
                
        //2. look for overlap. Adjust the coordinates of the component to fix it
        int trial = 0;
        int numChildrenComponents = getNumChildComponents();
        bool foundOverlap = true;
        int positionedX = curMovingModule->getX();
        int positionedY = curMovingModule->getY();
                
        while(foundOverlap && numChildrenComponents > initialNumChildren && trial < 10)
        {
            foundOverlap = false;
            
            for(int i = 0; i < getNumChildComponents(); i++)
            {
                //std::cout << "Child's Name: " << getChildComponent(i)->getName() << std::endl;
                if(getChildComponent(i)->getName() == "MouseHider") continue;
                if(getChildComponent(i)->getName() == curMovingModule->getName()) continue;
                
                Rectangle<int> curCompRect;
                curCompRect = getChildComponent(i)->getScreenBounds();
                curCompRect.setHeight(getChildComponent(i)->getProperties().getWithDefault("CustomHeight", getChildComponent(i)->getHeight()));
                
                Rectangle<int> curMovingModuleRect(curMovingModule->getScreenBounds());
                curMovingModuleRect.setHeight(curMovingModule->getProperties().getWithDefault("CustomHeight", curMovingModule->getHeight()));
                
                if(curMovingModuleRect.intersects(curCompRect))
                {
                    Rectangle<int> intersectionArea = curMovingModuleRect.getIntersection(curCompRect);
                    
                    //Check X vs. Y overlap and shift along axis of least distance.
                    if(intersectionArea.getWidth() <= intersectionArea.getHeight())
                    {
                        positionedX = (curMovingModule->getScreenBounds().getCentreX() <= curCompRect.getCentreX()) ? curMovingModule->getX() - intersectionArea.getWidth() - moduleGap: curMovingModule->getX() + intersectionArea.getWidth() + moduleGap;
                        positionedY = curMovingModule->getY();
                        
                    }else {
                        positionedY = (curMovingModule->getScreenBounds().getCentreY() <= curCompRect.getCentreY()) ? curMovingModule->getY() - intersectionArea.getHeight() - moduleGap: curMovingModule->getY() + intersectionArea.getHeight() + moduleGap;
                        positionedX = curMovingModule->getX();
                    }
                    
                    
                    //                std::cout << "Touch Name: " << curMovingModule->getName() << ", topY: " << curMovingModule->getScreenBounds().getY() << ", boty:" << curMovingModule->getScreenBounds().getBottom() << std::endl;
                    //                std::cout << "Comp Name: " << regionChildTouchComponents[i]->getName() << ", topY: " << curCompRect.getY() << ", boty:" << curCompRect.getBottom() << std::endl;
                
                    
                    curMovingModule->setTopLeftPosition(positionedX, positionedY);
                    
                    foundOverlap = true;
                }
            }
            trial++;
        }
        
        //3. if we can't fit the component into the palette, or between components, return component to original location.
        if((curMovingModule->getScreenBounds().getX() < getScreenX() + paletteMargin)
           || (curMovingModule->getScreenBounds().getRight() > getScreenX() + paletteMaxWidth - paletteMargin)
           || (curMovingModule->getScreenBounds().getY() < getScreenY() + paletteMargin)
           || (curMovingModule->getScreenBounds().getBottom() > getScreenY() + paletteMaxHeight - paletteMargin)
           || trial == 10)
        {
            return false;
        }
        
        //4. resize the palette after modules are positioned.
        resizeFloatingPalette();
        return true; //floating essentials
    }
    

    void resizeFloatingPalette()
    {
        int minLeft = 9999;
        int maxRight = 0;
        int minTop = 9999;
        int maxBottom = 0;
        
        for(int i = 0; i < getNumChildComponents(); i++)
        {
            if(getChildComponent(i)->getName() == "MouseHider") continue;
            if(getChildComponent(i)->getName() == "essentialsPaletteCustom") continue;
            if(getChildComponent(i)->getName() == "essentialsAddBarButton") continue;
            if(getChildComponent(i)->getName() == "essentialsRemoveBarButton") continue;
            
            if(getChildComponent(i)->getX() < minLeft) minLeft = getChildComponent(i)->getX();
            if(getChildComponent(i)->getRight() > maxRight) maxRight = getChildComponent(i)->getRight();
            if(getChildComponent(i)->getY() < minTop) minTop = getChildComponent(i)->getY();
            if(getChildComponent(i)->getBottom() > maxBottom) maxBottom = getChildComponent(i)->getBottom();
        }
        
        int paletteWidth = (maxRight - minLeft < initWidth) ? initWidth : maxRight - minLeft;
        int paletteHeight = (maxBottom - minTop < initHeight) ? initHeight : maxBottom - minTop;
        int moduleXShift = minLeft;
        int moduleYShift = minTop;
        int customizeButtonAdj = 0;
        
        for(int i = 0; i < getNumChildComponents(); i++)
        {
            if(getChildComponent(i)->getName() == "MouseHider") continue;
            if(getChildComponent(i)->getName() == "essentialsPaletteCustom") continue;
            if(getChildComponent(i)->getName() == "essentialsAddBarButton") continue;
            if(getChildComponent(i)->getName() == "essentialsRemoveBarButton") continue;
            
            getChildComponent(i)->setTopLeftPosition(getChildComponent(i)->getX() - moduleXShift + paletteMargin, getChildComponent(i)->getY() - moduleYShift + paletteMargin);
            
            //here check to see if intersecting cusomize buttons, if so, then shift right.
            Rectangle<int> curCompRect;
            curCompRect = getChildComponent(i)->getScreenBounds();
            curCompRect.setHeight(getChildComponent(i)->getProperties().getWithDefault("CustomHeight", getChildComponent(i)->getHeight()));
            
            Rectangle<int> customizeButtonRect(essentialsCustomButton->getScreenBounds());
            customizeButtonRect.setHeight(essentialsCustomButton->getProperties().getWithDefault("CustomHeight", essentialsCustomButton->getHeight()));
            
            Rectangle<int> addRackButtonRect(essentialsAddBarButton->getScreenBounds());
            addRackButtonRect.setHeight(essentialsAddBarButton->getProperties().getWithDefault("CustomHeight", essentialsAddBarButton->getHeight()));
            
            Rectangle<int> removeRackButtonRect(essentialsRemoveBarButton->getScreenBounds());
            removeRackButtonRect.setHeight(essentialsRemoveBarButton->getProperties().getWithDefault("CustomHeight", essentialsRemoveBarButton->getHeight()));
            
            if(customizeButtonRect.intersects(curCompRect) || addRackButtonRect.intersects(curCompRect) || removeRackButtonRect.intersects(curCompRect))
            {
                customizeButtonAdj = essentialsAddBarButton->getRight() + moduleGap;
            }
        }
        
        for(int i = 0; i < getNumChildComponents(); i++)
        {
            if(customizeButtonAdj == 0) break;
            
            if(getChildComponent(i)->getName() == "MouseHider") continue;
            if(getChildComponent(i)->getName() == "essentialsPaletteCustom") continue;
            if(getChildComponent(i)->getName() == "essentialsAddBarButton") continue;
            if(getChildComponent(i)->getName() == "essentialsRemoveBarButton") continue;

            getChildComponent(i)->setTopLeftPosition(getChildComponent(i)->getX() + customizeButtonAdj, getChildComponent(i)->getY());
        }
        
//        panelImg = panelImg.rescaled(panelImg.getWidth() + customizeButtonAdj, panelImg.getHeight(), Graphics::highResamplingQuality);
        
        panelImg = panelImg.rescaled(paletteWidth + 2*paletteMargin + customizeButtonAdj, paletteHeight + 2*paletteMargin, Graphics::highResamplingQuality);
        setSize(panelImg.getWidth(), panelImg.getHeight());
        mouseHider->setTopLeftPosition(0, 0);
        mouseHider->setSize(panelImg.getWidth(), panelImg.getHeight());
        mouseHider->toFront(false);
        mouseHider->setAlwaysOnTop(true);
        
    }
    
    
//    void dynamicallyResizePalette(Component* curMovingModule)
//    {
//        
//        int updatedWidth = curMovingModule->getScreenBounds().getRight() + paletteMargin - getScreenBounds().getX();
//        int updatedHeight = curMovingModule->getScreenBounds().getBottom() + paletteMargin - getScreenBounds().getY();
//         
//        if(updatedWidth > currentWidth && updatedWidth < paletteMaxWidth - paletteMargin){
//            updatedWidth = updatedWidth;
//        } else if(updatedWidth <= currentWidth){
//            updatedWidth = currentWidth;
//        }else if(updatedWidth >= paletteMaxWidth - paletteMargin){
//            updatedWidth = paletteMaxWidth;
//        }
//        
//        if(updatedHeight > currentHeight && updatedHeight < paletteMaxHeight - paletteMargin){
//            updatedHeight = updatedHeight;
//        } else if(updatedHeight <= currentHeight){
//            updatedWidth = currentHeight;
//        }else if(updatedHeight >= paletteMaxHeight - paletteMargin){
//            updatedHeight = paletteMaxHeight;
//        }
//        
//        panelImg = panelImg.rescaled(updatedWidth, updatedHeight);
//        setSize(panelImg.getWidth(), panelImg.getHeight());
//    }
    
    void setFloatingEssentialsButtonListener (Button::Listener *listener)
    {
    }
    
    void buttonStateChanged (Button* b)
    {
    }
    
    void buttonClicked (Button* b){}
    
    void paint(Graphics &g)
    {
        //g.fillAll(Colours::red);
        g.drawImageAt(panelImg, 0, 0);
    }
    
    RavenButton *getEssentialsCustomButton(){ return essentialsCustomButton; }
    RavenButton *getEssentialsAddBarButton(){ return essentialsAddBarButton; }
    RavenButton *getEssentialsRemoveBarButton(){ return essentialsRemoveBarButton; }
    
    void setButtonListenersForEssentials(Button::Listener *listener)
    {
        essentialsCustomButton->addListener(listener);
        essentialsAddBarButton->addListener(listener);
        essentialsRemoveBarButton->addListener(listener);
    }
    
    
    
private:
    
    bool toolbarMode, customMode;
        
    RavenButton *essentialsCustomButton;
    RavenButton *essentialsAddBarButton;
    RavenButton *essentialsRemoveBarButton;
    
    int initWidth;
    int initHeight;
    
    Image panelImg;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenFloatingEssentialsComponent)
    
};

#endif
