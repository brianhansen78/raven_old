
#ifndef Raven_XMLHandler_h
#define Raven_XMLHandler_h

#include "RavenFunctionsComponent.h"
#include "RavenRackManager.h"

#define XML_PATH File::getSpecialLocation(File::currentApplicationFile).getFullPathName() + "/Contents/Resources/Raven_User_Layouts/"

/*
 
 TO SAVE/LOAD
 - Module Y positions - getRackModules(), setModulePosition()
 - Module Rack positions - getRackPos(), setRackPos()
 - Modules visible or hidden? use isHidden() with showModule()/hidModule()
 
 - Float components/palettes (counter, inserts) - getFloatingComponents()
 
 
 => add bars/ remove bars first, then set positions..
 
replace getUnchecked with []
 
 */

class RavenSaveLayoutHandler
{
    
    
public:
    RavenSaveLayoutHandler(RavenFunctionsComponent* _functions, RavenRackManager* _rackManager) : functions(_functions), rackManager(_rackManager)
    {}
    ~RavenSaveLayoutHandler(){}
    
    
    static String getSaveLayoutXMLPath(int preset)
    {
        String path;
        path = String(XML_PATH) + "UserLayout_" + String(preset) + ".xml";
        //path = "/Users/musicmastermind/Documents/2_Brian/Work/1_Raven/raven/UserLayouts/UserLayout_" + String(preset) + ".xml";
        
        return path;
    }

    
    void loadUserLayout(int preset)
    {
        const String xmlPath = getSaveLayoutXMLPath(preset);
        const File xmlFile(xmlPath);
        XmlDocument myDocument(xmlFile);
        XmlElement* layoutElement = myDocument.getDocumentElement();
        
        //adjusmtent for small or custom palette view.
        int viewAdj = this->pannelSettingsAdjustment();
        
        //make sure the document has the element we are looking for
        if (!layoutElement->hasTagName("LAYOUT")) return;

        //iterate through and pars the child elements.
        forEachXmlChildElement (*layoutElement, funcLayout)
        {
            if (!funcLayout->hasTagName("FUNCTION")) continue;
            String funcCompName = funcLayout->getStringAttribute ("name");
                
            for (int i = 0; i < functions->getChildTouchComponents().size(); ++i)
            {
                RavenTouchComponent *curComp = functions->getChildTouchComponents().getUnchecked(i);
                if(curComp->getName() != funcCompName) continue;
                
                /////Animation Test
                
                Rectangle<int> moveToRect(funcLayout->getIntAttribute ("topleftx"), funcLayout->getIntAttribute ("toplefty"), curComp->getWidth(), curComp->getHeight());
                
                Desktop::getInstance().getAnimator().animateComponent (curComp, moveToRect, 1.0, 500, true, 0.0, 0.0);
                
                //////
                
                //unanimated
                //curComp->setTopLeftPosition(funcLayout->getIntAttribute ("topleftx"), funcLayout->getIntAttribute ("toplefty")+viewAdj);
                
                if(funcCompName.upToFirstOccurrenceOf("_", false, false) != "HotKeysComponent") continue;
                
                //find the hot key component associated with the xml hot key index.
                int hotIndex = funcLayout->getIntAttribute("hotkeyindx");
                RavenHotKeysComponent* curHotComp;
                for(int j = 0; j < functions->getHotKeyComponents().size(); j++){
                    curHotComp = functions->getHotKeyComponents().getUnchecked(j);
                    if(curHotComp->getName().getTrailingIntValue() == hotIndex) break;
                }
                
                //cycle through the button elements of the the hot key and set the attributes for each button.
                forEachXmlChildElement (*funcLayout, hotButton)
                {
                    if (!hotButton->hasTagName("HOTBUTTON")) continue;  
                    
                    int hotKeyIndex = hotButton->getIntAttribute ("labelindex");
                    String hotLabelString = hotButton->getStringAttribute ("labeltext");
                    String keyCodeString = hotButton->getStringAttribute ("keycode");
                    
                    curHotComp->setKeyLabelText(hotKeyIndex, hotLabelString);
                    
                    StringArray tokens;
                    tokens.addTokens (keyCodeString, "_", "");
                    std::vector<int> storedKeyCode; 
                    
                    for (int i=0; i<tokens.size(); i++)
                    {
                        storedKeyCode.push_back(tokens[i].getIntValue());
                    }
                    curHotComp->setKeyCode(hotKeyIndex, storedKeyCode);
                }
            }
        }

    }
    
    void saveUserLayout(int preset)
    {        
        // create an outer node
        XmlElement functionsLayout ("LAYOUT");
        
        for (int i = 0; i < functions->getChildTouchComponents().size(); ++i)
        {
            RavenTouchComponent *curComp = functions->getChildTouchComponents().getUnchecked(i);
            int curX = curComp->getX();
            int curY = curComp->getY();
            String curString = curComp->getName();
            
            // create an inner element..
            XmlElement* functionComponent = new XmlElement ("FUNCTION");
            functionComponent->setAttribute ("name", curString);
            functionComponent->setAttribute ("topleftx", curX);
            functionComponent->setAttribute ("toplefty", curY);
                        
            if(curString.upToFirstOccurrenceOf("_", false, false) == "HotKeysComponent"){
                functionComponent->setAttribute ("hotkeyindx", curString.getTrailingIntValue());
                
                //1. get the appropriate hotkey comp via index
                int hotCompIndx = curString.getTrailingIntValue();
                RavenHotKeysComponent* curHotComp = functions->getHotKeyComponents().getUnchecked(hotCompIndx);
                
                //2. cycle throught th 8 buttons and store button index, text, and key code
                for(int j = 0; j < 8; j++){
                    String labelText = curHotComp->getKeyLabelText(j);
                    std::vector<int> keyCode = curHotComp->getKeyCode(j);
                    
                    XmlElement* hotcomp = new XmlElement ("HOTBUTTON");
                    hotcomp->setAttribute ("labelindex", j);
                    hotcomp->setAttribute ("labeltext", labelText);
                    
                    //NOTE: Need to figure out the key code stuff more!!!
                    String keyCodeString = "";
                    for(int k = 0; k < keyCode.size(); k++){
                        String curKey(keyCode[k]);
                        String delimiter = (k == keyCode.size()-1) ? "" : "_";
                        keyCodeString += curKey + delimiter;
                    }
                    hotcomp->setAttribute ("keycode", keyCodeString);
                    
                    functionComponent->addChildElement(hotcomp);
                }
            }
             
            //add the child elements to the parent node
            functionsLayout.addChildElement (functionComponent);
        }
        
        //write to xml document
        String myXmlDoc = functionsLayout.createDocument (String::empty);
        const String outFile = getSaveLayoutXMLPath(preset);
        functionsLayout.writeToFile(outFile,"");
    }
            
    
    //keeps track of panel size (isSmall) and mode (isCustomMode)
    int pannelSettingsAdjustment()
    {
        int pannelAdjustment = 0;

        if(!functions->isToolbarMode() && functions->isCustomMode()){
            pannelAdjustment = functions->getSinglePanelHeight() + functions->getCustomPanelHeight();
        }else if(functions->isToolbarMode() && functions->isCustomMode()){
            pannelAdjustment = functions->getCustomPanelHeight();
        }else if(!functions->isToolbarMode() && !functions->isCustomMode()){
            pannelAdjustment = functions->getSinglePanelHeight();
        }else if(functions->isToolbarMode() && !functions->isCustomMode()){
            pannelAdjustment = 0;
        }
                
        return pannelAdjustment;
    }
    
    
private:    
    
    RavenFunctionsComponent* functions;
    RavenRackManager* rackManager;
    
};




#endif
