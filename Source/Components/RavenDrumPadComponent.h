

#ifndef Raven_RavenDrumPadComponent_h
#define Raven_RavenDrumPadComponent_h

#include "RavenButton.h"
#include "RavenTextDisplay.h"
#include "RavenWindowComponent.h"

const int SSDDrumNotes[8] = {36, 38, 48, 52, 41, 51, 55, 42};

class RavenDrumPadComponent : public RavenWindowComponent, public Button::Listener
{
public:
    
    RavenDrumPadComponent()
    {
                
        ravenMidiDevice = MidiOutput::createNewDevice("RavenDrumPad");
        
        int xPos = 0;
        int yPos = 30;
        int buttonWidth, buttonHeight;
        for(int i = 0; i < 8; i++)
        {
            RavenButton *drumPadButton = new RavenButton();
            String onFile, offFile;
            onFile = "PadButtons/pad_button_on.png";
            offFile = "PadButtons/pad_button_off.png";
            drumPadButton->setImagePaths(onFile, offFile);
            buttonWidth = drumPadButton->getWidth();
            buttonHeight = drumPadButton->getHeight();
            
            if(i == 4)
            {
                xPos = 0;
                yPos += buttonHeight;
            }
            drumPadButton->setTopLeftPosition(xPos, yPos);
            drumPadButton->addListener(this);
            drumPadButton->getProperties().set("midiNote", i);
            
            String padText(i);
            //drumPadButton->setName("D");
            addAndMakeVisible(drumPadButton);
            ravenTouchButtons.add(drumPadButton);
            xPos += drumPadButton->getWidth();
            
        }
        
        setSize(4*buttonWidth, 2*buttonHeight+30);
        
        mouseHider = new MouseHideComponent(getWidth(), getHeight());
        addAndMakeVisible(mouseHider);
        
        //setSize(200, 200);

    }
    
    
    void setDrumPadButtonListener (Button::Listener *listener)
    {
        for(int i = 0; i < ravenTouchButtons.size(); i++) ravenTouchButtons[i]->addListener(listener);
    }
    
    void buttonStateChanged (Button*)
    {
    }

    void buttonClicked (Button* b)
    {
        float ratioToPadHeight = b->getProperties().getWithDefault("ratioToTop", 0.0f);
        int noteIndex = b->getProperties().getWithDefault("midiNote", 0);
        int note = SSDDrumNotes[noteIndex];//rand()%50 + 25;
        MidiMessage ravenDrumPadMessage = MidiMessage::noteOn(1, note, ratioToPadHeight);
        ravenMidiDevice->sendMessageNow(ravenDrumPadMessage);
    }
    
    void paint(Graphics &g)
    {
        g.fillAll(Colours::red);
    }
    
private:

    ScopedPointer<MidiOutput> ravenMidiDevice;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenDrumPadComponent)
    
};

#endif
