
#ifndef Raven_NewSDK_RavenSendsBankComponent_h
#define Raven_NewSDK_RavenSendsBankComponent_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenSendsChannelComponent.h"
#include "NscGuiElement.h"

class RavenSendsBankComponent : public Component, public NscGuiContainer, public Slider::Listener,  public Button::Listener
{
public:
    RavenSendsBankComponent(int numChans, int bankNum) : NscGuiContainer(bankNum)
    {
        setSize (0, 0);
        int xOffset = 0;
        
        for(int i = 0; i < numChans; i++)
        {
             RavenSendsChannelComponent *sendChannel = new RavenSendsChannelComponent(this, bankNum, i);
             addAndMakeVisible(sendChannel);
             sendChannel->setTopLeftPosition(xOffset, 0);
             sendChannel->setChannelAndBankIndex(i, bankNum);
             sendChannel->addSliderListener(this);
             sendChannel->addButtonListener(this);
             sends.add(sendChannel);
            
            xOffset += sendChannel->getWidth();
            setSize(xOffset, sendChannel->getHeight());
        }
        
        //
        aButtonNSC = new RavenNSCButton(this);
        aButtonNSC->setChannelIndex(kNscGuiBankGlobalIdentifier);
        aButtonNSC->setBankIndex(bankNum);
        aButtonNSC->setTag(eNSCGSPTBankMode_SendA);
        aButtonNSC->addListener(this);
        
        bButtonNSC = new RavenNSCButton(this);
        bButtonNSC->setChannelIndex(kNscGuiBankGlobalIdentifier);
        bButtonNSC->setBankIndex(bankNum);
        bButtonNSC->setTag(eNSCGSPTBankMode_SendB);
        bButtonNSC->addListener(this);
        
        cButtonNSC = new RavenNSCButton(this);
        cButtonNSC->setChannelIndex(kNscGuiBankGlobalIdentifier);
        cButtonNSC->setBankIndex(bankNum);
        cButtonNSC->setTag(eNSCGSPTBankMode_SendC);
        cButtonNSC->addListener(this);
        
        dButtonNSC = new RavenNSCButton(this);
        dButtonNSC->setChannelIndex(kNscGuiBankGlobalIdentifier);
        dButtonNSC->setBankIndex(bankNum);
        dButtonNSC->setTag(eNSCGSPTBankMode_SendD);
        dButtonNSC->addListener(this);
        
        eButtonNSC = new RavenNSCButton(this);
        eButtonNSC->setChannelIndex(kNscGuiBankGlobalIdentifier);
        eButtonNSC->setBankIndex(bankNum);
        eButtonNSC->setTag(eNSCGSPTBankMode_SendE);
        aButtonNSC->addListener(this);
    }
    
    void enterFlipMode()
    {
        for(int i = 0; i < sends.size(); i++) sends[i]->enterFlipMode();
    }
    void exitFlipMode()
    {
        for(int i = 0; i < sends.size(); i++) sends[i]->exitFlipMode();
    }
    
    void setFinePan(bool fp)
    {
        for(int i = 0; i < sends.size(); i++) sends[i]->setFinePan(fp);
    }
    
    void setFineFader(bool ff)
    {
        for(int i = 0; i < sends.size(); i++) sends[i]->setFineFader(ff);
    }
    
    // Slider::Listener interface:
	void 	sliderValueChanged (Slider *slider)
    {
        NscGuiElement * element = dynamic_cast<NscGuiElement *>(slider);
        if (!element)
        {
            DBG("Faders and vpots must inherit and set up NscGuiElement");
            jassertfalse;
        }
        else
            sliderChanged(element, (float) slider->getValue());
    }
	void 	sliderDragStarted (Slider *slider)
    {
        NscGuiElement * element = dynamic_cast<NscGuiElement *>(slider);
        if (!element)
        {
            DBG("Faders and vpots must inherit and set up NscGuiElement");
            jassertfalse;
        }
        else
            sliderBegan(element);
    }
	void 	sliderDragEnded (Slider *slider)
    {
        NscGuiElement * element = dynamic_cast<NscGuiElement *>(slider);
        if (!element)
        {
            DBG("Faders and vpots must inherit and set up NscGuiElement");
            jassertfalse;
        }
        else
            this->sliderEnded(element);
    }
    
    void addButtonListener(Button::Listener *listener)
    {
        for(int i = 0; i < sends.size(); i++) sends[i]->addButtonListener(listener);
    }
    
    // Button::Listener interface
	void	buttonStateChanged (Button*){}
	void	buttonClicked (Button* b)
    {
        NscGuiElement * element = dynamic_cast<NscGuiElement *>(b);
        if (!element)
        {
            // try our temporary workaround hack.
            int ptr = b->getProperties()[kNscGuiIdentifier];
            element = (NscGuiElement *) ptr;
            if (!element)
            {
                DBG("Switches must inherit and set up NscGuiElement");
                jassertfalse;
            }
        }
        if (element)
        {
            switchClicked(element, true);
        }
        
    }
    
    void resized()
    {
        for (int i=0; i<sends.size();i++)
            sends[i]->setSize(sends[i]->getWidth(), getHeight());
    }
    
    void visibilityChanged()
    {
        for (int i=0; i<sends.size();i++)
            sends[i]->setVisible(isVisible());
    }
    
//    void setMeterMode(bool _meterMode)
//    {
//        for (int i=0; i<sends.size();i++)
//            sends[i]->setMeterMode(_meterMode);
//    }
    
    void setGlobalSendChannel(Button *sendButtonPushed)
    {
        if(sendButtonPushed->getName() == "SendA")
            aButtonNSC->triggerClick();
        else if(sendButtonPushed->getName() == "SendB")
            bButtonNSC->triggerClick();
        else if(sendButtonPushed->getName() == "SendC")
            cButtonNSC->triggerClick();
        else if(sendButtonPushed->getName() == "SendD")
            dButtonNSC->triggerClick();
        else if(sendButtonPushed->getName() == "SendE")
            eButtonNSC->triggerClick();
    }
    
    RavenSendsChannelComponent* getChannel(int num){return sends[num];}

    
private:
    OwnedArray<RavenSendsChannelComponent> sends;
    
    ScopedPointer<RavenNSCButton> aButtonNSC;
    ScopedPointer<RavenNSCButton> bButtonNSC;
    ScopedPointer<RavenNSCButton> cButtonNSC;
    ScopedPointer<RavenNSCButton> dButtonNSC;
    ScopedPointer<RavenNSCButton> eButtonNSC;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenSendsBankComponent)
    
};

#endif
