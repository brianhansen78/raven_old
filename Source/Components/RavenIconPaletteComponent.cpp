//
//  RavenIconPaletteComponent.cpp
//  Raven
//
//  Created by Joshua Dickinson on 6/6/13.
//
//

#include "RavenIconPaletteComponent.h"
#include "RavenRackContainer.h"

void RavenIconPaletteComponent::buttonClicked (Button* b)
{
    if(b == testButton)
    {
        //setVisible(false);
        getWindow()->removeFromDesktop();
        deActivateIcon();
    }
    else
    {
        for(int i = 0;i<iconButtons.size();i++)
        {
            if(b == iconButtons[i])
            {
                deActivateIcon();
                if(b->getToggleState())
                {
                    activeIcon = iconButtons[i];
                }
            }
        }
    }
}

void RavenIconPaletteComponent::convertIndexMapToIconMap(HashMap<String, int> *_indexMap)
{
    HashMap<String, int>::Iterator i(*_indexMap);
    HashMap<String, Image> iconMap;
    while (i.next())
    {
        iconMap.set(i.getKey(), iconButtons[i.getValue()-1]->getNormalImage());
    }
    RAVEN->getTrackNames()->loadIconMap(&iconMap);
}