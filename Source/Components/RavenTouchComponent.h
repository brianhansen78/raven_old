//
//  RavenTouchComponent.h
//  
//  Inherited by Raven components that need to be touched
//  Allows for one fader, one knob, and any numbers of buttons

#ifndef Raven_NewSDK_TouchComponent_h
#define Raven_NewSDK_TouchComponent_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "MTDefines.h"
#include "RavenConfig.h"
#include "RavenFader.h"
#include "RavenKnob.h"
#include "RavenButton.h"
#include "RavenMessage.h"

/*
typedef enum
{
    eAsyncUpdateFader,
    eAsyncUpdateKnob,    
} TouchUpdateType;
*/

class RavenTouchComponent : public Component, public MessageListener
{
public:
    RavenTouchComponent()
    :
    faderTouchYOffset(0),
    knobStartVal(0),
    finePan(false),
    fineFader(false),
    twoFingerFader(false),
    disableTouch(false)
    {}
    ~RavenTouchComponent()
    {}
    
    void handleMessage (const Message &message)
    {
        RavenMessage *m = (RavenMessage*)&message;
        if      (m->type == eUpdateFader) fader->setValue(m->val);
        else if (m->type == eUpdateKnob)  knob->setValue(m->val);
    }
    
    static bool touchTimeLessThan(long &thisSec, long &thisUSec, long &lastSec, long &lastUSec, long timeUSec)
    {
        if(thisSec == lastSec && (thisUSec - lastUSec) <= timeUSec) //last - this
        {
            return true;
        }
        else if(thisSec == (lastSec+1)) //-1
        {
            long uSecDiff = (1000000 - lastUSec) + thisUSec;
            if(uSecDiff <= timeUSec)
            {
                return true;
            }
        }
        return false;
    }
    
    bool checkSwipeTouch(int touchX, int touchY)
    {
        return (componentTouched(fader, touchX, touchY, false) && !checkFaderTouch(touchX, touchY) && !fader->isBeingTouched);
    }
    
    bool checkFaderTouch(TUIO::TuioCursor *tcur)
    {
        int touchX = tcur->getScreenX(SCREEN_WIDTH);
        int touchY = tcur->getScreenY(SCREEN_HEIGHT);
        return checkFaderTouch(touchX, touchY);
    }
    
    bool checkFaderTouch(int touchX, int touchY)
    {
        if(!fader->isShowing() || disableTouch || !fader->isEnabled()) return false;
        
        /*
        int thumbCenterX = fader->getScreenX() + fader->getWidth()/2;
        int thumbX1 = thumbCenterX - 20;
        int thumbX2 = thumbCenterX + 30; //20
        */
        
        int thumbX1 = fader->getScreenX();
        int thumbX2 = fader->getScreenX() + fader->getWidth();
        
        int faderCenterY = fader->getCapCenterScreenY();
        int halfCapHeight = FADER_HALF_HEIGHT_32;
        if(RAVEN_24) halfCapHeight= FADER_HALF_HEIGHT_24;
        //if(RAVEN->isHybridMode()) halfCapHeight = FADER_HALF_HEIGHT_32_HYBRID;
        int thumbY1 = faderCenterY - halfCapHeight;
        int thumbY2 = faderCenterY + halfCapHeight;
        
        if (touchX >= thumbX1-FADER_X_SLACK && touchX <= thumbX2+FADER_X_SLACK && touchY >= thumbY1 && touchY <= thumbY2)
        {
            if(fader->isBeingTouched)
            {
                twoFingerFader = true;
                return false;
            }
            
            faderTouchYOffset =  touchY - faderCenterY;
            
            return true;
        }
        else
            return false;
    }
    
    void updateFaderTouch(TUIO::TuioCursor *tcur)
    {
        if(disableTouch || !fader->isEnabled()) return;
            
        //*//////////////////////////////////////////////
        // moving threshold filter to prevent final hop
        // if history of moving quickly, then moving quickly is ok
        // if history of slower movement, then prevent sudden fast movements ("hops")
        
        const float motionAccelMag = fabs(tcur->getMotionAccel());
        
        const int numToAvg = 20; //probably no more than 40 works well with G4S
        
        int numVals = prevAccels.size();
        float sum = 0;
        for(int i = 0; i < numVals; i++) sum += prevAccels[i];
        float runningAvg = 0;
        if(numVals > 0) runningAvg = sum / numVals;
        
        //printf("running avg accel = %f\n", runningAvg);
        //printf("motion accel = %f\n\n", motionAccelMag);
        
        prevAccels.insert(0, motionAccelMag);
        if(prevAccels.size() > numToAvg) prevAccels.removeLast();
        
        //float thresh = powf(runningAvg, 1.0);   //exponential scaling
        float thresh = 1.0*runningAvg;        //or, linear scaling
        if(motionAccelMag >= thresh && numVals == numToAvg) return; // prevent final hop upon release
        ////////////////////////////////////////////*/
        
        int touchY = tcur->getScreenY(SCREEN_HEIGHT) - faderTouchYOffset;

        int trackY1 = fader->getTrackTopScreenY();
        int trackY2 = trackY1 + fader->getTrackLength();
        
        int faderY1 = fader->getScreenY();
        int faderY2 = faderY1 + fader->getHeight();
        
        float currentVal = float(fader->getValue());
        float newVal = currentVal;
        if(fineFader || twoFingerFader)
        {
            float ySpeed = -1*tcur->getYSpeed();
            newVal = currentVal + 0.005*ySpeed;
        }
        else if(touchY >= faderY1 && touchY <= faderY2)//Normal
        {
            newVal = float(trackY2 - touchY) / (float)(fader->getTrackLength());
        }

        float filterCoeff = FADER_SMOOTHING;
        // smaller coefficient is faster movement, higher is smoother (heaiver)
        //filterCoeff += 1.f - tcur->getYSpeed();
        
        float filterVal = newVal + filterCoeff*(currentVal- newVal);
        //std::cout << "val = " << newVal << std::endl;
        
        if(filterVal < 0) filterVal = 0;
        else if(filterVal > 1) filterVal = 1;

        //Thread-safe update
        //RavenMessage *m = new RavenMessage(eUpdateFader, filterVal);
        //postMessage(m);
        
        //update fader thread
        fader->updateTouchValue(filterVal);

    }
    
    void releaseFaderTouch()
    {
        twoFingerFader = false;
    }
    
    bool checkKnobTouch(TUIO::TuioCursor *tcur)
    {
        if(!knob->isShowing() || disableTouch || !knob->isEnabled()) return false;
        
        int knobX = knob->getScreenX();
        int knobXCenter = knobX + knob->getWidth()/2;
        int knobY = knob->getScreenY();
        int knobYCenter = knobY + knob->getHeight()/2 - KNOB_CENTER_OFFSET;

        float knobXCenterNorm = (float)knobXCenter/(float)SCREEN_WIDTH;
        float knobYCenterNorm = (float)knobYCenter/(float)SCREEN_HEIGHT;
        float knobRadiusNorm = float(knob->getWidth()/2)/float(SCREEN_WIDTH);


        if(tcur->getDistance(knobXCenterNorm, knobYCenterNorm) <= KNOB_TOUCH_RAD_FACTOR*knobRadiusNorm)
        {
            knobStartVal = knob->getValue();
            knob->syncPanning();
            return true;
        }
        else
            return false;
    }
    
    bool checkKnobTouch(int touchX, int touchY)
    {
        if(!isVisible() || disableTouch || !knob->isEnabled()) return false;
        
        int knobX = knob->getScreenX();
        int knobXCenter = knobX + knob->getWidth()/2;
        int knobY = knob->getScreenY();
        int knobYCenter = knobY + knob->getHeight()/2 - KNOB_CENTER_OFFSET;
        
        float knobXCenterNorm = (float)knobXCenter/(float)SCREEN_WIDTH;
        float knobYCenterNorm = (float)knobYCenter/(float)SCREEN_HEIGHT;
        float knobRadiusNorm = float(knob->getWidth()/2)/float(SCREEN_WIDTH);
        
        float dx = touchX-knobXCenterNorm;
        float dy = touchY-knobYCenterNorm;
        float distance = sqrtf(dx*dx+dy*dy);
        
        if(distance <= KNOB_TOUCH_RAD_FACTOR*knobRadiusNorm)
        {
            knobStartVal = knob->getValue();
            knob->syncPanning();
            return true;
        }
        else
            return false;
    }
    
    void updateKnobTouch(TUIO::TuioCursor *tcur)
    {
        if(disableTouch || !knob->isEnabled()) return;
        
        if(!finePan)
        {
            int knobX = knob->getScreenX();
            int knobXCenter = knobX + knob->getWidth()/2;
            int knobY = knob->getScreenY();
            int knobYCenter = knobY + knob->getHeight()/2;
            
            float knobXCenterNorm = (float)knobXCenter/(float)SCREEN_WIDTH;
            float knobYCenterNorm = (float)knobYCenter/(float)SCREEN_HEIGHT;
            //float knobRadiusNorm = float(knob->getWidth()/2)/float(SCREEN_WIDTH);
            
            //disable this check for infinite distance rotation
            //if(tcur->getDistance(knobXCenterNorm, knobYCenterNorm) <= KNOB_ROT_RAD_FACTOR*knobRadiusNorm)
            {
                float currentValue = knob->getValue();
                float angleDegrees = RAD2DEG * tcur->getAngle(knobXCenterNorm, knobYCenterNorm);
                angleDegrees = 360 - angleDegrees + 90;
                if(angleDegrees >= 360) angleDegrees -= 360;
                
                //good panning angles go from ~20-340 degrees, which equals 320 degrees of movement and 40 non-pannable degrees.
                static float nonPannableDegrees = 50.0;
                
                angleDegrees = (angleDegrees - (nonPannableDegrees/2.0))/(360.0-nonPannableDegrees);
                float newValue = angleDegrees;

                if(fabs(currentValue - newValue) > 0.5) return; //prevent 0 hop
                
                float filterVal = newValue + 0.44*(currentValue - newValue);
                
                //Thread-safe update
                RavenMessage *m = new RavenMessage(eUpdateKnob, filterVal);
                postMessage(m);
            }
        }
        
        else //fine pan (up/down movement)
        {
            int knobY = knob->getScreenY();
            int knobYCenter = knobY + knob->getHeight()/2;            
            float knobYCenterNorm = (float)knobYCenter/(float)SCREEN_HEIGHT;
            
            float currentValue = knob->getValue();

            float newValue = knobStartVal + 4*(knobYCenterNorm - tcur->getY());
            
            if(newValue < 0) newValue = 0;
            else if(newValue > 1) newValue = 1;
            
            float filterVal = newValue + 0.88*(currentValue - newValue);
                
            //Thread-safe update
            RavenMessage *m = new RavenMessage(eUpdateKnob, filterVal);
            postMessage(m);
        }
        
    }
    
    static bool componentTouched(Component *cmp, TUIO::TuioCursor *tcur, bool isButtonWithShadow = false, int xOffset = 0, int yOffset = 0, int xTrim = 0, int yTrim = 0)
    {
        int touchX = tcur->getScreenX(SCREEN_WIDTH);
        int touchY = tcur->getScreenY(SCREEN_HEIGHT);
        return componentTouched(cmp, touchX, touchY, isButtonWithShadow, xOffset, yOffset, xTrim, yTrim);
    }
    
    static bool componentTouched(Component *cmp, int touchX, int touchY, bool isButtonWithShadow = false, int xOffset = 0, int yOffset = 0, int xTrim = 0, int yTrim = 0)
    {
        if(!cmp->isShowing() || !cmp->isEnabled()) return false; //return false if component is not visible (for popup components)
                
        //Trim components with shadows
        if(yTrim == 0)
            yTrim = cmp->getHeight() - (int)cmp->getProperties().getWithDefault("CustomHeight", cmp->getHeight());
        
        int X = cmp->getScreenX() + xOffset;
        int X2 = cmp->getScreenX() + cmp->getWidth() - xTrim;
        int Y = cmp->getScreenY() + yOffset;
        //int Y2 = cmp->getScreenY() + cmp->getHeight() - yTrim;
        //* //old using isButtonWithShadow bool
        int Y2 = cmp->getScreenY() + cmp->getHeight() - yTrim;
//        if(isButtonWithShadow)      Y2 = cmp->getScreenY() + BUTTON_TOUCH_HEIGHT_RATIO*cmp->getHeight() - yTrim;
//        else                        Y2 = cmp->getScreenY() + cmp->getHeight() - yTrim;
        //*/
        
        if(touchX >= X && touchX <= X2 && touchY >= Y && touchY <= Y2)
        {
            //use for drum pad only, disable for now
            //float rTop = 1 - (float)(touchY - Y)/(float)(Y2 - Y);
            //cmp->getProperties().set("ratioToTop", rTop);
            return true;
        }
        else
        {
            return false;
        }
    }
    
    bool checkButtonTouch(TUIO::TuioCursor *tcur, bool ignoreIsVisible = false)
    {
        int touchX = tcur->getScreenX(SCREEN_WIDTH);
        int touchY = tcur->getScreenY(SCREEN_HEIGHT);
        return checkButtonTouch(touchX, touchY, ignoreIsVisible);
    }
    
    bool checkButtonTouch(int touchX, int touchY, bool ignoreIsVisible = false)
    {
        if((!isVisible() && !ignoreIsVisible)|| disableTouch) return false;
        
        ////////////////////////////////////////
        //new implementation 6/25/2013
        for(int i = 0; i < getNumChildComponents(); i++)
        {
            Component* child = getChildComponent(i);
            
            //for all children, check if they are child touch components, NSC buttons, or Raven buttons(regular, hot keys or modifiers)
            RavenTouchComponent *touchComp = dynamic_cast<RavenTouchComponent*>(child);
            if(touchComp != nullptr) // if it is a child touch component
            {
                if(touchComp->isShowing() && touchComp->checkButtonTouch(touchX, touchY))   return true;
                else                                                                        continue;
            }
            
            //important to check RavenNSCButtons first because they could also be cast to RavenButtons
            RavenNSCButton *nscButton = dynamic_cast<RavenNSCButton*>(child);
            if(nscButton != nullptr) // if it is a nsc button 
            {
                if(componentTouched(nscButton, touchX, touchY, true) && nscButton->enabled && nscButton->isShowing())
                {
                    LOCK_JUCE_THREAD nscButton->triggerClick();
                    return true;
                }
                else    continue; //if it is an NSC button, but not touched, then don't both checking if it is a raven button
            }
            
            RavenButton *button = dynamic_cast<RavenButton*>(child);
            if(button != nullptr) // if it is a raven button
            {
                bool isHotKey = button->getProperties().getWithDefault("isHotKey", false);
                bool isModifier = button->getProperties().getWithDefault("isModifier", false);
                if(componentTouched(button, touchX, touchY, true) && button->enabled && button->isShowing())
                {
                    if(isHotKey)
                    {
                        bool isBeingProgrammed = button->getProperties().getWithDefault("isBeingProgrammed", false);
                        LOCK_JUCE_THREAD
                        {
                            if(!isBeingProgrammed)
                            {
                                button->setToggleState(true, false);
                            }
                            else
                            {
                                button->triggerClick();
                            }
                        }
                        return true;
                    }
                    else if(isModifier)
                    {
                        LOCK_JUCE_THREAD ravenTouchModifiers[i]->setToggleState(!ravenTouchModifiers[i]->getToggleState(), false);
                        return true;
                    }
                    else //regular raven button
                    {
                        LOCK_JUCE_THREAD button->triggerClick();
                        return true;
                    }
                }
            }
        } //end for all children
        //end new implementation
        ///////////////////////////////////////
        
        /*// old implentation
         
         //check child touch components' buttons
         for(int i = 0; i < childTouchComponents.size(); i++)
         {
         //printf("child component name %s\n", (const char*)childTouchComponents[i]->getName().toUTF8());
            if(childTouchComponents[i]->isShowing() && childTouchComponents[i]->checkButtonTouch(touchX, touchY)) return true;
         }
         
        //check NSC buttons
        for(int i = 0; i < ravenNSCTouchButtons.size(); i++)
        {
            if(componentTouched(ravenNSCTouchButtons[i], touchX, touchY, true) && ravenNSCTouchButtons[i]->enabled && ravenNSCTouchButtons[i]->isShowing())
            {
                //printf("right before trigger click\n");
                LOCK_JUCE_THREAD ravenNSCTouchButtons[i]->triggerClick();
                //printf("right after trigger click\n");
                return true;
            }
        }
        
        //check Raven buttons
        for(int i = 0; i < ravenTouchButtons.size(); i++)
        {
            if(componentTouched(ravenTouchButtons[i], touchX, touchY, true) && ravenTouchButtons[i]->enabled && ravenTouchButtons[i]->isShowing())
            {
                //printf("is showing %d, %d\n", ravenTouchButtons[i]->isShowing(), i);
                LOCK_JUCE_THREAD ravenTouchButtons[i]->triggerClick();
                return true;
            }
        }
        
        //check Raven hot keys
        for(int i = 0; i < ravenTouchHotKeys.size(); i++)
        {
            if(componentTouched(ravenTouchHotKeys[i], touchX, touchY, false) && ravenTouchHotKeys[i]->enabled)
            {
                //ravenTouchHotKeys[i]->triggerClick();
                //LOCK_JUCE_THREAD ravenTouchHotKeys[i]->setToggleState(true, false);
                
                bool isBeingProgrammed = ravenTouchHotKeys[i]->getProperties().getWithDefault("isBeingProgrammed", false);
                LOCK_JUCE_THREAD
                {
                    if(!isBeingProgrammed)
                    {
                        ravenTouchHotKeys[i]->setToggleState(true, false);
                    }
                    else
                    {
                        //ravenTouchHotKeys[i]->getProperties().set("isBeingProgrammed", false);
                        ravenTouchHotKeys[i]->triggerClick();
                    }
                }
                
                return true;
            }
        }
        
        //check modifers
        for(int i = 0; i < ravenTouchModifiers.size(); i++)
        {
            if(componentTouched(ravenTouchModifiers[i], touchX, touchY, true) && ravenTouchModifiers[i]->enabled)
            {
                LOCK_JUCE_THREAD ravenTouchModifiers[i]->setToggleState(!ravenTouchModifiers[i]->getToggleState(), false);
                return true;
            }
        }
         */
        
        return false;
    }
    
    Button* checkHoldButtonTouch(TUIO::TuioCursor *tcur)
    {
        int touchX = tcur->getScreenX(SCREEN_WIDTH);
        int touchY = tcur->getScreenY(SCREEN_HEIGHT);
        return checkHoldButtonTouch(touchX, touchY);
    }
    
    Button* checkHoldButtonTouch(int touchX, int touchY)
    {
        if(!isVisible()|| disableTouch) return 0;
        for(int i = 0; i < getNumChildComponents(); i++)
        {
            Component* child = getChildComponent(i);
            //for all children, check if they are child touch components, NSC buttons, or Raven buttons(regular, hot keys or modifiers)
            RavenTouchComponent *touchComp = dynamic_cast<RavenTouchComponent*>(child);
            if(touchComp != nullptr) // if it is a child touch component
            {
                if(touchComp->isShowing() && touchComp->checkHoldButtonTouch(touchX, touchY) != 0)   return touchComp->checkHoldButtonTouch(touchX, touchY);
                else                                                                        continue;
            }
            

            
            //important to check RavenNSCButtons first because they could also be cast to RavenButtons
            RavenNSCButton *nscButton = dynamic_cast<RavenNSCButton*>(child);
            if(nscButton != nullptr) // if it is a nsc button
            {
                for(int j = 0;j < ravenHoldButtons.size();j++)
                {
                    if((Button*)nscButton == ravenHoldButtons[j])
                    {
                        if(componentTouched(nscButton, touchX, touchY, true) /*&& nscButton->enabled*/ && nscButton->isShowing())
                            return nscButton;
                        else    continue; //if it is an NSC button, but not touched, then don't both checking if it is a raven button
                    }
                }
            }

            RavenButton *button = dynamic_cast<RavenButton*>(child);
            if(button != nullptr) // if it is a raven button
            {
                for(int j = 0;j < ravenHoldButtons.size();j++)
                {
                    if((Button*)button == ravenHoldButtons[j])
                    {
                        if(componentTouched(button, touchX, touchY, true) /*&& button->enabled*/ && button->isShowing())
                            return button;
                    }
                }
            }
        } //end for all children
        return 0;//return null pointer
    }
    
    
    void releaseHotKeyTouch(TUIO::TuioCursor *tcur)
    {
        //check Raven hot keys
        for(int i = 0; i < ravenTouchHotKeys.size(); i++)
        {
            if(componentTouched(ravenTouchHotKeys[i], tcur, false) && ravenTouchHotKeys[i]->getToggleState())
            {
                bool isBeingProgrammed = ravenTouchHotKeys[i]->getProperties().getWithDefault("isBeingProgrammed", false);
                LOCK_JUCE_THREAD
                {
                    //ravenTouchHotKeys[i]->setToggleState(false, false);
                    if(!isBeingProgrammed)
                    {
                        ravenTouchHotKeys[i]->triggerClick();
                        ravenTouchHotKeys[i]->setToggleState(false, false);
                    }
                }
            }
        }
        
        /*//check modifiers
        for(int i = 0; i < ravenTouchModifiers.size(); i++)
        {
            if(componentTouched(ravenTouchModifiers[i], tcur, false) && ravenTouchModifiers[i]->getToggleState())
            {
                TUIO::TuioTime startTime = tcur->getStartTime();
                long startSec = startTime.getSeconds();
                long startUSec = startTime.getMicroseconds();
                
                TUIO::TuioTime thisTime = tcur->getTuioTime();
                long thisSec = thisTime.getSeconds();
                long thisUSec = thisTime.getMicroseconds();
                
                //if held for over 1 second then release, otherwise it should stay toggled
                if(!touchTimeLessThan(thisSec, thisUSec, startSec, startUSec, MT_MOD_HOLD_WAIT_USEC))
                {
                    LOCK_JUCE_THREAD ravenTouchModifiers[i]->setToggleState(false, false);
                }
            }
        }
        //*/
        
        //check child touch components' hot keys
//        for(int i = 0; i < childTouchComponents.size(); i++)
//        {
//            childTouchComponents[i]->releaseHotKeyTouch(tcur);
//        }
        //new implementation 6/25/2013
        for(int i = 0; i < getNumChildComponents(); i++)
        {
            //for all children, check if they are child touch components
            RavenTouchComponent *touchComp = dynamic_cast<RavenTouchComponent*>(getChildComponent(i));
            if(touchComp != nullptr) // if it is a child touch component
            {
                touchComp->releaseHotKeyTouch(tcur);
            }
        }
    }
    
    void setFinePan(bool fp){finePan = fp;}
    
    bool isFineFader() { return fineFader; }
    
    RavenKnob *getKnob(){return knob;}
    RavenFader *getFader(){return fader;}
    
    void disableTouches()  {disableTouch = true;}
    void enableTouches()   {disableTouch = false;}
    bool isTouchDisabled() {return disableTouch;}
    
protected:
    
    ScopedPointer<RavenFader> fader;
    ScopedPointer<RavenKnob> knob;
    
    int faderTouchYOffset;
    float knobStartVal;
    bool finePan, fineFader, twoFingerFader;
    
    bool disableTouch;
    
    OwnedArray<RavenNSCButton> ravenNSCTouchButtons;
    OwnedArray<RavenButton> ravenTouchButtons;
    OwnedArray<RavenButton> ravenTouchHotKeys;
    OwnedArray<RavenButton> ravenTouchModifiers;
    //Array<RavenTouchComponent*> childTouchComponents;
    
    Array<float> prevAccels;
    
    Array<Button*> ravenHoldButtons;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenTouchComponent)
};

#endif
