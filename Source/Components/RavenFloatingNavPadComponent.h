//
//  RavenFloatingNavPadComponent.h
//  Raven
//
//  Created by Joshua Dickinson on 2/24/13.
//

#ifndef Raven_RavenFloatingNavPadComponent_h
#define Raven_RavenFloatingNavPadComponent_h

#include "RavenFunctionsComponent.h"
#include "RavenNavPadComponent.h"
#include "RavenWindowComponent.h"
#include "RavenButton.h"
#include "MTDefines.h"
#include <algorithm>

#define FLOATING_NAV_PAD_GROUP_ID 27

class RavenFloatingNavPadComponent : public RavenWindowComponent, public Button::Listener, public ActionListener
{
public:
    RavenFloatingNavPadComponent(RavenFunctionsContainer *owner, RavenNavPadComponent *_masterNavPad) : sumXSpeed(0), sumYSpeed(0), sumCount(0), mouseStartX(0), mode(eNavPadTrackZoom)
    {
        //Raven Buttons
        waveformButton = new RavenButton();
        waveformButton->setName("waveformButon");
        waveformButton->setRadioGroupId(FLOATING_NAV_PAD_GROUP_ID);
        waveformButton->setClickingTogglesState(true);
        waveformButton->setTopLeftPosition(0, 0);
        waveformButton->addListener(this);
        waveformButton->setTopLeftPosition(374, 172);
        waveformButton->setImagePaths("FloatingNavpad/ButtonOff/Bt-WaveForm-On.png", "FloatingNavpad/ButtonOff/Bt-WaveForm-Off.png");
        waveformButton->setName("waveformButton");
        ravenTouchButtons.add(waveformButton);
        
        //the nav pad itself
        navPadImgCmp = new RavenNavPadImageComponent(true);
            navPadImgCmp->setTopLeftPosition(0, 0);
        addAndMakeVisible(navPadImgCmp);
        
        addAndMakeVisible(waveformButton);//make waveform button visible AFTER the nav pad so that it goes on top
        
        scrubButton = new RavenButton();
    scrubButton->addListener(this);
        scrubButton->setRadioGroupId(FLOATING_NAV_PAD_GROUP_ID);
        scrubButton->setTopLeftPosition(81, 172);
        scrubButton->setImagePaths("FloatingNavpad/ButtonOn/Bt-Scrub-On.png", "FloatingNavpad/ButtonOff/Bt-Scrub-Off.png");
        ravenTouchButtons.add(scrubButton);
        scrubButton->setClickingTogglesState(true);
        addAndMakeVisible(scrubButton);
        
        shuttleButton = new RavenButton();
        shuttleButton->addListener(this);
        shuttleButton->setRadioGroupId(FLOATING_NAV_PAD_GROUP_ID);
        shuttleButton->setTopLeftPosition(154, 172);
        shuttleButton->setImagePaths("FloatingNavpad/ButtonOn/Bt-Shuttle-On.png", "FloatingNavpad/ButtonOff/Bt-Shuttle-Off.png");
        ravenTouchButtons.add(shuttleButton);
        shuttleButton->setClickingTogglesState(true);
        addAndMakeVisible(shuttleButton);
        
        trackzoomButton = new RavenButton();
        trackzoomButton->setRadioGroupId(FLOATING_NAV_PAD_GROUP_ID);
        trackzoomButton->setName("trackzoomButton");
        trackzoomButton->setClickingTogglesState(true);
        trackzoomButton->addListener(this);
        trackzoomButton->setTopLeftPosition(302, 172);
        trackzoomButton->setImagePaths("FloatingNavpad/ButtonOn/Bt-TrackSize-On.png", "FloatingNavpad/ButtonOff/Bt-TrackSize-Off.png");
        ravenTouchButtons.add(trackzoomButton);
        addAndMakeVisible(trackzoomButton);
            setSize(navPadImgCmp->getWidth(), navPadImgCmp->getHeight());
        
        hideLeftButton = new RavenButton();
        hideLeftButton->setClickingTogglesState(true);
        hideLeftButton->setTopLeftPosition(0, 0);
        hideLeftButton->addListener(this);
        hideLeftButton->setTopLeftPosition(46, 29);
        hideLeftButton->setImagePaths("FloatingNavpad/ButtonOn/Hide-Left-On.png", "FloatingNavpad/ButtonOff/Hide-Left-Off.png");
        ravenTouchButtons.add(hideLeftButton);
        addAndMakeVisible(hideLeftButton);
        
        hideRightButton = new RavenButton();
        hideRightButton->setClickingTogglesState(true);
        hideRightButton->setTopLeftPosition(0, 0);
        hideRightButton->addListener(this);
        hideRightButton->setTopLeftPosition(455, 99);
        hideRightButton->setImagePaths("FloatingNavpad/ButtonOn/Hide-Right-On.png", "FloatingNavpad/ButtonOff/Hide-Right-Off.png");
        ravenTouchButtons.add(hideRightButton);
        addAndMakeVisible(hideRightButton);
        
        mouseHider = new MouseHideComponent(getWidth(), getHeight());
        addAndMakeVisible(mouseHider);
        
        masterNavPad = _masterNavPad;
    }
    
    void actionListenerCallback (const String &message)
    {
        //messages coming from the master nav pad
        if(message == "trackzoom")
        {
            navPadImgCmp->setImageIndex(eNavPadTrackZoom);
        }
        if(message == "trackzoom all")
        {
            navPadImgCmp->setImageIndex(eNavPadTrackZoomAll);
        }
        else if(message == "waveform")
        {
            navPadImgCmp->setImageIndex(eNavPadWaveform);
        }
        else if(message == "scrub")
        {
            navPadImgCmp->setImageIndex(eNavPadScrub);
        }
        else if(message == "shuttle")
        {
            navPadImgCmp->setImageIndex(eNavPadShuttle);
        }
    }
    
    void buttonStateChanged(Button* b)
    {
    }
    
    void buttonClicked(Button* b);
    
    bool checkPadTouch(int touchX, int touchY)
    {
        if(componentTouched(navPadImgCmp, touchX, touchY, false, 75, 15, 75, 60) == true)
        {
            masterNavPad->resetSums(touchX);
            return true;
        }
        else
        {
            return false;
        }
    }
    
    void updatePadTouch(TUIO::TuioCursor *tcur, bool isTwoFinger)
    {
        float xSpeed = tcur->getXSpeed();
        float ySpeed = tcur->getYSpeed();
        int xPos = tcur->getScreenX(SCREEN_WIDTH);
        int yPos = tcur->getScreenY(SCREEN_HEIGHT);
        //LOCK_JUCE_THREAD
        masterNavPad->updatePadTouch(xPos, yPos, xSpeed, ySpeed, isTwoFinger);
    }
    
    void updatePadTouch(int xPos, int yPos, float xSpeed, float ySpeed, bool isTwoFinger, int numAvg = RAVEN_NAVPAD_NUM_AVG_DEFAULT)
    {
        //LOCK_JUCE_THREAD
        masterNavPad->updatePadTouch(xPos, yPos, xSpeed, ySpeed, isTwoFinger, numAvg);
    }
    
    void setNscLink(NscLink *_link) { link = _link;}
    
    void setButtonListener(Button::Listener *listener)
    {
        waveformButton->addListener(listener);
        trackzoomButton->addListener(listener);
    }
    
private:
    
    ScopedPointer<RavenNavPadImageComponent> navPadImgCmp;
    
    RavenButton *waveformButton;
    RavenButton *trackzoomButton;
    
    RavenButton* scrubButton;
    RavenButton* shuttleButton;
    
    NscLink *link;
    bool isFloating;
    
    std::vector<int> trackZoomKeyCodeUp, trackZoomKeyCodeDown, waveformZoomKeyCodeUp, waveformZoomKeyCodeDown, waveformZoomKeyCodeLeft, waveformZoomKeyCodeRight, navigateKeyCodeUp, navigateKeyCodeDown, navigateKeyCodeLeft, navigateKeyCodeRight;
    
    std::vector<int> enterWaveformKeyCode, individualTrackZoomKeyCodeUp, individualTrackZoomKeyCodeDown;
    
    RavenButton *hideLeftButton;
    RavenButton *hideRightButton;
    
    float sumXSpeed;
    float sumYSpeed;
    int sumCount;
    
    int mouseStartX;
    int currentClicks;
    int mode;
    RavenNavPadComponent *masterNavPad;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenFloatingNavPadComponent)
};


#endif
