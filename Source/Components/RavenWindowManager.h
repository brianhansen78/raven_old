#ifndef Raven_RavenModuleContainer_h
#define Raven_RavenModuleContainer_h


#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenWindow.h"
#include "RavenTouchComponent.h"

static int moveModuleCount = 0;

struct windowInfo
{
    String windowName;
    bool isOnDesktop;
    int rackPosition;
    int topLeftX;
    int topLeftY;
};


class RavenWindowManager
{
public:
    
    RavenWindowManager() : mainAppWindow(0), moveOffset(0), moving(false) {}
    
    void setWindowPosition(RavenWindow* window, int yPos)
    {
        //animate
        Rectangle<int> moveToRect(window->getX(), yPos, window->getWidth(), window->getHeight());
        Desktop::getInstance().getAnimator().animateComponent (window, moveToRect, 1.0, 500, true, 0.0, 0.0);
        
        int rackPos = 0;
        for(int i = 0; i < windows.size(); i++)
        {
            if(windows[i] != window && windows[i]->getY() < yPos)
            {
                rackPos++;
            }
            else if(windows[i] != window && windows[i]->getY() == yPos)
            {
                windows[i]->incRackPos();
            }
        }
        
        while (window->getRackPos() < rackPos) window->incRackPos();
        while (window->getRackPos() > rackPos) window->decRackPos();
    }
    
    void startMoveWindow(int screenX, int screenY)
    {
        LOCK_JUCE_THREAD
        {                        
            for(int i =0; i < windows.size(); i++)
            {                
                if(RavenTouchComponent::componentTouched(windows[i], screenX, screenY) && !windows[i]->isHidden())
                {
                    movingWindow = windows[i];
                    movingWindow->toFront(false);
                    moveOffset = screenY - movingWindow->getScreenY();
                    moving = true;
                    return;
                }
            }
        }
    }
    
    void moveWindow(int screenY)
    {
        if(moving)
        {
            //downsample movements to save CPU and increase painting speed
            //update every 10th TUIO update
            if(moveModuleCount%MOVE_MODULE_DOWNSAMPLE_AMT == 0)
            {
                LOCK_JUCE_THREAD
                {
                    int topY = screenY - moveOffset;
                    
                    if(topY >= 0 && topY <= SCREEN_HEIGHT - movingWindow->getHeight())
                    {                    
                        //move
                        movingWindow->setTopLeftPosition(0, topY);
                        
                        //swap
                        for(int i = 0; i < windows.size(); i++)
                        {
                            if(windows[i]->getRackPos() > movingWindow->getRackPos()) // if above
                            {
                                if(topY < windows[i]->getY() + windows[i]->getHeight()/2)
                                {
                                    movingWindow->setTopLeftPosition(0, windows[i]->getY());
                                    movingWindow->incRackPos();
                                    windows[i]->setTopLeftPosition(0, movingWindow->getY()+movingWindow->getHeight());
                                    windows[i]->decRackPos();
                                }
                            }
                            else if(windows[i]->getRackPos() < movingWindow->getRackPos()) //if below
                            {
                                int bottomY = topY + movingWindow->getHeight();
                                if(bottomY > windows[i]->getY() + windows[i]->getHeight()/2)
                                {
                                    int modBottom = windows[i]->getY() + windows[i]->getHeight();
                                    movingWindow->setTopLeftPosition(0, modBottom - movingWindow->getHeight());
                                    movingWindow->decRackPos();
                                    windows[i]->setTopLeftPosition(0, movingWindow->getY() - windows[i]->getHeight());
                                    windows[i]->incRackPos();
                                }
                            }
                        }
                    }
                    
                    movingWindow->repaint();
                } //end if lock was gained
            } //end if modulo update
            
            moveModuleCount++;
            if(moveModuleCount >= 1000*MOVE_MODULE_DOWNSAMPLE_AMT) moveModuleCount = 0;
            
        } //end if moving
    } // end move module
    
    void endMoveWindow()
    {
        moving = false;
        
        LOCK_JUCE_THREAD
        {
            rebuildRack();
            //put floating palettes back on top.. probably not necessary anymore with window levels
            //for(int i = 0; i < floatingWindows.size(); i++) floatingWindows[i]->toFront(false);
        }
        
        moveModuleCount = 0;
    }
    
    bool isMoving(){return moving;}
    
    void fixOverlaps()
    {
        for(int i = 0; i < windows.size(); i++)
        {
            for(int j = 0; j < windows.size(); j++)
            {
                if(windows[j] == windows[i]) continue; //don't check for self-overlap
                
                if(windows[i]->getBottom() >= windows[j]->getY() && windows[i]->getBottom() <= windows[j]->getBottom())
                {
                    int diff = windows[i]->getBottom() - windows[j]->getY();
                    windows[i]->setTopLeftPosition(0, windows[i]->getY() - diff);
                    if(windows[i]->getY() < osxMenuBarHeight)
                    {
                        //move all down
                        int down =  osxMenuBarHeight - windows[i]->getY();
                        for(int k = 0; k < windows.size(); k++)
                        {
                            windows[k]->setTopLeftPosition(0, windows[k]->getY() + down);
                        }
                    }
                }
                else if(windows[i]->getY() <= windows[j]->getBottom() && windows[i]->getY() >= windows[j]->getY())
                {
                    int diff = windows[j]->getBottom() - windows[i]->getY();
                    windows[i]->setTopLeftPosition(0, windows[i]->getY() + diff);
                    if(windows[i]->getBottom() > screenHeight)
                    {
                        //move all up
                        int up =  windows[i]->getBottom() - screenHeight;
                        for(int k = 0; k < windows.size(); k++)
                        {
                            windows[k]->setTopLeftPosition(0, windows[k]->getY() - up);
                        }
                    }
                }
            }
        }
    }
    
    void clearRack()
    {
        for(int i = 0; i < windows.size(); i++)
        {
            windows[i]->removeFromDesktop();
        }
        windows.clear();
    }
    
    void loadSavedFloatingWindowsLayout()
    {
        //NOTE: if not on desktop, reposition 1st, then add to desktop.
        // else just reposition.
        for(int i = 0; i < savedFloatingWindowLayout.size(); i++)
        {
            for(int j = 0; j < floatingWindows.size(); j++)
            {
                if(floatingWindows[j]->getName() == savedFloatingWindowLayout[i].windowName)
                {
                    //NOTE: perform animation here...
                    floatingWindows[j]->setTopLeftPosition(savedFloatingWindowLayout[i].topLeftX, savedFloatingWindowLayout[i].topLeftY);
                    
                    if(!floatingWindows[j]->isOnDesktop() && savedFloatingWindowLayout[i].isOnDesktop) floatingWindows[j]->addToDesktopWithOptions(RAVEN_PALETTE_LEVEL);
                    
                    if(floatingWindows[j]->isOnDesktop() && !savedFloatingWindowLayout[i].isOnDesktop) floatingWindows[j]->removeFromDesktop();
                }
            }
        }
    }
    
    void rebuildRack();
    void loadSavedRackWindowLayout();
    
    
    void insertWindowIntoRack(String windowName, int rackPos, bool makeVisible = false)
    {
        //add the window to the current visible rack.
        for(int i = 0; i < allRackWindows.size(); i++)
        {
            if(allRackWindows[i]->getName() == windowName)
            {
                allRackWindows[i]->setRackPos(rackPos);
                windows.add(allRackWindows[i]);
            }
        }
        
        //adjust rack positions of other windows
        for(int i = 0; i < windows.size(); i++)
        {
            if(windows[i]->getRackPos() >= rackPos && windows[i]->getName() != windowName)
            {
                windows[i]->setRackPos(windows[i]->getRackPos() + 1);
            }
        }
        
        resetRackPositions();
        if(makeVisible) rebuildRack();
    }
    
    void insertWindowIntoRack(RavenWindow *addedWindow, int rackPos, bool makeVisible = false)
    {
        //if window is already there, don't add it, just reset rack position or make visible.
        if(rackContainsWindow(addedWindow)) //just reset the rack position
        {
            repositionWindowInRack(addedWindow, rackPos);
        }
        else //reset the rack positions and insert the new window.
        {
            for(int i = 0; i < windows.size(); i++)
            {
                if(windows[i]->getRackPos() >= rackPos)
                {
                    windows[i]->setRackPos(windows[i]->getRackPos() + 1);
                }
            }
            addedWindow->setRackPos(rackPos);
            windows.add(addedWindow);
        }
        
//*** Original way of doing it...
//        addedWindow->setRackPos(rackPos);
//        for(int i = 0; i < windows.size(); i++)
//        {
//            if(windows[i]->getRackPos() >= rackPos)
//            {
//                windows[i]->setRackPos(windows[i]->getRackPos() + 1);
//            }
//        }
//        windows.add(addedWindow);
        resetRackPositions();
        if(makeVisible) rebuildRack();
    }

    bool rackContainsWindow(RavenWindow *checkWindow)
    {
        for(int i = 0; i < windows.size(); i++)
        {
            if(windows[i] == checkWindow) return true;
        }
        return false;
    }
    
    void repositionWindowInRack(RavenWindow *reposWindow, int newPosition)
    {
        int originalPosition = reposWindow->getRackPos();
        int positionChange = newPosition - originalPosition;
        
        for(int i = 0; i < windows.size(); i++)
        {
            if(positionChange > 0)
            {
                if(windows[i]->getRackPos() > originalPosition && windows[i]->getRackPos() <= newPosition)
                    windows[i]->setRackPos(windows[i]->getRackPos() - 1);
            }
            else if(positionChange < 0)
            {
                if(windows[i]->getRackPos() < originalPosition && windows[i]->getRackPos() >= newPosition)
                    windows[i]->setRackPos(windows[i]->getRackPos() + 1);
            }
        }
        reposWindow->setRackPos(newPosition);
        
    }
    
    void insertWindowIntoRack(RavenWindow *addedWindow, RavenWindow *referenceWindow, bool makeVisible = false)
    {
        addedWindow->setRackPos(referenceWindow->getRackPos() + 1);
        for(int i = 0; i < windows.size(); i++)
        {
            if(windows[i]->getRackPos() > referenceWindow->getRackPos())
            {
                windows[i]->setRackPos(windows[i]->getRackPos() + 1);
            }
        }
        windows.add(addedWindow);
        resetRackPositions();
        if(makeVisible) rebuildRack();
    }

    void appendWindowToRack(RavenWindow *addedWindow, bool makeVisible = false)
    {
        //std::cout << "rack position assigning: " << modules.size() << std::endl;
        addedWindow->setRackPos(windows.size());
        windows.add(addedWindow);
        resetRackPositions();
        if(makeVisible) rebuildRack();
        
    }
        
    void removeWindowFromRack(int i)
    {
        windows[i]->removeFromDesktop();
        windows.remove(i);
        
        resetRackPositions();
    }
    
    
    void removeWindowFromRack(RavenWindow *removeWindow)
    {
        for(int i = 0; i < windows.size(); i++)
        {
            if(windows[i] == removeWindow)
            {
                windows[i]->removeFromDesktop();
                windows.remove(i);
                break;
            }
        }
        resetRackPositions();
    }

    
    void removeWindowFromRack(String windowName)
    {
        for(int i = 0; i < windows.size(); i++)
        {
            if(windows[i]->getName() == windowName)
            {
                windows[i]->removeFromDesktop();
                windows.remove(i);
                break;
            }
        }
        resetRackPositions();
    }
    
    void resetRackPositions()
    {
        //sort by rack position
        sortWindowsByRackPosition(0);
        
        int curRackPos = 0;
        for(int i = 0; i < windows.size(); i++)
        {
            windows[i]->setRackPos(curRackPos);
            curRackPos++;
        }
    }
    
    void sortWindowsByRackPosition(int direction)
    {
        if(direction == 0) //ascending order
        {
            RavenWindow* temp;
            for(int i = 0; i < windows.size(); i++)
            {
                for(int j = 0; j < windows.size() - i - 1; j++)
                {
                    if(windows[j]->getRackPos() > windows[j + 1]->getRackPos())
                    {
                        temp = windows[j];
                        windows.set(j, windows[j + 1]);
                        windows.set(j + 1, temp);
                    }
                }
            }
        }
        else if(direction == 1) //descending order
        {
            RavenWindow* temp;
            for(int i = 0; i < windows.size(); i++)
            {
                for(int j = 0; j < windows.size() - i - 1; j++)
                {
                    if(windows[j]->getRackPos() < windows[j + 1]->getRackPos())
                    {
                        temp = windows[j];
                        windows.set(j, windows[j + 1]);
                        windows.set(j + 1, temp);
                    }
                }
            }
        }        
    }
    
    void updateRemainingHeight()
    {
        int modulesHeight = 0;
        for(int i = 0; i < windows.size(); i++) modulesHeight += windows[i]->getHeight();
        remainingHeight = SCREEN_HEIGHT - modulesHeight;
    }
    
    Array<RavenWindow*> getRackWindows(){ return windows; };
    Array<RavenWindow*> getFloatingWindows(){ return floatingWindows; };
    
    void setSavedRackWindowLayout(Array<windowInfo> wl){ savedWindowLayout = wl; };
    void setSavedFloatingWindowLayout(Array<windowInfo> fwl){ savedFloatingWindowLayout = fwl; };
    
    int getCurrentNumFunctionBars(){ return curNumFunctionsBars;}
    void setCurrentNumFunctionBars(int numBars){curNumFunctionsBars = numBars;}
    
protected:

    DocumentWindow *mainAppWindow;
    Array<Component*> floatingComponents;
    
    RavenWindow *movingWindow;
    Array<RavenWindow*> windows;
    Array<RavenWindow*> floatingWindows;
    
    int moveOffset;
    bool moving;
    int remainingHeight;
    
    //used for adjusting windows
    int curNumFunctionsBars;
    
    //used for loading a saved window layout
    Array<RavenWindow*> allRackWindows;
    Array<windowInfo> savedWindowLayout;
    Array<windowInfo> savedFloatingWindowLayout;

    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenWindowManager)
    
};

#endif

