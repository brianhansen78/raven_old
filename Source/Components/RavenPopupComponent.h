
//unsed: previously used for selecting inserts from dropdown list

#ifndef Raven_RavenPopupComponent_h
#define Raven_RavenPopupComponent_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenLabel.h"


class RavenPopupComponent : public PopupMenu::CustomComponent
{
public:
    RavenPopupComponent(const String labelText)
    {
        
        setSize(60, 30);
        
        label = new RavenLabel();
        label->setBounds(0, 0, 60, 30);
        label->setText(labelText, false);
        label->setJustificationType(Justification::centred);
        addAndMakeVisible(label);
        
        setInterceptsMouseClicks(false, false);
        
    }
    ~RavenPopupComponent()
    {
        delete label;
    }
    
    void getIdealSize (int &idealWidth, int &idealHeight)
    {
        idealWidth = 60;
        idealHeight = 30;
    }
    
    void paint(Graphics &g)
    {
        g.fillAll(Colour::fromRGBA(0, 0, 0, 200));

        if(isItemHighlighted())
        {
            g.setColour(Colour::fromRGBA(255, 255, 255, 128));
            g.fillAll();
        }
    }
    
private:
    
    RavenLabel *label;
    
};


#endif
