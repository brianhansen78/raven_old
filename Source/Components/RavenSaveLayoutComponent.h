
#ifndef Raven_RavenSaveLayoutComponent_h
#define Raven_RavenSaveLayoutComponent_h

#define SAVE_GROUP_ID       3 //something that won't interfere with other radio group ID's.. this was the 3rd set added, so 3

#define NUM_PRESETS 8

#include "RavenTouchComponent.h"

class RavenSaveLayoutComponent : public RavenTouchComponent, public Button::Listener, public ActionBroadcaster, public ActionListener
{
public:
    RavenSaveLayoutComponent()
    {
        int secondRowY = 55;
        saveLayoutButton = new RavenButton();
        RAVEN_BUTTON_1(saveLayoutButton);
        saveLayoutButton->setClickingTogglesState(true);
        saveLayoutButton->setTopLeftPosition(0, 0);
        saveLayoutButton->addListener(this);
        saveLayoutButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/LayoutPreset/images/layoutpreset-on_01.png", "TOOLBAR/FUNCTIONMODULE/LayoutPreset/images/layoutpreset-off_01.png");
        ravenTouchButtons.add(saveLayoutButton);
        addAndMakeVisible(saveLayoutButton);
        
        revertLayoutButton = new RavenButton();
        RAVEN_BUTTON_1(revertLayoutButton);
        revertLayoutButton->setTopLeftPosition(0, secondRowY);
        revertLayoutButton->addListener(this);
        revertLayoutButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/LayoutPreset/RevertToLast-On-btn.png", "TOOLBAR/FUNCTIONMODULE/LayoutPreset/RevertToLast-Off-btn.png");
        ravenTouchButtons.add(revertLayoutButton);
        addAndMakeVisible(revertLayoutButton);

        
        int xPos = saveLayoutButton->getWidth();
        int yPos = 0;
        
        for(int i = 0; i < NUM_PRESETS; i++)
        {
            if(i == 4)
            {
                xPos = saveLayoutButton->getWidth();
                yPos = secondRowY;
            }
            
            String onFile, offFile;

            onFile = "TOOLBAR/FUNCTIONMODULE/LayoutPreset/images/layoutpreset-on_0" + String(i+2) + ".png";
            offFile = "TOOLBAR/FUNCTIONMODULE/LayoutPreset/images/layoutpreset-off_0" + String(i+2) + ".png";
            
            RavenButton *presetNumButton = new RavenButton();
            RAVEN_BUTTON_1(presetNumButton);
            presetNumButton->setTopLeftPosition(xPos, yPos);
            presetNumButton->addListener(this);
            presetNumButton->setImagePaths(onFile, offFile);
            ravenTouchButtons.add(presetNumButton);
            presetNumButton->setRadioGroupId(SAVE_GROUP_ID);
            presetNumButton->setClickingTogglesState(true);
            String presetButton(i+1);
            ravenTouchButtons[i+2]->setName(presetButton); //NOTE: needs to be "i+1" because the save layout button was added to touch buttons
            addAndMakeVisible(presetNumButton);
            xPos += presetNumButton->getWidth();
            
        }
    
        setSize(5*saveLayoutButton->getWidth(), 2*saveLayoutButton->getHeight());
        
        //indicatePreset(0);
    }
    
    void setPresetButtonListener (Button::Listener *listener)
    {
        for(int i = 0; i < ravenTouchButtons.size(); i++) ravenTouchButtons[i]->addListener(listener);
    }
    
    void buttonStateChanged (Button*)
    {        
    }
    
    void buttonClicked (Button* b)
    {
        if(b != revertLayoutButton && b != saveLayoutButton && saveLayoutButton->getToggleState())
        {
            int presetIndex = b->getName().getIntValue();            
            sendActionMessage("save" + String(presetIndex));
            
            //turn the save layout button off.
            saveLayoutButton->setToggleState(false, false);
            b->setToggleState(false, false);
        }
        else if(b != revertLayoutButton && b != saveLayoutButton && !saveLayoutButton->getToggleState())
        {
            int presetIndex = b->getName().getIntValue();
            sendActionMessage("load" + String(presetIndex));
        }
        else if(b == revertLayoutButton)
        {
            sendActionMessage("revertLayout");
        }
         
    }

    void indicatePreset(int idx)
    {
        if(idx < NUM_PRESETS) ravenTouchButtons[idx+1]->setToggleState(true, false);
    }
    
    void actionListenerCallback (const String &message)
    {
        if(message.startsWith("indicatePreset"))
        {
            int idx = message.substring(14, 15).getIntValue();
            indicatePreset(idx + 1);
        }
    }
    
private:
    
    RavenButton *saveLayoutButton;
    RavenButton *revertLayoutButton;

    //Array<RavenButton*> presetButtons;
    //RavenSaveLayoutHandler *saveLayoutHandler;
        
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenSaveLayoutComponent)
};


#endif
