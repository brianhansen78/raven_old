//
//  RavenImageComponent.h
//  Raven
//
//  Created by Joshua Dickinson on 4/9/13.
//
//

#ifndef Raven_RavenImageComponent_h
#define Raven_RavenImageComponent_h

class RavenImageComponent : public juce::Component
{
public:
    RavenImageComponent()
    {}
    
    void setImage(juce::String path)
    {
        image = ImageCache::getFromFile(File(path));
        setSize(image.getWidth(), image.getHeight());
        repaint();
    }
    
    void paint(Graphics &g)
    {
        g.drawImageAt(image, 0, 0);
    }
    
private:
    Image image;
};

#endif
