//
//  RavenSaveLayoutHandler.cpp
//  Raven
//
//  Created by Ryan McGee on 6/6/13.
//
//

#include "RavenSaveLayoutHandler.h"
#include "RavenRackContainer.h"

bool RavenSaveLayoutHandler::restoreDefaultLayouts()
{
    RAVEN_TOUCH_LOCK
    
    //NOTE: un-comment this if you want to create new default layouts. 
    //generateDefaultLayoutsFromTemplates();
    
    //Call alert window for to notify user of overwrites.
    String alertMessage = "Warning! You are about to overwrite all current Raven saved layouts with factory default layouts. Continue?";
    if(RAVEN->getAlertWindowComponent()->callAlertWindow(2, alertMessage, "OK", "Cancel") == 0)
    {
        RAVEN_TOUCH_UNLOCK
        return false;
    }

    //1. delete all layouts from global folder
    String globalDirectoryPath;
    globalDirectoryPath = String(XML_PATH_GLOBAL);
    DirectoryIterator globalDirIter(File(globalDirectoryPath), true, "*.xml");
    while(globalDirIter.next())
    {
        File curGlobalFile(globalDirIter.getFile());
        curGlobalFile.deleteFile();
    }
    
    //2. delete all layouts from current session folder
    DirectoryIterator sessionDirIter(File(sessionPath), true, "*.xml");
    while(sessionDirIter.next())
    {
        File curSessionFile(sessionDirIter.getFile());
        curSessionFile.deleteFile();
    }
    
    //3. copy all factory layouts into global and current session folder.
    String defaultDirectoryPath;
    defaultDirectoryPath = String(XML_PATH_DEFAULT);
    DirectoryIterator defaultDirIter(File(defaultDirectoryPath), true, "*.xml");
    while(defaultDirIter.next())
    {
        File curDefaultFile(defaultDirIter.getFile());
        curDefaultFile.copyFileTo(globalDirectoryPath + "/" + curDefaultFile.getFileName());
        curDefaultFile.copyFileTo(sessionPath + "/" + curDefaultFile.getFileName());
    }
    
    loadIcons();
    
    RAVEN_TOUCH_UNLOCK
    return true;
}

String RavenSaveLayoutHandler::getSaveLayoutXMLPath(bool isDAWMode, int preset)
{
    String path;
        
    if(RAVEN->isSaveToSession())
    {
        path = sessionPath + "/UserLayout_" + String(isDAWMode) + "_" + String(preset) + ".xml";
    }
    else
    {
        path = String(XML_PATH_GLOBAL) + "/UserLayout_" + String(isDAWMode) + "_" + String(preset) + ".xml";
    }
    return path;
}

void RavenSaveLayoutHandler::generateDefaultLayoutsFromTemplates()
{
    RAVEN_TOUCH_LOCK
    
    //1. setup path of templates files and new defaults.
    String defaultTemplatePath;
    defaultTemplatePath = (RAVEN_24) ? String(XML_PATH_TEMPLATE_MTI) : String(XML_PATH_TEMPLATE_MTX);
    String newDefualtsPath;
    newDefualtsPath = (RAVEN_24) ? String(XML_PATH_NEWDEFAULTS_MTI) : String(XML_PATH_NEWDEFAULTS_MTX);
    
    DirectoryIterator defaultTemplateIter(File(defaultTemplatePath), true, "*.xml");
    while(defaultTemplateIter.next())
    {
        File curTemplateFile(defaultTemplateIter.getFile());
        if(curTemplateFile.getFullPathName().endsWith("UserLayout_0_1.xml"))
        {
            for(int i = 0; i < 8; i++)
            {
                curTemplateFile.copyFileTo(newDefualtsPath + "/UserLayout_" + String(0) + "_" + String(i+1) + ".xml");
            }
            curTemplateFile.copyFileTo(newDefualtsPath + "/UserLayout_" + String(0) + "_" + String(DEFAULT_PRESET_INDEX) + ".xml");
            curTemplateFile.copyFileTo(newDefualtsPath + "/UserLayout_" + String(0) + "_" + String(REVERT_LAYOUT_INDEX) + ".xml");
            curTemplateFile.copyFileTo(newDefualtsPath + "/UserLayout_" + String(0) + "_" + String(MIX_DAW_INDEX) + ".xml");
            curTemplateFile.copyFileTo(newDefualtsPath + "/UserLayout_" + String(0) + "_" + String(PRIOR_CUSTOM_VIEW_INDEX) + ".xml");
            curTemplateFile.copyFileTo(newDefualtsPath + "/UserLayout_" + String(0) + "_" + String(PRIOR_SAVE_VIEW_INDEX) + ".xml");
        }
        else if(curTemplateFile.getFullPathName().endsWith("UserLayout_1_1.xml"))
        {
            for(int i = 0; i < 8; i++)
            {
                curTemplateFile.copyFileTo(newDefualtsPath + "/UserLayout_" + String(1) + "_" + String(i+1) + ".xml");
            }
            curTemplateFile.copyFileTo(newDefualtsPath + "/UserLayout_" + String(1) + "_" + String(DEFAULT_PRESET_INDEX) + ".xml");
            curTemplateFile.copyFileTo(newDefualtsPath + "/UserLayout_" + String(1) + "_" + String(REVERT_LAYOUT_INDEX) + ".xml");
            curTemplateFile.copyFileTo(newDefualtsPath + "/UserLayout_" + String(1) + "_" + String(MIX_DAW_INDEX) + ".xml");
            curTemplateFile.copyFileTo(newDefualtsPath + "/UserLayout_" + String(1) + "_" + String(PRIOR_CUSTOM_VIEW_INDEX) + ".xml");
            curTemplateFile.copyFileTo(newDefualtsPath + "/UserLayout_" + String(1) + "_" + String(PRIOR_SAVE_VIEW_INDEX) + ".xml");
        }
    }
        
    RAVEN_TOUCH_UNLOCK
    return;
}

void RavenSaveLayoutHandler::setLastView(String version, int mode, int index)
{
    XmlElement lastSessionIndex ("LAST_SESSION_INDEX");
    lastSessionIndex.setAttribute("version", version);
    lastSessionIndex.setAttribute("mode", mode);
    lastSessionIndex.setAttribute("preset", index);
    String xmlDoc1 = lastSessionIndex.createDocument (String::empty);
    const String outFile1 = sessionPath + "/LastSessionIndex" + ".xml";
    lastSessionIndex.writeToFile(outFile1,"");
}

void RavenSaveLayoutHandler::saveSession()
{
    RAVEN_TOUCH_LOCK
    
    //NOTE: Alert user before overwriting savedIcons.
    FileChooser ravenSessionChooser ("Save Raven Session Location...", File::getSpecialLocation (File::userHomeDirectory));
    if (ravenSessionChooser.browseForDirectoryWithSavePrompt())
    {
        //1. designate a directory. This will be the path to load/save user layouts.
        File ravenSessionPath (ravenSessionChooser.getResult());
        
        if(!ravenSessionPath.getFullPathName().endsWith("/RavenSession"))
        {
            ravenSessionPath = ravenSessionPath.getFullPathName() + "/RavenSession/";
        }

        //2. if the session path already exists, alert user before overwriting.
        if(ravenSessionPath.exists())
        {
            String alertMessage = "WARNING! You are about to overwrite an existing Raven saved layout session. Continue?";
            if(RAVEN->getAlertWindowComponent()->callAlertWindow(2, alertMessage, "OK", "Cancel") == 0)
            {
                RAVEN_TOUCH_UNLOCK
                return;
            }
        }
        else
        {
            ravenSessionPath.createDirectory();
        }

        //4. if there exists a sessionPath, then we copy that to the new directory, otherwise, we copy the globals.
        String currentLayoutPath = (sessionPath != "") ? sessionPath : XML_PATH_GLOBAL;
        String newSessionPath = ravenSessionPath.getFullPathName();
        DirectoryIterator directoryIterator(File(currentLayoutPath), true, "*.xml");
        while(directoryIterator.next())
        {
            File curLayoutFile(directoryIterator.getFile());
            curLayoutFile.copyFileTo(newSessionPath + "/" + curLayoutFile.getFileName());
        } 
        
        sessionPath = newSessionPath;
        setLastView(RAVEN->getCurrentRavenVersion(), RAVEN->isDawMode(), PRIOR_SAVE_VIEW_INDEX);

        File priorCustomModeFile(sessionPath + "/UserLayout_" + String(RAVEN->isDawMode()) + "_" + String(PRIOR_CUSTOM_VIEW_INDEX) + ".xml");
        priorCustomModeFile.copyFileTo(sessionPath + "/UserLayout_" + String(RAVEN->isDawMode()) + "_" + String(PRIOR_SAVE_VIEW_INDEX) + ".xml");
        
        //saveUserLayout(RAVEN->isDawMode(), PRIOR_SAVE_VIEW_INDEX);
        saveIcons();
        RAVEN->setSaveToSession(true);
    }
    RAVEN_TOUCH_UNLOCK
}

bool RavenSaveLayoutHandler::loadSession()
{
    RAVEN_TOUCH_LOCK
    
    FileChooser ravenSessionChooser ("Load Raven Session Folder...", File::getSpecialLocation (File::userHomeDirectory), "*.xml");
    if (ravenSessionChooser.browseForDirectory())
    {
        File ravenSessionPath (ravenSessionChooser.getResult());
        if(!ravenSessionPath.getFullPathName().endsWith("RavenSession"))
        {
            String alertMessage = "There is no existing saved Raven session at this location. Please select another directory.";
            RAVEN->getAlertWindowComponent()->callAlertWindow(1, alertMessage, "OK");
            RAVEN_TOUCH_UNLOCK
            return false;            
        }
        else
        {
            sessionPath = ravenSessionPath.getFullPathName();
        }
    }
    
    const File xmlFile(sessionPath + "/LastSessionIndex" + ".xml");
    if(!xmlFile.exists())
    {
        RAVEN_TOUCH_UNLOCK
        return false; //return if file not found
    }
    
    XmlDocument myDocument(xmlFile);
    XmlElement *layoutElement = myDocument.getDocumentElement();
    
    //make sure the document has the element we are looking for
    if (!layoutElement->hasTagName("LAST_SESSION_INDEX"))
    {
        RAVEN_TOUCH_UNLOCK
        return false;
    }
    
    
    int lastSessionMode = layoutElement->getIntAttribute("mode");
    int lastSessionPreset = layoutElement->getIntAttribute("preset");
    String savedRavenVersion = "MTX";
    String savedRavenDAW = "ProTools";
    
    String currentRavenDAW = "ProTools";

    //check for version and DAW compatibility
    String alertMessage;
    if(savedRavenVersion != RAVEN->getCurrentRavenVersion() || savedRavenDAW != currentRavenDAW)
    {
        if(savedRavenVersion != RAVEN->getCurrentRavenVersion())
            alertMessage = "This Raven Session is incompatible with the current Raven Version. Please select another directory.";
        else
            alertMessage = "This Raven Session is incompatible with the current DAW in use. Please select another directory.";
        
        RAVEN->getAlertWindowComponent()->callAlertWindow(1, alertMessage, "OK");

        RAVEN_TOUCH_UNLOCK
        return false;
    }
    
    
    //if we have gotten this far, we can successfully load a session folder and xml files
    RAVEN->setSaveToSession(true);
    loadUserLayout(lastSessionMode, lastSessionPreset);
    loadIcons();
    
    delete layoutElement;
    RAVEN_TOUCH_UNLOCK
    return true;
}

void RavenSaveLayoutHandler::saveIcons()
{
    RAVEN_TOUCH_LOCK

    XmlElement layout ("ICON_LAYOUT");
    
    //save incon HashMap info
    HashMap<String, int>::Iterator i(*RAVEN->getTrackNames()->getIconIndexMap());
    while (i.next())
    {
        XmlElement* iconInfo = new XmlElement ("ICON");
        iconInfo->setAttribute("trackname", i.getKey());
        iconInfo->setAttribute("index", i.getValue());
        layout.addChildElement (iconInfo);
    }

    String myXmlDoc = layout.createDocument (String::empty);
    const String outFile = sessionPath + "/RavenIcons" + ".xml";
    layout.writeToFile(outFile,"");
    layout.deleteAllChildElements();
    
    RAVEN_TOUCH_UNLOCK
}

void RavenSaveLayoutHandler::loadIcons()
{
    RAVEN_TOUCH_LOCK
    
    const File xmlIconsFile(sessionPath + "/RavenIcons" + ".xml");
    if(!xmlIconsFile.exists())
    {
        RAVEN_TOUCH_UNLOCK
        return; //return if file not found
    }
    XmlDocument iconsDocument(xmlIconsFile);
    XmlElement *iconLayoutElement = iconsDocument.getDocumentElement();
    
    //make sure the document has the element we are looking for
    if (!iconLayoutElement->hasTagName("ICON_LAYOUT"))
    {
        RAVEN_TOUCH_UNLOCK
        return;
    }
    
    HashMap<String, int> iconMap;
    
    forEachXmlChildElement (*iconLayoutElement, iconLayout)
    {
        //set attributes for rack windows
        if (iconLayout->hasTagName("ICON")) //Change to "WINDOW" instead of "MODULE"
        {
            String trackname = iconLayout->getStringAttribute ("trackname");
            int index = iconLayout->getIntAttribute ("index");
            iconMap.set(trackname, index);
        }
    }
    RAVEN->getIconPalette()->convertIndexMapToIconMap(&iconMap);
    delete iconLayoutElement;
    
    RAVEN_TOUCH_UNLOCK
}


void RavenSaveLayoutHandler::saveUserLayout(bool isDAWMode, int preset)
{
    RAVEN_TOUCH_LOCK
    
    if(isDAWMode)   currentDAWIndex = preset;
    else            currentMixIndex = preset;
    
    //std::cout << "DAW MODE: " << isDAWMode << "Save PRESET INDEXING: " << preset << std::endl;
        
    // create an outer node
    XmlElement functionsLayout ("LAYOUT");
    
    //save if in toolbar mode and number of bars displayed
    functionsLayout.setAttribute("toolbar", RAVEN->isDawMode());
    functionsLayout.setAttribute("hybrid", RAVEN->isHybridMode());
    functionsLayout.setAttribute("numbars", windowManager->getCurrentNumFunctionBars());
    
    //save hybrid mode mixer position and number of banks.
    XmlElement* hybridInfo = new XmlElement ("HYBRID");
    hybridInfo->setAttribute("topleftx", RAVEN->getMixer()->getHybridX());
    hybridInfo->setAttribute("toplefty", RAVEN->getMixer()->getHybridY());
    hybridInfo->setAttribute("numbanks", RAVEN->getMixer()->getNumberOfHybridModeBanks());
    functionsLayout.addChildElement (hybridInfo);
    
    //save rack windows info
    for(int i = 0 ; i < windowManager->getRackWindows().size(); i++){
        RavenWindow* curWindow = windowManager->getRackWindows()[i];
        
        XmlElement* rackModule = new XmlElement ("MODULE");
        rackModule->setAttribute("name", curWindow->getName());
        rackModule->setAttribute("rackpos", curWindow->getRackPos());
        rackModule->setAttribute("onDesktop", curWindow->isOnDesktop());
        rackModule->setAttribute("topleftx", curWindow->getX());
        rackModule->setAttribute("toplefty", curWindow->getY());
        functionsLayout.addChildElement (rackModule);
    }
    
    //save floating windows info
    for(int i = 0 ; i < windowManager->getFloatingWindows().size(); i++){
        RavenWindow* curFloat = windowManager->getFloatingWindows()[i];
        
        XmlElement* floatModule = new XmlElement ("FLOAT");
        floatModule->setAttribute("name", curFloat->getName());
        floatModule->setAttribute("topleftx", curFloat->getX());
        floatModule->setAttribute("toplefty", curFloat->getY());
        floatModule->setAttribute("onDesktop", curFloat->isOnDesktop());
        functionsLayout.addChildElement (floatModule);
    }
    
    //save functions modules info
    for (int i = 0; i < functionsManager->getFunctionsComponents().size(); ++i)
    {
        for(int j = 0; j < functionsManager->getFunctionsComponents()[i]->getNumChildComponents(); j++)
        {
            Component *curComp = functionsManager->getFunctionsComponents()[i]->getChildComponent(j);
            
            int storeX = curComp->getX();
            int storeY = curComp->getY();
            String curString = curComp->getName();
            if(curString == "" || curString == "mouseHider") continue;
            
            // create an inner element..
            XmlElement* functionComponent = new XmlElement ("FUNCTION");
            functionComponent->setAttribute ("paletteName", functionsManager->getFunctionsComponents()[i]->getName());
            functionComponent->setAttribute ("moduleName", curString);
            functionComponent->setAttribute ("screenx", curComp->getScreenX());
            functionComponent->setAttribute ("screeny", curComp->getScreenY());
            functionComponent->setAttribute ("topleftx", storeX);
            functionComponent->setAttribute ("toplefty", storeY);
            
            if(curString.upToFirstOccurrenceOf("_", false, false) == "HotKeysComponent"){
                functionComponent->setAttribute ("hotkeyindx", curString.getTrailingIntValue());
                
                //1. get the appropriate hotkey comp via index
                int hotCompIndx = curString.getTrailingIntValue();
                RavenHotKeysComponent* curHotComp = functionsManager->getHotKeyComponents().getUnchecked(hotCompIndx);
                
                //2. cycle throught th 8 buttons and store button index, text, and key code
                for(int k = 0; k < 8; k++){
                    String labelText = curHotComp->getKeyLabelText(k);
                    std::vector<int> keyCode = curHotComp->getKeyCode(k);
                    
                    XmlElement* hotcomp = new XmlElement ("HOTBUTTON");
                    hotcomp->setAttribute ("labelindex", k);
                    hotcomp->setAttribute ("labeltext", labelText);
                    
                    //NOTE: Need to figure out the key code stuff more!!!
                    String keyCodeString = "";
                    for(int n = 0; n < keyCode.size(); n++){
                        String curKey(keyCode[n]);
                        String delimiter = (n == keyCode.size()-1) ? "" : "_";
                        keyCodeString += curKey + delimiter;
                    }
                    hotcomp->setAttribute ("keycode", keyCodeString);
                    
                    functionComponent->addChildElement(hotcomp);
                }
            }
            functionsLayout.addChildElement (functionComponent);
        }
    }
    
    //save funcions modules on floating essentials palette
    for (int j = 0; j < floatingEssentials->getNumChildComponents(); ++j)
    {
        Component *curComp = floatingEssentials->getChildComponent(j);
        
        int storeX = curComp->getX();
        int storeY = curComp->getY();
        String curString = curComp->getName();
        if(curString == "" || curString == "MouseHider") continue;
        
        // create an inner element..
        XmlElement* functionComponent = new XmlElement ("FUNCTION");
        functionComponent->setAttribute ("paletteName", floatingEssentials->getName());
        functionComponent->setAttribute ("moduleName", curString);
        functionComponent->setAttribute ("screenx", curComp->getScreenX());
        functionComponent->setAttribute ("screeny", curComp->getScreenY());
        functionComponent->setAttribute ("topleftx", storeX);
        functionComponent->setAttribute ("toplefty", storeY);
        
        if(curString.upToFirstOccurrenceOf("_", false, false) == "HotKeysComponent"){
            functionComponent->setAttribute ("hotkeyindx", curString.getTrailingIntValue());
            
            //1. get the appropriate hotkey comp via index
            int hotCompIndx = curString.getTrailingIntValue();
            RavenHotKeysComponent* curHotComp = functionsManager->getHotKeyComponents()[hotCompIndx];
            
            //2. cycle throught th 8 buttons and store button index, text, and key code
            for(int k = 0; k < 8; k++){
                String labelText = curHotComp->getKeyLabelText(k);
                std::vector<int> keyCode = curHotComp->getKeyCode(k);
                
                XmlElement* hotcomp = new XmlElement ("HOTBUTTON");
                hotcomp->setAttribute ("labelindex", k);
                hotcomp->setAttribute ("labeltext", labelText);
                
                //NOTE: Need to figure out the key code stuff more!!!
                String keyCodeString = "";
                for(int n = 0; n < keyCode.size(); n++){
                    String curKey(keyCode[n]);
                    String delimiter = (n == keyCode.size()-1) ? "" : "_";
                    keyCodeString += curKey + delimiter;
                }
                hotcomp->setAttribute ("keycode", keyCodeString);
                
                functionComponent->addChildElement(hotcomp);
            }
        }
        functionsLayout.addChildElement (functionComponent);
    }
    
    //write to xml document
    String xmlDoc2 = functionsLayout.createDocument (String::empty);
    const String outFile2 = getSaveLayoutXMLPath(isDAWMode, preset);
    functionsLayout.writeToFile(outFile2,"");
    functionsLayout.deleteAllChildElements();
    
    RAVEN_TOUCH_UNLOCK
}


void RavenSaveLayoutHandler::loadUserLayout(bool isDAWMode, int preset)
{
    RAVEN_TOUCH_LOCK
    
    rackWindowLayout.clear();
    floatingWindowLayout.clear();
    functionsLayout.clear();
    
    const String xmlPath = getSaveLayoutXMLPath(isDAWMode, preset);
    //std::cout << "FilePath: " << xmlPath << std::endl;
    const File xmlFile(xmlPath);
    if(!xmlFile.exists())
    {
        RAVEN_TOUCH_UNLOCK
        return; //return if file not found
    }
        
    XmlDocument myDocument(xmlFile);
    XmlElement *layoutElement = myDocument.getDocumentElement();
    
    //make sure the document has the element we are looking for
    if (!layoutElement->hasTagName("LAYOUT"))
    {
        RAVEN_TOUCH_UNLOCK
        return;
    }
    
    //bool toolbarMode = layoutElement->getBoolAttribute("toolbar");
    bool hybridMode = layoutElement->getBoolAttribute("hybrid");
    int numBars = layoutElement->getIntAttribute("numbars");
    
    windowManager->setCurrentNumFunctionBars(numBars);
    
    RAVEN->setSavedHybridModeLayout(hybridMode);
    (isDAWMode) ? RAVEN->getFunctionsManager()->enterToolbarMode() : RAVEN->getFunctionsManager()->exitToolbarMode();
    (!isDAWMode && !hybridMode && RAVEN->shouldShowBackground()) ? RAVEN->showBackground() : RAVEN->getBackgroundWindow()->removeFromDesktop();
    

    //iterate through and parse the child elements.
    forEachXmlChildElement (*layoutElement, layout)
    {
        //load hybrid information
        if (layout->hasTagName("HYBRID"))
        {
            int xpos = layout->getIntAttribute ("topleftx");
            int ypos = layout->getIntAttribute ("toplefty");
            int numracks = layout->getIntAttribute ("numbanks");
            RAVEN->getMixer()->setHybridPositionParameters(xpos, ypos, numracks);
        }
        
        //set attributes for rack windows
        if (layout->hasTagName("MODULE")) //Change to "WINDOW" instead of "MODULE"
        {
            windowInfo wi;
            wi.windowName = layout->getStringAttribute ("name");
            wi.rackPosition = layout->getIntAttribute ("rackpos");
            wi.isOnDesktop = layout->getBoolAttribute("onDesktop");
            wi.topLeftX = layout->getBoolAttribute("topleftx");
            wi.topLeftY = layout->getBoolAttribute("toplefty");
            rackWindowLayout.add(wi);
        }
        
        //set attributes for floating windows
        if (layout->hasTagName("FLOAT"))
        {
            windowInfo wi;
            wi.windowName = layout->getStringAttribute ("name");
            wi.topLeftX = layout->getIntAttribute ("topleftx");
            wi.topLeftY = layout->getIntAttribute ("toplefty");
            wi.isOnDesktop = layout->getBoolAttribute("onDesktop");
            floatingWindowLayout.add(wi);
            
        }
        
        //set attributes for functions modules, including those on essentials palette.
        if (layout->hasTagName("FUNCTION"))
        {
            savedFunctionsModuleInfo funcInfo;
            funcInfo.paletteName = layout->getStringAttribute ("paletteName");
            funcInfo.moduleName = layout->getStringAttribute ("moduleName");
            funcInfo.topLeftX = layout->getIntAttribute ("topleftx");
            funcInfo.topLeftY = layout->getIntAttribute ("toplefty");
            
            //retrieve hot key info
            if(funcInfo.moduleName.upToFirstOccurrenceOf("_", false, false) == "HotKeysComponent")
            {
                funcInfo.isHotKey = true;
                
                forEachXmlChildElement (*layout, hotButton)
                {
                    if (!hotButton->hasTagName("HOTBUTTON")) continue;
                    int hotKeyIndex = hotButton->getIntAttribute ("labelindex");
                    funcInfo.hotKeyLabel[hotKeyIndex] = hotButton->getStringAttribute ("labeltext");
                    funcInfo.hotkeyCode[hotKeyIndex] = hotButton->getStringAttribute ("keycode");
                }
            }
            functionsLayout.add(funcInfo);
        }
    }
    
    //return data structure of randWindows layout, floatingWindows layout, and functionsModules layout
    windowManager->setSavedRackWindowLayout(rackWindowLayout);
    windowManager->setSavedFloatingWindowLayout(floatingWindowLayout);
    functionsManager->setSavedFunctionsModuleLayout(functionsLayout);
    
    layoutElement->deleteAllChildElements();
    delete layoutElement;
    
    RAVEN_TOUCH_UNLOCK
}

