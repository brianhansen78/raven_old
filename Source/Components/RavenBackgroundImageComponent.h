//
//  RavenSettingsComponent.h
//  Raven
//
//  Created by Brian Hansen on 6/23/13.
//
//

#ifndef Raven_BackgroundImageComponent_h
#define Raven_BackgroundImageComponent_h

#include "RavenConfig.h"
#include "RavenTextDisplay.h"
#include "RavenWindowComponent.h"

class RavenBackgroundImageComponent : public RavenWindowComponent
{
public:
    RavenBackgroundImageComponent()
    {
        panelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "RailRack-Images/Rail-Rack/raven-background.png"));
        setSize(panelImg.getWidth(), panelImg.getHeight());
        
        mouseHider = new MouseHideComponent(getWidth(), getHeight());
        mouseHider->setName("MouseHider");
        addAndMakeVisible(mouseHider);
        
    }
    ~RavenBackgroundImageComponent(){}
    
    void buttonStateChanged (Button* b){};    
    void buttonClicked (Button* b){};
    
    void paint(Graphics &g)
    {
        g.drawImageAt(panelImg, 0, 0);
    }
    
private:
        
    Image panelImg;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenBackgroundImageComponent)
};

#endif
