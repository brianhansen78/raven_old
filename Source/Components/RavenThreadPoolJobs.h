//
//  RavenThreadPoolJobs.h
//  Raven
//
//  Created by Ryan McGee on 3/9/13.
//
//

#ifndef Raven_RavenThreadPoolJobs_h
#define Raven_RavenThreadPoolJobs_h

#include "RavenMixerComponent.h"
#include "RavenSendsComponent.h"

class FaderTouchThreadPoolJob : public ThreadPoolJob, public MessageListener
{
public:
    FaderTouchThreadPoolJob(RavenMixerComponent *_mixer, int _bank) : ThreadPoolJob("Fader Touch Thread Pool"), mixer(_mixer), bank(_bank) {}
    
    void handleMessage (const Message &message)
    {
        RavenMessage *m = (RavenMessage*)&message;
        if (m->type == eUpdateFaderBankFromTouch)
        {
            for(int c = 0; c < chansPerBank; c++)
            {
                mixer->getBank(bank)->getChannel(c)->getFader()->updateTouchThread();
            }
        }
    }
    
    JobStatus runJob()
    {
        
        Thread::sleep (RAVEN_FADER_THREAD_MS);
        
        RavenMessage *m = new RavenMessage(eUpdateFaderBankFromTouch);
        postMessage(m);
            
        return jobNeedsRunningAgain;
    }
private:
    RavenMixerComponent *mixer;
    int bank;
    
};

class SendsFaderTouchThreadPoolJob : public ThreadPoolJob, public MessageListener
{
public:
    SendsFaderTouchThreadPoolJob(RavenSendsComponent *_sends, int _bank) : ThreadPoolJob("Sends Fader Touch Thread Pool"), sends(_sends), bank(_bank) {}
    
    void handleMessage (const Message &message)
    {
        RavenMessage *m = (RavenMessage*)&message;
        if (m->type == eUpdateFaderBankFromTouch)
        {
            for(int c = 0; c < chansPerBank; c++)
            {
                sends->getBank(bank)->getChannel(c)->getFader()->updateTouchThread();
            }
        }
    }
    
    JobStatus runJob()
    {
        
        Thread::sleep (RAVEN_FADER_THREAD_MS);
        
        RavenMessage *m = new RavenMessage(eUpdateFaderBankFromTouch);
        postMessage(m);
        
        return jobNeedsRunningAgain;
    }
private:
    RavenSendsComponent *sends;
    int bank;
    
};

class FaderNSCThreadPoolJob : public ThreadPoolJob, public MessageListener
{
public:
    FaderNSCThreadPoolJob(RavenMixerComponent *_mixer, int _bank) : ThreadPoolJob("Fader NSC Thread Pool"), mixer(_mixer), bank(_bank) {}
    
    void handleMessage (const Message &message)
    {
        RavenMessage *m = (RavenMessage*)&message;
        if (m->type == eUpdateFaderBankFromNSC)
        {
            for(int c = 0; c < chansPerBank; c++)
            {
                mixer->getBank(bank)->getChannel(c)->getFader()->updateNSCThread();
            }
        }
    }
    
    JobStatus runJob()
    {
        
        Thread::sleep (RAVEN_METER_THREAD_MS);
        
        RavenMessage *m = new RavenMessage(eUpdateFaderBankFromNSC);
        postMessage(m);
        
        /* old CPU hogging way to send thread-safe messages...
         const MessageManagerLock mml (this);
         if (mml.lockWasGained())
         {
            for(int c = 0; c < chansPerBank; c++)
            {
                mixer->getBank(bank)->getChannel(c)->getFader()->updateNSCThread();
            }
         }
         */
        
        return jobNeedsRunningAgain;
    }
private:
    RavenMixerComponent *mixer;
    int bank;
};

class SendsFaderNSCThreadPoolJob : public ThreadPoolJob, public MessageListener
{
public:
    SendsFaderNSCThreadPoolJob(RavenSendsComponent *_sends, int _bank) : ThreadPoolJob("Sends Fader NSC Thread Pool"), sends(_sends), bank(_bank) {}
    
    void handleMessage (const Message &message)
    {
        RavenMessage *m = (RavenMessage*)&message;
        if (m->type == eUpdateFaderBankFromNSC)
        {
            for(int c = 0; c < chansPerBank; c++)
            {
                sends->getBank(bank)->getChannel(c)->getFader()->updateNSCThread();
            }
        }
    }
    
    JobStatus runJob()
    {
        Thread::sleep (RAVEN_METER_THREAD_MS);
        
        RavenMessage *m = new RavenMessage(eUpdateFaderBankFromNSC);
        postMessage(m);
        
        return jobNeedsRunningAgain;
    }
private:
    RavenSendsComponent *sends;
    int bank;
};

class MeterThreadPoolJob : public ThreadPoolJob, public MessageListener
{
public:
    MeterThreadPoolJob(RavenMixerComponent *_mixer, int _bank) : ThreadPoolJob("Meter Thread Pool"), mixer(_mixer), bank(_bank) {}
    
    void handleMessage (const Message &message)
    {
        RavenMessage *m = (RavenMessage*)&message;
        if (m->type == eUpdateMeterLevels)
        {
            for(int c = 0; c < chansPerBank; c++)
            {
                mixer->getBank(bank)->getChannel(c)->getMeterL()->updateLevel();
                mixer->getBank(bank)->getChannel(c)->getMeterR()->updateLevel();
            }
        }
    }
    
    JobStatus runJob()
    {
        
        Thread::sleep (RAVEN_METER_THREAD_MS);
        
        RavenMessage *m = new RavenMessage(eUpdateMeterLevels);
        postMessage(m);
        
        return jobNeedsRunningAgain;
    }
private:
    RavenMixerComponent *mixer;
    int bank;
    
};

#if RAVEN_ADD_TRANSPARENT_MIXER
class TransparentFaderTouchThreadPoolJob : public ThreadPoolJob, public MessageListener
{
public:
    TransparentFaderTouchThreadPoolJob(RavenTransparentMixerComponent *_mixer, int _bank) : ThreadPoolJob("Transparent Fader Touch Thread Pool"), transparentMixer(_mixer), bank(_bank) {}
    
    void handleMessage (const Message &message)
    {
        RavenMessage *m = (RavenMessage*)&message;
        if (m->type == eUpdateFaderBankFromTouch)
        {
            for(int c = 0; c < chansPerBank; c++)
            {
                transparentMixer->getBank(bank)->getChannel(c)->getFader()->updateTouchThread();
            }
        }
    }
    
    JobStatus runJob()
    {
        
        Thread::sleep (RAVEN_FADER_THREAD_MS);
        
        RavenMessage *m = new RavenMessage(eUpdateFaderBankFromTouch);
        postMessage(m);
        
        return jobNeedsRunningAgain;
    }
private:
    RavenTransparentMixerComponent *transparentMixer;
    int bank;
    
};



class TransparentFaderNSCThreadPoolJob : public ThreadPoolJob, public MessageListener
{
public:
    TransparentFaderNSCThreadPoolJob(RavenTransparentMixerComponent *_mixer, int _bank) : ThreadPoolJob("Fader NSC Thread Pool"), transparentMixer(_mixer), bank(_bank) {}
    
    void handleMessage (const Message &message)
    {
        RavenMessage *m = (RavenMessage*)&message;
        if (m->type == eUpdateFaderBankFromNSC)
        {
            for(int c = 0; c < chansPerBank; c++)
            {
                transparentMixer->getBank(bank)->getChannel(c)->getFader()->updateNSCThread();
            }
        }
    }
    
    JobStatus runJob()
    {
        
        Thread::sleep (RAVEN_METER_THREAD_MS);
        
        RavenMessage *m = new RavenMessage(eUpdateFaderBankFromNSC);
        postMessage(m);
        
        /* old CPU hogging way to send thread-safe messages...
         const MessageManagerLock mml (this);
         if (mml.lockWasGained())
         {
         for(int c = 0; c < chansPerBank; c++)
         {
         mixer->getBank(bank)->getChannel(c)->getFader()->updateNSCThread();
         }
         }
         */
        
        return jobNeedsRunningAgain;
    }
private:
    RavenTransparentMixerComponent *transparentMixer;
    int bank;
};
#endif




//
//class LargeMeterThreadPoolJob : public ThreadPoolJob, public MessageListener
//{
//public:
//    LargeMeterThreadPoolJob(RavenSendsComponent *_sends, int _bank) : ThreadPoolJob("Large Meter Thread Pool"), sends(_sends), bank(_bank) {}
//    
//    void handleMessage (const Message &message)
//    {
//        RavenMessage *m = (RavenMessage*)&message;
//        if (m->type == eUpdateLargeMeterLevels)
//        {
//            for(int c = 0; c < chansPerBank; c++)
//            {
//                sends->getBank(bank)->getChannel(c)->getLargeMeters()->meterL->updateLevel();
//                sends->getBank(bank)->getChannel(c)->getLargeMeters()->meterR->updateLevel();
//            }
//        }
//    }
//    
//    JobStatus runJob()
//    {
//        
//        Thread::sleep (RAVEN_METER_THREAD_MS);
//        
//        RavenMessage *m = new RavenMessage(eUpdateLargeMeterLevels);
//        postMessage(m);
//        
//        return jobNeedsRunningAgain;
//    }
//private:
//    RavenSendsComponent *sends;
//    int bank;
//    
//};

#endif
