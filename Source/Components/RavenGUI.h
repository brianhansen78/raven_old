

#ifndef Raven_RavenGUI_h
#define Raven_RavenGUI_h

#include "../JuceLibraryCode/JuceHeader.h"

#define INSERTS_X_OFFSET 18
#define SENDS_X_OFFSET 10
#define MIXER_X_OFFSET 4

#define GUI_PATH File::getSpecialLocation(File::currentApplicationFile).getFullPathName() + "/Contents/Resources/Raven_GUI_Resources/"

class RavenGUI
{
public:
    
    static String getMixerFaderPath(int bank, int chan)
    {
        int faderNum = (bank*8)+(chan+1); //1 to 24
        
        String path;
        if(faderNum < 10)   path = String(GUI_PATH) + "MIXERGRAPHICS/MIXER/MIXERFADERS/V1FADERS/images/FadersMixer_0" + String(faderNum) + ".png";
        else                path = String(GUI_PATH) + "MIXERGRAPHICS/MIXER/MIXERFADERS/V1FADERS/images/FadersMixer_" + String(faderNum) + ".png";
        
        return path;
        //return String(GUI_PATH) + "MIXERGRAPHICS/MIXER/MIXERFADERS/fadercap.png";
    }
    
    static String getSendFaderPath(int bank, int chan)
    {
        int faderNum = (bank*8)+(chan+1); //1 to 24
        
        String path;
        if(faderNum < 10)   path = String(GUI_PATH) + "MIXERGRAPHICS/SENDS/FADERS/FADERS/images/FadersSends_0" + String(faderNum) + ".png";
        else                path = String(GUI_PATH) + "MIXERGRAPHICS/SENDS/FADERS/FADERS/images/FadersSends_" + String(faderNum) + ".png";
        
        return path;
    }
    
    static String getMixerKnobBGPath(int knobNum)
    {
        String path = String(GUI_PATH) + "MIXERGRAPHICS/MIXER/MIXERMONOKNOBPAN/background/images/MixerPan_background_";
        if(knobNum < 10) path = path + "0";
        path = path + String(knobNum) + ".png";
        
        return path;
    }
    
    /*
    static String getTrackNameBGPath(int chanNum)
    {
        String path = String(GUI_PATH) + "MIXERGRAPHICS/MIXER/MIXERMONOKNOBPAN/background/images/MixerPan_background_";
        if(chanNum < 10) path = path + "0";
        path = path + String(knobNum) + ".png";
        
        return path;
    }
     */
    
    static String getSendKnobBGPath(int knobNum)
    {
        String path = String(GUI_PATH) + "MIXERGRAPHICS/SENDS/SENDSMONOKNOBPAN/background/images/sendsPan_background_";
        if(knobNum < 10) path = path + "0";
        path = path + String(knobNum) + ".png";
        
        return path;
    }
    
    static String getMixerKnobPath(bool stereo, int bank, int chan)
    {
        
        int knobNum = (bank*8)+(chan+1); //1 to 24
        String path = String(GUI_PATH) + "MIXERGRAPHICS/MIXER/MIXERMONOKNOBPAN/move/images/MixPan1ch_";
        if(knobNum < 10) path = path + "0";
        path = path + String(knobNum) + ".png";
        
        return path;
    }
    
    static String getSendKnobPath(bool stereo, int bank, int chan)
    {
        
        int knobNum = (bank*8)+(chan+1); //1 to 24
        String path = String(GUI_PATH) + "MIXERGRAPHICS/SENDS/SENDSMONOKNOBPAN/move/images/SendPan1ch_";
        if(knobNum < 10) path = path + "0";
        path = path + String(knobNum) + ".png";
        
        return path;
    }
    
    static String getMixerMeterPath(bool stereo, bool on, int bank, int chan)
    {
        int faderNum = (bank*8)+(chan+1); //1 to 24
        int nameNum = 2*faderNum + 1;
        
        String path;
                
        if(on)  path = String(GUI_PATH) + "MIXERGRAPHICS/MIXER/MIXERLEDMETER/ON/";
        else    path = String(GUI_PATH) + "MIXERGRAPHICS/MIXER/MIXERLEDMETER/OFF/";   
        
        if(stereo && on)        path = path + "STEREO/images/LedMeter-2ch-ON_";
        else if(stereo && !on)  path = path + "STEREO/images/LedMeter-2ch-OFF_";
        else if (!stereo && on) path = path + "MONO/images/LedMeter-1ch-ON_";
        else if(!stereo && !on) path = path + "MONO/images/LedMeter-1ch-OFF_";
        
        if(nameNum < 10) path = path + "0";
        path = path + String(nameNum) + ".png";
        
//        if(on) path = String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixLedMeter/LedMeter-On.png";
//        else path = String(GUI_PATH) + "FLAT_GRAPHICS/MIXER_AND_SEND/Pictures/MixLedMeter/LedMeter-Off.png";
        
        return path;
    }
    
    static String getMixerRecButtonPath(bool on, int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1) - 1; //0 to 23
        int nameNum = 28 + 2*chanNum;
        
        String path;
        if(on)  path = "MIXERGRAPHICS/MIXER/BUTTONS/ON/images/MixerButtons-ON_" + String(nameNum) + ".png";
        else    path = "MIXERGRAPHICS/MIXER/BUTTONS/OFF/images/MixerButtons-OFF_" + String(nameNum) + ".png";
        
        return path;
    }
    
    static String getMixerSoloButtonPath(bool on, int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1) - 1; //0 to 23
        int nameNum = 76 + 2*chanNum;
        
        String path;
        if(on)  path = "MIXERGRAPHICS/MIXER/BUTTONS/ON/images/MixerButtons-ON_" + String(nameNum) + ".png";
        else    path = "MIXERGRAPHICS/MIXER/BUTTONS/OFF/images/MixerButtons-OFF_" + String(nameNum) + ".png";
        
        return path;
    }
    
    static String getMixerInsertButtonPath(bool on, int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1) - 1; //0 to 23
        int nameNum = 29 + 2*chanNum;
        
        String path;
        if(on)  path = "MIXERGRAPHICS/MIXER/BUTTONS/ON/images/MixerButtons-ON_" + String(nameNum) + ".png";
        else    path = "MIXERGRAPHICS/MIXER/BUTTONS/OFF/images/MixerButtons-OFF_" + String(nameNum) + ".png";
        
        return path;
    }
    
    static String getMixerMuteButtonPath(bool on, int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1) - 1; //0 to 23
        int nameNum = 77 + 2*chanNum;
        
        String path;
        if(on)  path = "MIXERGRAPHICS/MIXER/BUTTONS/ON/images/MixerButtons-ON_" + String(nameNum) + ".png";
        else    path = "MIXERGRAPHICS/MIXER/BUTTONS/OFF/images/MixerButtons-OFF_" + String(nameNum) + ".png";
        
        return path;
    }
    
    static String getSendsButtonPath(String name, bool on, int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1) - 1; //0 to 23
        int nameNum;
        if(name == "mute")      nameNum = 3 + 2*chanNum;
        else if(name == "a")    nameNum = 51 + chanNum;
        else if(name == "b")    nameNum = 75 + chanNum;
        else if(name == "c")    nameNum = 99 + chanNum;
        else if(name == "d")    nameNum = 123 + chanNum;
        else if(name == "e")    nameNum = 147 + chanNum;
        else if(name == "pre")  nameNum = 171 + chanNum;
    
        String path;
        if(on)  path = "MIXERGRAPHICS/SENDS/ON/images/Sends-Buttons-ON_";
        else    path = "MIXERGRAPHICS/SENDS/OFF/images/Sends-Buttons-OFF_";
        if(nameNum < 10) path = path + "0";
        path = path + String(nameNum) + ".png";
        
        return path;
    }
    
    static String getInsertButtonPath(String name, bool on, int bank, int chan, int insertIndex)
    {
        int chanIndex = (bank*8)+(chan+1) - 1; //0 to 23
        int insertNum = insertIndex + 1;
        String path;
        
        if(name == "bypass")
        {
            if(on)  path = "MIXERGRAPHICS/INSERTSV4/ONBYPASS/images/Chan";
            else    path = "MIXERGRAPHICS/INSERTSV4/OFFBYPASS/images/Chan"; 
            if(chanIndex < 10) path = path + "0";
            path = path + String(chanIndex) + "-ins0" + String(insertNum) + "-byp.png";
        }
        
        else if(name == "display")
        {
            if(on)  path = "MIXERGRAPHICS/INSERTSV4/ONDISPLAY/images/Chan"; //1to5-ONBYP_";
            else    path = "MIXERGRAPHICS/INSERTSV4/OFFDISPLAY/images/Chan"; //1to5-ONBYP_";
            if(chanIndex < 10) path = path + "0";
            path = path + String(chanIndex) + "-ins0" + String(insertNum) + "-disp.png";
        }
        
        return path;
    }
    
    static String getMixerAutoOffButtonPath(bool on, int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1) - 1; //0 to 23
        int nameNum = 3 + chanNum;
        
        String path;
        if(on)
        {
            if(nameNum < 10)    path = "MIXERGRAPHICS/MIXER/BUTTONS/ON/images/MixerButtons-ON_0" + String(nameNum) + ".png";
            else                path = "MIXERGRAPHICS/MIXER/BUTTONS/ON/images/MixerButtons-ON_" + String(nameNum) + ".png";
        }
        else
        {
            if(nameNum < 10)    path = "MIXERGRAPHICS/MIXER/BUTTONS/OFF/images/MixerButtons-OFF_0" + String(nameNum) + ".png";
            else                path = "MIXERGRAPHICS/MIXER/BUTTONS/OFF/images/MixerButtons-OFF_" + String(nameNum) + ".png";
        }
        return path;
    }
    
    static String getAutoModeButtonPath(String name, int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1) - 1; //0 to 23
        int nameNum = 3 + chanNum;
        
        String path = "MIXERGRAPHICS/MIXER/BUTTONS/AUTOVARIATIONS/images/Auto-";
        if(name == "latch")         path = path + "Latch_";
        else if(name == "read")    path = path + "Read_";
        else if(name == "touch")    path = path + "Touch_";
        else if(name == "write")    path = path + "Write_";
        else if(name == "trim")    path = path + "Trim_";
        
        if(nameNum < 10) path = path + "0";
        path = path + String(nameNum) + ".png";
        
        return path;
    }
    
    static String getAutoModeButtonPath(String name, bool on)
    {
        String path = "MIXERGRAPHICS/MIXER/AUTOMATIONMENU/";
        if(on)  path = path + "ON/images/";
        else    path = path + "OFF/images/";
        
        if(name == "latch")         path = path + "latch";
        if(name == "off")         path = path + "off";
        else if(name == "read")    path = path + "read";
        else if(name == "touch")    path = path + "touch";
        else if(name == "write")    path = path + "write";
        else if(name == "trim")    path = path + "trim";
        
        path = path + ".png";
        
        return path;
    }
    
    static int getSendChannelWidth(int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1); //1 to 24
        
        int width = 78;
        
        switch (chanNum) {
            case 1: width = 78;
                break;
            case 2: width = 79;
                break;
            case 3: width = 79;
                break;
            case 4: width = 79;
                break;
            case 5: width = 79;
                break;
            case 6: width = 79;
                break;
            case 7: width = 79;
                break;
            case 8: width = 79;
                break;
            case 9: width = 79;
                break;
            case 10: width = 80;
                break;
            case 11: width = 79;
                break;
            case 12: width = 79;
                break;
            case 13: width = 79;
                break;
            case 14: width = 78;
                break;
            case 15: width = 80;
                break;
            case 16: width = 79;
                break;
            case 17: width = 77;
                break;
            case 18: width = 79;
                break;
            case 19: width = 78;
                break;
            case 20: width = 80;
                break;
            case 21: width = 79;
                break;
            case 22: width = 78;
                break;
            case 23: width = 80;
                break;
            case 24: width = 95;
                break;
            default: width = 78;
                break;
        }
        
        return width;
    }

    
    static int getMixerChannelWidth(int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1); //1 to 24
        
        int width = 76;
        
        switch (chanNum) {
            case 1: width = 80;//76;
                break;
            case 2: width = 79;//82;
                break;
            case 3: width = 80;//82;
                break;
            case 4: width = 79;//82;
                break;
            case 5: width = 80;
                break;
            case 6: width = 79;//82;
                break;
            case 7: width = 79;
                break;
            case 8: width = 80;//82;
                break;
            case 9: width = 79;//76;
                break;
            case 10: width = 80;//85;
                break;
            case 11: width = 79;//80;
                break;
            case 12: width = 79;//80;
                break;
            case 13: width = 79;//82;
                break;
            case 14: width = 80;
                break;
            case 15: width = 79;//81;
                break;
            case 16: width = 79;//82;
                break;
            case 17: width = 79;
                break;
            case 18: width = 80;//82;
                break;
            case 19: width = 80;
                break;
            case 20: width = 79;//76;
                break;
            case 21: width = 80;//85;
                break;
            case 22: width = 79;
                break;
            case 23: width = 79;//78;
                break;
            case 24: width = 94;//70; //1920 - rest (for fader based)
                break;
            default: width = 80;
                break;
        }
        
        return width;
    }
    
    static int getMixerFaderXOffset(int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1); //1 to 24
        
        int xOffset = 0;
        
        switch (chanNum) {

            case 7: xOffset = 1;
                break;
            case 8: xOffset = 1;
                break;
            case 9: xOffset = 3;
                break;
            case 11: xOffset = 5;
                break;
            case 12: xOffset = 6;
                break;
            case 13: xOffset = 7;
                break;
            case 14: xOffset = 10;
                break;
            case 15: xOffset = 10;
                break;
            case 16: xOffset = 12;
                break;
            case 17: xOffset = 15;
                break;
            case 18: xOffset = 15;
                break;
            case 19: xOffset = 17;
                break;
            case 20: xOffset = 17;
                break;
            case 21: xOffset = 14;
                break;
            case 22: xOffset = 19;
                break;
            case 23: xOffset = 19;
                break;
            case 24: xOffset = 18;
                break;
            default: xOffset = 0;
                break;
        }
        
        return xOffset+2;  //changed start offset from 6 to 4...
    }
    
    static int getMixerFaderTrim(int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1); //1 to 24
        
        int trim = 0;
        
        switch (chanNum) {
            case 1: trim = 6;//80;//76;
                break;
            case 2: trim = 10;//79;//82;
                break;
            case 3: trim = 7;//80;//82;
                break;
            case 4: trim = 5;//79;//82;
                break;
            case 5: trim = 2;//80;
                break;
            case 6: trim = 2;//79;//82;
                break;
            case 7: trim = -1;//79;
                break;
            case 8: trim = -1;//80;//82;
                break;
            case 9: trim = -3;//79;//76;
                break;
            case 10: trim = 0;//80;//85;
                break;
            case 11: trim = -5;//79;//80;
                break;
            case 12: trim = -6;//79;//80;
                break;
            case 13: trim = -7;//79;//82;
                break;
            case 14: trim = -10;//80;
                break;
            case 15: trim = -10;//79;//81;
                break;
            case 16: trim = -12;//79;//82;
                break;
            case 17: trim = -15;//79;
                break;
            case 18: trim = -15;//80;//82;
                break;
            case 19: trim = -17;//80;
                break;
            case 20: trim = -17;//79;//76;
                break;
            case 21: trim = -14;//80;//85;
                break;
            case 22: trim = -19;//79;
                break;
            case 23: trim = -19;//79;//78;
                break;
            case 24: trim = -18;//80;//70; //1920 - rest (for fader based)
                break;
            default: trim = 0;
                break;
        }
        
        if(trim < 0)    trim = 0;
        else            trim = trim + 2;
        
        return trim;
    }

    static int getMixerMeterXOffset(int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1); //1 to 24
        
        int xOffset = 0;
        
        if(chanNum >= 8 && chanNum <= 16)       xOffset = 2;
        else if(chanNum >= 17 && chanNum <= 19) xOffset = 5;
        else if(chanNum >= 20 && chanNum <= 23) xOffset = 2;
        else if(chanNum == 24)                  xOffset = 6;
        
        return xOffset;
    }
    
    static int getMixerMeterTrim(int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1); //1 to 24
        
        int trim = 0;
        
        switch (chanNum) {
            case 2: trim = 1;
                break;
            case 3: trim = 1;
                break;
            case 4: trim = 2;
                break;
            case 5: trim = 1;
                break;
            case 6: trim = 1;
                break;
            case 7: trim = 1;
                break;
            
            default: trim = 0;
                break;
        }
        
        return trim;
    }

    static int getMixerKnobXOffset(int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1); //1 to 24
        
        int base = 15;
        int xOffset = base;
        
        switch (chanNum) {
                
            case 1: xOffset = base + 1;
                break;
            case 2: xOffset = base;
                break;
            case 3: xOffset = base;
                break;
            case 4: xOffset = base + 1;
                break;
            case 5: xOffset = base + 2;
                break;
            case 6: xOffset = base + 2;
                break;
            case 7: xOffset = base + 1;
                break;
            case 8: xOffset = base + 2;
                break;
            case 9: xOffset = base + 2;
                break;
            case 10: xOffset = base + 2;
                break;
            case 11: xOffset = base + 3;
                break;
            case 12: xOffset = base + 3;
                break;
            case 13: xOffset = base + 4;
                break;
            case 14: xOffset = base + 5;
                break;
            case 15: xOffset = base + 4;
                break;
            case 16: xOffset = base + 5;
                break;
            case 17: xOffset = base + 6;
                break;
            case 18: xOffset = base + 6;
                break;
            case 19: xOffset = base + 7;
                break;
            case 20: xOffset = base + 6;
                break;
            case 21: xOffset = base + 8;
                break;
            case 22: xOffset = base + 7;
                break;
            case 23: xOffset = base + 8;
                break;
            case 24: xOffset = base + 8;
                break;
                
            default: xOffset = 0;
                break;
        }
        
        return xOffset;
    }

    static int getSendFaderXOffset(int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1); //1 to 24
        
        int base = 26;
        int xOffset = base;
        
        switch (chanNum) {
                
            case 1: xOffset = base;
                break;
            case 2: xOffset = getSendFaderXOffset(0, 0) + 76 - getSendChannelWidth(0, 0);
                break;
            case 3: xOffset = getSendFaderXOffset(0, 1) + 82 - getSendChannelWidth(0, 1);
                break;
            case 4: xOffset = getSendFaderXOffset(0, 2) + 76 - getSendChannelWidth(0, 2);
                break;
            case 5: xOffset = getSendFaderXOffset(0, 3) + 81 - getSendChannelWidth(0, 3);
                break;
            case 6: xOffset = getSendFaderXOffset(0, 4) + 82 - getSendChannelWidth(0, 4);
                break;
            case 7: xOffset = getSendFaderXOffset(0, 5) + 80 - getSendChannelWidth(0, 5);
                break;
            case 8: xOffset = getSendFaderXOffset(0, 6) + 77 - getSendChannelWidth(0, 6);
                break;
            case 9: xOffset = getSendFaderXOffset(0, 7) + 81 - getSendChannelWidth(0, 7);
                break;
            case 10: xOffset = getSendFaderXOffset(1, 0) + 76 - getSendChannelWidth(1, 0);
                break;
            case 11: xOffset = getSendFaderXOffset(1, 1) + 84 - getSendChannelWidth(1, 1);
                break;
            case 12: xOffset = getSendFaderXOffset(1, 2) + 78 - getSendChannelWidth(1, 2);
                break;
            case 13: xOffset = getSendFaderXOffset(1, 3) + 77 - getSendChannelWidth(1, 3);
                break;
            case 14: xOffset = getSendFaderXOffset(1, 4) + 80 - getSendChannelWidth(1, 4);
                break;
            case 15: xOffset = getSendFaderXOffset(1, 5) + 78 - getSendChannelWidth(1, 5);
                break;
            case 16: xOffset = getSendFaderXOffset(1, 6) + 78 - getSendChannelWidth(1, 6);
                break;
            case 17: xOffset = getSendFaderXOffset(1, 7) + 82 - getSendChannelWidth(1, 7);
                break;
            case 18: xOffset = getSendFaderXOffset(2, 0) + 80 - getSendChannelWidth(2, 0);
                break;
            case 19: xOffset = getSendFaderXOffset(2, 1) + 77 - getSendChannelWidth(2, 1);
                break;
            case 20: xOffset = getSendFaderXOffset(2, 2) + 79 - getSendChannelWidth(2, 2);
                break;
            case 21: xOffset = getSendFaderXOffset(2, 3) + 83 - getSendChannelWidth(2, 3);
                break;
            case 22: xOffset = getSendFaderXOffset(2, 4) + 77 - getSendChannelWidth(2, 4);
                break;
            case 23: xOffset = getSendFaderXOffset(2, 5) + 79 - getSendChannelWidth(2, 5);
                break;
            case 24: xOffset = getSendFaderXOffset(2, 6) + 83 - getSendChannelWidth(2, 6);
                break; //*/
                
            default: xOffset = base;
                break;
        }
                
        return xOffset;
    }

    static int getSendKnobXOffset(int bank, int chan)
    {
        int chanNum = (bank*8)+(chan+1); //1 to 24
        
        int base = 14;
        int xOffset = base;
        
        switch (chanNum) {
                
            case 1: xOffset = base;
                break;
            case 2: xOffset = getSendKnobXOffset(0, 0) + 79 - getSendChannelWidth(0, 0);
                break;
            case 3: xOffset = getSendKnobXOffset(0, 1) + 79 - getSendChannelWidth(0, 2);
                break;
            case 4: xOffset = getSendKnobXOffset(0, 2) + 79 - getSendChannelWidth(0, 3);
                break;
            case 5: xOffset = getSendKnobXOffset(0, 3) + 80 - getSendChannelWidth(0, 4);
                break;
            case 6: xOffset = getSendKnobXOffset(0, 4) + 78 - getSendChannelWidth(0, 5);
                break;
            case 7: xOffset = getSendKnobXOffset(0, 5) + 79 - getSendChannelWidth(0, 5);
                break;
            case 8: xOffset = getSendKnobXOffset(0, 6) + 80 - getSendChannelWidth(0, 6);
                break;
            case 9: xOffset = getSendKnobXOffset(0, 7) + 80 - getSendChannelWidth(0, 7);
                break;
            case 10: xOffset = getSendKnobXOffset(1, 0) + 79 - getSendChannelWidth(1, 0);
                break;
            case 11: xOffset = getSendKnobXOffset(1, 1) + 79 - getSendChannelWidth(1, 1);
                break;
            case 12: xOffset = getSendKnobXOffset(1, 2) + 79 - getSendChannelWidth(1, 2);
                break;
            case 13: xOffset = getSendKnobXOffset(1, 3) + 78 - getSendChannelWidth(1, 3);
                break;
            case 14: xOffset = getSendKnobXOffset(1, 4) + 80 - getSendChannelWidth(1, 4);
                break;
            case 15: xOffset = getSendKnobXOffset(1, 5) + 79 - getSendChannelWidth(1, 5);
                break;
            case 16: xOffset = getSendKnobXOffset(1, 6) + 78 - getSendChannelWidth(1, 6);
                break;
            case 17: xOffset = getSendKnobXOffset(1, 7) + 79 - getSendChannelWidth(1, 7);
                break;
            case 18: xOffset = getSendKnobXOffset(2, 0) + 80 - getSendChannelWidth(2, 0);
                break;
            case 19: xOffset = getSendKnobXOffset(2, 1) + 79 - getSendChannelWidth(2, 1);
                break;
            case 20: xOffset = getSendKnobXOffset(2, 2) + 79 - getSendChannelWidth(2, 2);
                break;
            case 21: xOffset = getSendKnobXOffset(2, 3) + 80 - getSendChannelWidth(2, 3);
                break;
            case 22: xOffset = getSendKnobXOffset(2, 4) + 78 - getSendChannelWidth(2, 4);
                break;
            case 23: xOffset = getSendKnobXOffset(2, 5) + 80 - getSendChannelWidth(2, 5);
                break;
            case 24: xOffset = getSendKnobXOffset(2, 6) + 80 - getSendChannelWidth(2, 6);
                break;
                
            default: xOffset = 0;
                break;
        }
        
        return xOffset;
    }

    
};


#endif
