

#ifndef Raven_RavenUtilitiesComponent_h
#define Raven_RavenUtilitiesComponent_h

#include "RavenButton.h"
#include "RavenFunctionsComponent.h"
#include "RavenTouchComponent.h"

class RavenUtilitiesComponent : public RavenTouchComponent, public Button::Listener
{
public:
    RavenUtilitiesComponent(RavenFunctionsContainer *owner)
    {
        
        int xPos = 0;
        
        RavenNSCButton *saveButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_1(saveButton);
        saveButton->setTopLeftPosition(xPos, 0);
        saveButton->addListener(owner);
        saveButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Utilities/images/utilities-on_01.png", "TOOLBAR/FUNCTIONMODULE/Utilities/images/utilities-off_01.png");
        saveButton->setTag(eNSCGSUtilitiesSaveSwitch);
        ravenNSCTouchButtons.add(saveButton);
        addAndMakeVisible(saveButton);
        xPos += saveButton->getWidth();
        
        RavenNSCButton *undoButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_1(undoButton);
        undoButton->setTopLeftPosition(xPos, 0);
        undoButton->addListener(owner);
        undoButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Utilities/images/utilities-on_02.png", "TOOLBAR/FUNCTIONMODULE/Utilities/images/utilities-off_02.png");
        undoButton->setTag(eNSCGSUtilitiesUndoSwitch);
        ravenNSCTouchButtons.add(undoButton);
        addAndMakeVisible(undoButton);
        xPos += undoButton->getWidth();
        
        RavenNSCButton *redoButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_1(redoButton);
        redoButton->setTopLeftPosition(xPos, 0);
        redoButton->setName("redoButton");
        redoButton->addListener(owner);
        redoButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Utilities/images/Bt-utilities-Redo-On.png", "TOOLBAR/FUNCTIONMODULE/Utilities/images/Bt-utilities-Redo-Off.png");
        redoButton->setTag(eNSCGSUtilitiesRedoSwitch);
        ravenNSCTouchButtons.add(redoButton);
        addAndMakeVisible(redoButton);
        xPos += redoButton->getWidth();
        
        redoButton->getKeyCode()->push_back(55);//command
        redoButton->getKeyCode()->push_back(56);//shift
        redoButton->getKeyCode()->push_back(6);//Z
        
        RavenNSCButton *cancelescButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_1(cancelescButton);
        cancelescButton->setTopLeftPosition(xPos, 0);
        cancelescButton->addListener(this);
        cancelescButton->setChannelIndex(kNscGuiKeyboardIdentifier);
        cancelescButton->addListener(owner);
        cancelescButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Utilities/images/utilities-on_03.png", "TOOLBAR/FUNCTIONMODULE/Utilities/images/utilities-off_03.png");
        //cancelescButton->setTag(eNSCGSUtilitiesCancelSwitch); // use keyboard tag instead with setKeyboard(true)
        cancelescButton->setTag(eNSCKeyboard_Escape);
        ravenNSCTouchButtons.add(cancelescButton);
        addAndMakeVisible(cancelescButton);
        xPos += cancelescButton->getWidth();
        
        enterButton = new RavenNSCButton(owner);
        RAVEN_BUTTON_1(enterButton);
        enterButton->setTopLeftPosition(xPos, 0);
        enterButton->addListener(this);
        enterButton->setChannelIndex(kNscGuiKeyboardIdentifier);
        enterButton->addListener(owner);
        enterButton->setImagePaths("TOOLBAR/FUNCTIONMODULE/Utilities/images/utilities-on_04.png", "TOOLBAR/FUNCTIONMODULE/Utilities/images/utilities-off_04.png");
        //enterButton->setTag(eNSCGSUtilitiesEnterSwitch); // use keyboard tag instead with setKeyboard(true)
        enterButton->setTag(eNSCKeyboard_Enter); 
        ravenNSCTouchButtons.add(enterButton);
        addAndMakeVisible(enterButton);
        xPos += enterButton->getWidth();
        
        setSize(xPos, enterButton->getHeight());
        
    }
    
    RavenNSCButton* getEnterButton(){return enterButton;}
    
    void buttonStateChanged (Button*)
    {
        
    }
    void buttonClicked (Button* b)
    {
        //move raven window to back so pro tool window comes forth
        // need PT window in front for enter/cancel
        //Process::setDockIconVisible(false);
        //Process::setDockIconVisible(true);
        //Process::makeForegroundProcess();
    }
    
private:
    RavenNSCButton *enterButton;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenUtilitiesComponent)
    
};


#endif
