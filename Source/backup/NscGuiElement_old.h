/*
  ==============================================================================

    NscGuiElement.h
    Created: 24 Feb 2012 5:28:07am
    Author:  cjubien

  ==============================================================================
*/

#ifndef __NSCGUIELEMENT_H_3D9B0E26__
#define __NSCGUIELEMENT_H_3D9B0E26__

//#include "includes.h"
#include "../JuceLibraryCode/JuceHeader.h"
#include "NscSDKIncludes.h"

#define cNumberChannels 8			// 8 channels per bank

#define GUI_PATH "/Users/ryanmcgee/Code/Raven/GUI_Resources/"
//#define GUI_PATH (File::getCurrentWorkingDirectory()).getFullPathName() + "/GUI_Resources/"



/*
#define cMinChannelFader 1500
#define cMaxChannelFader (cMinChannelFader + cNumberChannels - 1)

#define cMinChannelSolo 1000
#define cMaxChannelSolo (cMinChannelSolo + cNumberChannels - 1)

#define cMinChannelRecord 2000
#define cMaxChannelRecord (cMinChannelRecord + cNumberChannels - 1)

#define cMinChannelMute 3000
#define cMaxChannelMute (cMinChannelMute + cNumberChannels - 1)

#define cMinChannelInput 3200
#define cMaxChannelInput (cMinChannelInput + cNumberChannels - 1)

#define cMinChannelSelect 4000
#define cMaxChannelSelect (cMinChannelSelect + cNumberChannels - 1)

#define cMinChannelSelectOverlay 4010
#define cMaxChannelSelectOverlay (cMinChannelSelectOverlay + cNumberChannels - 1)

#define cMinChannelAuto 5000
#define cMaxChannelAuto (cMinChannelAuto + cNumberChannels - 1)

#define cMinTransport 7000
#define cMaxTransport (cMinTransport + eGSTransportLast-eGSTransportFirst )

#define cMinGlobalSwitch cMinTransport
#define cMaxGlobalSwitch (cMinGlobalSwitch+999)

#define cMinChannelVSelect 8000
#define cMaxChannelVSelect (cMinChannelVSelect + cNumberChannels - 1)
#define cMinDSPSectionVPot 8008
#define cMaxDSPSectionVPot (cMinDSPSectionVPot + cNumberDSPVPots - 1)
#define cMinChannelSendButCubase 8200
#define cMaxChannelSendButCubase (cMinChannelSendButCubase + cNumberChannels - 1)
#define cMinChannelInputButCubase 8300
#define cMaxChannelInputButCubase (cMinChannelInputButCubase + cNumberChannels - 1)

#define cMinChannelMode 8400
#define cMaxChannelMode 8431

#define cMinMixerMode 8432
#define cMaxMixerMode 8463

#define cMinPlugInInsert 8464
#define cMaxPlugInInsert 8479
#define cPlugInBankLeft 8496
#define cPlugInBankRight 8497
#define cInstrumentBankLeft 8498
#define cInstrumentBankRight 8499

#define cMinPlugInState 8480
#define cMaxPlugInState 8495

#define cMinPlugInControl 8500
#define cMaxPlugInControl 8599

#define cMinInstrumentControl 8600
#define cMaxInstrumentControl 8699

#define cMinChannelInsert 6000
#define cMaxChannelInsert (cMinChannelInsert + cNumberChannels - 1)

#define cMinChannelEQ 6100
#define cMaxChannelEQ (cMinChannelEQ + cNumberChannels - 1)

#define cMinLeftMeter 9000
#define cMaxLeftMeter (cMinLeftMeter + cNumberChannels - 1)
#define cMinRightMeter 9010
#define cMaxRightMeter (cMinRightMeter + cNumberChannels - 1)

#define cClearMeters 280
#define cLoop 281

#define cOnOffSwitch 0

*/


#define kNscGuiIdentifier "nscg"


class NscGuiElement
{
public:
	NscGuiElement(class NscGuiContainer * owner);
	NscGuiElement(NscGuiContainer * owner, int tag);
	NscGuiElement(NscGuiContainer * owner, int bank, int channel, int tag);
	virtual ~NscGuiElement();

	virtual void	setChannelIndex(int index) {channelIndex = index;}
	virtual void	setBankIndex(int index) { bankIndex = index;}
	virtual void	setTag(int tag) { guiTag = tag;}

	int		getChannelIndex() { return channelIndex;}
	int		getBankIndex() { return bankIndex;}
	int		getTag() { return guiTag;}

	virtual void  updateNscValue(float value);			// callback when DAW changes a value; generally
	virtual void  updateNscValue(const String & value);	// intended to be overriden

protected:
	friend class NscGuiContainer;
	int		bankIndex;
	int		channelIndex;
	int		guiTag;


	// We have a minor kludge here.  The jucer does not display classes derived from as images without some extra work.
	// So, in order to facilitate the creation of components in the jucer, rather than using our Switch class, we're using ImageButtons
	// and then creating an NscGuiElement that we associate with each one.
	friend  class ChannelPT;
	void	* tempWorkaroundPtr;		// used by switches for now...


};




class NscGuiContainer
{
public:
	NscGuiContainer(int bank);
	virtual ~NscGuiContainer();

	void addGuiElement(NscGuiElement * element);
	bool handleUpdate(const struct SControlEvent& message);
	void setNscLink	(class NscLink * link_) { link = link_;};

	void 	sliderChanged (NscGuiElement *  slider, float sliderValue);
	void 	sliderBegan (NscGuiElement * slider);
	void 	sliderEnded (NscGuiElement * slider);

	// Button::Listener interface
    void	switchClicked (NscGuiElement * switchThatWasClicked, bool switchState);

	// nsc update callback (changed when change received from daw)
	virtual int callback(ENSCEventType type, int subType, int bank, int which, float val);
	virtual int callback(ENSCEventType type, int subType, int bank, int which, const char * text);

protected:
	NscGuiElement * findNscGuiElement(int tag);
	NscGuiElement * findNscGuiElement(int bank, int channel, int tag);
	int		bankNum;

	NscLink		* link;

private:
	Array < NscGuiElement *> elements;

};



#endif  // __NSCGUIELEMENT_H_3D9B0E26__
