#ifndef __RAVENCONTAINER_H_B6CC5B6__
#define __RAVENCONTAINER_H_B6CC5B6__

//JUCE
#include "../JuceLibraryCode/JuceHeader.h"

//Raven
#include "RavenMixerContainer.h"
#include "RavenFunctionsComponent.h"
#include "RavenLoadingWindow.h"
#include "RavenConfig.h"

//NSC
#include "NscSDKIncludes.h"

//Multi-Touch
#include "MTDefines.h"

const int numBanks = 3;
const int chansPerBank = 8;
const int viewWidth = 1920;

class RavenContainer : public Component, public TUIO::TuioListener, public NscAppDelegate
{
public:
    RavenContainer();
    ~RavenContainer();
    
    void setMainAppWindow(DocumentWindow *win) { mixerContainer->setMainAppWindow(win); }

    //TUIO
    TUIO::TuioClient *tuioClient;
    void addTuioCursor(TUIO::TuioCursor *tcur);
	void updateTuioCursor(TUIO::TuioCursor *tcur);
	void removeTuioCursor(TUIO::TuioCursor *tcur);
    bool componentTouched(Component *cmp, int touchX, int touchY);
    bool touchTimeLessThan(long &thisSec, long &thisUSec, long &lastSec, long &lastUSec, long timeUSec);
    //unused:
    void addTuioObject(TUIO::TuioObject *tobj){}
	void updateTuioObject(TUIO::TuioObject *tobj){}
	void removeTuioObject(TUIO::TuioObject *tobj){}
    void refresh(TUIO::TuioTime frameTime){}
    
    //Touches
    struct touch
    {
        EMTGestureType gesture;
        int moduleNum;
        bool isMixer, isSend, isCounter, isInsertsPalette, isNavPad, isFunction;
        bool isFader, isKnob, isButton, isModuleMove;
        int bank, chan;
    };
    touch touches[MAX_NUM_TOUCHES];
    
    // NscAppDelegate interface
	virtual int nscServerFound(const char* serverName, const char * domain);	// Called when a NeyFi server has been detected.  Pass arguments to connectToServer to establish connection.
	virtual int nscConnected(const char* serverName, const char * domain);		// Called once link to server has been established.  NeyFi link is valid only after this callback.
	virtual int nscDisconnected(const char* serverName, const char * domain);	// Called when NeyFi has stopped (regardless of whether or not a link to it has been formed via connectToServer).  link is no longer valid.
	virtual int callback(ENSCEventType type, int subType, int bank, int which, float val);
	virtual int callback(ENSCEventType type, int subType, int bank, int which, const std::string & data);
	virtual void setDawMode(EDAWMode mode);
    

        
private:
    
    NscLink *link;

    RavenMixerContainer *mixerContainer;
    RavenMixerComponent *mixer;
    RavenSendsComponent *sends;
    RavenInsertsComponent *inserts;
    RavenTrackNameComponent *names;
    RavenFunctionsComponent *functions;
        
    Array<Component*> modules;
    
    //void initializeInserts(int bank, const std::string & data, int insertNum);
    void exitInit();
        
    RavenNSCButton *assignButton;
        
    RavenLoadingWindow *loadingWindow;
    
    void forceNewHUI();
    
    bool touchesOn, initInserts, init5;
    TUIO::TuioPoint lastTapPos;
    TUIO::TuioTime lastTapTime;
    Time prevTime;
    int numAssignPresses;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenContainer)
};



#endif 
