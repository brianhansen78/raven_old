

#ifndef Raven_NewSDK_RavenInsertsBankComponent_h
#define Raven_NewSDK_RavenInsertsBankComponent_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenInsertsChannelComponent.h"
#include "NscGuiElement.h"

class RavenInsertsBankComponent : public Component, public NscGuiContainer, public Button::Listener
{
public:
    RavenInsertsBankComponent(int numChans, int bankNum) :
    NscGuiContainer(bankNum),
    mBankNum(bankNum),
    initMode(true),
    exitInit(false),
    getPTInsertName(false),
    showWindow(false),
    bypass(false),
    windowCurrentlyShown(false),
    currentChannel(0),
    currentInsNum(0),
    insChan(0),
    insNum(0),
    bypassCurrentlyOn(false),
    ins5(false),
    selectInsert(true),
    selectInsertCount(0)
    {
        setSize (0, 0);
        int xOffset = 0;
        
        for(int i = 0; i < numChans; i++)
        {
            RavenInsertsChannelComponent *insertChannel = new RavenInsertsChannelComponent(this, bankNum, i);
            insertChannel->setTopLeftPosition(xOffset, 0);
            insertChannel->setChannelAndBankIndex(i, bankNum);
            insertChannel->addButtonListener(this);
            addAndMakeVisible(insertChannel);
            inserts.add(insertChannel);

            xOffset += insertChannel->getWidth();
            setSize(xOffset, insertChannel->getHeight());
        }
            
    }
    
    // Button::Listener interface
	void	buttonStateChanged (Button*){}
	void	buttonClicked (Button* b)
    {
        String buttonName = b->getName();
        
        if(buttonName.length() <= 0) return;
        if(!buttonName.contains("byp") && !buttonName.contains("win")) return;
        
        int underScoreIndex = buttonName.indexOf("_");
        String chanStr = buttonName.substring(3, underScoreIndex);  //3 is right after "win" or "byp"
        insChan = chanStr.getIntValue();
        String insNumStr = buttonName.substring(underScoreIndex+1);
        insNum = insNumStr.getIntValue();
        
        if(buttonName.substring(0, 3).compare("byp") == 0)
        {
            //printf("bypass ");
            //printf("chan %d, insert %d\n", insChan, insNum);
            
            //disable bypass for now
            
            //bypass = true;
        }
        else if (buttonName.substring(0, 3).compare("win") == 0)
        {
            //printf("window ");
            //printf("chan %d, insert %d\n", insChan, insNum);
            showWindow = true;
            
            //move raven window to back so pro tool window comes forth
            // should always be in back... dont use this method, use set app background only ON in plist
            /*
            Process::setDockIconVisible(false);
            Process::setDockIconVisible(true);
            Process::makeForegroundProcess();
            */
            
        }
        

        
    }
        
    bool checkShowWindowAndBypass()
    {
        if(showWindow || bypass)
        {
           if(selectInsert) //initially true
           {
               if(selectInsertCount == 0)
               {
                   link->set(eNSCTrackEvent, eNSCTrackInsertSwitch, mBankNum, insChan, 1);
                   selectInsertCount++;
                   return true;
               }
               else if(selectInsertCount == 1)
               {
                   //bank if necessary
                   if(ins5 && insNum !=4) //4 = insert #5 (0 indexed)
                   {
                       printf("bank left to go to insert # %d\n", insNum);
                       link->set(eNSCGlobalControl, eNSCGSPlugInBankLeft, mBankNum, insChan, 1);
                       ins5 = false;
                       return true;
                   }
                   else if(!ins5 && insNum == 4)
                   {
                       printf("bank right to go to insert # %d\n", insNum);
                       link->set(eNSCGlobalControl, eNSCGSPlugInBankRight, mBankNum, insChan, 1);
                       ins5 = true;
                       return true;
                   }
                   
                   /*if(bypass)
                   {
                       //insert/param button on/off: HUI requires this before selecting the plugin switch to bypass
                       link->set(eNSCGlobalControl, eNSCGSPlugInInsert, mBankNum, insChan, 1);
                       if(!bypassCurrentlyOn)link->set(eNSCGlobalControl, eNSCGSPlugInInsert, mBankNum, insChan, 1);
                   }
                   else*/ //show window
                   {
                       if(insNum == 0)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect1, mBankNum, insChan, 1);
                       else if(insNum == 1)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect2, mBankNum, insChan, 1);
                       else if(insNum == 2)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect3, mBankNum, insChan, 1);
                       else if(insNum == 3)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect4, mBankNum, insChan, 1);
                       else if(insNum == 4)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect1, mBankNum, insChan, 1);
                   }

                   selectInsertCount++;
                   return true;
               }
               else if(selectInsertCount == 2)
               {
                   if(showWindow)
                   {
                       link->set(eNSCGlobalControl, eNSCGSPlugInInsert, mBankNum, insChan, 1);
                   }
                   /*
                   else //now select by bypass
                   {
                       if(insNum == 0)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect1, mBankNum, insChan, 1);
                       else if(insNum == 1)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect2, mBankNum, insChan, 1);
                       else if(insNum == 2)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect3, mBankNum, insChan, 1);
                       else if(insNum == 3)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect4, mBankNum, insChan, 1);
                       else if(insNum == 4)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect1, mBankNum, insChan, 1);
                   }*/
                   
                   selectInsertCount = 0;
                   selectInsert = false;
                   return true;
               }

           }
           else //done with selection - trigger show window or bypass
           {
               if(showWindow)
               {
                   link->set(eNSCGlobalControl, eNSCGSPTWindow_PlugIn, mBankNum, insChan, 1);
                   showWindow = false;
               }
               /*
               else if(bypass)
               {
                   if(!bypassCurrentlyOn)link->set(eNSCGlobalControl, eNSCGSPlugInBypass, mBankNum, insChan, 1);
                   else link->set(eNSCGlobalControl, eNSCGSPlugInBypass, mBankNum, insChan, 0);
                   bypassCurrentlyOn = !bypassCurrentlyOn;
                   //link->set(eNSCGlobalControl, eNSCGSPlugInBypass, mBankNum, winChan, 1);
                   //need to toggle insert/param mode on then off again before we can turn this off...weird
                   // how do track select and pluginswitches affect insert/param mode?
                   bypass = false;
               }*/
               
               selectInsert = true;
               return true;
           }
        }

        return false;

    }
    

    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, float val)
    {
        if(type == eNSCGlobalControl && subType == eNSCGSPTWindow_PlugIn)
        {
            //printf("window shown = %f\n", val);
            if(val != 0) windowCurrentlyShown = true;
            else windowCurrentlyShown = false;
        }
        
        if(checkShowWindowAndBypass()) return;
        
        if (bank == eMatchesAllBanks || bank == mBankNum) this->callback(type, subType, bank, which, val);
    }
    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, const char * text)
    {
        
        if(checkShowWindowAndBypass()) return;
        
        if(getPTInsertName)
        {
            //if insert name message
            if(type == eNSCTrackEvent && subType == 2 && bank == 0) //mBank insead of 0?
            {
                if(which == 40 || which == 50 || which == 60 || which == 70)
                {
                    String insStr = String(text);
                    if(insStr.length() > 0 && !insStr.startsWith(">"))
                    {
                        printf("name = %s, which = %d\n", text, which);
                        if(which == 40)
                        {
                            String current40 = getInsertName(currentChannel, 0);
                            printf("current 40 name is %s\n", (const char*)current40.toUTF8());
                            if(insStr.compare(getInsertName(currentChannel, 0))) //if != to current selection
                            {
                                printf("current 40 different from new 40\n");
                                setInsert(currentChannel, 0, insStr);
                                getPTInsertName = false;
                            }
                        }
                        else if(which == 50)
                        {
                            if(insStr.compare(getInsertName(currentChannel, 1))) //if != to current selection
                            {
                                setInsert(currentChannel, 1, insStr);
                                getPTInsertName = false;
                            }
                        }
                        else if(which == 60)
                        {
                            if(insStr.compare(getInsertName(currentChannel, 2))) //if != to current selection
                            {
                                setInsert(currentChannel, 2, insStr);
                                getPTInsertName = false;
                            }
                        }
                        else if(which == 70)
                        {
                            if(insStr.compare(getInsertName(currentChannel, 3))) //if != to current selection
                            {
                                setInsert(currentChannel, 3, insStr);
                                getPTInsertName = false;
                            }
                        }
                        
                    }
                }
            }
            
            return;
        }
        
        //if ProTools Changes Insert (after initialization)
        if(!initMode)
        {
            //printf("PTPT type = %d, subtype = %d, bank = %d, which = %d, ((((  %s  ))))\n", type, subType, bank, which, text);
            
            if(type == eNSCTrackEvent && subType == 2 && bank == 0 && which == 20) //mBank insead of 0?
            {
                String insertText = String(text);
                int iTrackNum = insertText.getIntValue();
                if(iTrackNum == 0) iTrackNum = 1;
                int iBankNum = iTrackNum / 9;
                int iChanNum = (iTrackNum % 9) - 1;
                printf("insert changed for track %d (bank %d, chan %d), mBank = %d\n", iTrackNum, iBankNum, iChanNum, mBankNum);
                
                if(iBankNum == mBankNum)
                {
                    currentChannel = iChanNum;
                    //getPTInsertName = true;  //causing string malloc bug..
                    return;
                }
            }
        }
        
        //then pass along callback
        if (bank == eMatchesAllBanks || bank == mBankNum) this->callback(type, subType, bank, which, text);
        
    }

    
    void setInsert(int chanNum, int insertNum, String insertName)
    {
        inserts[chanNum]->setInsertDisplayName(insertNum, insertName);
    }
    
    String getInsertName(int chanNum, int insertNum)
    {
        return inserts[chanNum]->getInsertName(insertNum);
    }
    
    void exitInitMode(){exitInit = true;}
    
    RavenInsertsChannelComponent* getChannel(int num){return inserts[num];}

    
private:
    OwnedArray<RavenInsertsChannelComponent> inserts;
    int mBankNum;
    bool initMode, exitInit, getPTInsertName;
    bool showWindow, bypass, windowCurrentlyShown;
    int currentChannel, currentInsNum, insChan, insNum;
    bool bypassCurrentlyOn, ins5, selectInsert;
    int selectInsertCount;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (RavenInsertsBankComponent)
};


#endif
