
#ifndef Raven_NewSDK_RavenInsert_h
#define Raven_NewSDK_RavenInsert_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "NscGuiElement.h"
#include "RavenLookAndFeel.h"

class RavenInsert : public ComboBox, public NscGuiElement
{
public:
    RavenInsert(NscGuiContainer *owner) : NscGuiElement(owner)
    {
        
        setLookAndFeel(new RavenLookAndFeel());
        setColour(textColourId, Colour::fromRGBA(255, 0, 0, 200));
        
    }
    ~RavenInsert() {}
    
    
    virtual void  updateNscValue(float value){}			// callback when DAW changes a value
    
    
private:
    

    
};


#endif
