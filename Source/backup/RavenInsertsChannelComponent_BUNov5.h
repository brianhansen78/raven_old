

#ifndef Raven_NewSDK_RavenInsertsChannelComponent_h
#define Raven_NewSDK_RavenInsertsChannelComponent_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenButton.h"
#include "RavenTouchComponent.h"
#include "RavenLabel.h"
#include "NscGuiElement.h"

class RavenInsertsChannelComponent : public RavenTouchComponent, public NscGuiElement, Button::Listener
{
public:
    RavenInsertsChannelComponent (NscGuiContainer *owner, int bank, int chan) : NscGuiElement(owner), mChan(0)
    {
        
        int yOffset = 2;
        for(int i = 0; i < 5; i++)
        {
         
            int xOff = 0;
            if(i == 0) xOff = 0;
            if(i == 1) xOff = 0;
            if(i == 2) xOff = 0;
            if(i == 3) xOff = 0;
            if(i == 4) xOff = 0;
            
            RavenButton *insertBypassButton = new RavenButton();
            insertBypassButton->setClickingTogglesState(true);
            insertBypassButton->setImagePaths(RavenGUI::getInsertButtonPath("bypass", true, bank, chan, i), RavenGUI::getInsertButtonPath("bypass", false, bank, chan, i));
            insertBypassButton->setTopLeftPosition(xOff, yOffset);
            addAndMakeVisible(insertBypassButton);
            bypassButtons.add(insertBypassButton);
            ravenTouchButtons.add(insertBypassButton);
                        
            RavenButton *insertWinButton = new RavenButton();
            insertWinButton->setImagePaths(RavenGUI::getInsertButtonPath("display", true, bank, chan, i), RavenGUI::getInsertButtonPath("display", false, bank, chan, i));
            insertWinButton->setTopLeftPosition(insertBypassButton->getWidth(), yOffset);
            addAndMakeVisible(insertWinButton);
            insertWinButton->addListener(this);
            winButtons.add(insertWinButton);
            ravenTouchButtons.add(insertWinButton);
            
            int chanNum = (bank*8)+(chan+1); //1 to 24
            RavenLabel *insLabel = new RavenLabel();
            insLabel->setBounds(26+chanNum/8, yOffset+2, 45, 20);
            addAndMakeVisible(insLabel);
            insLabels.add(insLabel);
            if(bank == 0 && chan == 0 && i == 0)
            {
                //setInsertDisplayName(i, "EQ-7Band");
            }
            else if(bank == 0 && chan == 1 && i == 0)
            {
                //setInsertDisplayName(i, "BF-76");
            }
            /*else*/ setInsertDisplayName(i, ""); //default (empty) insert name
            
            yOffset += insertBypassButton->getHeight();
            
            setSize (insertBypassButton->getWidth() + insertWinButton->getWidth(), getHeight() + yOffset);
            
        }
        
        static int panelOffset = INSERTS_X_OFFSET;
        insertsPanelImg = ImageCache::getFromFile(File(String(GUI_PATH) + "MIXERGRAPHICS/INSERTSV4/INSERTPANEL/INSERT-PANEL-1to5.png"));
        Rectangle<int>clipRect;
        clipRect = Rectangle<int>(panelOffset, 0, getWidth(), insertsPanelImg.getHeight());
        insertsPanelImg = insertsPanelImg.getClippedImage(clipRect);
        panelOffset += insertsPanelImg.getWidth();
                
    }
    
    ~RavenInsertsChannelComponent()
    {
        deleteAllChildren();
    }
    
    void	buttonStateChanged (Button* b)
    {
        for(int i = 0; i < winButtons.size(); i++)
        {
            if(b == winButtons[i] && b->isDown())
            {
                Label *lbl = insLabels[i];
                lbl->setTopLeftPosition(lbl->getX(), lbl->getY()+2);
                //printf("down %d\n", i);
            }
        }
    }
	void	buttonClicked (Button* b)
    {
        for(int i = 0; i < winButtons.size(); i++)
        {
            if(b == winButtons[i])
            {
                
                //light up bypass for now..
                bypassButtons[i]->triggerClick();
                
                Label *lbl = insLabels[i];
                lbl->setTopLeftPosition(lbl->getX(), lbl->getY()-2);
                //printf("click %d\n", i);
            }
        }
    }
    
    void setChannelAndBankIndex(int chan, int bank)
    {
        mChan = chan;
        
        for(int i = 0; i < winButtons.size(); i++)
        {
            winButtons[i]->setName("win" + String(chan) + "_" + String(i));
        }
        for(int i = 0; i < bypassButtons.size(); i++)
        {
            bypassButtons[i]->setName("byp" + String(chan) + "_" + String(i));
        }
    }
    
    void addButtonListener(Button::Listener * listener)
    {
        for(int i = 0; i < ravenTouchButtons.size(); i++) ravenTouchButtons[i]->addListener(listener);
    }
    
    void setInsertDisplayName(int which, String name)
    {
        //printf("Called to set %d to %s\n", which, (const char*)name.toUTF8());
        const MessageManagerLock lock;// lock mm queue since we are going to be updating gui elements
        if(lock.lockWasGained())
        {
            if(name.length() > 8) name = name.substring(0, 8); //limit to 8 chars
            if(name.compare("No Inser") == 0) name = "";
            insLabels[which]->setText(name, false);
        }
    }
    
    String getInsertName(int insertNum)
    {
        const MessageManagerLock lock;// lock mm queue since we are going to be updating gui elements
        if(lock.lockWasGained()) return insLabels[insertNum]->getText();
        else return String(" ");
    }
    
    void paint (Graphics &g)
    {
        //g.drawImageAt(insertsPanelImg, 0, 0);
    }
    
    
private:
    
    int mChan;
    
    Image insertsPanelImg;

    OwnedArray<RavenButton> winButtons;
    OwnedArray<RavenButton> bypassButtons;
    OwnedArray<RavenLabel> insLabels;

};
 


#endif
