/*
  ==============================================================================

    NscGuiElement.cpp
    Created: 24 Feb 2012 5:28:07am
    Author:  cjubien

  ==============================================================================
*/


//==========================================================================================
#include "NscGuiElement.h"
//#include "GuiIncludes.h"
//#include "Components/Switch.h"

//==========================================================================================


//==========================================================================================
NscGuiElement::NscGuiElement(NscGuiContainer * owner)
	: bankIndex(-1)
	, channelIndex(-1)
	, guiTag(-1)
{
	if (owner)		owner->addGuiElement(this);
}

NscGuiElement::NscGuiElement(NscGuiContainer * owner, int tag)
	: bankIndex(-1)
	, channelIndex(-1)
	, guiTag(tag)
	, tempWorkaroundPtr(0)
{
	if (owner)		owner->addGuiElement(this);
}

NscGuiElement::NscGuiElement(NscGuiContainer * owner, int bank, int channel, int tag)
	: bankIndex(bank)
	, channelIndex(channel)
	, guiTag(tag)
{
	if (owner)		owner->addGuiElement(this);
}

NscGuiElement::~NscGuiElement()
{
}

void NscGuiElement::updateNscValue(float value)
{
	if (tempWorkaroundPtr)
	{
		Component * c = (Component *) tempWorkaroundPtr;
		ImageButton * sw = dynamic_cast<ImageButton *>(c);
		if (sw)
			sw->setToggleState( value!=0 ? true: false, false);

	}


}
void NscGuiElement::updateNscValue(const String& value) 
{
}

//==========================================================================================

NscGuiContainer::NscGuiContainer(int bank)
	: bankNum(bank)
	, link(0)
{
	elements.clear();
}
NscGuiContainer::~NscGuiContainer()
{
	elements.clear();
}

void NscGuiContainer::addGuiElement( NscGuiElement * element)
{
	elements.addIfNotAlreadyThere(element);
}

NscGuiElement * NscGuiContainer::findNscGuiElement(int tag)
{
	NscGuiElement * retVal = 0;

	for (int i=0; i< elements.size(); ++i)
	{
		NscGuiElement * el = elements[i];
		if (el->guiTag == tag)
		{
			retVal = el;
			break;
		}
	}

	return retVal;
}


NscGuiElement * NscGuiContainer::findNscGuiElement(int bank, int channel, int tag)
{
	NscGuiElement * retVal = 0;

	for (int i=0; i< elements.size(); ++i)
	{
		NscGuiElement * el = elements[i];
		if (el->guiTag == tag && el->channelIndex== channel && el->bankIndex == bank)
		{
			retVal = el;
			break;
		}
	}

	return retVal;
}

int NscGuiContainer::callback(ENSCEventType type, int subType, int bank, int which, float val)
{
	// note: this is not the desired way to handle this.  This should form a message
	// to be serviced by the juce message manager, since these updates may take a while.
	// When the DAW changes a lot of parameters simultaneously a lot of notifications will occur,
	// and so if this is not quick your app may get bogged down.
	// However, for the purposes of illustration and simplicity, this shows what needs to happen.
    
	const MessageManagerLock lock;			// lock mm queue since we are going to be updating gui elements
	if (!lock.lockWasGained())
	{
		DBG("Couldn't lock message manager queue");
		return 0;
	}

	// we prioritize type checks based on how often we expect message types to appear
	NscGuiElement * el = 0;

	if (type == eNSCTrackEvent)
	{
		el = findNscGuiElement(bank, which, subType); 	if(el) 	el->updateNscValue(val);
	}
	else if (type == eNSCGlobalControl)
	{
		el = findNscGuiElement(subType);  if (el) el->updateNscValue(val);
	}


	return 0;
}
int NscGuiContainer::callback(ENSCEventType type, int subType, int bank, int which, const char * text)
{
	// note: this is not the desired way to handle this.  This should form a message
	// to be serviced by the juce message manager, since these updates may take a while.
	// When the DAW changes a lot of parameters simultaneously a lot of notifications will occur,
	// and so if this is not quick your app may get bogged down.
	// However, for the purposes of illustration and simplicity, this shows what needs to happen.
    
	const MessageManagerLock lock;			// lock mm queue since we are going to be updating gui elements
	if (!lock.lockWasGained())
	{
		DBG("Couldn't lock message manager queue");
		return 0;
	}

	// we prioritize type checks based on how often we expect message types to appear
	NscGuiElement * el = 0;

	if (type == eNSCTrackEvent)
	{
		el = findNscGuiElement(bank, which, subType); 	if(el) 	el->updateNscValue(text);
	}
	else if (type == eNSCGlobalControl)
	{
		el = findNscGuiElement(bank, which, subType);  if (el) el->updateNscValue(text);
	}

	return 0;

}


void NscGuiContainer::sliderChanged (NscGuiElement *  slider, float sliderValue)
{
    
    //DBG (String::formatted("%ld Sending bank = %d, channel = %d, tag = %d", Time::currentTimeMillis(), element->getBankIndex(), element->getChannelIndex(), element->getTag()));
	if (!link || !slider)	return;

	int tag = slider->getTag();
	int channel = slider->getChannelIndex();
	if (channel < 0)
		link->set(eNSCGlobalControl, tag , 0, 0, sliderValue);
	else{
		link->set(eNSCTrackEvent, tag , slider->getBankIndex(), channel, sliderValue);
    }
    

}
void NscGuiContainer::sliderBegan (NscGuiElement * slider)
{
	if (!link || !slider)	return;
	int tag = slider->getTag();

	if (tag == eNSCTrackFaderValue/* || tag == eNSCTrackSendValue*/)
    link->set(eNSCTrackEvent, eNSCTrackFaderSwitch, slider->getBankIndex(), slider->getChannelIndex(), 1);
}

void NscGuiContainer::sliderEnded (NscGuiElement * slider)
{
	if (!link || !slider)	return;
	int tag = slider->getTag();

	if (tag == eNSCTrackFaderValue/* || tag == eNSCTrackSendValue*/)
		link->set(eNSCTrackEvent, eNSCTrackFaderSwitch, slider->getBankIndex(), slider->getChannelIndex(), 0.0);
}

void NscGuiContainer::switchClicked (NscGuiElement * Switch, bool switchState)
{
    
	if (!link || !Switch)	return;

	int tag = Switch->getTag();
	int channel = Switch->getChannelIndex();
	if (channel < 0)		// We're using this to indicate global switches here.
	{
		link->set(eNSCGlobalControl, tag, 0, 0, 1);
	}
	else
	{
		link->set(eNSCTrackEvent, tag, Switch->getBankIndex(), channel , 1);
	}

}



