

#include "RavenContainer.h"

//==============================================================================
RavenContainer::RavenContainer() :
    loadingWindow(0),
    touchesOn(RAVEN_FORCE_TOUCH_ON),
    initInserts(false),
    init5(false),
    lastTapPos(0, 0),
    prevTime(0),
    numAssignPresses(0)
{
    int totalHeight = 0;
    
    //Mixer/Sends/Inserts/Track Names
    mixerContainer = new RavenMixerContainer(numBanks, chansPerBank);
    mixerContainer->setTopLeftPosition(0, totalHeight);
    totalHeight +=  mixerContainer->getHeight();
    setSize(viewWidth, totalHeight);
    addAndMakeVisible(mixerContainer);
    modules.add(mixerContainer);
    mixer = mixerContainer->getMixer();
    sends = mixerContainer->getSends();
    inserts = mixerContainer->getInserts();
    names = mixerContainer->getTrackNames();
    
    //Functions(Toolbar)
    functions = new RavenFunctionsComponent();
    functions->setTopLeftPosition(0, totalHeight);
    totalHeight += functions->getHeight();
    setSize(viewWidth, totalHeight);
    addAndMakeVisible(functions);
    modules.add(functions);
    functions->setMixerButtonListener(mixerContainer);
    inserts->setMixerButtonListener(mixerContainer);
    mixerContainer->initializeCounter(functions);
    mixerContainer->initializeInsertsPalette(functions);
    mixerContainer->setFunctions(functions);
    
    assignButton = functions->getAssignButton();
    
    //Loading Window.
    loadingWindow = new RavenLoadingWindow();
    
    //*//TUIO
    tuioClient = new TUIO::TuioClient(3333);
	tuioClient->addTuioListener(this);
	tuioClient->connect();
    //*/
    
    //initialize touch fields
    for(int i = 0; i < MAX_NUM_TOUCHES; i++)
    {   
        touches[i].gesture = eMTOneFinger;
        touches[i].moduleNum = 999;
        touches[i].isFader = false;
        touches[i].isKnob = false;
        touches[i].isButton = false;
        touches[i].isModuleMove = false;
        touches[i].isMixer = false;
        touches[i].isSend = false;
        touches[i].isCounter = false;
        touches[i].isInsertsPalette = false;
        touches[i].isNavPad = false;
        touches[i].isFunction = false;
        touches[i].bank = 999;
        touches[i].chan = 999;
    }
    
    // once gui created, create nsc link:
	unsigned int nscLibVersion = NSC_GetLibraryVersion();
	if ( nscLibVersion >= kLocalNscLibraryVersion)		// we require version 1.7.0.X or higher
	{
		link = NSC_CreateLink(kLocalNscLibraryVersion, this, eDAWMode_ProTools, eControllerBankVControl, numBanks);
        mixerContainer->setNscLink(link);
        functions->setLink(link); //sets functions Nsc link for GuiContainer and sub components (nav pad, for example)
	}
    
}

RavenContainer::~RavenContainer()
{
    tuioClient->disconnect();
    delete tuioClient;
    NSC_DestroyLink(link);
	deleteAllChildren();
}

bool RavenContainer::componentTouched(Component *cmp, int touchX, int touchY)
{
    
    if(!cmp->isVisible()) return false; //return false if component is not visible
    
    int cmpX = cmp->getScreenX();
    int cmpX2 = cmpX + cmp->getWidth();
    int cmpY = cmp->getScreenY();
    int cmpY2 = cmpY + cmp->getHeight();
    
    if(touchX >= cmpX && touchX <= cmpX2 && touchY >= cmpY && touchY <= cmpY2)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool RavenContainer::touchTimeLessThan(long &thisSec, long &thisUSec, long &lastSec, long &lastUSec, long timeUSec)
{
    if(thisSec == lastSec && (thisUSec - lastUSec) <= timeUSec) //last - this
    {
        return true;
    }
    else if(thisSec == (lastSec+1)) //-1
    {
        long uSecDiff = (1000000 - lastUSec) + thisUSec;
        if(uSecDiff <= timeUSec)
        {
            return true;
        }
    }

    return false;
}


void RavenContainer::addTuioCursor(TUIO::TuioCursor *tcur)
{
    if(!touchesOn) return; // wait until after initialization to listen for touches
    
    int id = tcur->getCursorID();
    int touchX = tcur->getScreenX(SCREEN_WIDTH);
    int touchY = tcur->getScreenY(SCREEN_HEIGHT);
    
    TUIO::TuioTime startTime = tcur->getStartTime();  
    long startSec = startTime.getSeconds(); 
    long startUSec = startTime.getMicroseconds();
    
    //tuioClient->lockCursorList();
    std::list<TUIO::TuioCursor*> cursorList = tuioClient->getTuioCursors();    

    for(int i = 0; i < modules.size(); i++)
    {
        if(componentTouched(modules[i], touchX, touchY))
        {
            touches[id].moduleNum = i;
        }
        
    }
    
    if (functions->getMoveToggleState()) //moveMode
    {
        if (touches[id].moduleNum == 0) //if mixer touch
        {
            mixerContainer->startMoveModule(touchY);
            touches[id].isModuleMove = true;
            return;
        }
    }
    
    if (functions->isInCustomMode()) //custom mode
    {
        if (touches[id].moduleNum == 1) //if functions touch
        {
            if(functions->checkModuleTouch(tcur))
            {
                touches[id].isModuleMove = true;
            }
            return;
        }
    }
    
    if (touches[id].moduleNum == 0) //if mixer (container) touch
    {        
        if(mixerContainer->checkCounterTouch(tcur))
        {
            touches[id].isCounter = true;
            return;
        }
        
        if(mixerContainer->checkInsertsPaletteTouch(tcur))
        {
            touches[id].isInsertsPalette = true;
            return;
        }
        
        for(int b = 0; b < mixer->getNumBanks(); b++)
        {
            for(int c = 0; c < mixer->getChansPerBank(); c++)
            {
                 if(mixer->getBank(b)->getChannel(c)->checkFaderTouch(tcur))
                 {
                     touches[id].isFader = true;
                     touches[id].isMixer = true;
                     touches[id].bank = b;
                     touches[id].chan = c;
                     mixer->getBank(b)->getChannel(c)->beginFader();
                 }
                 else if(mixer->getBank(b)->getChannel(c)->checkKnobTouch(tcur))
                 {
                     touches[id].isKnob = true;
                     touches[id].isMixer = true;
                     touches[id].bank = b;
                     touches[id].chan = c;
                 }
                 else if(mixer->getBank(b)->getChannel(c)->checkButtonTouch(tcur))
                 {
                     touches[id].isButton = true;
                     touches[id].isMixer = true;
                     touches[id].bank = b;
                     touches[id].chan = c;
                 }
                 else if(sends->getBank(b)->getChannel(c)->checkButtonTouch(tcur))
                 {
                     touches[id].isButton = true;
                     touches[id].isSend = true;
                     touches[id].bank = b;
                     touches[id].chan = c;
                 }
                else if (sends->getBank(b)->getChannel(c)->checkFaderTouch(tcur))
                {
                    touches[id].isFader = true;
                    touches[id].isSend = true;
                    touches[id].bank = b;
                    touches[id].chan = c;
                    sends->getBank(b)->getChannel(c)->beginFader();
                }
                else if(sends->getBank(b)->getChannel(c)->checkKnobTouch(tcur))
                {
                    touches[id].isKnob = true;
                    touches[id].isSend = true;
                    touches[id].bank = b;
                    touches[id].chan = c;
                }
                else if(inserts->getBank(b)->getChannel(c)->checkButtonTouch(tcur))
                {
                    touches[id].isButton = true;
                    touches[id].bank = b;
                    touches[id].chan = c;
                }
            }
        }
    }
    
    else if (touches[id].moduleNum == 1) //if functions touch
    {
        if(functions->checkNavPadTouch(tcur))
        {
            //printf("nav pad touch!\n");
            touches[id].isNavPad = true;
        }
        else if(functions->checkButtonTouch(tcur))
        {
            touches[id].isButton = true;
            touches[id].isFunction = true;
        }
    }
    
    
    if(cursorList.size() > 1)
    {
        std::list<TUIO::TuioCursor*>::iterator it = cursorList.end();
        it--; it--;//previous touch
        TUIO::TuioTime lastTime = (*it)->getStartTime(); 
        long lastSec = lastTime.getSeconds();
        long lastUSec = lastTime.getMicroseconds();
        
        if(touchTimeLessThan(startSec, startUSec, lastSec, lastUSec, MT_2FINGER_WAIT_USEC))
        {
            TUIO::TuioPoint lastPos = (*it)->getPosition();
            if(tcur->getDistance(&lastPos) < MT_2FINGER_TOUCH_DISTANCE)
            {
                touches[id].gesture = eMTTwoFinger;
                touches[id-1].gesture = eMTTwoFinger;
                std::cout << "two fingers down" << std::endl; 
            }
        }
        
    }

    //tuioClient->unlockCursorList();
    //init TUIO time after seconds > ???    
     
}
void RavenContainer::updateTuioCursor(TUIO::TuioCursor *tcur)
{
    /*//downsample by 2
    static long int tuioUpdateCount = 0;
    tuioUpdateCount++;
    if(tuioUpdateCount >= 10000) tuioUpdateCount = 0;
    if(tuioUpdateCount %2 != 0) return;
    //*/
    
    int id = tcur->getCursorID();
    int touchX = tcur->getScreenX(SCREEN_WIDTH);
    int touchY = tcur->getScreenY(SCREEN_HEIGHT);
    
    TUIO::TuioTime startTime = tcur->getStartTime();  
    long startSec = startTime.getSeconds();
    long startUSec = startTime.getMicroseconds();
    
    TUIO::TuioTime thisTime = tcur->getTuioTime();  
    long thisSec = thisTime.getSeconds();
    long thisUSec = thisTime.getMicroseconds();
    
    //tuioClient->lockCursorList();
    std::list<TUIO::TuioCursor*> cursorList = tuioClient->getTuioCursors();    
    
    //if(true)//if(!touchTimeLessThan(thisSec, thisUSec, startSec, startUSec, MT_WAIT_USEC))
    {
    
        //if(true)//if(cursorList.size() == 1)
        {

            //std::cout << "one finger move" << std::endl;
            if(touches[id].isFader && touches[id].isMixer)
            {
                int b = touches[id].bank;
                int c = touches[id].chan;
                mixer->getBank(b)->getChannel(c)->updateFaderTouch(tcur);
            }
            else if(touches[id].isFader && touches[id].isSend)
            {
                int b = touches[id].bank;
                int c = touches[id].chan;
                sends->getBank(b)->getChannel(c)->updateFaderTouch(tcur);
            }
            else if(touches[id].isKnob && touches[id].isMixer)
            {
                int b = touches[id].bank;
                int c = touches[id].chan;
                mixer->getBank(b)->getChannel(c)->updateKnobTouch(tcur);
            }
            else if(touches[id].isKnob && touches[id].isSend)
            {
                int b = touches[id].bank;
                int c = touches[id].chan;
                sends->getBank(b)->getChannel(c)->updateKnobTouch(tcur);
            }
            else if(functions->getMoveToggleState() && touches[id].isModuleMove) //moveMode
            {
                mixerContainer->moveModule(touchY);
            }
            else if(functions->isInCustomMode() && touches[id].isModuleMove) //moveMode
            {
                functions->moveModule(touchX, touchY);
            }
            else if(touches[id].isCounter) //move counter
            {
                mixerContainer->moveCounter(touchX, touchY);
            }
            else if(touches[id].isInsertsPalette) //move inserts palette
            {
                mixerContainer->moveInsertsPalette(touchX, touchY);
            }
            else if(touches[id].isNavPad) //nav pad gesture
            {
                if(touches[id].gesture == eMTTwoFinger) functions->navPadTouch(tcur, true);
                else                                    functions->navPadTouch(tcur, false);
            }
        }
        /*
        else if(cursorList.size() == 2)   //more detail needed (fingers close, etc)
        {
            float speedX = tcur->getXSpeed();
            std::cout << "two finger move! " << speedX << std::endl;
            scrollView(mixerView, speedX);
        }
         */
        
    }


    
    
    //tuioClient->unlockCursorList();
}
void RavenContainer::removeTuioCursor(TUIO::TuioCursor *tcur)
{
    int id = tcur->getCursorID();

    TUIO::TuioTime startTime = tcur->getStartTime();  
    long startSec = startTime.getSeconds(); 
    long startUSec = startTime.getMicroseconds();

    TUIO::TuioTime thisTime = tcur->getTuioTime();  
    long thisSec = thisTime.getSeconds();
    long thisUSec = thisTime.getMicroseconds();
    

    //tuioClient->lockCursorList();
    //std::list<TUIO::TuioCursor*> cursorList = tuioClient->getTuioCursors();
    
    if(touches[id].gesture == eMTOneFinger)
    {        
        if(touchTimeLessThan(thisSec, thisUSec, startSec, startUSec, MT_TAP_WAIT_USEC))
        {
            long lastTapSec = lastTapTime.getSeconds();
            long lastTapUS = lastTapTime.getMicroseconds();
            if(touchTimeLessThan(thisSec, thisUSec, lastTapSec, lastTapUS, MT_TAP_WAIT_USEC))
            {
                //printf("dist = %f\n", tcur->getDistance(&lastTapPos));
                
                if(tcur->getDistance(&lastTapPos) < MT_TAP_TOUCH_DISTANCE)
                {
                    //std::cout<< "two finger tap" << "id " << id << std::endl;
                    /*
                    if(touches[id].isFader && touches[id].isMixer)
                    {
                        mixer->getBank(touches[id].bank)->getChannel(touches[id].chan)->zeroFader();
                    }
                    else if(touches[id].isKnob && touches[id].isMixer)
                    {
                        mixer->getBank(touches[id].bank)->getChannel(touches[id].chan)->zeroKnob();
                    }
                    else if(touches[id].isFader && touches[id].isSend)
                    {
                        sends->getBank(touches[id].bank)->getChannel(touches[id].chan)->zeroFader();
                    }
                    else if(touches[id].isKnob && touches[id].isSend)
                    {
                        sends->getBank(touches[id].bank)->getChannel(touches[id].chan)->zeroKnob();
                    }
                     */
                }
            }
            else
            {
                //std::cout<< "one finger tap" << "id " << id << std::endl;
            }
            
            lastTapTime = thisTime;
            lastTapPos = tcur->getPosition();
        }
    }
    else if(touches[id].gesture == eMTTwoFinger)
    {
        if(touchTimeLessThan(thisSec, thisUSec, startSec, startUSec, MT_TAP_WAIT_USEC))
        {
            //std::cout<< "two finger tap" << "id " << id << std::endl;
        }
    }
    
    if(touches[id].isFader)
    {
        touches[id].isFader = false;
        int b = touches[id].bank;
        int c = touches[id].chan;
        if(touches[id].isMixer) mixer->getBank(b)->getChannel(c)->endFader();
        if(touches[id].isSend) sends->getBank(b)->getChannel(c)->endFader();
        touches[id].bank = 999;
        touches[id].chan = 999;
    }
    if(touches[id].isKnob)
    {
        touches[id].isKnob = false;
        touches[id].bank = 999;
        touches[id].chan = 999;
    }
    
    if(touches[id].isMixer)
    {
        touches[id].isMixer = false; 
    }
    
    if(touches[id].isSend)
    {
        touches[id].isSend = false; 
    }
    
    //resest touch fields
    touches[id].gesture = eMTOneFinger;
    
    if(touches[id].isModuleMove && functions->getMoveToggleState())
    { 
        mixerContainer->endMoveModule();
        touches[id].isModuleMove = false;
    }
    
    if(touches[id].isModuleMove && functions->isInCustomMode())
    {
        functions->endModuleMove();
        touches[id].isModuleMove = false;
    }
    
    if(touches[id].isFunction && touches[id].isButton)
    {
        functions->releaseHotKeyTouch(tcur);
    }
    
    touches[id].isButton = false;
    touches[id].isCounter = false;
    touches[id].isInsertsPalette = false;
    touches[id].isNavPad = false;
    //tuioClient->unlockCursorList();
                        
                        
}


//NSC callbacks//////////////

int RavenContainer::nscServerFound(const char* serverName, const char * domain)
{
	// It is possible that multiple servers will be found, running on different domains and different machines.
	// These chould be managed in a list and presented to the user for selection.
	// In this simple example, we assume just a single instance of NeyFi will be running on the network, and
	// connect to it immediately:
	// It is possible that you will get mulitple notifications of the same serverName and domain.  This will happen
	// if there are multiple network paths to the server (e.g. wired and wireless).  Even locally, it is possible to get multiple connections, 
	// for instance through the internal loopback adaptor and through a connected router.
	if (link)
		link->connectToServer(serverName, domain);
    
	return 0;			// return code not currently used, but should return 0.
    
}
int RavenContainer::nscConnected(const char* serverName, const char * domain)
{
	DBG("New connection from " + String(CharPointer_UTF8(serverName)) + "." + String(CharPointer_UTF8(domain)));
	// link is now connected for communications with Ney-Fi, and can be used for bi-directional communication.
	// Note: this callback can occur on a different thread than nscServerFound, but will be the same thread as the callback functions below.
	return 0;			// return code not currently used, but should return 0.
}

int RavenContainer::nscDisconnected(const char* serverName, const char * domain)
{
	// if you are managing a list of services and presenting these to the user, note that this service might not be the one
	// that you are currently connected to.  In other words, this callback happens might happen due to NeyFi stopping on 
	// a different machine than the one that you are currently connected to.
	DBG("NSC disconnected -- " +  String(serverName) + "." + String(domain) );
	return 0;		// return code not currently used, but should return 0.
}

/*
void RavenContainer::initializeInserts(int bank, const std::string & data, int insertNum)
{
    static int slotCount = 0;
    static int insBank = 0;
    static int insChan = 0; //skip 0 since we have to set this separately (since we use it to build list earlier)
    static int insStep = 0;
    
    if(insertNum == 4) printf("insert 5 = %s insBank = %d bank = %d\n", data.c_str(), insBank, bank);
    
    if(slotCount != insertNum) return;
    
    String nameStr = String(data.c_str());
    if(nameStr.startsWith(">")) return;
    if(!nameStr.compare("          ")) return; //ignore blank names from HUI blinking
    
    if(insStep == 0 && insBank == bank)
    {
        link->set(eNSCTrackEvent, eNSCTrackInsertSwitch, insBank, insChan, 1);
        insStep++;
        return;
    }
    else if (insStep == 1 && insBank == bank)
    {
        printf("bank %d, chan %d insert %d = %s\n", insBank, insChan, slotCount, (const char*)nameStr.toUTF8());
        
        inserts->setInsert(bank, insChan, insertNum, nameStr, false); //set name but don't send update to PT
        
        insStep = 0;
        insChan++;
        
        if(insChan >= chansPerBank) //if(insBank >= numBanks)
        {
            slotCount++;
            insBank = 0;
            insChan = 0;
            //insStep = 0;
            
            //if(slotCount == 4) insertButton->triggerClick(); to get #5...
            
            if(slotCount == 4)
            {
                init5 = true;
                link->set(eNSCGlobalControl, eNSCGSPlugInBankRight, insBank, insChan, 1);
                link->set(eNSCGlobalControl, eNSCGSPlugInBankRight, insBank, insChan, 1);
                link->set(eNSCTrackEvent, eNSCTrackInsertSwitch, 0, 0, 1);
                insStep = 1;
                printf("trigger insert bank\n");
            }
            
            if(slotCount == 5)
            {
                numAssignPresses = 0;
                link->set(eNSCGlobalControl, eNSCGSPlugInBankLeft, insBank, insChan, 1);
                link->set(eNSCGlobalControl, eNSCGSPlugInBankLeft, insBank, insChan, 1);
                //insBank++;
                inserts->exitInitMode();
                this->exitInit();
                initInserts = false;
            }
            
        }
        
        return;
    }
    /*
    else if(insStep == 2)
    {
        assignButton->triggerClick();
        insStep++;
        return;
    }
    else if(insStep == 3)
    {
        assignButton->triggerClick();
        insStep = 0;
        return;
    }
    
    assignButton->triggerClick();
     *//*

}
*/

void RavenContainer::forceNewHUI()
{
    Time time = Time::getCurrentTime();
    
    static bool firstTime = true;
    if(firstTime) { firstTime = false; prevTime = time; return; }
    
    if(time.toMilliseconds() - prevTime.toMilliseconds() >= 50)
    {
        //printf("more than 0.1 second between HUI messages, need to press assign\n");
        for(int i =0 ; i < numAssignPresses; i ++) assignButton->triggerClick();
    }
    prevTime = time;
}

void RavenContainer::exitInit()
{
    const MessageManagerLock lock;
    if(lock.lockWasGained())
    {
        if(loadingWindow) delete loadingWindow;
        if(!getParentComponent()->isVisible()) getParentComponent()->setVisible(true);
    }
    touchesOn = true;
    
    printf("Raven Ready!\n");
}

int RavenContainer::callback(ENSCEventType type, int subType, int bank, int which, float val)
{
    // First get initial fader values, then build inserts List
    static bool initInsertsOnce = true;
    if(initInsertsOnce)
    {
        if(type == eNSCTrackEvent && subType == eNSCTrackFaderValue && bank == (0) && which == (chansPerBank-1)) //why are other banks not sending?
        {
            //Init Inserts
            if(RAVEN_INIT_INSERTS)initInserts = true;
            if(initInserts)
            {
                loadingWindow->updateStatusLabel(String("initializing inserts..."));
                link->set(eNSCGlobalControl, eNSCGSPlugInBankLeft, 0, 0, 1); //make this button trigger
                link->set(eNSCGlobalControl, eNSCGSPlugInBankLeft, 0, 0, 1);
                assignButton->triggerClick();
                assignButton->triggerClick();
                numAssignPresses = 2;
            }
            else
            {
                exitInit();
            }
            
            initInsertsOnce = false;
        }
    }
    
    //if done with initialization, then pass on to callbacks as normal
    if(!initInserts)
    {
        //printf("incoming  HUI: bank = %d, which = %d, val = %f\n\n", bank, which, val);
        mixerContainer->nscCallback(type, subType, bank, which, val);
        functions->callback(type, subType, bank, which, val);
    }
    
	return 0;
}
int RavenContainer::callback(ENSCEventType type, int subType, int bank, int which, const std::string & data)
{
    /*//intialize inserts already set in ProTools
    if (initInserts)
    {
        forceNewHUI(); //trigger assign presses every 0.1 seconds to speed things up...
        
        //if(type == eNSCPlugInLCD || subType == eNSCPlugInLCD)
        //printf("incoming HUI: bank = %d, which = %d, text = %s\n\n", bank, which, data.c_str());
        
        if(subType == eNSCPlugInLCD && bank == 0 && which == 40)
        {
            if(init5)   initializeInserts(bank, data, 4);
            else        initializeInserts(bank, data, 0);
        }
        else if(subType == eNSCPlugInLCD && bank == 0 && which == 50)
        {
            initializeInserts(bank, data, 1);
        }
        else if(subType == eNSCPlugInLCD && bank == 0 && which == 60)
        {
            initializeInserts(bank, data, 2);
        }
        else if(subType == eNSCPlugInLCD && bank == 0 && which == 70)
        {
            initializeInserts(bank, data, 3);
        }
    }
    else //normal behavior
    {

    }*/
    
    //printf("incoming HUI: bank = %d, which = %d, text = %s\n\n", bank, which, data.c_str());
    mixerContainer->nscCallback(type, subType, bank, which, data);
    functions->callback(type, subType, bank, which, data.c_str());
    
	return 0;
}

void RavenContainer::setDawMode(EDAWMode mode)		// this gets called when the mode changes remotely (all devices must have the same mode).
{
    //unused for now...
}
