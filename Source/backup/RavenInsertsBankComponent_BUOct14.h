

#ifndef Raven_NewSDK_RavenInsertsBankComponent_h
#define Raven_NewSDK_RavenInsertsBankComponent_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenInsertsChannelComponent.h"
#include "NscGuiElement.h"

class RavenInsertsBankComponent : public Component, public NscGuiContainer, public Button::Listener, public ComboBox::Listener
{
public:
    RavenInsertsBankComponent(int numChans, int bankNum) : NscGuiContainer(bankNum), mBankNum(bankNum), assignMode(false), incMode(false),
    trackInsertSelectMode(true), currentChannel(0), incUp(true), getPTInsertName(false), initMode(true), exitInit(false), showWindow(false), winNum(0), bypass(false), windowCurrentlyShown(false)
    {
        setSize (0, 0);
        int xOffset = 0;
        
        for(int i = 0; i < numChans; i++)
        {
            RavenInsertsChannelComponent *insertChannel = new RavenInsertsChannelComponent(this);
            addAndMakeVisible(insertChannel);
            insertChannel->setTopLeftPosition(xOffset, 0);
            insertChannel->setChannelAndBankIndex(i, bankNum);
            insertChannel->addButtonListener(this);
            insertChannel->addComboBoxListener(this);
            inserts.add(insertChannel);

            xOffset += insertChannel->getWidth();
            setSize(xOffset, insertChannel->getHeight());
        }
            
    }
    
    ~RavenInsertsBankComponent()
    {
        
    }
    
    void setInsertNames(StringArray &a)
    {
        for(int i = 0; i < inserts.size(); i++)
        {
            inserts[i]->setInsertNames(a);
        }
    }
    
    // Button::Listener interface
	void	buttonStateChanged (Button*){}
	void	buttonClicked (Button* b)
    {
    
        String buttonName = b->getName();
        if(buttonName.length() <= 0) return;
        int underScoreIndex = buttonName.indexOf("_");
        String chanStr = buttonName.substring(0, underScoreIndex);
        winChan = chanStr.getIntValue();
        String insNumStr = buttonName.substring(underScoreIndex+1);
        winNum = insNumStr.getIntValue();
        
        printf("insert , chan %d, insert %d\n", winChan, winNum);
        
        //showWindow = true;
        //check for separate window vs bypass buttons...
        bypass = true;
        
        
        /*
        NscGuiElement * element = dynamic_cast<NscGuiElement *>(b);
        if (!element)
        {
            // try our temporary workaround hack.
            int ptr = b->getProperties()[kNscGuiIdentifier];
            element = (NscGuiElement *) ptr;
            if (!element)
            {
                DBG("Switches must inherit and set up NscGuiElement");
                jassertfalse;
            }
        }
        if (element)
        {
            switchClicked(element, true);
        }
         */
    }
        
    bool checkShowWindowAndBypass()
    {
       if(showWindow || bypass)
       {
           static bool bypassOn = false;
           static bool win5 = false;
           static bool setInsertWin = true;
           static int setInsertWinCount = 0;
           if(setInsertWin)
           {
               if(setInsertWinCount ==0)
               {
                   link->set(eNSCTrackEvent, eNSCTrackInsertSwitch, mBankNum, winChan, 1);
                   setInsertWinCount++;
                   return true;
               }
               else if(setInsertWinCount == 1)
               {
                   
                   //check to make sure track insert switch set correctly winChan = last passed in chan if not, then repeat
                   
                   if(win5 && winNum !=4)
                   {
                       link->set(eNSCGlobalControl, eNSCGSPlugInBankLeft, mBankNum, winChan, 1);
                       win5 = false;
                       return true;
                   }
                   else if(!win5 && winNum == 4)
                   {
                       link->set(eNSCGlobalControl, eNSCGSPlugInBankRight, mBankNum, winChan, 1);
                       win5 = true;
                       return true;
                   }
                   
                   if(bypass)
                   {
                       //insert/param on/off
                       link->set(eNSCGlobalControl, eNSCGSPlugInInsert, mBankNum, winChan, 1);
                       if(!bypassOn)link->set(eNSCGlobalControl, eNSCGSPlugInInsert, mBankNum, winChan, 1);
                   }

                   else
                   {
                       if(winNum == 0)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect1, mBankNum, winChan, 1);
                       else if(winNum == 1)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect2, mBankNum, winChan, 1);
                       else if(winNum == 2)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect3, mBankNum, winChan, 1);
                       else if(winNum == 3)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect4, mBankNum, winChan, 1);
                       else if(winNum == 4)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect1, mBankNum, winChan, 1);
                   }


                   setInsertWinCount++;
                   return true;
               }
               else if(setInsertWinCount == 2)
               {
                   if(showWindow)
                   {
                       link->set(eNSCGlobalControl, eNSCGSPlugInInsert, mBankNum, winChan, 1);
                   }
                   else
                   {
                       if(winNum == 0)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect1, mBankNum, winChan, 1);
                       else if(winNum == 1)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect2, mBankNum, winChan, 1);
                       else if(winNum == 2)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect3, mBankNum, winChan, 1);
                       else if(winNum == 3)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect4, mBankNum, winChan, 1);
                       else if(winNum == 4)link->set(eNSCGlobalControl, eNSCGSPlugInSwitchSelect1, mBankNum, winChan, 1);
                   }
                   
                   setInsertWinCount = 0;
                   setInsertWin = false;
                   return true;
               }

           }
           else
           {
               if(showWindow)
               {
                   link->set(eNSCGlobalControl, eNSCGSPTWindow_PlugIn, mBankNum, winChan, 1);
                   showWindow = false;
               }
               else if(bypass)
               {
                   
                   if(!bypassOn)link->set(eNSCGlobalControl, eNSCGSPlugInBypass, mBankNum, winChan, 1);
                   else link->set(eNSCGlobalControl, eNSCGSPlugInBypass, mBankNum, winChan, 0);
                   bypassOn = !bypassOn;
                   //link->set(eNSCGlobalControl, eNSCGSPlugInBypass, mBankNum, winChan, 1);
                   //need to toggle insert/param mode on then off again before we can turn this off...weird
                   // how do track select and pluginswitches affect insert/param mode?
                   bypass = false;
               }
               
               setInsertWin = true;
               return true;
           }
       }
       else
       {
           return false;
       }
    }
    

    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, float val)
    {
        
        if(type == eNSCGlobalControl && subType == eNSCGSPTWindow_PlugIn)
        {
            //printf("window shown = %f\n", val);
            if(val != 0.0) windowCurrentlyShown = true;
            else windowCurrentlyShown = false;
        }
        
        if(checkShowWindowAndBypass()) return;
        
        //check for insert updates
        //if(checkInsertUpdate()) return;
        
        if (bank == eMatchesAllBanks || bank == mBankNum) this->callback(type, subType, bank, which, val);
    }
    
    void nscCallback(ENSCEventType type, int subType, int bank, int which, const char * text)
    {
        
        if(checkShowWindowAndBypass()) return;
        
        if(getPTInsertName)
        {
            //if insert name message
            if(type == eNSCTrackEvent && subType == 2 && bank == 0) //mBank insead of 0?
            {
                if(which == 40 || which == 50 || which == 60 || which == 70)
                {
                    String insStr = String(text);
                    if(insStr.length() > 0 && !insStr.startsWith(">"))
                    {
                        printf("name = %s, which = %d\n", text, which);
                        if(which == 40)
                        {
                            String current40 = getInsertName(currentChannel, 0);
                            printf("current 40 name is %s\n", (const char*)current40.toUTF8());
                            if(insStr.compare(getInsertName(currentChannel, 0))) //if != to current selection
                            {
                                printf("current 40 different from new 40\n");
                                setInsert(currentChannel, 0, insStr, false);
                                getPTInsertName = false;
                            }
                        }
                        else if(which == 50)
                        {
                            if(insStr.compare(getInsertName(currentChannel, 1))) //if != to current selection
                            {
                                setInsert(currentChannel, 1, insStr, false);
                                getPTInsertName = false;
                            }
                        }
                        else if(which == 60)
                        {
                            if(insStr.compare(getInsertName(currentChannel, 2))) //if != to current selection
                            {
                                setInsert(currentChannel, 2, insStr, false);
                                getPTInsertName = false;
                            }
                        }
                        else if(which == 70)
                        {
                            if(insStr.compare(getInsertName(currentChannel, 3))) //if != to current selection
                            {
                                setInsert(currentChannel, 3, insStr, false);
                                getPTInsertName = false;
                            }
                        }
                        
                    }
                }
            }
            
            return;
        }
        
        //if ProTools Changes Insert (after initialization)
        if(!initMode)
        {
            //printf("PTPT type = %d, subtype = %d, bank = %d, which = %d, ((((  %s  ))))\n", type, subType, bank, which, text);
            
            if(type == eNSCTrackEvent && subType == 2 && bank == 0 && which == 20) //mBank insead of 0?
            {
                String insertText = String(text);
                int iTrackNum = insertText.getIntValue();
                if(iTrackNum == 0) iTrackNum = 1;
                int iBankNum = iTrackNum / 9;
                int iChanNum = (iTrackNum % 9) - 1;
                printf("insert changed for track %d (bank %d, chan %d), mBank = %d\n", iTrackNum, iBankNum, iChanNum, mBankNum);
                
                if(iBankNum == mBankNum)
                {
                    currentChannel = iChanNum;
                    //getPTInsertName = true;  //causing string malloc bug..
                    return;
                }
            }
        }
        
        //check for insert updates
        //if(checkInsertUpdate()) return;
        
        //then pass along callback
        if (bank == eMatchesAllBanks || bank == mBankNum) this->callback(type, subType, bank, which, text);
        
    }

    //ComboBox Listener interface
    void comboBoxChanged (ComboBox *comboBoxThatHasChanged)
    {        
        newInsertName = comboBoxThatHasChanged->getText();
        
        String boxName = comboBoxThatHasChanged->getName();
        if(boxName.length() <= 0) return;
        int underScoreIndex = boxName.indexOf("_");
        String chanStr = boxName.substring(0, underScoreIndex);
        currentChannel = chanStr.getIntValue();
        String insNumStr = boxName.substring(underScoreIndex+1);
        currentInsNum = insNumStr.getIntValue();
        
        incMode = true;
        initMode = true;
    }
    
    void setInsert(int chanNum, int insertNum, String insertName, bool updateDAW)
    {
        inserts[chanNum]->setInsertDisplayName(insertNum, insertName); // 0 should be insNum when this is implemented...
        
        if(updateDAW)
        {
            newInsertName = insertName;
            incMode = true;
        }
    }
    
    String getInsertName(int chanNum, int insertNum)
    {
        inserts[chanNum]->getInsertName(insertNum);
    }
    
    void setLCD(RavenTextDisplay *lcd, int which)
    {
        if(which == 0) pluginLCD1 = lcd;
        else if(which == 1) pluginLCD2 = lcd;
        else if(which == 2) pluginLCD3 = lcd;
        else if(which == 3) pluginLCD4 = lcd;
    }
    
    void setAssignButton(RavenNSCButton *but){assignButton = but;}
    void exitInitMode(){exitInit = true;}
    
    RavenInsertsChannelComponent* getChannel(int num){return inserts[num];}

    
private:
    OwnedArray<RavenInsertsChannelComponent> inserts;
    RavenTextDisplay* pluginLCD1;
    RavenTextDisplay* pluginLCD2;
    RavenTextDisplay* pluginLCD3;
    RavenTextDisplay* pluginLCD4;
    int mBankNum;
    String currentInsertName, prevInsertName;
    String newInsertName;
    bool assignMode, trackInsertSelectMode, incMode, incUp, getPTInsertName, initMode, exitInit;
    bool showWindow, bypass, windowCurrentlyShown;
    int currentChannel, currentInsNum, winChan, winNum;
    RavenNSCButton *assignButton;
    
};


#endif
