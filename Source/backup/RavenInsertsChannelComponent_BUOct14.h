

#ifndef Raven_NewSDK_RavenInsertsChannelComponent_h
#define Raven_NewSDK_RavenInsertsChannelComponent_h

#include "../JuceLibraryCode/JuceHeader.h"
#include "RavenButton.h"
#include "RavenTouchComponent.h"
#include "RavenInsert.h"
#include "RavenInsertPop.h"
#include "RavenLabel.h"
#include "RavenPopupComponent.h"
#include "NscGuiElement.h"




class RavenInsertsChannelComponent : public RavenTouchComponent, public NscGuiElement, Button::Listener
{
public:
    RavenInsertsChannelComponent (NscGuiContainer *owner) : NscGuiElement(owner), mChan(0)
    {
        File imgFile(String(GUI_PATH) + "INSERT/insertPanel.png");
        insertPanelImg = ImageCache::getFromFile(imgFile);
        setSize (insertPanelImg.getWidth(), insertPanelImg.getHeight());
        
        //show insert window button
        File winButtonOffImgFile(String(GUI_PATH) + "INSERT/insertDisable_03.png");
        File winButtonOnImgFile(String(GUI_PATH) + "INSERT/insertOff_03.png");
        Image winButtonOffImage = ImageCache::getFromFile(winButtonOffImgFile);
        Image winButtonOnImage = ImageCache::getFromFile(winButtonOnImgFile);
        
        //display insert list button
        File insButtonOffImgFile(String(GUI_PATH) + "INSERT/insertDisable_04.png");
        File insButtonOnImgFile(String(GUI_PATH) + "INSERT/insertOff_04.png");
        Image insButtonOffImage = ImageCache::getFromFile(insButtonOffImgFile);
        Image insButtonOnImage = ImageCache::getFromFile(insButtonOnImgFile);
        
        int yOffset = 0;
        for(int i = 0; i < 5; i++)
        {
            
            ImageButton *insertWinButton = new ImageButton();
            insertWinButton->setImages(true, false, true, winButtonOnImage, 1.0f, Colour(0x0), Image(), 1.0f, Colour(0x0), winButtonOffImage, 1.0f, Colour(0x0));
            insertWinButton->setTopLeftPosition(0, yOffset);
            addAndMakeVisible(insertWinButton);
            winButtons.add(insertWinButton);
            touchImageButtons.add(insertWinButton);
            
            ///Combo Box...not foor for MultiTouch///
            RavenInsert *insert = new RavenInsert(owner);
            insert->setEditableText(false);
            insert->setJustificationType(Justification::left);
            insert->setTextWhenNothingSelected("loading...");
            insert->setTextWhenNoChoicesAvailable("loading...");
            insert->setBounds(28, yOffset, 80, 17); //28 = insert win button width
            //addAndMakeVisible(insert);
            inserts.add(insert);
            //touchBoxes.add(insert);
            
            
            //Custom PopupMenu for multitouch////////
            //its button:
            ImageButton *insertButton = new ImageButton();
            insertButton->setImages(true, false, true, insButtonOnImage, 1.0f, Colour(0x0), Image(), 1.0f, Colour(0x0), insButtonOffImage, 1.0f, Colour(0x0));
            insertButton->setTopLeftPosition(28, yOffset); //28 = insert win button width
            addAndMakeVisible(insertButton);
            insertButton->addListener(this);
            insButtons.add(insertButton);
            touchImageButtons.add(insertButton);
            
            //the popup menu:
            RavenInsertPop *insertPop = new RavenInsertPop();
            insertsPop.add(insertPop);
            
            //the label
            RavenLabel *insLabel = new RavenLabel();
            insLabel->setBounds(28, yOffset, 52, 20);
            addAndMakeVisible(insLabel);
            insLabels.add(insLabel);
            
            yOffset += 17;
        }
        
    }
    
    ~RavenInsertsChannelComponent()
    {
        deleteAllChildren();
    }
    
    void	buttonStateChanged (Button*){}
	void	buttonClicked (Button* b)
    {
        for(int i = 0; i < insButtons.size(); i++)
        {
            if(b == insButtons[i])
            {
                //ins #i
                int selectedIndex = insertsPop[i]->showAt(b) - 1;
                insLabels[i]->setText(insertNames[selectedIndex], false);
                inserts[i]->setSelectedItemIndex(selectedIndex);
            }
        }
    }
    
    void paint (Graphics& g)
    {
        
        //g.fillAll (Colours::lightblue);
        g.drawImageAt(insertPanelImg, 0, 0);
        
    }
    
    void setChannelAndBankIndex(int chan, int bank)
    {
        
        mChan = chan;
        
        for(int i = 0; i < inserts.size(); i++)
        {
            inserts[i]->setName(String(chan) + "_" + String(i));
            
        }
        
        for(int i = 0; i < winButtons.size(); i++)
        {
            winButtons[i]->setName(String(chan) + "_" + String(i));
        }
        
         
    }
    
    
    void addButtonListener(Button::Listener * listener)
    {
        for(int i = 0; i < ravenNSCTouchButtons.size(); i++) ravenNSCTouchButtons[i]->addListener(listener);
        for(int i = 0; i < winButtons.size(); i++) winButtons[i]->addListener(listener);
    }
    
    void addComboBoxListener(ComboBox::Listener * listener)
    {
        for(int i = 0; i < inserts.size(); i++) inserts[i]->addListener(listener);
    }
    
    void setInsertNames(StringArray &a)
    {
        insertNames = a;
        
        for(int i = 0; i < inserts.size(); i++)
        {
            const MessageManagerLock lock;// lock mm queue since we are going to be updating gui elements
            if(lock.lockWasGained())
            {
                inserts[i]->clear();
                inserts[i]->addItemList(insertNames, 1); //1 indexed, 0 used to indicate lack of selection
                inserts[i]->setSelectedItemIndex(0, true);// true => don't send change message 
                //need this to automatically update to the actual insert selected
                
                //new popup:
                for(int j = 0; j < insertNames.size(); j++)
                {
                    RavenPopupComponent *popCmp = new RavenPopupComponent(insertNames[j]);
                    touchPopupComponents.add(popCmp);
                    insertsPop[i]->addCustomItem(j+1, popCmp);
                }
                
                
            }
        }
    }
    
    void setInsertDisplayName(int which, String name)
    {
        //printf("Called to set %d to %s\n", which, (const char*)name.toUTF8());
        const MessageManagerLock lock;// lock mm queue since we are going to be updating gui elements
        while(!(lock.lockWasGained())){}
        if(lock.lockWasGained())
        {
        
            for(int i = 0; i < inserts[which]->getNumItems(); i++)
            {
                if(inserts[which]->getItemText(i).compare(name) == 0)
                {
                    //printf("item %d = %s\n", i, (const char*)name.toUTF8());
                    inserts[which]->setSelectedItemIndex(i, true); //true=> don't send change message
                }
            }
        }
    }
    
    String getInsertName(int insertNum)
    {
        const MessageManagerLock lock;// lock mm queue since we are going to be updating gui elements
        if(lock.lockWasGained()) return inserts[insertNum]->getText();
        else return String(" ");
    }
    
    
private:
    
    int mChan;
    
    Image insertPanelImg;
    
    RavenNSCButton *testButton1;
    RavenNSCButton *testButton2;
    RavenNSCButton *testButton3;
    RavenNSCButton *testButton4;
    
    RavenTextDisplay *nameDisplay1;
    RavenTextDisplay *nameDisplay2;
    
    OwnedArray<ComboBox> inserts;
    OwnedArray<PopupMenu> insertsPop;
    OwnedArray<ImageButton> winButtons;
    OwnedArray<ImageButton> insButtons;
    OwnedArray<RavenLabel> insLabels;
    
    
    StringArray testNames;
    
    StringArray insertNames;
        
};
 


#endif
