
#ifndef __JUCER_HEADER_MOUSEHIDECOMPONENT_MOUSEHIDECOMPONENT_783F27E3__
#define __JUCER_HEADER_MOUSEHIDECOMPONENT_MOUSEHIDECOMPONENT_783F27E3__

#include "../JuceLibraryCode/JuceHeader.h"

class MouseHideComponent  : public Component
{
public:
    MouseHideComponent (int parentWidth, int parentHeight)
    {
        setBounds(0, 0, parentWidth, parentHeight);
        setMouseCursor(MouseCursor::NoCursor);
        //setInterceptsMouseClicks is true by default
        setRepaintsOnMouseActivity(false); //nothing to paint, but just in case
        setAlwaysOnTop(true);
    }
    
    ~MouseHideComponent()
    {
        
    }
    
    /*
    void paint (Graphics& g)
    {
        g.fillAll(Colours::blue.withAlpha(0.5f));
    }//*/

//just for testing. These shouldn't be necessary:
//    virtual void mouseMove(const MouseEvent & 	event)
//    {
//        printf("mouseMove\n");
//        Desktop::getInstance().getMainMouseSource().ravenShowMouseCursor(MouseCursor::NoCursor);
//    }
//    
//    virtual void mouseEnter(const MouseEvent & 	event)
//    {
//        printf("mouseEnter\n");
//        Desktop::getInstance().getMainMouseSource().ravenShowMouseCursor(MouseCursor::NoCursor);
//    }
//    
//    virtual void mouseExit(const MouseEvent & 	event)
//    {
//        printf("mouseExit\n");
//        Desktop::getInstance().getMainMouseSource().ravenShowMouseCursor(MouseCursor::NoCursor);
//    }

private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(MouseHideComponent)
};

#endif   // __JUCER_HEADER_MOUSEHIDECOMPONENT_MOUSEHIDECOMPONENT_783F27E3__
