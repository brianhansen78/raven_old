/* (Auto-generated binary data file). */

#ifndef BINARY_RAVENFUTURAFONT_H
#define BINARY_RAVENFUTURAFONT_H

namespace RavenFuturaFont
{
    extern const char*  ravenfuturafont;
    const int           ravenfuturafontSize = 16352;

}

#endif
